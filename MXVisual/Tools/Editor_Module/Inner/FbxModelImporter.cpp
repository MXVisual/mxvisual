#include "FbxModelImporter.h"
#include "MXCEditorPanel.h"

#include "MXCModelImporterCommon.h"
#include "MXMath.h"
#include "fbxsdk.h"

#include "MXLog.h"
#include "MXAsset.h"
#include "vector"
#include <functional>

using namespace MXVisual;

Void	ConvertScene(FbxManager* pManager, FbxScene* pFbxScene)
{
	FbxAxisSystem SceneAxisSystem = pFbxScene->GetGlobalSettings().GetAxisSystem();

	if (SceneAxisSystem != FbxAxisSystem::OpenGL)
	{
		FbxAxisSystem::OpenGL.ConvertScene(pFbxScene);
	}

	// Convert Unit System to what is used in this example, if needed
	FbxSystemUnit SceneSystemUnit = pFbxScene->GetGlobalSettings().GetSystemUnit();
	//The unit in this example is centimeter.
	FbxSystemUnit::cm.ConvertScene(pFbxScene);

	//三角化转换
	FbxGeometryConverter triConverter(pManager);
	triConverter.Triangulate(pFbxScene, true);
}

Float3	ConvertVector(FbxVector4 vec)
{
	return Float3((Float1)vec[0], (Float1)vec[1], (Float1)vec[2]);
}
Float3	ConvertMeshVector(FbxVector4 vec)
{
	return Float3((Float1)vec[0], (Float1)vec[1], (Float1)vec[2]);
}
Float4	ConvertMeshVector4(FbxVector4 vec)
{
	return Float4((Float1)vec[0], (Float1)vec[1], (Float1)vec[2], (Float1)vec[3]);
}

RawNodeData* ConvertNode(FbxNode* pNode)
{
	RawNodeData* pRes = nullptr;

	if (pNode)
	{
		pRes = new RawNodeData;
		pRes->eType = RawNodeData::eNormal;
		pRes->strName = pNode->GetName();

		FbxNodeAttribute *pAttribute = pNode->GetNodeAttribute();

		//转换node的初始世界变换
		FbxAMatrix matGeo(pNode->GetGeometricTranslation(FbxNode::eSourcePivot),
			pNode->GetGeometricRotation(FbxNode::eSourcePivot),
			pNode->GetGeometricScaling(FbxNode::eSourcePivot));

		FbxAMatrix matOffset = pNode->EvaluateLocalTransform();
		matOffset *= matGeo;

		pRes->matInit = Matrix4(
			(Float1)matOffset[0][0], (Float1)matOffset[0][1], (Float1)matOffset[0][2], (Float1)matOffset[0][3],
			(Float1)matOffset[1][0], (Float1)matOffset[1][1], (Float1)matOffset[1][2], (Float1)matOffset[1][3],
			(Float1)matOffset[2][0], (Float1)matOffset[2][1], (Float1)matOffset[2][2], (Float1)matOffset[2][3],
			(Float1)matOffset[3][0], (Float1)matOffset[3][1], (Float1)matOffset[3][2], (Float1)matOffset[3][3]
		);

		if (pNode->GetNodeAttribute())
		{
			if (pAttribute->GetAttributeType() == FbxNodeAttribute::eMesh)
			{
				pRes->eType = RawNodeData::eMesh;

				FbxMesh *pFbxMesh = (FbxMesh*)pNode->GetNodeAttribute();

				FbxGeometryElementNormal*		pFbxElementNormal = pFbxMesh->GetElementNormal(0);
				FbxGeometryElementTangent*		pFbxElementTangent = pFbxMesh->GetElementTangent(0);
				FbxGeometryElementVertexColor*	pFbxElementVertexColor0 = pFbxMesh->GetElementVertexColor(0);
				FbxGeometryElementUV*			pFbxElementUV0 = pFbxMesh->GetElementUV(0);
				FbxGeometryElementMaterial*		pFbxElementMaterial = pFbxMesh->GetElementMaterial(0);

				Boolean bHasNormal = pFbxElementNormal && pFbxElementNormal->GetMappingMode() != FbxGeometryElement::eNone;
				Boolean bHasTangent = pFbxElementTangent && pFbxElementTangent->GetMappingMode() != FbxGeometryElement::eNone;
				Boolean bHasVertexColor0 = pFbxElementVertexColor0 && pFbxElementVertexColor0->GetMappingMode() != FbxGeometryElement::eNone;
				Boolean bHasTexCoord0 = pFbxElementUV0 && pFbxElementUV0->GetMappingMode() != FbxGeometryElement::eNone;
				Boolean bHasMaterialID = pFbxElementMaterial != nullptr;

				Boolean bAllByControlPoint =
					bHasNormal && pFbxElementNormal->GetMappingMode() == FbxGeometryElement::eByControlPoint &&
					bHasTangent && pFbxElementTangent->GetMappingMode() == FbxGeometryElement::eByControlPoint &&
					bHasVertexColor0 && pFbxElementVertexColor0->GetMappingMode() == FbxGeometryElement::eByControlPoint &&
					bHasTexCoord0 && pFbxElementUV0->GetMappingMode() == FbxGeometryElement::eByControlPoint;

				UInt32 uTriangleCount = (UInt32)pFbxMesh->GetPolygonCount();
				UInt32 uVertexCount = (UInt32)pFbxMesh->GetControlPointsCount();
				if (!bAllByControlPoint)
				{
					uVertexCount = uTriangleCount * 3;
				}

				Array<Float3> aNormal(uVertexCount);
				Array<Float4> aTangent(uVertexCount);
				Array<Float4> aColor(uVertexCount);
				Array<Float2> aTexCoord(uVertexCount);
				Array<UInt32> aMaterialID(uTriangleCount);

				FbxVector4* v4ControlPoints = pFbxMesh->GetControlPoints();

				Array<std::vector<UInt32>> arrCtrlPtIdxToVertexIdx(pFbxMesh->GetControlPointsCount());

#pragma region CONVERT_VERTEX_DATA_BYCONTROLPOINT
				if (bAllByControlPoint)
				{
					//Convert Vertex If By Control Point
					for (UInt32 u = 0; u < uVertexCount; u++)
					{
						pRes->f3VertexPosition.Add(ConvertMeshVector(v4ControlPoints[u]));

						arrCtrlPtIdxToVertexIdx[u].push_back(u);

#define CONVERT_ELEMENT(element)\
int nIndex = u;\
if(element->GetReferenceMode() == FbxLayerElement::eIndexToDirect)\
{\
	nIndex = element->GetIndexArray().GetAt(nIndex);\
}\
auto data = element->GetDirectArray().GetAt(nIndex);

						if (bHasNormal)
						{
							CONVERT_ELEMENT(pFbxElementNormal);
							aNormal[u] = Float3((Float1)data[0], (Float1)data[1], (Float1)data[2]);
						}

						if (bHasTangent)
						{
							CONVERT_ELEMENT(pFbxElementTangent);
							aTangent[u] = Float4((Float1)data[0], (Float1)data[1], (Float1)data[2], (Float1)data[3]);
						}

						if (bHasTexCoord0)
						{
							CONVERT_ELEMENT(pFbxElementUV0);
							aTexCoord[u] = Float2((Float1)data[0], (Float1)data[1]);
						}

						if (bHasVertexColor0)
						{
							CONVERT_ELEMENT(pFbxElementVertexColor0);
							aColor[u] = Float4((Float1)data[0], (Float1)data[1], (Float1)data[2], (Float1)data[3]);
						}
#undef CONVERT_ELEMENT
					}
				}
#pragma endregion

#pragma region CONVERT_INDEX_DATA
				for (UInt32 uTriangleIndex = 0; uTriangleIndex < uTriangleCount; uTriangleIndex++)
				{
					for (UInt32 u = 0; u < 3; u++)
					{
						UInt32 uVertexIndex = pFbxMesh->GetPolygonVertex(uTriangleIndex, u);
						if (!bAllByControlPoint)
						{
							UInt32 uNewIndex = uTriangleIndex * 3 + u;
							pRes->uIndex.Add(uNewIndex);
							arrCtrlPtIdxToVertexIdx[uVertexIndex].push_back(uNewIndex);

							FbxVector4 pos = v4ControlPoints[uVertexIndex];
							pRes->f3VertexPosition.Add(Float3((Float1)pos[0], (Float1)pos[1], (Float1)pos[2]));


#pragma region CONVERT_NORMAL
							if (bHasNormal)
							{
								FbxVector4 data;
								pFbxMesh->GetPolygonVertexNormal(uTriangleIndex, u, data);
								aNormal[uNewIndex] = Float3((Float1)data[0], (Float1)data[1], (Float1)data[2]);
							}
#pragma endregion

#pragma region CONVERT_TANGENT
							if (bHasTangent)
							{
								int nIndex = uTriangleIndex * 3 + u;
								if (pFbxElementTangent->GetReferenceMode() == FbxLayerElement::eIndexToDirect)
								{
									nIndex = pFbxElementTangent->GetIndexArray().GetAt(nIndex);
								}
								auto data = pFbxElementTangent->GetDirectArray().GetAt(nIndex);
								aTangent[uNewIndex] = Float4((Float1)data[0], (Float1)data[1], (Float1)data[2], (Float1)data[3] < 0.0f ? 1.0f : -1.0f);
							}
#pragma endregion

#pragma region CONVERT_VERTEXCOLOR
							if (bHasVertexColor0)
							{
								int nIndex = uTriangleIndex * 3 + u;
								if (pFbxElementVertexColor0->GetReferenceMode() == FbxLayerElement::eIndexToDirect)
								{
									nIndex = pFbxElementVertexColor0->GetIndexArray().GetAt(nIndex);
								}
								auto data = pFbxElementVertexColor0->GetDirectArray().GetAt(nIndex);
								aColor[uNewIndex] = Float4((Float1)data[0], (Float1)data[1], (Float1)data[2], (Float1)data[3]);
							}
#pragma endregion

#pragma region CONVERT_TEXCOORD0
							if (bHasTexCoord0)
							{
								FbxVector2 data;
								bool b = false;
								pFbxMesh->GetPolygonVertexUV(uTriangleIndex, u, pFbxElementUV0->GetName(), data, b);
								aTexCoord[uNewIndex] = Float2((Float1)data[0], 1.0f - (Float1)data[1]);
							}
#pragma endregion

#pragma region CONVERT_MATERIALID
							if (bHasMaterialID)
							{
								UInt32 uMtlID = (UInt32)pFbxElementMaterial->GetIndexArray().GetAt(uTriangleIndex);
								aMaterialID[uTriangleIndex] = uMtlID;
							}
#pragma endregion
						}
						else
						{
							pRes->uIndex.Add(uVertexIndex);
						}
					}
				}

				if (bHasNormal)
					pRes->f3VertexNormal = aNormal;
				if (bHasTangent)
					pRes->f4VertexTangent = aTangent;
				if (bHasVertexColor0)
					pRes->f4VertexColor = aColor;
				if (bHasTexCoord0)
					pRes->f2VertexTexCoord = aTexCoord;
				if (bHasMaterialID)
					pRes->uMaterialID = aMaterialID;
#pragma endregion

#pragma region CONVERT_SKIN_DATA
				UInt32 uDeformerCount = pFbxMesh->GetDeformerCount();
				FbxDeformer* pDeformer = nullptr;
				auto FindBoneIndex = [pRes](const Char* szBoneName) {
					for (UInt32 u = 0; u < pRes->BoneName.ElementCount(); u++)
					{
						if (pRes->BoneName[u] == szBoneName)
						{
							return (SInt32)u;
						}
					}
					return -1;
				};
				//有且只有一个蒙皮信息
				for (UInt32 u = 0; u < uDeformerCount; u++)
				{
					pDeformer = pFbxMesh->GetDeformer(u);
					if (pDeformer->GetDeformerType() != FbxDeformer::eSkin)
					{
						pDeformer = nullptr;
						continue;
					}
				}

				if (pDeformer)
				{
					pRes->VertexBlendData.Resize(uVertexCount);
					for (UInt32 uVertex = 0; uVertex < uVertexCount; uVertex++)
					{
						NodeVertexBlendData emptydata;
						pRes->VertexBlendData.Add(emptydata);
					}

					FbxSkin* pSkin = (FbxSkin*)pDeformer;
					for (UInt32 uCluster = 0; uCluster < (UInt32)pSkin->GetClusterCount(); uCluster++)
					{
						FbxCluster* pCluster = pSkin->GetCluster(uCluster);
						if (!pCluster->GetLink())continue;

						FbxNode* pNode = pCluster->GetLink();
						FbxAMatrix matLocal2Bone;
						FbxAMatrix matA, matB;
						pCluster->GetTransformMatrix(matA);
						pCluster->GetTransformLinkMatrix(matB);
						matLocal2Bone = matB.Inverse() * matA;

						FbxVector4 p = matLocal2Bone.GetT();
						FbxVector4 s = matLocal2Bone.GetS();
						FbxQuaternion q = matLocal2Bone.GetQ();

						Matrix4 mat;
						mat = Math::MatrixScale(Float3((Float1)s[0], (Float1)s[1], (Float1)s[2]));
						mat *= Math::MatrixQuaternion(Quaternion((Float1)q[0], (Float1)q[1], (Float1)q[2], (Float1)q[3]));
						mat *= Math::MatrixTranslation(Float3((Float1)p[0], (Float1)p[1], (Float1)p[2]));

						pRes->BoneName.Add(pNode->GetName());
						pRes->BoneLocal2Bone.Add(mat);

						UInt32 uBlendIndexCount = pCluster->GetControlPointIndicesCount();

						int* indexs = pCluster->GetControlPointIndices();
						double* weights = pCluster->GetControlPointWeights();

						for (UInt32 uBlendVertex = 0; uBlendVertex < uBlendIndexCount; uBlendVertex++)
						{
							UInt32 u = 0;
							for (UInt32 uVertexIdx = 0; uVertexIdx < arrCtrlPtIdxToVertexIdx[indexs[uBlendVertex]].size(); uVertexIdx++)
							{
								UInt32 idx = arrCtrlPtIdxToVertexIdx[indexs[uBlendVertex]][uVertexIdx];
								while (u < 4 &&
									pRes->VertexBlendData[idx].uBlendIndex[u] != -1 &&
									pRes->VertexBlendData[idx].fBlendWeight[u] > Constant_MIN_FLOAT)
								{
									u++;
								}
								if (u < 4)
								{
									pRes->VertexBlendData[idx].uBlendIndex[u] = uCluster;
									pRes->VertexBlendData[idx].fBlendWeight[u] = (Float1)weights[uBlendVertex];
								}
							}

						}
					}
				}
#pragma endregion
			}
			else if (pAttribute->GetAttributeType() == FbxNodeAttribute::eSkeleton)
			{
				pRes->eType = RawNodeData::eBone;
			}
		}

		SInt32 nChildCount = pNode->GetChildCount();
		for (SInt32 n = 0; n < (SInt32)pNode->GetChildCount(); n++)
		{
			RawNodeData* pChild = ConvertNode(pNode->GetChild(n));
			pRes->aChild.Add(pChild);
		}
	}
	return pRes;
}

RawNodeData* ConvertModel(FbxScene* pFbxScene)
{
	FbxNode *pRootNode = pFbxScene->GetRootNode();

	RawNodeData* pRawRoot = nullptr;

	pRawRoot = ConvertNode(pRootNode);

	return pRawRoot;
}

Void		 ConvertAllAnim(FbxScene* pFbxScene, Array<RawAnimation*>& outArrAnim)
{
	auto ConvertSingleNodeAnim = [](FbxNode* pNode, FbxTime TimeStart, FbxTime TimeEnd, RawNodeAnimStack* pStack)
	{
		pStack->strNodeName = pNode->GetName();

		FbxTime deltaTime;
		deltaTime.SetTime(0, 0, 0, 1, 0, TimeStart.GetGlobalTimeMode());

		for (FbxTime t = TimeStart; t < TimeEnd; t += deltaTime)
		{
			FbxAMatrix matGeo(pNode->GetGeometricTranslation(FbxNode::eSourcePivot),
				pNode->GetGeometricRotation(FbxNode::eSourcePivot),
				pNode->GetGeometricScaling(FbxNode::eSourcePivot));

			FbxAMatrix mat = pNode->EvaluateLocalTransform(t);
			mat *= matGeo;

			FbxVector4 p = mat.GetT();
			FbxVector4 s = mat.GetS();
			FbxQuaternion q = mat.GetQ();

			Matrix4 m;
			m = Math::MatrixScale(Float3((Float1)s[0], (Float1)s[1], (Float1)s[2]));
			m *= Math::MatrixQuaternion(Quaternion((Float1)q[0], (Float1)q[1], (Float1)q[2], (Float1)q[3]));
			m *= Math::MatrixTranslation(Float3((Float1)p[0], (Float1)p[1], (Float1)p[2]));

			pStack->arrTransform.Add(m);
		}
	};

	std::function<void(FbxNode*, FbxTime, FbxTime, RawAnimation*)> ConvertSingleAnim;
	ConvertSingleAnim = [&ConvertSingleAnim, &ConvertSingleNodeAnim](FbxNode* pNode, FbxTime TimeStart, FbxTime TimeEnd, RawAnimation* outAnimStack)
	{
		if (strcmp(pNode->GetName(), "RootNode") && (!pNode->GetNodeAttribute() ||
			pNode->GetNodeAttribute()->GetAttributeType() != FbxNodeAttribute::eMesh))
		{
			RawNodeAnimStack* pNewNodeStack = new RawNodeAnimStack;
			ConvertSingleNodeAnim(pNode, TimeStart, TimeEnd, pNewNodeStack);
			outAnimStack->arrAnimStack.Add(pNewNodeStack);
		}
		for (UInt32 u = 0; u < (UInt32)pNode->GetChildCount(); u++)
		{
			ConvertSingleAnim(pNode->GetChild(u), TimeStart, TimeEnd, outAnimStack);
		}
	};

	FbxNode* pRootNode = pFbxScene->GetRootNode();

	FbxArray<FbxString*> arrAnimStackName;
	pFbxScene->FillAnimStackNameArray(arrAnimStackName);
	for (UInt32 u = 0; u < (UInt32)arrAnimStackName.Size(); u++)
	{
		FbxAnimStack* pAnimStack = pFbxScene->FindMember<FbxAnimStack>(arrAnimStackName[u]->Buffer());
		FbxTakeInfo* pInfo = pFbxScene->GetTakeInfo(*arrAnimStackName[u]);
		FbxAnimLayer* pAnimLayer = pAnimStack->GetMember<FbxAnimLayer>();
		auto t = pInfo->mLocalTimeSpan;

		RawAnimation* pNewAnim = new RawAnimation;
		pNewAnim->strName = arrAnimStackName[u]->Buffer();
		pNewAnim->fAnimTime = t.GetDuration().GetMilliSeconds() / 1000.0f;
		ConvertSingleAnim(pRootNode, t.GetStart(), t.GetStop(), pNewAnim);

		pNewAnim->fSampleRate = pNewAnim->fAnimTime / pNewAnim->arrAnimStack[0]->arrTransform.ElementCount();
		outArrAnim.Add(pNewAnim);
	}
}

Boolean FindMeshes(RawNodeData* pData, Array<RawNodeData*>& outMeshesToConvert, Boolean& bHasSkeleton)
{
	if (!pData)return True;

	if (pData->eType == RawNodeData::eMesh && pData->f3VertexPosition.ElementCount() > 0)
	{
		outMeshesToConvert.Add(pData);
		bHasSkeleton |= pData->BoneName.ElementCount() > 0;
	}

	for (UInt32 u = 0; u < pData->aChild.ElementCount(); u++)
	{
		FindMeshes(pData->aChild[u], outMeshesToConvert, bHasSkeleton);
	}

	return True;
}

FbxModelImporter::FbxModelImporter()
	: m_bImportMesh(True)
	, m_bImportSkeleton(False)
	, m_bImportAnimation(False)
	, m_fImportScale(1.0f)
{}


Void	FbxModelImporter::SetImportFileName(const Char* szFileName)
{
	m_strFileName = szFileName;
}

Void	FbxModelImporter::SetImportTargetPath(const Char* szTargetPath)
{
	m_strTargetPath = szTargetPath;
}

Boolean FbxModelImporter::Import()
{
	if (m_strFileName.IsEmpty() || m_strTargetPath.IsEmpty())
		return False;

	FbxManager* pManager = FbxManager::Create();
	FbxScene* pScene = FbxScene::Create(pManager, "Scene");

	FbxIOSettings *pFbxIOSetting = FbxIOSettings::Create(pManager, "");
	pFbxIOSetting->SetBoolProp(IMP_FBX_EXTRACT_EMBEDDED_DATA, false);//禁止加载嵌入的其他数据，比如贴图，否则加载的时候会在fbx路径下生成一堆贴图
	FbxImporter* pImporter = FbxImporter::Create(pManager, "");

	RawImportData *pRes = nullptr;
	if (pImporter->Initialize(m_strFileName.CString(), -1, pFbxIOSetting))
	{
		int lFileMajor, lFileMinor, lFileRevision;
		pImporter->GetFileVersion(lFileMajor, lFileMinor, lFileRevision);

		if (pImporter->Import(pScene))
		{
			pImporter->Destroy();
			pRes = new RawImportData;
			ConvertScene(pManager, pScene);
			pRes->pRootNode = ConvertModel(pScene);

			if (m_bImportAnimation)
			{
				ConvertAllAnim(pScene, pRes->arrAnimStack);
			}
		}

	}
	else
	{
		FbxString error = pImporter->GetStatus().GetErrorString();
		Log::Output("FbxImporter", error.Buffer());
		pScene->Destroy();
		pManager->Destroy();

		return False;
	}

	pScene->Destroy();
	pManager->Destroy();

	String strFileName = m_strFileName;
	SInt32 n = strFileName.FindLastOf("\\");
	if (n > -1)	strFileName.Erase(0, n + 1);
	n = strFileName.Find(".");
	if (n > -1)	strFileName.Erase(n);

	Array<RawNodeData*> arrMeshesToConvert;
	Boolean bHasSkeleton = False;
	FindMeshes(pRes->pRootNode, arrMeshesToConvert, bHasSkeleton);

	Boolean bResult = True;

	IAppSkeleton* pSkeleton = nullptr;
	if (bHasSkeleton && m_bImportSkeleton)
	{
		pSkeleton = IAppSkeleton::Create(strFileName.CString());
		if (!ConvertSkeleton(pRes->pRootNode, pSkeleton))
		{
			Log::Output("FbxImporter", "Convert Skeleton Failed");
			bResult = False;
		}

		String strName =
			String::Format("%s\\%s.%s", m_strTargetPath.CString(), strFileName.CString(), IAppSkeleton::Suffix);
		if (!AppAssetManager::SaveAsset(pSkeleton, strName.CString()))
		{
			Log::Output("FbxImporter", "Save Skeleton Failed");
			bResult = False;
		}

		MX_RELEASE_INTERFACE(pSkeleton);
	}

	if (m_bImportMesh)
	{
		for (UInt32 u = 0; u < arrMeshesToConvert.ElementCount(); u++)
		{
			arrMeshesToConvert[u]->strName = strFileName + String::Format("_%d", u);

			if (arrMeshesToConvert[u]->BoneName.ElementCount() > 0)
			{
				IAppMesh* pMesh = ConvertSkinMeshData(arrMeshesToConvert[u]);
				if (pMesh == nullptr)
				{
					Log::Output("FbxImporter", "Convert Mesh Failed");
					bResult = False;
				}

				String strName =
					String::Format("%s\\%s.%s", m_strTargetPath.CString(),
						arrMeshesToConvert[u]->strName.CString(),
						IAppMesh::Suffix);
				if (!AppAssetManager::SaveAsset(pMesh, strName.CString()))
				{
					Log::Output("FbxImporter", "Save Mesh Failed");
					bResult = False;
				}

				MX_RELEASE_INTERFACE(pMesh);
			}
			else
			{
				IAppMesh* pMesh = ConvertRigidMeshData(arrMeshesToConvert[u]);
				if (pMesh == nullptr)
				{
					Log::Output("FbxImporter", "Convert Mesh Failed");
					bResult = False;
				}

				String strName =
					String::Format("%s\\%s.%s", m_strTargetPath.CString(),
						arrMeshesToConvert[u]->strName.CString(),
						IAppMesh::Suffix);
				if (!AppAssetManager::SaveAsset(pMesh, strName.CString()))
				{
					Log::Output("FbxImporter", "Save Mesh Failed");
					bResult = False;
				}
				MX_RELEASE_INTERFACE(pMesh);
			}
		}
	}

	if (m_bImportAnimation)
	{
		IAppAnimationClip* pAnim = nullptr;
		for (UInt32 u = 0; u < pRes->arrAnimStack.ElementCount(); u++)
		{
			pAnim = ConvertAnimationData(pRes->arrAnimStack[u]);
			if (pAnim == nullptr)
			{
				Log::Output("FbxImporter", "Convert Animation Failed");
				bResult = False;
			}

			String strName =
				String::Format("%s\\%s.%s", m_strTargetPath.CString(), 
					pRes->arrAnimStack[u]->strName.CString(), IAppAnimationClip::Suffix);

			if (!AppAssetManager::SaveAsset(pAnim, strName.CString()))
			{
				Log::Output("FbxImporter", "Save Animation Failed");
				bResult = False;
			}
		}
	}

	delete pRes;
	return bResult;
}

Void	FbxModelImporter::OnPanelActived(IEditorPropertyPanel& Panel)
{
	Panel.AddPropertyTrigger("ImportMesh", m_bImportMesh);
	Panel.AddPropertyTrigger("CombineMesh", False);
	Panel.AddPropertyTrigger("ImportSkeleton", m_bImportSkeleton);
	Panel.AddPropertyTrigger("ImportAnimation", m_bImportAnimation);
	//Panel.AddPropertyFloat1("ImportScale", m_fImportScale, 0.0f, Constant_MAX_PATH, 0.01f);
}
Void	FbxModelImporter::OnPanelChanged(const IEditorProperty* pProperty)
{
	if (strcmp(pProperty->Name, "ImportMesh") == 0)
	{
		m_bImportMesh = ((EditorPropertyTrigger*)pProperty)->Value;
	}
	else if (strcmp(pProperty->Name, "ImportSkeleton") == 0)
	{
		m_bImportSkeleton = ((EditorPropertyTrigger*)pProperty)->Value;
	}
	else if (strcmp(pProperty->Name, "ImportAnimation") == 0)
	{
		m_bImportAnimation = ((EditorPropertyTrigger*)pProperty)->Value;
	}
	else if (strcmp(pProperty->Name, "ImportScale") == 0)
	{
		m_fImportScale = ((EditorPropertyFloat1*)pProperty)->Value;
	}
}
