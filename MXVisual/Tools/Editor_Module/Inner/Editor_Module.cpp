#include "Editor_Module.h"
#include <map>

//Basic Importer
#include "FbxModelImporter.h"

namespace Editor_Importer
{
	MXVisual::Void			Enumerate(MXVisual::Array<Description>& arrImporter)
	{
#define MAKE_DESCRIPTION(name, formatcount, format) arrImporter.Add({ #name##, formatcount, {format} });

		MAKE_DESCRIPTION(FbxModelImporter, 1, "fbx");

#undef	MAKE_DESCRIPTION
	}

	MXVisual::ICImporter*	Create(const MXVisual::Char* szImporterName)
	{
#define MAKE_IMPORTER(type)	if(_stricmp(szImporterName, #type##) == 0)	return new type;

		MAKE_IMPORTER(FbxModelImporter);

#undef	MAKE_IMPORTER
		return nullptr;
	}
}