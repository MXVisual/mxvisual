#ifndef _MODEL_IMPORTER_
#define _MODEL_IMPORTER_
#include "MXPlatform.h"
#include "MXArray.h"
#include "MXVector.h"
#include "MXMatrix.h"
#include "MXString.h"

namespace MXVisual
{
	const UInt32 uMaxBlendCount = 4;
	struct NodeVertexBlendData
	{
		UInt32 uBlendIndex[uMaxBlendCount];
		Float1 fBlendWeight[uMaxBlendCount];
	};

	struct RawNodeAnimStack
	{
		String strNodeName;
		Array<Matrix4> arrTransform;
	};

	struct RawAnimation
	{
		String strName;
		Float1 fAnimTime;
		Float1 fSampleRate;
		Array<RawNodeAnimStack*> arrAnimStack;
	};

	struct RawNodeData
	{
		enum NodeType
		{
			eNormal,
			eMesh,
			eBone
		};
		//Node Base Data
		NodeType		eType;
		String			strName;
		Matrix4			matInit;
		Array<RawNodeData*> aChild;

		//Mesh Data
		Array<Float3> f3VertexPosition;
		Array<Float3> f3VertexNormal;
		Array<Float4> f4VertexTangent;//����������
		Array<Float2> f2VertexTexCoord;
		Array<Float4> f4VertexColor;
		Array<NodeVertexBlendData> VertexBlendData;
		Array<UInt32> uIndex;
		Array<UInt32> uMaterialID;

		//Skeleton Data
		Array<String>	BoneName;
		Array<Matrix4>	BoneLocal2Bone;
	};

	struct RawImportData
	{
		RawNodeData* pRootNode;
		Array<RawAnimation*> arrAnimStack;
	};

	Boolean ConvertSkeleton(RawNodeData* pData, class IAppSkeleton* pSkeleton);
	class IAppMesh* ConvertRigidMeshData(RawNodeData* pData);
	class IAppMesh* ConvertSkinMeshData(RawNodeData* pData);
	class IAppAnimationClip*	ConvertAnimationData(RawAnimation* pData);
}

#endif