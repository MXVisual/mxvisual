#include "MXCModelImporterCommon.h"
#include "MXLog.h"
#include "MXMath.h"
#include "MXFile.h"
#include "MXAsset.h"

namespace MXVisual
{
#define MAX_SUBMESH_COUNT 16

	Boolean ConvertSkeleton(RawNodeData* pData, IAppSkeleton* pSkeleton)
	{
		if (pSkeleton == nullptr)return False;

		Boolean bResult = True;
		for (UInt32 u = 0; u < pData->aChild.ElementCount(); u++)
		{
			if (pData->eType == RawNodeData::eBone ||
				pData->eType == RawNodeData::eNormal)
			{
				if (pSkeleton->GetBoneCount() == 0)//Root Bone
				{
					pSkeleton->AddBone(nullptr,
						pData->strName.CString(),
						pData->matInit);
				}

				if (pData->aChild[u]->eType == RawNodeData::eBone ||
					pData->eType == RawNodeData::eNormal)
				{
					pSkeleton->AddBone(pData->strName.CString(),
						pData->aChild[u]->strName.CString(), pData->aChild[u]->matInit);
				}
			}
			bResult = bResult && ConvertSkeleton(pData->aChild[u], pSkeleton);
		}
		return True;
	}

	IAppMesh* ConvertRigidMeshData(RawNodeData* pData)
	{
		UInt32 uVertexCount = pData->f3VertexPosition.ElementCount();
		Boolean bConvertNormal = pData->f3VertexNormal.ElementCount() == uVertexCount;
		Boolean bConvertTangent = pData->f4VertexTangent.ElementCount() == uVertexCount;
		Boolean bConvertColor = pData->f4VertexColor.ElementCount() == uVertexCount;
		Boolean bConvertTexCoord = pData->f2VertexTexCoord.ElementCount() == uVertexCount;
		Boolean bMaterialID = pData->uMaterialID.ElementCount() == pData->uIndex.ElementCount() / 3;
		UInt32 SubMeshIndexCount[MAX_SUBMESH_COUNT] = { 0 };

		UInt32 uVertexCountNoRepeat = 0;

		IAppMesh::VertexRigid* pVertex = new IAppMesh::VertexRigid[uVertexCount];
		UInt32* pVertexIndex = new UInt32[uVertexCount];
		UInt32* pIndex = new UInt32[pData->uIndex.ElementCount()];

		//Will Remove Repeated Vertex
		for (UInt32 u = 0; u < uVertexCount; u++)
		{
			IAppMesh::VertexRigid newVertex;
			newVertex.f4Position = Math::Combine(pData->f3VertexPosition[u], 0.0f);
			newVertex.f4Normal = Math::Combine(bConvertNormal ? pData->f3VertexNormal[u] : Float3(0, 0, 0), 1.0f);
			newVertex.f4Tangent = bConvertTangent ? pData->f4VertexTangent[u] : Float4(0, 0, 0, 1);
			if (Math::Abs(newVertex.f4Tangent[3]) < 0.99f)//Not Eq to +-1.0
				newVertex.f4Tangent[3] = 1.0f;
			newVertex.f4Color = bConvertColor ? pData->f4VertexColor[u] : Float4(1, 1, 1, 1);
			newVertex.f2TexCoord0 = bConvertTexCoord ? pData->f2VertexTexCoord[u] : Float2(0, 0);

			UInt32 uDestVertex = 0;
			for (; uDestVertex < uVertexCountNoRepeat; uDestVertex++)
			{
				if (newVertex == pVertex[uDestVertex])
				{
					break;
				}
			}

			if (uDestVertex >= uVertexCountNoRepeat)
			{
				pVertex[uVertexCountNoRepeat] = newVertex;
				uVertexCountNoRepeat++;
			}
			pVertexIndex[u] = uDestVertex;
		}

		for (UInt32 u = 0; u < pData->uIndex.ElementCount(); u += 3)
		{
			if (bMaterialID)
				SubMeshIndexCount[pData->uMaterialID[u / 3]] += 3;
			else
				SubMeshIndexCount[0] += 3;

			UInt32 uTri0 = pData->uIndex[u];
			UInt32 uTri1 = pData->uIndex[u + 1];
			UInt32 uTri2 = pData->uIndex[u + 2];

			pIndex[u] = pVertexIndex[uTri0];
			pIndex[u + 1] = pVertexIndex[uTri1];
			pIndex[u + 2] = pVertexIndex[uTri2];

			if (!bConvertNormal)
			{
				Float4 n = Math::Combine(Math::ComputeNormal(
					Math::Split(pVertex[uTri0].f4Position),
					Math::Split(pVertex[uTri1].f4Position),
					Math::Split(pVertex[uTri2].f4Position)), 0.0f);

				pVertex[uTri0].f4Normal = Math::Normalize(pVertex[uTri0].f4Normal + n);
				pVertex[uTri1].f4Normal = Math::Normalize(pVertex[uTri1].f4Normal + n);
				pVertex[uTri2].f4Normal = Math::Normalize(pVertex[uTri2].f4Normal + n);
			}

			if (!bConvertTangent)
			{
				Float4 t = Math::ComputeTangent(
					Math::Split(pVertex[uTri0].f4Position),
					Math::Split(pVertex[uTri1].f4Position),
					Math::Split(pVertex[uTri2].f4Position),
					pVertex[uTri0].f2TexCoord0,
					pVertex[uTri1].f2TexCoord0,
					pVertex[uTri2].f2TexCoord0);

				Float3 t3 = Float3(t[0], t[1], t[2]);
				Float3 blendT = Float3(pVertex[uTri0].f4Tangent[0],
					pVertex[uTri0].f4Tangent[1],
					pVertex[uTri0].f4Tangent[2]);
				blendT = Math::Normalize(blendT + t3);
				pVertex[uTri0].f4Tangent = Float4(blendT[0], blendT[1], blendT[2], t[3]);

				blendT = Float3(pVertex[uTri1].f4Tangent[0],
					pVertex[uTri1].f4Tangent[1],
					pVertex[uTri1].f4Tangent[2]);
				blendT = Math::Normalize(blendT + t3);
				pVertex[uTri1].f4Tangent = Float4(blendT[0], blendT[1], blendT[2], t[3]);

				blendT = Float3(pVertex[uTri2].f4Tangent[0],
					pVertex[uTri2].f4Tangent[1],
					pVertex[uTri2].f4Tangent[2]);
				blendT = Math::Normalize(blendT + t3);
				pVertex[uTri2].f4Tangent = Float4(blendT[0], blendT[1], blendT[2], t[3]);
			}
		}

		UInt32 SubMeshCount = 0;
		UInt32 SubMeshOffset[MAX_SUBMESH_COUNT] = { 0 };
		UInt32 SubMeshSize[MAX_SUBMESH_COUNT] = { 0 };
		for (UInt32 u = 0; u < MAX_SUBMESH_COUNT; u++)
		{
			SubMeshOffset[SubMeshCount] = SubMeshCount > 0 ?
				SubMeshOffset[SubMeshCount - 1] + SubMeshIndexCount[SubMeshCount - 1] : 0;
			SubMeshSize[SubMeshCount] = SubMeshIndexCount[u];
			SubMeshCount += SubMeshIndexCount[u] > 0 ? 1 : 0;
		}

		IAppMesh *pNewMesh = IAppMesh::Create(pData->strName.CString(), IAppMesh::EAPPMT_Rigid, True,
			uVertexCountNoRepeat, pData->uIndex.ElementCount(), SubMeshCount);
		if (pNewMesh == nullptr)
			return nullptr;

		pNewMesh->SetVertices(uVertexCountNoRepeat, pVertex);
		pNewMesh->SetIndices(pData->uIndex.ElementCount(), (UInt8*)pIndex);
		for (UInt32 u = 0; u < SubMeshCount; u++)
		{
			pNewMesh->SetSubMesh(u, SubMeshOffset[u], SubMeshSize[u]);
		}

		delete[] pVertex;
		delete[] pIndex;
		delete[] pVertexIndex;

		return pNewMesh;
	}

	IAppMesh* ConvertSkinMeshData(RawNodeData* pData)
	{
		UInt32 uVertexCount = pData->f3VertexPosition.ElementCount();
		Boolean bConvertNormal = pData->f3VertexNormal.ElementCount() == uVertexCount;
		Boolean bConvertTangent = pData->f4VertexTangent.ElementCount() == uVertexCount;
		Boolean bConvertColor = pData->f4VertexColor.ElementCount() == uVertexCount;
		Boolean bConvertTexCoord = pData->f2VertexTexCoord.ElementCount() == uVertexCount;
		Boolean bMaterialID = pData->uMaterialID.ElementCount() == pData->uIndex.ElementCount() / 3;
		UInt32 SubMeshIndexCount[MAX_SUBMESH_COUNT] = { 0 };

		UInt32 uVertexCountNoRepeat = 0;

		IAppMesh::VertexSkin* pVertex = new IAppMesh::VertexSkin[uVertexCount];
		UInt32* pVertexIndex = new UInt32[uVertexCount];
		UInt32* pIndex = new UInt32[pData->uIndex.ElementCount()];

		//Will Remove Repeated Vertex
		for (UInt32 u = 0; u < uVertexCount; u++)
		{
			IAppMesh::VertexSkin newVertex;
			newVertex.f4Position = Math::Combine(pData->f3VertexPosition[u], 0.0f);
			newVertex.f4Normal = Math::Combine(bConvertNormal ? pData->f3VertexNormal[u] : Float3(0, 0, 0), 0.0f);
			newVertex.f4Tangent = bConvertTangent ? pData->f4VertexTangent[u] : Float4(0, 0, 0, 1);
			if (Math::Abs(newVertex.f4Tangent[3]) < 0.99f)//Not Eq to +-1.0
				newVertex.f4Tangent[3] = 1.0f;
			newVertex.f4Color = bConvertColor ? pData->f4VertexColor[u] : Float4(1, 1, 1, 1);
			newVertex.f2TexCoord0 = bConvertTexCoord ? pData->f2VertexTexCoord[u] : Float2(0, 0);

			newVertex.uRefBoneIndex[0] = pData->VertexBlendData[u].uBlendIndex[0];
			newVertex.uRefBoneIndex[1] = pData->VertexBlendData[u].uBlendIndex[1];
			newVertex.uRefBoneIndex[2] = pData->VertexBlendData[u].uBlendIndex[2];
			newVertex.uRefBoneIndex[3] = pData->VertexBlendData[u].uBlendIndex[3];

			newVertex.fRefBoneWeight[0] = pData->VertexBlendData[u].fBlendWeight[0];
			newVertex.fRefBoneWeight[1] = pData->VertexBlendData[u].fBlendWeight[1];
			newVertex.fRefBoneWeight[2] = pData->VertexBlendData[u].fBlendWeight[2];
			newVertex.fRefBoneWeight[3] = pData->VertexBlendData[u].fBlendWeight[3];

			UInt32 uDestVertex = 0;
			for (; uDestVertex < uVertexCountNoRepeat; uDestVertex++)
			{
				if (newVertex == pVertex[uDestVertex])
				{
					break;
				}
			}

			if (uDestVertex >= uVertexCountNoRepeat)
			{
				pVertex[uVertexCountNoRepeat] = newVertex;
				uVertexCountNoRepeat++;
			}
			pVertexIndex[u] = uDestVertex;
		}

		for (UInt32 u = 0; u < pData->uIndex.ElementCount(); u += 3)
		{
			if (bMaterialID)
				SubMeshIndexCount[pData->uMaterialID[u / 3]] += 3;
			else
				SubMeshIndexCount[0] += 3;

			UInt32 uTri0 = pData->uIndex[u];
			UInt32 uTri1 = pData->uIndex[u + 1];
			UInt32 uTri2 = pData->uIndex[u + 2];

			pIndex[u] = pVertexIndex[uTri0];
			pIndex[u + 1] = pVertexIndex[uTri1];
			pIndex[u + 2] = pVertexIndex[uTri2];

			if (!bConvertNormal)
			{
				Float4 n = Math::Combine(Math::ComputeNormal(
					Math::Split(pVertex[uTri0].f4Position),
					Math::Split(pVertex[uTri1].f4Position),
					Math::Split(pVertex[uTri2].f4Position)), 0.0f);

				pVertex[uTri0].f4Normal = Math::Normalize(pVertex[uTri0].f4Normal + n);
				pVertex[uTri1].f4Normal = Math::Normalize(pVertex[uTri1].f4Normal + n);
				pVertex[uTri2].f4Normal = Math::Normalize(pVertex[uTri2].f4Normal + n);
			}

			if (!bConvertTangent)
			{
				Float4 t = Math::ComputeTangent(
					Math::Split(pVertex[uTri0].f4Position),
					Math::Split(pVertex[uTri1].f4Position),
					Math::Split(pVertex[uTri2].f4Position),
					pVertex[uTri0].f2TexCoord0,
					pVertex[uTri1].f2TexCoord0,
					pVertex[uTri2].f2TexCoord0);

				Float3 t3 = Float3(t[0], t[1], t[2]);
				Float3 blendT = Float3(pVertex[uTri0].f4Tangent[0],
					pVertex[uTri0].f4Tangent[1],
					pVertex[uTri0].f4Tangent[2]);
				blendT = Math::Normalize(blendT + t3);
				pVertex[uTri0].f4Tangent = Float4(blendT[0], blendT[1], blendT[2], t[3]);

				blendT = Float3(pVertex[uTri1].f4Tangent[0],
					pVertex[uTri1].f4Tangent[1],
					pVertex[uTri1].f4Tangent[2]);
				blendT = Math::Normalize(blendT + t3);
				pVertex[uTri1].f4Tangent = Float4(blendT[0], blendT[1], blendT[2], t[3]);

				blendT = Float3(pVertex[uTri2].f4Tangent[0],
					pVertex[uTri2].f4Tangent[1],
					pVertex[uTri2].f4Tangent[2]);
				blendT = Math::Normalize(blendT + t3);
				pVertex[uTri2].f4Tangent = Float4(blendT[0], blendT[1], blendT[2], t[3]);
			}
		}

		UInt32 SubMeshCount = 0;
		UInt32 SubMeshOffset[MAX_SUBMESH_COUNT] = { 0 };
		UInt32 SubMeshSize[MAX_SUBMESH_COUNT] = { 0 };
		for (UInt32 u = 0; u < MAX_SUBMESH_COUNT; u++)
		{
			SubMeshOffset[SubMeshCount] = SubMeshCount > 0 ?
				SubMeshOffset[SubMeshCount - 1] + SubMeshIndexCount[SubMeshCount - 1] : 0;
			SubMeshSize[SubMeshCount] = SubMeshIndexCount[u];
			SubMeshCount += SubMeshIndexCount[u] > 0 ? 1 : 0;
		}

		IAppMesh* pNewMesh = IAppMesh::Create(pData->strName.CString(), IAppMesh::EAPPMT_Skin,
			True, uVertexCountNoRepeat, pData->uIndex.ElementCount(), SubMeshCount);
		if (pNewMesh == nullptr)
			return nullptr;

		pNewMesh->SetVertices(uVertexCountNoRepeat, pVertex);
		pNewMesh->SetIndices(pData->uIndex.ElementCount(), (UInt8*)pIndex);
		for (UInt32 u = 0; u < SubMeshCount; u++)
		{
			pNewMesh->SetSubMesh(u, SubMeshOffset[u], SubMeshSize[u]);
		}

		for (UInt32 u = 0; u < pData->BoneName.ElementCount(); u++)
		{
			pNewMesh->AddBindingBone(pData->BoneName[u].CString(), pData->BoneLocal2Bone[u]);
		}
		delete[] pVertex;
		delete[] pIndex;
		delete[] pVertexIndex;

		return pNewMesh;
	}

	IAppAnimationClip*	ConvertAnimationData(RawAnimation* pData)
	{
		IAppAnimationClip* pAnim = IAppAnimationClip::Create(pData->strName.CString(), pData->fSampleRate);
		if (pAnim == nullptr)
		{
			return nullptr;
		}

		for (UInt32 u = 0; u < pData->arrAnimStack.ElementCount(); u++)
		{
			Float1 fDeltaTime = pData->fAnimTime / pData->arrAnimStack[u]->arrTransform.ElementCount();
			for (UInt32 uSample = 0; uSample < pData->arrAnimStack[u]->arrTransform.ElementCount(); uSample++)
			{
				pAnim->AddSample(pData->arrAnimStack[u]->strNodeName.CString(),
					fDeltaTime * uSample,
					pData->arrAnimStack[u]->arrTransform[uSample]);
			}
		}

		return pAnim;
	}
}