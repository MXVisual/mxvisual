#pragma once
#ifndef _FBX_MODEL_IMPORTER_
#define _FBX_MODEL_IMPORTER_
#include "MXCImporter.h"
#include "MXString.h"
#include "MXCEditorPanel.h"

using namespace MXVisual;

class FbxModelImporter 
	: public ICImporter
	, public ICSupportedEditorPropertyPanel
{
public:
	FbxModelImporter();

	Void	SetImportFileName(const Char* szFileName) override;
	Void	SetImportTargetPath(const Char* szTargetPath) override;
	Boolean Import() override;

	Void	OnPanelActived(IEditorPropertyPanel& Panel) override;
	Void	OnPanelChanged(const IEditorProperty* pProperty) override;

protected:
	String m_strFileName;
	String m_strTargetPath;

	Boolean m_bImportMesh;
	Boolean m_bImportSkeleton;
	Boolean m_bImportAnimation;
	Float1	m_fImportScale;
};
#endif