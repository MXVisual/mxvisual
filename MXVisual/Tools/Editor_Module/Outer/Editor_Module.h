#pragma once
#ifndef _EDITOR_MODULE_
#define _EDITOR_MODULE_
#include "MXString.h"
#include "MXArray.h"
#include "MXCComponent.h"
#include "MXCImporter.h"

#if defined(DYNAMIC_LINK)
#define DLL_DECLARE DLL_EXPORT
//#define DLL_DECLARE DLL_IMPORT
#else
#define DLL_DECLARE
#endif

namespace Editor_Importer
{
	DLL_DECLARE
	struct Description
	{
		MXVisual::Char	Name[64];
		MXVisual::UInt8	SupportedFormatCount;
		MXVisual::Char	SupportedFormat[16][64];
	};

	DLL_DECLARE
	MXVisual::Void			Enumerate(MXVisual::Array<Description>& arrImporter);

	DLL_DECLARE
	MXVisual::ICImporter*	Create(const MXVisual::Char* szImporterName);
};
/*
namespace Editor_Exporter
{
	DLL_DECLARE
	struct Description
	{
		MXVisual::Char	Name[64];
		MXVisual::Char	SourceFormat[64];
		MXVisual::UInt8	SupportedFormatCount;
		MXVisual::Char	SupportedFormat[16][64];
	};
	DLL_DECLARE
	MXVisual::Void			Enumerate(MXVisual::Array<Description>& arrExporter);

	DLL_DECLARE
	MXVisual::ICExporter*	Create(const MXVisual::Char* szExporterName);
};
*/
#endif

