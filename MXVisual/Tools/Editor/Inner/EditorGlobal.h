#pragma once
#ifndef _EDITOR_GLOBAL_
#define _EDITOR_GLOBAL_
#include "MXCObject.h"
#include "MXCEditorGraphics.h"
#include "MXCScene.h"
#include "MXCComponent.h"
#include "MXFile.h"
#include "MXCSerializer.h"

//eg_ =>> editorglobal

using namespace MXVisual;
IGTexture* eg_LoadTGATexture(const Char* szTextureFileName);
extern const Char* DragDropAssetID;
extern const Char* DragDropSceneObjectID;

enum AssetType
{
	EUnknown,
	EFolder,
	EScene,
	EMesh,
	ESkeleton,
	EMaterial,
	EMaterialInstance,
	ETexture,
	EAnimation,
	EFont,
	EAudio,
	EFileTypeCount,
	EHiden = EFileTypeCount
};
const char* AssetTypeEnumToString(AssetType eFileType);
Float4 AssetTypeEnumColor(AssetType eFileType);
AssetType AssetTypeStringToEnum(const char* szFileType);
const char* AssetTypeFormatString(AssetType eFileType);
AssetType IdentifyAssetType(const String& strFileName);

struct AssetInfo
{
	String strName;
	AssetType eType;
	const struct FolderInfo* pParentFolderInfo;
};

struct FolderInfo
{
	String strName;
	String strPath;
	Array<FolderInfo*>	arrChildFolder;
	Array<AssetInfo>	arrAssetInfo;

	~FolderInfo();
};

void eg_ResetEditorGraphics();
void eg_FlushEditorGraphics();
void eg_ReleaseEditorGraphics();
IEditorGraphics* eg_GetEditorGraphics();

void eg_SelectObject(const Char* szObjName);
ICObject* eg_GetSelectedObject();

enum CameraViewType
{
	EPerspective,
	ELeft,
	ETop,
	EFront
};

class ITransformEditControl
{
protected:
	static Float4 Color_AXIS_X;
	static Float4 Color_AXIS_Y;
	static Float4 Color_AXIS_Z;
	static Float4 Color_Selected;
	static Float1 IntersectionThresold;

	enum EditBit
	{
		EB_X = 0x1,
		EB_Y = 0x2,
		EB_Z = 0x4,

		EB_XY = EB_X | EB_Y,
		EB_YZ = EB_Y | EB_Z, 
		EB_XZ = EB_X | EB_Z,

		EB_XYZ = EB_X | EB_Y | EB_Z
	};
	unsigned	m_editBit;
	bool		m_bEditing;
	Float2 m_f2CheckPoint;

	ITransformEditControl()
		: m_editBit(0)
		, m_bEditing(false)
	{}
public:
	virtual void Reset(ICObject* pObj, Boolean bLocal, Float1 fScale) = 0;
	virtual Boolean Transform(
		const Matrix4& WorldToClip, 
		const Matrix4& WClipToSreen,
		const Float2& mousePos, 
		Boolean bDoModify) = 0;
	virtual void Render() = 0;

	Float1 ComputeDragMotion2D(
		const Float2& linePoint0,
		const Float2& linePoint1,
		const Float2& mousePos);
	Float1 ComputeDragMotion3D(
		const Float3& linePoint0,
		const Float3& linePoint1,
		const Matrix4& WorldToClip,
		const Matrix4& WClipToSreen,
		const Float2& mousePos);
	bool PointAtLine(const Float2& p, const Float2& lp0, const Float2& lp1, Float1 fThresold);
};

enum TransformEditType
{
	ENone,
	ETranslation,
	ERotation,
	EScale,
};
//When Control Change Will Give A UI Animation
ITransformEditControl* eg_GetEditControl(TransformEditType eType);


#endif