#pragma once
#ifndef _EDITOR_ASSET_EDITOR_
#define _EDITOR_ASSET_EDITOR_
#include "EditorUIManager.h"
#include "EditorGlobal.h"
#include "PropertyPage.h"
#include "EditorMaterialPanel.h"

struct EditorAssetDocument
{
	Boolean		bTabSelected;
	Boolean		bEditedButNotSaved;
	String		strName;
	String		strAbsName;
	AssetType	eType;
};

class EditorAssetEditor
{
public:
	EditorAssetEditor();
	~EditorAssetEditor();

	void Layout();

	void OpenAsset(const AssetInfo* pAsset);
protected:
	EditorMaterialPanel			m_MaterialPanel;
	EditorMaterialInstancePanel m_MaterialInstancePanel;

	Array<EditorAssetDocument> m_arrAssetDocs;
};

#endif