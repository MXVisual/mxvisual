#include "EditorSceneTree.h"
#include "EditorGlobal.h"
#include "imgui.h"

#include "MXCScene.h"
#include "MXCObject.h"
#include "MXCComponent.h"

#include "MXAsset.h"
#include "MXCCamera.h"

EditorSceneTree::EditorSceneTree()
	: m_szMenuName("SceneTreeEditMenu")
{
}
EditorSceneTree::~EditorSceneTree()
{

}

void EditorSceneTree::Layout()
{
	static ImGuiTextFilter filter;

	ImGui::SetNextWindowSize(ImVec2(256, 512), ImGuiCond_FirstUseEver);
	ImGui::Begin("Scene Tree Panel");
	{
		filter.Draw("Filter");
		ImGui::Separator();

		ImGui::BeginChild("");
		{
			CScene* pScene = CScene::ActivedScene();
			ImGui::SetNextTreeNodeOpen(true, ImGuiCond_Always);
			if (ImGui::TreeNodeEx("Scene", ImGuiTreeNodeFlags_DefaultOpen))
			{
				if (ImGui::IsItemClicked(ImGuiMouseButton_Left))
				{
					eg_SelectObject(nullptr);
				}

				if (filter.IsActive())
					;

				for (UInt32 u = 0; u < pScene->GetRootObjectCount(); u++)
				{
					LayoutSceneObject(pScene->GetRootObject(u));
				}

				ImGui::TreePop();
			}

			if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
			{
				ICObject* pObj = eg_GetSelectedObject();

				ImGui::SetDragDropPayload(DragDropSceneObjectID, &pObj, sizeof(pObj));
				ImGui::Text(pObj->GetName());
				ImGui::EndDragDropSource();
			}
		}
		ImGui::EndChild();

		ImGui::OpenPopupOnItemClick(m_szMenuName, ImGuiPopupFlags_MouseButtonRight);
	}

	Layout_EditMenu();

	ImGui::End();
}

void EditorSceneTree::LayoutSceneObject(ICObject* pObj)
{
	ASSERT_IF_FAILED(pObj != nullptr);
	Boolean bSelected = eg_GetSelectedObject() == nullptr ? false : eg_GetSelectedObject() == pObj;

	ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;
	flags |= bSelected ? ImGuiTreeNodeFlags_Selected : 0;
	flags |= pObj->GetChildCount() == 0 ? ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Bullet : 0;

	bool bOpened = ImGui::TreeNodeEx(pObj->GetName(), flags);
	if (ImGui::IsItemClicked(ImGuiMouseButton_Left) || ImGui::IsItemClicked(ImGuiMouseButton_Right))
		eg_SelectObject(pObj->GetName());

	if (bOpened)
	{
		for (UInt32 u = 0; u < pObj->GetChildCount(); u++)
		{
			LayoutSceneObject(pObj->GetChild(u));
		}
		ImGui::TreePop();
	}
}

void EditorSceneTree::Layout_EditMenu()
{
	if (ImGui::BeginPopupContextItem(m_szMenuName))
	{
		if (eg_GetSelectedObject() != nullptr)
		{
			if (ImGui::MenuItem("Delete"))
			{
				ICObject* pObj =  CScene::ActivedScene()->RemoveObject(eg_GetSelectedObject()->GetUniqueID());
				MX_DELETE(pObj);
				eg_SelectObject(nullptr);
			}
			ImGui::Separator();
		}

		if (ImGui::MenuItem("Add Object3D"))
		{
			AddSceneObject(new CObject("Object3D"));
		}
		if (ImGui::MenuItem("Add Object2D"))
		{
			AddSceneObject(new CObject2D("Object2D"));
		}
		if (ImGui::MenuItem("Add Camera"))
		{
			AddSceneObject(new CCamera("Camera"));
		}
		ImGui::Separator();
		if (ImGui::MenuItem("Add DirectionLight"))
		{
			ICObject* pNewObj = new CObject("DirectionLight");
			pNewObj->AddComponent(ICComponent::Create(DefaultResource::COMPONENT_NAME_DIRECTION_LIGHT));
			AddSceneObject(pNewObj);
		}
		if (ImGui::MenuItem("Add PointLight"))
		{
			ICObject* pNewObj = new CObject("PointLight");
			pNewObj->AddComponent(ICComponent::Create(DefaultResource::COMPONENT_NAME_POINT_LIGHT));
			AddSceneObject(pNewObj);
		}
		if (ImGui::MenuItem("Add SpotLight"))
		{
			ICObject* pNewObj = new CObject("SpotLight");
			pNewObj->AddComponent(ICComponent::Create(DefaultResource::COMPONENT_NAME_SPOT_LIGHT));
			AddSceneObject(pNewObj);
		}

		ImGui::EndPopup();
	}
}

void EditorSceneTree::AddSceneObject(ICObject* pObj)
{
	if (eg_GetSelectedObject() != nullptr)
	{
		eg_GetSelectedObject()->AddChild(pObj);
	}
	else
	{
		CScene* pScene = CScene::ActivedScene();
		pScene->RegisterObject(pObj);
	}
}
