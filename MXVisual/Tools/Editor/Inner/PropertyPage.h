﻿#pragma once
#ifndef _PROPERTY_PAGE_
#define _PROPERTY_PAGE_
#include "MXCType.h"
#include "EditorGlobal.h"
#include "MXCObject.h"
#include "MXCComponent.h"
#include "MXCEditorPanel.h"

class wxPropertyGrid;

using namespace MXVisual;
class PropertyPanel : public IEditorPropertyPanel
{
protected:
	static const Char*				szPropertyPanelMenuName;
	ICSupportedEditorPropertyPanel* m_pPropertyObject;
	ICSupportedEditorGraphics*		m_pSupportedRender;
	const Char*						m_szName;

	Array<Boolean>	m_arrGroupStackOpenedFlag;
public:
	PropertyPanel();
	~PropertyPanel();

	void SetPropertyObject(const Char* szName, ICSupportedEditorPropertyPanel* pSupportedObj);
	void SetSupportedRender(ICSupportedEditorGraphics* pSupportedRender);
	void Layout(Boolean& bDeleted);
	void Layout();

	virtual Void OnEditorRender(MXVisual::IEditorGraphics& Graphics) {};

	Void BeginGroup(
		const Char* szName) override;
	Void EndGroup() override;

	Void AddPropertyString(
		const Char* szName,
		const Char* szValue) override;

	Void AddPropertyLongString(
		const Char* szName,
		const Char* szValue) override;

	Void AddPropertyFile(
		const Char* szName,
		const Char* szValue,
		const Char* szFilter = "*.*")override;

	Void AddPropertyColor(
		const Char* szName,
		const Float4& color = Float4(0, 0, 0, 1)) override;

	Void AddPropertyTrigger(
		const Char* szName,
		const Boolean bValue)override;

	Void AddPropertyEnumeration(
		const Char* szName,
		Array<String>& arrEnumLabel,
		Array<SInt32>& arrEnumValue,
		SInt32 nValue)override;

	Void AddPropertyFloat1(
		const Char* szName,
		const Float1 fValue = 0.0f,
		const Float1 fStep = 0.01f,
		const Float1 fMin = 0.0f,
		const Float1 fMax = 1.0f) override;

	Void AddPropertyFloat2(
		const Char* szName,
		const Float2& f2Value = Float2(0, 0),
		const Float1 fStep = 0.01f,
		const Float1 fMin = 0.0f,
		const Float1 fMax = 1.0f)override;

	Void AddPropertyFloat3(
		const Char* szName,
		const Float3& f3Value = Float3(0.0f, 0.0f, 0.0f),
		const Float1 fStep = 0.01f,
		const Float1 fMin = 0.0f,
		const Float1 fMax = 1.0f) override;

	Void AddPropertyFloat4(
		const Char* szName,
		const Float4& f4Value = Float4(0.0f, 0.0f, 0.0f, 0.0f),
		const Float1 fStep = 0.01f,
		const Float1 fMin = 0.0f,
		const Float1 fMax = 1.0f) override;
};

#endif