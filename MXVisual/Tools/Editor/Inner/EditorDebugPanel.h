#pragma once
#ifndef _EDITOR_DEBUG_PANEL_
#define _EDITOR_DEBUG_PANEL_
#include "EditorUIManager.h"
#include "MXArray.h"
#include "MXString.h"

class EditorDebugPanel 
{
public:
	EditorDebugPanel();
	~EditorDebugPanel();

	void Layout();

	void LogMessage(const char* szMessage);
	void LogError(const char* szMessage);
	void LogWarning(const char* szMessage);

protected:
	enum LogType
	{
		ELT_MESSAGE,
		ELT_WARNING,
		ELT_ERROR,
	};

	struct LogData
	{
		LogType eType;
		String	strText;
		String	strTime;
	};
	Array<LogData> m_arrLog;
	SInt32	m_nSelectLog;
};

#endif