#include "EditorMainFrame.h"
#include "EditorGlobal.h"
#include "MXCCamera.h"
#include "MXCScene.h"
#include "MXCSceneRender.h"
#include "MXTimer.h"
#include "MXSystem.h"
#include "MXAsset.h"
#include "MXGraphicsRenderer.h"
#include "MXPhysicsSimulator.h"

#include "EditorDebugPanel.h"
#include "EditorSceneTree.h"
#include "EditorPropertyPanel.h"
#include "EditorAssetBrowser.h"
#include "EditorAssetEditor.h"

#include "MXThread.h"

#if PLATFORM == PLATFORM_WINDOWS
#include "windows.h"
#endif

#include "imgui.h"
#include <functional>

namespace PhysicsDebug
{
	Void FDrawLine(const Float3& p0, const Float3& p1, const Float4& color)
	{
		eg_GetEditorGraphics()->ChangeColor(color);
		eg_GetEditorGraphics()->RenderLine(p0, p1);
	}
	Void FDrawText3D(const Float3& p0, const Char*)
	{

	}
}

namespace EditorMainFrame
{
	Boolean m_bRenderGrid;
	Boolean m_bRenderIcon;

	Boolean m_bShowPanels;
	Boolean m_bShowDebugPanel;
	Boolean m_bShowSceneTreePanel;
	Boolean m_bShowPropertyPanel;
	Boolean m_bShowAssetBrowser;

	Boolean m_bShowStatistics;

	EditorDebugPanel*		m_pDebugPanel;
	EditorSceneTree*		m_pSceneTreePanel;
	EditorPropertyPanel*	m_pPropertyPanel;
	EditorAssetBrowser*		m_pAssetBrowser;
	EditorAssetEditor*		m_pAssetEditor;

	Boolean				m_bShouldRefreshAssetInfo;
	String				m_strProjectPath;
	String				m_strScenePath;
	FolderInfo*			m_pProjectInfo;

	IGRenderWindow* m_pRWindow;
	CScene*		m_pScene;
	CameraViewType m_eCVType;
	CCamera*	m_pEditModeCamera;
	Float1		m_fMoveSpeed;
	Float1		m_fRotateSpeed;
	TransformEditType m_eEditType;
	ITransformEditControl* m_pEditControl;
	Boolean		m_bEditLocal;

#define EDITOR_STATE_NONE			0
#define EDITOR_STATE_SCENE_LOADING	1
	Atomic<SInt16>	m_atomEditorState(EDITOR_STATE_NONE);

	Chain<ThreadTaskStatistics> m_stats;

	enum class IconType : UInt8
	{
		E3DObject = 0,
		ECamera,
		EDirectionLight,
		EPointLight,
		ESpotLight,
		ECount
	};
	IGPrimitive*	m_p3DObjectIconPrimitive;
	IGMaterial*		m_p3DObjectIconMaterial;
	IGMaterialParameterBuffer* m_arr3DObjectIconParameterBlock[(UInt8)IconType::ECount];
	IGPrimitiveTransformBuffer* m_arr3DObjectIconTransformBuffer[(UInt8)IconType::ECount];
	Array<Matrix4>	m_arr3DObjectIconTransform[(UInt8)IconType::ECount];
	IGTexture*		m_arr3DObjectIconTexture[(UInt8)IconType::ECount];
	const Char* IconTextureNames[] = {
		"./Icons/SceneIcon/IconObject.tga",
		"./Icons/SceneIcon/IconCamera.tga",
		"./Icons/SceneIcon/IconDirectionLight.tga",
		"./Icons/SceneIcon/IconPointLight.tga",
		"./Icons/SceneIcon/IconSpotLight.tga",
	};

	Boolean Init3DObjectMaterial()
	{
		IGMaterial::Description desc;
		desc.strUniqueName = "3DObjectIcon_Material";
		desc.bCanCache = False;
		desc.eDomain = MaterialDomain::EWorldSpace;
		desc.eBlendMode = MaterialBlendMode::ETranslucency;
		desc.eFaceType = MaterialFaceType::EFrontface;
		desc.eShadingModel = MaterialShadingModel::ENolighting;
		desc.arrMaterialParams.Add(
			IGMaterialParam::Description(
				IGMaterialParam::EMPT_Texture2D, "BaseTex", ""
			));

		desc.arrMaterialFunction[(const UInt32)MaterialFunctionType::ESurfaceParams] =
			"float4 color = Tex2D_Sample(BaseTex, input.TexCoord0);\n\
				OUTPUT_EMISSIVE(PCFunc_Color_GammaCorrect(color.rgb, 2.2f) * PCFunc_Color_GammaCorrect(input.VertexColor.rgb, 2.2f));\n\
				OUTPUT_OPACITY(color.a * input.VertexColor.a);";

		m_p3DObjectIconMaterial = GraphicsResource::CreateMaterial(desc);
		return m_p3DObjectIconMaterial != nullptr;
	}
	Boolean Init3DObjectPrimitive()
	{
		IGPrimitive::Description desc;
		desc.bDynamic = False;
		desc.eType = IGPrimitive::EPrimT_Rigid;
		desc.uVertexCount = 4;
		desc.uIndexCount = 6;
		m_p3DObjectIconPrimitive = GraphicsResource::CreatePrimitive(desc);
		if (m_p3DObjectIconPrimitive == nullptr)
			return False;

		GVertexRigid v[4];
		v[0].f4Position = Float4(-1.0f, 1.0f, 0.0f, 0.0f);
		v[0].f2TexCoord0 = Float2(0.0f, 0.0f);
		v[0].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);

		v[1].f4Position = Float4(1.0f, 1.0f, 0.0f, 0.0f);
		v[1].f2TexCoord0 = Float2(1.0f, 0.0f);
		v[1].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);

		v[2].f4Position = Float4(-1.0f, -1.0f, 0.0f, 0.0f);
		v[2].f2TexCoord0 = Float2(0.0f, 1.0f);
		v[2].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);

		v[3].f4Position = Float4(1.0f, -1.0f, 0.0f, 0.0f);
		v[3].f2TexCoord0 = Float2(1.0f, 1.0f);
		v[3].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);

		m_p3DObjectIconPrimitive->UpdateVertexBuffer(v, ARRAY_ELEMENT_COUNT(v));

		UInt32 i[6] = { 0,1,2,1,3,2 };
		m_p3DObjectIconPrimitive->UpdateIndexBuffer(i, ARRAY_ELEMENT_COUNT(i));

		return True;
	}
	Boolean Init3DObjectRenderData()
	{
		if (!Init3DObjectMaterial())
			return False;

		if (!Init3DObjectPrimitive())
			return False;

		for (UInt32 uIcon = 0; uIcon < (UInt32)IconType::ECount; uIcon++)
		{
			IGMaterialParameterBuffer* pBuffer =
				GraphicsResource::CreateMaterialParameterBuffer(m_p3DObjectIconMaterial);

			IGTexture* pTex = eg_LoadTGATexture(IconTextureNames[uIcon]);
			if (pBuffer == nullptr || pTex == nullptr)
				return False;

			pBuffer->GetParamByName("BaseTex")->SetTexture2D(pTex);
			m_arr3DObjectIconTexture[uIcon] = pTex;

			m_arr3DObjectIconParameterBlock[uIcon] = pBuffer;

			IGPrimitiveTransformBuffer* pTBuffer =
				GraphicsResource::CreatePrimitiveTransformBuffer(True);
			m_arr3DObjectIconTransformBuffer[uIcon] = pTBuffer;
		}

		return True;
	}

	FModalDlgYesNoCallBack m_ModalDlgContentCallback;

	void ResetEditCamera(CameraViewType eType)
	{
		const Float1 fZNear = 0.1f;
		const Float1 fZFar = 2000.0f;
		MXVisual::Float2 wh((Float1)m_pRWindow->GetWidth(), (Float1)m_pRWindow->GetHeight());
		wh /= 4.0f;
		m_pEditModeCamera->SetRotation(MXVisual::Float3(0.0f, 0.0f, 0.0f), MXVisual::False);

		switch (eType)
		{
		case EPerspective://45
			m_pEditModeCamera->SetPerspective(MXVisual::Constant_PI / 2, wh[0] / wh[1], fZNear, fZFar);
			m_pEditModeCamera->SetPosition(MXVisual::Float3(0.0f, 40.0f, -40.0f), MXVisual::False);
			m_pEditModeCamera->SetRotation(MXVisual::Float3(0.0f, 0.0f, 0.0f), MXVisual::False);
			m_pEditModeCamera->TransRotate(MXVisual::Math::AXIS_X, MXVisual::Constant_PI / -6.0f);
			break;
		case ELeft:
			m_pEditModeCamera->SetOrthographic(wh[0], wh[1], -fZFar, fZFar);
			m_pEditModeCamera->SetPosition(MXVisual::Float3(), MXVisual::False);
			m_pEditModeCamera->TransRotate(MXVisual::Math::AXIS_Y, MXVisual::Constant_PI * 0.5f);
			break;
		case ETop:
			m_pEditModeCamera->SetOrthographic(wh[0], wh[1], -fZFar, fZFar);
			m_pEditModeCamera->SetPosition(MXVisual::Float3(), MXVisual::False);
			m_pEditModeCamera->TransRotate(MXVisual::Math::AXIS_X, MXVisual::Constant_PI * -0.5f);
			break;
		case EFront:
			m_pEditModeCamera->SetOrthographic(wh[0], wh[1], -fZFar, fZFar);
			m_pEditModeCamera->SetPosition(MXVisual::Float3(), MXVisual::False);
			break;
		}
		m_eCVType = eType;
	}

	void Init(IGRenderWindow* pRWindow, const Char* szProjectPath)
	{
		PhysicsSimulator::DebugCallbacks callbacks = {
			PhysicsDebug::FDrawLine,
			PhysicsDebug::FDrawText3D
		};
		PhysicsSimulator::SetDebugCallbacks(callbacks);

		m_bRenderGrid = True;
		m_bRenderIcon = True;
		m_pRWindow = pRWindow;

		m_bShowPanels = True;
		m_bShowDebugPanel = True;
		m_bShowSceneTreePanel = True;
		m_bShowPropertyPanel = True;
		m_bShowAssetBrowser = True;

		m_bShowStatistics = False;

		m_eEditType = TransformEditType::ENone;
		m_bEditLocal = False;
		m_fMoveSpeed = 4.0f;
		m_fRotateSpeed = 2.0f;
		m_strProjectPath = szProjectPath;
		m_ModalDlgContentCallback = nullptr;

		m_pDebugPanel = new EditorDebugPanel;
		m_pSceneTreePanel = new EditorSceneTree;
		m_pPropertyPanel = new EditorPropertyPanel;
		m_pAssetBrowser = new EditorAssetBrowser;
		m_pAssetEditor = new EditorAssetEditor;

		m_pEditModeCamera = new MXVisual::CCamera("_RoamingCamera");
		m_pEditModeCamera->SetRenderWindow(pRWindow);
		ResetEditCamera(CameraViewType::EPerspective);

		RefreshDirectoryInfo();

		Init3DObjectRenderData();

		m_pScene = new CScene("_TEMP_");
		m_pScene->Active();
	}
	void Release()
	{
		MX_DELETE(m_pDebugPanel);
		MX_DELETE(m_pSceneTreePanel);
		MX_DELETE(m_pPropertyPanel);
		MX_DELETE(m_pAssetBrowser);
		MX_DELETE(m_pAssetEditor);

		MX_DELETE(m_pEditModeCamera);
		m_pScene->Inactive();
		MX_DELETE(m_pScene);

		for (UInt32 uIcon = 0; uIcon < (UInt32)IconType::ECount; uIcon++)
		{
			MX_RELEASE_INTERFACE(m_arr3DObjectIconParameterBlock[uIcon]);
			MX_RELEASE_INTERFACE(m_arr3DObjectIconTransformBuffer[uIcon]);
			MX_RELEASE_INTERFACE(m_arr3DObjectIconTexture[uIcon]);
		}
		MX_RELEASE_INTERFACE(m_p3DObjectIconMaterial);
		MX_RELEASE_INTERFACE(m_p3DObjectIconPrimitive);

		eg_ReleaseEditorGraphics();
	}

	Boolean OpenAsset(const AssetInfo* pAssetInfo)
	{
		switch (pAssetInfo->eType)
		{
		case AssetType::EScene:
		{
			CScene* pNewScene = new CScene(pAssetInfo->strName.CString());
			String strAbsName = String::Format("%s/%s/%s",
				pAssetInfo->pParentFolderInfo->strPath.CString(),
				pAssetInfo->pParentFolderInfo->strName.CString(),
				pAssetInfo->strName.CString());
#if 0
			m_atomEditorState.Exchange(EDITOR_STATE_SCENE_LOADING);
			ThreadManager::AddThreadTask(
				IO_THREAD_NAME,
				[strAbsName]() {
				IDeserializer* pDeserial = TextDeserializer::Open(strAbsName.CString());
				ASSERT_IF_FAILED(pDeserial != nullptr);
				ASSERT_IF_FAILED(pDeserial != nullptr);

				CScene* pNewScene = new CScene("");
				pNewScene->OnDeserialize(*pDeserial->GetRootDeserializerBlock());
				pDeserial->Close();
				m_atomEditorState.Exchange(EDITOR_STATE_SCENE_LOADING);
				pNewScene->Active();
			},
				"Scene Loading");
#else
			IDeserializer* pDeserializer = TextDeserializer::Open(strAbsName.CString());
			ASSERT_IF_FAILED(pDeserializer != nullptr);
			if (pNewScene->OnDeserialize(*pDeserializer->GetRootDeserializerBlock()))
			{
				MX_DELETE(m_pScene);
				m_pScene = pNewScene;
				pNewScene->Active();
				pDeserializer->Close();

				m_strScenePath = strAbsName.CString();
				return True;
			}
			pDeserializer->Close();
#endif
			break;
		}
		case AssetType::EMaterial:
		case AssetType::EMaterialInstance:
			m_pAssetEditor->OpenAsset(pAssetInfo);
			break;
		}
		return False;
	}

	void LogMessage(const char* szMessage)
	{
		ASSERT_IF_FAILED(m_pDebugPanel != nullptr);
		m_pDebugPanel->LogMessage(szMessage);
	}
	void LogError(const char* szMessage)
	{
		ASSERT_IF_FAILED(m_pDebugPanel != nullptr);
		m_pDebugPanel->LogError(szMessage);
	}
	void LogWarning(const char* szMessage)
	{
		ASSERT_IF_FAILED(m_pDebugPanel != nullptr);
		m_pDebugPanel->LogWarning(szMessage);
	}

	void UpdateEditCamera()
	{
		Float2 f2CurPosition = Input::GetMouseScreenPosition();
		Float2 f2DeltaPos = Input::GetMouseRelativePosition();
		Float1 fDeltaTime = (Float1)Timer::GetDeltaTime();

		if (Input::GetKeyDown(Input::Key::eLButton))
		{
			//Move On XZ Plane
			Float3 xzdir0(m_pEditModeCamera->GetDirForward());
			Float3 xzdir1(m_pEditModeCamera->GetDirRight());
			xzdir0[1] = 0;
			xzdir1[1] = 0;
			xzdir0 = Math::Normalize(xzdir0);
			xzdir1 = Math::Normalize(xzdir1);
			m_pEditModeCamera->TransMove(xzdir0,
				-f2DeltaPos[1] * fDeltaTime * m_fMoveSpeed);
			m_pEditModeCamera->TransMove(xzdir1,
				f2DeltaPos[0] * fDeltaTime * m_fMoveSpeed);
		}

		if (Input::GetKeyDown(Input::Key::eRButton))
		{
			m_pEditModeCamera->TransRotate(m_pEditModeCamera->GetDirRight(), -f2DeltaPos[1] * fDeltaTime * m_fRotateSpeed);
			m_pEditModeCamera->TransRotate(Math::AXIS_Y, -f2DeltaPos[0] * fDeltaTime * m_fRotateSpeed);
		}

		if (Input::GetKeyDown(Input::Key::eMButton))
		{
			m_pEditModeCamera->TransMove(m_pEditModeCamera->GetDirUp(), -f2DeltaPos[1] * fDeltaTime * m_fMoveSpeed);
			m_pEditModeCamera->TransMove(m_pEditModeCamera->GetDirRight(), f2DeltaPos[0] * fDeltaTime * m_fMoveSpeed);
		}
		else
		{
			Float1 fWheel = Input::GetMouseWheelRelativePosition();
			if (fWheel > 0.0f || fWheel < 0.0f)
			{
				m_pEditModeCamera->TransMove(m_pEditModeCamera->GetDirForward(), fWheel * m_fMoveSpeed);
			}
		}

		m_pEditModeCamera->OnUpdate();
	}

	void LayoutMenu_NewScene(Boolean bMenuOpened)
	{
		Boolean bMenuClick = Input::GetKeyDown(Input::eLCtrl) && Input::GetKeyClick(Input::eN);
		if (bMenuOpened)
			bMenuClick |= ImGui::MenuItem("New", "Ctrl+N");

		if (bMenuClick)
		{

		}
	}
	void LayoutMenu_SaveScene(Boolean bMenuOpened)
	{
		Boolean bMenuClick = Input::GetKeyDown(Input::eLCtrl) && Input::GetKeyClick(Input::eS);
		if (bMenuOpened)
		{
			bMenuClick |= ImGui::MenuItem("Save", "Ctrl+S");
		}

		if (bMenuClick)
		{
			ISerializer* pSerializer = TextSerializer::Open(m_strScenePath.CString());
			ASSERT_IF_FAILED(pSerializer != nullptr);
			if (!m_pScene->OnSerialize(*pSerializer))
			{
				LogError("Save As Scene Failed!");
			}
			else
			{
				LogMessage("Save Scene Succeed!");
			}
			pSerializer->Close();
		}
	}
	void LayoutMenu_SaveAsScene(Boolean bMenuOpened)
	{
		Boolean bMenuClick = Input::GetKeyDown(Input::eLCtrl) && Input::GetKeyClick(Input::eLShift) && Input::GetKeyClick(Input::eS);
		if (bMenuOpened)
			bMenuClick |= ImGui::MenuItem("Save As", "Ctrl+Shift+S");

		if (bMenuClick)
		{
			Char buf[Constant_MAX_PATH] = { 0 };
			if (System::DlgSelectFile("Save As Scene", buf, Constant_MAX_PATH))
			{
				ISerializer* pSerializer = TextSerializer::Open(buf);
				ASSERT_IF_FAILED(pSerializer != nullptr);
				if (!m_pScene->OnSerialize(*pSerializer))
				{
					LogError("Save As Scene Failed!");
				}
				else
				{
					LogMessage("Save Scene Succeed!");
					m_strScenePath = buf;
				}
				pSerializer->Close();
			}
		}
	}
	void LayoutMenu_Edit(Boolean bMenuOpened)
	{
		Boolean bMenuClick[] = {
			Input::GetKeyDown(Input::eLCtrl) && Input::GetKeyClick(Input::eQ),
			Input::GetKeyDown(Input::eLCtrl) && Input::GetKeyClick(Input::eW),
			Input::GetKeyDown(Input::eLCtrl) && Input::GetKeyClick(Input::eE),
			Input::GetKeyDown(Input::eLCtrl) && Input::GetKeyClick(Input::eR),
			Input::GetKeyDown(Input::eLCtrl) && Input::GetKeyClick(Input::eX),
		};
		if (bMenuOpened)
		{
			bMenuClick[0] |= ImGui::MenuItem("No Transform", "Ctrl+Q", m_eEditType == TransformEditType::ENone, m_eEditType != TransformEditType::ENone);
			bMenuClick[1] |= ImGui::MenuItem("Translation", "Ctrl+W", m_eEditType == TransformEditType::ETranslation, m_eEditType != TransformEditType::ETranslation);
			bMenuClick[2] |= ImGui::MenuItem("Rotation", "Ctrl+E", m_eEditType == TransformEditType::ERotation, m_eEditType != TransformEditType::ERotation);
			bMenuClick[3] |= ImGui::MenuItem("Scale", "Ctrl+R", m_eEditType == TransformEditType::EScale, m_eEditType != TransformEditType::EScale);
			ImGui::Separator();
			bMenuClick[4] |= ImGui::MenuItem(m_bEditLocal ? "Edit Space: Local" : "Edit Space: Global", "Ctrl+X");
		}

		if (bMenuClick[0])m_eEditType = ENone;
		else if (bMenuClick[1])m_eEditType = ETranslation;
		else if (bMenuClick[2])m_eEditType = ERotation;
		else if (bMenuClick[3])m_eEditType = EScale;

		if (bMenuClick[4])m_bEditLocal = !m_bEditLocal;
	}
	void LayoutMenu()
	{
		ImGui::BeginMainMenuBar();
		{
			Boolean bMenuOpened = ImGui::BeginMenu("File");
			if (bMenuOpened) ImGui::Text("Scene----------------");
			LayoutMenu_NewScene(bMenuOpened);
			if (!m_strScenePath.IsEmpty())
			{
				LayoutMenu_SaveScene(bMenuOpened);
			}
			LayoutMenu_SaveAsScene(bMenuOpened);
			if (bMenuOpened) ImGui::EndMenu();

			bMenuOpened = ImGui::BeginMenu("Edit");
			LayoutMenu_Edit(bMenuOpened);
			if (bMenuOpened)
			{
				ImGui::Separator();
				ImGui::MenuItem("Play", "F5");//Or Pause
				ImGui::MenuItem("Stop", "Shift+F5", false, false);
				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("View"))
			{
				ImGui::MenuItem("Show Panels", "", &m_bShowPanels);
				if (m_bShowPanels)
				{
					ImGui::Separator();
					ImGui::MenuItem("SceneTree", "", &m_bShowSceneTreePanel);
					ImGui::MenuItem("Asset Browser", "", &m_bShowAssetBrowser);
					ImGui::MenuItem("Debug", "", &m_bShowDebugPanel);
					ImGui::MenuItem("Property", "", &m_bShowPropertyPanel);
					ImGui::Separator();
					ImGui::MenuItem("Game Panel");
				}
				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("Camera"))
			{
				if (ImGui::MenuItem("Perspective", "", m_eCVType == CameraViewType::EPerspective))
				{
					ResetEditCamera(CameraViewType::EPerspective);
				}
				if (ImGui::MenuItem("Orthogonal Left", "", m_eCVType == CameraViewType::ELeft))
				{
					ResetEditCamera(CameraViewType::ELeft);
				}
				if (ImGui::MenuItem("Orthogonal Top", "", m_eCVType == CameraViewType::ETop))
				{
					ResetEditCamera(CameraViewType::ETop);
				}
				if (ImGui::MenuItem("Orthogonal Front", "", m_eCVType == CameraViewType::EFront))
				{
					ResetEditCamera(CameraViewType::EFront);
				}
				ImGui::Separator();
				ImGui::SliderFloat("Move Speed", &m_fMoveSpeed, 0.0f, 8.0f);
				ImGui::SliderFloat("Rotate Speed", &m_fRotateSpeed, 0.0f, 4.0f);
				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("Render Debug"))
			{
				ImGui::MenuItem("Show Backbuffer", "", &GVarShadingDebugView.bValue);
				ImGui::InputFloat("Shadow Depth Offset", &GVarShadowDepthOffset.fValue, 1e-6f, 1e-5f, 7);
				ImGui::SliderFloat("Cascade Shadow Distance", &GVarCascadeShadowDistance.fValue, 0.01f, 1.0f);
				ImGui::MenuItem("Cascade Shadow Debug", "", &GVarCascadeShadowDebug.bValue);
				ImGui::Separator();
				ImGui::MenuItem("Show Grid", "", &m_bRenderGrid);
				ImGui::MenuItem("Show Icon", "", &m_bRenderIcon);
				ImGui::MenuItem("Show Physics Frame", "", &GVarPhysicsDebug.bValue);
				ImGui::EndMenu();
			}

			ImGui::Separator();
			if (ImGui::Checkbox("Statistics", &m_bShowStatistics))
			{
				ThreadManager::SetStatisticsSwitch(m_bShowStatistics);
			}
			ImGui::Separator();

			if (m_atomEditorState.Value() == EDITOR_STATE_SCENE_LOADING)
			{
				char AnimChars[] = { '|', '/', '-', '\\' };
				ImGui::Text("Scene Loading:%c", AnimChars[UInt32(System::GetTime() * 1000) % ARRAY_ELEMENT_COUNT(AnimChars)]);
			}
		}
		ImGui::EndMainMenuBar();
	}

	void LayoutModalDlg_YesNo()
	{
		static ModalDlgYesNoResult eResult = ModalDlgYesNoResult::EWaitingFor;

		const Char* PopupName = "ModalDlg_YesNo";
		if (m_ModalDlgContentCallback != nullptr && !ImGui::IsPopupOpen(PopupName))
		{
			ImGui::OpenPopup(PopupName);
		}

		if (ImGui::BeginPopupModal(PopupName, nullptr, ImGuiWindowFlags_NoTitleBar))
		{
			eResult = ImGui::Button("Yes") ? ModalDlgYesNoResult::EYes : eResult;
			ImGui::SameLine();
			eResult = ImGui::Button("No") ? ModalDlgYesNoResult::ENo : eResult;

			if (m_ModalDlgContentCallback(eResult))
			{
				eResult = ModalDlgYesNoResult::EWaitingFor;
				m_ModalDlgContentCallback = nullptr;
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}
	}

	void ModalDlg(FModalDlgYesNoCallBack DlgContentCallback)
	{
		m_ModalDlgContentCallback = DlgContentCallback;
	}

	void ScanAssets(const Char* szProjectPath)
	{
		MX_DELETE(m_pProjectInfo);
		m_pProjectInfo = new FolderInfo;
		Char buffer[Constant_MAX_PATH];
		CalFileName(szProjectPath, buffer, Constant_MAX_PATH);
		m_pProjectInfo->strName = buffer;

		NormalizePath(szProjectPath, buffer, Constant_MAX_PATH);
		m_pProjectInfo->strPath = buffer;
		SInt32 nLastSeperator = m_pProjectInfo->strPath.FindLastOf("/");
		if (nLastSeperator >= 0)
		{
			m_pProjectInfo->strPath.Erase(nLastSeperator);
		}

		FolderInfo* pDefaultFolderInfo = new FolderInfo;
		pDefaultFolderInfo->strName = "Default";
		pDefaultFolderInfo->strPath = "";

		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::RED_TEXTURE, AssetType::ETexture, nullptr });
		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::GREEN_TEXTURE, AssetType::ETexture, nullptr });
		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::BLUE_TEXTURE, AssetType::ETexture, nullptr });
		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::WHITE_TEXTURE, AssetType::ETexture, nullptr });
		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::BLACK_TEXTURE, AssetType::ETexture, nullptr });
		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::MOSAIC_TEXTURE, AssetType::ETexture, nullptr });
		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::NOISE_TEXTURE, AssetType::ETexture , nullptr });
		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::FLATNORMAL_TEXTURE, AssetType::ETexture , nullptr });

		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::MESH_PLANE, AssetType::EMesh, nullptr });
		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::MESH_CUBE, AssetType::EMesh, nullptr });
		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::MESH_SPHERE, AssetType::EMesh, nullptr });

		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::MATERIAL, AssetType::EMaterial, nullptr });
		pDefaultFolderInfo->arrAssetInfo.Add(AssetInfo{ DefaultResource::MATERIALINSTANCE, AssetType::EMaterialInstance, nullptr });
		m_pProjectInfo->arrChildFolder.Add(pDefaultFolderInfo);

		Array<WIN32_FIND_DATA> stackFD;
		Array<FolderInfo*> stackFolderInfo;
		Array<String> stackPath;
		Array<HANDLE> stackFolderHandle;
		stackFD.Add(WIN32_FIND_DATA());
		stackFolderInfo.Add(m_pProjectInfo);
		stackPath.Add(szProjectPath);
		stackFolderHandle.Add(::FindFirstFile(
			String::Format("%s\\*", stackPath[stackPath.ElementCount() - 1].CString()).CString(), &stackFD[0]));
		if (!::FindNextFile(stackFolderHandle[stackFolderHandle.ElementCount() - 1], &stackFD[0]))
			return;

		Boolean bFoundNextFile = True;
		while (1)
		{
			WIN32_FIND_DATA& refFD = stackFD[stackFD.ElementCount() - 1];
			if (bFoundNextFile &&
				strcmp(refFD.cFileName, "..") != 0 &&
				strcmp(refFD.cFileName, ".") != 0)
			{
				stackFolderInfo[stackFolderInfo.ElementCount() - 1]->arrAssetInfo.Add(
					AssetInfo{ refFD.cFileName, IdentifyAssetType(refFD.cFileName), stackFolderInfo[stackFolderInfo.ElementCount() - 1] });

				if (refFD.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					FolderInfo* pInfo = new FolderInfo;
					pInfo->strName = refFD.cFileName;
					pInfo->strPath = stackPath[stackPath.ElementCount() - 1];
					stackFolderInfo[stackFolderInfo.ElementCount() - 1]->arrChildFolder.Add(pInfo);
					stackFolderInfo.Add(pInfo);

					stackFD.Add(WIN32_FIND_DATA());

					stackPath.Add(
						String::Format("%s\\%s",
							stackPath[stackPath.ElementCount() - 1].CString(),
							pInfo->strName.CString()));

					stackFolderHandle.Add(::FindFirstFile(
						String::Format("%s\\*", stackPath[stackPath.ElementCount() - 1].CString()).CString(),
						&stackFD[stackFD.ElementCount() - 1]));
				}
			}

			bFoundNextFile = ::FindNextFile(stackFolderHandle[stackFolderHandle.ElementCount() - 1], &refFD);
			if (!bFoundNextFile)
			{
				if (stackPath.ElementCount() == 1)
				{
					FindClose(stackFolderHandle[0]);
					break;
				}

				stackFD.Erase(stackFD.ElementCount() - 1, 1);
				stackPath.Erase(stackPath.ElementCount() - 1, 1);
				stackFolderInfo.Erase(stackFolderInfo.ElementCount() - 1, 1);
				FindClose(stackFolderHandle[stackFolderHandle.ElementCount() - 1]);
				stackFolderHandle.Erase(stackFolderHandle.ElementCount() - 1, 1);
			}
		}
	}

	void Layout()
	{
		if (m_bShouldRefreshAssetInfo)
		{
			ScanAssets(m_strProjectPath.CString());
			m_bShouldRefreshAssetInfo = False;
			LogMessage("Refresh Asset Info Done.");
		}

		LayoutMenu();
		eg_ResetEditorGraphics();

		m_pEditControl = eg_GetEditControl(m_eEditType);

		if (m_bShowPanels)
		{
			if (m_bShowSceneTreePanel)
				m_pSceneTreePanel->Layout();

			if (m_bShowPropertyPanel)
				m_pPropertyPanel->Layout();

			if (m_bShowAssetBrowser)
				m_pAssetBrowser->Layout(m_pProjectInfo);

			if (m_bShowDebugPanel)
				m_pDebugPanel->Layout();

			m_pAssetEditor->Layout();
		}

		LayoutModalDlg_YesNo();

		ImGuiIO& io = ImGui::GetIO();
		Boolean bTransformEdit = False;
		if (m_pEditControl != nullptr)
		{
			Matrix4 matW2C = m_pEditModeCamera->GetViewFrustum()->GetWorldToView() *
				m_pEditModeCamera->GetViewFrustum()->GetViewToClip();
			Matrix4 matC2S = Math::MatrixClipToViewport(Float2(),
				Float2(io.DisplaySize.x, io.DisplaySize.y),
				Float2(0, 0));

			bTransformEdit = m_pEditControl->Transform(matW2C, matC2S,
				Float2(io.MousePos.x, io.MousePos.y),
				io.MouseDown[ImGuiMouseButton_Left]);
		}

		if (!ImGui::IsAnyWindowHovered() &&
			!ImGui::IsAnyItemActive() &&
			!bTransformEdit)
		{
			UpdateEditCamera();
		}

		if (m_bRenderIcon)
		{
			std::function<Void(ICObject*)> TreeDepthGetTransform;
			TreeDepthGetTransform = [&TreeDepthGetTransform](ICObject* pObj)
			{
				if (pObj == nullptr)return;

				Float3 pos = Math::MatrixGetTranslation(pObj->GetTransform(False));
				Quaternion q = Math::VectorToVectorQuaternion(Math::AXIS_Z, m_pEditModeCamera->GetDirForward());

				const Float1 fScale = Math::Lerp(0.2f, 1.0f, Math::Length(pos - m_pEditModeCamera->GetPosition(False)));
				Matrix4 mat =
					Math::MatrixScale(Float3(fScale, fScale, fScale)) * Math::MatrixQuaternion(q) * Math::MatrixTranslation(pos);

				switch (pObj->GetType())
				{
				case CObjectType::eBase:
					m_arr3DObjectIconTransform[(UInt8)IconType::E3DObject].Add(mat);
					break;
				case CObjectType::eCamera:
					m_arr3DObjectIconTransform[(UInt8)IconType::ECamera].Add(mat);
					break;
				}
				if (pObj->GetComponent(DefaultResource::COMPONENT_NAME_DIRECTION_LIGHT) != nullptr)
				{
					m_arr3DObjectIconTransform[(UInt8)IconType::EDirectionLight].Add(mat);
				}
				else if (pObj->GetComponent(DefaultResource::COMPONENT_NAME_POINT_LIGHT) != nullptr)
				{
					m_arr3DObjectIconTransform[(UInt8)IconType::EPointLight].Add(mat);
				}
				else if (pObj->GetComponent(DefaultResource::COMPONENT_NAME_SPOT_LIGHT) != nullptr)
				{
					m_arr3DObjectIconTransform[(UInt8)IconType::ESpotLight].Add(mat);
				}

				for (UInt32 u = 0; u < pObj->GetChildCount(); u++)
				{
					TreeDepthGetTransform(pObj->GetChild(u));
				}
			};

			for (UInt32 u = 0; u < CScene::ActivedScene()->GetRootObjectCount(); u++)
			{
				ICObject* pObj = CScene::ActivedScene()->GetRootObject(u);
				TreeDepthGetTransform(pObj);
			}

			for (UInt32 uIcon = 0; uIcon < (UInt32)IconType::ECount; uIcon++)
			{
				if (m_arr3DObjectIconTransform[uIcon].ElementCount() == 0)continue;
				m_arr3DObjectIconTransformBuffer[uIcon]->SetTransforms(
					m_arr3DObjectIconTransform[uIcon].GetRawData(),
					m_arr3DObjectIconTransform[uIcon].ElementCount());
			}
		}

		if (m_bShowStatistics)
		{
			GStatistics stat;
			GraphicsRenderer::GetStatistics(&stat);

			ImGui::Begin("Statistics", 0,
				ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar);

			ImGui::BulletText("PrimitiveCount: %d", stat.uPrimitiveCount);
			ImGui::BulletText("DrawCall: %d", stat.uDrawCall);

			ImGui::BulletText("%s: ", RENDER_THREAD_NAME);
			ThreadManager::GetThreadStatistics(RENDER_THREAD_NAME, m_stats);

			Float1 fCurTime = Timer::GetTime();
			UInt32 uNodeIndex = 0;
			auto Node = m_stats.GetNode(uNodeIndex++);
			while (Node != nullptr)
			{
				if (Node->Data.EndTime >= fCurTime)
				{
					ImGui::SameLine();
					ImGui::Text("|%s: %4.2f(ms)", Node->Data.Name.CString(), Node->Data.EndTime - Node->Data.StartTime);
				}
				else
				{
					Node->Detach();
					delete Node;
					uNodeIndex--;
				}
				Node = m_stats.GetNode(uNodeIndex++);
			}
			ImGui::End();
		}

		if (m_pScene)
		{
			//m_pScene->OnUpdate();
		}
	}

	void RefreshDirectoryInfo()
	{
		m_bShouldRefreshAssetInfo = True;
	}

	void RenderScene()
	{
		if (m_bRenderGrid)
		{
			MXVisual::SInt32 nRange = 50 / 2;
			eg_GetEditorGraphics()->ChangeColor(MXVisual::Float4(0.8f, 0.8f, 0.8f, 1.0f));
			for (MXVisual::SInt32 x = -nRange; x <= nRange; x += 1)
			{
				eg_GetEditorGraphics()->RenderLine(
					MXVisual::Float3((Float1)x, 0, (Float1)-nRange),
					MXVisual::Float3((Float1)x, 0, (Float1)nRange),
					1.0f);

				eg_GetEditorGraphics()->RenderLine(
					MXVisual::Float3((Float1)-nRange, 0, (Float1)x),
					MXVisual::Float3((Float1)nRange, 0, (Float1)x),
					1.0f);
			}
		}

		if (m_bRenderIcon)
		{
			for (UInt32 uIcon = 0; uIcon < (UInt32)IconType::ECount; uIcon++)
			{
				if (m_arr3DObjectIconTransform[uIcon].ElementCount() > 0)
				{
					GraphicsRenderer::RegisterPrimitive(
						m_p3DObjectIconPrimitive,
						0, 6,
						m_p3DObjectIconMaterial,
						m_arr3DObjectIconParameterBlock[uIcon],
						m_arr3DObjectIconTransformBuffer[uIcon]);
				}
				m_arr3DObjectIconTransform[uIcon].Clear();
			}
		}

		{//Render Axis System
			Float3 ow = m_pEditModeCamera->GetPosition(False);
			ow += m_pEditModeCamera->GetDirForward() *5.0f +
				m_pEditModeCamera->GetDirRight() * 3.5f +
				m_pEditModeCamera->GetDirUp() *-2.0f;

			eg_GetEditorGraphics()->ChangeColor(MXVisual::Float4(1.0f, 0.0f, 0.0f, 1.0f));
			eg_GetEditorGraphics()->RenderArrow(ow, ow + Math::AXIS_X, Math::AXIS_Y, 0.1f);
			eg_GetEditorGraphics()->ChangeColor(MXVisual::Float4(0.0f, 1.0f, 0.0f, 1.0f));
			eg_GetEditorGraphics()->RenderArrow(ow, ow + Math::AXIS_Y, Math::AXIS_Z, 0.1f);
			eg_GetEditorGraphics()->ChangeColor(MXVisual::Float4(0.0f, 0.0f, 1.0f, 1.0f));
			eg_GetEditorGraphics()->RenderArrow(ow, ow + Math::AXIS_Z, Math::AXIS_Y, 0.1f);
		}
	}
	void Render()
	{
		RenderScene();

		if (m_pEditControl != nullptr && eg_GetSelectedObject() != nullptr)
		{
			Float1 fScale = 64.0f;
			ICObject* pObj = eg_GetSelectedObject();
			if (pObj->GetType() != eBase2D)
			{
				CObject* pObj3D = (CObject*)pObj;
				Float3 dis = m_pEditModeCamera->GetPosition(False) - pObj3D->GetPosition(False);
				fScale = Math::Lerp(0.5f, 32.0f, Math::Length(dis) / 128.0f);
			}

			m_pEditControl->Reset(eg_GetSelectedObject(), m_bEditLocal, fScale);
			m_pEditControl->Render();
		}

		PhysicsSimulator::Render();

		eg_FlushEditorGraphics();

		CSceneRender::Render(m_pScene, m_pEditModeCamera);
	}
}
