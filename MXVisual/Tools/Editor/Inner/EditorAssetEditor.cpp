#include "EditorAssetEditor.h"
#include "EditorGlobal.h"
#include "EditorMainFrame.h"

#include "MXFile.h"
#include "MXSystem.h"
#include "MXAsset.h"

#include "imgui.h"

EditorAssetEditor::EditorAssetEditor()
{
}
EditorAssetEditor::~EditorAssetEditor()
{
}

void EditorAssetEditor::Layout()
{
	if (m_arrAssetDocs.ElementCount() == 0)
	{
		return;
	}

	SInt32 nToRemovedAsset = -1;
	if (ImGui::Begin("AssetEditor"))
	{
		if (ImGui::BeginTabBar("AssetTabBar"))
		{
			for (UInt32 u = 0; u < m_arrAssetDocs.ElementCount(); u++)
			{
				Boolean bOriginOpenFlag = True;
				if (ImGui::BeginTabItem(
					m_arrAssetDocs[u].strName.CString(),
					&bOriginOpenFlag,
					m_arrAssetDocs[u].bEditedButNotSaved ? ImGuiTabItemFlags_UnsavedDocument : 0))
				{
					switch (m_arrAssetDocs[u].eType)
					{
					case AssetType::EMaterial:
						m_MaterialPanel.Layout(m_arrAssetDocs[u].strAbsName.CString(),
							m_arrAssetDocs[u].bEditedButNotSaved);
						break;
					case AssetType::EMaterialInstance:
						m_MaterialInstancePanel.Layout(m_arrAssetDocs[u].strAbsName.CString(),
							m_arrAssetDocs[u].bEditedButNotSaved);
						break;
					}

					ImGui::EndTabItem();
				}

				if (!bOriginOpenFlag)
					nToRemovedAsset = u;
			}
			ImGui::EndTabBar();
		}
		ImGui::End();
	}

	if (nToRemovedAsset > -1)
	{
		if (m_arrAssetDocs[nToRemovedAsset].bEditedButNotSaved)
		{
			//EditorMainFrame::ModalDlg();
		}
		else
		{
			m_arrAssetDocs.Erase(nToRemovedAsset, 1);
		}
	}
}

void EditorAssetEditor::OpenAsset(const AssetInfo* pAsset)
{
	ASSERT_IF_FAILED(pAsset != nullptr);
	if (pAsset->pParentFolderInfo == nullptr)
	{
		EditorMainFrame::LogWarning("Asset is not editable");
		return;
	}
	String strAssetAbsName = String::Format("%s/%s/%s",
		pAsset->pParentFolderInfo->strPath.CString(),
		pAsset->pParentFolderInfo->strName.CString(),
		pAsset->strName.CString());

	Boolean bAssetExist = False;
	for (UInt32 u = 0; u < m_arrAssetDocs.ElementCount(); u++)
	{
		if (m_arrAssetDocs[u].strAbsName == strAssetAbsName)
		{
			bAssetExist = True;
			m_arrAssetDocs[u].bTabSelected = True;
		}
		else
		{
			m_arrAssetDocs[u].bTabSelected = False;
		}
	}
	if (bAssetExist)return;
	m_arrAssetDocs.Add(EditorAssetDocument{ True, False, pAsset->strName, strAssetAbsName, pAsset->eType });
}