#pragma once
#ifndef _EDITOR_MAIN_FRAME_
#define _EDITOR_MAIN_FRAME_
#include "EditorUIManager.h"
#include "EditorGlobal.h"

namespace MXVisual
{
	class IGRenderWindow;
	class CCamera;
	class CScene;
}

enum class ModalDlgYesNoResult : UInt8
{
	EWaitingFor,
	EYes,
	ENo,
};
typedef Boolean(*FModalDlgYesNoCallBack)(ModalDlgYesNoResult);

namespace EditorMainFrame
{
	void Init(IGRenderWindow* pRWindow, const Char* szProjectPath);
	void Release();

	Boolean OpenAsset(const AssetInfo* pAssetInfo);

	void Layout();
	void Render();

	void ModalDlg(FModalDlgYesNoCallBack DlgContentCallback);

	void LogMessage(const char* szMessage);
	void LogError(const char* szMessage);
	void LogWarning(const char* szMessage);

	void RefreshDirectoryInfo();
}
#endif