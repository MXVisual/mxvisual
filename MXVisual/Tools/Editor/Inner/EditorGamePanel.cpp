#include "EditorGamePanel.h"
#include "MXGraphicsResource.h"
#include "MXCScene.h"
#include "MXCSceneRender.h"
#include "MXCCamera.h"

using namespace MXVisual;
IGRenderWindow*	g_pGameRenderWindow = nullptr;

BEGIN_EVENT_TABLE(EditorGamePanel, wxPanel)
EVT_SIZE(EditorGamePanel::OnSize)
END_EVENT_TABLE()

EditorGamePanel::EditorGamePanel(wxWindow* parent)
	: wxPanel(parent)
{
	Layout();
}
EditorGamePanel::~EditorGamePanel()
{
	if (CScene::ActivedScene() != nullptr && CScene::ActivedScene()->MainCamera() != nullptr)
	{
		CScene::ActivedScene()->MainCamera()->SetRenderWindow(nullptr);
	}

	MX_RELEASE_INTERFACE(g_pGameRenderWindow);
}

void EditorGamePanel::Running(bool bTimePause)
{
	if (CScene::ActivedScene() != nullptr)
	{
		if (!bTimePause)
		{
			CScene::ActivedScene()->OnUpdate();
		}

		if (g_pGameRenderWindow != nullptr)
		{
			CSceneRender::Render(CScene::ActivedScene(),
				CScene::ActivedScene()->MainCamera());

			g_pGameRenderWindow->Present();
		}
	}
}

void EditorGamePanel::OnSize(wxSizeEvent& event)
{
	wxSize newSize = event.GetSize();
	if (newSize.GetWidth() <= 0 || newSize.GetHeight() <= 0)
		return;

	if (g_pGameRenderWindow == nullptr)
	{
		MXVisual::IGRenderWindow::Description desc;
		desc.uParentWindowHandle = (MXVisual::UInt64)this->GetHWND();
		desc.uBackbufferCount = 2;
		desc.eBackbufferFormat = MXVisual::PixelFormat::EPF_R8G8B8A8;
		desc.uBackbufferWidth = newSize.GetWidth();
		desc.uBackbufferHeight = newSize.GetHeight();
		g_pGameRenderWindow = MXVisual::GraphicsResource::CreateRenderWindow(desc);
		g_pGameRenderWindow->Show(MXVisual::True);
	}
	else
	{
		g_pGameRenderWindow->Resize(newSize.GetWidth(), newSize.GetHeight());
	}

	if (CScene::ActivedScene() != nullptr && CScene::ActivedScene()->MainCamera() != nullptr)
	{
		if (CScene::ActivedScene()->MainCamera()->GetViewFrustum() == nullptr)
		{
			CScene::ActivedScene()->MainCamera()->SetRenderWindow(g_pGameRenderWindow);
		}
	}
}
