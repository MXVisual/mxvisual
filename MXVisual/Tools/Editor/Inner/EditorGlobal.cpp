#include "EditorGlobal.h"
#include "MXCComponent.h"
#include "MXCEditorGraphics.h"

#include "MXMath.h"
#include "MXCCamera.h"
#include "MXAsset.h"
#include "MXEngine.h"
#include "MXTimer.h"

#include "imgui.h"

#include "MXGraphicsResource.h"
using namespace MXVisual;

#pragma pack(push,1)
struct TGAHeader
{
	UInt8     bIDLength;
	UInt8     bColorMapType;
	UInt8     bImageType;
	UInt16    wColorMapFirst;
	UInt16    wColorMapLength;
	UInt8     bColorMapSize;
	UInt16    wXOrigin;
	UInt16    wYOrigin;
	UInt16    wWidth;
	UInt16    wHeight;
	UInt8     bBitsPerPixel;
	UInt8     bDescriptor;
};
#pragma pack(pop)

IGTexture* eg_LoadTGATexture(const Char* szTextureFileName)
{
	IGTexture::Description desc;
	TGAHeader tga;
	IFile* pTexFile = IFile::Open(szTextureFileName, IFile::EFDT_Binary);
	pTexFile->Read(&tga, sizeof(tga));

	desc.uWidth = tga.wWidth;
	desc.uHeight = tga.wHeight;
	desc.uDepth = 1;
	desc.uMipLevel = 1;
	desc.bDynamic = True;
	desc.bGenerateMipMap = False;
	desc.bTargetable = False;
	desc.eFormat = EPF_R8G8B8A8;
	desc.eType = TextureType::ETexture2D;
	IGTexture* pTex = GraphicsResource::CreateTexture(desc);

	UInt8* pBuf = new UInt8[desc.uWidth * desc.uHeight * 4];
	pTexFile->Read(pBuf, desc.uWidth * desc.uHeight * 4);
	pTexFile->Close();

	MapRegion region;
	if (pTex->Map(0, 0, region))
	{
		for (UInt32 u = 0; u < desc.uHeight; u++)
		{
			UInt8* pAddress = (UInt8*)region.pAddress + u * region.uPitch;
			UInt8* pSrc = (UInt8*)pBuf + u * 4 * desc.uWidth;

			for (UInt32 uX = 0; uX < desc.uWidth; uX++)
			{
				pAddress[uX * 4] = pSrc[uX * 4 + 2];
				pAddress[uX * 4 + 1] = pSrc[uX * 4 + 1];
				pAddress[uX * 4 + 2] = pSrc[uX * 4];
				pAddress[uX * 4 + 3] = pSrc[uX * 4 + 3];
			}
		}
		pTex->Unmap();
	}
	delete[] pBuf;

	return pTex;
}

const Char* DragDropAssetID = "AssetInfo";
const Char* DragDropSceneObjectID = "SceneObject";

const char* AssetTypeEnumToString(AssetType eFileType)
{
	switch (eFileType)
	{
	case EFolder:			return "Folder";
	case EScene:			return "Scene";
	case EMesh:				return "Mesh";
	case ESkeleton:			return "Skeleton";
	case EMaterial:			return "Material";
	case EMaterialInstance:	return "Material Instance";
	case ETexture:			return "Texture";
	case EAnimation:		return "Animation";
	case EFont:				return "Font";
	case EAudio:			return "Audio";

	case EUnknown:
	case EFileTypeCount:
	default:break;
	}
	return "";
}
Float4 AssetTypeEnumColor(AssetType eFileType)
{
	switch (eFileType)
	{
	case EFolder:			return Float4(0.93f, 0.89f, 176, 1.0f);

	case EScene:			return Float4(1.0f, 0.5f, 0.5f, 1.0f);

	case EMesh:				return Float4(1.0f, 0.5f, 0.5f, 1.0f);
	case ESkeleton:			return Float4(1.0f, 0.5f, 0.25f, 1.0f);
	case EAnimation:		return Float4(0, 0.5f, 1.0f, 1.0f);

	case EMaterial:			return Float4(0.5f, 1.0f, 1.0f, 1.0f);
	case EMaterialInstance:	return Float4(0.5f, 1.0f, 0.5f, 1.0f);

	case ETexture:			return Float4(0.46f, 0.77f, 0.86f, 1.0f);

	case EFont:				return Float4(0.81f, 0.84f, 0.9f, 1.0f);
	case EAudio:			return Float4(0.9f, 0.75f, 0.75f, 1.0f);
	case EUnknown:
	case EFileTypeCount:
		return Float4(1.0f, 1.0f, 1.0f, 1.0f);
	}
	return Float4(1.0f, 1.0f, 1.0f, 1.0f);
}
AssetType AssetTypeStringToEnum(const char* szFileType)
{
	if (strcmp(szFileType, "Folder") == 0)
		return AssetType::EFolder;
	else if (strcmp(szFileType, "Scene") == 0)
		return AssetType::EScene;
	else if (strcmp(szFileType, "Mesh") == 0)
		return AssetType::EMesh;
	else if (strcmp(szFileType, "Skeleton") == 0)
		return AssetType::ESkeleton;
	else if (strcmp(szFileType, "Material") == 0)
		return AssetType::EMaterial;
	else if (strcmp(szFileType, "Material Instance") == 0)
		return AssetType::EMaterialInstance;
	else if (strcmp(szFileType, "Texture") == 0)
		return AssetType::ETexture;
	else if (strcmp(szFileType, "Animation") == 0)
		return AssetType::EAnimation;
	else if (strcmp(szFileType, "Font") == 0)
		return AssetType::EFont;
	else if (strcmp(szFileType, "Audio") == 0)
		return AssetType::EAudio;
	else
		return AssetType::EUnknown;
}
const char* AssetTypeFormatString(AssetType eFileType)
{
	switch (eFileType)
	{
	case EFolder:			return "*";
	case EScene:			return "mscn";
	case EMesh:				return IAppMesh::Suffix;
	case ESkeleton:			return IAppSkeleton::Suffix;
	case EMaterial:			return IAppMaterial::Suffix;
	case EMaterialInstance:	return IAppMaterialInstance::Suffix;
	case ETexture:			return IAppTexture::Suffix;
	case EAnimation:		return IAppAnimationClip::Suffix;
	case EFont:				return IAppFont::Suffix;
	case EAudio:			return IAppAudio::Suffix;

	case EUnknown:
	case EFileTypeCount:
	default:break;
	}
	return "";
}
AssetType IdentifyAssetType(const MXVisual::String& strFileName)
{
	AssetType imageID = AssetType::EUnknown;

	if (strFileName.FindLastOf(".") >= 0)
	{
		for (UInt32 u = 0; u < AssetType::EFileTypeCount; u++)
		{
			if (CheckFileSuffix(strFileName.CString(), AssetTypeFormatString((AssetType)u)))
			{
				imageID = (AssetType)u;
			}
		}
	}
	else
	{
		imageID = AssetType::EFolder;
	}
	return imageID;
}

FolderInfo::~FolderInfo()
{
	for (UInt32 u = 0; u < arrChildFolder.ElementCount(); u++)
	{
		delete arrChildFolder[u];
	}
	arrChildFolder.Clear();
}

class EditorGraphics : public IEditorGraphics
{
public:
	EditorGraphics()
		: m_pMeshLine(nullptr)
		, m_pMeshTri2D(nullptr)
		, m_pMtlTranslucency(nullptr)
		, m_pMtlTranslucency2D(nullptr)
	{
		Char buffer[Constant_MAX_PATH] = { 0 };
		GetWorkingPath(buffer);

		m_pMtlTranslucency = IAppMaterial::Create("EditorGraphics");
		m_pMtlTranslucency->SetDomain(IAppMaterial::Domain::EDM_WorldSpace);
		m_pMtlTranslucency->SetBlendMode(IAppMaterial::BlendMode::EBM_Translucency);
		m_pMtlTranslucency->SetFaceType(IAppMaterial::FaceType::EFT_Frontface);
		m_pMtlTranslucency->SetShadingModel(IAppMaterial::ShadingModel::ESM_Nolighting);
		m_pMtlTranslucency->SetParameter(
			IAppMaterial::ParameterDescription(
				"Color", IAppMaterial::EPT_Float4, Float4(1, 1, 1, 1), 0.01f
			));
		m_pMtlTranslucency->SetMaterialFunc(
			IAppMaterial::EMF_SurfaceParams,
			"OUTPUT_EMISSIVE(PCFunc_Color_GammaCorrect(input.VertexColor.rgb, 2.2f) * PCFunc_Color_GammaCorrect(Color.rgb, 2.2f));\n\
			OUTPUT_OPACITY(input.VertexColor.a * Color.a);"
		);
		m_pMtlTranslucency->Recompile();

		m_pMtlTranslucency2D = IAppMaterial::Create("EditorGraphics2D");
		m_pMtlTranslucency2D->SetDomain(IAppMaterial::Domain::EDM_ScreenSpace);
		m_pMtlTranslucency2D->SetBlendMode(IAppMaterial::BlendMode::EBM_Translucency);
		m_pMtlTranslucency2D->SetFaceType(IAppMaterial::FaceType::EFT_Frontface);
		m_pMtlTranslucency2D->SetShadingModel(IAppMaterial::ShadingModel::ESM_Nolighting);
		m_pMtlTranslucency2D->SetParameter(
			IAppMaterial::ParameterDescription(
				"Color", IAppMaterial::EPT_Float4, Float4(1, 1, 1, 1), 0.01f
			));
		m_pMtlTranslucency2D->SetMaterialFunc(
			IAppMaterial::EMF_SurfaceParams,
			"OUTPUT_EMISSIVE(PCFunc_Color_GammaCorrect(input.VertexColor.rgb, 2.2f) * PCFunc_Color_GammaCorrect(Color.rgb, 2.2f));\n\
			OUTPUT_OPACITY(input.VertexColor.a * Color.a);"
		);
		m_pMtlTranslucency2D->Recompile();
	}
	~EditorGraphics()
	{
		MX_RELEASE_INTERFACE(m_pMtlInsTranslucency);
		MX_RELEASE_INTERFACE(m_pMtlInsTranslucency2D);

		MX_RELEASE_INTERFACE(m_pMeshLine);
		MX_RELEASE_INTERFACE(m_pMeshTri2D);
		MX_RELEASE_INTERFACE(m_pMtlTranslucency);
		MX_RELEASE_INTERFACE(m_pMtlTranslucency2D);
	}

	Void Reset()
	{
		m_f4Color = Float4(1, 1, 1, 1);
		if (m_pMeshLine != nullptr)
		{
			m_pMeshLine->SetSubMesh(0, 0, 0);
		}
		m_arrLineVertex.Clear();
		if (m_pMeshTri2D != nullptr)
		{
			m_pMeshTri2D->SetSubMesh(0, 0, 0);
		}
		m_arrTriVertex2D.Clear();


		if (m_pMtlInsTranslucency == nullptr)
		{
			m_pMtlInsTranslucency = IAppMaterialInstance::Create(
				"EditorGraphics_mtlins0",
				m_pMtlTranslucency->GetName());
		}

		if (m_pMtlInsTranslucency2D == nullptr)
		{
			m_pMtlInsTranslucency2D = IAppMaterialInstance::Create(
				"EditorGraphics_mtlins1",
				m_pMtlTranslucency2D->GetName());
		}
	}
	Void Flush()
	{
		if (m_arrLineVertex.ElementCount() > 0)
		{
			if ((m_pMeshLine == nullptr || m_arrLineVertex.ElementCount() > m_pMeshLine->GetVertexCapacity()))
			{
				MX_RELEASE_INTERFACE(m_pMeshLine);

				//Triangle Index Count 
				SInt32 nIndexCount =
					Math::AlignmentedSize(m_arrLineVertex.ElementCount(), 1024);

				m_pMeshLine = IAppMesh::Create(
					"EditorGraphics_mesh_0",
					IAppMesh::EAPPMT_Line, True,
					nIndexCount, nIndexCount, 1);

				Array<UInt32> aIndex;
				for (UInt32 u = 0; u < nIndexCount; u++)
				{
					aIndex.Add(u);
				}

				m_pMeshLine->SetIndices(nIndexCount, (const UInt8*)aIndex.GetRawData());
			}

			m_pMeshLine->SetVertices(m_arrLineVertex.ElementCount(), m_arrLineVertex.GetRawData());
			m_pMeshLine->SetSubMesh(0, 0, m_arrLineVertex.ElementCount());

			EngineRenderMesh(m_pMeshLine, 0, m_pMtlInsTranslucency);
		}

		if (m_arrTriVertex2D.ElementCount() > 0)
		{
			if ((m_pMeshTri2D == nullptr || m_arrTriVertex2D.ElementCount() > m_pMeshTri2D->GetVertexCapacity()))
			{
				MX_RELEASE_INTERFACE(m_pMeshTri2D);

				//Triangle Index Count 
				SInt32 nIndexCount =
					Math::AlignmentedSize(m_arrTriVertex2D.ElementCount(), 512 * 3);

				m_pMeshTri2D = IAppMesh::Create(
					"EditorGraphics_mesh_1",
					IAppMesh::EAPPMT_Rigid2D, True,
					nIndexCount, nIndexCount, 1);

				Array<UInt32> aIndex;
				for (UInt32 u = 0; u < nIndexCount; u++)
				{
					aIndex.Add(u);
				}

				m_pMeshTri2D->SetIndices(nIndexCount, (const UInt8*)aIndex.GetRawData());
			}

			m_pMeshTri2D->SetVertices(m_arrTriVertex2D.ElementCount(), m_arrTriVertex2D.GetRawData());
			m_pMeshTri2D->SetSubMesh(0, 0, m_arrTriVertex2D.ElementCount());

			EngineRenderMesh(m_pMeshTri2D, 0, m_pMtlInsTranslucency2D);
		}
	}

	Void ChangeColor(const Float4& f4Color)
	{
		m_f4Color = f4Color;
	}

	Void RenderArrow(
		const Float3& f3Position0,
		const Float3& f3Position1,
		const Float3& f3Up,
		const Float1 fWidth = 1
	) override
	{
		const Float1 ArrowRate = 0.4f;
		const UInt32 Slice = 4;
		const Float1 RadiusStep = Constant_PI / Slice;

		Float3 dir = f3Position1 - f3Position0;
		Float3 right = Math::Normalize(Math::CrossProduct(dir, f3Up));

		right *= fWidth;
		Float3 up = f3Up * fWidth;

		Float1 s, c;
		Float3 FullOffset = right;
		Array<Float3> keyPoint(6);
		Array<Float3> nextKeyPoint(6);

		keyPoint[0] = f3Position1 - dir * ArrowRate + FullOffset;
		keyPoint[1] = f3Position1 - dir * ArrowRate + FullOffset * 0.5f;
		keyPoint[2] = f3Position0 + FullOffset * 0.5f;
		keyPoint[3] = f3Position1 - dir * ArrowRate - FullOffset;
		keyPoint[4] = f3Position1 - dir * ArrowRate - FullOffset * 0.5f;
		keyPoint[5] = f3Position0 - FullOffset * 0.5f;

		for (UInt32 u = 1; u <= Slice; u++)
		{
			s = sinf(u * RadiusStep);
			c = cosf(u * RadiusStep);
			FullOffset = up * s + right * c;

			nextKeyPoint[0] = f3Position1 - dir * ArrowRate + FullOffset;
			nextKeyPoint[1] = f3Position1 - dir * ArrowRate + FullOffset * 0.5f;
			nextKeyPoint[2] = f3Position0 + FullOffset * 0.5f;
			nextKeyPoint[3] = f3Position1 - dir * ArrowRate - FullOffset;
			nextKeyPoint[4] = f3Position1 - dir * ArrowRate - FullOffset * 0.5f;
			nextKeyPoint[5] = f3Position0 - FullOffset * 0.5f;

			RenderLine(f3Position1, keyPoint[0]);
			RenderLine(keyPoint[0], keyPoint[1]); RenderLine(keyPoint[0], nextKeyPoint[0]);
			RenderLine(keyPoint[1], keyPoint[2]); RenderLine(keyPoint[1], nextKeyPoint[1]);
			RenderLine(keyPoint[2], nextKeyPoint[2]);

			RenderLine(f3Position1, keyPoint[3]);
			RenderLine(keyPoint[3], keyPoint[4]); RenderLine(keyPoint[3], nextKeyPoint[3]);
			RenderLine(keyPoint[4], keyPoint[5]); RenderLine(keyPoint[4], nextKeyPoint[4]);
			RenderLine(keyPoint[5], nextKeyPoint[5]);

			keyPoint = nextKeyPoint;
		}
	}

	Void RenderArrow2D(
		const Float2& f2Position0,
		const Float2& f2Position1,
		const Float1 fWidth = 1
	) override
	{
		RenderLine2D(f2Position0, f2Position1, fWidth);

		Float2 Dir = f2Position1 - f2Position0;
		Float2 ExpDir = Math::Normalize(Dir);
		Float1 t = ExpDir[0];
		ExpDir[0] = ExpDir[1];
		ExpDir[1] = t;
		ExpDir = ExpDir * fWidth;

		Float2 Arrow[2];
		Arrow[0] = f2Position1 - Float2(ExpDir[1], ExpDir[0]);
		Arrow[1] = Arrow[0];

		Arrow[0] += ExpDir;
		Arrow[1] -= ExpDir;

		RenderLine2D(f2Position1, Arrow[0], fWidth);
		RenderLine2D(Arrow[0], Arrow[1], fWidth);
		RenderLine2D(Arrow[1], f2Position1, fWidth);
	}

	Void RenderLine(
		const Float3& f3Position0,
		const Float3& f3Position1,
		const Float1 fWidth = 1
	) override
	{
		IAppMesh::VertexRigid v[2];
		v[0].f4Color = m_f4Color;
		v[1].f4Color = m_f4Color;
		v[0].f4Position = Math::Combine(f3Position0, 0.0f);
		v[1].f4Position = Math::Combine(f3Position1, 0.0f);

		m_arrLineVertex.Add(v[0]);
		m_arrLineVertex.Add(v[1]);
	}

	Void RenderLine2D(
		const Float2& f2Position0,
		const Float2& f2Position1,
		const Float1 fWidth = 1
	) override
	{
		IAppMesh::VertexRigid2D v[6];
		v[0].f4Color = m_f4Color;
		v[1].f4Color = m_f4Color;
		v[2].f4Color = m_f4Color;
		v[3].f4Color = m_f4Color;
		v[4].f4Color = m_f4Color;
		v[5].f4Color = m_f4Color;

		Float2 Dir = f2Position1 - f2Position0;
		Float2 ExpDir = Math::Normalize(Dir);
		Float1 t = ExpDir[0];
		ExpDir[0] = ExpDir[1];
		ExpDir[1] = t;

		ExpDir = ExpDir * fWidth * 0.5f;

		v[0].f2Position = f2Position0 - ExpDir;
		v[1].f2Position = f2Position0 + ExpDir;
		v[2].f2Position = f2Position1 - ExpDir;
		v[3].f2Position = f2Position0 + ExpDir;
		v[4].f2Position = f2Position1 + ExpDir;
		v[5].f2Position = f2Position1 - ExpDir;

		m_arrTriVertex2D.Add(v[0]);
		m_arrTriVertex2D.Add(v[1]);
		m_arrTriVertex2D.Add(v[2]);
		m_arrTriVertex2D.Add(v[3]);
		m_arrTriVertex2D.Add(v[4]);
		m_arrTriVertex2D.Add(v[5]);
	}

	Void RenderPlane(
		const Float3& f3Position,
		const Float3& f3Scale,
		const Float3& f3Right,
		const Float3& f3Up
	) override
	{
		Float3 f3Forward = Math::CrossProduct(f3Up, f3Right);

		const Float3 p[4] = {
			f3Position - f3Right * f3Scale[0] - f3Up * f3Scale[1] - f3Forward * f3Scale[2],
			f3Position + f3Right * f3Scale[0] - f3Up * f3Scale[1] - f3Forward * f3Scale[2],
			f3Position + f3Right * f3Scale[0] - f3Up * f3Scale[1] + f3Forward * f3Scale[2],
			f3Position - f3Right * f3Scale[0] - f3Up * f3Scale[1] + f3Forward * f3Scale[2]
		};

		RenderLine(p[0], p[1]);
		RenderLine(p[1], p[2]);
		RenderLine(p[2], p[3]);
		RenderLine(p[3], p[0]);
	}

	Void RenderPlane2D(
		const Float2& f3Position,
		const Float2& f3Scale,
		Float1 fRotate
	) override
	{

	}

	Void RenderCube(
		const Float3& f3Position,
		const Float3& f3Scale,
		const Float3& f3Right,
		const Float3& f3Up
	) override
	{
		Float3 f3Forward = Math::CrossProduct(f3Up, f3Right);

		const Float3 p[8] = {
			f3Position - f3Right * f3Scale[0] - f3Up * f3Scale[1] - f3Forward * f3Scale[2],
			f3Position + f3Right * f3Scale[0] - f3Up * f3Scale[1] - f3Forward * f3Scale[2],
			f3Position + f3Right * f3Scale[0] - f3Up * f3Scale[1] + f3Forward * f3Scale[2],
			f3Position - f3Right * f3Scale[0] - f3Up * f3Scale[1] + f3Forward * f3Scale[2],

			f3Position - f3Right * f3Scale[0] + f3Up * f3Scale[1] - f3Forward * f3Scale[2],
			f3Position + f3Right * f3Scale[0] + f3Up * f3Scale[1] - f3Forward * f3Scale[2],
			f3Position + f3Right * f3Scale[0] + f3Up * f3Scale[1] + f3Forward * f3Scale[2],
			f3Position - f3Right * f3Scale[0] + f3Up * f3Scale[1] + f3Forward * f3Scale[2],
		};

		RenderLine(p[0], p[1]);
		RenderLine(p[1], p[2]);
		RenderLine(p[2], p[3]);
		RenderLine(p[3], p[0]);

		RenderLine(p[4], p[5]);
		RenderLine(p[5], p[6]);
		RenderLine(p[6], p[7]);
		RenderLine(p[7], p[4]);

		RenderLine(p[0], p[4]);
		RenderLine(p[1], p[5]);
		RenderLine(p[2], p[6]);
		RenderLine(p[3], p[7]);
	}

	Void RenderSphere(
		const Float3& f3Position,
		const Float3& f3Scale,
		const Float3& f3Right,
		const Float3& f3Up
	) override
	{
		const UInt32 Slice = 8;
		const Float1 RadiusStep = Constant_PI / Slice;

		Float3 right = f3Right * f3Scale[0];
		Float3 up = f3Up * f3Scale[1];
		Float3 forward = Math::CrossProduct(f3Up, f3Right) * f3Scale[2];

		Float1 s = sinf(0);
		Float1 c = cosf(0);
		Float3 FullOffset = up * s + right * c;
		Float3 keyPoint, nextKeyPoint;

		for (UInt32 uLatitude = 0; uLatitude < Slice; uLatitude++)
		{
			Float3 offsetForward = forward * cosf(uLatitude * RadiusStep);
			Float3 offsetRight = right * cosf(uLatitude * RadiusStep);
			Float3 offsetUp = up * sinf(uLatitude * RadiusStep);

			Float3 nextOffsetForward = forward * cosf((uLatitude + 1) * RadiusStep);
			Float3 nextOffsetRight = right * cosf((uLatitude + 1) * RadiusStep);
			Float3 nextOffsetUp = up * sinf((uLatitude + 1) * RadiusStep);

			keyPoint = offsetRight;

			for (UInt32 uLongitude = 1; uLongitude <= Slice; uLongitude++)
			{
				s = sinf(uLongitude * RadiusStep);
				c = cosf(uLongitude * RadiusStep);
				nextKeyPoint = offsetForward * s + offsetRight * c;

				RenderLine(f3Position + offsetUp + keyPoint,
					f3Position + offsetUp + nextKeyPoint);

				RenderLine(f3Position - offsetUp - keyPoint,
					f3Position - offsetUp - nextKeyPoint);

				s = sinf((uLongitude - 1) * RadiusStep);
				c = cosf((uLongitude - 1) * RadiusStep);

				RenderLine(f3Position + offsetUp + keyPoint,
					f3Position + nextOffsetUp + nextOffsetForward * s + nextOffsetRight * c);
				RenderLine(f3Position - offsetUp - keyPoint,
					f3Position - nextOffsetUp - nextOffsetForward * s - nextOffsetRight * c);

				keyPoint = nextKeyPoint;
			}
		}
	}

	Void RenderText2D(
		const Float2& Position,
		const Float2& CharScale,
		const Char* szText
	) override
	{

	}

protected:
	Float4			m_f4Color;
	IAppMesh*		m_pMeshLine;
	IAppMesh*		m_pMeshTri2D;

	Array<IAppMesh::VertexRigid>	m_arrLineVertex;
	Array<IAppMesh::VertexRigid2D>	m_arrTriVertex2D;

	IAppMaterialInstance* m_pMtlInsTranslucency;
	IAppMaterialInstance* m_pMtlInsTranslucency2D;

	IAppMaterial* m_pMtlTranslucency;
	IAppMaterial* m_pMtlTranslucency2D;
};

EditorGraphics* pGraphics = nullptr;
void eg_ResetEditorGraphics()
{
	if (pGraphics == nullptr)
	{
		pGraphics = new EditorGraphics;
	}
	pGraphics->Reset();
}
void eg_FlushEditorGraphics()
{
	if (pGraphics != nullptr)
	{
		pGraphics->Flush();
	}
}
void eg_ReleaseEditorGraphics()
{
	if (pGraphics != nullptr)
	{
		delete pGraphics;
		pGraphics = nullptr;
	}
}
IEditorGraphics* eg_GetEditorGraphics()
{
	return pGraphics;
}

ICObject* pObj = nullptr;
void eg_SelectObject(const Char* szObjName)
{
	if (szObjName == nullptr)
	{
		pObj = nullptr;
		return;
	}

	CScene* pActScene = CScene::ActivedScene();
	if (pActScene != 0)
	{
		pObj = pActScene->GetObject(szObjName);
	}
}
ICObject* eg_GetSelectedObject()
{
	return pObj;
}


Float4 ITransformEditControl::Color_AXIS_X(1, 0, 0, 1);
Float4 ITransformEditControl::Color_AXIS_Y(0, 1, 0, 1);
Float4 ITransformEditControl::Color_AXIS_Z(0, 0, 1, 1);
Float4 ITransformEditControl::Color_Selected(1, 1, 1, 1);
Float1 ITransformEditControl::IntersectionThresold(2.0f);

bool ITransformEditControl::PointAtLine(const Float2& p,
	const Float2& lp0, const Float2& lp1, Float1 fThresold)
{
	Float1 l = Math::Length(lp0 - lp1);

	Float2 v0 = p - lp0;
	Float2 v1 = p - lp1;
	Float1 s = v0[0] * v1[1] - v1[0] * v0[1];
	s = abs(s) * 0.5f;
	return s < fThresold * l;
}
Float1 ITransformEditControl::ComputeDragMotion2D(
	const Float2& linePoint0,
	const Float2& linePoint1,
	const Float2& mouseDelta)
{
	return Math::DotProduct(mouseDelta, Math::Normalize(linePoint1 - linePoint0));
}
Float1 ITransformEditControl::ComputeDragMotion3D(
	const Float3& linePoint0,
	const Float3& linePoint1,
	const Matrix4& WorldToClip,
	const Matrix4& WClipToSreen,
	const Float2& mouseDelta)
{
	Float4 p0 = Math::MatrixTransform(Math::Combine(linePoint0, 1.0f), WorldToClip);
	Float4 p1 = Math::MatrixTransform(Math::Combine(linePoint1, 1.0f), WorldToClip);
	p0 /= p0[3];
	p1 /= p1[3];
	p0 = Math::MatrixTransform(p0, WClipToSreen);
	p1 = Math::MatrixTransform(p1, WClipToSreen);

	return ComputeDragMotion2D(Float2(p0[0], p0[1]), Float2(p1[0], p1[1]), mouseDelta);
}


class TranslationEditControl : public ITransformEditControl
{
public:
	void Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale) override;
	MXVisual::Boolean Transform(
		const MXVisual::Matrix4& WorldToClip,
		const MXVisual::Matrix4& WClipToSreen,
		const MXVisual::Float2& mousePos,
		MXVisual::Boolean bDoModify)  override;
	void Render() override;

	TranslationEditControl();
protected:
	MXVisual::CObject*	m_pObj;

	//XYZ XY XY YZ YZ XZ XZ
	MXVisual::Float3	linePoint[9][2];
	MXVisual::UInt32	lineSelectedBit[9];
};

class Translation2DEditControl : public ITransformEditControl
{
public:
	void Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale) override;
	MXVisual::Boolean Transform(
		const MXVisual::Matrix4& WorldToClip,
		const MXVisual::Matrix4& WClipToSreen,
		const MXVisual::Float2& mousePos,
		MXVisual::Boolean bDoModify) override;
	void Render() override;

	Translation2DEditControl();
protected:
	MXVisual::CObject2D*m_pObj;

	MXVisual::Float2	linePoint[4][2];
	MXVisual::UInt32	lineSelectedBit[4];
};

class ScaleEditControl : public ITransformEditControl
{
public:
	void Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale) override;
	MXVisual::Boolean Transform(
		const MXVisual::Matrix4& WorldToClip,
		const MXVisual::Matrix4& WClipToSreen,
		const MXVisual::Float2& mousePos,
		MXVisual::Boolean bDoModify) override;
	void Render() override;

	ScaleEditControl();
protected:
	MXVisual::CObject*	m_pObj;

	//XYZ XY YZ XZ
	MXVisual::Float3	linePoint[6][2];
	MXVisual::UInt32	lineSelectedBit[6];
};

class Scale2DEditControl : public ITransformEditControl
{
public:
	void Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale) override;
	MXVisual::Boolean Transform(
		const MXVisual::Matrix4& WorldToClip,
		const MXVisual::Matrix4& WClipToSreen,
		const MXVisual::Float2& mousePos,
		MXVisual::Boolean bDoModify) override;
	void Render() override;

	Scale2DEditControl();
protected:
	MXVisual::CObject2D*	m_pObj;

	//X Y XY
	MXVisual::Float2	linePoint[3][2];
	MXVisual::UInt32	lineSelectedBit[3];
};

class RotationEditControl : public ITransformEditControl
{
public:
	void Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale) override;
	MXVisual::Boolean Transform(
		const MXVisual::Matrix4& WorldToClip,
		const MXVisual::Matrix4& WClipToSreen,
		const MXVisual::Float2& mousePos,
		MXVisual::Boolean bDoModify) override;
	void Render() override;

	RotationEditControl();
protected:
	MXVisual::CObject*	m_pObj;

	const static MXVisual::UInt32 SectionCount = 32;
	//XYZ
	MXVisual::Float3	linePoint[3][SectionCount][2];
	MXVisual::UInt32	lineSelectedBit[3];
	MXVisual::UInt32	lineSelectedIndex;
	MXVisual::Float3	rotateAxis[3];
};

class Rotation2DEditControl : public ITransformEditControl
{
public:
	void Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale) override;
	MXVisual::Boolean Transform(
		const MXVisual::Matrix4& WorldToClip,
		const MXVisual::Matrix4& WClipToSreen,
		const MXVisual::Float2& mousePos,
		MXVisual::Boolean bDoModify) override;
	void Render() override;

	Rotation2DEditControl();
protected:
	MXVisual::CObject2D*	m_pObj;

	const static MXVisual::UInt32 SectionCount = 32;

	//Figure X Y Dir
	MXVisual::Float2	figureLinePoint[2][2];
	//Z
	MXVisual::Float2	linePoint[SectionCount][2];
	MXVisual::UInt32	lineSelectedBit;
	MXVisual::UInt32	lineSelectedIndex;
};

TranslationEditControl::TranslationEditControl()
	: m_pObj(nullptr)
	, ITransformEditControl()
{
	lineSelectedBit[0] = EditBit::EB_X;
	lineSelectedBit[1] = EditBit::EB_Y;
	lineSelectedBit[2] = EditBit::EB_Z;
	lineSelectedBit[3] = EditBit::EB_XY;
	lineSelectedBit[4] = EditBit::EB_XY;
	lineSelectedBit[5] = EditBit::EB_YZ;
	lineSelectedBit[6] = EditBit::EB_YZ;
	lineSelectedBit[7] = EditBit::EB_XZ;
	lineSelectedBit[8] = EditBit::EB_XZ;
}
void TranslationEditControl::Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale)
{
	m_pObj = dynamic_cast<MXVisual::CObject*>(pObj);
	if (m_pObj == nullptr)return;

	linePoint[0][0] = linePoint[1][0] = linePoint[2][0] = m_pObj->GetPosition(False);

	linePoint[0][1] = MXVisual::Math::AXIS_X;
	linePoint[1][1] = MXVisual::Math::AXIS_Y;
	linePoint[2][1] = MXVisual::Math::AXIS_Z;
	if (bLocal)
	{
		linePoint[0][1] = m_pObj->GetDirRight();
		linePoint[1][1] = m_pObj->GetDirUp();
		linePoint[2][1] = m_pObj->GetDirForward();
	}
	linePoint[0][1] *= fScale;//X
	linePoint[1][1] *= fScale;//Y
	linePoint[2][1] *= fScale;//Z

	//XY
	linePoint[3][0] = linePoint[0][0] + linePoint[0][1] / 3.0f;
	linePoint[4][0] = linePoint[0][0] + linePoint[1][1] / 3.0f;
	linePoint[3][1] = linePoint[4][1] = linePoint[3][0] + linePoint[1][1] / 3.0f;
	//YZ
	linePoint[5][0] = linePoint[0][0] + linePoint[1][1] / 3.0f;
	linePoint[6][0] = linePoint[0][0] + linePoint[2][1] / 3.0f;
	linePoint[5][1] = linePoint[6][1] = linePoint[5][0] + linePoint[2][1] / 3.0f;
	//XZ
	linePoint[7][0] = linePoint[0][0] + linePoint[0][1] / 3.0f;
	linePoint[8][0] = linePoint[0][0] + linePoint[2][1] / 3.0f;
	linePoint[7][1] = linePoint[8][1] = linePoint[7][0] + linePoint[2][1] / 3.0f;

	linePoint[0][1] += linePoint[0][0];
	linePoint[1][1] += linePoint[0][0];
	linePoint[2][1] += linePoint[0][0];
}
Boolean TranslationEditControl::Transform(
	const Matrix4& WorldToClip,
	const Matrix4& WClipToSreen,
	const Float2& mousePos,
	Boolean bDoModify)
{
	Float2 delta = mousePos - m_f2CheckPoint;

	if (m_bEditing)
	{
		Float3 move;
		if (m_editBit & EB_X)
		{
			move += (linePoint[0][1] - linePoint[0][0]) *
				ComputeDragMotion3D(linePoint[0][0], linePoint[0][1],
					WorldToClip, WClipToSreen, delta) / Math::Length(Float2(WClipToSreen.Get(0, 0), WClipToSreen.Get(1, 1)));
		}
		if (m_editBit & EB_Y)
		{
			move += (linePoint[1][1] - linePoint[1][0]) *
				ComputeDragMotion3D(linePoint[1][0], linePoint[1][1],
					WorldToClip, WClipToSreen, delta) / Math::Length(Float2(WClipToSreen.Get(0, 0), WClipToSreen.Get(1, 1)));
		}
		if (m_editBit & EB_Z)
		{
			move += (linePoint[2][1] - linePoint[2][0]) *
				ComputeDragMotion3D(linePoint[2][0], linePoint[2][1],
					WorldToClip, WClipToSreen, delta) / Math::Length(Float2(WClipToSreen.Get(0, 0), WClipToSreen.Get(1, 1)));
		}
		m_pObj->TransMove(move, 1.0f);

		m_bEditing = bDoModify;
	}
	else
	{
		m_editBit = 0;
		for (UInt32 u = 0; u < 9; u++)
		{
			Float4 p0 = Math::MatrixTransform(Math::Combine(linePoint[u][0], 1.0f), WorldToClip);
			Float4 p1 = Math::MatrixTransform(Math::Combine(linePoint[u][1], 1.0f), WorldToClip);
			p0 /= p0[3];
			p1 /= p1[3];
			p0 = Math::MatrixTransform(p0, WClipToSreen);
			p1 = Math::MatrixTransform(p1, WClipToSreen);
			if (PointAtLine(mousePos, Float2(p0[0], p0[1]), Float2(p1[0], p1[1]), IntersectionThresold))
			{
				m_editBit = lineSelectedBit[u];
				m_bEditing = bDoModify;
				break;
			}
		}
	}
	m_f2CheckPoint = mousePos;
	return m_bEditing;
}
void TranslationEditControl::Render()
{
	for (UInt32 u = 0; u < 9; u++)
	{
		Float4 color =
			(lineSelectedBit[u] & EditBit::EB_X ? Color_AXIS_X : Float4()) +
			(lineSelectedBit[u] & EditBit::EB_Y ? Color_AXIS_Y : Float4()) +
			(lineSelectedBit[u] & EditBit::EB_Z ? Color_AXIS_Z : Float4());

		pGraphics->ChangeColor((m_editBit & lineSelectedBit[u]) == lineSelectedBit[u] ? Color_Selected : color);
		pGraphics->RenderLine(linePoint[u][0], linePoint[u][1]);
	}
}

Translation2DEditControl::Translation2DEditControl()
	: m_pObj(nullptr)
	, ITransformEditControl()
{
	lineSelectedBit[0] = EditBit::EB_X;
	lineSelectedBit[1] = EditBit::EB_Y;
	lineSelectedBit[2] = EditBit::EB_XY;
	lineSelectedBit[3] = EditBit::EB_XY;
}
void Translation2DEditControl::Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale)
{
	m_pObj = dynamic_cast<MXVisual::CObject2D*>(pObj);
	if (m_pObj == nullptr)return;

	linePoint[0][0] = linePoint[1][0] = m_pObj->GetPosition(False);

	linePoint[0][1] = Float2(1, 0);
	linePoint[1][1] = Float2(0, 1);
	if (bLocal)
	{
		linePoint[0][1] = m_pObj->GetDirRight();
		linePoint[1][1] = m_pObj->GetDirUp();
	}
	linePoint[0][1] *= fScale;//X
	linePoint[1][1] *= -fScale;//Y

	//XY
	linePoint[2][0] = linePoint[0][0] + linePoint[0][1] / 3.0f;
	linePoint[3][0] = linePoint[0][0] + linePoint[1][1] / 3.0f;
	linePoint[2][1] = linePoint[3][1] = linePoint[2][0] + linePoint[1][1] / 3.0f;

	linePoint[0][1] += linePoint[0][0];
	linePoint[1][1] += linePoint[0][0];
}
Boolean Translation2DEditControl::Transform(
	const Matrix4& WorldToClip,
	const Matrix4& WClipToSreen,
	const Float2& mousePos,
	Boolean bDoModify)
{
	Float2 delta = mousePos - m_f2CheckPoint;

	if (m_bEditing)
	{
		Float2 move;
		if (m_editBit & EB_X)
		{
			move += Math::Normalize(linePoint[0][1] - linePoint[0][0]) *
				ComputeDragMotion2D(linePoint[0][0], linePoint[0][1], delta);
		}
		if (m_editBit & EB_Y)
		{
			move += Math::Normalize(linePoint[1][1] - linePoint[1][0]) *
				ComputeDragMotion2D(linePoint[1][0], linePoint[1][1], delta);
		}
		m_pObj->TransMove(move, 1.0f);

		m_bEditing = bDoModify;
	}
	else
	{
		m_editBit = 0;
		for (UInt32 u = 0; u < 4; u++)
		{
			if (PointAtLine(mousePos, linePoint[u][0], linePoint[u][1], IntersectionThresold))
			{
				m_editBit = lineSelectedBit[u];
				m_bEditing = bDoModify;
				break;
			}
		}
	}
	m_f2CheckPoint = mousePos;
	return m_bEditing;
}
void Translation2DEditControl::Render()
{
	for (UInt32 u = 0; u < 4; u++)
	{
		Float4 color =
			(lineSelectedBit[u] & EditBit::EB_X ? Color_AXIS_X : Float4()) +
			(lineSelectedBit[u] & EditBit::EB_Y ? Color_AXIS_Y : Float4());

		pGraphics->ChangeColor((m_editBit & lineSelectedBit[u]) == lineSelectedBit[u] ? Color_Selected : color);
		pGraphics->RenderLine2D(linePoint[u][0], linePoint[u][1]);
	}
}

ScaleEditControl::ScaleEditControl()
	: m_pObj(nullptr)
	, ITransformEditControl()
{
	lineSelectedBit[0] = EditBit::EB_X;
	lineSelectedBit[1] = EditBit::EB_Y;
	lineSelectedBit[2] = EditBit::EB_Z;
	lineSelectedBit[3] = EditBit::EB_XY;
	lineSelectedBit[4] = EditBit::EB_YZ;
	lineSelectedBit[5] = EditBit::EB_XZ;
}
void ScaleEditControl::Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale)
{
	m_pObj = dynamic_cast<MXVisual::CObject*>(pObj);
	if (m_pObj == nullptr)return;

	linePoint[0][0] = linePoint[1][0] = linePoint[2][0] = m_pObj->GetPosition(False);

	linePoint[0][1] = MXVisual::Math::AXIS_X;
	linePoint[1][1] = MXVisual::Math::AXIS_Y;
	linePoint[2][1] = MXVisual::Math::AXIS_Z;
	if (bLocal)
	{
		linePoint[0][1] = m_pObj->GetDirRight();
		linePoint[1][1] = m_pObj->GetDirUp();
		linePoint[2][1] = m_pObj->GetDirForward();
	}
	linePoint[0][1] *= fScale;//X
	linePoint[1][1] *= fScale;//Y
	linePoint[2][1] *= fScale;//Z

	//XY
	linePoint[3][0] = linePoint[0][0] + linePoint[0][1] / 3.0f;
	linePoint[3][1] = linePoint[0][0] + linePoint[1][1] / 3.0f;
	//YZ
	linePoint[4][0] = linePoint[0][0] + linePoint[1][1] / 3.0f;
	linePoint[4][1] = linePoint[0][0] + linePoint[2][1] / 3.0f;
	//XZ
	linePoint[5][0] = linePoint[0][0] + linePoint[2][1] / 3.0f;
	linePoint[5][1] = linePoint[0][0] + linePoint[0][1] / 3.0f;

	linePoint[0][1] += linePoint[0][0];
	linePoint[1][1] += linePoint[0][0];
	linePoint[2][1] += linePoint[0][0];
}
Boolean ScaleEditControl::Transform(
	const Matrix4& WorldToClip,
	const Matrix4& WClipToSreen,
	const Float2& mousePos,
	Boolean bDoModify)
{
	Float2 delta = mousePos - m_f2CheckPoint;

	if (m_bEditing)
	{
		Float3 move;
		if (m_editBit & EB_X)
		{
			move += (linePoint[0][1] - linePoint[0][0]) *
				ComputeDragMotion3D(linePoint[0][0], linePoint[0][1],
					WorldToClip, WClipToSreen, delta) / Math::Length(Float2(WClipToSreen.Get(0, 0), WClipToSreen.Get(1, 1)));
		}
		if (m_editBit & EB_Y)
		{
			move += (linePoint[1][1] - linePoint[1][0]) *
				ComputeDragMotion3D(linePoint[1][0], linePoint[1][1],
					WorldToClip, WClipToSreen, delta) / Math::Length(Float2(WClipToSreen.Get(0, 0), WClipToSreen.Get(1, 1)));
		}
		if (m_editBit & EB_Z)
		{
			move += (linePoint[2][1] - linePoint[2][0]) *
				ComputeDragMotion3D(linePoint[2][0], linePoint[2][1],
					WorldToClip, WClipToSreen, delta) / Math::Length(Float2(WClipToSreen.Get(0, 0), WClipToSreen.Get(1, 1)));
		}
		m_pObj->TransScale(move, 1.0f);

		m_bEditing = bDoModify;
	}
	else
	{
		m_editBit = 0;
		for (UInt32 u = 0; u < 6; u++)
		{
			Float4 p0 = Math::MatrixTransform(Math::Combine(linePoint[u][0], 1.0f), WorldToClip);
			Float4 p1 = Math::MatrixTransform(Math::Combine(linePoint[u][1], 1.0f), WorldToClip);
			p0 /= p0[3];
			p1 /= p1[3];
			p0 = Math::MatrixTransform(p0, WClipToSreen);
			p1 = Math::MatrixTransform(p1, WClipToSreen);
			if (PointAtLine(mousePos, Float2(p0[0], p0[1]), Float2(p1[0], p1[1]), IntersectionThresold))
			{
				m_editBit = lineSelectedBit[u];
				m_bEditing = bDoModify;
				break;
			}
		}
	}
	m_f2CheckPoint = mousePos;
	return m_bEditing;
}
void ScaleEditControl::Render()
{
	for (UInt32 u = 0; u < 6; u++)
	{
		Float4 color =
			(lineSelectedBit[u] & EditBit::EB_X ? Color_AXIS_X : Float4()) +
			(lineSelectedBit[u] & EditBit::EB_Y ? Color_AXIS_Y : Float4()) +
			(lineSelectedBit[u] & EditBit::EB_Z ? Color_AXIS_Z : Float4());

		pGraphics->ChangeColor((m_editBit & lineSelectedBit[u]) == lineSelectedBit[u] ? Color_Selected : color);
		pGraphics->RenderLine(linePoint[u][0], linePoint[u][1]);
	}
}

Scale2DEditControl::Scale2DEditControl()
	: m_pObj(nullptr)
	, ITransformEditControl()
{
	lineSelectedBit[0] = EditBit::EB_X;
	lineSelectedBit[1] = EditBit::EB_Y;
	lineSelectedBit[2] = EditBit::EB_XY;
}
void Scale2DEditControl::Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale)
{
	m_pObj = dynamic_cast<MXVisual::CObject2D*>(pObj);
	if (m_pObj == nullptr)return;

	linePoint[0][0] = linePoint[1][0] = m_pObj->GetPosition(False);

	linePoint[0][1] = Float2(1, 0);
	linePoint[1][1] = Float2(0, 1);
	if (bLocal)
	{
		linePoint[0][1] = m_pObj->GetDirRight();
		linePoint[1][1] = m_pObj->GetDirUp();
	}
	linePoint[0][1] *= fScale;//X
	linePoint[1][1] *= -fScale;//Y

	//XY
	linePoint[2][0] = linePoint[0][0] + linePoint[0][1] / 3.0f;
	linePoint[2][1] = linePoint[1][0] + linePoint[1][1] / 3.0f;

	linePoint[0][1] += linePoint[0][0];
	linePoint[1][1] += linePoint[1][0];
}
Boolean Scale2DEditControl::Transform(
	const Matrix4& WorldToClip,
	const Matrix4& WClipToSreen,
	const Float2& mousePos,
	Boolean bDoModify)
{
	Float2 delta = mousePos - m_f2CheckPoint;

	if (m_bEditing)
	{
		Float2 move;
		if (m_editBit & EB_X)
		{
			move += Math::Normalize(linePoint[0][1] - linePoint[0][0]) *
				ComputeDragMotion2D(linePoint[0][0], linePoint[0][1], delta);
		}
		if (m_editBit & EB_Y)
		{
			move += Math::Normalize(linePoint[1][1] - linePoint[1][0]) *
				ComputeDragMotion2D(linePoint[1][0], linePoint[1][1], delta);
		}
		m_pObj->TransScale(move, 1.0f);

		m_bEditing = bDoModify;
	}
	else
	{
		m_editBit = 0;
		for (UInt32 u = 0; u < 3; u++)
		{
			if (PointAtLine(mousePos, linePoint[u][0], linePoint[u][1], IntersectionThresold))
			{
				m_editBit = lineSelectedBit[u];
				m_bEditing = bDoModify;
				break;
			}
		}
	}
	m_f2CheckPoint = mousePos;
	return m_bEditing;
}
void Scale2DEditControl::Render()
{
	for (UInt32 u = 0; u < 3; u++)
	{
		Float4 color =
			(lineSelectedBit[u] & EditBit::EB_X ? Color_AXIS_X : Float4()) +
			(lineSelectedBit[u] & EditBit::EB_Y ? Color_AXIS_Y : Float4());

		pGraphics->ChangeColor((m_editBit & lineSelectedBit[u]) == lineSelectedBit[u] ? Color_Selected : color);
		pGraphics->RenderLine2D(linePoint[u][0], linePoint[u][1]);
	}
}

RotationEditControl::RotationEditControl()
	: m_pObj(nullptr)
	, ITransformEditControl()
{
	lineSelectedBit[0] = EditBit::EB_X;
	lineSelectedBit[1] = EditBit::EB_Y;
	lineSelectedBit[2] = EditBit::EB_Z;
}
void RotationEditControl::Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale)
{
	m_pObj = dynamic_cast<MXVisual::CObject*>(pObj);
	if (m_pObj == nullptr)return;

	Float3 p = m_pObj->GetPosition(False);
	Float3 DirX = MXVisual::Math::AXIS_X;
	Float3 DirY = MXVisual::Math::AXIS_Y;
	Float3 DirZ = MXVisual::Math::AXIS_Z;
	if (bLocal)
	{
		DirX = m_pObj->GetDirRight();
		DirY = m_pObj->GetDirUp();
		DirZ = m_pObj->GetDirForward();
	}
	DirX *= fScale;
	DirY *= fScale;
	DirZ *= fScale;

	rotateAxis[0] = DirX;
	rotateAxis[1] = DirY;
	rotateAxis[2] = DirZ;

	Float1 fPerRate = Constant_PI * 2.0f / SectionCount;
	Float1 fRate = 0.0f;
	for (UInt32 uSection = 0; uSection < SectionCount; uSection++)
	{
		linePoint[0][uSection][0] = p + DirY * sinf(fRate) + DirZ * cosf(fRate);

		fRate += fPerRate;
		linePoint[0][uSection][1] = p + DirY * sinf(fRate) + DirZ * cosf(fRate);
	}

	fRate = 0.0f;
	for (UInt32 uSection = 0; uSection < SectionCount; uSection++)
	{
		linePoint[1][uSection][0] = p + DirZ * sinf(fRate) + DirX * cosf(fRate);

		fRate += fPerRate;
		linePoint[1][uSection][1] = p + DirZ * sinf(fRate) + DirX * cosf(fRate);
	}

	fRate = 0.0f;
	for (UInt32 uSection = 0; uSection < SectionCount; uSection++)
	{
		linePoint[2][uSection][0] = p + DirY * sinf(fRate) + DirX * cosf(fRate);

		fRate += fPerRate;
		linePoint[2][uSection][1] = p + DirY * sinf(fRate) + DirX * cosf(fRate);
	}
}
Boolean RotationEditControl::Transform(
	const Matrix4& WorldToClip,
	const Matrix4& WClipToSreen,
	const Float2& mousePos,
	Boolean bDoModify)
{
	Float2 delta = mousePos - m_f2CheckPoint;

	if (m_bEditing)
	{
		Float1 degree = 6.0f;
		if (m_editBit & EB_X)
		{
			degree *= ComputeDragMotion3D(linePoint[0][lineSelectedIndex][0], linePoint[0][lineSelectedIndex][1],
				WorldToClip, WClipToSreen, delta) / Math::Length(Float2(WClipToSreen.Get(0, 0), WClipToSreen.Get(1, 1)));

			m_pObj->TransRotate(rotateAxis[0], Math::Degree2Radian(degree));
		}
		else if (m_editBit & EB_Y)
		{
			degree *= ComputeDragMotion3D(linePoint[1][lineSelectedIndex][0], linePoint[1][lineSelectedIndex][1],
				WorldToClip, WClipToSreen, delta) / Math::Length(Float2(WClipToSreen.Get(0, 0), WClipToSreen.Get(1, 1)));
			m_pObj->TransRotate(rotateAxis[1], Math::Degree2Radian(degree));
		}
		else if (m_editBit & EB_Z)
		{
			degree *= ComputeDragMotion3D(linePoint[2][lineSelectedIndex][0], linePoint[2][lineSelectedIndex][1],
				WorldToClip, WClipToSreen, delta) / Math::Length(Float2(WClipToSreen.Get(0, 0), WClipToSreen.Get(1, 1)));
			m_pObj->TransRotate(rotateAxis[2], Math::Degree2Radian(degree));
		}

		m_bEditing = bDoModify;
	}
	else
	{
		m_editBit = 0;
		for (UInt32 u = 0; u < 3; u++)
		{
			for (UInt32 uSection = 0; uSection < SectionCount; uSection++)
			{
				Float4 p0 = Math::MatrixTransform(Math::Combine(linePoint[u][uSection][0], 1.0f), WorldToClip);
				Float4 p1 = Math::MatrixTransform(Math::Combine(linePoint[u][uSection][1], 1.0f), WorldToClip);
				p0 /= p0[3];
				p1 /= p1[3];
				p0 = Math::MatrixTransform(p0, WClipToSreen);
				p1 = Math::MatrixTransform(p1, WClipToSreen);
				if (PointAtLine(mousePos, Float2(p0[0], p0[1]), Float2(p1[0], p1[1]), IntersectionThresold))
				{
					m_editBit = lineSelectedBit[u];
					lineSelectedIndex = uSection;
					m_bEditing = bDoModify;
					break;
				}
			}
			if (m_editBit != 0)break;
		}
	}
	m_f2CheckPoint = mousePos;
	return m_bEditing;
}
void RotationEditControl::Render()
{
	for (UInt32 u = 0; u < 3; u++)
	{
		Float4 color =
			(lineSelectedBit[u] & EditBit::EB_X ? Color_AXIS_X : Float4()) +
			(lineSelectedBit[u] & EditBit::EB_Y ? Color_AXIS_Y : Float4()) +
			(lineSelectedBit[u] & EditBit::EB_Z ? Color_AXIS_Z : Float4());

		pGraphics->ChangeColor((m_editBit & lineSelectedBit[u]) == lineSelectedBit[u] ? Color_Selected : color);
		for (UInt32 uSection = 0; uSection < SectionCount; uSection++)
		{
			pGraphics->RenderLine(linePoint[u][uSection][0], linePoint[u][uSection][1]);
		}
	}
}

Rotation2DEditControl::Rotation2DEditControl()
	: m_pObj(nullptr)
	, ITransformEditControl()
{
	lineSelectedBit = EditBit::EB_Z;
}
void Rotation2DEditControl::Reset(MXVisual::ICObject* pObj, MXVisual::Boolean bLocal, MXVisual::Float1 fScale)
{
	m_pObj = dynamic_cast<MXVisual::CObject2D*>(pObj);
	if (m_pObj == nullptr)return;

	figureLinePoint[0][0] = figureLinePoint[1][0] = m_pObj->GetPosition(False);
	figureLinePoint[0][1] = Float2(1, 0);
	figureLinePoint[1][1] = Float2(0, 1);
	if (bLocal)
	{
		figureLinePoint[0][1] = m_pObj->GetDirRight();
		figureLinePoint[1][1] = m_pObj->GetDirUp();
	}
	figureLinePoint[0][1] *= fScale;
	figureLinePoint[1][1] *= -fScale;

	Float1 fPerRate = Constant_PI * 2.0f / SectionCount;
	Float1 fRate = 0.0f;
	for (UInt32 uSection = 0; uSection < SectionCount; uSection++)
	{
		linePoint[uSection][0] = figureLinePoint[0][0] +
			figureLinePoint[1][1] * sinf(fRate) +
			figureLinePoint[0][1] * cosf(fRate);

		fRate += fPerRate;
		linePoint[uSection][1] = figureLinePoint[0][0] +
			figureLinePoint[1][1] * sinf(fRate) +
			figureLinePoint[0][1] * cosf(fRate);
	}

	figureLinePoint[0][1] += figureLinePoint[0][0];
	figureLinePoint[1][1] += figureLinePoint[1][0];
}
Boolean Rotation2DEditControl::Transform(
	const Matrix4& WorldToClip,
	const Matrix4& WClipToSreen,
	const Float2& mousePos,
	Boolean bDoModify)
{
	Float2 delta = mousePos - m_f2CheckPoint;

	if (m_bEditing)
	{
		if (m_editBit & EB_Z)
		{
			Float1 degree = ComputeDragMotion2D(linePoint[lineSelectedIndex][0], linePoint[lineSelectedIndex][1], delta);

			m_pObj->TransRotate(Math::Degree2Radian(degree));
		}

		m_bEditing = bDoModify;
	}
	else
	{
		m_editBit = 0;
		for (UInt32 uSection = 0; uSection < SectionCount; uSection++)
		{
			if (PointAtLine(mousePos,
				linePoint[uSection][0], linePoint[uSection][1], IntersectionThresold))
			{
				m_editBit = lineSelectedBit;
				m_bEditing = bDoModify;
				break;
			}
		}
	}
	m_f2CheckPoint = mousePos;
	return m_bEditing;
}
void Rotation2DEditControl::Render()
{
	pGraphics->ChangeColor(Color_AXIS_X);
	pGraphics->RenderLine2D(figureLinePoint[0][0], figureLinePoint[0][1]);

	pGraphics->ChangeColor(Color_AXIS_Y);
	pGraphics->RenderLine2D(figureLinePoint[1][0], figureLinePoint[1][1]);

	pGraphics->ChangeColor((m_editBit & lineSelectedBit) == lineSelectedBit ? Color_Selected : Color_AXIS_Z);
	for (UInt32 uSection = 0; uSection < SectionCount; uSection++)
	{
		pGraphics->RenderLine2D(linePoint[uSection][0], linePoint[uSection][1]);
	}
}

ITransformEditControl* eg_GetEditControl(TransformEditType eType)
{
	static TransformEditType m_eLastType = ENone;
	static Float1 m_fAnimTrans = 0.0f;
	static TranslationEditControl	m_TEditControl;
	static Translation2DEditControl	m_T2DEditControl;
	static RotationEditControl		m_REditControl;
	static Rotation2DEditControl	m_R2DEditControl;
	static ScaleEditControl			m_SEditControl;
	static Scale2DEditControl		m_S2DEditControl;
	static ITransformEditControl*	m_pEditControl = nullptr;
	static const Char*				m_szEditControlName = "None";
	const Float1 m_fAnimTransSpeed = 0.5f;//2s

	if (m_eLastType != eType)
	{
		m_fAnimTrans = 1.0f;
		m_eLastType = eType;
	}

	m_pEditControl = nullptr;
	m_szEditControlName = "None";
	if (eg_GetSelectedObject() != nullptr)
	{
		switch (eType)
		{
		case TransformEditType::ETranslation:
			m_szEditControlName = "Translation";
			if (eg_GetSelectedObject()->GetType() == eBase2D)
				m_pEditControl = &m_T2DEditControl;
			else
				m_pEditControl = &m_TEditControl;
			break;
		case TransformEditType::ERotation:
			m_szEditControlName = "Rotation";
			if (eg_GetSelectedObject()->GetType() == eBase2D)
				m_pEditControl = &m_R2DEditControl;
			else
				m_pEditControl = &m_REditControl;
			break;
		case TransformEditType::EScale:
			m_szEditControlName = "Scale";
			if (eg_GetSelectedObject()->GetType() == eBase2D)
				m_pEditControl = &m_S2DEditControl;
			else
				m_pEditControl = &m_SEditControl;
			break;
		}
	}

	if (m_fAnimTrans > Constant_MIN_FLOAT)
	{
		ImGuiIO& io = ImGui::GetIO();
		ImGui::SetNextWindowFocus();
		ImGui::SetNextWindowPos(ImVec2(io.DisplaySize.x * 0.5f, io.DisplaySize.y * 0.5f),
			ImGuiCond_Always, ImVec2(0.5f, 0.5f));
		ImGui::SetNextWindowBgAlpha(m_fAnimTrans);
		ImGui::Begin("EditControl Change", nullptr, ImGuiWindowFlags_NoDecoration);

		ImGui::BulletText(m_szEditControlName);

		ImGui::End();

		m_fAnimTrans -= m_fAnimTransSpeed * io.DeltaTime;
	}

	return m_pEditControl;
}