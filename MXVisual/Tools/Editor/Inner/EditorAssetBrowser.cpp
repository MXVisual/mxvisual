#include "EditorAssetBrowser.h"
#include "EditorGlobal.h"
#include "EditorMainFrame.h"

#include "Editor_Module.h"
#include "MXFile.h"
#include "MXSystem.h"
#include "MXAsset.h"

#include "imgui.h"

#if PLATFORM == PLATFORM_WINDOWS
#include "windows.h"
#endif

const char* FileTypeImage[] = {
	"./Icons/IconUnknow.tga",
	"./Icons/IconFolder.tga",
	"./Icons/IconScene.tga",
	"./Icons/IconMesh.tga",
	"./Icons/IconSkeleton.tga",
	"./Icons/IconMaterial.tga",
	"./Icons/IconMaterialIns.tga",
	"./Icons/IconTexture.tga",
	"./Icons/IconAnimation.tga",
	"./Icons/IconFont.tga",
	"./Icons/IconAudio.tga",
};

namespace ImporterPanel
{
	PropertyPanel*		pImporterPanel;
	ICImporter*			pImporter;

	Boolean Layout(ModalDlgYesNoResult eResult)
	{
		if (eResult == ModalDlgYesNoResult::EYes)
		{
			if (pImporter->Import())
			{
				EditorMainFrame::RefreshDirectoryInfo();
				EditorMainFrame::LogMessage("Asset Import Succeed.");
			}
			else
			{
				EditorMainFrame::LogError("Asset Import Failed.");
			}
			return True;
		}
		else if (eResult == ModalDlgYesNoResult::ENo)
			return True;

		pImporterPanel->Layout();
		return False;
	}
}

namespace RenamePanel
{
	Char		szName[Constant_MAX_PATH];
	String		strAbsOriginName;
	AssetInfo*	pAssetInfo;

	Boolean Layout(ModalDlgYesNoResult eResult)
	{
		if (eResult == ModalDlgYesNoResult::EYes)
		{
			String strNewName = strAbsOriginName;
			strNewName.Replace(pAssetInfo->strName.CString(), szName);
			if (AppAssetManager::RenameAsset(strAbsOriginName.CString(), strNewName.CString()))
			{
				pAssetInfo->strName = szName;
			}
			return True;
		}
		else if (eResult == ModalDlgYesNoResult::ENo)
			return True;

		ImGui::InputText("Name", szName, Constant_MAX_PATH, ImGuiInputTextFlags_AutoSelectAll);
		return False;
	}
}

namespace DeleteAssetPanel
{
	String strAbsAssetName;

	Boolean Layout(ModalDlgYesNoResult eResult)
	{
		if (eResult == ModalDlgYesNoResult::EYes)
		{
			EditorMainFrame::RefreshDirectoryInfo();
			return True;
		}
		else if (eResult == ModalDlgYesNoResult::ENo)
		{
			return True;
		}

		ImGui::Text("Delete Confirm");
		return False;
	}
}

EditorAssetBrowser::EditorAssetBrowser()
	: m_fContentItemScale(0.0f)
	, m_bAssetShouldOpen(False)
	, m_pSelectedFolderInfo(nullptr)
	, m_pSelectedAssetInfo(nullptr)
	, m_szMenuName("AssetBrowserMenu")
{
	for (UInt32 u = 0; u < ARRAY_ELEMENT_COUNT(FileTypeImage); u++)
	{
		m_arrContentIconTexture.Add(eg_LoadTGATexture(FileTypeImage[u]));
	}
}
EditorAssetBrowser::~EditorAssetBrowser()
{
	for (UInt32 u = 0; u < m_arrContentIconTexture.ElementCount(); u++)
	{
		MX_RELEASE_INTERFACE(m_arrContentIconTexture[u]);
	}
	m_arrContentIconTexture.Clear();
}

void EditorAssetBrowser::Layout(const FolderInfo* pRootFolder)
{
	if (m_pSelectedFolderInfo == nullptr)
		m_pSelectedFolderInfo = pRootFolder;

	ImGui::Begin("Asset Browser");

	ImGui::BeginChild("Directory", ImVec2(256, 0), true);

	Layout_FolderTree(pRootFolder);

	ImGui::EndChild();

	ImGui::SameLine();

	Layout_FolderAssets(m_pSelectedFolderInfo);

	Layout_AssetMenu();

	ImGui::End();

	if (m_bAssetShouldOpen &&
		m_pSelectedFolderInfo != nullptr &&
		m_pSelectedAssetInfo != nullptr)
	{
		AssetOpen();
	}
}

void EditorAssetBrowser::AssetOpen()
{
	switch (m_pSelectedAssetInfo->eType)
	{
	case EFolder:
		for (UInt32 u = 0; u < m_pSelectedFolderInfo->arrChildFolder.ElementCount(); u++)
		{
			if (m_pSelectedFolderInfo->arrChildFolder[u]->strName == m_pSelectedAssetInfo->strName)
				m_pSelectedFolderInfo = m_pSelectedFolderInfo->arrChildFolder[u];
		}
		break;
	case EMesh:
	case ESkeleton:
	case EAnimation:
		//TODO!!
		break;
	case EScene:
	case EMaterial:
	case EMaterialInstance:
		EditorMainFrame::OpenAsset(m_pSelectedAssetInfo);
		break;
	case ETexture:
	case EUnknown:
	{
		String str = String::Format("explorer.exe explorer.exe %s/%s",
			m_pSelectedFolderInfo->strPath.CString(),
			m_pSelectedAssetInfo->strName.CString());

#if PLATFORM == PLATFORM_WINDOWS
		WinExec(str.CString(), SW_SHOWNORMAL);
#endif
		break;
	}
	}

	m_bAssetShouldOpen = False;
}

void EditorAssetBrowser::Layout_FolderTree(const FolderInfo* info)
{
	ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;
	flags |= info == m_pSelectedFolderInfo ? ImGuiTreeNodeFlags_Selected : 0;
	flags |= info->arrChildFolder.ElementCount() == 0 ?
		ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Bullet : 0;

	bool bOpened = ImGui::TreeNodeEx(info->strName.CString(), flags);
	if (ImGui::IsItemClicked(ImGuiMouseButton_Left))
	{
		m_pSelectedFolderInfo = info;
	}

	if (bOpened)
	{
		for (UInt32 u = 0; u < info->arrChildFolder.ElementCount(); u++)
		{
			Layout_FolderTree(info->arrChildFolder[u]);
		}

		ImGui::TreePop();
	}
}
void EditorAssetBrowser::Layout_FolderAssets(const FolderInfo* info)
{
	static ImGuiTextFilter filter;

	if (info == nullptr)return;

	ImGui::BeginGroup();

	ImGui::SetNextItemWidth(64.0f);
	ImGui::SliderFloat("LayoutScale", &m_fContentItemScale, 0.0f, 4.0f);
	ImGui::SameLine();
	filter.Draw("Filter", 128.0f);

	ImGui::BeginChild("Asset", ImVec2(0, 0), true);

	if (info != nullptr)
	{
		if (m_fContentItemScale < 1.0f)//List
		{
			for (UInt32 u = 0; u < info->arrAssetInfo.ElementCount(); u++)
			{
				if (filter.PassFilter(info->arrAssetInfo[u].strName.CString()))
				{
					if (Layout_Asset_List(
						&info->arrAssetInfo[u],
						m_pSelectedAssetInfo == &info->arrAssetInfo[u]))
					{
						m_pSelectedAssetInfo = &info->arrAssetInfo[u];
					}
					ImGui::Separator();
				}
			}
			ImGui::NewLine();
			ImGui::NewLine();
			ImGui::NewLine();
		}
		else
		{
			//thumbnail
			float w = ImGui::GetContentRegionAvailWidth();
			const Float1 f = 32.0f * m_fContentItemScale;
			UInt32 uLineItemCount = w / f;

			for (UInt32 u = 0; u < info->arrAssetInfo.ElementCount(); u++)
			{
				if (filter.PassFilter(info->arrAssetInfo[u].strName.CString()))
				{
					if (Layout_Asset_Thumbnail(
						&info->arrAssetInfo[u],
						m_pSelectedAssetInfo == &info->arrAssetInfo[u], f))
					{
						m_pSelectedAssetInfo = &info->arrAssetInfo[u];
					}
				}

				if ((u + 1) % uLineItemCount != 0)
					ImGui::SameLine();
			}
		}
	}

	if (ImGui::IsMouseClicked(ImGuiMouseButton_Right))
	{
		if (!ImGui::IsAnyItemHovered())
		{
			m_pSelectedAssetInfo = nullptr;
		}
		ImGui::OpenPopup(m_szMenuName);
	}

	ImGui::EndChild();

	ImGui::EndGroup();
}

Boolean EditorAssetBrowser::Layout_Asset_List(const AssetInfo* info, Boolean bSelected)
{
	Boolean bNewSelect =
		ImGui::Selectable(String::Format("%-10s|%s", AssetTypeEnumToString(info->eType), info->strName.CString()).CString(),
			bSelected, ImGuiSelectableFlags_SpanAllColumns | ImGuiSelectableFlags_AllowDoubleClick);

	if (bNewSelect && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
	{
		m_bAssetShouldOpen = True;
		return True;
	}

	bNewSelect |= ImGui::IsItemClicked(ImGuiMouseButton_Right);

	if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
	{
		bNewSelect = True;
		if (m_pSelectedAssetInfo == nullptr)m_pSelectedAssetInfo = info;

		ImGui::SetDragDropPayload(DragDropAssetID, &m_pSelectedAssetInfo, sizeof(m_pSelectedAssetInfo));
		ImGui::Text(m_pSelectedAssetInfo->strName.CString());
		ImGui::EndDragDropSource();
	}
	return bNewSelect;
}
Boolean EditorAssetBrowser::Layout_Asset_Thumbnail(const AssetInfo* info, Boolean bSelected, const Float1 fScale)
{
	ImGui::BeginGroup();

	Boolean bNewSelect =
		ImGui::ImageButton(m_arrContentIconTexture[info->eType], ImVec2(fScale, fScale));

	if (bNewSelect && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
	{
		m_bAssetShouldOpen = True;
		return True;
	}

	bNewSelect |= ImGui::IsItemClicked(ImGuiMouseButton_Right);

	if (ImGui::IsItemHovered())
	{
		ImGui::BeginTooltip();
		ImGui::Text(info->strName.CString());
		ImGui::EndTooltip();
	}

	if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
	{
		bNewSelect = True;
		if (m_pSelectedAssetInfo == nullptr)m_pSelectedAssetInfo = info;

		ImGui::SetDragDropPayload(DragDropAssetID, &m_pSelectedAssetInfo, sizeof(m_pSelectedAssetInfo));
		ImGui::Text(m_pSelectedAssetInfo->strName.CString());
		ImGui::EndDragDropSource();
	}

	ImVec2 textSize = ImGui::CalcTextSize(info->strName.CString());
	const Char* textBegin = info->strName.CString();
	const Char* textEnd = textSize.x <= fScale ? nullptr : &textBegin[(UInt32)(fScale / textSize.x * info->strName.Length())];
	ImGui::TextUnformatted(textBegin, textEnd);

	ImGui::EndGroup();

	return bNewSelect;
}

void EditorAssetBrowser::Layout_AssetMenu()
{
	if (ImGui::BeginPopupContextItem(m_szMenuName))
	{
		AssetType t = m_pSelectedAssetInfo != nullptr ?
			m_pSelectedAssetInfo->eType : AssetType::EUnknown;

		if (t == AssetType::EUnknown)
		{
			bool bImport = ImGui::MenuItem("Import Asset");
			ImGui::EndPopup();

			if (bImport)
				Layout_ImportDlg();
		}
		else
		{
			if (ImGui::MenuItem("Open"))
			{
				m_bAssetShouldOpen = True;
			}

			if (ImGui::MenuItem("Show In Folder"))
			{
#if PLATFORM == PLATFORM_WINDOWS
				String str = String::Format("explorer.exe %s", m_pSelectedFolderInfo->strPath.CString());
				WinExec(str.CString(), SW_SHOW);
#endif
			}

			Boolean bRename = ImGui::MenuItem("Rename");
			Boolean bDelete = ImGui::MenuItem("Delete");
			ImGui::Separator();

			switch (t)
			{
			case EFolder:
				bRename |= ImGui::MenuItem("Create Material");
				break;
			case EMaterial:
				if (ImGui::MenuItem("Create Material Instance"))
				{
					String strMaterialName = String::Format("%s/%s/%s",
						m_pSelectedFolderInfo->strPath.CString(),
						m_pSelectedFolderInfo->strName.CString(),
						m_pSelectedAssetInfo->strName.CString());
					IAppMaterialInstance* pTemp = IAppMaterialInstance::Create("TEMP", strMaterialName.CString());
					if (pTemp == nullptr)
						EditorMainFrame::LogError("Create Material Instance Failed");

					String strMaterialInstanceName = String::Format("%s/%s/NewMaterialInstance.%s",
						m_pSelectedFolderInfo->strPath.CString(),
						m_pSelectedFolderInfo->strName.CString(),
						IAppMaterialInstance::Suffix);
					if(!AppAssetManager::SaveAsset(pTemp, strMaterialInstanceName.CString()))
						EditorMainFrame::LogError("Create Material Instance Failed");

					EditorMainFrame::RefreshDirectoryInfo();
					m_pSelectedAssetInfo = nullptr;
					m_pSelectedFolderInfo = nullptr;
				}
				break;
			case EMesh:
			case ESkeleton:
			case EMaterialInstance:
			case ETexture:
			case EAnimation:
				break;
			}

			if (bRename)
			{
				RenamePanel::pAssetInfo = const_cast<AssetInfo*>(m_pSelectedAssetInfo);
				strcpy(RenamePanel::szName, m_pSelectedAssetInfo->strName.CString());
				RenamePanel::strAbsOriginName = String::Format("%s/%s/%s",
					m_pSelectedFolderInfo->strPath.CString(),
					m_pSelectedFolderInfo->strName.CString(),
					m_pSelectedAssetInfo->strName.CString());
				EditorMainFrame::ModalDlg(RenamePanel::Layout);
			}
			else if (bDelete)
			{
				DeleteAssetPanel::strAbsAssetName = String::Format("%s/%s/%s",
					m_pSelectedFolderInfo->strPath.CString(),
					m_pSelectedFolderInfo->strName.CString(),
					m_pSelectedAssetInfo->strName.CString());
				EditorMainFrame::ModalDlg(DeleteAssetPanel::Layout);
			}

			ImGui::EndPopup();
		}
	}
}
void EditorAssetBrowser::Layout_ImportDlg()
{
	MX_DELETE(m_pActivedImporter);

	Char buf[Constant_MAX_PATH] = { 0 };
	if (System::DlgSelectFile("Import File", buf, Constant_MAX_PATH))
	{
		Array<Editor_Importer::Description> arrDesc;
		Editor_Importer::Enumerate(arrDesc);

		for (UInt32 uImporter = 0; uImporter < arrDesc.ElementCount(); uImporter++)
		{
			for (UInt32 uFormat = 0; uFormat < arrDesc[uImporter].SupportedFormatCount; uFormat++)
			{
				if (CheckFileSuffix(buf, arrDesc[uImporter].SupportedFormat[uFormat]))
				{
					m_pActivedImporter = Editor_Importer::Create(arrDesc[uImporter].Name);
					m_pActivedImporter->SetImportFileName(buf);
					m_pActivedImporter->SetImportTargetPath(String::Format("%s/%s", m_pSelectedFolderInfo->strPath.CString(), m_pSelectedFolderInfo->strName.CString()).CString());
					m_ImporterPanel.SetPropertyObject("Configure", dynamic_cast<ICSupportedEditorPropertyPanel*>(m_pActivedImporter));

					ImporterPanel::pImporterPanel = &m_ImporterPanel;
					ImporterPanel::pImporter = m_pActivedImporter;
					EditorMainFrame::ModalDlg(ImporterPanel::Layout);
					return;
				}
			}
		}
	}
}
