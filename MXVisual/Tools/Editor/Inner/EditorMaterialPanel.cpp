#include "EditorMaterialPanel.h"
#include "EditorMainFrame.h"

#include "PropertyPage.h"
#include "MXFile.h"
#include "MXLog.h"
#include "MXAsset.h"
#include "MXSystem.h"

#include "imgui.h"

const Char* arrMaterialFunctionName[] = {
	"Custom",
	"Surface",
	"Mesh"
};

Boolean IsMaterialFunctionEnable(
	IAppMaterial::Domain eDomain,
	IAppMaterial::MaterialFunction eFuncType)
{
	if (eDomain == IAppMaterial::Domain::EDM_WorldSpace)
	{
		return eFuncType == IAppMaterial::MaterialFunction::EMF_MeshParams ||
			eFuncType == IAppMaterial::MaterialFunction::EMF_SurfaceParams ||
			eFuncType == IAppMaterial::MaterialFunction::EMF_Custom;
	}
	else if (eDomain == IAppMaterial::Domain::EDM_ScreenSpace)
	{
		return eFuncType == IAppMaterial::MaterialFunction::EMF_SurfaceParams ||
			eFuncType == IAppMaterial::MaterialFunction::EMF_Custom;
	}
	else if (eDomain == IAppMaterial::Domain::EDM_Decal)
	{
		return eFuncType == IAppMaterial::MaterialFunction::EMF_SurfaceParams ||
			eFuncType == IAppMaterial::MaterialFunction::EMF_Custom;
	}
	return False;
}

EditorMaterialProperty::EditorMaterialProperty()
	: m_bGroupOpened(False)
{}
Void EditorMaterialProperty::BeginGroup(const Char* szName)
{
	m_bGroupOpened = ImGui::TreeNode(szName);
}
Void EditorMaterialProperty::EndGroup()
{
	if (!m_bGroupOpened) return;

	ImGui::TreePop();
}
Boolean EditorMaterialProperty::PropertyEnumeration(
	const Char* szName,
	Array<String>& arrEnumLabel,
	Array<SInt32>& arrEnumValue,
	SInt32& nValue)
{
	Boolean bChanged = False;
	ASSERT_IF_FAILED(szName != nullptr);
	if (!m_bGroupOpened) return bChanged;

	if (ImGui::BeginCombo(szName, arrEnumLabel[nValue].CString()))
	{
		for (int n = 0; n < arrEnumLabel.ElementCount(); n++)
		{
			ImGui::PushID(arrEnumValue[n]);
			if (ImGui::Selectable(arrEnumLabel[n].CString(), nValue == n))
			{
				nValue = n;
				bChanged = True;
			}
			ImGui::PopID();
		}
		ImGui::EndCombo();
	}
	return bChanged;
}
Boolean EditorMaterialProperty::PropertyFloat1(
	const Char* szName,
	Float1& fValue)
{
	ASSERT_IF_FAILED(szName != nullptr);
	if (!m_bGroupOpened) return False;

	return ImGui::DragFloat(szName, &fValue, 0.01f, 0.0f, 1.0f);
}
Boolean EditorMaterialProperty::PropertyFloat2(
	const Char* szName,
	Float2& f2Value)
{
	ASSERT_IF_FAILED(szName != nullptr);
	if (!m_bGroupOpened) return False;

	float f[2] = { f2Value[0], f2Value[1] };
	if (ImGui::DragFloat2(szName, f, 0.01f, 0.0f, 1.0f))
	{
		f2Value = Float2(f[0], f[1]);
		return True;
	}
	return False;
}
Boolean EditorMaterialProperty::PropertyFloat3(
	const Char* szName,
	Float3& f3Value)
{
	ASSERT_IF_FAILED(szName != nullptr);
	if (!m_bGroupOpened) return False;

	float f[3] = { f3Value[0], f3Value[1], f3Value[2] };
	if (ImGui::DragFloat3(szName, f, 0.01f, 0.0f, 1.0f))
	{
		f3Value = Float3(f[0], f[1], f[2]);
		return True;
	}
	return False;
}
Boolean EditorMaterialProperty::PropertyFloat4(
	const Char* szName,
	Float4& f4Value)
{
	ASSERT_IF_FAILED(szName != nullptr);
	if (!m_bGroupOpened) return False;

	float f[4] = { f4Value[0], f4Value[1], f4Value[2], f4Value[3] };
	if (ImGui::DragFloat4(szName, f, 0.01f, 0.0f, 1.0f))
	{
		f4Value = Float4(f[0], f[1], f[2], f[3]);
		return True;
	}
	return False;
}
Boolean EditorMaterialProperty::PropertyTexture(
	const Char* szName,
	String& strValue)
{
	Boolean bChanged = False;
	ASSERT_IF_FAILED(szName != nullptr);
	if (!m_bGroupOpened) return bChanged;

	Char szValue[Constant_MAX_PATH] = { 0 };
	sprintf(szValue, strValue.CString());

	String strPropertyName = String::Format("##%s", szName);
	ImGui::InputText(strPropertyName.CString(), szValue, Constant_MAX_PATH);
	if (ImGui::BeginDragDropTarget())
	{
		const ImGuiPayload* payload = ImGui::AcceptDragDropPayload(DragDropAssetID);
		if (payload != nullptr)
		{
			ASSERT_IF_FAILED(payload->DataSize == sizeof(const AssetInfo*));
			const AssetInfo* pInfo = *(const AssetInfo**)payload->Data;
			if (pInfo->pParentFolderInfo != nullptr)
			{
				sprintf(szValue, "%s/%s/%s",
					pInfo->pParentFolderInfo->strPath.CString(),
					pInfo->pParentFolderInfo->strName.CString(),
					pInfo->strName.CString());
			}
			else
			{
				sprintf(szValue, "%s", pInfo->strName.CString());
			}
			bChanged = True;
		}
		ImGui::EndDragDropTarget();
	}
	ImGui::SameLine();
	if (ImGui::Button(szName) &&
		System::DlgSelectFile("Select Asset", szValue, Constant_MAX_PATH))
	{
		bChanged = True;
	}
	bChanged &= CheckFileSuffix(szValue, IAppTexture::Suffix);
	if (bChanged)
	{
		IAppTexture* pTex = (IAppTexture*)AppAssetManager::GetAsset(szValue);
		//ת����ͼ����
		strValue = pTex->GetName();
		MX_RELEASE_INTERFACE(pTex);
	}
	return bChanged;
}
Boolean EditorMaterialProperty::PropertyMaterial(
	const Char* szName,
	String& strValue)
{
	Boolean bChanged = False;
	ASSERT_IF_FAILED(szName != nullptr);
	if (!m_bGroupOpened) return bChanged;

	Char szValue[Constant_MAX_PATH] = { 0 };
	sprintf(szValue, strValue.CString());

	String strPropertyName = String::Format("##%s", szName);
	ImGui::InputText(strPropertyName.CString(), szValue, Constant_MAX_PATH);
	if (ImGui::BeginDragDropTarget())
	{
		const ImGuiPayload* payload = ImGui::AcceptDragDropPayload(DragDropAssetID);
		if (payload != nullptr)
		{
			ASSERT_IF_FAILED(payload->DataSize == sizeof(const AssetInfo*));
			const AssetInfo* pInfo = *(const AssetInfo**)payload->Data;
			if (pInfo->pParentFolderInfo != nullptr)
			{
				sprintf(szValue, "%s/%s/%s",
					pInfo->pParentFolderInfo->strPath.CString(),
					pInfo->pParentFolderInfo->strName.CString(),
					pInfo->strName.CString());
			}
			else
			{
				sprintf(szValue, "%s", pInfo->strName.CString());
			}
			bChanged = True;
		}
		ImGui::EndDragDropTarget();
	}
	ImGui::SameLine();
	if (ImGui::Button(szName) &&
		System::DlgSelectFile("Select Asset", szValue, Constant_MAX_PATH))
	{
		bChanged = True;
	}
	bChanged &= CheckFileSuffix(szValue, IAppMaterial::Suffix);
	if (bChanged)
	{
		IAppMaterial* pMtl = (IAppMaterial*)AppAssetManager::GetAsset(szValue);
		//ת����ͼ����
		strValue = pMtl->GetName();
		MX_RELEASE_INTERFACE(pMtl);
	}
	return bChanged;
}

EditorMaterialPanel::EditorMaterialPanel()
	: m_szParameterMenuName("MaterialParameterMenu")
	, m_szCodeMenuName("MaterialCodeMenu")
{
	m_pMaterial = nullptr;
	m_bGroupOpened = False;
}
EditorMaterialPanel::~EditorMaterialPanel()
{
	MX_RELEASE_INTERFACE(m_pMaterial);
}
void EditorMaterialPanel::Layout(const char* szFile, Boolean& bModified)
{
	if (m_strMaterialName != szFile)
	{
		MX_RELEASE_INTERFACE(m_pMaterial);
		m_strMaterialName = szFile;
		m_pMaterial = (IAppMaterial*)AppAssetManager::GetAsset(szFile);

		if (m_pMaterial != nullptr)
		{
			m_eDomain = m_pMaterial->GetDomain();
			m_eShadingModel = m_pMaterial->GetShadingModel();
			m_eFaceType = m_pMaterial->GetFaceType();
			m_eBlendMode = m_pMaterial->GetBlendMode();

			for (UInt32 u = 0; u < IAppMaterial::MaterialFunction::EMF_Count; u++)
			{
				m_arrCode[u] = m_pMaterial->GetMaterialFunc((IAppMaterial::MaterialFunction)u);
			}

			m_arrParameterDefault.Clear();
			for (UInt32 n = 0; n < m_pMaterial->GetParameterCount(); n++)
			{
				m_arrParameterDefault.Add(m_pMaterial->GetParameterDescription(n));
			}
		}
	}
	if (m_pMaterial == nullptr) return;

	if (ImGui::BeginChild("Material", ImVec2(0, 0), true, ImGuiWindowFlags_MenuBar))
	{
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::MenuItem("Save", nullptr, nullptr, bModified))
			{
				bModified = !Save();
			}
			ImGui::EndMenuBar();
		}

		Layout_Parameter(bModified);
		Layout_ParameterMenu(bModified);

		Layout_Code(bModified);
		Layout_CodeMenu(bModified);

		ImGui::EndChild();
	}
}
void EditorMaterialPanel::Layout_ParameterMenu(Boolean& bModified)
{
	if (ImGui::BeginPopupContextItem(m_szParameterMenuName))
	{
		ImGui::MenuItem("Add Float1");
		ImGui::MenuItem("Add Float2");
		ImGui::MenuItem("Add Float3");
		ImGui::MenuItem("Add Float4");

		ImGui::MenuItem("Add Texture1D");
		if (ImGui::MenuItem("Add Texture2D"))
		{
			m_arrParameterDefault.Add(
				IAppMaterial::ParameterDescription("TextureParameter",
					IAppMaterial::EPT_Texture2D,
					DefaultResource::MOSAIC_TEXTURE)
			);
			bModified = True;
		}
		ImGui::MenuItem("Add Texture3D");
		ImGui::MenuItem("Add TextureCube");

		ImGui::MenuItem("Rename");
		ImGui::MenuItem("Delete");

		ImGui::EndPopup();
	}
}
void EditorMaterialPanel::Layout_CodeMenu(Boolean& bModified)
{
	if (ImGui::BeginPopupContextItem(m_szCodeMenuName))
	{
		if (ImGui::BeginMenu("MaterialCodeInput"))
		{
			switch (m_nSelectedCodePanel)
			{
			case IAppMaterial::MaterialFunction::EMF_Custom:
				break;

			case IAppMaterial::MaterialFunction::EMF_SurfaceParams:
				ImGui::MenuItem("INPUT_WORLDPOSITION");
				ImGui::MenuItem("INPUT_VERTEXCOLOR");
				ImGui::MenuItem("INPUT_TEXCOORD0");
				ImGui::MenuItem("INPUT_TANGENTTOWORLD");
				break;

			case IAppMaterial::MaterialFunction::EMF_MeshParams:
				ImGui::MenuItem("INPUT_LOCALPOSITION");
				ImGui::MenuItem("INPUT_LOCALNORMAL");
				ImGui::MenuItem("INPUT_VERTEXCOLOR");
				break;
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("MaterialCodeOutput"))
		{
			switch (m_nSelectedCodePanel)
			{
			case IAppMaterial::MaterialFunction::EMF_SurfaceParams:
				ImGui::MenuItem("OUTPUT_EMISSIVE");
				ImGui::MenuItem("OUTPUT_ALBEDO");
				ImGui::MenuItem("OUTPUT_WORLD_NORMAL");
				ImGui::MenuItem("OUTPUT_TANGENT_NORMAL");
				ImGui::MenuItem("OUTPUT_METALLIC");
				ImGui::MenuItem("OUTPUT_ROUGHNESS");
				ImGui::MenuItem("OUTPUT_OPACITY");
				ImGui::MenuItem("OUTPUT_SCREEN_DISTORT");
				ImGui::MenuItem("OUTPUT_CUSTOMDATA");
				break;

			case IAppMaterial::MaterialFunction::EMF_MeshParams:
				ImGui::MenuItem("OUTPUT_WORLDPOSITION_OFFSET");
				break;
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("MaterialCodeOperation"))
		{
			switch (m_nSelectedCodePanel)
			{
			case IAppMaterial::MaterialFunction::EMF_SurfaceParams:
			case IAppMaterial::MaterialFunction::EMF_MeshParams:
				ImGui::MenuItem("TEX1D_SAMPLE");
				ImGui::MenuItem("TEX2D_SAMPLE");
				ImGui::MenuItem("TEX3D_SAMPLE");
				ImGui::MenuItem("TEXCUBE_SAMPLE");
				ImGui::MenuItem("TEX1D_SAMPLELEVEL");
				ImGui::MenuItem("TEX2D_SAMPLELEVEL");
				ImGui::MenuItem("TEX3D_SAMPLELEVEL");
				ImGui::MenuItem("TEXCUBE_SAMPLELEVEL");
				break;
			}
			ImGui::EndMenu();
		}
		ImGui::EndPopup();
	}
}
void EditorMaterialPanel::Layout_Parameter(Boolean& bModified)
{
	BeginGroup("Configuration");
	Array<String> arrString;
	Array<SInt32> arrEnum;
#define CLEAR_ENUM_MAP() arrEnum.Clear(); arrString.Clear();
#define ADD_ENUM_MAP(e, s) arrEnum.Add((SInt32)e); arrString.Add(s);

	ADD_ENUM_MAP(IAppMaterial::Domain::EDM_WorldSpace, "WorldSpace");
	ADD_ENUM_MAP(IAppMaterial::Domain::EDM_Decal, "Decal");
	ADD_ENUM_MAP(IAppMaterial::Domain::EDM_ScreenSpace, "ScreenSpace");
	ADD_ENUM_MAP(IAppMaterial::Domain::EDM_PostProcess, "Postprocess");
	SInt32 nValue = (SInt32)m_eDomain;
	if (PropertyEnumeration("Domain", arrString, arrEnum, nValue))
	{
		m_eDomain = (IAppMaterial::Domain)nValue;
		bModified = True;
	}
	CLEAR_ENUM_MAP();

	ADD_ENUM_MAP(IAppMaterial::ShadingModel::ESM_Nolighting, "NoLighting");
	ADD_ENUM_MAP(IAppMaterial::ShadingModel::ESM_Default, "Default");
	ADD_ENUM_MAP(IAppMaterial::ShadingModel::ESM_PostprocessFur, "Postprocess Fur");
	nValue = (SInt32)m_eShadingModel;
	if (PropertyEnumeration("ShadingModel", arrString, arrEnum, nValue))
	{
		m_eShadingModel = (IAppMaterial::ShadingModel)nValue;
		bModified = True;
	}
	CLEAR_ENUM_MAP();

	ADD_ENUM_MAP(IAppMaterial::FaceType::EFT_Frontface, "Front Face");
	ADD_ENUM_MAP(IAppMaterial::FaceType::EFT_Backface, "Back Face");
	ADD_ENUM_MAP(IAppMaterial::FaceType::EFT_TwoSide, "Two Side");
	nValue = (SInt32)m_eFaceType;
	if (PropertyEnumeration("FaceType", arrString, arrEnum, nValue))
	{
		m_eFaceType = (IAppMaterial::FaceType)nValue;
		bModified = True;
	}
	CLEAR_ENUM_MAP();

	ADD_ENUM_MAP(IAppMaterial::BlendMode::EBM_Opaque, "Opaque");
	ADD_ENUM_MAP(IAppMaterial::BlendMode::EBM_Masked, "Masked");
	ADD_ENUM_MAP(IAppMaterial::BlendMode::EBM_Translucency, "Translucency");
	ADD_ENUM_MAP(IAppMaterial::BlendMode::EBM_Additive, "Additive");
	ADD_ENUM_MAP(IAppMaterial::BlendMode::EBM_MaskTranslucency, "MaskTranslucency");
	nValue = (SInt32)m_eBlendMode;
	if (PropertyEnumeration("BlendModel", arrString, arrEnum, nValue))
	{
		m_eBlendMode = (IAppMaterial::BlendMode)nValue;
		bModified = True;
	}
	CLEAR_ENUM_MAP();

	EndGroup();
#undef CLEAR_ENUM_MAP
#undef ADD_ENUM_MAP

	BeginGroup("UserParameters - DefaultValue");
	for (UInt32 n = 0; n < m_arrParameterDefault.ElementCount(); n++)
	{
		switch (m_arrParameterDefault[n].eType)
		{
		case IAppMaterial::EPT_Float1:
			PropertyFloat1(m_arrParameterDefault[n].strName.CString(), m_arrParameterDefault[n].f1Default);
			break;
		case IAppMaterial::EPT_Float2:
			PropertyFloat2(m_arrParameterDefault[n].strName.CString(), m_arrParameterDefault[n].f2Default);
			break;
		case IAppMaterial::EPT_Float3:
			PropertyFloat3(m_arrParameterDefault[n].strName.CString(), m_arrParameterDefault[n].f3Default);
			break;
		case IAppMaterial::EPT_Float4:
			PropertyFloat4(m_arrParameterDefault[n].strName.CString(), m_arrParameterDefault[n].f4Default);
			break;
		case IAppMaterial::EPT_Texture1D:
		case IAppMaterial::EPT_Texture2D:
		case IAppMaterial::EPT_Texture3D:
		case IAppMaterial::EPT_TextureCube:
			PropertyTexture(m_arrParameterDefault[n].strName.CString(), m_arrParameterDefault[n].strDefault);
			break;
		}
	}
	EndGroup();
}
void EditorMaterialPanel::Layout_Code(Boolean& bModified)
{
	m_nSelectedCodePanel = -1;
	if (ImGui::BeginTabBar("MaterialFunction"))
	{
		for (UInt32 u = 0; u < IAppMaterial::MaterialFunction::EMF_Count; u++)
		{
			if (IsMaterialFunctionEnable(m_pMaterial->GetDomain(), (IAppMaterial::MaterialFunction)u))
			{
				if (ImGui::BeginTabItem(arrMaterialFunctionName[u], nullptr,
					ImGuiTabItemFlags_NoCloseWithMiddleMouseButton))
				{
					if (m_nSelectedCodePanel != u)
					{
						m_arrCode[m_nSelectedCodePanel] = m_szCodeBuffer;
						m_nSelectedCodePanel = u;
						strcpy(m_szCodeBuffer, m_arrCode[m_nSelectedCodePanel].CString());
					}

					ImGui::InputTextMultiline("##Code", m_szCodeBuffer, sizeof(m_szCodeBuffer),
						ImVec2(-FLT_MIN, -FLT_MIN));

					ImGui::EndTabItem();
					ImGui::OpenPopupOnItemClick(m_szCodeMenuName, ImGuiPopupFlags_MouseButtonRight);
				}
			}
		}
		ImGui::EndTabBar();
	}
}
Boolean EditorMaterialPanel::Recompile()
{
	m_pMaterial->SetDomain(m_eDomain);
	m_pMaterial->SetFaceType(m_eFaceType);
	m_pMaterial->SetBlendMode(m_eBlendMode);
	m_pMaterial->SetShadingModel(m_eShadingModel);

	for (UInt32 u = 0; u < m_arrParameterDefault.ElementCount(); u++)
	{
		m_pMaterial->RemoveParameter(m_arrParameterDefault[u].strName.CString());
		m_pMaterial->SetParameter(m_arrParameterDefault[u]);
	}

	m_arrCode[m_nSelectedCodePanel] = m_szCodeBuffer;
	m_pMaterial->SetMaterialFunc(
		(IAppMaterial::MaterialFunction)m_nSelectedCodePanel,
		m_arrCode[m_nSelectedCodePanel]);

	return m_pMaterial->Recompile();
}
Boolean EditorMaterialPanel::Save()
{
	if (!Recompile())
	{
		return False;
	}

	return AppAssetManager::SaveAsset(m_pMaterial, m_strMaterialName.CString());
}

EditorMaterialInstancePanel::EditorMaterialInstancePanel()
{
	m_pMaterialInstance = nullptr;
}
EditorMaterialInstancePanel::~EditorMaterialInstancePanel()
{
	MX_RELEASE_INTERFACE(m_pMaterialInstance);
}
void EditorMaterialInstancePanel::Layout(const char* szFile, Boolean& bModified)
{
	if (m_strMaterialInstanceName != szFile)
	{
		MX_RELEASE_INTERFACE(m_pMaterialInstance);
		m_arrMaterialParameter.Clear();

		m_strMaterialInstanceName = szFile;
		m_pMaterialInstance = (IAppMaterialInstance*)AppAssetManager::GetAsset(szFile);

		if (m_pMaterialInstance != nullptr)
		{
			IAppMaterial* pMaterial =
				(IAppMaterial*)AppAssetManager::GetAsset(m_pMaterialInstance->GetRefMaterialName());

			if (pMaterial == nullptr)
			{
				m_pMaterialInstance->Release();
				m_pMaterialInstance = nullptr;
			}
			else
			{
				for (UInt32 u = 0; u < pMaterial->GetParameterCount(); u++)
				{
					m_arrMaterialParameter.Add(pMaterial->GetParameterDescription(u));
				}

				pMaterial->Release();
				pMaterial = nullptr;
			}
		}
	}
	if (m_pMaterialInstance == nullptr) return;

	if (ImGui::BeginChild("MaterialInstance", ImVec2(0, 0), true, ImGuiWindowFlags_MenuBar))
	{
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::MenuItem("Save"))
			{
				bModified = !Save();
			}
			ImGui::EndMenuBar();
		}

		BeginGroup("Configuration");
		String strMtlName = m_pMaterialInstance->GetRefMaterialName();
		if (PropertyMaterial("Material", strMtlName))
		{
			bModified = True;
		}
		EndGroup();

		BeginGroup("UserParameters");
		Float1 fValue;
		Float2 f2Value;
		Float3 f3Value;
		Float4 f4Value;
		String strValue;
		for (UInt32 u = 0; u < m_arrMaterialParameter.ElementCount(); u++)
		{
			switch (m_arrMaterialParameter[u].eType)
			{
			case IAppMaterial::EPT_Float1:
				fValue = m_pMaterialInstance->GetFloat1(m_arrMaterialParameter[u].strName.CString());
				if (PropertyFloat1(m_arrMaterialParameter[u].strName.CString(), fValue))
				{
					m_pMaterialInstance->SetFloat1(m_arrMaterialParameter[u].strName.CString(), fValue);
					bModified = True;
				}
				break;
			case IAppMaterial::EPT_Float2:
				f2Value = m_pMaterialInstance->GetFloat2(m_arrMaterialParameter[u].strName.CString());
				if (PropertyFloat2(m_arrMaterialParameter[u].strName.CString(), f2Value))
				{
					m_pMaterialInstance->SetFloat2(m_arrMaterialParameter[u].strName.CString(), f2Value);
					bModified = True;
				}
				break;
			case IAppMaterial::EPT_Float3:
				f3Value = m_pMaterialInstance->GetFloat3(m_arrMaterialParameter[u].strName.CString());
				if (PropertyFloat3(m_arrMaterialParameter[u].strName.CString(), f3Value))
				{
					m_pMaterialInstance->SetFloat3(m_arrMaterialParameter[u].strName.CString(), f3Value);
					bModified = True;
				}
				break;
			case IAppMaterial::EPT_Float4:
				f4Value = m_pMaterialInstance->GetFloat4(m_arrMaterialParameter[u].strName.CString());
				if (PropertyFloat4(m_arrMaterialParameter[u].strName.CString(), f4Value))
				{
					m_pMaterialInstance->SetFloat4(m_arrMaterialParameter[u].strName.CString(), f4Value);
					bModified = True;
				}
				break;
			case IAppMaterial::EPT_Texture1D:
				strValue = m_pMaterialInstance->GetTexture1D(m_arrMaterialParameter[u].strName.CString());
				if (PropertyTexture(m_arrMaterialParameter[u].strName.CString(), strValue))
				{
					m_pMaterialInstance->SetTexture1D(m_arrMaterialParameter[u].strName.CString(), strValue.CString());
					bModified = True;
				}
				break;
			case IAppMaterial::EPT_Texture2D:
				strValue = m_pMaterialInstance->GetTexture2D(m_arrMaterialParameter[u].strName.CString());
				if (PropertyTexture(m_arrMaterialParameter[u].strName.CString(), strValue))
				{
					m_pMaterialInstance->SetTexture2D(m_arrMaterialParameter[u].strName.CString(), strValue.CString());
					bModified = True;
				}
				break;
			case IAppMaterial::EPT_Texture3D:
				strValue = m_pMaterialInstance->GetTexture3D(m_arrMaterialParameter[u].strName.CString());
				if (PropertyTexture(m_arrMaterialParameter[u].strName.CString(), strValue))
				{
					m_pMaterialInstance->SetTexture3D(m_arrMaterialParameter[u].strName.CString(), strValue.CString());
					bModified = True;
				}
				break;
			case IAppMaterial::EPT_TextureCube:
				strValue = m_pMaterialInstance->GetTextureCube(m_arrMaterialParameter[u].strName.CString());
				if (PropertyTexture(m_arrMaterialParameter[u].strName.CString(), strValue))
				{
					m_pMaterialInstance->SetTextureCube(m_arrMaterialParameter[u].strName.CString(), strValue.CString());
					bModified = True;
				}
				break;
			}
		}
		EndGroup();

		ImGui::EndChild();
	}
}
Boolean EditorMaterialInstancePanel::Save()
{
	return AppAssetManager::SaveAsset(m_pMaterialInstance, m_strMaterialInstanceName.CString());
}
