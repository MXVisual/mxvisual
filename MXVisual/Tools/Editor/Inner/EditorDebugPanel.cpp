#include "EditorDebugPanel.h"
#include "MXSystem.h"
#include "imgui.h"

EditorDebugPanel::EditorDebugPanel()
{
}
EditorDebugPanel::~EditorDebugPanel()
{
}

void EditorDebugPanel::LogMessage(const char* szMessage)
{
	LogData log;
	log.eType = ELT_MESSAGE;
	log.strText = szMessage;

	LocalTime t = System::GetLocalTime();
	log.strTime = String::Format("%i:%.2i:%.2i", t.uHour, t.uMinute, t.uSecond);
	m_arrLog.Add(log);
}
void EditorDebugPanel::LogError(const char* szMessage)
{
	LogData log;
	log.eType = ELT_ERROR;
	log.strText = szMessage;

	LocalTime t = System::GetLocalTime();
	log.strTime = String::Format("%i:%.2i:%.2i", t.uHour, t.uMinute, t.uSecond);
	m_arrLog.Add(log);
}
void EditorDebugPanel::LogWarning(const char* szMessage)
{
	LogData log;
	log.eType = ELT_WARNING;
	log.strText = szMessage;

	LocalTime t = System::GetLocalTime();
	log.strTime = String::Format("%i:%.2i:%.2i", t.uHour, t.uMinute, t.uSecond);
	m_arrLog.Add(log);
}

void EditorDebugPanel::Layout()
{
	ImGui::SetNextWindowSize(ImVec2(500, 440), ImGuiCond_FirstUseEver);
	ImGui::Begin("Log Panel", nullptr, ImGuiWindowFlags_MenuBar);

	ImGui::BeginMenuBar();
	if (ImGui::MenuItem("Clear"))
	{
		m_arrLog.Clear();
		m_nSelectLog = -1;
	}
	ImGui::EndMenuBar();

	{
		ImGui::BeginChild("Short", ImVec2(256, 0), true);
		for (SInt32 n = m_arrLog.ElementCount() - 1; n >= 0; n--)
		{
			if (ImGui::Selectable(m_arrLog[n].strText.CString(), m_nSelectLog == n))
				m_nSelectLog = n;
		}
		ImGui::EndChild();
	}
	ImGui::SameLine();

	ImGui::BeginGroup();
	{
		ImGui::BeginChild("Detail"); // Leave room for 1 line below us
		if (m_nSelectLog > -1)
		{
			ImGui::Text(m_arrLog[m_nSelectLog].strTime.CString());
			ImGui::Separator();
			ImGui::TextWrapped(m_arrLog[m_nSelectLog].strText.CString());
		}
		ImGui::EndChild();
	}
	ImGui::EndGroup();

	ImGui::End();
}
