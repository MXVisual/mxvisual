#pragma once
#ifndef _EDITOR_GAME_PANEL_
#define _EDITOR_GAME_PANEL_

#include "Frame.h"

class EditorGamePanel : public wxPanel
{
public:
	EditorGamePanel(wxWindow* parent);
	~EditorGamePanel();

	void Running(bool bTimePause);

protected:
	void OnSize(wxSizeEvent& event);

	wxDECLARE_EVENT_TABLE();
};

#endif