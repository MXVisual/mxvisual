#pragma once
#ifndef _PROJECT_SELECT_WINDOW_
#define _PROJECT_SELECT_WINDOW_
#include "EditorUIManager.h"
#include "MXArray.h"
#include "MXString.h"

namespace MXVisual
{
	class IGRenderWindow;
	class IGViewFrustum;
}

class ProjectSelectWindow : public IEditorUIWindow
{
protected:
	class IGTexture*		m_pMarkTex;
	class IGViewFrustum*	m_pViewFrustum;

	Array<String>	m_arrProjectName;
	Array<String>	m_arrProjectDate;
	Array<String>	m_arrProjectPath;
	SInt32			m_nSelectedIndex;
	Boolean			m_bOpened;

public:
	ProjectSelectWindow(IGRenderWindow* pRWindow);
	~ProjectSelectWindow();

	void Layout();
	void Render();

	Boolean IsOpened()const { return m_bOpened; }
	const Char* GetSelectedProjectPath()
	{
		return m_nSelectedIndex > -1 && m_nSelectedIndex < m_arrProjectPath.ElementCount() ?
			m_arrProjectPath[m_nSelectedIndex].CString() :
			nullptr;
	}
};

#endif