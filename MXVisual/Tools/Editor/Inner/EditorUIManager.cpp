#include "EditorUIManager.h"
#include "MXGraphicsRenderer.h"
#include "MXTimer.h"
#include "MXInput.h"
#include "imgui.h"
#if PLATFORM == PLATFORM_WINDOWS
#include "windows.h"
#endif

EditorUIManager::EditorUIManager()
	: m_pMaterial(nullptr)
	, m_uPrimitiveUsed(0)
	, m_uPerPrimitiveSize(1024)
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();

	ImGui::StyleColorsLight();

	// Build texture atlas
	ImGuiIO& io = ImGui::GetIO();

	io.KeyMap[ImGuiKey_Tab] = VK_TAB;
	io.KeyMap[ImGuiKey_LeftArrow] = VK_LEFT;
	io.KeyMap[ImGuiKey_RightArrow] = VK_RIGHT;
	io.KeyMap[ImGuiKey_UpArrow] = VK_UP;
	io.KeyMap[ImGuiKey_DownArrow] = VK_DOWN;
	io.KeyMap[ImGuiKey_PageUp] = VK_PRIOR;
	io.KeyMap[ImGuiKey_PageDown] = VK_NEXT;
	io.KeyMap[ImGuiKey_Home] = VK_HOME;
	io.KeyMap[ImGuiKey_End] = VK_END;
	io.KeyMap[ImGuiKey_Insert] = VK_INSERT;
	io.KeyMap[ImGuiKey_Delete] = VK_DELETE;
	io.KeyMap[ImGuiKey_Backspace] = VK_BACK;
	io.KeyMap[ImGuiKey_Space] = VK_SPACE;
	io.KeyMap[ImGuiKey_Enter] = VK_RETURN;
	io.KeyMap[ImGuiKey_Escape] = VK_ESCAPE;
	io.KeyMap[ImGuiKey_KeyPadEnter] = VK_RETURN;
	io.KeyMap[ImGuiKey_A] = 'A';
	io.KeyMap[ImGuiKey_C] = 'C';
	io.KeyMap[ImGuiKey_V] = 'V';
	io.KeyMap[ImGuiKey_X] = 'X';
	io.KeyMap[ImGuiKey_Y] = 'Y';
	io.KeyMap[ImGuiKey_Z] = 'Z';

	unsigned char* pixels;
	int width, height;
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

	IGTexture::Description desc;
	desc.bDynamic = True;
	desc.bGenerateMipMap = False;
	desc.bTargetable = False;
	desc.eFormat = EPF_R8G8B8A8;
	desc.eType = TextureType::ETexture2D;
	desc.uWidth = width;
	desc.uHeight = height;
	desc.uDepth = 1;
	desc.uMipLevel = 1;
	m_pFontTex = GraphicsResource::CreateTexture(desc);
	
	MapRegion region;
	if (m_pFontTex->Map(0, 0, region))
	{
		for (UInt32 u = 0; u < height; u++)
		{
			memcpy((UInt8*)region.pAddress + u * region.uPitch,
				pixels + u * 4 * width, 4 * width);
		}
		m_pFontTex->Unmap();
	}

	io.Fonts->TexID = (ImTextureID)m_pFontTex;
}
EditorUIManager::~EditorUIManager()
{
	ImGui::DestroyContext();

	MX_RELEASE_INTERFACE(m_pFontTex);
	for (UInt32 u = 0; u < m_arrPrimitive.ElementCount(); u++)
	{
		MX_RELEASE_INTERFACE(m_arrPrimitive[0]);
	}
	MX_RELEASE_INTERFACE(m_pMaterial);
	for (UInt32 u = 0; u < m_arrParameterBlock.ElementCount(); u++)
	{
		MX_RELEASE_INTERFACE(m_arrParameterBlock[u]);
	}
	m_arrParameterBlock.Clear();
}

Void EditorUIManager::Update(const Float2& wndSize)
{
	ImGuiIO& io = ImGui::GetIO();
	io.DisplaySize.x = wndSize[0];
	io.DisplaySize.y = wndSize[1];
	io.DeltaTime = Timer::GetDeltaTime();

	Float2 p = Input::GetMousePosition();
	io.MousePos = ImVec2(p[0], p[1]);
	io.MouseDown[ImGuiMouseButton_Left] = Input::GetKeyDown(Input::eLButton);
	io.MouseDown[ImGuiMouseButton_Right] = Input::GetKeyDown(Input::eRButton);
	io.MouseDown[ImGuiMouseButton_Middle] = Input::GetKeyDown(Input::eMButton);
	io.MouseWheel = Input::GetMouseWheelRelativePosition();

	io.KeyCtrl = Input::GetKeyDown(Input::eLCtrl);
	io.KeyShift = Input::GetKeyDown(Input::eLShift);
	io.KeyAlt = Input::GetKeyDown(Input::eLAlt);
	io.KeySuper = false;

	io.KeysDown[VK_CONTROL] = io.KeyCtrl;
	io.KeysDown[VK_SHIFT] = io.KeyShift;
	io.KeysDown[VK_MENU] = io.KeyAlt;
	io.KeysDown[VK_TAB] = Input::GetKeyDown(Input::eTab);
	io.KeysDown[VK_LEFT] = Input::GetKeyDown(Input::eLeft);
	io.KeysDown[VK_RIGHT] = Input::GetKeyDown(Input::eRight);
	io.KeysDown[VK_UP] = Input::GetKeyDown(Input::eUp);
	io.KeysDown[VK_DOWN] = Input::GetKeyDown(Input::eDown);
	io.KeysDown[VK_PRIOR] = Input::GetKeyDown(Input::ePageUp);
	io.KeysDown[VK_NEXT] = Input::GetKeyDown(Input::ePageDown);
	io.KeysDown[VK_HOME] = Input::GetKeyDown(Input::ePageHome);
	io.KeysDown[VK_END] = Input::GetKeyDown(Input::ePageEnd);
	io.KeysDown[VK_DELETE] = Input::GetKeyDown(Input::eDelete);
	io.KeysDown[VK_BACK] = Input::GetKeyDown(Input::eBackSpace);
	io.KeysDown[VK_SPACE] = Input::GetKeyDown(Input::eSpace);
	io.KeysDown[VK_RETURN] = Input::GetKeyDown(Input::eEnter);
	io.KeysDown[VK_ESCAPE] = Input::GetKeyDown(Input::eEscape);

	io.KeysDown['Q'] = Input::GetKeyDown(Input::eQ);
	io.KeysDown['W'] = Input::GetKeyDown(Input::eW);
	io.KeysDown['E'] = Input::GetKeyDown(Input::eE);
	io.KeysDown['R'] = Input::GetKeyDown(Input::eR);
	io.KeysDown['T'] = Input::GetKeyDown(Input::eT);
	io.KeysDown['Y'] = Input::GetKeyDown(Input::eY);
	io.KeysDown['U'] = Input::GetKeyDown(Input::eU);
	io.KeysDown['I'] = Input::GetKeyDown(Input::eI);
	io.KeysDown['O'] = Input::GetKeyDown(Input::eO);
	io.KeysDown['P'] = Input::GetKeyDown(Input::eP);

	io.KeysDown['A'] = Input::GetKeyDown(Input::eA);
	io.KeysDown['S'] = Input::GetKeyDown(Input::eS);
	io.KeysDown['D'] = Input::GetKeyDown(Input::eD);
	io.KeysDown['F'] = Input::GetKeyDown(Input::eF);
	io.KeysDown['G'] = Input::GetKeyDown(Input::eG);
	io.KeysDown['H'] = Input::GetKeyDown(Input::eH);
	io.KeysDown['J'] = Input::GetKeyDown(Input::eJ);
	io.KeysDown['K'] = Input::GetKeyDown(Input::eK);
	io.KeysDown['L'] = Input::GetKeyDown(Input::eL);

	io.KeysDown['Z'] = Input::GetKeyDown(Input::eZ);
	io.KeysDown['X'] = Input::GetKeyDown(Input::eX);
	io.KeysDown['C'] = Input::GetKeyDown(Input::eC);
	io.KeysDown['V'] = Input::GetKeyDown(Input::eV);
	io.KeysDown['B'] = Input::GetKeyDown(Input::eB);
	io.KeysDown['N'] = Input::GetKeyDown(Input::eN);
	io.KeysDown['M'] = Input::GetKeyDown(Input::eM);

	io.KeysDown[VK_NUMPAD0] = Input::GetKeyDown(Input::eNum0);
	io.KeysDown[VK_NUMPAD1] = Input::GetKeyDown(Input::eNum1);
	io.KeysDown[VK_NUMPAD2] = Input::GetKeyDown(Input::eNum2);
	io.KeysDown[VK_NUMPAD3] = Input::GetKeyDown(Input::eNum3);
	io.KeysDown[VK_NUMPAD4] = Input::GetKeyDown(Input::eNum4);
	io.KeysDown[VK_NUMPAD5] = Input::GetKeyDown(Input::eNum5);
	io.KeysDown[VK_NUMPAD6] = Input::GetKeyDown(Input::eNum6);
	io.KeysDown[VK_NUMPAD7] = Input::GetKeyDown(Input::eNum7);
	io.KeysDown[VK_NUMPAD8] = Input::GetKeyDown(Input::eNum8);
	io.KeysDown[VK_NUMPAD9] = Input::GetKeyDown(Input::eNum9);

	ImGuiMouseCursor imgui_cursor = ImGui::GetMouseCursor();
#if PLATFORM == PLATFORM_WINDOWS
	if (imgui_cursor == ImGuiMouseCursor_None || io.MouseDrawCursor)
	{
		// Hide OS mouse cursor if imgui is drawing it or if it wants no cursor
		::SetCursor(NULL);
	}
	else
	{
		// Show OS mouse cursor
		LPTSTR win32_cursor = IDC_ARROW;
		switch (imgui_cursor)
		{
		case ImGuiMouseCursor_Arrow:        win32_cursor = IDC_ARROW; break;
		case ImGuiMouseCursor_TextInput:    win32_cursor = IDC_IBEAM; break;
		case ImGuiMouseCursor_ResizeAll:    win32_cursor = IDC_SIZEALL; break;
		case ImGuiMouseCursor_ResizeEW:     win32_cursor = IDC_SIZEWE; break;
		case ImGuiMouseCursor_ResizeNS:     win32_cursor = IDC_SIZENS; break;
		case ImGuiMouseCursor_ResizeNESW:   win32_cursor = IDC_SIZENESW; break;
		case ImGuiMouseCursor_ResizeNWSE:   win32_cursor = IDC_SIZENWSE; break;
		case ImGuiMouseCursor_Hand:         win32_cursor = IDC_HAND; break;
		case ImGuiMouseCursor_NotAllowed:   win32_cursor = IDC_NO; break;
		}
		::SetCursor(::LoadCursor(NULL, win32_cursor));
	}
#endif

	ImGui::NewFrame();
}

Boolean EditorUIManager::InitMaterial()
{
	IGMaterial::Description desc;
	desc.strUniqueName = "DearImgui_Material";
	desc.bCanCache = True;
	desc.eDomain = MaterialDomain::EScreenSpace;
	desc.eBlendMode = MaterialBlendMode::ETranslucency;
	desc.eFaceType = MaterialFaceType::EFrontface;
	desc.eShadingModel = MaterialShadingModel::ENolighting;
	desc.arrMaterialParams.Add(
		IGMaterialParam::Description(
			IGMaterialParam::EMPT_Texture2D, "BaseTex", ""
		));

	desc.arrMaterialFunction[(const UInt32)MaterialFunctionType::ESurfaceParams] =
		"float4 color = Tex2D_Sample(BaseTex, input.TexCoord0);\n\
				OUTPUT_EMISSIVE(PCFunc_Color_GammaCorrect(color.rgb, 2.2f) * PCFunc_Color_GammaCorrect(input.VertexColor.rgb, 2.2f));\n\
				OUTPUT_OPACITY(color.a * input.VertexColor.a);\n\
				OUTPUT_ROUGHNESS(0.5f);\n\
				OUTPUT_METALLIC(0.0f);";

	m_pMaterial = GraphicsResource::CreateMaterial(desc);
	return m_pMaterial != nullptr;
}

Void EditorUIManager::Flush()
{
	static Array<GVertexRigid2D> Vertexs;
	static Array<UInt32> Indexs;

	ImGui::Render();

	ImDrawData* pDrawData = ImGui::GetDrawData();

	// Avoid rendering when minimized
	if (pDrawData->DisplaySize.x <= 0.0f || pDrawData->DisplaySize.y <= 0.0f)
		return;

	if (m_arrPrimitive.ElementCount() == 0)
		m_arrPrimitive.Add(nullptr);

	if (m_arrPrimitive[0] != nullptr && 
		(m_arrPrimitive[0]->GetVertexCapacity() < pDrawData->TotalVtxCount ||
			m_arrPrimitive[0]->GetIndexCapacity() < pDrawData->TotalIdxCount))
	{
		m_arrPrimitive[0]->Release();
		m_arrPrimitive[0] = nullptr;
	}

	if (m_arrPrimitive[0] == nullptr)
	{
		IGPrimitive::Description desc;
		desc.eType = IGPrimitive::EPrimT_Rigid2D;
		desc.bDynamic = True;
		desc.uVertexCount = pDrawData->TotalVtxCount + 64;
		desc.uIndexCount = pDrawData->TotalIdxCount + 64;
		m_arrPrimitive[0] = GraphicsResource::CreatePrimitive(desc);
	}
	ASSERT_IF_FAILED(m_arrPrimitive[0]);

	if (m_pMaterial == nullptr &&
		!InitMaterial())
	{
		return;
	}

	UInt32 uCmdBufferOffset = 0;
	UInt32 uCmdVertexOffset = 0;
	for (UInt32 uCmd = 0; uCmd < pDrawData->CmdListsCount; uCmd++)
	{
		const ImDrawList* cmd_list = pDrawData->CmdLists[uCmd];
		for (UInt32 uVertex = 0; uVertex < cmd_list->VtxBuffer.size(); uVertex++)
		{
			const ImDrawVert& v = cmd_list->VtxBuffer.Data[uVertex];
			GVertexRigid2D v2d;
			v2d.f2Position = Float2(v.pos.x, v.pos.y);
			v2d.f2TexCoord0 = Float2(v.uv.x, v.uv.y);
			v2d.f4Color = Float4(v.col & 0xff, (v.col >> 8) & 0xff, (v.col >> 16) & 0xff, (v.col >> 24) & 0xff) / 255.0f;
			Vertexs.Add(v2d);
		}

		for (UInt32 uIndex = 0; uIndex < cmd_list->IdxBuffer.size(); uIndex++)
		{
			const ImDrawIdx& i = cmd_list->IdxBuffer.Data[uIndex];
			Indexs.Add(uCmdVertexOffset + i);
		}

		uCmdVertexOffset += cmd_list->VtxBuffer.size();

		for (UInt32 cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
		{
			const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];

			IGTexture* pTex = (IGTexture*)pcmd->TextureId;
			if (m_arrParameterBlock.ElementCount() < (uCmdBufferOffset + 1))
			{
				m_arrParameterBlock.Add(GraphicsResource::CreateMaterialParameterBuffer(m_pMaterial));
			}
			m_arrParameterBlock[uCmdBufferOffset]->GetParamByName("BaseTex")->SetTexture2D(pTex);
			uCmdBufferOffset++;
		}
	}

	m_arrPrimitive[0]->UpdateVertexBuffer(Vertexs.GetRawData(), Vertexs.ElementCount());
	m_arrPrimitive[0]->UpdateIndexBuffer(Indexs.GetRawData(), Indexs.ElementCount());
	Vertexs.Clear();
	Indexs.Clear();

	uCmdBufferOffset = 0;
	UInt32 uCmdIndexOffset = 0;
	//ImVec2 clip_off = draw_data->DisplayPos;
	for (UInt32 uCmd = 0; uCmd < pDrawData->CmdListsCount; uCmd++)
	{
		const ImDrawList* cmd_list = pDrawData->CmdLists[uCmd];
		for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
		{
			const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
			pcmd->ClipRect;

			Region r;
			r.uLeft = pcmd->ClipRect.x;
			r.uTop = pcmd->ClipRect.y;
			r.uRight = pcmd->ClipRect.z;
			r.uBottom = pcmd->ClipRect.w;

			GraphicsRenderer::RegisterPrimitive(
				m_arrPrimitive[0],
				uCmdIndexOffset + pcmd->IdxOffset, pcmd->ElemCount,
				m_pMaterial,
				m_arrParameterBlock[uCmdBufferOffset], nullptr, r);

			uCmdBufferOffset++;
		}
		uCmdIndexOffset += cmd_list->IdxBuffer.Size;
	}
}
