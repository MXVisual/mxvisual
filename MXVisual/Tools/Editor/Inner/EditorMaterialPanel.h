#pragma once
#ifndef _EDITOR_MATERIAL_PANEL_
#define _EDITOR_MATERIAL_PANEL_
#include "EditorUIManager.h"
#include "PropertyPage.h"
#include "MXAsset.h"
#include "imgui.h"

class EditorMaterialProperty
{
public:
	EditorMaterialProperty();

	Void BeginGroup(const Char* szName);
	Void EndGroup();

	Boolean PropertyEnumeration(
		const Char* szName,
		Array<String>& arrEnumLabel,
		Array<SInt32>& arrEnumValue,
		SInt32& nValue);
	Boolean PropertyFloat1(
		const Char* szName,
		Float1& fValue);
	Boolean PropertyFloat2(
		const Char* szName,
		Float2& f2Value);
	Boolean PropertyFloat3(
		const Char* szName,
		Float3& f3Value);
	Boolean PropertyFloat4(
		const Char* szName,
		Float4& f4Value);
	Boolean PropertyTexture(
		const Char* szName,
		String& strValue);

	//For Material Instance
	Boolean PropertyMaterial(
		const Char* szName,
		String& strValue);

protected:
	Boolean m_bGroupOpened;
};

class EditorMaterialPanel : public EditorMaterialProperty
{
public:
	EditorMaterialPanel();
	~EditorMaterialPanel();

	void Layout(const char* szFile, Boolean& bModified);

protected:
	const Char* m_szParameterMenuName;
	const Char* m_szCodeMenuName;

	void Layout_ParameterMenu(Boolean& bModified);
	void Layout_CodeMenu(Boolean& bModified);

	void Layout_Parameter(Boolean& bModified);
	void Layout_Code(Boolean& bModified);

	String	m_strMaterialName;
	IAppMaterial* m_pMaterial;

	IAppMaterial::Domain m_eDomain;
	IAppMaterial::ShadingModel m_eShadingModel;
	IAppMaterial::FaceType m_eFaceType;
	IAppMaterial::BlendMode m_eBlendMode;

	Array<IAppMaterial::ParameterDescription> m_arrParameterDefault;

	Char	m_szCodeBuffer[Constant_MAX_PATH * 8];
	SInt32	m_nSelectedCodePanel;
	String	m_arrCode[IAppMaterial::MaterialFunction::EMF_Count];

	Boolean Recompile();
	Boolean Save();
};

class EditorMaterialInstancePanel : public EditorMaterialProperty
{
public:
	EditorMaterialInstancePanel();
	~EditorMaterialInstancePanel();

	void Layout(const char* szFile, Boolean& bModified);

protected:
	String	m_strMaterialInstanceName;
	IAppMaterialInstance* m_pMaterialInstance;
	Array<IAppMaterial::ParameterDescription> m_arrMaterialParameter;

	Boolean Save();
};

#endif