#include "EditorPropertyPanel.h"

#include "EditorGlobal.h"

#include "MXCObject.h"
#include "MXCComponent.h"
#include "MXCCamera.h"

#include "imgui.h"

/*
typedef void(*FEnumComponentsName)(MXVisual::Array<MXVisual::String>& arrComponentName);
typedef MXVisual::ICComponent* (*FCreateComponent)(const char* szComponentName);

FEnumComponentsName g_pEnumComponentsName = nullptr;
FCreateComponent g_pCreateComponent = nullptr;

HMODULE g_hUserScript = NULL;
bool InitUserScriptModule()
{
	g_hUserScript = LoadLibraryA("UserScript.dll");
	if (g_hUserScript != NULL)
	{
		g_pEnumComponentsName = (FEnumComponentsName)GetProcAddress(g_hUserScript, "EnumComponentsName");
		g_pCreateComponent = (FCreateComponent)GetProcAddress(g_hUserScript, "CreateComponent");
	}

	if (g_pEnumComponentsName != nullptr &&
		g_pCreateComponent != nullptr)
	{
		return true;
	}
	return false;
}
void ReleaseUserScriptModule()
{
	g_pEnumComponentsName = nullptr;
	g_pCreateComponent = nullptr;
	FreeLibrary(g_hUserScript);
}
*/

EditorPropertyPanel::EditorPropertyPanel()
	: m_pLastSelectedObj(nullptr)
	, m_nSelectedComponentClassName(-1)
{
	UpdateComponentClassNameList();
}

EditorPropertyPanel::~EditorPropertyPanel()
{
}

void EditorPropertyPanel::Layout()
{
	ImGui::Begin("Property Panel");

	ICObject* pObj = eg_GetSelectedObject();

	if (pObj != m_pLastSelectedObj)
	{
		m_pLastSelectedObj = pObj;
		if (m_pLastSelectedObj == nullptr)
		{
			m_arrPropertyPanel.SetElementCount(1);

			m_arrPropertyPanel[0].SetPropertyObject("Scene Property",
				dynamic_cast<ICSupportedEditorPropertyPanel*>(CScene::ActivedScene()));
		}
		else
		{
			m_arrPropertyPanel.SetElementCount(pObj->GetComponentCount() + 1);

			m_arrPropertyPanel[0].SetPropertyObject(pObj->GetClassName(),
				dynamic_cast<ICSupportedEditorPropertyPanel*>(pObj));
			m_arrPropertyPanel[0].SetSupportedRender(
				dynamic_cast<ICSupportedEditorGraphics*>(pObj));

			for (UInt32 u = 0; u < pObj->GetComponentCount(); u++)
			{
				ICComponent* pComp = pObj->GetComponent(u);
				m_arrPropertyPanel[u + 1].SetPropertyObject(pComp->GetClassName(),
					dynamic_cast<ICSupportedEditorPropertyPanel*>(pComp));
				m_arrPropertyPanel[u + 1].SetSupportedRender(
					dynamic_cast<ICSupportedEditorGraphics*>(pComp));
			}
		}
	}

	if (m_arrPropertyPanel.ElementCount() > 0)
	{
		m_arrPropertyPanel[0].Layout();

		UInt32 uDeletedIndex = 0;
		for (UInt32 u = 1; u < m_arrPropertyPanel.ElementCount(); u++)
		{
			Boolean bDeleted = False;
			m_arrPropertyPanel[u].Layout(bDeleted);
			if (bDeleted)
			{
				uDeletedIndex = u;
				break;
			}
		}
		if (uDeletedIndex > 0)
		{
			m_pLastSelectedObj->RemoveComponent(uDeletedIndex - 1);
			m_pLastSelectedObj = nullptr;
		}
	}

	if (m_pLastSelectedObj != nullptr)
	{
		LayoutAddComponentList();
		//Add Component
		if (m_nSelectedComponentClassName != -1)
		{
			pObj->AddComponent(
				ICComponent::Create(m_arrComponentClassName[m_nSelectedComponentClassName].CString())
			);
			m_nSelectedComponentClassName = -1;
			m_pLastSelectedObj = nullptr;
		}
	}
	ImGui::End();
}

void EditorPropertyPanel::UpdateComponentClassNameList()
{
	m_arrComponentClassName.Clear();
	ICComponent::Enumerate(m_arrComponentClassName);
}
void EditorPropertyPanel::LayoutAddComponentList()
{
	const Char* PopupMenuName = "##ComponentList";
	static ImGuiTextFilter filter;

	ImGui::Separator();
	if (ImGui::Button("Add Component",
		ImVec2(ImGui::GetWindowContentRegionWidth(), 0.0f)))
	{
		ImGui::OpenPopup(PopupMenuName);
		m_nSelectedComponentClassName = -1;
	}

	if (ImGui::BeginPopup(PopupMenuName))
	{
		filter.Draw("Filter");
		ImGui::Separator();

		for (UInt32 u = 0; u < m_arrComponentClassName.ElementCount(); u++)
		{
			if (filter.PassFilter(m_arrComponentClassName[u].CString()))
			{
				if (ImGui::Selectable(m_arrComponentClassName[u].CString(),
					m_nSelectedComponentClassName == u, ImGuiSelectableFlags_AllowDoubleClick))
				{
					m_nSelectedComponentClassName = u;
					break;
				}
			}
			ImGui::Separator();
		}
		ImGui::EndPopup();
	}
}
