#pragma once
#ifndef _EDITOR_SCENETREE_
#define _EDITOR_SCENETREE_
#include "EditorUIManager.h"

namespace MXVisual
{
	class ICObject;
}
class EditorSceneTree 
{
public:
	EditorSceneTree();
	~EditorSceneTree();

	void Layout();

protected:
	const Char* m_szMenuName;

	void LayoutSceneObject(ICObject* pObj);
	void Layout_EditMenu();

	void AddSceneObject(ICObject* pObj);
};

#endif