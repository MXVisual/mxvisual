#pragma once
#ifndef _EDITOR_PROPERTY_PANEL_
#define _EDITOR_PROPERTY_PANEL_
#include "EditorUIManager.h"
#include "PropertyPage.h"

class EditorPropertyPanel
{
public:
	EditorPropertyPanel();
	~EditorPropertyPanel();

	void Layout();

protected:
	void LayoutAddComponentList();

	ICObject* m_pLastSelectedObj;
	Array<PropertyPanel> m_arrPropertyPanel;

	void UpdateComponentClassNameList();
	Array<String>	m_arrComponentClassName;
	SInt32			m_nSelectedComponentClassName;
};

#endif