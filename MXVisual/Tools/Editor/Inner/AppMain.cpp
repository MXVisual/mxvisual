#include "MXPlatform.h"
#if PLATFORM == PLATFORM_WINDOWS
#include "Windows.h"
#endif
#include "MXGraphicsRenderer.h"
#include "MXGraphicsResource.h"

#include "MXMediaPlayer.h"

#include "MXPhysicsSimulator.h"

#include "MXAsset.h"
#include "MXTimer.h"
#include "MXFile.h"
#include "MXLog.h"
#include "EditorUIManager.h"

#include "ProjectSelectWindow.h"
#include "EditorMainFrame.h"
#include "MXThread.h"

#include "imgui.h"
using namespace MXVisual;
IGRenderWindow* g_pRenderWindow = nullptr;

EditorUIManager* g_pUIManager = nullptr;
ProjectSelectWindow* g_pProjSelWnd = nullptr;
class EditorLogCallback : public Log::IOutputCallback
{
public:
	Void Print(const Char* szLogMessage)
	{
		EditorMainFrame::LogError(szLogMessage);
	}
};
EditorLogCallback logcallback;

String	g_strProjectPath;

Boolean ApplicationInit()
{
	ThreadManager::Init();

	GraphicsRenderer::InitParam initparams;
	initparams.szAdapterName = nullptr;
	initparams.uBackBufferWidth = 1920;
	initparams.uBackBufferHeight = 1080;
	if (!GraphicsRenderer::Init(initparams))
	{
		return False;
	}

	MediaPlayer::InitParam mi_params;
	if (!MediaPlayer::Init(mi_params))
	{
		return False;
	}

	PhysicsSimulator::InitParam ps_params;
	if (!PhysicsSimulator::Init(ps_params))
	{
		return False;
	}

	IGRenderWindow::Description desc;
	desc.uParentWindowHandle = NULL;
	desc.szWindowName = "Editor";
	desc.uBackbufferCount = 2;
	desc.eBackbufferFormat = MXVisual::PixelFormat::EPF_R8G8B8A8;
	desc.uBackbufferWidth = 512;
	desc.uBackbufferHeight = 512;
	g_pRenderWindow = GraphicsResource::CreateRenderWindow(desc);
	if (g_pRenderWindow == nullptr)return False;

	Timer::Update();
	g_pRenderWindow->Show(True);

	g_pUIManager = new EditorUIManager();
	g_pProjSelWnd = new ProjectSelectWindow(g_pRenderWindow);

	return True;
}
Void ApplicationRelease()
{
	EditorMainFrame::Release();
	MX_DELETE(g_pProjSelWnd);
	MX_DELETE(g_pUIManager);

	MX_RELEASE_INTERFACE(g_pRenderWindow);

	PhysicsSimulator::Release();

	MediaPlayer::Release();

	AppAssetManager::Release();

	GraphicsRenderer::Release();

	ThreadManager::Release();
}
Void ApplicationIME(UInt8 uCharSize, const Char* c)
{
	ImGuiIO& io = ImGui::GetIO();
	if (uCharSize == 1)
		io.AddInputCharacter(c[0]);
}

Void Update()
{
	if (g_pProjSelWnd != nullptr &&
		!g_strProjectPath.IsEmpty() &&
		CheckFile(g_strProjectPath.CString()))
	{
		delete g_pProjSelWnd;
		g_pProjSelWnd = nullptr;
		g_pRenderWindow->Maximize();

		AppAssetManager::InitParams params;
		params.szAssetsPath = g_strProjectPath.CString();
		if (!MXVisual::AppAssetManager::Init(params))
		{
			ASSERT_IF_FAILED(!"Asset Manager Init Failed");
			return;
		}

		EditorMainFrame::Init(g_pRenderWindow, g_strProjectPath.CString());
		Log::SetOutputCallback(&logcallback);
	}

	g_pUIManager->Update(Float2(g_pRenderWindow->GetWidth(), g_pRenderWindow->GetHeight()));
	if (g_pProjSelWnd != nullptr)
	{
		g_pProjSelWnd->Layout();
		if (g_pProjSelWnd->IsOpened())
		{
			g_strProjectPath = g_pProjSelWnd->GetSelectedProjectPath();
		}
	}
	else
	{
		EditorMainFrame::Layout();
	}
}
Void Render()
{
	g_pUIManager->Flush();

	if (g_pProjSelWnd != nullptr)
	{
		g_pProjSelWnd->Render();
	}
	else
	{
		EditorMainFrame::Render();
	}

	g_pRenderWindow->Present();
}
Void ApplicationLoop()
{
	Update();
	Render();
}

int _stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lp1CmdLine, int nCmdShow)
{
	Application::FOnInit = ApplicationInit;
	Application::FOnRelease = ApplicationRelease;
	Application::FOnLoop = ApplicationLoop;
	Application::FOnIME = ApplicationIME;

	return Application::Run();
}
