#pragma once
#ifndef _EDITOR_UI_MANAGER_
#define _EDITOR_UI_MANAGER_
#include "MXPlatform.h"
#include "MXGraphicsResource.h"
#include "MXArray.h"
using namespace MXVisual;

class EditorUIManager
{
public:
	EditorUIManager();
	~EditorUIManager();

	Void Update(const Float2& wndSize);
	Void Flush();

protected:
	Boolean InitMaterial();

	const UInt32 m_uPerPrimitiveSize;
	UInt32 m_uPrimitiveUsed;
	Array<IGPrimitive*> m_arrPrimitive;
	IGMaterial*		m_pMaterial;
	Array<IGMaterialParameterBuffer*> m_arrParameterBlock;

	IGTexture*		m_pFontTex;
};

class IEditorUIWindow
{
public:
	virtual ~IEditorUIWindow() {};
	virtual void Layout() = 0;
	virtual void Render() = 0;
};

#endif
