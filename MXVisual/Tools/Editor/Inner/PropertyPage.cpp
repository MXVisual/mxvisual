﻿#include "PropertyPage.h"
#include "EditorGlobal.h"
#include "MXSystem.h"
#include "MXCEditorPanel.h"
#include "MXCEditorGraphics.h"
#include "imgui.h"

const Char* PropertyPanel::szPropertyPanelMenuName = "PropertyEditMenu";

PropertyPanel::PropertyPanel()
	: m_pPropertyObject(nullptr)
{

}
PropertyPanel::~PropertyPanel()
{
}

void PropertyPanel::SetPropertyObject(const Char* szName, ICSupportedEditorPropertyPanel* pSupportedObj)
{
	m_szName = szName;
	m_pPropertyObject = pSupportedObj;
	m_pSupportedRender = nullptr;
	m_arrGroupStackOpenedFlag.Clear();
}
void PropertyPanel::SetSupportedRender(ICSupportedEditorGraphics* pSupportedRender)
{
	if (pSupportedRender != nullptr)
	{
		m_pSupportedRender = pSupportedRender;
	}
}
void PropertyPanel::Layout()
{
	m_arrGroupStackOpenedFlag.Clear();
	bool bOpened = ImGui::CollapsingHeader(m_szName, ImGuiTreeNodeFlags_DefaultOpen);
	if (bOpened)
	{
		if (m_pPropertyObject != nullptr)
		{
			m_pPropertyObject->OnPanelActived(*this);
		}
		else
		{
			ImGui::BulletText("[Empty]");
		}

		if (m_pSupportedRender != nullptr)
		{
			m_pSupportedRender->OnEditorRender(*eg_GetEditorGraphics());
		}

		if (m_pPropertyObject != nullptr)
		{
			//m_pPropertyObject->OnPanelInactived();
		}
	}
}
void PropertyPanel::Layout(Boolean& bDeleted)
{
	bDeleted = False;
	m_arrGroupStackOpenedFlag.Clear();
	bool bOpened = ImGui::CollapsingHeader(m_szName, ImGuiTreeNodeFlags_DefaultOpen);
	ImGui::OpenPopupOnItemClick(szPropertyPanelMenuName, ImGuiPopupFlags_MouseButtonRight);

	if (ImGui::BeginPopupContextItem(szPropertyPanelMenuName))
	{
		if (ImGui::MenuItem("Delete"))
		{
			bDeleted = True;
		}
		ImGui::MenuItem("Copy", nullptr, nullptr, false);
		ImGui::EndPopup();
	}

	if (bOpened)
	{
		if (m_pPropertyObject != nullptr)
		{
			m_pPropertyObject->OnPanelActived(*this);
		}
		else
		{
			ImGui::BulletText("[Empty]");
		}

		if (m_pSupportedRender != nullptr)
		{
			m_pSupportedRender->OnEditorRender(*eg_GetEditorGraphics());
		}

		if (m_pPropertyObject != nullptr)
		{
			//m_pPropertyObject->OnPanelInactived();
		}
	}
}


Void PropertyPanel::BeginGroup(const Char* szName)
{
	Boolean bGroupOpened = ImGui::TreeNode(szName);
	m_arrGroupStackOpenedFlag.Add(bGroupOpened);
}
Void PropertyPanel::EndGroup()
{
	if (m_arrGroupStackOpenedFlag.ElementCount() > 0 &&
		m_arrGroupStackOpenedFlag[m_arrGroupStackOpenedFlag.ElementCount() - 1])
	{
		m_arrGroupStackOpenedFlag.Erase(m_arrGroupStackOpenedFlag.ElementCount() - 1, 1);
		ImGui::TreePop();
	}
}
Void PropertyPanel::AddPropertyString(
	const Char* szName,
	const Char* szValue)
{
	ASSERT_IF_FAILED(szName != nullptr && szValue != nullptr);
	if (m_arrGroupStackOpenedFlag.ElementCount() > 0 &&
		!m_arrGroupStackOpenedFlag[m_arrGroupStackOpenedFlag.ElementCount() - 1])
		return;

	EditorPropertyString value;
	strcpy(value.Name, szName);
	strcpy(value.Value, szValue);
	if (ImGui::InputText(szName, value.Value, Constant_MAX_PATH))
	{
		m_pPropertyObject->OnPanelChanged(&value);
	}
}
Void PropertyPanel::AddPropertyLongString(
	const Char* szName,
	const Char* szValue)
{
	ASSERT_IF_FAILED(szName != nullptr && szValue != nullptr);
	if (m_arrGroupStackOpenedFlag.ElementCount() > 0 &&
		!m_arrGroupStackOpenedFlag[m_arrGroupStackOpenedFlag.ElementCount() - 1])
		return;

	Char buf[Constant_MAX_PATH * 2];
	sprintf(buf, szValue);
	if (ImGui::InputTextMultiline(szName, buf, Constant_MAX_PATH))
	{
		EditorPropertyLongString value;
		strcpy(value.Name, szName);
		value.Value = buf;
		m_pPropertyObject->OnPanelChanged(&value);
	}
}
Void PropertyPanel::AddPropertyFile(
	const Char* szName,
	const Char* szValue,
	const Char* szFilter)
{
	ASSERT_IF_FAILED(szName != nullptr && szValue != nullptr);
	if (m_arrGroupStackOpenedFlag.ElementCount() > 0 &&
		!m_arrGroupStackOpenedFlag[m_arrGroupStackOpenedFlag.ElementCount() - 1])
		return;

	Boolean bChanged = False;
	EditorPropertyFile value;
	strcpy(value.Name, szName);
	strcpy(value.Value, szValue);

	String strPropertyName = String::Format("##%s", value.Name);
	ImGui::InputText(strPropertyName.CString(), value.Value, Constant_MAX_PATH);
	if (ImGui::BeginDragDropTarget())
	{
		const ImGuiPayload* payload = ImGui::AcceptDragDropPayload(DragDropAssetID);
		if (payload != nullptr)
		{
			ASSERT_IF_FAILED(payload->DataSize == sizeof(const AssetInfo*));
			const AssetInfo* pInfo = *(const AssetInfo**)payload->Data;
			if (pInfo->pParentFolderInfo != nullptr)
			{
				sprintf(value.Value, "%s/%s/%s",
					pInfo->pParentFolderInfo->strPath.CString(),
					pInfo->pParentFolderInfo->strName.CString(),
					pInfo->strName.CString());
			}
			else
			{
				sprintf(value.Value, "%s", pInfo->strName.CString());
			}
			bChanged = True;
		}
		ImGui::EndDragDropTarget();
	}
	ImGui::SameLine();
	if (ImGui::Button(szName) &&
		System::DlgSelectFile("Select Asset", value.Value, Constant_MAX_PATH))
	{
		bChanged = True;
	}
	if (bChanged &&	CheckFileSuffix(value.Value, szFilter))
	{
		m_pPropertyObject->OnPanelChanged(&value);
	}
}
Void PropertyPanel::AddPropertyColor(
	const Char* szName,
	const Float4& color)
{
	ASSERT_IF_FAILED(szName != nullptr);
	if (m_arrGroupStackOpenedFlag.ElementCount() > 0 &&
		!m_arrGroupStackOpenedFlag[m_arrGroupStackOpenedFlag.ElementCount() - 1])
		return;

	float c[4] = { color[0], color[1], color[2], color[3] };
	if (ImGui::ColorEdit4(szName, c, ImGuiColorEditFlags_Float))
	{
		EditorPropertyColor value;
		strcpy(value.Name, szName);
		value.Value = Float4(c[0], c[1], c[2], c[3]);
		m_pPropertyObject->OnPanelChanged(&value);
	}
}
Void PropertyPanel::AddPropertyTrigger(
	const Char* szName,
	const Boolean bValue)
{
	ASSERT_IF_FAILED(szName != nullptr);
	if (m_arrGroupStackOpenedFlag.ElementCount() > 0 &&
		!m_arrGroupStackOpenedFlag[m_arrGroupStackOpenedFlag.ElementCount() - 1])
		return;

	EditorPropertyTrigger value;
	strcpy(value.Name, szName);
	value.Value = bValue;
	if (ImGui::Checkbox(szName, &value.Value))
	{
		m_pPropertyObject->OnPanelChanged(&value);
	}
}
Void PropertyPanel::AddPropertyEnumeration(
	const Char* szName,
	Array<String>& arrEnumLabel,
	Array<SInt32>& arrEnumValue,
	SInt32 nValue)
{
	ASSERT_IF_FAILED(szName != nullptr);
	if (m_arrGroupStackOpenedFlag.ElementCount() > 0 &&
		!m_arrGroupStackOpenedFlag[m_arrGroupStackOpenedFlag.ElementCount() - 1])
		return;

	EditorPropertyEnumeration value;
	strcpy(value.Name, szName);
	value.Value = nValue;
	int nEnumIndex = 0;
	for (int n = 0; n < arrEnumLabel.ElementCount(); n++)
	{
		if (arrEnumValue[n] == nValue)
			nEnumIndex = n;
	}
	if (ImGui::BeginCombo(szName, arrEnumLabel[nEnumIndex].CString()))
	{
		for (int n = 0; n < arrEnumLabel.ElementCount(); n++)
		{
			ImGui::PushID(arrEnumValue[n]);
			if (ImGui::Selectable(arrEnumLabel[n].CString(), value.Value == arrEnumValue[n]))
			{
				value.Value = arrEnumValue[n];
				m_pPropertyObject->OnPanelChanged(&value);
			}
			ImGui::PopID();
		}
		ImGui::EndCombo();
	}
}
Void PropertyPanel::AddPropertyFloat1(
	const Char* szName,
	const Float1 fValue,
	const Float1 fStep,
	const Float1 fMin,
	const Float1 fMax)
{
	ASSERT_IF_FAILED(szName != nullptr);
	if (m_arrGroupStackOpenedFlag.ElementCount() > 0 &&
		!m_arrGroupStackOpenedFlag[m_arrGroupStackOpenedFlag.ElementCount() - 1])
		return;

	EditorPropertyFloat1 value;
	strcpy(value.Name, szName);
	value.Value = fValue;
	if (ImGui::DragFloat(szName, &value.Value, fStep, fMin, fMax))
	{
		m_pPropertyObject->OnPanelChanged(&value);
	}
}
Void PropertyPanel::AddPropertyFloat2(
	const Char* szName,
	const Float2& f2Value,
	const Float1 fStep,
	const Float1 fMin,
	const Float1 fMax)
{
	ASSERT_IF_FAILED(szName != nullptr);
	if (m_arrGroupStackOpenedFlag.ElementCount() > 0 &&
		!m_arrGroupStackOpenedFlag[m_arrGroupStackOpenedFlag.ElementCount() - 1])
		return;

	float f[2] = { f2Value[0], f2Value[1] };
	if (ImGui::DragFloat2(szName, f, fStep, fMin, fMax))
	{
		EditorPropertyFloat2 value;
		strcpy(value.Name, szName);
		value.Value = Float2(f[0], f[1]);
		m_pPropertyObject->OnPanelChanged(&value);
	}
}
Void PropertyPanel::AddPropertyFloat3(
	const Char* szName,
	const Float3& f3Value,
	const Float1 fStep,
	const Float1 fMin,
	const Float1 fMax)
{
	ASSERT_IF_FAILED(szName != nullptr);
	if (m_arrGroupStackOpenedFlag.ElementCount() > 0 &&
		!m_arrGroupStackOpenedFlag[m_arrGroupStackOpenedFlag.ElementCount() - 1])
		return;

	float f[3] = { f3Value[0], f3Value[1], f3Value[2] };
	if (ImGui::DragFloat3(szName, f, fStep, fMin, fMax))
	{
		EditorPropertyFloat3 value;
		strcpy(value.Name, szName);
		value.Value = Float3(f[0], f[1], f[2]);
		m_pPropertyObject->OnPanelChanged(&value);
	}
}
Void PropertyPanel::AddPropertyFloat4(
	const Char* szName,
	const Float4& f4Value,
	const Float1 fStep,
	const Float1 fMin,
	const Float1 fMax)
{
	ASSERT_IF_FAILED(szName != nullptr);
	if (m_arrGroupStackOpenedFlag.ElementCount() > 0 &&
		!m_arrGroupStackOpenedFlag[m_arrGroupStackOpenedFlag.ElementCount() - 1])
		return;
	
	float f[4] = { f4Value[0], f4Value[1], f4Value[2], f4Value[3] };
	if (ImGui::DragFloat4(szName, f, fStep, fMin, fMax))
	{
		EditorPropertyFloat4 value;
		strcpy(value.Name, szName);
		value.Value = Float4(f[0], f[1], f[2], f[3]);
		m_pPropertyObject->OnPanelChanged(&value);
	}
}

