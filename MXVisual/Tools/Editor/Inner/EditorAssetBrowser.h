#pragma once
#ifndef _EDITOR_ASSET_BROWSER_
#define _EDITOR_ASSET_BROWSER_
#include "EditorUIManager.h"
#include "EditorGlobal.h"
#include "PropertyPage.h"

namespace MXVisual
{
	class ICImporter;
}

class EditorAssetBrowser
{
public:
	EditorAssetBrowser();
	~EditorAssetBrowser();

	void Layout(const FolderInfo* pRootFolder);

protected:
	void Layout_FolderTree(const FolderInfo* info);
	void Layout_FolderAssets(const FolderInfo* info);
	Boolean Layout_Asset_List(const AssetInfo* info, Boolean bSelected);
	Boolean Layout_Asset_Thumbnail(const AssetInfo* info, Boolean bSelected, const Float1 fScale);

	const Char* m_szMenuName;
	void Layout_AssetMenu();

	void Layout_ImportDlg();

	void AssetOpen();

	const FolderInfo*	m_pSelectedFolderInfo;
	const AssetInfo*	m_pSelectedAssetInfo;

	Float1				m_fContentItemScale;
	Boolean				m_bAssetShouldOpen;

	Array<IGTexture*>	m_arrContentIconTexture;

	PropertyPanel		m_ImporterPanel;
	ICImporter*			m_pActivedImporter;
};

#endif