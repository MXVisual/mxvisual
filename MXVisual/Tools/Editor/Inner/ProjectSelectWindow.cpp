#include "ProjectSelectWindow.h"
#include "imgui.h"
#include "MXGraphicsResource.h"
#include "MXGraphicsRenderer.h"
#include "MXMath.h"
#include "MXCSerializer.h"
#include "MXFile.h"
#include "MXSystem.h"
#include "EditorGlobal.h"

using namespace MXVisual;

ProjectSelectWindow::ProjectSelectWindow(IGRenderWindow* pRWindow)
	: m_nSelectedIndex(-1)
{
	IGViewFrustum::Description vfDesc;
	vfDesc.pTargetWindow = pRWindow;
	m_pViewFrustum = GraphicsResource::CreateViewFrustum(vfDesc);
	m_pViewFrustum->SetOrthographic(-512, 512, 0, 200);

	MXVisual::Char path[MXVisual::Constant_MAX_PATH];
	MXVisual::GetWorkingPath(path);
	MXVisual::String strIniFile = path;
	strIniFile += "\\EditorCache.ini";
	MXVisual::IDeserializer* pDeserializer = MXVisual::TextDeserializer::Open(strIniFile.CString());
	if (pDeserializer != nullptr)
	{
		const MXVisual::IDeserializerBlock* pBlock = pDeserializer->GetRootDeserializerBlock();
		MXVisual::String strProject, strDir, strDate;
		for (int n = 0; n < pBlock->GetChildBlocksCount(); n++)
		{
			const MXVisual::IDeserializerBlock* pChildBlock = pBlock->GetChildBlock(n);
			pChildBlock->Load("Project", strProject);
			pChildBlock->Load("Date", strDate);
			pChildBlock->Load("Directory", strDir);

			m_arrProjectName.Add(strProject);
			m_arrProjectDate.Add(strDate);
			m_arrProjectPath.Add(strDir);
		}
		pDeserializer->Close();
	}

	m_pMarkTex = eg_LoadTGATexture(".\\Icons\\Mark.tga");
}
ProjectSelectWindow::~ProjectSelectWindow()
{
	MX_RELEASE_INTERFACE(m_pViewFrustum);
	MX_RELEASE_INTERFACE(m_pMarkTex);
}

void ProjectSelectWindow::Layout()
{
	ImGuiIO& io = ImGui::GetIO();

	ImGui::SetNextWindowPos(ImVec2(0, 0));
	ImGui::SetNextWindowSize(io.DisplaySize);

	ImGui::Begin("Project", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize);
	ImGui::SetNextItemWidth(io.DisplaySize.x);

	Float1 fAspect = (Float1)m_pMarkTex->GetHeight() / m_pMarkTex->GetWidth();
	ImGui::Image(m_pMarkTex, ImVec2(io.DisplaySize.x - 16.0f, (io.DisplaySize.x - 16.0f) * fAspect));

	Float1 fResHeight = io.DisplaySize.y - (io.DisplaySize.x - 16.0f) * fAspect;
	ImGui::BeginChild("List", ImVec2(io.DisplaySize.x - 16.0f, fResHeight * 0.8f), true, ImGuiWindowFlags_NoResize);

	ImGui::Columns(3, "Project - List");
	ImGui::Text("Name"); ImGui::NextColumn();
	ImGui::Text("Date"); ImGui::NextColumn();
	ImGui::Text("Path"); ImGui::NextColumn();
	ImGui::Separator();
	const UInt32 uLineCount = Math::Max(8u, m_arrProjectName.ElementCount());
	for (UInt32 u = 0; u < uLineCount; u++)
	{
		if (u < m_arrProjectName.ElementCount())
		{
			if (ImGui::Selectable(m_arrProjectName[u].CString(), (SInt32)u == m_nSelectedIndex, 
				ImGuiSelectableFlags_SpanAllColumns | ImGuiSelectableFlags_AllowDoubleClick))
			{
				m_nSelectedIndex = (SInt32)u;
				if (ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
				{
					m_bOpened = True;
				}
			}
			ImGui::NextColumn();
			ImGui::Text(m_arrProjectDate[u].CString()); ImGui::NextColumn();
			ImGui::Text(m_arrProjectPath[u].CString()); ImGui::NextColumn();
		}
		else
		{
			ImGui::Text(""); ImGui::NextColumn();
			ImGui::Text(""); ImGui::NextColumn();
			ImGui::Text(""); ImGui::NextColumn();
		}
	}
	ImGui::Separator();
	ImGui::EndChild();

	Float1 fWidth = (io.DisplaySize.x - 8.0f * 4.0f) / 3.0f;
	Float1 fHeight = fResHeight * 0.2f - 8.0f * 3.0f;
	if (ImGui::Button("Add", ImVec2(fWidth, fHeight)))
	{
		Char FileFullName[Constant_MAX_PATH] = { 0 };

		if (System::DlgSelectFolder("Select Project Folder", FileFullName, Constant_MAX_PATH))
		{
			String strFileName = FileFullName;
			SInt32 nLast = strFileName.FindLastOf("\\");
			if (nLast > 1)
			{
				strFileName.Erase(0, nLast + 1);

				m_arrProjectName.Add(strFileName);

				LocalTime t = System::GetLocalTime();

				m_arrProjectDate.Add(
					String::Format("%02d/%02d/%02d|%02d:%02d:%02d",
						t.uMouth, t.uDay, t.uYear,
						t.uHour, t.uMinute, t.uSecond)
				);
				m_arrProjectPath.Add(FileFullName);
			}
		}
	}

	ImGui::SameLine(fWidth + 8.0f * 2.0f);
	if (m_nSelectedIndex > -1 && m_nSelectedIndex < (SInt32)m_arrProjectName.ElementCount())
	{
		if (ImGui::Button("Remove", ImVec2(fWidth, fHeight)))
		{
			m_arrProjectName.Erase(m_nSelectedIndex, 1);
			m_arrProjectDate.Erase(m_nSelectedIndex, 1);
			m_arrProjectPath.Erase(m_nSelectedIndex, 1);
			if (m_arrProjectName.ElementCount() == 0)
				m_nSelectedIndex = -1;
		}
		ImGui::SameLine(fWidth * 2.0f + 8.0f * 3.0f);
		if (ImGui::Button("Open", ImVec2(fWidth, fHeight)))
		{
			m_bOpened = True;
		}
	}
	ImGui::End();
}

void ProjectSelectWindow::Render()
{
	GRendererConfigure configure;
	configure.fResolutionRatio = 1.0f;
	GraphicsRenderer::Render(m_pViewFrustum, configure);
}
