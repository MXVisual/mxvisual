#include "Script_SampleProject.h"

#include "PlayerController.h"
#include "AudioFFTPlayer.h"
#include "PlayerMeshComponent.h"

using namespace MXVisual;

Void EnumerateComponents_Script_Project(Array<String>& arrComponentName)
{
#define MAKE_COMPONENT(name) arrComponentName.Add(#name);

	MAKE_COMPONENT(PlayerController);
	MAKE_COMPONENT(AudioFFTPlayer);
	MAKE_COMPONENT(PlayerMeshComponent);

#undef MAKE_COMPONENT
}

ICComponent* CreateComponent_Script_Project(const Char* szComponentName)
{
#define REALISE_(type)\
	if(_stricmp(szComponentName, #type) == 0)\
		return new type;

	REALISE_(PlayerController);
	REALISE_(AudioFFTPlayer);
	REALISE_(PlayerMeshComponent);

#undef REALISE_
	return nullptr;
}