#include "PlayerMeshComponent.h"
#include "MXInterface.h"
#include "MXTimer.h"
#include "MXAsset.h"
#include "MXFile.h"
#include "MXEngine.h"

#include "CRigidBodyComponent.h"

namespace MXVisual
{
	PlayerMeshComponent::PlayerMeshComponent()
		: m_pMeshInstance(nullptr)
		, m_pSkeleton(nullptr)
		, m_pActiveAnimation(nullptr)
		, m_pAnimationIdle(nullptr)
		, m_pAnimationRun(nullptr)
		, m_pRigidBody(nullptr)
		, m_uMaterialInsCount(0)
	{
	}
	PlayerMeshComponent::~PlayerMeshComponent()
	{
		OnRelease();
	}

	Void	PlayerMeshComponent::OnInit()
	{
		ICObject* pParent = GetObject()->GetParent();
		if (pParent != nullptr)
		{
			m_pRigidBody = dynamic_cast<CRigidBodyComponent*>(pParent->GetComponent("CRigidBodyComponent"));
		}

		UpdateTransform(0.0f);
	}
	Void	PlayerMeshComponent::OnUpdate()
	{
		ASSERT_IF_FAILED(m_pRigidBody != nullptr);
		Float3 v = m_pRigidBody->GetVelocity();
		Float1 vLength = Math::Length(v);

		Float1 fAnimSpeed = 1.0f;
		if (vLength > 0.01f)
		{
			if (m_pActiveAnimation != m_pAnimationRun)
				m_pAnimationRun->SetSamplingTime(0.0f);
			m_pActiveAnimation = m_pAnimationRun;
			fAnimSpeed = Math::Min(vLength * 1e-1f, 1.0f);
		}
		else
		{
			if (m_pActiveAnimation != m_pAnimationIdle)
				m_pAnimationIdle->SetSamplingTime(0.0f);
			m_pActiveAnimation = m_pAnimationIdle;
		}

		Float1 fTime = 0.0f;
		if (m_pActiveAnimation != nullptr)
		{
			fTime = m_pActiveAnimation->GetSamplingTime() + (Float1)Timer::GetDeltaTime() * fAnimSpeed;
			fTime = fTime - (SInt32)(fTime / m_pActiveAnimation->GetTotalTime()) * m_pActiveAnimation->GetTotalTime();
		}

		UpdateTransform(fTime);
	}
	Void	PlayerMeshComponent::OnUpdateLate()
	{

	}
	Void	PlayerMeshComponent::OnRender()
	{
		if (m_pMeshInstance != nullptr)
		{
			EngineRenderMesh(m_pMeshInstance);
		}
	}
	Void	PlayerMeshComponent::OnRelease()
	{
		MX_RELEASE_INTERFACE(m_pMeshInstance);
		MX_RELEASE_INTERFACE(m_pSkeleton);
		MX_RELEASE_INTERFACE(m_pAnimationIdle);
		MX_RELEASE_INTERFACE(m_pAnimationRun);
	}
	Boolean	PlayerMeshComponent::SetMesh(const Char* szMeshName)
	{
		if (m_pMeshInstance == nullptr ||
			strcmp(GetMeshName(), szMeshName) != 0)
		{
			IAppMesh* pMesh = (IAppMesh*)AppAssetManager::GetAsset(szMeshName);
			if (pMesh == nullptr)return False;

			m_uMaterialInsCount = pMesh->GetSubMeshCount();

			MX_RELEASE_INTERFACE(m_pMeshInstance);
			m_pMeshInstance = IAppMeshInstance::Create(
				String::Format("%s_Instance", szMeshName).CString(), szMeshName);

			MX_RELEASE_INTERFACE(pMesh);
		}
		return m_pMeshInstance != nullptr;
	}

	Void	PlayerMeshComponent::UpdateTransform(Float1 fAnimationTime)
	{
		if (m_pMeshInstance == nullptr)return;

		Matrix4 matGlobal = GetObjectTransform(False);
		Array<Matrix4> arrTransform;
		if (m_pActiveAnimation != nullptr && m_pSkeleton != nullptr)
		{
			m_pActiveAnimation->Sampling(
				m_pMeshInstance->GetMesh(),
				m_pSkeleton,
				arrTransform);
			for (UInt32 u = 0; u < m_pMeshInstance->GetMesh()->GetBindingBoneCount(); u++)
			{
				arrTransform[u] = arrTransform[u] * matGlobal;
			}
			m_pActiveAnimation->SetSamplingTime(fAnimationTime);
		}
		else if (m_pMeshInstance != nullptr)
		{
			for (UInt32 u = 0; u < m_pMeshInstance->GetMesh()->GetBindingBoneCount(); u++)
			{
				arrTransform.Add(matGlobal);
			}
		}
		m_pMeshInstance->SetTransforms(arrTransform.ElementCount(), arrTransform.GetRawData());
	}

	const Char*		PlayerMeshComponent::GetMeshName()
	{
		return m_pMeshInstance != nullptr &&  m_pMeshInstance->GetMesh() != nullptr ?
			m_pMeshInstance->GetMesh()->GetName() : "";
	}

	Boolean PlayerMeshComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		String strValue;
		DeserializerBlock.Load("Skeleton", strValue);
		if (m_pSkeleton == nullptr ||
			strcmp(m_pSkeleton->GetName(), strValue.CString()) != 0)
		{
			MX_RELEASE_INTERFACE(m_pSkeleton);
			m_pSkeleton = (IAppSkeleton*)AppAssetManager::GetAsset(strValue.CString());
		}
		strValue.Clear();

		DeserializerBlock.Load("Mesh", strValue);
		SetMesh(strValue.CString()); 
		strValue.Clear();

		for (UInt32 u = 0; u < m_uMaterialInsCount; u++)
		{
			String strMaterialInstanceName;
			DeserializerBlock.Load(
				String::Format("MaterialInstance%d", u).CString(), strMaterialInstanceName);
			if (!strMaterialInstanceName.IsEmpty())
			{
				m_pMeshInstance->SetMaterialInstance(u, strMaterialInstanceName.CString());
			}
		}

		DeserializerBlock.Load("AnimationIdle", strValue);
		if (m_pAnimationIdle == nullptr ||
			strcmp(m_pAnimationIdle->GetName(), strValue.CString()) != 0)
		{
			MX_RELEASE_INTERFACE(m_pAnimationIdle);
			m_pAnimationIdle = (IAppAnimationClip*)AppAssetManager::GetAsset(strValue.CString());
		}
		strValue.Clear();

		DeserializerBlock.Load("AnimationRun", strValue);
		if (m_pAnimationRun == nullptr ||
			strcmp(m_pAnimationRun->GetName(), strValue.CString()) != 0)
		{
			MX_RELEASE_INTERFACE(m_pAnimationRun);
			m_pAnimationRun = (IAppAnimationClip*)AppAssetManager::GetAsset(strValue.CString());
		}
		strValue.Clear();
		return True;
	}

#if MX_BUILD_EDITOR
	Boolean PlayerMeshComponent::OnSerialize(ISerializer& Serializer)
	{
		if (m_pMeshInstance != nullptr)
			Serializer.Save("Mesh", m_pMeshInstance->GetMesh()->GetName());
		if (m_pSkeleton != nullptr)
			Serializer.Save("Skeleton", m_pSkeleton->GetName());
		if (m_pAnimationIdle != nullptr)
			Serializer.Save("AnimationIdle", m_pAnimationIdle->GetName());
		if (m_pAnimationRun != nullptr)
			Serializer.Save("AnimationRun", m_pAnimationRun->GetName());

		for (UInt32 u = 0; u < m_uMaterialInsCount; u++)
		{
			Serializer.Save(
				String::Format("MaterialInstance%d", u).CString(),
				m_pMeshInstance->GetMaterialInstanceName(u));
		}
		return True;
	}
	Void PlayerMeshComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyFile("Skeleton", m_pSkeleton != nullptr ? m_pSkeleton->GetName() : "", String::Format("*.%s", IAppSkeleton::Suffix).CString());
		Panel.AddPropertyFile("Mesh", GetMeshName(), String::Format("*.%s", IAppMesh::Suffix).CString());
		Panel.AddPropertyFile("AnimationIdle", m_pAnimationIdle != nullptr ? m_pAnimationIdle->GetName() : "", String::Format("*.%s", IAppAnimationClip::Suffix).CString());
		Panel.AddPropertyFile("AnimationRun", m_pAnimationRun != nullptr ? m_pAnimationRun->GetName() : "", String::Format("*.%s", IAppAnimationClip::Suffix).CString());

		for (UInt32 u = 0; u < m_uMaterialInsCount; u++)
		{
			Panel.AddPropertyFile(String::Format("MaterialInstance%d", u).CString(),
				m_pMeshInstance->GetMaterialInstanceName(u),
				String::Format("*.%s", IAppMaterialInstance::Suffix).CString());
		}
	}
	Void PlayerMeshComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFile:
			if (strcmp(pProperty->Name, "Skeleton") == 0)
			{
				if (m_pSkeleton == nullptr ||
					strcmp(m_pSkeleton->GetName(), ((EditorPropertyFile*)pProperty)->Value) != 0)
				{
					MX_RELEASE_INTERFACE(m_pSkeleton);
					m_pSkeleton = (IAppSkeleton*)AppAssetManager::GetAsset(((EditorPropertyFile*)pProperty)->Value);
				}
			}
			else if (strcmp(pProperty->Name, "Mesh") == 0)
			{
				SetMesh(((EditorPropertyFile*)pProperty)->Value);
			}
			else if (strcmp(pProperty->Name, "AnimationIdle") == 0)
			{
				if (m_pAnimationIdle == nullptr ||
					strcmp(m_pAnimationIdle->GetName(), ((EditorPropertyFile*)pProperty)->Value) != 0)
				{
					MX_RELEASE_INTERFACE(m_pAnimationIdle);
					m_pAnimationIdle = (IAppAnimationClip*)AppAssetManager::GetAsset(((EditorPropertyFile*)pProperty)->Value);
				}
			}
			else if (strcmp(pProperty->Name, "AnimationRun") == 0)
			{
				if (m_pAnimationRun == nullptr ||
					strcmp(m_pAnimationRun->GetName(), ((EditorPropertyFile*)pProperty)->Value) != 0)
				{
					MX_RELEASE_INTERFACE(m_pAnimationRun);
					m_pAnimationRun = (IAppAnimationClip*)AppAssetManager::GetAsset(((EditorPropertyFile*)pProperty)->Value);
				}
			}
			else
			{
				SInt32 nIndex;
				sscanf(pProperty->Name, "MaterialInstance%d", &nIndex);
				m_pMeshInstance->SetMaterialInstance(nIndex, ((EditorPropertyFile*)pProperty)->Value);
			}
			break;
		}

		UpdateTransform(0.0f);
	}
#endif
}