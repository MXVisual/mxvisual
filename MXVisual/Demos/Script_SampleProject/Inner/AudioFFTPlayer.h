#pragma once
#ifndef _AUDIO_FFT_PLAYER_
#define _AUDIO_FFT_PLAYER_
#include "MXCommon.h"
#include "MXCComponent.h"

namespace MXVisual
{
	class AudioFFTPlayer 
		: public ICComponent
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(AudioFFTPlayer);
	public:
		AudioFFTPlayer();
		~AudioFFTPlayer();

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif
	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRelease() override;
		Void	OnRender() override;

	protected:
		class IAppAudio*	m_pAudio;
		class IAppMesh*		m_pMesh;
		class IAppMeshInstance* m_pMeshInstance;
		class IAppMaterialInstance* m_pMtlIns;

	};
}

#endif

