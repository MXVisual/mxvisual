#include "PlayerController.h"
#include "MXCEditorPanel.h"
#include "MXCEditorGraphics.h"
#include "MXCScene.h"
#include "MXCCamera.h"
#include "MXInput.h"
#include "MXTimer.h"

namespace MXVisual
{
	PlayerController::PlayerController()
		: m_fDistance(2.0f)
		, m_uDistanceMode(0)
		, m_f2ViewRotate(0, 0)
		, m_pTPSCamera(nullptr)
		, m_pTPSMesh(nullptr)
	{
	}
	PlayerController::~PlayerController()
	{
	}

	Void	PlayerController::OnInit()
	{
		if (m_pTPSCamera == nullptr)
		{
			m_pTPSCamera = dynamic_cast<CObject*>(GetObject()->GetChild("Camera", False));
		}
		if (m_pTPSMesh == nullptr)
		{
			m_pTPSMesh = dynamic_cast<CObject*>(GetObject()->GetChild("Mesh", False));
		}
		if (m_pRigidbody == nullptr)
		{
			m_pRigidbody = dynamic_cast<CRigidBodyComponent*>(GetObject()->GetComponent("CRigidBodyComponent"));
		}
	}
	Void	PlayerController::OnUpdate()
	{
		ASSERT_IF_FAILED(m_pTPSCamera != nullptr);
		if (m_pTPSCamera == nullptr)return;

		const Float1 fDistances[] = { 8, 16, 24 };

		if (Input::GetKeyClick(Input::JoystickKey::eJoyBack) ||
			Input::GetKeyClick(Input::eV))
		{
			m_uDistanceMode = (m_uDistanceMode + 1) % 3;
		}
		m_fDistance = Math::Lerp(m_fDistance, fDistances[m_uDistanceMode], (Float1)Timer::GetDeltaTime());

		Float3 Position = GetObjectPosition(False);

		Float2 delta = Input::GetJoyStickRelativePosition(Input::eJoyStickR);
		delta += Input::GetMouseRelativePosition() * 0.5f;

		m_f2ViewRotate += delta * Float2(-1.0f, 1.0f) * 0.02f;
		m_f2ViewRotate[1] = Math::Clamp(m_f2ViewRotate[1], -Constant_PI * 0.48f, -Constant_PI * 0.01f);

		Float3 axis_r = Math::Normalize(GetObjectDirRight());
		Matrix4 matR = Math::MatrixAxisRotation(axis_r, m_f2ViewRotate[1]) *
			Math::MatrixAxisRotation(Math::AXIS_Y, m_f2ViewRotate[0]);

		Float3 view = Math::Split(Math::MatrixTransform(Float4(0, 0, 1, 0), matR));
		Matrix4 matT = Math::MatrixTranslation(view * -m_fDistance);

		m_pTPSCamera->SetTransform(matR * matT, True);

		delta = Input::GetJoyStickRelativePosition(Input::eJoyStickL) * Float2(1, -1);
		if (Input::GetKeyDown(Input::eW)) delta += Float2(0, 1.0f);
		else if (Input::GetKeyDown(Input::eS)) delta += Float2(0, -1.0f);
		if (Input::GetKeyDown(Input::eA)) delta += Float2(-1.0f, 0);
		else if (Input::GetKeyDown(Input::eD)) delta += Float2(1.0f, 0);

		delta *= 10.0f;
		Float3 f = m_pTPSCamera->GetDirForward();
		Float3 r = m_pTPSCamera->GetDirRight();
		f[1] = 0.0f;
		r[1] = 0.0f;
		f = Math::Normalize(f);
		r = Math::Normalize(r);

		m_pRigidbody->AddForce(f * delta[1] + r * delta[0]);

		if (Input::GetKeyClick(Input::JoystickKey::eJoyA) ||
			Input::GetKeyClick(Input::eSpace))
		{
			m_pRigidbody->AddForce(Float3(0.0f, 500.0f, 0.0f));
		}

		if (delta != Float2())
		{
			delta = Math::Normalize(delta);
			delta[0] = delta[0] > 0 ? 1.0f : -1.0f;

			m_pTPSMesh->SetRotation(
				Float3(0, delta[0] * (delta[1] * -0.5f + 0.5f) * Constant_PI - m_f2ViewRotate[0], 0),
				True);
		}
	}

	Void	PlayerController::OnRelease()
	{

	}
}
