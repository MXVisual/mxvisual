#include "AudioFFTPlayer.h"
#include "MXCEditorPanel.h"
#include "MXAsset.h"
#include "MXEngine.h"

namespace MXVisual
{
#define FFT_DATA_SIZE 32

	AudioFFTPlayer::AudioFFTPlayer()
		: m_pAudio(nullptr)
		, m_pMesh(nullptr)
		, m_pMeshInstance(nullptr)
		, m_pMtlIns(nullptr)
	{
	}
	AudioFFTPlayer::~AudioFFTPlayer()
	{
		MX_RELEASE_INTERFACE(m_pMtlIns);
		MX_RELEASE_INTERFACE(m_pMeshInstance);
		MX_RELEASE_INTERFACE(m_pMesh);
		MX_RELEASE_INTERFACE(m_pAudio);
	}

	Void	AudioFFTPlayer::OnInit()
	{
		if (m_pAudio != nullptr)
			EnginePlayAudio(m_pAudio);

		if (m_pMtlIns == nullptr)
		{
			m_pMtlIns = (IAppMaterialInstance*)AppAssetManager::GetAsset(DefaultResource::MATERIALINSTANCE);
		}

		if (m_pMesh == nullptr)
		{
			m_pMesh = IAppMesh::Create("AudioFFTPlayer", IAppMesh::EAPPMT_Line, True,
				FFT_DATA_SIZE * 4, FFT_DATA_SIZE * 6, 1);

			UInt32 fftIndex[FFT_DATA_SIZE * 6];
			UInt32 uIndex = 0;
			for (UInt32 u = 0; u < FFT_DATA_SIZE; u++)
			{
				fftIndex[u * 6] = u * 4;
				fftIndex[u * 6 + 1] = u * 4 + 1;

				fftIndex[u * 6 + 2] = u * 4 + 1;
				fftIndex[u * 6 + 3] = u * 4 + 2;

				fftIndex[u * 6 + 4] = u * 4 + 2;
				fftIndex[u * 6 + 5] = u * 4 + 3;
			}
			m_pMesh->SetIndices(FFT_DATA_SIZE * 6, (UInt8*)fftIndex);
			m_pMesh->SetSubMesh(0, 0, FFT_DATA_SIZE * 6);

			m_pMeshInstance = IAppMeshInstance::Create("AudioFFTPlayerIns", m_pMesh->GetName());
			m_pMeshInstance->SetMaterialInstance(0, m_pMtlIns->GetName());
		}
	}
	Void	AudioFFTPlayer::OnUpdate()
	{
		static Array<Float1> PCFData;
		static Float1 FFTData[44100] = { 0 };//最多44100个数据
		static IAppMesh::VertexRigid v[FFT_DATA_SIZE * 4];

		if (m_pAudio == nullptr)return;

		PCFData.Clear();
		m_pAudio->SamplePCF(m_pAudio->GetCurrentSecond(),
			PCFData, m_pAudio->GetSampleRate());
		if (m_pAudio->GetChannelType() == IAppAudio::ChannelType::EStero)
		{
			for (UInt32 u = 0; u < PCFData.ElementCount() / 2; u++)
			{
				PCFData[u] = (PCFData[u * 2] + PCFData[u * 2 + 1]) * 0.5f;
			}
			PCFData.SetElementCount(PCFData.ElementCount() / 2);
		}
		Math::FFT_Transform(PCFData.GetRawData(), FFTData, PCFData.ElementCount());

		UInt32 uFFTData = PCFData.ElementCount() / 2;
		UInt32 uAverageRange = 1;
		Float1 fAverage = 0.0f;
		UInt32 vIndex = 0;

		Float1 fBaseRadius = 0.8f;
		Float1 fBaseY = 0.0f;
		Float1 fStartAngle = Constant_PI;
		Float1 fAngle = 0.0f;
		Float4 cyan(0.5f, 0.8f, 0.9f, 1.0f);

		Float1 fStepAngle = Constant_PI * 2.0f / FFT_DATA_SIZE;
		Float1 fBase = 1.035f;

		UInt32 uSection = 0;
		for (UInt32 u = 0; u < uFFTData && vIndex < FFT_DATA_SIZE * 4; u++)
		{
			fAverage += FFTData[u];
			uAverageRange--;
			if (uAverageRange == 0)
			{
				fAverage /= pow(fBase, uSection == 0 ? 0 : (uSection - 1)) * 65535.0f;
				fAverage = log10f(fAverage) * 0.5f;

				uAverageRange = pow(fBase, uSection);
				uSection++;

				fAngle = -fStepAngle;

				v[vIndex].f4Color = cyan;
				v[vIndex].f4Position = Float4(
					cosf(fStartAngle), 0.0f, sinf(fStartAngle), 0) * fBaseRadius;
				v[vIndex].f4Position[1] = fBaseY;
				vIndex++;

				v[vIndex].f4Color = cyan;
				v[vIndex].f4Position = Float4(
					cosf(fStartAngle), 0.0f, sinf(fStartAngle), 0) * (fBaseRadius + fAverage);
				v[vIndex].f4Position[1] = fBaseY;
				vIndex++;

				v[vIndex].f4Color = cyan;
				v[vIndex].f4Position = Float4(
					cosf(fStartAngle + fAngle), 0.0f, sinf(fStartAngle + fAngle), 0) * (fBaseRadius + fAverage);
				v[vIndex].f4Position[1] = fBaseY;
				vIndex++;

				v[vIndex].f4Color = cyan;
				v[vIndex].f4Position = Float4(
					cosf(fStartAngle + fAngle), 0.0f, sinf(fStartAngle + fAngle), 0) * fBaseRadius;
				v[vIndex].f4Position[1] = fBaseY;
				vIndex++;

				fAverage = 0.0f;

				fStartAngle += fAngle;
			}
		}

		m_pMesh->SetVertices(FFT_DATA_SIZE * 4, v);
	}
	Void	AudioFFTPlayer::OnRelease()
	{

	}
	Void	AudioFFTPlayer::OnRender()
	{
		if (m_pMeshInstance != nullptr)
		{
			Matrix4 mat = GetObjectTransform(False);
			m_pMeshInstance->SetTransforms(1, &mat);
			EngineRenderMesh(m_pMeshInstance);
		}
	}

	Boolean	AudioFFTPlayer::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		MX_RELEASE_INTERFACE(m_pAudio);
		String file;
		DeserializerBlock.Load("Audio", file);

		m_pAudio = (IAppAudio*)AppAssetManager::GetAsset(file.CString());
		DeserializerBlock.Load("MaterialInstance", file);
		m_pMtlIns = (IAppMaterialInstance*)AppAssetManager::GetAsset(file.CString());

		return True;
	}

#if MX_BUILD_EDITOR
	Void	AudioFFTPlayer::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyFile("Audio", m_pAudio != nullptr ? m_pAudio->GetName() : "", "flac");
		Panel.AddPropertyFile("MaterialInstance", m_pMtlIns != nullptr ? m_pMtlIns->GetName() : "", IAppMaterialInstance::Suffix);
	}
	Void	AudioFFTPlayer::OnPanelChanged(const IEditorProperty* pProperty)
	{
		if (strcmp(pProperty->Name, "Audio") == 0)
		{
			String str = ((EditorPropertyFile*)pProperty)->Value;
			MX_RELEASE_INTERFACE(m_pAudio);
			m_pAudio = (IAppAudio*)AppAssetManager::GetAsset(str.CString());
		}
		else if (strcmp(pProperty->Name, "MaterialInstance") == 0)
		{
			String str = ((EditorPropertyFile*)pProperty)->Value;
			MX_RELEASE_INTERFACE(m_pMtlIns);
			m_pMtlIns = (IAppMaterialInstance*)AppAssetManager::GetAsset(str.CString());
			if (m_pMeshInstance != nullptr)
				m_pMeshInstance->SetMaterialInstance(0, m_pMtlIns->GetName());
		}
	}

	Boolean	AudioFFTPlayer::OnSerialize(ISerializer& Serializer)
	{
		if (m_pAudio != nullptr)
		{
			Serializer.Save("Audio", m_pAudio->GetName());
		}

		if (m_pMtlIns != nullptr)
		{
			Serializer.Save("MaterialInstance", m_pMtlIns->GetName());
		}
		return True;
	}
#endif
}
