#pragma once
#ifndef _PLAYER_CONTROLLER_
#define _PLAYER_CONTROLLER_
#include "MXCommon.h"
#include "MXCComponent.h"
#include "CRigidBodyComponent.h"

namespace MXVisual
{
	class PlayerController 
		: public ICComponent
	{
		DECLARE_RTTI_INFO(PlayerController);
	public:
		PlayerController();
		~PlayerController();

	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRelease() override;

	protected:
		class CObject* m_pTPSCamera;
		class CObject* m_pTPSMesh;
		CRigidBodyComponent* m_pRigidbody;

		Float2 m_f2ViewRotate;
		Float1 m_fDistance;
		UInt32 m_uDistanceMode;
	};
}

#endif

