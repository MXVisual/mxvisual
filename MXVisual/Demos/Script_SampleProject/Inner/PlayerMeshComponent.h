#pragma once
#ifndef _PLAYER_MESH_COMPONENT_
#define _PLAYER_MESH_COMPONENT_
#include "MXCComponent.h"
#include "MXMath.h"
#include "MXArray.h"
#include "MXCEditorPanel.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	class PlayerMeshComponent
		: public ICComponent
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(PlayerMeshComponent);
	public:
		PlayerMeshComponent();
		~PlayerMeshComponent();

		Boolean				SetMesh(const Char* szMeshName);

		const Char*			GetMeshName();

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif

	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnUpdateLate() override;
		Void	OnRender() override;
		Void	OnRelease() override;

		Void	UpdateTransform(Float1 fAnimationTime);
	protected:
		class IAppSkeleton* m_pSkeleton;
		class IAppMeshInstance* m_pMeshInstance;

		class CRigidBodyComponent* m_pRigidBody;
		class IAppAnimationClip* m_pActiveAnimation;

		class IAppAnimationClip* m_pAnimationIdle;
		class IAppAnimationClip* m_pAnimationRun;

		UInt32 m_uMaterialInsCount;
	};
}
#endif