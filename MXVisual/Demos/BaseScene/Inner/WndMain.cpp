#include "MXPlatform.h"
#include "MXMediaResource.h"
#include "MXPhysicsSimulator.h"
#include "MXGraphicsRenderer.h"
#include "MXGraphicsResource.h"
#include "MXMediaPlayer.h"
#include "MXCCamera.h"

#include "MXCScene.h"
#include "MXCSceneRender.h"
#include "MXCObject.h"
#include "MXCComponent.h"

#include "MXTimer.h"
#include "MXInput.h"
#include "MXAsset.h"
#include "MXThread.h"

#include "MXLog.h"
#include "MXCSerializer.h"

#include <functional>
#include "MXFile.h"
#include "Windows.h"

using namespace MXVisual;

IGRenderWindow* g_pRWindow = nullptr;
Array<CScene*> g_arrScenePool;
UInt32 uActiveScene = 0;

Boolean ApplicationInit()
{
	MXVisual::GraphicsRenderer::InitParam initparams;
	initparams.szAdapterName = nullptr;
	initparams.uBackBufferWidth = 1920;
	initparams.uBackBufferHeight = 1080;
	if (!MXVisual::GraphicsRenderer::Init(initparams))
	{
		MessageBox(NULL, "Init Failed. May Be Resources Lost!", "ERROR", MB_OK);
		return False;
	}

	MXVisual::MediaPlayer::InitParam mi_params;
	if (!MXVisual::MediaPlayer::Init(mi_params))
	{
		return False;
	}

	MXVisual::PhysicsSimulator::InitParam ps_params;
	if (!MXVisual::PhysicsSimulator::Init(ps_params))
	{
		return False;
	}

	MXVisual::ThreadManager::Init();

	Char workPath[Constant_MAX_PATH] = { 0 };
	GetWorkingPath(workPath);
	Char assetPath[Constant_MAX_PATH] = { 0 };
	CalAbsolutePath(workPath, "../../../Resources/SampleProject", assetPath, Constant_MAX_PATH);

	MXVisual::AppAssetManager::InitParams params;
	params.szAssetsPath = assetPath;
	if (!MXVisual::AppAssetManager::Init(params))
	{
		return False;
	}

	MXVisual::IGRenderWindow::Description desc;
	desc.uParentWindowHandle = NULL;
	desc.uBackbufferCount = 2;
	desc.eBackbufferFormat = MXVisual::PixelFormat::EPF_R8G8B8A8;
	desc.uBackbufferWidth = 1366;
	desc.uBackbufferHeight = 768;
	g_pRWindow = MXVisual::GraphicsResource::CreateRenderWindow(desc);
	if (g_pRWindow == nullptr)return False;

	Timer::Update();
	g_pRWindow->Show(True);

	String SceneFileName = String::Format("%s/SampleProject.mscn", assetPath);

	ThreadManager::AddThreadTask(
		IO_THREAD_NAME,
		[SceneFileName]() {
		IDeserializer* pDeserial = TextDeserializer::Open(SceneFileName.CString());
		ASSERT_IF_FAILED(pDeserial != nullptr);
		ASSERT_IF_FAILED(pDeserial != nullptr);

		CScene* pNewScene = new CScene("");
		pNewScene->OnDeserialize(*pDeserial->GetRootDeserializerBlock());
		pDeserial->Close();
		g_arrScenePool.Add(pNewScene);
		pNewScene->Active(); },
		"Scene Loading");

	return True;
}
Void ApplicationRelease()
{
	for (UInt32 u = 0; u < g_arrScenePool.ElementCount(); u++)
	{
		delete g_arrScenePool[u];
	}
	g_arrScenePool.Clear();
	MX_RELEASE_INTERFACE(g_pRWindow);

	MXVisual::PhysicsSimulator::Release();

	MXVisual::AppAssetManager::Release();

	MXVisual::GraphicsRenderer::Release();

	MXVisual::ThreadManager::Release();
}

Void Update()
{
	if (CScene::ActivedScene() != nullptr)
		CScene::ActivedScene()->OnUpdate();
}
Void Render()
{
	if (CScene::ActivedScene() != nullptr)
	{
		if (CScene::ActivedScene()->MainCamera() != nullptr &&
			CScene::ActivedScene()->MainCamera()->GetViewFrustum() == nullptr)
		{
			CScene::ActivedScene()->MainCamera()->SetRenderWindow(g_pRWindow);
		}

		CSceneRender::Render(CScene::ActivedScene(), CScene::ActivedScene()->MainCamera());

		g_pRWindow->Present();
	}
}
Void ApplicationLoop()
{
	Update();
	Render();
}

int _stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lp1CmdLine, int nCmdShow)
{
	Application::FOnInit = ApplicationInit;
	Application::FOnRelease = ApplicationRelease;
	Application::FOnLoop = ApplicationLoop;

	return Application::Run();
}