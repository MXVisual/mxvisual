#include "UICheckbox.h"
#include "MXInput.h"

namespace MXVisual
{
	UICheckbox::UICheckbox(const Char * szName)
		: CObject2D(szName)
		, m_bCheck(True)
	{
	}

	UICheckbox::~UICheckbox()
	{
	}

	Void UICheckbox::OnInit()
	{
		m_pBtnComponent = new CUIButtonComponent;
		AddComponent(m_pBtnComponent);
	}

	Void UICheckbox::SetCheck(Boolean bCheck)
	{
		if (m_bCheck != bCheck)
		{
			m_bCheck = bCheck;
			m_pBtnComponent->SetColor(m_bCheck ? Float4(1, 1, 1, 1) : Float4(0.4f, 0.4f, 0.4f, 1.0f));
		}
	}

	Void UICheckbox::SetText(const Char* szText)
	{
		if (m_pTextComponent == nullptr)
		{
			CObject2D* pChild = new CObject2D("_childtext");
			AddChild(pChild);

			pChild->SetPosition(Float2(1.0f, 0), True);
			m_pTextComponent = new CUITextComponent;
			pChild->AddComponent(m_pTextComponent);

			m_pTextComponent->SetFont("..\\..\\..\\Resources\\Fonts\\arial.ttf");

			Float2 s = GetScale(False);
			m_pTextComponent->SetCharacterSize(s[1]);
			m_pTextComponent->SetColor(Float4(0, 0, 0, 1));
		}

		m_pTextComponent->SetText(szText);
	}

	Void UICheckbox::OnMouseInside()
	{
		if (Input::GetKeyClick(Input::eLButton))
		{
			SetCheck(!GetCheck());
		}

		CObject2D::OnMouseInside();
	}

}