#pragma once
#ifndef DEMO_UI_SLIDER
#define DEMO_UI_SLIDER
#include "MXCUIComponent.h"
#include "MXCObject.h"

namespace MXVisual
{
	DECLARE_SCENEOBJECT_BEGINE(UISlider, CObject2D)
public:
	Void SetHorizon(Boolean bHorizon);
	Void SetStep(Float1 fStep);
	Void SetRate(Float1 fRate);
	Void SetRange(Float1 fMin, Float1 fMax);

protected:
	Void OnInit() override;
	Void OnTick() override;
	Void OnMouseInside() override;

protected:
	Float1 m_fStep = 0.1f;
	Float1 m_fRate = 0.0f;
	Float2 m_f2Range = Float2(0.0f, 1.0f);
	Boolean m_bHorizon = False;
	CUIImageComponent* m_pImgBackground = nullptr;
	CObject2D* m_pSliderBtn = nullptr;

	DECLARE_SCENEOBJECT_END()
}
#endif