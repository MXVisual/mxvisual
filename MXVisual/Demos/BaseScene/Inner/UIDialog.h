#pragma once
#ifndef DEMO_UI_DIALOG
#define DEMO_UI_DIALOG
#include "MXCUIComponent.h"
#include "MXCObject.h"

namespace MXVisual
{
	DECLARE_SCENEOBJECT_BEGINE(UIDialog, CObject2D)
public:
	enum Style
	{
		ESizable = 0x1,
		ETitle = 0x2,
		ECloseBtn = 0x4,
		EScrollBar = 0x8,

		ENormal = ESizable | ETitle | ECloseBtn
	};
	Void SetStyle(UInt32 uStyle);

protected:
	Void OnInit() override;

	Void OnMouseInside() override;

	Void UpdateDialog();

protected:
	UInt32 m_uStype = ENormal;
	CUIImageComponent* m_pImgComponent = nullptr;
	CObject2D* m_pTitle = nullptr;
	CObject2D* m_pScrollbar = nullptr;
	CObject2D* m_pCloseBtn = nullptr;
	CObject2D* m_pContainer = nullptr;
	DECLARE_SCENEOBJECT_END()
}
#endif