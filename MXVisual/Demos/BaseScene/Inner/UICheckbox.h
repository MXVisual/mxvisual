#pragma once
#ifndef DEMO_UI_CHECKBOX
#define DEMO_UI_CHECKBOX
#include "MXCObject.h"

namespace MXVisual
{
	class UICheckbox : public CObject2D
	{
	public:
		UICheckbox(const Char* szName);
		~UICheckbox();

		Void SetCheck(Boolean bCheck);
		Boolean GetCheck() const { return m_bCheck; }

		Void SetText(const Char* szText);

	protected:
		Void OnMouseInside() override;

		Void OnInit() override;

	protected:
		Boolean m_bCheck;

		CUIButtonComponent* m_pBtnComponent;
		CUITextComponent* m_pTextComponent;
	};
}
#endif