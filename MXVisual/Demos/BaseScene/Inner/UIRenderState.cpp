#include "UIRenderState.h"
#include "MXTimer.h"

namespace MXVisual
{
	UIRenderState::UIRenderState(const Char * szName)
		: CObject2D(szName)
	{
	}

	UIRenderState::~UIRenderState()
	{
	}

	Void UIRenderState::OnInit()
	{
		m_pTextComponent = new CUITextComponent;
		AddComponent(m_pTextComponent);

		const Char* szFont = "..\\..\\..\\Resources\\Fonts\\arial.ttf";
		m_pTextComponent->SetText("FPS:\nMemory:");
		m_pTextComponent->SetFont(szFont);
		m_pTextComponent->SetColor(Float4(1.0f, 0, 0, 0.9f));
		m_pTextComponent->SetCharacterSize(20.0f);
	}

	Void UIRenderState::OnTick()
	{
		String str = String::Format("Fps:%3.1f\nDeltaTime:%.4fs\nMemory:%.2fmb\n",
			1.0f / Timer::GetDeltaTime(),
			Timer::GetDeltaTime(),
			MemoryPool::GetMemoryOccupied() / 1024.0f);
		m_pTextComponent->SetText(str.ToASCII());
	}
}