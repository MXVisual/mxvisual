#include "UISlider.h"
#include "UIButton.h"
#include "MXGraphicsResource.h"

namespace MXVisual
{
	Void UISlider::OnInit()
	{
		SetScale(Float2(16.0f, 256.0f), True);

		if (m_pImgBackground == nullptr)
		{
			m_pImgBackground = new CUIImageComponent;
			AddComponent(m_pImgBackground);
			m_pImgBackground->SetTexture(DEFAULT_WHITE_TEXTURE);
			m_pImgBackground->SetColor(Float4(0.5f, 0.5f, 0.5f, 0.8f));
		}

		if (m_pSliderBtn == nullptr)
		{
			m_pSliderBtn = new UIButton("_sliderbtn");
			AddChild(m_pSliderBtn);
			m_pSliderBtn->OnInit();
			m_pSliderBtn->SetScale(
				m_bHorizon ? Float2(m_fStep / (m_f2Range[1] - m_f2Range[0]), 1.0f) :
				Float2(1.0f, m_fStep / (m_f2Range[1] - m_f2Range[0])),
				True);
		}
	}
	Void UISlider::OnTick()
	{
		Float1 p = 1.0f - m_fStep / (m_f2Range[1] - m_f2Range[0]);
		p *= m_fRate;
		m_pSliderBtn->SetPosition(
			m_bHorizon ? Float2(p, 0) : Float2(0, p),
			True);
	}

	Void UISlider::OnMouseInside()
	{
		Float1 fWheel = Input::GetMouseWheelRelativePosition();
		if (Math::Abs(fWheel) > Constant_MIN_FLOAT)
		{
			m_fRate = Math::Clamp(m_fRate - m_fStep * fWheel, m_f2Range[0], m_f2Range[1]);
		}
	}

	Void UISlider::SetHorizon(Boolean bHorizon)
	{
		m_bHorizon = bHorizon;
		m_pSliderBtn->SetScale(
			m_bHorizon ? Float2(m_fStep / (m_f2Range[1] - m_f2Range[0]), 1.0f) :
			Float2(1.0f, m_fStep / (m_f2Range[1] - m_f2Range[0])),
			True);
	}
	Void UISlider::SetStep(Float1 fStep)
	{
		m_fStep = fStep;
	}
	Void UISlider::SetRate(Float1 fRate)
	{
		m_fRate = Math::Clamp(fRate, m_f2Range[0], m_f2Range[1]);
	}
	Void UISlider::SetRange(Float1 fMin, Float1 fMax)
	{
		m_f2Range = Float2(fMin > fMax ? fMax : fMin, fMax < fMin ? fMin : fMax);
		m_fRate = Math::Clamp(m_fRate, m_f2Range[0], m_f2Range[1]);
	}

}