#include "UIButton.h"

namespace MXVisual
{
	UIButton::UIButton(const Char * szName)
		: CObject2D(szName)
	{
	}

	UIButton::~UIButton()
	{
	}

	Void UIButton::OnInit()
	{
		m_pBtnComponent = new CUIButtonComponent;
		AddComponent(m_pBtnComponent);
	}

	Void UIButton::SetText(const Char* szText)
	{
		if (m_pTextComponent == nullptr)
		{
			CObject2D* pChild = new CObject2D("_childtext");
			AddChild(pChild);

			m_pTextComponent = new CUITextComponent;
			pChild->AddComponent(m_pTextComponent);

			m_pTextComponent->SetFont("..\\..\\..\\Resources\\Fonts\\arial.ttf");
			Float2 s = GetScale(False);
			m_pTextComponent->SetCharacterSize(s[1] * 0.8f);
		}

		m_pTextComponent->SetText(szText);
	}

	Void UIButton::SetTextColor(const Float4& f4Color)
	{
		if (m_pTextComponent == nullptr)
		{
			CObject2D* pChild = new CObject2D("_childtext");
			AddChild(pChild);

			m_pTextComponent = new CUITextComponent;
			pChild->AddComponent(m_pTextComponent);

			m_pTextComponent->SetFont("..\\..\\..\\Resources\\Fonts\\arial.ttf");
			m_pTextComponent->SetCharacterSize(m_f2Scale[1] * 0.8f);
		}
		m_pTextComponent->SetColor(f4Color);
	}
}