#pragma once
#ifndef DEMO_UI_RENDERSTATE
#define DEMO_UI_RENDERSTATE
#include "MXCUIComponent.h"
#include "MXCObject.h"

namespace MXVisual
{
	class UIRenderState : public CObject2D
	{
	public:
		UIRenderState(const Char* szName);
		~UIRenderState();

	protected:
		Void OnInit() override;
		Void OnTick() override;

	protected:
		CUITextComponent* m_pTextComponent;
	};
}
#endif