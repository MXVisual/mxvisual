#include "UIDialog.h"
#include "UISlider.h"
#include "UIButton.h"
#include "MXGraphicsResource.h"

namespace MXVisual
{
	Void UIDialog::OnInit()
	{
		if (m_pImgComponent == nullptr)
		{
			m_pImgComponent = new CUIImageComponent;
			AddComponent(m_pImgComponent);
			m_pImgComponent->SetTexture(DEFAULT_WHITE_TEXTURE);
			m_pImgComponent->SetColor(Float4(0.5f, 0.5f, 0.5f, 0.8f));
		}

		if (m_pContainer == nullptr)
		{
			m_pContainer = new CObject2D("_container");
			AddChild(m_pContainer);
		}
		UpdateDialog();
	}

	Void UIDialog::SetStyle(UInt32 uStyle)
	{
		if (uStyle != m_uStype)
		{
			m_uStype = uStyle;
			UpdateDialog();
		}
	}

	Void UIDialog::UpdateDialog()
	{
		if (m_uStype | ETitle && (m_pTitle == nullptr))
		{
			m_pTitle = new CObject2D("_title");
			AddChild(m_pTitle);

			CUIImageComponent* pTitleImg = new CUIImageComponent;
			CUITextComponent* pTitleText = new CUITextComponent;
			m_pTitle->AddComponent(pTitleImg);
			m_pTitle->AddComponent(pTitleText);
			m_pTitle->SetScale(Float2(1.0f, 0.1f), True);

			pTitleText->SetFont("..\\..\\..\\Resources\\Fonts\\arial.ttf");
			pTitleText->SetCharacterSize(24.0f);
			pTitleText->SetText(m_strName.ToASCII());
			pTitleImg->SetTexture(DEFAULT_WHITE_TEXTURE);
			pTitleImg->SetColor(Float4(0.8f, 0.8f, 0.8f, 0.8f));
		}

		if (m_uStype | ECloseBtn && (m_pCloseBtn == nullptr))
		{
			UIButton* pBtn = new UIButton("_closebtn");
			m_pCloseBtn = pBtn;
			AddChild(m_pCloseBtn);
			m_pCloseBtn->SetAnchorPosition(Float2(1.0f, 0.0f));
			m_pCloseBtn->SetPosition(Float2(1.0f, 0.0f), True);
			m_pCloseBtn->SetScale(Float2(0.08f, 0.1f), True);
			pBtn->SetText("X");
			pBtn->SetTextColor(Float4(1, 0, 0, 1));
		}

		if (m_uStype | EScrollBar && (m_pScrollbar == nullptr))
		{
			m_pScrollbar = new UISlider("_scrollbar");
			AddChild(m_pScrollbar);
			m_pScrollbar->SetPosition(Float2(0.95f, 0.1f), True);
			m_pScrollbar->SetScale(Float2(0.05f, 0.9f), True);
		}

		m_pContainer->SetPosition(Float2(0, m_uStype | ETitle ? 0.1f : 0.0f), True);
		m_pContainer->SetScale(Float2(
			m_uStype | EScrollBar ? 0.95f : 1.0f,
			m_uStype | ETitle ? 0.9f : 1.0f), True);
	}

	Void UIDialog::OnMouseInside()
	{
		if (Input::GetKeyDown(Input::eLButton))
		{
			TransMove(Input::GetMouseRelativePosition(), 1.0f);
		}
	}
}