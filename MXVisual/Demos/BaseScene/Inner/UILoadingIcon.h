#pragma once
#ifndef DEMO_UI_LOADINGICON
#define DEMO_UI_LOADINGICON
#include "MXCUIComponent.h"
#include "MXCObject.h"

namespace MXVisual
{
	class UILoadingIcon : public CObject2D
	{
	public:
		UILoadingIcon(const Char* szName);

	protected:
		Void OnInit() override;
		Void OnTick() override;
	};
}
#endif