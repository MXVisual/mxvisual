#include "UILoadingIcon.h"
#include "MXTimer.h"

namespace MXVisual
{
	UILoadingIcon::UILoadingIcon(const Char * szName)
		: CObject2D(szName)
	{
	}

	Void UILoadingIcon::OnInit()
	{
		CUIImageComponent* pComponentIcon = new CUIImageComponent();
		AddComponent(pComponentIcon);

		pComponentIcon->SetTexture("..\\..\\..\\Resources\\Sample\\IconLoading.dds");
	}

	Void UILoadingIcon::OnTick()
	{
		TransRotate(Timer::GetDeltaTime() * 3);
	}
}