#pragma once
#ifndef DEMO_UI_BUTTON
#define DEMO_UI_BUTTON
#include "MXCObject.h"

namespace MXVisual
{
	class UIButton : public CObject2D
	{
	public:
		UIButton(const Char* szName);
		~UIButton();

		Void SetText(const Char* szText);
		Void SetTextColor(const Float4& f4Color);
		
	protected:
		Void OnInit() override;

	protected:
		CUIButtonComponent* m_pBtnComponent;
		CUITextComponent* m_pTextComponent;
	};
}
#endif