#pragma once
#ifndef _MX_GRAPHICS_RESOURCE_
#define _MX_GRAPHICS_RESOURCE_
#include "MXPlatform.h"
#include "MXArray.h"
#include "MXVector.h"
#include "MXMatrix.h"
#include "MXString.h"
#include "MXInterface.h"

#if defined(USE_DLL_GRAPHICS)
#define DLL_DELCARE __declspec(dllimport)
#elif defined(DLL_GRAPHICS)
#define DLL_DELCARE __declspec(dllexport)
#else 
#define DLL_DELCARE
#endif

namespace MXVisual
{
	enum PixelFormat
	{
		EPF_R8,
		EPF_R8G8,
		EPF_R8G8B8A8,

		EPF_R16_F,
		EPF_R16G16_F,
		EPF_R16G16B16A16_F,
		EPF_R16G16B16A16,

		EPF_R32_F,
		EPF_R32G32_F,
		EPF_R32G32B32A32_F,

		//DDS Format
		EPF_BC1,
		EPF_BC3,
		EPF_BC4,
		EPF_BC5,
		EPF_BC6H,
		EPF_BC7,

		//Depth
		EPF_D24,
		EPF_D32,

		EPF_Count,
		EPF_Invalid = EPF_Count
	};
	enum class TextureType : UInt32
	{
		ETexture1D,
		ETexture2D,
		ETexture3D,
		ETextureCube,
		ECount,
		EInvalid = ECount
	};
	enum TextureAddressMode
	{
		ETexAM_Repeat,
		ETexAM_Clamp,
		ETexAM_Mirror,

		ETexAM_Count,
		ETexAM_Invalid = ETexAM_Count
	};
	enum TextureFilterMode
	{
		ETexFM_Point,
		ETexFM_Bilinear,
		ETexFM_Anisotropic,

		ETexFM_Count,
		ETexFM_Invalid = ETexFM_Count
	};

	struct MapRegion
	{
		UInt32	uPitch;
		Void*	pAddress;
	};

	class DLL_DELCARE IGResource : public IInterface
	{
	public:
		virtual ~IGResource() {}

		virtual UInt32 GetUniqueID() const = 0;
		Void	Release() override;
	};

	class DLL_DELCARE IGTexture : public IGResource
	{
	public:
		struct Description
		{
			PixelFormat eFormat = EPF_Invalid;
			TextureType	eType = TextureType::EInvalid;
			Boolean bDynamic = False;
			Boolean bTargetable = False;
			Boolean bUnorderAccessable = False;
			Boolean bGenerateMipMap = False;
			UInt32	uWidth = 0;
			UInt32	uHeight = 0;
			UInt32	uDepth = 0;
			UInt32	uMipLevel = 0;
			Float4	f4ClearValue = Float4(0.0f, 0.0f, 0.0f, 0.0f);//Only Work For Color Texture
		};

		virtual PixelFormat GetFormat() const = 0;
		virtual TextureType GetType()	const = 0;

		virtual UInt32		GetWidth()	const = 0;
		virtual UInt32		GetHeight() const = 0;
		virtual UInt32		GetDepth()	const = 0;
		virtual UInt32		GetMipmapCount() const = 0;

		virtual Boolean	Map(UInt32 uDepth, UInt32 uMipMap, MapRegion& region, Boolean bReadBack = False) = 0;
		virtual Void	Unmap() = 0;
	};

	class DLL_DELCARE IGRenderWindow : public IGResource
	{
	public:
		struct Description
		{
			UInt64 uParentWindowHandle = 0;
			const Char* szWindowName = nullptr;

			PixelFormat eBackbufferFormat = EPF_Invalid;
			UInt32 uBackbufferCount = 0;

			UInt32 uBackbufferWidth = 0;
			UInt32 uBackbufferHeight = 0;
		};

		//Client Pos
		virtual Float2				ScreenToWindow(const Float2& ScreenPos) const = 0;

		virtual Void				Resize(UInt32 uWidth, UInt32 uHeight) = 0;
		virtual Void				Maximize() = 0;
		virtual Void				Minimize() = 0;

		virtual const UInt32		GetWidth() const = 0;
		virtual const UInt32		GetHeight() const = 0;
		virtual Boolean				IsActived() const = 0;
		virtual Void				Show(Boolean bShow) = 0;
		virtual Void				Present() = 0;
	};

	class DLL_DELCARE IGViewFrustum : public IGResource
	{
	public:
		struct Description
		{
			IGRenderWindow* pTargetWindow = nullptr;//Render Window Or Null
		};

		virtual Void SetPosition(const Float3& f3Position) = 0;
		virtual Void SetForwardAndUp(const Float3& f3Forward, const Float3& f3Up) = 0;

		virtual Void SetPerspective(Float1 fFovW, Float1 fAspectWH, Float1 fZNear, Float1 fZFar) = 0;
		virtual Void SetOrthographic(Float1 fWidth, Float1 fHeight, Float1 fZNear, Float1 fZFar) = 0;

		virtual Matrix4 GetWorldToView() const = 0;
		virtual Matrix4 GetViewToClip() const = 0;
		virtual Float2	GetViewportSize() const = 0;

		virtual Void	GetCornerPositions(Float3 nearCorners[4], Float3 farCorners[4]) const = 0;

		//virtual Void	GetVisibleDrawCall() const = 0;
	};
	/*
	class DLL_DELCARE IGViewPanoramicFrustum : public IGViewFrustum
	{
	public:
		virtual Void SetPosition(const Float3& f3Position) = 0;
		virtual Void SetForwardAndUp(const Float3& f3Forward, const Float3& f3Up) override {};

		virtual Void SetPerspective(Float1 fFovW, Float1 fAspectWH, Float1 fZNear, Float1 fZFar) = 0;
		virtual Void SetOrthographic(Float1 fWidth, Float1 fHeight, Float1 fZNear, Float1 fZFar) = 0;

		virtual Matrix4 GetWorldToView() const = 0;
		virtual Matrix4 GetViewToClip() const = 0;
		virtual Float2	GetViewportSize() const = 0;
	};
	*/
	class DLL_DELCARE IGMaterialParam
	{
	public:
		enum MaterialParamType
		{
			//Only 0~1
			EMPT_Float1 = 0x01,
			EMPT_Float2 = 0x02,
			EMPT_Float3 = 0x04,
			EMPT_Float4 = 0x08,
			EMPT_Float_Mask = 0x0f,

			EMPT_Texture1D = 0x10,
			EMPT_Texture2D = 0x20,
			EMPT_Texture3D = 0x40,
			EMPT_TextureCube = 0x80,
			EMPT_Texture_Mask = 0xf0,

			EMPT_Count,
			EMPT_Invalid = EMPT_Count,
		};

		struct Description
		{
			MaterialParamType eType;
			String strName;

			Float4 fDefault;

			String strDefault;
			TextureAddressMode eAddressMode;
			TextureFilterMode eFilterMode;

			Description()
			{
				eType = EMPT_Invalid;
				fDefault = Float4();
			}
			Description(MaterialParamType e, const Char* s, const Float4& fv)
			{
				eType = e;
				strName = s;
				fDefault = fv;
			}
			Description(MaterialParamType e, const Char* s, const Char* sv,
				TextureAddressMode ea = ETexAM_Repeat, TextureFilterMode ef = ETexFM_Bilinear)
			{
				eType = e;
				strName = s;
				strDefault = sv;
				eAddressMode = ea;
				eFilterMode = ef;
			}
		};

		virtual const Char*	GetName() const = 0;
		virtual MaterialParamType GetType() const = 0;

		virtual Float1 GetFloat1() const = 0;
		virtual Float2 GetFloat2() const = 0;
		virtual Float3 GetFloat3() const = 0;
		virtual Float4 GetFloat4() const = 0;

		virtual IGTexture* GetTexture1D() const = 0;
		virtual IGTexture* GetTexture2D() const = 0;
		virtual IGTexture* GetTexture3D() const = 0;
		virtual IGTexture* GetTextureCube() const = 0;

		virtual Boolean SetFloat1(Float1 f1Value) = 0;
		virtual Boolean SetFloat2(const Float2& f2Value) = 0;
		virtual Boolean SetFloat3(const Float3& f3Value) = 0;
		virtual Boolean SetFloat4(const Float4& f4Value) = 0;

		virtual Boolean SetTexture1D(IGTexture* pTexture) = 0;
		virtual Boolean SetTexture2D(IGTexture* pTexture) = 0;
		virtual Boolean SetTexture3D(IGTexture* pTexture) = 0;
		virtual Boolean SetTextureCube(IGTexture* pTexture) = 0;

		virtual Void	Copy(IGMaterialParam* pOther) = 0;
	};

	enum class MaterialDomain : UInt8
	{
		EWorldSpace = 0,
		EDecal,
		EScreenSpace,
		EPostProcess,
		ECount,
		EInvalid = ECount
	};

	enum class MaterialShadingModel : UInt8
	{
		ENolighting = 0,
		EDefault,
		EPostprocessFur,//Custom XYZ For FurDirection In WorldSpace  W For FurLength
		ECount,
		EInvalid = ECount
	};

	enum class MaterialBlendMode : UInt8
	{
		EOpaque = 0,
		EMasked,
		ETranslucency,
		EAdditive,
		EMaskTranslucency,
		ECount,
		EInvalid = ECount
	};

	enum class MaterialFaceType : UInt8
	{
		EFrontface = 0,
		EBackface,
		ETwoSide,
		EWireframe,
		ECount,
		EInvalid = ECount
	};

	enum class MaterialFunctionType : UInt8
	{
		ECustom = 0,
		ESurfaceParams,
		EMeshParams,
		ECount,
		EInvalid = ECount
	};

	class DLL_DELCARE IGMaterial : public IGResource
	{
	public:
		struct Description
		{
			String strUniqueName;
			Boolean bCanCache;
			MaterialDomain eDomain;
			MaterialFaceType eFaceType;
			MaterialShadingModel eShadingModel;
			MaterialBlendMode eBlendMode;
			//User Params
			Array<IGMaterialParam::Description> arrMaterialParams;
			String arrMaterialFunction[(const UInt32)MaterialFunctionType::ECount];

			Description()
				: eDomain(MaterialDomain::EInvalid)
				, eFaceType(MaterialFaceType::EInvalid)
				, eShadingModel(MaterialShadingModel::EInvalid)
				, eBlendMode(MaterialBlendMode::EInvalid)
			{

			}
			Description(const Description& other)
			{
				strUniqueName = other.strUniqueName;
				eDomain = other.eDomain;
				eFaceType = other.eFaceType;
				eShadingModel = other.eShadingModel;
				eBlendMode = other.eBlendMode;
				//User Params
				arrMaterialParams = other.arrMaterialParams;
				for (UInt32 u = 0; u < (const UInt32)MaterialFunctionType::ECount; u++)
				{
					arrMaterialFunction[u] = other.arrMaterialFunction[u];
				}
			}
			const Description& operator=(const Description& other)
			{
				strUniqueName = other.strUniqueName;
				eDomain = other.eDomain;
				eFaceType = other.eFaceType;
				eShadingModel = other.eShadingModel;
				eBlendMode = other.eBlendMode;
				//User Params
				arrMaterialParams = other.arrMaterialParams;
				for (UInt32 u = 0; u < (const UInt32)MaterialFunctionType::ECount; u++)
				{
					arrMaterialFunction[u] = other.arrMaterialFunction[u];
				}
			}
		};

		//以后可能会移除这几个get接口
		virtual MaterialDomain			GetDomainType() const = 0;
		virtual MaterialShadingModel	GetShadingModel() const = 0;
		virtual MaterialFaceType		GetFaceType() const = 0;
		virtual MaterialBlendMode		GetBlendMode() const = 0;

		virtual UInt32			GetParameterCount() const = 0;
	};

	class DLL_DELCARE IGMaterialParameterBuffer : public IGResource
	{
	public:
		virtual IGMaterialParam*	GetParamByIndex(UInt32 uIndex) const = 0;
		virtual IGMaterialParam*	GetParamByName(const Char* szName) const = 0;
	};
	class DLL_DELCARE IGPrimitiveTransformBuffer : public IGResource
	{
	public:
		virtual Void	SetTransforms(const Matrix4* pTransforms, UInt32 uTransforms) = 0;
		virtual Boolean	IsInstance() const = 0;
		virtual UInt32	GetTransformsCapacity() const = 0;
	};
	class DLL_DELCARE IGPrimitiveVertexBuffer : public IGResource
	{
	public:
		enum Type : UInt8
		{
			EStatic2D,
			EAnimate2D,
			EStatic3D,
			EAnimate3D,
			ECount,
			EInvalid = ECount,
		};
		virtual Type	GetType() const = 0;
		virtual UInt32	GetCapacity() const = 0;
		virtual Void	Update(const Void* pData, UInt32 uSize) = 0;
	};
	class DLL_DELCARE IGPrimitiveIndexBuffer : public IGResource
	{
	public:
		enum Type : UInt8
		{
			//EUInt8,
			EUInt16,
			EUInt32,
			ECount,
			EInvalid = ECount,
		};
		virtual Type	GetType() const = 0;
		virtual UInt32	GetCapacity() const = 0;
		virtual Void	Update(const Void* pData, UInt32 uSize) = 0;
	};

	struct IGVertex
	{

	};
	struct GVertexRigid2D : public IGVertex
	{
		Float2 f2Position;
		Float4 f4Color;
		Float2 f2TexCoord0;
	};
	struct GVertexRigid : public IGVertex
	{
		Float4 f4Position;//第四位先占位，可能以后有用
		Float4 f4Normal;
		Float4 f4Tangent;
		Float4 f4Color;
		Float2 f2TexCoord0;
	};
	struct GVertexSkin : public IGVertex
	{
		Float4 f4Position;
		Float4 f4Normal;
		Float4 f4Tangent;
		Float4 f4Color;
		Float2 f2TexCoord0;
		UInt32 uRefBoneIndex[4];
		Float1 fRefBoneWeight[4];
	};

	class IGPrimitive : public IGResource
	{
	public:
		enum Type
		{
			EPrimT_Line,
			EPrimT_Rigid,
			EPrimT_Skin,
			EPrimT_Rigid2D,
			//EPrimT_Skin2D,
			EPrimT_Invalid
		};

		struct Description
		{
			IGPrimitive::Type eType = IGPrimitive::EPrimT_Invalid;
			Boolean bDynamic = False;
			UInt32 uVertexCount = 0;
			UInt32 uIndexCount = 0;
		};

		virtual Type GetType() = 0;

		virtual Void UpdateVertexBuffer(const IGVertex* pVertex, UInt32 uVertexCount) = 0;
		virtual Void UpdateIndexBuffer(const UInt32* pIndex, UInt32 uIndexCount) = 0;

		virtual Boolean	CalIntersect(const Float3& f3Begin, const Float3& f3Direction, Float1 fDistance, const Matrix4* pTransform = nullptr, Array<Float3>* pOutIntersectPoint = nullptr) const = 0;

		virtual UInt32 GetVertexCapacity() const = 0;
		virtual UInt32 GetIndexCapacity() const = 0;
	};

	struct DLL_DELCARE GraphicsResource
	{
		static IGRenderWindow*				CreateRenderWindow(const IGRenderWindow::Description& desc);
		static IGTexture*					CreateTexture(const IGTexture::Description& desc);
		static IGMaterial*					CreateMaterial(const IGMaterial::Description& desc, Boolean bReCreate = False);
		static IGMaterialParameterBuffer*	CreateMaterialParameterBuffer(IGMaterial* pMaterial);
		static IGPrimitiveTransformBuffer*	CreatePrimitiveTransformBuffer(Boolean bForInstance);
		static IGPrimitiveVertexBuffer*		CreatePrimitiveVertexBuffer(IGPrimitiveVertexBuffer::Type eType, UInt32 uCapacity);
		static IGPrimitiveIndexBuffer*		CreatePrimitiveIndexBuffer(IGPrimitiveIndexBuffer::Type eType, UInt32 uCapacity);
		static IGViewFrustum*				CreateViewFrustum(const IGViewFrustum::Description& desc);


		static IGPrimitive*					CreatePrimitive(const IGPrimitive::Description& desc);
	};

	class DLL_DELCARE IGBoundingVolume
	{
	public:
		virtual Void	Merge(const IGBoundingVolume* pOther) = 0;
		virtual Void	Reset() = 0;
		virtual Void	AddVertexPosition(const Float3& f3Point) = 0;

		virtual Boolean	CalIntersect(const Float3& f3Begin, const Float3& f3Direction, Float1 fDistance, const Matrix4* pTransform = nullptr, Array<Float3>* pOutIntersectPoint = nullptr) const = 0;
		virtual UInt32  GetVolumePointCount() const = 0;
		virtual Float3  GetVolumePoint(UInt32 uIndex) const = 0;
	};

	class DLL_DELCARE IGLight
	{
	public:
		//virtual IGMaterialInstance* GetMaterialInstance() const = 0;
		virtual Float3	GetColor() const = 0;
		virtual Float1	GetCandela() const = 0;

		virtual Boolean IsCastShadow() const = 0;
		virtual Boolean IsAtmosphericScatter() const = 0;
	};

	class DLL_DELCARE IGDirectionLight : public IGLight
	{
	public:
		virtual Float3	GetLightDirection() const = 0;
	};

	class DLL_DELCARE IGPointLight : public IGLight
	{
	public:
		virtual Float3	GetLightPosition() const = 0;
		virtual Float1	GetLightRange() const = 0;
		virtual Float1	GetLightAttenuation() const = 0;
	};

	class DLL_DELCARE IGSpotLight : public IGLight
	{
	public:
		virtual Float3	GetLightPosition() const = 0;
		virtual Float3	GetLightDirection() const = 0;
		virtual Float1	GetLightRange() const = 0;
		virtual Float1	GetLightSpotAngle() const = 0;//In Radius
		virtual Float1	GetLightAttenuation() const = 0;
	};

	class DLL_DELCARE IGEnvironmentProbe
	{
	public:
		virtual Boolean	GetRealTime() = 0;
		virtual Float3	GetProbePosition() = 0;
		virtual Float1	GetProbeRange() = 0;
	};

	class DLL_DELCARE IGReflectionProbe : public IGEnvironmentProbe
	{
	public:
		virtual IGTexture* GetReflectionTexture() = 0;
	};

}
#endif