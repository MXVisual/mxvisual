#pragma once
#ifndef _MX_GRAPHICS_RENDERER_
#define _MX_GRAPHICS_RENDERER_
#include "MXPlatform.h"
#include "MXGlobalVariable.h"

#if defined(USE_DLL_GRAPHICS)
#define DLL_DELCARE __declspec(dllimport)
#elif defined(DLL_GRAPHICS)
#define DLL_DELCARE __declspec(dllexport)
#else 
#define DLL_DELCARE
#endif

//Dynamic Or Static
namespace MXVisual
{
#if MX_BUILD_EDITOR
	DLL_DELCARE extern  GlobalVariable	GVarShadingDebugView;
#endif
	DLL_DELCARE extern  GlobalVariable	GVarEnableVSync;

	DLL_DELCARE extern  GlobalVariable	GVarEnableShadow;
	DLL_DELCARE extern  GlobalVariable	GVarCascadeShadowDebug;
	DLL_DELCARE extern  GlobalVariable  GVarCascadeShadowDistance;
	DLL_DELCARE extern  GlobalVariable	GVarShadowDepthOffset;
	DLL_DELCARE extern  GlobalVariable	GVarShadowMapResolution;
	DLL_DELCARE extern  GlobalVariable	GVarShadowCascadeNum;

	DLL_DELCARE extern  GlobalVariable	GVarMaxAnisotropy;
	DLL_DELCARE extern  GlobalVariable	GVarAmbientColor;
	DLL_DELCARE extern  GlobalVariable	GVarAmbientScale;

	DLL_DELCARE extern  GlobalVariable	GVarEnableSSR;
	DLL_DELCARE extern  GlobalVariable	GVarEnableSSAO;
	DLL_DELCARE extern  GlobalVariable	GVarSSAORange;

	DLL_DELCARE extern  GlobalVariable	GVarEnableBloom;
	DLL_DELCARE extern	GlobalVariable	GVarBloomExporedLuminance;

	DLL_DELCARE extern	GlobalVariable	GVarEnableRBGodRay;
	DLL_DELCARE extern	GlobalVariable	GVarRBGodRaySampleCount;
	DLL_DELCARE extern	GlobalVariable	GVarRBGodRayDepthBegin;
	DLL_DELCARE extern	GlobalVariable	GVarRBGodRayAttenuation;
	DLL_DELCARE extern	GlobalVariable	GVarRBGodRayStrength;
	DLL_DELCARE extern	GlobalVariable	GVarRBGodRayRange;

	DLL_DELCARE extern  GlobalVariable	GVarEnableToneMapping;

	DLL_DELCARE extern  GlobalVariable	GVarMaxGPUBlendMatrixPerPass;

	DLL_DELCARE extern  GlobalVariable	GVarHeightFogEnable;
	DLL_DELCARE extern  GlobalVariable	GVarHeightFogColor;
	DLL_DELCARE extern  GlobalVariable	GVarHeightFogDensity;
	DLL_DELCARE extern  GlobalVariable	GVarHeightFogStartDistance;
	DLL_DELCARE extern  GlobalVariable	GVarHeightFogDistanceFalloff;
	DLL_DELCARE extern  GlobalVariable	GVarHeightFogHeight;
	DLL_DELCARE extern  GlobalVariable	GVarHeightFogHeightFalloff;

	DLL_DELCARE extern  GlobalVariable	GVarEnableTemporalAA;

	class IGLight;
	class IGEnvironmentProbe;
	class IGPrimitive;
	class IGMaterial;
	class IGMaterialParameterBuffer;
	class IGPrimitiveTransformBuffer;
	class IGMaterialInstance;
	class IGViewFrustum;

	struct GStatistics
	{
		UInt32 uGPUMemoryOccupy;//In UInt8
		UInt32 uMemoryOccupy;//In UInt8
		UInt32 uPrimitiveCount;
		UInt32 uDrawCall;
	};

	struct GRendererConfigure
	{
		Float1	fResolutionRatio = 1.0f;//0.5f ~ 2.0f
		Boolean	bRenderUI = True;
		Boolean bAutoExposure = True;
	};

	enum GRendererPostProcessInsertionPoint
	{
		EPPIP_AfterOpaque,
		EPPIP_AfterTranslucency,
		EPPIP_BeforeToneMap,
		EPPIP_Count,
		EPPIP_Invalid = EPPIP_Count,
	};

	struct DLL_DELCARE GraphicsRenderer
	{
		struct InitParam
		{
			const Char* szAdapterName = nullptr;
			Boolean bMultiThread = False;
			Boolean bReverseZ = False;
			UInt32	uBackBufferWidth;
			UInt32	uBackBufferHeight;
		};
		static Boolean	Init(InitParam& param);
		static Void		Release();

		static Void	RegisterPostProcess(
			IGMaterial* pMaterial,
			IGMaterialParameterBuffer* pParameterBlock,
			GRendererPostProcessInsertionPoint eIPoint);

		static Void	RegisterLight(IGLight* pLight);
		static Void	RegisterEnvironmentProbe(IGEnvironmentProbe* pProbe);

		//For Skin And UI Instance Alway Off
		static Void	RegisterPrimitive(
			IGPrimitive* pPrimitive,
			UInt32 uVertexOffset,
			UInt32 uVertexCount,
			IGMaterial* pMaterial,
			IGMaterialParameterBuffer* pParameterBlock,
			IGPrimitiveTransformBuffer* pTransformBlock = nullptr,
			Region regionClip = Region()
		);
		static Void	Render(IGViewFrustum* pFrustum, GRendererConfigure& configure);

		static Void GetStatistics(GStatistics* pStatis);
	};
}

#endif