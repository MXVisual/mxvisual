#include "MXVPixelShaderCommon.msh"
#include "MXVMaterialCommon.msh"

Tex2D(SceneDepth);
Tex2D(SceneNormal_ShadingModel);
Tex2D(RandomVector);

Sampler(sampler_0);
#define s_SceneDepth sampler_0
#define s_SceneNormal_ShadingModel sampler_0
#define s_RandomVector sampler_0

#ifndef AO_SAMPLE_COUNT 
#error "Must Define AO_SAMPLE_COUNT"
#endif

#ifndef AO_NOISE_WIDTH_COUNT 
#error "Must Define AO_NOISE_WIDTH_COUNT"
#endif

#ifndef AO_NOISE_HEIGHT_COUNT 
#error "Must Define AO_NOISE_WIDTH_COUNT"
#endif

CONSTANT_BUFFER_BEGIN(SSAO, 1)
float SampleCoreRange;
float3 dump;
float4	SampleDir[AO_SAMPLE_COUNT];
float4	NoiseDir[AO_NOISE_WIDTH_COUNT * AO_NOISE_HEIGHT_COUNT];
CONSTANT_BUFFER_END

float4 PSMain(PixelInput input) : SV_TARGET0
{
	float4 output = 1.0f;
	float2 TargetSamplePos = PCFunc_Coord_ScreenUVToGBufferUV(input.TexCoord0);

	float4 SurfaceNormal_LightModel = Tex2D_SampleLevel(SceneNormal_ShadingModel, TargetSamplePos, 0);
	float  SurfaceNDCDepth = Tex2D_SampleLevel(SceneDepth, TargetSamplePos, 0).r;

	float  ViewDepth = PCFunc_Depth_NDCToCurrentView(SurfaceNDCDepth);
	float2 NDCPos = PCFunc_Coord_UVToNDC(input.TexCoord0);
	float3 SurfacePosition = PCFunc_Coord_ViewToWorld(PCFunc_Coord_NDCToView(NDCPos, ViewDepth));
	uint   SurfaceShadingModel = DecodeShadingModelID(SurfaceNormal_LightModel.a);
	float3 SurfaceNormal = normalize(SurfaceNormal_LightModel.xyz * 2.0f - 1.0f);

	if (SurfaceShadingModel != SHADINGMODEL_NOLIGHTING)
	{
		float3 t = 0.0f;
		t.xy = NoiseDir[input.ScreenPosition.x % AO_NOISE_WIDTH_COUNT + input.ScreenPosition.y % AO_NOISE_HEIGHT_COUNT * AO_NOISE_WIDTH_COUNT].xy;

		t = normalize(t - SurfaceNormal * dot(t, SurfaceNormal));
		float3 b = cross(SurfaceNormal, t);
		float3x3 tbn = float3x3(t, b, SurfaceNormal);

		float AO = 0.0f;
		[loop]
		for (uint i = 0; i < AO_SAMPLE_COUNT; i++)
		{
			float3 SamplePos = SampleCoreRange * mul(SampleDir[i].xyz, tbn) + SurfacePosition;

			float4 SampleUV = Multiply(float4(SamplePos, 1.0f), ENG_MatrixWorldToClip);
			SampleUV.xy /= SampleUV.w;
			SampleUV.xy = saturate(PCFunc_Coord_NDCToUV(SampleUV.xy));

			float CompDepth = PCFunc_Depth_NDCToCurrentView(Tex2D_SampleLevel(SceneDepth, PCFunc_Coord_ScreenUVToGBufferUV(SampleUV.xy), 0).r);

			float rangeCheck = lerp(1.0f, 0.0f, abs(CompDepth - ViewDepth) / SampleCoreRange);
			AO += (CompDepth > ViewDepth ? 0.0f : 1.0f) *rangeCheck;
		}

		output = 1.0f - AO / AO_SAMPLE_COUNT;
	}
	return output;
}