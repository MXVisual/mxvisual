#include "MXVPixelShaderCommon.msh"
#include "MXVMaterialCommon.msh"

Tex2D(SceneDepth);

Sampler(sampler_0);
#define s_SceneDepth sampler_0

#ifndef AO_SAMPLE_COUNT
#define AO_SAMPLE_COUNT 16
#endif

CONSTANT_BUFFER_BEGIN(HeightFogProperty, 1)
	float3 Color;
	float Density;

	float StartDistance;
	float DistanceFalloff;
	float Height;
	float HeightFalloff;
CONSTANT_BUFFER_END

float4 PSMain(PixelInput input) : SV_TARGET0
{
	float2 TargetSamplePos = PCFunc_Coord_ScreenUVToGBufferUV(input.TexCoord0);

	float  SurfaceNDCDepth = Tex2D_SampleLevel(SceneDepth, TargetSamplePos, 0).r;
	float  ViewDepth = PCFunc_Depth_NDCToCurrentView(SurfaceNDCDepth);
	float2 NDCPos	= PCFunc_Coord_UVToNDC(TargetSamplePos);
	float3 SurfacePosition = PCFunc_Coord_NDCToView(NDCPos, ViewDepth);

	float ViewDistance = SurfacePosition.z;

	SurfacePosition = PCFunc_Coord_ViewToWorld(SurfacePosition);

	float FogDensity = Density * exp(HeightFalloff * (Height - SurfacePosition.y));

	float Falloff = HeightFalloff * abs(ENGINE_CAMERA_POSITION.y - SurfacePosition.y);
	float FogFactor = (1.0f - exp2(-Falloff)) / Falloff;
	float FogLine = max(ViewDistance - StartDistance, 0.0f) / DistanceFalloff / (ENGINE_CAMERA_FARPLANE - StartDistance);

	return float4(Color, saturate(FogDensity * FogFactor * FogLine));
}