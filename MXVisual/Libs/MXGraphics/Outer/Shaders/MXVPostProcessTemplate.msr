#include "MXVPixelShaderCommon.msh"
#include "MXVMaterialCommon.msh"

#if !defined(DOMAIN) || DOMAIN != DOMAIN_POSTPROCESS
#error "Must Define DOMAIN: DOMAIN_POSTPROCESS"
#endif

//Macros
%s

struct PixelParams
{
	float3 SceneColor;
	float  SceneDepth;
	float  SceneLightIntensity;
	float  SceneRoughness;
	float  SceneMetallic;
	float3 WorldPosition;
	float3 WorldNormal;
};

Tex2D(ENGINE_SceneColor);
Tex2D(ENGINE_SceneRM_AO_X);
Tex2D(ENGINE_SceneNormal_LightID);
Tex2D(ENGINE_SceneDepth);

//Function
//register(b1) for User PostProcess Param
%s

PixelParams CalGBuffer(float2 uv)
{
	PixelParams output;
	float4 scenecolor = Tex2D_Sample(ENGINE_SceneColor, uv);
	float4 rm_ao_x = Tex2D_Sample(ENGINE_SceneRM_AO_X, uv);
	float4 scenenormal = Tex2D_Sample(ENGINE_SceneNormal_LightID, uv);
	float scenedepth = Tex2D_Sample(ENGINE_SceneDepth, uv).r;

	output.SceneColor = scenecolor.rgb;
	output.SceneDepth = scenedepth;
	output.SceneLightIntensity = rm_ao_x.b;
	output.SceneRoughness = rm_ao_x.r;
	output.SceneMetallic = rm_ao_x.g;
	output.WorldPosition = 0;//sceneworldpos_lightID.rgb;
	output.WorldNormal = scenenormal.rgb;
	return output;
}

RWTexture2d<float4> Output : register(u0);

[numthreads(1, 1, 1)]
void CSMain(uint3 ThreadID : SV_DispatchThreadID)
{
	float2 TargetSamplePos = PCFunc_Coord_ScreenUVToGBufferUV((PCFunc_Coord_ScreenToUV(ThreadID.xy));

	PixelParams userparam = CalGBuffer(TargetSamplePos);

	SurfaceParams material = GetDefaultSurfaceParams(normalize(input.WorldNormal));

#if defined(CALLBACK_SURFACEPARAMS)
	CBSurfaceParams(userparam, material);
#endif

	Output[ThreadID.xy] = float4(material.Emissive, 1);
}

