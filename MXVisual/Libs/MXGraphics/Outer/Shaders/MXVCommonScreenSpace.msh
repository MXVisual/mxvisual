#include "MXVPixelShaderCommon.msh"

float3 SSFunc_RayMatching_BySceneDepth(
	Texture2D texDepth,
#if SHADER_PLATFORM == SHADER_PLATFORM_HLSL
	SamplerState s_texDepth,
#elif SHADER_PLATFORM == SHADER_PLATFORM_GLSL
#error "SHADER_PLATFORM_GLSL SSFunc_RaymatchingByDepth TODO!!"
#endif
	float3 RayOrigin,
	float3 RayDirection,
	uint SampleCount)
{
	float4 RayStart = Multiply(float4(RayOrigin, 1.0f), ENG_MatrixWorldToClip);

	float4 RayEnd = Multiply(float4(RayOrigin + normalize(RayDirection) * ENGINE_CAMERA_FARPLANE, 1.0f), ENG_MatrixWorldToClip);

	//if RayDir Go Inside Screen, DepthCompare Func Is Greater
	//else Is Less
	float CompareInverse = sign(RayEnd.w - RayStart.w);
	if (any(abs(RayEnd.xy / RayEnd.w)) > 1.0f)
	{
		const float SampleDist = 1.1f;
		float t = 0.0f;//Make x/w or y/w = 1.1
		float4 SampleDir = RayEnd - RayStart;
		if (abs(SampleDir.x) > abs(SampleDir.y))
		{
			float s = sign(SampleDir.x) * SampleDist;
			t = (s * RayStart.w - RayStart.x) / (SampleDir.x - SampleDir.w);
		}
		else
		{
			float s = sign(SampleDir.y) * SampleDist;
			t = (s * RayStart.w - RayStart.y) / (SampleDir.y - s * SampleDir.w);
		}
		RayEnd = RayStart + SampleDir * t;
	}
	RayEnd.xyz /= RayEnd.w;
	RayStart.xyz /= RayStart.w;

	float4 RayStep = RayEnd - RayStart;
	//RayStep Should GreaterEqual To A PixelSize
	if (all(RayStep.xy < ENGINE_PIXELSIZE))
	{
		SampleCount = 0;
	}
	else
	{
		RayStep /= SampleCount;
	}

	float3 CurrentStep = RayStart.xyz + RayStep.xyz;
	float CurrentDepth = 0.0f;
	int nFound = 0;
	int nCurrentFound = 0;

	float3 IntersectionPoint = 0.0f;
	float IntersectionDepth = 0.0f;

	[loop]
	for (uint i = 0; i < SampleCount; i++)
	{
		float2 CurrentUV = saturate(PCFunc_Coord_NDCToUV(CurrentStep.xy));
		float Depth = Tex2D_SampleLevel(texDepth, PCFunc_Coord_ScreenUVToGBufferUV(CurrentUV), 0).r;

		float Dis = PCFunc_Depth_NDCToCurrentView(CurrentStep.z) - PCFunc_Depth_NDCToCurrentView(Depth) * CompareInverse;
		nCurrentFound =
			((Dis <= RayStep.w) && (Dis >= 0.0f)) ? 1 : 0;
		

		bool bUpdate = (nCurrentFound == 1) && (nFound == 0);
		IntersectionDepth = bUpdate ? Depth : IntersectionDepth;
		IntersectionPoint = bUpdate ? CurrentStep : IntersectionPoint;

		nFound += nCurrentFound;

		CurrentStep += RayStep.xyz;
	}

	float2 Result = 0.0f;
	float Rate = 0.0f;
	if (nFound > 0)
	{
		Result = lerp(IntersectionPoint.xy, IntersectionPoint.xy - RayStep.xy,
			(IntersectionPoint.z - IntersectionDepth) / RayStep.z);
		Result = saturate(PCFunc_Coord_NDCToUV(Result));
		
		Rate = max(abs(Result.x - 0.5f), abs(Result.y - 0.5f)) * 2.0f;
		Rate = saturate(-6.0f * Rate + 5.0f);
	}
	return float3(Result, Rate);
}