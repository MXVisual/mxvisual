#ifndef _MXV_SHADER_PLATFORM_
#define _MXV_SHADER_PLATFORM_

#ifndef SHADER_PLATFORM
#error "must define SHADER_PLATFORM"
#endif

#define SHADER_PLATFORM_HLSL 0
#define SHADER_PLATFORM_GLSL 1

#ifndef SHADER_TYPE
#error "must define SHADER_TYPE"
#endif

#define SHADER_TYPE_VERTEX	0
#define SHADER_TYPE_PIXEL	1
#define SHADER_TYPE_COMPUTE	2

#define CONSTANT_PI 3.14159265359f
#define CONSTANT_FLOAT_EPSILON 0.0000001f

#if SHADER_PLATFORM == SHADER_PLATFORM_HLSL
//HLSL
#define Sampler(name) SamplerState name;

#define Tex1D(name) Texture1D name;
#define Tex2D(name) Texture2D name;
#define Tex3D(name) Texture3D name;
#define TexCube(name) TextureCube name;

#define CONSTANT_BUFFER_BEGIN(name, slot) cbuffer name : register(b##slot) {
#define CONSTANT_BUFFER_END	};

float3 Multiply(float3 value, float3x3 mat) { return mul(value, mat); }
float4 Multiply(float4 value, float4x4 mat){ return mul(value, mat); }
float4x4 Multiply(float4x4 mata, float4x4 matb){ return mul(mata, matb); }

#define Tex2D_SampleLevel(tex, uv, lod) (tex.SampleLevel(s_##tex, uv, lod))
#define Tex3D_SampleLevel(tex, uvw, lod) (tex.SampleLevel(s_##tex, uvw, lod))
#define TexCube_SampleLevel(tex, uvw, lod) (tex.SampleLevel(s_##tex, uvw, lod))

#if SHADER_TYPE == SHADER_TYPE_VERTEX
#define Tex2D_Sample(tex, uv)		Tex2D_SampleLevel(tex, uv, 0)
#define Tex3D_Sample(tex, uvw)		Tex3D_SampleLevel(tex, uv, 0)
#define TexCube_Sample(tex, uvw)	TexCube_SampleLevel(tex, uv, 0)
#else
#define Tex2D_Sample(tex, uv)		(tex.Sample(s_##tex, uv))
#define Tex3D_Sample(tex, uvw)		(tex.Sample(s_##tex, uvw))
#define TexCube_Sample(tex, uvw)	(tex.Sample(s_##tex, uvw))
#endif

#define CalculateLod(tex, lodRate) (tex.CalculateLevelOfDetail(s_##tex, lodRate))
#elif SHADER_PLATFORM == SHADER_PLATFORM_GLSL
//GLSL
#if defined(VALUE_PRECISION_HIGH)
precision highp float;
#elif defined(VALUE_PRECISION_MIDDLE)
precision mediump float;
#elif defined(VALUE_PRECISION_LOW)
precision lowp float;
#else
precision highp float;
#endif

#define float2 vec2
#define float3 vec3
#define float4 vec4

#define int2 ivec2
#define int3 ivec3
#define int4 ivec4

#define bool2 bvec2
#define bool3 bvec3
#define bool4 bvec4

#define float2x2 mat2
#define float3x3 mat3
#define float4x4 mat4

#define Texture1D sampler2D
#define Texture2D sampler2D
#define Texture3D sampler3D
#define TextureCube samplerCube

#define Tex1D(name) uniform sampler2D name;
#define Tex2D(name) uniform sampler2D name;
#define Tex3D(name) uniform sampler3D name;
#define TexCube(name) uniform samplerCube name;

#define CONSTANT_BUFFER_BEGIN(name, slot) layout(std140, binding = slot) uniform name {
#define CONSTANT_BUFFER_END	};

float3 Multiply(float3 value, float3x3 mat) { return mat * value; }
float4 Multiply(float4 value, float4x4 mat){ return mat * value; }
float4x4 Multiply(float4x4 matrixa, float4x4 matrixb){ return matrixb * matrixa; }

#define Tex2D_SampleLevel(tex, uv, lod) (textureLod(tex, uv, lod))
#define Tex3D_SampleLevel(tex, uvw, lod) (textureLod(tex, uvw, lod))
#define TexCube_SampleLevel(tex, uvw, lod) (textureLod(tex, uvw, lod))

#if SHADER_TYPE == SHADER_TYPE_VERTEX
#define Tex2D_Sample(tex, uv)		Tex2D_SampleLevel(tex, uv, 0)
#define Tex3D_Sample(tex, uvw)		Tex3D_SampleLevel(tex, uv, 0)
#define TexCube_Sample(tex, uvw)	TexCube_SampleLevel(tex, uv, 0)
#else
#define Tex2D_Sample(tex, uv)		(texture(tex, uv))
#define Tex3D_Sample(tex, uvw)		(texture(tex, uvw))
#define TexCube_Sample(tex, uvw)	(texture(tex, uvw))
#endif

#define lerp(a,b,t) mix(a,b,t)
#define saturate(v)	clamp(v, 0.0f, 1.0f)
#endif

#if SHADER_TYPE == SHADER_TYPE_PIXEL
	void AlphaTest(float alpha, float mask){ if((alpha - mask) < 0.0f) discard; }
#endif

#endif