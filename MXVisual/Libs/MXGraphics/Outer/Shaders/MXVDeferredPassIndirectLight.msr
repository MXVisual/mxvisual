#include "MXVPixelShaderCommon.msh"
#include "MXVMaterialCommon.msh"
#include "MXVCommonScreenSpace.msh"
#include "MXVCommonLight.msh"

Tex2D(ENGINE_SceneDepth);
Tex2D(ENGINE_SceneColor);
Tex2D(SceneAlbedo_Opacity);
Tex2D(SceneRM_AO_X);
Tex2D(SceneNormal_ShadingModel);

#ifndef MAX_ENV_REFLECTION
#define MAX_ENV_REFLECTION 4
#endif
TexCube(EnvReflection[MAX_ENV_REFLECTION]);

Sampler(sampler_0);
#define s_ENGINE_SceneDepth sampler_0
#define s_ENGINE_SceneColor sampler_0
#define s_SceneAlbedo_Opacity sampler_0
#define s_SceneRM_AO_X sampler_0
#define s_SceneNormal_ShadingModel sampler_0
#define s_EnvReflection sampler_0

CONSTANT_BUFFER_BEGIN(IndirectLightCommon, 1)
float4	IndirectLightColor_Intensity;
float	EnableSSR;
float2	Dump;
uint	EnvReflectionCount;
float4	EnvReflectionCenter_InvRadius2[MAX_ENV_REFLECTION];
CONSTANT_BUFFER_END

float4 PSMain(PixelInput input) : SV_TARGET0
{
	float4 output = float4(0,0,0,1);

	float2 TargetSamplePos = PCFunc_Coord_ScreenUVToGBufferUV(input.TexCoord0);
	float2 NDCPos = PCFunc_Coord_UVToNDC(input.TexCoord0);

	output.rgb = Tex2D_SampleLevel(ENGINE_SceneColor, TargetSamplePos, 0).rgb;

	float  NDCDepth = Tex2D_SampleLevel(ENGINE_SceneDepth, TargetSamplePos, 0).r;
	float  ViewDepth = PCFunc_Depth_NDCToCurrentView(NDCDepth);

	float4 SceneColor = Tex2D_SampleLevel(SceneAlbedo_Opacity, TargetSamplePos, 0);
	float4 Albedo = Tex2D_SampleLevel(SceneAlbedo_Opacity, TargetSamplePos, 0);
	float4 RM_AO_X = Tex2D_SampleLevel(SceneRM_AO_X, TargetSamplePos, 0);//r roughness g metallic
	float4 Normal = Tex2D_SampleLevel(SceneNormal_ShadingModel, TargetSamplePos, 0);

	float3 SurfacePosition = PCFunc_Coord_ViewToWorld(PCFunc_Coord_NDCToView(NDCPos, ViewDepth));
	float3 SurfaceAlbedo = Albedo.rgb;
	float  SurfaceRoughness = RM_AO_X.r;
	float  SurfaceMetallic = RM_AO_X.g;
	float  SurfaceAO = RM_AO_X.b;
	float3 SurfaceNormal = normalize(Normal.xyz * 2.0f - 1.0f);
	uint   SurfaceShadingModel = DecodeShadingModelID(Normal.a);
	float3 ViewDirection = normalize(ENGINE_CAMERA_POSITION - SurfacePosition);

	float3 ReflectionColor = 0.0f;

	if (SurfaceShadingModel != SHADINGMODEL_NOLIGHTING)
	{
		ReflectionColor = IndirectLightColor_Intensity.rgb * IndirectLightColor_Intensity.a;
		//Reflection
		float3 ReflectDir = reflect(ViewDirection * -1.0f, SurfaceNormal);

		uint SampleCount = min(EnvReflectionCount, MAX_ENV_REFLECTION);
		for (uint i = 0; i < SampleCount; i++)
		{
			float3 Dir = SurfacePosition - EnvReflectionCenter_InvRadius2[i].xyz;
			float  Distance2 = dot(Dir, Dir);

			float Rate = saturate(Distance2 * EnvReflectionCenter_InvRadius2[i].w);
			Rate = saturate(-6.0f * Rate + 5.0f);

			float3 uvw = ReflectDir;// normalize(Dir + ReflectDir);
			float Lod = EnvReflection[i].CalculateLevelOfDetail(s_EnvReflection, SurfaceRoughness);
			ReflectionColor = lerp(ReflectionColor, EnvReflection[i].SampleLevel(s_EnvReflection, uvw, Lod).rgb, Rate);
		}

		if (EnableSSR > 0.5f)
		{
			float3 ReflectionUVRate = SSFunc_RayMatching_BySceneDepth(
				ENGINE_SceneDepth,
	#if SHADER_PLATFORM == SHADER_PLATFORM_HLSL
					s_ENGINE_SceneDepth,
	#elif SHADER_PLATFORM == SHADER_PLATFORM_GLSL
	#error "SHADER_PLATFORM_GLSL SSR Function TODO"
	#endif
					SurfacePosition, ReflectDir, 32);

			float Lod = CalculateLod(ENGINE_SceneColor, SurfaceRoughness);
			ReflectionColor = lerp(ReflectionColor, Tex2D_SampleLevel(ENGINE_SceneColor, PCFunc_Coord_ScreenUVToGBufferUV(ReflectionUVRate.xy), Lod).rgb, ReflectionUVRate.z);
		}
		ReflectionColor = FLight_Ambient(
			SurfacePosition,
			SurfaceAlbedo,
			SurfaceRoughness,
			SurfaceMetallic,
			SurfaceNormal,
			0.02f,
			ReflectionColor,
			SurfaceAO,
			ViewDirection);		
	}
	
	output.rgb += max(ReflectionColor, 0.0f);
	return output;
}