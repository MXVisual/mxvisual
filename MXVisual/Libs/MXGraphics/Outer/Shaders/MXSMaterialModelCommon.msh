#ifndef _MXS_MATERIAL_MODEL_COMMON_
#define _MXS_MATERIAL_MODEL_COMMON_
#include "MXVShaderPlatform.msh"

#if DOMAIN != DOMAIN_WORLDSPACE && DOMAIN != DOMAIN_SCREENSPACE
#error "DOMAIN Must DOMAIN_WORLDSPACE or DOMAIN_SCREENSPACE"
#endif

#if SHADER_TYPE == SHADER_TYPE_VERTEX
	struct VertexOutput 
	{
		float4 SVPosition : SV_POSITION;
		float3 WorldPosition : POSITION;
		float4 VertexColor : COLOR0;
		float2 TexCoord0 : TEXCOORD0;
		float3 WorldNormal : NORMAL;
		float4 WorldTangent : TANGENT;
		uint   InstanceID : SV_InstanceID;
	};

	struct MeshParams
	{
		float3 Offset;
	};
	#define OUTPUT_WORLDPOSITION_OFFSET(f3)		output.Offset = (f3).xyz

#elif SHADER_TYPE == SHADER_TYPE_PIXEL || SHADER_TYPE == SHADER_TYPE_COMPUTE
#ifndef RENDERPASS
#error "Must Define RENDERPASS"
#endif

	struct PixelInput
	{
		float4 ScreenPosition : SV_POSITION;
		float3 WorldPosition : POSITION;
		float4 VertexColor : COLOR0;
		float2 TexCoord0 : TEXCOORD0;
		float3 WorldNormal : NORMAL;
		float4 WorldTangent : TANGENT;
		uint   InstanceID : SV_InstanceID;
	};

	struct PixelOutput
	{
#if RENDERPASS == RENDERPASS_DEFERRED_MATERIAL
		float4 SceneColor : SV_TARGET0;
		float4 GBuffer0 : SV_TARGET1;//rgb: albedo a:opacity
		float4 GBuffer1 : SV_TARGET2;//r:roughness g:metallic b:ao
		float4 GBuffer2 : SV_TARGET3;//rgb:worldNormal a:ShadingModel
		float4 GBuffer3 : SV_TARGET4;//customdata
#elif RENDERPASS == RENDERPASS_FORWARD_MATERIAL
		float4 SceneColor	: SV_TARGET0;
#if ENABLE_SCREENDISTORT
		float4 GBuffer0		: SV_TARGET1;//screen dir
#endif
#endif
		//float  Depth : SV_Depth;
	};

	struct SurfaceParams
	{
		float3	Emissive;
	#if BLENDMODE != BLENDMODE_OPAQUE
		float	DiscardFlag;
	#endif

		float	DepthOffset;
		float3	WorldNormal;

	#if BLENDMODE == BLENDMODE_TRANSLUCENCY || \
			BLENDMODE == BLENDMODE_ADDITIVE || \
			BLENDMODE == BLENDMODE_MASKTRANSLUCENCY
		float	Opacity;
	#endif

	#if RENDERPASS == RENDERPASS_FORWARD_MATERIAL
#if ENABLE_SCREENDISTORT
		float2	ScreenDistort;
#endif
	#endif

	#if SHADINGMODEL != SHADINGMODEL_NOLIGHTING
		float3	Albedo;
		float	Metallic;
		float	Roughness;
		float	AO;

		float4	CustomData;
	#endif
	};

	SurfaceParams GetDefaultSurfaceParams(in PixelInput input)
	{
		SurfaceParams material;

		material.Emissive = 0.0f;
	#if BLENDMODE != BLENDMODE_OPAQUE
		material.DiscardFlag = 1.0f;
	#endif
		
		material.DepthOffset = 0.0f;
		material.WorldNormal = normalize(input.WorldNormal);

	#if BLENDMODE == BLENDMODE_TRANSLUCENCY || \
			BLENDMODE == BLENDMODE_ADDITIVE || \
			BLENDMODE == BLENDMODE_MASKTRANSLUCENCY
		material.Opacity = 1.0f;
	#endif

	#if RENDERPASS == RENDERPASS_FORWARD_MATERIAL
#if ENABLE_SCREENDISTORT
		material.ScreenDistort = 0.0f;
#endif
	#endif

	#if SHADINGMODEL != SHADINGMODEL_NOLIGHTING
		material.Albedo = 0.0f;
		material.Metallic = 0.8f;
		material.Roughness = 0.5f;
		material.AO = 1.0f;
		material.CustomData = 0;
	#endif
		return material;
	}

	void NormalizeSurfaceParams(inout SurfaceParams material)
	{
		material.Emissive = max(material.Emissive, 0.0f);

		//material.DepthOffset
		material.WorldNormal = normalize(material.WorldNormal);

	#if BLENDMODE == BLENDMODE_TRANSLUCENCY ||\
			BLENDMODE == BLENDMODE_ADDITIVE	||\
			BLENDMODE == BLENDMODE_MASKTRANSLUCENCY
		material.Opacity = saturate(material.Opacity);
	#endif

	#if RENDERPASS == RENDERPASS_FORWARD_MATERIAL
#if ENABLE_SCREENDISTORT
		material.ScreenDistort = clamp(material.ScreenDistort, -1.0f, 1.0f);
#endif
	#endif

	#if SHADINGMODEL != SHADINGMODEL_NOLIGHTING
		material.Albedo = saturate(material.Albedo);
		material.Metallic = saturate(material.Metallic);
		material.Roughness = saturate(material.Roughness);
		material.AO = saturate(material.AO);
		//material.CustomData
	#endif

	#if BLENDMODE != BLENDMODE_OPAQUE
		material.DiscardFlag = material.DiscardFlag > 0 ? 1.0f : -1.0f;
	#endif
	}

	#define OUTPUT_EMISSIVE(f3)		output.Emissive = (f3).rgb
	#if BLENDMODE != BLENDMODE_OPAQUE
	#define OUTPUT_DISCARDFLAG(b)	output.DiscardFlag = (b).r
	#endif	

	#define OUTPUT_DEPTHOFFSET(f)	output.DepthOffset = (f).r
	#define OUTPUT_WORLD_NORMAL(f3)	output.WorldNormal = (f3).rgb
	#define OUTPUT_TANGENT_NORMAL(f3)	output.WorldNormal = Multiply((f3).rgb, INPUT_TANGENTTOWORLD)

	#if BLENDMODE == BLENDMODE_TRANSLUCENCY || BLENDMODE == BLENDMODE_ADDITIVE
	#define OUTPUT_OPACITY(f)		output.Opacity = (f).r
#if ENABLE_SCREENDISTORT
	#define OUTPUT_SCREEN_DISTORT(f2)	output.ScreenDistort = (f2).rg
#endif
	#elif BLENDMODE == BLENDMODE_MASKTRANSLUCENCY
	#define OUTPUT_OPACITY(f)		output.Opacity = (f).r
	#endif

	#if SHADINGMODEL != SHADINGMODEL_NOLIGHTING
	#define OUTPUT_ALBEDO(f3)		output.Albedo = (f3).rgb
	#define OUTPUT_METALLIC(f)		output.Metallic = (f).r
	#define OUTPUT_ROUGHNESS(f)		output.Roughness = (f).r
	#define OUTPUT_AO(f)			output.AO = (f).r
	#define OUTPUT_CUSTOMDATA(f4)	output.CustomData = (f4).rgba
	#endif

	PixelOutput EncodeOutput(in SurfaceParams material)
	{
		PixelOutput output;

#if RENDERPASS == RENDERPASS_DEFERRED_MATERIAL
		output.SceneColor.rgb = material.Emissive;
		output.SceneColor.a = 1.0f;

#if SHADINGMODEL != SHADINGMODEL_NOLIGHTING
		output.SceneColor.a = 1.0f;

		output.GBuffer0.rgb = material.Albedo;
		output.GBuffer0.a = 1.0f;

		output.GBuffer1.r = material.Roughness;
		output.GBuffer1.g = material.Metallic;
		output.GBuffer1.b = material.AO;
		output.GBuffer1.a = 1.0f;
		output.GBuffer2.rgb = material.WorldNormal * 0.5f + 0.5f;
		output.GBuffer2.a = (SHADINGMODEL & ((1 << SHADINGMODEL_MAX) - 1)) / 255.0f;
#endif
		
#if SHADINGMODEL == SHADINGMODEL_POSTPROCESSFUR
		output.GBuffer3.xy = material.CustomData.xy;
#else
		output.GBuffer3 = 0;
#endif

#elif RENDERPASS == RENDERPASS_FORWARD_MATERIAL
		output.SceneColor.rgb = material.Emissive;
		output.SceneColor.a = material.Opacity;

#if ENABLE_SCREENDISTORT
		output.GBuffer0.rg = material.ScreenDistort * 0.5f + 0.5f;
		output.GBuffer0.b = 0.0f;
		output.GBuffer0.a = material.Opacity;
#endif
#endif
		return output;
	}

#endif
#endif