#define PRIMITIVE_TYPE 0
#define INSTANCE_RENDER 0

#include "MXVVertexShaderCommon.msh"
#include "MXVMaterialCommon.msh"

#ifndef SIMPLE_MESH_TYPE
#error "must define SIMPLE_MESH_TYPE"
#endif

#define SIMPLE_MESH_SCREENQUAD	0
#define SIMPLE_MESH_AACUBE		1

CONSTANT_BUFFER_BEGIN(TransformBuffer, 1)
	float4x4 LocalToWorld;
	float4x4 WorldToLocal;
CONSTANT_BUFFER_END

VertexOutput VSMain(uint InstanceID : SV_InstanceID, uint VertexID : SV_VertexID)
{
	VertexOutput o;

#if SIMPLE_MESH_TYPE == SIMPLE_MESH_SCREENQUAD
	float4 svpos[3] = {
		float4(-1, 1, 0, 1),
		float4(3, 1, 0, 1),
		float4(-1, -3, 0, 1),
	};

	float2 uv[3] = {
		float2(0, 0),
		float2(2, 0),
		float2(0, 2),
	};

	o.WorldPosition = 0.0f;
	o.SVPosition = svpos[VertexID];
	o.TexCoord0  = uv[VertexID];
#elif SIMPLE_MESH_TYPE == SIMPLE_MESH_AACUBE
	float3 pos[8] =
	{
		//Z-
		float3(-1.0f, 1.0f, -1.0f),
		float3(1.0f, 1.0f, -1.0f),
		float3(-1.0f, -1.0f, -1.0f),
		float3(1.0f, -1.0f, -1.0f),
		//Z+
		float3(1.0f, 1.0f, 1.0f),
		float3(-1.0f, 1.0f, 1.0f),
		float3(1.0f, -1.0f, 1.0f),
		float3(-1.0f, -1.0f, 1.0f)
	};
	//TexCoord Not Important
	float2 uv[8] = {
		float2(0.0f,0.0f),
		float2(1.0f,0.0f),
		float2(0.0f,1.0f),
		float2(1.0f,1.0f),

		float2(0.0f,0.0f),
		float2(1.0f,0.0f),
		float2(0.0f,1.0f),
		float2(1.0f,1.0f)
	};

	uint vertexindex[36] = {
		0,1,2,1,3,2,
		4,5,6,5,7,6,
		1,4,3,4,6,3,
		5,0,7,0,2,7,
		5,4,0,4,1,0,
		2,3,7,3,6,7
	};

	o.WorldPosition = Multiply(float4(pos[vertexindex[VertexID]], 1.0f), LocalToWorld).xyz;
	o.SVPosition = Multiply(float4(o.WorldPosition, 1.0f), ENG_MatrixWorldToClip);
	o.TexCoord0  = uv[vertexindex[VertexID]];
#endif
	o.VertexColor = 1.0f;
	o.WorldNormal = 0.0f;
	o.WorldTangent = 0.0f;
	o.InstanceID = InstanceID;

	return o;
}