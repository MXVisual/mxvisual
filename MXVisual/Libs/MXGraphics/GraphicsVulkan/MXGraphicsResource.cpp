#include "MXGraphicsRenderer.h"
#include "GRenderer.h"

#include "GRenderWindow.h"
#include "GTexture.h"
#include "GPrimitive.h"
#include "GMaterial.h"
#include "GViewFrustum.h"

#include "GAPIWrapper.h"
#include "GCommandList.h"
#include "GShaderManager.h"

#include <unordered_map>

namespace MXVisual
{
	VkVertexInputAttributeDescription GInputDesc2D[3] = {
		/*POSITION*/{ 0, 0, VK_FORMAT_R32G32_SFLOAT, 0 },
		/*COLOR*/{ 1, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 8 },
		/*TEXCOORD*/{ 2, 0, VK_FORMAT_R32G32_SFLOAT, 24 },
	};
	VkVertexInputAttributeDescription GInputDescStatic[5] = {
		/*POSITION*/{ 0, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 0},
		/*NORMAL*/{ 1, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 16},
		/*TANGENT*/{ 2,	0, VK_FORMAT_R32G32B32A32_SFLOAT, 32},
		/*COLOR*/{ 3, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 48},
		/*TEXCOORD*/{ 4, 0, VK_FORMAT_R32G32_SFLOAT, 64},
	};

	VkVertexInputAttributeDescription GInputDescStatic_Instance[9] = {
		/*POSITION*/{ 0, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 0},
		/*NORMAL*/{ 1, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 16},
		/*TANGENT*/{ 2,	0, VK_FORMAT_R32G32B32A32_SFLOAT, 32},
		/*COLOR*/{ 3, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 48},
		/*TEXCOORD*/{ 4, 0, VK_FORMAT_R32G32_SFLOAT, 64},
		//Instance Data
		/*WORLD*/{ 0, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 0},
		/*WORLD*/{ 1, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 16},
		/*WORLD*/{ 2, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 32},
		/*WORLD*/{ 3, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 48},
	};

	VkVertexInputAttributeDescription GInputDescSkin[7] = {
		/*POSITION*/{ 0, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 0},
		/*NORMAL*/{ 1, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 16},
		/*TANGENT*/{ 2,	0, VK_FORMAT_R32G32B32A32_SFLOAT, 32},
		/*COLOR*/{ 3, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 48},
		/*TEXCOORD*/{ 4, 0, VK_FORMAT_R32G32_SFLOAT, 64},
		/*BLENDINDICES*/ { 5, 0, VK_FORMAT_R32G32B32A32_UINT, 72},
		/*BLENDWEIGHT*/ { 6, 0,VK_FORMAT_R32G32B32A32_SFLOAT, 88}
	};

	VkVertexInputAttributeDescription GInputDescSkin_Instance[11] = {
		/*POSITION*/{ 0, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 0},
		/*NORMAL*/{ 1, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 16},
		/*TANGENT*/{ 2,	0, VK_FORMAT_R32G32B32A32_SFLOAT, 32},
		/*COLOR*/{ 3, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 48},
		/*TEXCOORD*/{ 4, 0, VK_FORMAT_R32G32_SFLOAT, 64},
		/*BLENDINDICES*/ { 5, 0, VK_FORMAT_R32G32B32A32_UINT, 72},
		/*BLENDWEIGHT*/ { 6, 0,VK_FORMAT_R32G32B32A32_SFLOAT, 88},
		//Instance Data
		/*WORLD*/{ 0, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 0},
		/*WORLD*/{ 1, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 16},
		/*WORLD*/{ 2, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 32},
		/*WORLD*/{ 3, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 48},
	};


	VkFormat PixelFormatMap[ERT_Count][EPF_Count + 1] = {
		//EPFT_Resource
		{
			VK_FORMAT_R8_UNORM,
			VK_FORMAT_R8G8_UNORM,
			VK_FORMAT_R8G8B8A8_UNORM,

			VK_FORMAT_R16G16_UNORM,
			VK_FORMAT_R16G16_SFLOAT,
			VK_FORMAT_R16G16B16A16_UNORM,
			VK_FORMAT_R16G16B16A16_SFLOAT,

			VK_FORMAT_R32_SFLOAT,
			VK_FORMAT_R32G32_SFLOAT,
			VK_FORMAT_R32G32B32A32_SFLOAT,

			VK_FORMAT_BC1_RGB_UNORM_BLOCK,
			VK_FORMAT_BC3_UNORM_BLOCK,
			VK_FORMAT_BC4_UNORM_BLOCK,
			VK_FORMAT_BC5_UNORM_BLOCK,
			VK_FORMAT_BC6H_UFLOAT_BLOCK,
			VK_FORMAT_BC7_UNORM_BLOCK,

			VK_FORMAT_D24_UNORM_S8_UINT,
			VK_FORMAT_D32_SFLOAT_S8_UINT,

			//EPF_Count
			VK_FORMAT_UNDEFINED,
		},

		//EPFT_ShaderResourceView
		{
			VK_FORMAT_R8_UNORM,
			VK_FORMAT_R8G8_UNORM,
			VK_FORMAT_R8G8B8A8_UNORM,

			VK_FORMAT_R16G16_UNORM,
			VK_FORMAT_R16G16_SFLOAT,
			VK_FORMAT_R16G16B16A16_UNORM,
			VK_FORMAT_R16G16B16A16_SFLOAT,

			VK_FORMAT_R32_SFLOAT,
			VK_FORMAT_R32G32_SFLOAT,
			VK_FORMAT_R32G32B32A32_SFLOAT,

			VK_FORMAT_BC1_RGB_UNORM_BLOCK,
			VK_FORMAT_BC3_UNORM_BLOCK,
			VK_FORMAT_BC4_UNORM_BLOCK,
			VK_FORMAT_BC5_UNORM_BLOCK,
			VK_FORMAT_BC6H_UFLOAT_BLOCK,
			VK_FORMAT_BC7_UNORM_BLOCK,

			VK_FORMAT_D24_UNORM_S8_UINT,
			VK_FORMAT_D32_SFLOAT_S8_UINT,

			//EPF_Count
			VK_FORMAT_UNDEFINED,
		},

		//EPFT_RenderTargetView
		{
			VK_FORMAT_R8_UNORM,
			VK_FORMAT_R8G8_UNORM,
			VK_FORMAT_R8G8B8A8_UNORM,

			VK_FORMAT_R16G16_UNORM,
			VK_FORMAT_R16G16_SFLOAT,
			VK_FORMAT_R16G16B16A16_UNORM,
			VK_FORMAT_R16G16B16A16_SFLOAT,

			VK_FORMAT_R32_SFLOAT,
			VK_FORMAT_R32G32_SFLOAT,
			VK_FORMAT_R32G32B32A32_SFLOAT,

			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,

			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,

			//EPF_Count
			VK_FORMAT_UNDEFINED,
		},

		//EPFT_DepthStencilTargetView
		{
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,

			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,

			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,

			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,
			VK_FORMAT_UNDEFINED,

			VK_FORMAT_D24_UNORM_S8_UINT,
			VK_FORMAT_D32_SFLOAT_S8_UINT,

			//EPF_Count
			VK_FORMAT_UNDEFINED,
		}
	};
	UInt8		PixelFormatSize[EPF_Count + 1] = {
	   32,
	   32,
	   32,
	   64,
	   64,

	   32,
	   64,
	   128,
	   //DX Format
	   4,
	   8,
	   //Depth
	   32,
	   32,

	   //EPF_Count
	   0,
	};

	namespace GRendererResource
	{
		UInt32										uResourceID;
		Array<IGResource*>							Resources;

		std::unordered_map<std::string, VkFramebuffer>	FrameBufferMap;
		std::unordered_map<UInt64, VkRenderPass>	RenderPassMap;

		VkDescriptorPool					RasterizerDescriptorPool = nullptr;
		VkDescriptorPool					ComputeDescriptorPool = nullptr;

		VkDescriptorSetLayout				RasterizerDescriptorSetLayout = nullptr;
		VkDescriptorSetLayout				ComputeDescriptorSetLayout = nullptr;

		VkPipelineLayout					RasterizerRootSignature = nullptr;
		VkPipelineLayout					ComputeRootSignature = nullptr;

		GBuffer*							UploadBuffer = nullptr;
		UInt32								FrameBufferIndex = 0;
		Array<FrameBuffer*>					FrameBuffers;
		UInt32								RShaderResourceSetIndex = 0;
		UInt32								CShaderResourceSetIndex = 0;
		Array<RasterizerShaderResourceSet*>	RShaderResourceSet;
		Array<ComputeShaderResourceSet*>	CShaderResourceSet;

		std::map<UInt32, GSampler*>			m_mapSState;

		VertexInputDescription				DefaultInputLayout[(const UInt8)PrimitiveIAFactory::ECount] = { 0, };
		ShaderCompiledResult				DefaultVertexShader[(const UInt8)PrimitiveIAFactory::ECount][(const UInt32)MaterialDomain::ECount] = { nullptr, };

		Boolean					InitRasterizerRootSignature()
		{
			VkDescriptorSetLayoutBinding binding[] = {
				{
					(const UInt32)RasterizerRootSignatureDesc::EVS_CBV_BEGIN,
					VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					(const UInt32)RasterizerRootSignatureDesc::EVS_CBV_COUNT,
					VK_SHADER_STAGE_VERTEX_BIT , nullptr
				},
				{
					(const UInt32)RasterizerRootSignatureDesc::EVS_SRV_BEGIN,
					VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
					(const UInt32)RasterizerRootSignatureDesc::EVS_SRV_COUNT,
					VK_SHADER_STAGE_VERTEX_BIT , nullptr
				},
				{
					(const UInt32)RasterizerRootSignatureDesc::EVS_SAMPLER_BEGIN,
					VK_DESCRIPTOR_TYPE_SAMPLER,
					(const UInt32)RasterizerRootSignatureDesc::EVS_SAMPLER_COUNT,
					VK_SHADER_STAGE_VERTEX_BIT , nullptr
				},
				{
					(const UInt32)RasterizerRootSignatureDesc::EPS_CBV_BEGIN,
					VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					(const UInt32)RasterizerRootSignatureDesc::EPS_CBV_COUNT,
					VK_SHADER_STAGE_FRAGMENT_BIT , nullptr
				},
				{
					(const UInt32)RasterizerRootSignatureDesc::EPS_SRV_BEGIN,
					VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
					(const UInt32)RasterizerRootSignatureDesc::EPS_SRV_COUNT,
					VK_SHADER_STAGE_FRAGMENT_BIT , nullptr
				},
				{
					(const UInt32)RasterizerRootSignatureDesc::EPS_SAMPLER_BEGIN,
					VK_DESCRIPTOR_TYPE_SAMPLER,
					(const UInt32)RasterizerRootSignatureDesc::EPS_SAMPLER_COUNT,
					VK_SHADER_STAGE_FRAGMENT_BIT , nullptr
				},
			};

			VkDescriptorSetLayoutCreateInfo setlayoutInfo;
			memset(&setlayoutInfo, 0, sizeof(setlayoutInfo));
			setlayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
			setlayoutInfo.pBindings = binding;
			setlayoutInfo.bindingCount = ARRAYSIZE(binding);
			if (VK_SUCCESS != vkCreateDescriptorSetLayout(GRenderer::Device,
				&setlayoutInfo, nullptr, &RasterizerDescriptorSetLayout))
			{
				return False;
			}

			VkPipelineLayoutCreateInfo layoutInfo;
			memset(&layoutInfo, 0, sizeof(layoutInfo));
			layoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			layoutInfo.pSetLayouts = &RasterizerDescriptorSetLayout;
			layoutInfo.setLayoutCount = 1;
			if (VK_SUCCESS != vkCreatePipelineLayout(GRenderer::Device,
				&layoutInfo, nullptr, &RasterizerRootSignature))
			{
				return False;
			}

			const UInt32 MAX_DESCRIPTOR_SET = 256;
			VkDescriptorPoolSize descriptor[] = {
				{
					VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					(const UInt32)RasterizerRootSignatureDesc::EVS_CBV_COUNT +
					(const UInt32)RasterizerRootSignatureDesc::EPS_CBV_COUNT
				},
				{
					VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
					(const UInt32)RasterizerRootSignatureDesc::EVS_SRV_COUNT +
					(const UInt32)RasterizerRootSignatureDesc::EPS_SRV_COUNT
				},
				{
					VK_DESCRIPTOR_TYPE_SAMPLER,
					(const UInt32)RasterizerRootSignatureDesc::EVS_SAMPLER_COUNT +
					(const UInt32)RasterizerRootSignatureDesc::EPS_SAMPLER_COUNT
				},
			};

			VkDescriptorPoolCreateInfo DescriptorPoolInfo;
			memset(&DescriptorPoolInfo, 0, sizeof(DescriptorPoolInfo));
			DescriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
			DescriptorPoolInfo.maxSets = MAX_DESCRIPTOR_SET;
			DescriptorPoolInfo.poolSizeCount = ARRAYSIZE(descriptor);
			DescriptorPoolInfo.pPoolSizes = descriptor;
			if (VK_SUCCESS != vkCreateDescriptorPool(GRenderer::Device, &DescriptorPoolInfo, nullptr, 
				&RasterizerDescriptorPool))
			{
				return False;
			}

			return True;
		}
		Boolean					InitComputeRootSignature()
		{
			VkDescriptorSetLayoutBinding binding[] = {
				{
					(const UInt8)ComputeRootSignatureDesc::ECBV_BEGIN,
					VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					(const UInt8)ComputeRootSignatureDesc::ECBV_COUNT,
					VK_SHADER_STAGE_COMPUTE_BIT , nullptr
				},
				{
					(const UInt8)ComputeRootSignatureDesc::ESRV_BEGIN,
					VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
					(const UInt8)ComputeRootSignatureDesc::ESRV_COUNT,
					VK_SHADER_STAGE_COMPUTE_BIT , nullptr
				},
				{
					(const UInt8)ComputeRootSignatureDesc::EUAV_BEGIN,
					VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
					(const UInt8)ComputeRootSignatureDesc::EUAV_COUNT,
					VK_SHADER_STAGE_COMPUTE_BIT , nullptr
				},
				{
					(const UInt8)ComputeRootSignatureDesc::ESAMPLER_BEGIN,
					VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					(const UInt8)ComputeRootSignatureDesc::ESAMPLER_COUNT,
					VK_SHADER_STAGE_COMPUTE_BIT , nullptr
				},
			};

			VkDescriptorSetLayoutCreateInfo setlayoutInfo;
			memset(&setlayoutInfo, 0, sizeof(setlayoutInfo));
			setlayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
			setlayoutInfo.pBindings = binding;
			setlayoutInfo.bindingCount = ARRAYSIZE(binding);
			if (VK_SUCCESS != vkCreateDescriptorSetLayout(GRenderer::Device,
				&setlayoutInfo, nullptr, &GRendererResource::ComputeDescriptorSetLayout))
			{
				return False;
			}

			VkPipelineLayoutCreateInfo layoutInfo;
			memset(&layoutInfo, 0, sizeof(layoutInfo));
			layoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			layoutInfo.pSetLayouts = &ComputeDescriptorSetLayout;
			layoutInfo.setLayoutCount = 1;
			if (VK_SUCCESS != vkCreatePipelineLayout(GRenderer::Device,
				&layoutInfo, nullptr, &ComputeRootSignature))
			{
				return False;
			}

			const UInt32 MAX_DESCRIPTOR_SET = 256;
			VkDescriptorPoolSize descriptor[] = {
				{
					VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					(const UInt32)ComputeRootSignatureDesc::ECBV_COUNT
				},
				{
					VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
					(const UInt32)ComputeRootSignatureDesc::ESRV_COUNT
				},
				{
					VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
					(const UInt32)ComputeRootSignatureDesc::EUAV_COUNT
				},
				{
					VK_DESCRIPTOR_TYPE_SAMPLER,
					(const UInt32)ComputeRootSignatureDesc::ESAMPLER_COUNT
				},
			};

			VkDescriptorPoolCreateInfo DescriptorPoolInfo;
			memset(&DescriptorPoolInfo, 0, sizeof(DescriptorPoolInfo));
			DescriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
			DescriptorPoolInfo.maxSets = MAX_DESCRIPTOR_SET;
			DescriptorPoolInfo.poolSizeCount = ARRAYSIZE(descriptor);
			DescriptorPoolInfo.pPoolSizes = descriptor;
			if (VK_SUCCESS != vkCreateDescriptorPool(GRenderer::Device, &DescriptorPoolInfo, nullptr,
				&ComputeDescriptorPool))
			{
				return False;
			}

			return True;
		}

		Boolean					InitAPIDependencyResource()
		{
			if (!InitRasterizerRootSignature() ||
				!InitComputeRootSignature())
				return False;

			ShaderManager = new GShaderManager;

			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic].NumElements = ARRAY_ELEMENT_COUNT(GInputDescStatic);
			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic].pInputElementDescs = GInputDescStatic;
			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic_Instance].NumElements = ARRAY_ELEMENT_COUNT(GInputDescStatic_Instance);
			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic_Instance].pInputElementDescs = GInputDescStatic_Instance;

			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::ESkin].NumElements = ARRAY_ELEMENT_COUNT(GInputDescSkin);
			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::ESkin].pInputElementDescs = GInputDescSkin;

			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic2D].NumElements = ARRAY_ELEMENT_COUNT(GInputDesc2D);
			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic2D].pInputElementDescs = GInputDesc2D;

			const String strVertexShader3D = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVVertexShaderTemplate.msr");
			const String strVertexShader2D = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVVertexShader2D.msr");

			const Char* arrShaderTemplateFileName[(const UInt8)PrimitiveIAFactory::ECount] = {
				strVertexShader3D.CString(),
				strVertexShader3D.CString(),
				strVertexShader3D.CString(),
				strVertexShader2D.CString()
			};

			UInt32 primitiveTypeMap[(const UInt8)PrimitiveIAFactory::ECount] = {
				0,
				0,
				1,
				2
			};

			for (UInt32 uDomain = 0; uDomain < (const UInt32)MaterialDomain::ECount; uDomain++)
			{
				for (UInt32 uIAFactory = 0; uIAFactory < (const UInt32)PrimitiveIAFactory::ECount; uIAFactory++)
				{
					String strCacheName = String::Format("DefaultVertexShader_%d_%d.msr", uIAFactory, uDomain);

					ShaderMacro macros;
					if (!GRendererResource::ShaderManager->CompileShader(
						strCacheName.CString(),
						macros, DefaultVertexShader[uIAFactory][uDomain], nullptr, 0, ShaderType::EVertexShader, False))
					{
						IFile* pShaderFile = IFile::Open(arrShaderTemplateFileName[uIAFactory], IFile::EFDT_Text);
						if (pShaderFile == nullptr)return False;

						Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
						pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

						String strDomain = String::Format("%d", uDomain);
						macros.AddMacro("DOMAIN", strDomain.CString());
						switch ((PrimitiveIAFactory)uIAFactory)
						{
						case PrimitiveIAFactory::EStatic_Instance:
							macros.AddMacro("INSTANCE_RENDER", "1");
							break;
						case PrimitiveIAFactory::ESkin:
						case PrimitiveIAFactory::EStatic:
						case PrimitiveIAFactory::EStatic2D:
							macros.AddMacro("INSTANCE_RENDER", "0");
							break;
						}
						String strPrimitiveType = String::Format("%d", primitiveTypeMap[uIAFactory]);
						macros.AddMacro("PRIMITIVE_TYPE", strPrimitiveType.CString());
						String strTransformCount = String::Format("%d", GVarMaxGPUBlendMatrixPerPass.nValue);
						macros.AddMacro("MAX_TRANSFORMS_COUNT", strTransformCount.CString());

						String strCode = String::Format(pBuf, "", "");//Template With No Custom Code

						delete[] pBuf;
						pShaderFile->Close();

						GRendererResource::ShaderManager->CompileShader(
							strCacheName.CString(),
							macros, DefaultVertexShader[uIAFactory][uDomain], strCode.CString(), strCode.Length(), ShaderType::EVertexShader, False);
					}
				}
			}
			return True;
		}
		Void					ReleaseAPIDependencyResource()
		{
			MX_DELETE(ShaderManager);

			if (RasterizerRootSignature != nullptr)
			{
				vkDestroyPipelineLayout(GRenderer::Device, RasterizerRootSignature, nullptr);
				vkDestroyDescriptorSetLayout(GRenderer::Device, RasterizerDescriptorSetLayout, nullptr);
				RasterizerDescriptorSetLayout = nullptr;
				RasterizerRootSignature = nullptr;
			}
			if (ComputeRootSignature != nullptr)
			{
				vkDestroyPipelineLayout(GRenderer::Device, ComputeRootSignature, nullptr);
				vkDestroyDescriptorSetLayout(GRenderer::Device, ComputeDescriptorSetLayout, nullptr);
				ComputeDescriptorSetLayout = nullptr;
				ComputeRootSignature = nullptr;
			}
		}

		IGBuffer*				CreateBuffer(BufferType eType, Boolean bDynamic, UInt32 uSize)
		{
			VkBufferCreateInfo resource_desc = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
			resource_desc.size = Math::AlignmentedSize(uSize, 256);
			resource_desc.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
			resource_desc.queueFamilyIndexCount = 0;
			resource_desc.pQueueFamilyIndices = NULL;
			switch (eType)
			{
			case BufferType::EVertexBuffer:
				resource_desc.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
				break;
			case BufferType::EConstantBuffer:
				resource_desc.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
				break;
			case BufferType::EIndexBuffer:
				resource_desc.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
				break;
			default:
				return False;
			}

			if (bDynamic)
			{
				resource_desc.usage |= VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
			}
			else
			{
				resource_desc.usage |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;
			}

			VkBuffer buffer;
			if (vkCreateBuffer(GRenderer::Device, &resource_desc, NULL, &buffer)
				!= VkResult::VK_SUCCESS)
			{
				return False;
			}

			VkMemoryRequirements require;
			vkGetBufferMemoryRequirements(GRenderer::Device, buffer, &require);

			VkDeviceMemory memory = GRendererResource::AllocateMemory(require.size, True, require.memoryTypeBits,
				bDynamic ?
				(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) :
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);


			vkBindBufferMemory(GRenderer::Device, buffer, memory, 0);

			GBuffer* pNewBuffer = new GBuffer;
			pNewBuffer->Real = buffer;
			pNewBuffer->Memory = memory;
			pNewBuffer->Stride = 0;
			pNewBuffer->Size = resource_desc.size;

			GRendererResource::Resources.Add(pNewBuffer);
			GRendererResource::FrameStatistics.uGPUMemoryOccupy += pNewBuffer->Size;
			return pNewBuffer;
		}

		IGSampler*				GetSampler(TextureAddressMode eAddressMode, TextureFilterMode eFilterMode, UInt32 uMaxAnisotropicLevel)
		{
			UInt32 ID = ((eAddressMode & 0x8) << 16) | ((eFilterMode & 0x8) << 8) | (uMaxAnisotropicLevel & 0x8);

			IGSampler* pRes = nullptr;
			auto it = m_mapSState.find(ID);
			if (it == m_mapSState.end())
			{
				VkSamplerCreateInfo samplerCreateInfo = {};
				samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
				samplerCreateInfo.mipLodBias = 0.0;
				samplerCreateInfo.anisotropyEnable = VK_FALSE;
				samplerCreateInfo.maxAnisotropy = 1;
				samplerCreateInfo.compareOp = VK_COMPARE_OP_NEVER;
				samplerCreateInfo.minLod = 0.0;
				samplerCreateInfo.maxLod = 0.0;
				samplerCreateInfo.compareEnable = VK_FALSE;
				samplerCreateInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;

				switch (eAddressMode)
				{
				case TextureAddressMode::ETexAM_Clamp:
					samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
					samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
					samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
					break;
				case TextureAddressMode::ETexAM_Repeat:
					samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
					samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
					samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
					break;
				case TextureAddressMode::ETexAM_Mirror:
					samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
					samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
					samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
					break;
				}

				switch (eFilterMode)
				{
				case TextureFilterMode::ETexFM_Point:
					samplerCreateInfo.magFilter = VK_FILTER_NEAREST;
					samplerCreateInfo.minFilter = VK_FILTER_NEAREST;
					samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
					break;
				case TextureFilterMode::ETexFM_Bilinear:
					samplerCreateInfo.magFilter = VK_FILTER_LINEAR;
					samplerCreateInfo.minFilter = VK_FILTER_LINEAR;
					samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
					break;
				case TextureFilterMode::ETexFM_Anisotropic:
					samplerCreateInfo.magFilter = VK_FILTER_LINEAR;
					samplerCreateInfo.minFilter = VK_FILTER_LINEAR;
					samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
					samplerCreateInfo.anisotropyEnable = VK_TRUE;
					samplerCreateInfo.maxAnisotropy = 16;
					break;
				}

				VkSampler sampler;
				vkCreateSampler(GRenderer::Device, &samplerCreateInfo, nullptr, &sampler);

				GSampler* pSampler = new GSampler;
				pSampler->Real = sampler;

				m_mapSState[ID] = pSampler;
				GRendererResource::Resources.Add(pSampler);
				pRes = pSampler;
			}
			else
			{
				pRes = it->second;
			}
			return pRes;
		}

		VkImageView				GetImageView(const GTexture* pTexture)
		{
			if (pTexture->View == nullptr)
			{
				VkImageViewCreateInfo imageViewInfo;
				memset(&imageViewInfo, 0, sizeof(imageViewInfo));
				imageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
				imageViewInfo.image = pTexture->GetGResource();
				imageViewInfo.format = PixelFormatMap[ResourceType::ERT_ShaderResourceView][pTexture->GetFormat()];
				switch (pTexture->GetType())
				{
				case TextureType::ETexture1D:
					imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_1D;
					break;
				case TextureType::ETexture2D:
					imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
					break;
				case TextureType::ETexture3D:
					imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_3D;
					break;
				case TextureType::ETextureCube:
					imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
					break;
				}

				if (pTexture->GetFormat() == EPF_D24 || pTexture->GetFormat() == EPF_D32)
				{
					imageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
					if (imageViewInfo.format == VK_FORMAT_D16_UNORM_S8_UINT ||
						imageViewInfo.format == VK_FORMAT_D24_UNORM_S8_UINT ||
						imageViewInfo.format == VK_FORMAT_D32_SFLOAT_S8_UINT)
					{
						imageViewInfo.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
					}
				}
				else
				{
					imageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				}
				imageViewInfo.subresourceRange.baseArrayLayer = 0;
				imageViewInfo.subresourceRange.baseMipLevel = 0;
				imageViewInfo.subresourceRange.levelCount = pTexture->GetMipmapCount();
				imageViewInfo.subresourceRange.layerCount = pTexture->GetDepth();
				ASSERT_IF_FAILED(VK_SUCCESS == vkCreateImageView(GRenderer::Device, &imageViewInfo, nullptr, &((GTexture*)pTexture)->View));
			}

			return pTexture->View;
		}
		VkFramebuffer			GetGResource(const GRenderPass* pRP, const FrameBuffer* pFrameBuf)
		{
			std::string strID = String::Format(
				"%p%d%p%d%p%d%p%d%p%d%p%d%p%d%p%d%p%d",
				pFrameBuf->DSV, pFrameBuf->DSVSubresourceIndex,
				pFrameBuf->RTV[0], pFrameBuf->RTVSubresourceIndex[0],
				pFrameBuf->RTV[1], pFrameBuf->RTVSubresourceIndex[1],
				pFrameBuf->RTV[2], pFrameBuf->RTVSubresourceIndex[2],
				pFrameBuf->RTV[3], pFrameBuf->RTVSubresourceIndex[3],
				pFrameBuf->RTV[4], pFrameBuf->RTVSubresourceIndex[4],
				pFrameBuf->RTV[5], pFrameBuf->RTVSubresourceIndex[5],
				pFrameBuf->RTV[6], pFrameBuf->RTVSubresourceIndex[6],
				pFrameBuf->RTV[7], pFrameBuf->RTVSubresourceIndex[7]
			).CString();

			if (FrameBufferMap.find(strID) == FrameBufferMap.end())
			{
				VkFramebuffer VkFB = nullptr;

				Array<VkImageView> attachenmentViews;
				VkFramebufferCreateInfo info = { VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO };
				if (pFrameBuf->DSV != nullptr)
				{
					info.attachmentCount++;
					info.layers = pFrameBuf->DSV->GetDepth();
					info.width = pFrameBuf->DSV->GetWidth();
					info.height = pFrameBuf->DSV->GetHeight();

					attachenmentViews.Add(GetImageView((GTexture*)pFrameBuf->DSV));
				}
				for (UInt32 u = 0; u < 8; u++)
				{
					if (pFrameBuf->RTV[u] != nullptr)
					{
						info.layers = pFrameBuf->RTV[u]->GetDepth();
						info.width = pFrameBuf->RTV[u]->GetWidth();
						info.height = pFrameBuf->RTV[u]->GetHeight();

						attachenmentViews.Add(GetImageView((GTexture*)pFrameBuf->RTV[u]));
					}
				}

				info.renderPass = GetGResource(pRP);
				info.attachmentCount = attachenmentViews.ElementCount();
				info.pAttachments = attachenmentViews.GetRawData();

				ASSERT_IF_FAILED(VK_SUCCESS == vkCreateFramebuffer(GRenderer::Device, &info, nullptr, &VkFB));

				FrameBufferMap[strID] = VkFB;

				return VkFB;
			}


			return FrameBufferMap[strID];
		}
		VkRenderPass			GetGResource(const GRenderPass* pRenderPass)
		{
			UInt64 ID =
				(pRenderPass->DSVFormat & 0x3) << (8 * 2) |
				(pRenderPass->RTVFormats[0] & 0x3) << (7 * 2) |
				(pRenderPass->RTVFormats[1] & 0x3) << (6 * 2) |
				(pRenderPass->RTVFormats[2] & 0x3) << (5 * 2) |
				(pRenderPass->RTVFormats[3] & 0x3) << (4 * 2) |
				(pRenderPass->RTVFormats[4] & 0x3) << (3 * 2) |
				(pRenderPass->RTVFormats[5] & 0x3) << (2 * 2) |
				(pRenderPass->RTVFormats[6] & 0x3) << (1 * 2) |
				(pRenderPass->RTVFormats[7] & 0x3);

			if (RenderPassMap.find(ID) == RenderPassMap.end())
			{
				VkRenderPass VkRP = nullptr;

				Array<VkAttachmentReference> attachenmentRefs;
				Array<VkAttachmentDescription> attachenmentInfos;
				VkRenderPassCreateInfo info = { VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO };
				info.attachmentCount = pRenderPass->RTVCount;
				if (pRenderPass->DSVFormat != EPF_Invalid)
				{
					info.attachmentCount++;

					attachenmentRefs.Add(
						VkAttachmentReference{
							attachenmentInfos.ElementCount(),
							VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
						}
					);

					attachenmentInfos.Add(VkAttachmentDescription{
						0, PixelFormatMap[ResourceType::ERT_DepthStencilTargetView][pRenderPass->DSVFormat],
						VK_SAMPLE_COUNT_1_BIT,
						VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE,
						VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE,
						VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL
						});
				}
				for (UInt32 u = 0; u < pRenderPass->RTVCount; u++)
				{
					attachenmentRefs.Add(
						VkAttachmentReference{
							attachenmentInfos.ElementCount(),
							VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
						}
					);

					attachenmentInfos.Add(VkAttachmentDescription{
						0, PixelFormatMap[ResourceType::ERT_RenderTargetView][pRenderPass->RTVFormats[u]],
						VK_SAMPLE_COUNT_1_BIT,
						VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE,
						VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE,
						VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
						});
				}
				info.pAttachments = attachenmentInfos.GetRawData();

				VkSubpassDescription spDesc;
				memset(&spDesc, 0, sizeof(spDesc));
				spDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
				spDesc.colorAttachmentCount = pRenderPass->RTVCount;
				spDesc.pColorAttachments = pRenderPass->DSVFormat == EPF_Invalid ? &attachenmentRefs[0] : &attachenmentRefs[1];
				spDesc.pDepthStencilAttachment = pRenderPass->DSVFormat == EPF_Invalid ? nullptr : &attachenmentRefs[0];

				info.subpassCount = 1;
				info.pSubpasses = &spDesc;

				ASSERT_IF_FAILED(VK_SUCCESS == vkCreateRenderPass(GRenderer::Device, &info, nullptr, &VkRP));

				RenderPassMap[ID] = VkRP;

				return VkRP;
			}

			return RenderPassMap[ID];
		}

		VkDeviceMemory			AllocateMemory(
			UInt32 uSize,
			Boolean bTextureMemory,
			UInt32 TypeIndexMask,
			VkMemoryPropertyFlags PropertyFlags)
		{
			VkPhysicalDeviceMemoryProperties prop;
			UInt32 memoryTypeIndex = -1;
			vkGetPhysicalDeviceMemoryProperties(GRenderer::SelectedAdapter, &prop);

			for (UInt32 u = 0; u < prop.memoryTypeCount; u++)
			{
				if ((TypeIndexMask & (1 << u)) != 0 &&
					(prop.memoryTypes[u].propertyFlags & PropertyFlags) == PropertyFlags)
				{
					memoryTypeIndex = u;
					break;
				}
			}
			if (memoryTypeIndex == -1)
				return nullptr;

			VkDeviceMemory Memory = nullptr;
			VkMemoryAllocateInfo memoryInfo = { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO };
			memoryInfo.allocationSize = uSize;
			memoryInfo.memoryTypeIndex = memoryTypeIndex;
			vkAllocateMemory(GRenderer::Device, &memoryInfo, nullptr, &Memory);
			return Memory;
		}


		VkPipelineRasterizationStateCreateInfo RasterizerState(
			VkCullModeFlagBits eCullMode,
			Boolean bWireFrame,
			Boolean bDepthClip,
			UInt32 uDepthBias,
			Boolean bScissor)
		{
			VkPipelineRasterizationStateCreateInfo rs_desc;
			memset(&rs_desc, 0, sizeof(rs_desc));
			rs_desc.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
			rs_desc.pNext = NULL;
			rs_desc.polygonMode = bWireFrame ? VK_POLYGON_MODE_LINE : VK_POLYGON_MODE_FILL;
			rs_desc.cullMode = eCullMode;
			rs_desc.frontFace = VK_FRONT_FACE_CLOCKWISE;
			rs_desc.lineWidth = 1.0f;
			rs_desc.depthClampEnable = VK_FALSE;// bDepthClip ? VK_TRUE : VK_FALSE;
			rs_desc.depthBiasEnable = uDepthBias > 0 ? VK_TRUE : VK_FALSE;
			rs_desc.depthBiasConstantFactor = uDepthBias;
			rs_desc.depthBiasSlopeFactor = uDepthBias;
			rs_desc.depthBiasClamp = 0.0f;
			rs_desc.rasterizerDiscardEnable = VK_FALSE;
			return rs_desc;
		}

		VkPipelineDepthStencilStateCreateInfo DepthStencilState(
			VkCompareOp eDepthOp,
			Boolean bDepthWrite,
			Boolean bStencil,
			UInt32	uStencilReadMask,
			UInt32	uStencilWriteMask)
		{
			VkPipelineDepthStencilStateCreateInfo ds_desc;
			ds_desc.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
			ds_desc.pNext = NULL;
			ds_desc.depthBoundsTestEnable = VK_FALSE;
			ds_desc.depthWriteEnable = bDepthWrite ? VK_TRUE : VK_FALSE;
			ds_desc.depthTestEnable = VK_TRUE;
			ds_desc.depthCompareOp = eDepthOp;
			ds_desc.minDepthBounds = 0.0f;
			ds_desc.maxDepthBounds = 1.0f;
			ds_desc.stencilTestEnable = bStencil ? VK_TRUE : VK_FALSE;
			ds_desc.front = { VK_STENCIL_OP_KEEP,VK_STENCIL_OP_KEEP,VK_STENCIL_OP_KEEP,VK_COMPARE_OP_NEVER, 0,0,0 };
			ds_desc.back = { VK_STENCIL_OP_KEEP,VK_STENCIL_OP_KEEP,VK_STENCIL_OP_KEEP,VK_COMPARE_OP_NEVER, 0,0,0 };

			return ds_desc;
		}

		BlendStateCreateInfo BlendState(
			Boolean bBlend,
			VkBlendOp eAlphaOp,
			VkBlendOp eColorOp,
			VkBlendFactor eAlphaSrc,
			VkBlendFactor eAlphaDest,
			VkBlendFactor eColorSrc,
			VkBlendFactor eColorDest,
			UInt8	uWriteMask)
		{
			BlendStateCreateInfo bs_desc;
			bs_desc.pNext = nullptr;
			bs_desc.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
			bs_desc.attachmentCount = 1;
			bs_desc.logicOpEnable = VK_FALSE;
			bs_desc.logicOp = VK_LOGIC_OP_COPY;
			bs_desc.blendConstants[0] = 1.0f;
			bs_desc.blendConstants[1] = 1.0f;
			bs_desc.blendConstants[2] = 1.0f;
			bs_desc.blendConstants[3] = 1.0f;
			bs_desc.flags = 0;

			memset(bs_desc.RenderTarget, 0, sizeof(bs_desc.RenderTarget));

			bs_desc.RenderTarget[0].blendEnable = bBlend ? VK_TRUE : VK_FALSE;
			bs_desc.RenderTarget[0].colorWriteMask = uWriteMask;
			bs_desc.RenderTarget[0].alphaBlendOp = eAlphaOp;
			bs_desc.RenderTarget[0].dstAlphaBlendFactor = eAlphaDest;
			bs_desc.RenderTarget[0].srcAlphaBlendFactor = eAlphaSrc;

			bs_desc.RenderTarget[0].colorBlendOp = eColorOp;
			bs_desc.RenderTarget[0].dstColorBlendFactor = eColorDest;
			bs_desc.RenderTarget[0].srcColorBlendFactor = eColorSrc;
			return bs_desc;
		}

		Void								ResetFrameBuffer()
		{
			FrameBufferIndex = 0;
		}
		Void								NextFrameBuffer()
		{
			FrameBufferIndex++;
		}
		FrameBuffer*						GetCurFrameBuffer()
		{
			if (FrameBufferIndex >= FrameBuffers.ElementCount())
			{
				FrameBuffers.Add(new FrameBuffer());
			}

			return FrameBuffers[FrameBufferIndex];
		}

		Void								ResetShaderResourceSet()
		{
			RShaderResourceSetIndex = 0;
			CShaderResourceSetIndex = 0;
		}
		Void								NextRasterizerShaderResourceSet()
		{
			RShaderResourceSetIndex++;
		}
		RasterizerShaderResourceSet*		GetCurRasterizerShaderResourceSet()
		{
			if (RShaderResourceSetIndex >= RShaderResourceSet.ElementCount())
			{
				RShaderResourceSet.Add(new RasterizerShaderResourceSet());
			}

			return RShaderResourceSet[RShaderResourceSetIndex];
		}
		Void								NextComputeShaderResourceSet()
		{
			CShaderResourceSetIndex++;
		}
		ComputeShaderResourceSet*			GetCurComputeShaderResourceSet()
		{
			if (CShaderResourceSetIndex >= CShaderResourceSet.ElementCount())
			{
				CShaderResourceSet.Add(new ComputeShaderResourceSet());
			}

			return CShaderResourceSet[CShaderResourceSetIndex];
		}
	}

	template<>
	DescriptorHeap<True>::DescriptorHeap()
	{
		VkDescriptorSetAllocateInfo DescriptorSetInfo;
		memset(&DescriptorSetInfo, 0, sizeof(DescriptorSetInfo));
		DescriptorSetInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		DescriptorSetInfo.descriptorPool = GRendererResource::RasterizerDescriptorPool;
		DescriptorSetInfo.descriptorSetCount = 1;
		DescriptorSetInfo.pSetLayouts = &GRendererResource::RasterizerDescriptorSetLayout;

		ASSERT_IF_FAILED(VK_SUCCESS == vkAllocateDescriptorSets(GRenderer::Device, &DescriptorSetInfo, &m_Set));
	}
	template<>
	DescriptorHeap<True>::~DescriptorHeap()
	{
		if (m_Set != nullptr)
		{
			vkFreeDescriptorSets(GRenderer::Device, GRendererResource::RasterizerDescriptorPool, 1, &m_Set);
			m_Set = nullptr;
		}
	}
	template<>
	DescriptorHeap<False>::DescriptorHeap()
	{
		VkDescriptorSetAllocateInfo DescriptorSetInfo;
		memset(&DescriptorSetInfo, 0, sizeof(DescriptorSetInfo));
		DescriptorSetInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		DescriptorSetInfo.descriptorPool = GRendererResource::ComputeDescriptorPool;
		DescriptorSetInfo.descriptorSetCount = 1;
		DescriptorSetInfo.pSetLayouts = &GRendererResource::ComputeDescriptorSetLayout;

		ASSERT_IF_FAILED(VK_SUCCESS == vkAllocateDescriptorSets(GRenderer::Device, &DescriptorSetInfo, &m_Set));
	}
	template<>
	DescriptorHeap<False>::~DescriptorHeap()
	{
		if (m_Set != nullptr)
		{
			vkFreeDescriptorSets(GRenderer::Device, GRendererResource::ComputeDescriptorPool, 1, &m_Set);
			m_Set = nullptr;
		}
	}

	Void RasterizerShaderResourceSet::BindGResource(IGCommandList* pCommandList)
	{
		VkCommandBuffer APICommandlist = ((GCommandList*)pCommandList)->APICommandlist;

		VkDescriptorSet set = ResourceHeap.GetGResource();
		vkCmdBindDescriptorSets(
			APICommandlist, VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_GRAPHICS,
			GRendererResource::RasterizerRootSignature, 0, 1, &set, 0, nullptr);
	}
	Void ComputeShaderResourceSet::BindGResource(IGCommandList* pCommandList)
	{
		VkCommandBuffer APICommandlist = ((GCommandList*)pCommandList)->APICommandlist;

		VkDescriptorSet set = ResourceHeap.GetGResource();
		vkCmdBindDescriptorSets(
			APICommandlist, VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_COMPUTE,
			GRendererResource::ComputeRootSignature, 0, 1, &set, 0, nullptr);
	}

	IGRenderWindow*		GraphicsResource::CreateRenderWindow(const IGRenderWindow::Description& desc)
	{
		GRenderWindow* pTemp = new GRenderWindow(GRendererResource::uResourceID);
		if (!pTemp->Init(desc))
		{
			Log::Output("GraphicsD3D12", "RenderWindow Create Failed");
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		return pTemp;
	}
	IGTexture*			GraphicsResource::CreateTexture(const IGTexture::Description& desc)
	{
		GTexture* pTemp = new GTexture(GRendererResource::uResourceID);
		if (!pTemp->Init(desc))
		{
			Log::Output("GraphicsD3D12", "Texture Create Failed");
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		return pTemp;
	}
	IGPrimitive*		GraphicsResource::CreatePrimitive(const IGPrimitive::Description& desc)
	{
		GPrimitive* pTemp = nullptr;
		switch (desc.eType)
		{
		case IGPrimitive::EPrimT_Line:pTemp = new GPrimitiveLine(GRendererResource::uResourceID);	break;
		case IGPrimitive::EPrimT_Rigid:	pTemp = new GPrimitiveRigid(GRendererResource::uResourceID); break;
		case IGPrimitive::EPrimT_Skin:pTemp = new GPrimitiveSkin(GRendererResource::uResourceID);	break;
		case IGPrimitive::EPrimT_Rigid2D: pTemp = new GPrimitive2D(GRendererResource::uResourceID); break;
		default:
		{
			Log::Output("GraphicsD3D12", "Invalid Primitive Type");
			return nullptr;
		}
		}

		if (!pTemp->Init(desc.bDynamic, desc.uVertexCount, desc.uIndexCount))
		{
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		return pTemp;
	}
	IGMaterial*			GraphicsResource::CreateMaterial(const IGMaterial::Description& desc, Boolean bReCreate)
	{
		IGMaterial::Description ModifiedDesc = desc;
		const GShaderManager::MaterialCache* cache = nullptr;

		bReCreate &= GRendererResource::HasRawShaderFolder();

		if (!bReCreate)
			cache = GRendererResource::ShaderManager->GetMaterialPropertyCache(desc.strUniqueName.CString());

		if (cache != nullptr)
		{
			ModifiedDesc.eDomain = (MaterialDomain)cache->uDomain;
			ModifiedDesc.eShadingModel = (MaterialShadingModel)cache->uShadingModel;
			ModifiedDesc.eFaceType = (MaterialFaceType)cache->uFaceType;
			ModifiedDesc.eBlendMode = (MaterialBlendMode)cache->uBlendMode;
		}

		GMaterial* pTemp = new GMaterial(GRendererResource::uResourceID);
		if (!pTemp->Init(ModifiedDesc, bReCreate))
		{
			Log::Output("GraphicsD3D12", "Material Create Failed");
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		if (cache == nullptr)
			pTemp->Cache();
		return pTemp;
	}
	IGMaterialParameterBuffer* GraphicsResource::CreateMaterialParameterBuffer(IGMaterial* pMaterial)
	{
		GCommonMaterialParameterBuffer* pBlock = new GCommonMaterialParameterBuffer(GRendererResource::uResourceID);
		if (!((GMaterial*)pMaterial)->ConvertMaterialParameterBlock(pBlock))
		{
			Log::Output("GraphicsD3D12", "MaterialParameterBlock Create Failed");
			delete pBlock;
			return nullptr;
		}
		GRendererResource::Resources.Add(pBlock);
		GRendererResource::uResourceID++;
		return pBlock;
	}
	IGPrimitiveTransformBuffer*GraphicsResource::CreatePrimitiveTransformBuffer(Boolean bForInstance)
	{
		GPrimitiveTransformBuffer* pBlock = new GPrimitiveTransformBuffer(GRendererResource::uResourceID);
		if (!pBlock->Init(bForInstance, GVarMaxGPUBlendMatrixPerPass.nValue))
		{
			Log::Output("GraphicsD3D12", "PrimitiveTransformBlock Create Failed");
			delete pBlock;
			return nullptr;
		}
		GRendererResource::Resources.Add(pBlock);
		GRendererResource::uResourceID++;
		return pBlock;
	}
	IGViewFrustum*		GraphicsResource::CreateViewFrustum(const IGViewFrustum::Description& desc)
	{
		GViewFrustum* pViewFrustum = new GViewFrustum(GRendererResource::uResourceID);
		if (!pViewFrustum->Init(desc))
		{
			Log::Output("GraphicsD3D12", "ViewFrustum Create Failed");
			delete pViewFrustum;
			return nullptr;
		}

		GRendererResource::Resources.Add(pViewFrustum);
		GRendererResource::uResourceID++;
		return pViewFrustum;
	}
}