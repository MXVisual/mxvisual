#pragma once
#ifndef _G_RENDERWINDOW_
#define _G_RENDERWINDOW_
#include "MXPlatform.h"
#include "MXGraphicsResource.h"
#include "GTexture.h"
#include "GRenderer.h"
#include <unordered_map>
#if PLATFORM == PLATFORM_WINDOWS
#include "Windows.h"
#endif

namespace MXVisual
{
	class GRenderWindow : public IGRenderWindow
	{
	public:
		GRenderWindow(UInt32 UniqueID)
			: m_uUniqueID(UniqueID)
			, m_bActived(True)
			, m_uBackbufferCount(0)
			, m_eBackbufferFormat(PixelFormat::EPF_Invalid)
			, m_pSwapChain(nullptr)
			, m_uActiveRtv(0)
		{
		}
		~GRenderWindow();

		UInt32 GetUniqueID() const { return m_uUniqueID; }

		Boolean Init(const IGRenderWindow::Description& desc);

		Float2	ScreenToWindow(const Float2& ScreenPos) const;

		const UInt32 GetWidth() const { return m_uBackbufferWidth; }
		const UInt32 GetHeight() const { return m_uBackbufferHeight; }
		const IGTexture* GetRenderTarget() const
		{
			return m_arrRenderTarget[m_uActiveRtv];
		}

		Boolean	IsActived() const override { return m_bActived; }
		Void	Show(Boolean bShow) override;
		Void	Resize(UInt32 uWidth, UInt32 uHeight);
		Void	Maximize();
		Void	Minimize();

		Void	ResizeTarget(UInt32 uWidth, UInt32 uHeight);
		Void	Present() override;

	protected:
		Boolean InitSwapChain();
		Void	ReleaseSwapChain();

#if PLATFORM == PLATFORM_WINDOWS
		static std::unordered_map<UInt64, GRenderWindow*> m_mapRenderWindows;
		static LRESULT CALLBACK RenderWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
#endif

		UInt32 m_uUniqueID;
		UInt64 m_uWindowHandle;
		Boolean m_bActived;
		UInt32 m_uBackbufferWidth;
		UInt32 m_uBackbufferHeight;
		UInt32 m_uBackbufferCount;
		PixelFormat m_eBackbufferFormat;

		VkSurfaceKHR			m_pSurface;
		VkSwapchainKHR			m_pSwapChain;

		UInt32					m_uActiveRtv;
		Array<IGTexture*>		m_arrRenderTarget;
	};
}

#endif