#pragma once
#ifndef _G_TEXTURE_
#define _G_TEXTURE_
#include "MXGraphicsResource.h"
#include "MXConstant.h"
#include "GRenderer.h"

namespace MXVisual
{
	class GTexture : public IGTexture
	{
	public:
		VkImageLayout	State;
		VkDeviceMemory	Memory;
		VkImageView		View;

	public:
		UInt32			GetUniqueID() const override { return m_uUniqueID; }

		GTexture(UInt32 UniqueID);
		~GTexture();

		Boolean			Init(const IGTexture::Description& desc);
		Boolean			InitFromResource(VkImage Resource, const IGTexture::Description& desc);

		PixelFormat		GetFormat() const override { return m_eFormat; }
		TextureType		GetType() const override { return m_eType; }

		UInt32			GetWidth() const override { return m_uWidth; }
		UInt32			GetHeight() const override { return m_uHeight; }
		UInt32			GetDepth() const override { return m_uDepth; }
		UInt32			GetMipmapCount() const override { return m_uMipMapCount; }

		Boolean			Map(UInt32 uDepth, UInt32 uMipMap, MapRegion& region, Boolean bReadBack = False) override;
		Void			Unmap() override;

		Boolean			Targetable() const { return m_bTargetable; }
		VkImage			GetGResource() const { return m_Resource; }
		Float4			GetClearValue() const { return m_f4ClearValue; }

	protected:
		UInt32		m_uUniqueID;

		PixelFormat m_eFormat;
		TextureType	m_eType;

		VkImage		m_Resource;
		Float4		m_f4ClearValue;

		Boolean		m_bTargetable;
		Boolean		m_bUnorderAccessable;
		
		UInt32		m_uWidth;
		UInt32		m_uHeight;
		UInt32		m_uDepth;
		UInt32		m_uMipMapCount;

		struct MappedSubResource
		{
			UInt32 uDepth = 0;
			UInt32 uMip = 0;
			UInt64 uSize = 0;
			UInt8* pCPUBuffer = nullptr;
			~MappedSubResource()
			{
				MX_DELETE(pCPUBuffer);
			}
		};
		MappedSubResource* m_pMapData;
	};
}

#endif