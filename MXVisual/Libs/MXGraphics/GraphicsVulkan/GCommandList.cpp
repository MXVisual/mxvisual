#include "GCommandList.h"
#include "GTexture.h"

namespace MXVisual
{
	GCommandList::GCommandList(VkCommandPool APICP, VkCommandBuffer APICL)
		: APICommandlist(APICL)
		, APICommandPool(APICP)
	{
	}
	GCommandList::~GCommandList()
	{
		if (APICommandlist != nullptr)
		{
			vkFreeCommandBuffers(GRenderer::Device, APICommandPool, 1, &APICommandlist);
		}
	}

	Void	GCommandList::SetViewport(const GViewport& Viewport)
	{
		VkViewport	 vp;
		vp.x = Viewport.uX;
		vp.y = Viewport.uY;
		vp.width = Viewport.uWidth;
		vp.height = Viewport.uHeight;
		vp.minDepth = Viewport.fMinDepth;
		vp.maxDepth = Viewport.fMaxDepth;
		vkCmdSetViewport(APICommandlist, 0, 1, &vp);
	}
	Void	GCommandList::SetScissor(const Region& Scissor)
	{
		VkRect2D rect;
		rect.offset.x = Scissor.uLeft;
		rect.offset.y = Scissor.uTop;
		rect.extent.width = Scissor.uRight - Scissor.uLeft;
		rect.extent.height = Scissor.uBottom - Scissor.uTop;
		vkCmdSetScissor(APICommandlist, 0, 1, &rect);
	}

	Void	GCommandList::ResourceBarrier(GTexture* pTexture, VkImageLayout targetstate)
	{
		if (pTexture->State != targetstate)
		{
			VkImageMemoryBarrier barrier = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER };
			barrier.image = pTexture->GetGResource();
			barrier.oldLayout = pTexture->State;
			barrier.newLayout = targetstate;
			barrier.srcQueueFamilyIndex = 0;
			barrier.dstQueueFamilyIndex = 0;
			if (pTexture->GetFormat() == EPF_D24 || pTexture->GetFormat() == EPF_D32)
			{
				VkFormat ft = PixelFormatMap[ResourceType::ERT_DepthStencilTargetView][pTexture->GetFormat()];
				barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
				if (ft == VK_FORMAT_D16_UNORM_S8_UINT ||
					ft == VK_FORMAT_D24_UNORM_S8_UINT ||
					ft == VK_FORMAT_D32_SFLOAT_S8_UINT)
				{
					barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
				}
			}
			else
			{
				barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			}
			barrier.subresourceRange.baseArrayLayer = 0;
			barrier.subresourceRange.baseMipLevel = 0;
			barrier.subresourceRange.layerCount = pTexture->GetDepth();
			barrier.subresourceRange.levelCount = pTexture->GetMipmapCount();

			vkCmdPipelineBarrier(APICommandlist,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_DEPENDENCY_BY_REGION_BIT,
				0, nullptr,
				0, nullptr,
				1, &barrier);

			pTexture->State = targetstate;
		}
	}
	/*
	Void	GCommandList::ResourceBarrier(GBuffer* pBuffer, D3D12_RESOURCE_STATES targetstate)
	{
		if (pBuffer->State != targetstate &&
			pBuffer->State != D3D12_RESOURCE_STATE_GENERIC_READ)
		{
			D3D12_RESOURCE_BARRIER barrier;
			barrier.Transition.pResource = pBuffer->Real;
			barrier.Transition.StateBefore = pBuffer->State;
			barrier.Transition.StateAfter = targetstate;
			barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			APICommandlist->ResourceBarrier(1, &barrier);

			pBuffer->State = targetstate;
		}
	}
	*/
	Void	GCommandList::BindPipeline(GPipeline* Pipeline)
	{
		vkCmdBindPipeline(APICommandlist,
			Pipeline->PSOForRasterizer ? VK_PIPELINE_BIND_POINT_GRAPHICS : VK_PIPELINE_BIND_POINT_COMPUTE,
			Pipeline->PSO);
	}
	Void	GCommandList::BindRenderTarget(
		Boolean bClearDepthTarget,
		const IGTexture* DT, UInt32 uDTSubresourceIdx,
		Boolean bClearColorTarget,
		const IGTexture* RT0, UInt32 uRT0SubresourceIdx,
		const IGTexture* RT1, UInt32 uRT1SubresourceIdx,
		const IGTexture* RT2, UInt32 uRT2SubresourceIdx,
		const IGTexture* RT3, UInt32 uRT3SubresourceIdx,
		const IGTexture* RT4, UInt32 uRT4SubresourceIdx,
		const IGTexture* RT5, UInt32 uRT5SubresourceIdx,
		const IGTexture* RT6, UInt32 uRT6SubresourceIdx,
		const IGTexture* RT7, UInt32 uRT7SubresourceIdx)
	{
		if (bInRenderPass)
		{
			vkCmdEndRenderPass(APICommandlist);
		}

		FrameBuffer* pFrameBuffer = GRendererResource::GetCurFrameBuffer();

		GRenderPass RP;
		static Array<VkClearAttachment> clearInfo;
		static Array<VkClearRect>		clearRect;
		VkRect2D						renderArea = { 0,0,0,0 };

		if (DT != nullptr)
		{
			RP.DSVFormat = DT->GetFormat();

			if (pFrameBuffer->DSV != DT || pFrameBuffer->DSVSubresourceIndex != uDTSubresourceIdx)
			{
				pFrameBuffer->DSV = DT;
				pFrameBuffer->DSVSubresourceIndex = uDTSubresourceIdx;
			}

			renderArea.extent.width = DT->GetWidth();
			renderArea.extent.height = DT->GetHeight();

			VkImageAspectFlags  aspectMask = 0;
			VkImageLayout		targetstate = VK_IMAGE_LAYOUT_UNDEFINED;
			if (RP.DSVFormat == EPF_D24 || RP.DSVFormat == EPF_D32)
			{
				VkFormat ft = PixelFormatMap[ResourceType::ERT_DepthStencilTargetView][RP.DSVFormat];

				if (ft == VK_FORMAT_D16_UNORM_S8_UINT ||
					ft == VK_FORMAT_D24_UNORM_S8_UINT ||
					ft == VK_FORMAT_D32_SFLOAT_S8_UINT)
				{
					targetstate = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
					aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
				}
				else
				{
					targetstate = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL;
					aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
				}
			}

			ResourceBarrier((GTexture*)DT, targetstate);

			if (bClearDepthTarget)
			{
				VkClearAttachment clear;
				clear.aspectMask = aspectMask;
				clear.colorAttachment = clearInfo.ElementCount();
				clear.clearValue.depthStencil.depth = DEPTH_CLEAR_VALUE;
				clear.clearValue.depthStencil.stencil = 0;

				clearInfo.Add(clear);

				clearRect.Add(
					VkClearRect{
						{{0,0}, {DT->GetWidth(), DT->GetHeight()}},
						0,
						DT->GetDepth()
					}
				);
			}
		}

		const IGTexture* RTs[] = { RT0, RT1, RT2, RT3, RT4, RT5, RT6, RT7 };
		UInt32 RTSubresource[] = {
			uRT0SubresourceIdx, uRT1SubresourceIdx, uRT2SubresourceIdx, uRT3SubresourceIdx,
			uRT4SubresourceIdx, uRT5SubresourceIdx, uRT6SubresourceIdx, uRT7SubresourceIdx
		};

		for (UInt32 u = 0; u < 8; u++)
		{
			if (RTs[u] == nullptr)
				break;

			renderArea.extent.width = RTs[u]->GetWidth();
			renderArea.extent.height = RTs[u]->GetHeight();

			RP.RTVCount++;
			RP.RTVFormats[u] = RTs[u]->GetFormat();

			if (pFrameBuffer->RTV[u] != RTs[u] || pFrameBuffer->RTVSubresourceIndex[u] != RTSubresource[u])
			{
				pFrameBuffer->RTV[u] = RTs[u];
				pFrameBuffer->RTVSubresourceIndex[u] = RTSubresource[u];
			}

			ResourceBarrier((GTexture*)DT, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

			if (bClearColorTarget)
			{
				Float4 clearValue = ((const GTexture*)RTs[u])->GetClearValue();

				VkClearAttachment clear;
				clear.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				clear.colorAttachment = clearInfo.ElementCount();
				clear.clearValue.color.float32[0] = clearValue[0];
				clear.clearValue.color.float32[1] = clearValue[1];
				clear.clearValue.color.float32[2] = clearValue[2];
				clear.clearValue.color.float32[3] = clearValue[3];

				clearInfo.Add(clear);

				clearRect.Add(
					VkClearRect{
						{{0,0}, {RTs[u]->GetWidth(), RTs[u]->GetHeight()}},
						0,
						RTs[u]->GetDepth()
					}
				);
			}
		}

		VkRenderPassBeginInfo info = {
				VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
				nullptr,
				GRendererResource::GetGResource(&RP),
				GRendererResource::GetGResource(&RP, pFrameBuffer),
				renderArea,
				0, nullptr,
		};
		vkCmdBeginRenderPass(APICommandlist, &info, VK_SUBPASS_CONTENTS_INLINE);

		if (clearInfo.ElementCount() > 0)
		{
			vkCmdClearAttachments(APICommandlist,
				clearInfo.ElementCount(), clearInfo.GetRawData(),
				clearRect.ElementCount(), clearRect.GetRawData());
		}

		clearInfo.Clear();
		GRendererResource::NextFrameBuffer();
	}
	Void	GCommandList::BindVertexBuffer(UInt32 uSlot, IGBuffer* pBuffer, UInt32 uStride)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);

		GBuffer* pRealBuffer = (GBuffer*)pBuffer;
		//ResourceBarrier(pRealBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);

		VkDeviceSize size = 0;
		vkCmdBindVertexBuffers(APICommandlist, uSlot, 1, &pRealBuffer->Real, &size);
	}
	Void	GCommandList::BindIndexBuffer(IGBuffer* pBuffer, Boolean b32Bit)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);

		GBuffer* pRealBuffer = (GBuffer*)pBuffer;
		//ResourceBarrier(pRealBuffer, D3D12_RESOURCE_STATE_INDEX_BUFFER);

		vkCmdBindIndexBuffer(APICommandlist, pRealBuffer->Real, 0,
			b32Bit ? VkIndexType::VK_INDEX_TYPE_UINT32 : VkIndexType::VK_INDEX_TYPE_UINT16);
	}

	Void	GCommandList::BindShaderResource(ShaderType eType, UInt32 uSlot, const IGTexture* pTexture)
	{
		ASSERT_IF_FAILED(pTexture != nullptr);
		RasterizerShaderResourceSet* pRSet = GRendererResource::GetCurRasterizerShaderResourceSet();

		GTexture* pRealTexture = (GTexture*)pTexture;
		/*
		D3D12_RESOURCE_STATES targetState = eType == ShaderType::EPixelShader ?
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE :
			D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE;

		ResourceBarrier(pRealTexture, targetState);
		*/
		VkDescriptorImageInfo imgInfo;
		imgInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imgInfo.imageView = GRendererResource::GetImageView(pRealTexture);
		imgInfo.sampler = nullptr;

		VkWriteDescriptorSet writeSet = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
		writeSet.pImageInfo = &imgInfo;
		writeSet.descriptorCount = 1;
		writeSet.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		writeSet.dstArrayElement = 0;

		switch (eType)
		{
		case ShaderType::EVertexShader:
			if (pRSet->VS_SRV[uSlot] != pTexture)
			{
				pRSet->VS_SRV[uSlot] = pTexture;

				writeSet.dstSet = pRSet->ResourceHeap.GetGResource();
				writeSet.dstBinding = (const UInt8)RasterizerRootSignatureDesc::EVS_SRV_BEGIN + uSlot;

				vkUpdateDescriptorSets(GRenderer::Device, 1, &writeSet, 0, nullptr);
			}
			break;
		case ShaderType::EPixelShader:
			if (pRSet->PS_SRV[uSlot] != pTexture)
			{
				pRSet->PS_SRV[uSlot] = pTexture;

				writeSet.dstSet = pRSet->ResourceHeap.GetGResource();
				writeSet.dstBinding = (const UInt8)RasterizerRootSignatureDesc::EPS_SRV_BEGIN + uSlot;

				vkUpdateDescriptorSets(GRenderer::Device, 1, &writeSet, 0, nullptr);
			}
			break;
		case ShaderType::EComputeShader:
		{
			ComputeShaderResourceSet* pCSet = GRendererResource::GetCurComputeShaderResourceSet();
			if (pCSet->SRV[uSlot] != pTexture)
			{
				pCSet->SRV[uSlot] = pTexture;

				writeSet.dstSet = pCSet->ResourceHeap.GetGResource();
				writeSet.dstBinding = (const UInt8)ComputeRootSignatureDesc::ESRV_BEGIN + uSlot;

				vkUpdateDescriptorSets(GRenderer::Device, 1, &writeSet, 0, nullptr);
			}
		}
		break;
		}
	}
	Void	GCommandList::BindUnorderResource(ShaderType eType, UInt32 uSlot, const  IGTexture* pTexture, UInt32 uSubresourceIdx)
	{
		ASSERT_IF_FAILED(pTexture != nullptr);

		GTexture* pRealTexture = (GTexture*)pTexture;
		//ResourceBarrier(pRealTexture, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);

		VkDescriptorImageInfo imgInfo;
		imgInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
		imgInfo.imageView = GRendererResource::GetImageView(pRealTexture);
		imgInfo.sampler = nullptr;

		VkWriteDescriptorSet writeSet = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
		writeSet.pImageInfo = &imgInfo;
		writeSet.descriptorCount = 1;
		writeSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
		writeSet.dstArrayElement = 0;

		switch (eType)
		{
		case MXVisual::ShaderType::EVertexShader:
		case MXVisual::ShaderType::EPixelShader:
			ASSERT_IF_FAILED("!Not Supported");
			break;
		case MXVisual::ShaderType::EComputeShader:
		{
			ComputeShaderResourceSet* pCSet = GRendererResource::GetCurComputeShaderResourceSet();
			if (pCSet->UAV[uSlot] != pRealTexture || pCSet->UAVSubresourceIndex[uSlot] != uSubresourceIdx)
			{
				pCSet->UAV[uSlot] = pRealTexture;
				pCSet->UAVSubresourceIndex[uSlot] = uSubresourceIdx;

				writeSet.dstSet = pCSet->ResourceHeap.GetGResource();
				writeSet.dstBinding = (const UInt8)ComputeRootSignatureDesc::EUAV_BEGIN + uSlot;

				vkUpdateDescriptorSets(GRenderer::Device, 1, &writeSet, 0, nullptr);
			}
		}
		break;
		}
	}
	Void	GCommandList::BindConstantBuffer(ShaderType eType, UInt32 uSlot, const IGBuffer* pBuffer)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);
		RasterizerShaderResourceSet*	pRSet = GRendererResource::GetCurRasterizerShaderResourceSet();

		GBuffer* pRealBuffer = (GBuffer*)pBuffer;
		//ResourceBarrier(pRealBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);

		VkDescriptorBufferInfo BufferInfo;
		BufferInfo.buffer = pRealBuffer->Real;
		BufferInfo.offset = 0;
		BufferInfo.range = pRealBuffer->Size;

		VkWriteDescriptorSet writeSet = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
		writeSet.pBufferInfo = &BufferInfo;
		writeSet.descriptorCount = 1;
		writeSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		writeSet.dstArrayElement = 0;

		switch (eType)
		{
		case ShaderType::EVertexShader:
			if (pRSet->VS_CBV[uSlot] != pRealBuffer)
			{
				pRSet->VS_CBV[uSlot] = pRealBuffer;

				writeSet.dstSet = pRSet->ResourceHeap.GetGResource();
				writeSet.dstBinding = (const UInt8)RasterizerRootSignatureDesc::EVS_CBV_BEGIN + uSlot;

				vkUpdateDescriptorSets(GRenderer::Device, 1, &writeSet, 0, nullptr);
			}
			break;
		case ShaderType::EPixelShader:
			if (pRSet->PS_CBV[uSlot] != pRealBuffer)
			{
				pRSet->PS_CBV[uSlot] = pRealBuffer;

				writeSet.dstSet = pRSet->ResourceHeap.GetGResource();
				writeSet.dstBinding = (const UInt8)RasterizerRootSignatureDesc::EPS_CBV_BEGIN + uSlot;

				vkUpdateDescriptorSets(GRenderer::Device, 1, &writeSet, 0, nullptr);
			}
			break;
		case ShaderType::EComputeShader:
		{
			ComputeShaderResourceSet*		pCSet = GRendererResource::GetCurComputeShaderResourceSet();
			if (pCSet->CBV[uSlot] != pRealBuffer)
			{
				pCSet->CBV[uSlot] = pRealBuffer;

				writeSet.dstSet = pCSet->ResourceHeap.GetGResource();
				writeSet.dstBinding = (const UInt8)ComputeRootSignatureDesc::ECBV_BEGIN + uSlot;

				vkUpdateDescriptorSets(GRenderer::Device, 1, &writeSet, 0, nullptr);
			}
		}
		break;
		}
	}
	Void	GCommandList::BindSampler(ShaderType eType, UInt32 uSlot, const IGSampler* pSampler)
	{
		ASSERT_IF_FAILED(pSampler != nullptr);
		RasterizerShaderResourceSet* pRSet = GRendererResource::GetCurRasterizerShaderResourceSet();

		const GSampler* pRealSampler = (const GSampler*)pSampler;
		VkDescriptorImageInfo imgInfo;
		imgInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
		imgInfo.imageView = nullptr;
		imgInfo.sampler = pRealSampler->Real;

		VkWriteDescriptorSet writeSet = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
		writeSet.pImageInfo = &imgInfo;
		writeSet.descriptorCount = 1;
		writeSet.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
		writeSet.dstArrayElement = 0;

		switch (eType)
		{
		case MXVisual::ShaderType::EVertexShader:
			if (pRSet->VS_Sampler[uSlot] != pSampler)
			{
				pRSet->VS_Sampler[uSlot] = pSampler;

				writeSet.dstSet = pRSet->ResourceHeap.GetGResource();
				writeSet.dstBinding = (const UInt8)RasterizerRootSignatureDesc::EVS_SAMPLER_BEGIN + uSlot;

				vkUpdateDescriptorSets(GRenderer::Device, 1, &writeSet, 0, nullptr);
			}
			break;
		case MXVisual::ShaderType::EPixelShader:
			if (pRSet->VS_Sampler[uSlot] != pSampler)
			{
				pRSet->VS_Sampler[uSlot] = pSampler;

				writeSet.dstSet = pRSet->ResourceHeap.GetGResource();
				writeSet.dstBinding = (const UInt8)RasterizerRootSignatureDesc::EPS_SAMPLER_BEGIN + uSlot;

				vkUpdateDescriptorSets(GRenderer::Device, 1, &writeSet, 0, nullptr);
			}
			break;
		case MXVisual::ShaderType::EComputeShader:
		{
			ComputeShaderResourceSet*		pCSet = GRendererResource::GetCurComputeShaderResourceSet();
			if (pCSet->Sampler[uSlot] != pSampler)
			{
				pCSet->Sampler[uSlot] = pSampler;

				writeSet.dstSet = pCSet->ResourceHeap.GetGResource();
				writeSet.dstBinding = (const UInt8)ComputeRootSignatureDesc::ESAMPLER_BEGIN + uSlot;

				vkUpdateDescriptorSets(GRenderer::Device, 1, &writeSet, 0, nullptr);
			}
		}
		break;
		}
	}

	Void	GCommandList::Draw(UInt32 uVertexCount, UInt32 uInstanceCount, Boolean bTriangleNotLine)
	{
		RasterizerShaderResourceSet* sets = GRendererResource::GetCurRasterizerShaderResourceSet();

		sets->BindGResource(this);

		/*
		vkCmdSetPrimitiveTopologyEXT(APICommandlist,
			bTriangleNotLine ? VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST : VK_PRIMITIVE_TOPOLOGY_LINE_LIST);
			*/
		vkCmdDraw(APICommandlist, uVertexCount, uInstanceCount, 0, 0);

		GRendererResource::NextRasterizerShaderResourceSet();
		GRendererResource::FrameStatistics.uDrawCall++;
	}
	Void	GCommandList::DrawIndex(UInt32 uIndexCount, UInt32 uIndexOffset, UInt32 uInstanceCount, Boolean bTriangleNotLine)
	{
		RasterizerShaderResourceSet* sets = GRendererResource::GetCurRasterizerShaderResourceSet();

		sets->BindGResource(this);
		/*
		vkCmdSetPrimitiveTopologyEXT(APICommandlist,
			bTriangleNotLine ? VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST : VK_PRIMITIVE_TOPOLOGY_LINE_LIST);
			*/
		vkCmdDrawIndexed(APICommandlist, uIndexCount, uInstanceCount, uIndexOffset, 0, 0);

		GRendererResource::NextRasterizerShaderResourceSet();
		GRendererResource::FrameStatistics.uDrawCall++;
	}

	Void	GCommandList::Dispatch(UInt32 X, UInt32 Y, UInt32 Z)
	{
		ComputeShaderResourceSet* sets = GRendererResource::GetCurComputeShaderResourceSet();

		sets->BindGResource(this);

		vkCmdDispatch(APICommandlist, X, Y, Z);
		GRendererResource::NextComputeShaderResourceSet();
	}

	Void	GCommandList::Flush()
	{
		if (bInRenderPass)
		{
			vkCmdEndRenderPass(APICommandlist);
			bInRenderPass = False;
		}
	}

	Void	GCommandList::CopyResource(IGTexture* pDest, IGTexture* pSrc)
	{
		ASSERT_IF_FAILED(pDest != nullptr && pSrc != nullptr);

		GTexture* pRealDest = (GTexture*)pDest;
		GTexture* pRealSrc = (GTexture*)pSrc;

		Array<VkImageCopy> copyInfos;

		for (UInt32 u = 0; u < pRealSrc->GetMipmapCount(); u++)
		{
			copyInfos.Add(
				VkImageCopy{
					{
						VK_IMAGE_ASPECT_COLOR_BIT | VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT,
						u, 0, pRealSrc->GetDepth()
					},
					{0,0,0},
					{
						VK_IMAGE_ASPECT_COLOR_BIT | VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT,
						u, 0, pRealDest->GetDepth()
					},
					{0,0,0},
					{pRealSrc->GetWidth(), pRealSrc->GetHeight(), pRealSrc->GetDepth()}
				}
			);
		}


		vkCmdCopyImage(APICommandlist,
			pRealSrc->GetGResource(), pRealSrc->State,
			pRealDest->GetGResource(), pRealDest->State,
			copyInfos.ElementCount(), copyInfos.GetRawData());
	}

	Boolean	GCommandList::Map(IGTexture* pTexture, UInt32 uSubResouce, Boolean bRead, MapRegion& region)
	{
		UInt32 uMipMap = uSubResouce / pTexture->GetDepth();
		UInt32 uWidth = pTexture->GetWidth();
		uWidth = Math::Max<SInt32>(1, uWidth >> uMipMap);
		region.uPitch = pTexture->GetWidth() * PixelFormatSize[pTexture->GetFormat()] / 8;

		if (VK_SUCCESS ==
			vkMapMemory(GRenderer::Device, ((GTexture*)pTexture)->Memory, 0, region.uPitch, 0, &region.pAddress))
		{
			return True;
		}
		return False;
	}
	Void	GCommandList::Unmap(IGTexture* pTexture, UInt32 uSubResouce)
	{
		vkUnmapMemory(GRenderer::Device, ((GTexture*)pTexture)->Memory);
	}

	Boolean	GCommandList::Map(IGBuffer* pBuffer, Boolean bRead, MapRegion& region)
	{
		region.uPitch = ((GBuffer*)pBuffer)->Size;

		if (VK_SUCCESS ==
			vkMapMemory(GRenderer::Device, ((GBuffer*)pBuffer)->Memory, 0, region.uPitch, 0, &region.pAddress))
		{
			return True;
		}
		return False;
	}
	Void	GCommandList::Unmap(IGBuffer* pBuffer)
	{
		vkUnmapMemory(GRenderer::Device, ((GBuffer*)pBuffer)->Memory);
	}

	Boolean	CheckUpdateBuffer(UInt32 uSize)
	{
		const UInt32 MAX_UPLOAD_BUFFER_SIZE = 4096 * 4096 * 32 / 8;
		if (uSize > MAX_UPLOAD_BUFFER_SIZE)
			return False;

		if (GRendererResource::UploadBuffer == nullptr)
		{
			MX_RELEASE_INTERFACE(GRendererResource::UploadBuffer);

			GRendererResource::UploadBuffer = (GBuffer*)GRendererResource::CreateBuffer(
				BufferType::EConstantBuffer, True, MAX_UPLOAD_BUFFER_SIZE);
		}

		return True;
	}

	Boolean	GCommandList::SubmitData(IGTexture* pTexture, UInt32 uSubResouce, UInt32 uSize, const Void* pData)
	{
		ASSERT_IF_FAILED(pTexture != nullptr);

		if (!CheckUpdateBuffer(uSize))
			return False;

		GTexture* pResource = (GTexture*)pTexture;
		Void* pMappedAddress = nullptr;
		if (VK_SUCCESS ==
			vkMapMemory(GRenderer::Device, GRendererResource::UploadBuffer->Memory, 0, uSize, 0, &pMappedAddress))
		{
			memcpy(pMappedAddress, pData, uSize);
			vkUnmapMemory(GRenderer::Device, GRendererResource::UploadBuffer->Memory);
		}
		ResourceBarrier(pResource, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

		VkBufferImageCopy copyInfo;
		copyInfo.bufferOffset = 0;
		copyInfo.bufferRowLength = 0;
		copyInfo.bufferImageHeight = 0;
		copyInfo.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		copyInfo.imageSubresource.baseArrayLayer = uSubResouce / pResource->GetDepth();
		copyInfo.imageSubresource.layerCount = 1;
		copyInfo.imageSubresource.mipLevel = uSubResouce % pResource->GetDepth();
		copyInfo.imageOffset.x = 0;
		copyInfo.imageOffset.y = 0;
		copyInfo.imageOffset.z = 0;
		copyInfo.imageExtent.width = pResource->GetWidth();
		copyInfo.imageExtent.height = pResource->GetHeight();
		copyInfo.imageExtent.depth = pResource->GetDepth();

		vkCmdCopyBufferToImage(APICommandlist,
			GRendererResource::UploadBuffer->Real,
			pResource->GetGResource(),
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			1, &copyInfo);


		pResource->State = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		GRenderer::Execute();
		return True;
	}
	Boolean GCommandList::SubmitData(IGBuffer* pBuffer, UInt32 uSize, const Void* pData)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);


		GBuffer* pResource = (GBuffer*)pBuffer;
		vkCmdUpdateBuffer(APICommandlist, pResource->Real, 0, uSize, pData);
		/*
		Void* pMappedAddress = nullptr;
		if (VK_SUCCESS ==
			vkMapMemory(GRenderer::Device, GRendererResource::UploadBuffer->Memory, 0, uSize, 0, &pMappedAddress))
		{
			memcpy(pMappedAddress, pData, uSize);
			vkUnmapMemory(GRenderer::Device, GRendererResource::UploadBuffer->Memory);
		}
		//ResourceBarrier(pResource, D3D12_RESOURCE_STATE_COPY_DEST);

		VkBufferCopy copyInfo;
		copyInfo.dstOffset = 0;
		copyInfo.srcOffset = 0;
		copyInfo.size = uSize;

		vkCmdCopyBuffer(APICommandlist, GRendererResource::UploadBuffer->Real,
			pResource->Real, 1, &copyInfo);

		GRenderer::Execute();
		*/
		return True;
	}
}