#include "GTexture.h"
#include "GRenderer.h"

namespace MXVisual
{
	GTexture::GTexture(UInt32 UniqueID)
		: m_uUniqueID(UniqueID)
		, m_Resource(nullptr)
		, m_pMapData(nullptr)
		, m_bTargetable(False)
		, m_bUnorderAccessable(False)
		, View(nullptr)
	{
		m_nRefCount = 1;
	}
	GTexture::~GTexture()
	{
		MX_DELETE(m_pMapData);
		if (View != nullptr)
		{
			vkDestroyImageView(GRenderer::Device, View, nullptr);
			View = nullptr;
		}
	}

	Boolean	GTexture::Init(const IGTexture::Description& desc)
	{
		Boolean bGenTarget = desc.bTargetable | desc.bGenerateMipMap;

		m_bTargetable = bGenTarget;
		m_bUnorderAccessable = desc.bUnorderAccessable;
		m_eType = desc.eType;
		m_eFormat = desc.eFormat;
		m_uWidth = desc.uWidth;
		m_uHeight = desc.uHeight;
		m_uDepth = desc.uDepth;
		m_uMipMapCount = Math::Min<UInt32>(desc.uMipLevel, Math::Min<UInt32>(log2(desc.uWidth), log2(desc.uHeight)));
		m_f4ClearValue = (m_eFormat == EPF_D24 || m_eFormat == EPF_D32) ?
			Float4(DEPTH_CLEAR_VALUE, DEPTH_CLEAR_VALUE, DEPTH_CLEAR_VALUE, DEPTH_CLEAR_VALUE) : desc.f4ClearValue;

		if (bGenTarget && desc.bDynamic)
		{
			Log::Output("GraphicsVulkan", "Texture With Target Cannot be Dyanmic");
			return False;
		}

		VkImageCreateInfo resource_desc = {};
		if (m_eFormat == EPF_D24 || m_eFormat == EPF_D32)
		{
			resource_desc.usage |= bGenTarget ? VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT : 0;
		}
		else
		{
			resource_desc.usage |= bGenTarget ? VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT : 0;
		}

		if (desc.bUnorderAccessable)
		{
			resource_desc.usage |= VK_IMAGE_USAGE_STORAGE_BIT;
		}
		resource_desc.usage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;

		resource_desc.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		resource_desc.pNext = NULL;
		resource_desc.format = PixelFormatMap[ResourceType::ERT_Resouce][m_eFormat];
		resource_desc.samples = VK_SAMPLE_COUNT_1_BIT;
		resource_desc.tiling = VK_IMAGE_TILING_OPTIMAL;
		resource_desc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		resource_desc.queueFamilyIndexCount = 0;
		resource_desc.pQueueFamilyIndices = NULL;
		resource_desc.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		resource_desc.mipLevels = m_uMipMapCount;
		resource_desc.flags = 0;

		switch (m_eType)
		{
		case TextureType::ETexture1D:
			resource_desc.imageType = VK_IMAGE_TYPE_1D;
			resource_desc.extent.width = m_uWidth;
			resource_desc.extent.height = 1;
			resource_desc.extent.depth = 1;
			resource_desc.arrayLayers = 1;
			break;
		case TextureType::ETexture2D:
			resource_desc.imageType = VK_IMAGE_TYPE_2D;
			resource_desc.extent.width = m_uWidth;
			resource_desc.extent.height = m_uHeight;
			resource_desc.extent.depth = 1;
			resource_desc.arrayLayers = 1;
			break;
		case TextureType::ETextureCube:
			resource_desc.imageType = VK_IMAGE_TYPE_2D;
			resource_desc.extent.width = m_uWidth;
			resource_desc.extent.height = m_uHeight;
			resource_desc.extent.depth = 1;
			resource_desc.arrayLayers = m_uDepth;
			break;
		case TextureType::ETexture3D:
			resource_desc.imageType = VK_IMAGE_TYPE_3D;
			resource_desc.extent.width = m_uWidth;
			resource_desc.extent.height = m_uHeight;
			resource_desc.extent.depth = m_uDepth;
			resource_desc.arrayLayers = 1;
			break;
		}
		VkMemoryAllocateInfo mem_alloc = {};
		mem_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		mem_alloc.pNext = NULL;
		mem_alloc.allocationSize = 0;
		mem_alloc.memoryTypeIndex = 0;

		VkImage image;
		if (VK_SUCCESS != vkCreateImage(GRenderer::Device, &resource_desc, NULL, &image))
		{
			return False;
		}
		m_Resource = image;

		VkMemoryRequirements require;
		vkGetImageMemoryRequirements(GRenderer::Device, image, &require);
		
		Memory = GRendererResource::AllocateMemory(require.size, True, require.memoryTypeBits,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

		if (VK_SUCCESS != vkBindImageMemory(GRenderer::Device, image, Memory, 0))
		{
			return False;
		}

		return True;
	}

	Boolean	GTexture::InitFromResource(VkImage Resource, const IGTexture::Description& desc)
	{
		Boolean bGenTarget = desc.bTargetable | desc.bGenerateMipMap;

		m_bTargetable = bGenTarget;
		m_eType = desc.eType;
		m_eFormat = desc.eFormat;
		m_uWidth = desc.uWidth;
		m_uHeight = desc.uHeight;
		m_uDepth = desc.uDepth;
		m_uMipMapCount = Math::Min<UInt32>(desc.uMipLevel, Math::Min<UInt32>(log2(desc.uWidth), log2(desc.uHeight)));

		if (bGenTarget && desc.bDynamic)
		{
			Log::Output("GraphicsVulkan", "Texture With Target Cannot be Dyanmic");
			return False;
		}
		State = VK_IMAGE_LAYOUT_GENERAL;
		m_Resource = Resource;
		return True;
	}

	Boolean	GTexture::Map(UInt32 uDepth, UInt32 uMipMap, MapRegion& region, Boolean bReadBack)
	{
		if (bReadBack)
		{
			ASSERT_IF_FAILED(!"TODO");
		}
		else
		{
			MX_DELETE(m_pMapData);
			m_pMapData = new MappedSubResource;
			m_pMapData->uDepth = uDepth;
			m_pMapData->uMip = uMipMap;

			UInt32 uWidth = m_uWidth;
			UInt32 uHeight = m_uHeight;
			uWidth = Math::Max<SInt32>(1, uWidth >> uMipMap);
			uHeight = Math::Max<SInt32>(1, uHeight >> uMipMap);

			region.uPitch = PixelFormatSize[GetFormat()] * uWidth;
			m_pMapData->uSize = region.uPitch * uHeight;
			m_pMapData->pCPUBuffer = new UInt8[m_pMapData->uSize];
			region.pAddress = m_pMapData->pCPUBuffer;
			return True;
		}
		return False;
	}
	Void	GTexture::Unmap()
	{
		if (m_pMapData != nullptr)
		{
			UInt32 uWidth = m_uWidth;
			UInt32 uHeight = m_uHeight;
			uWidth = Math::Max<SInt32>(1, uWidth >> m_pMapData->uMip);
			uHeight = Math::Max<SInt32>(1, uHeight >> m_pMapData->uMip);

			GRenderer::CommandList()->SubmitData(this,
				m_pMapData->uDepth + m_pMapData->uMip * m_uDepth,
				m_pMapData->uSize, m_pMapData->pCPUBuffer);


			delete m_pMapData;
			m_pMapData = nullptr;
		}
	}
}