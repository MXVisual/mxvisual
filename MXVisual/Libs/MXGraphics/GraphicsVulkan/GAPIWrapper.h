#pragma once
#ifndef _G_API_WRAPPER_
#define _G_API_WRAPPER_
#include "MXPlatform.h"
#include "MXInterface.h"
#include "vulkan/vulkan.h"

namespace MXVisual
{
	class IGTexture;

	template<Boolean ForRasterizer>
	class DescriptorHeap
	{
	public:
		DescriptorHeap();
		~DescriptorHeap();

		VkDescriptorSet 	GetGResource() const { return m_Set; }
	protected:
		VkDescriptorSet		m_Set;
	};

	enum class RasterizerRootSignatureDesc : UInt8
	{
		EVS_CBV_BEGIN = 0,
		EVS_CBV_COUNT = 8,

		EVS_SRV_BEGIN = EVS_CBV_BEGIN + EVS_CBV_COUNT,
		EVS_SRV_COUNT = 8,

		EVS_SAMPLER_BEGIN = EVS_SRV_BEGIN + EVS_SRV_COUNT,
		EVS_SAMPLER_COUNT = 4,

		EPS_CBV_BEGIN = EVS_SAMPLER_BEGIN + EVS_SAMPLER_COUNT,
		EPS_CBV_COUNT = 8,

		EPS_SRV_BEGIN = EPS_CBV_BEGIN + EPS_CBV_COUNT,
		EPS_SRV_COUNT = 32,

		EPS_SAMPLER_BEGIN = EPS_SRV_BEGIN + EPS_SRV_COUNT,
		EPS_SAMPLER_COUNT = 8,
	};
	enum class ComputeRootSignatureDesc : UInt8
	{
		ECBV_BEGIN = 0,
		ECBV_COUNT = 8,

		ESRV_BEGIN = ECBV_BEGIN + ECBV_COUNT,
		ESRV_COUNT = 8,

		EUAV_BEGIN = ESRV_BEGIN + ESRV_COUNT,
		EUAV_COUNT = 16,

		ESAMPLER_BEGIN = EUAV_BEGIN + EUAV_COUNT,
		ESAMPLER_COUNT = 4,
	};
	struct FrameBuffer
	{
		const IGTexture*	RTV[8];
		UInt32				RTVSubresourceIndex[8];
		const IGTexture*	DSV;
		UInt32				DSVSubresourceIndex;

		FrameBuffer()
		{
			memset(RTV, 0, sizeof(RTV));
			memset(RTVSubresourceIndex, 0, sizeof(RTVSubresourceIndex));
			DSV = nullptr;
			DSVSubresourceIndex;
		}
	};

	struct RasterizerShaderResourceSet
	{
		const IGBuffer*		VS_CBV[(const UInt8)RasterizerRootSignatureDesc::EVS_CBV_COUNT];
		const IGTexture*	VS_SRV[(const UInt8)RasterizerRootSignatureDesc::EVS_SRV_COUNT];
		const IGSampler*	VS_Sampler[(const UInt8)RasterizerRootSignatureDesc::EVS_SAMPLER_COUNT];

		const IGBuffer*		PS_CBV[(const UInt8)RasterizerRootSignatureDesc::EPS_CBV_COUNT];
		const IGTexture*	PS_SRV[(const UInt8)RasterizerRootSignatureDesc::EPS_SRV_COUNT];
		const IGSampler*	PS_Sampler[(const UInt8)RasterizerRootSignatureDesc::EPS_SAMPLER_COUNT];

		DescriptorHeap<True>	ResourceHeap;

		RasterizerShaderResourceSet()
		{
			memset(VS_CBV, 0, sizeof(VS_CBV));
			memset(VS_SRV, 0, sizeof(VS_SRV));
			memset(VS_Sampler, 0, sizeof(VS_Sampler));
			memset(PS_CBV, 0, sizeof(PS_CBV));
			memset(PS_SRV, 0, sizeof(PS_SRV));
			memset(PS_Sampler, 0, sizeof(PS_Sampler));
		}
		Void BindGResource(IGCommandList* pCommandList);
	};

	struct ComputeShaderResourceSet
	{
		const IGBuffer*		CBV[(const UInt8)ComputeRootSignatureDesc::ECBV_COUNT] = { nullptr, };
		const IGTexture*	SRV[(const UInt8)ComputeRootSignatureDesc::ESRV_COUNT] = { nullptr, };
		const IGTexture*	UAV[(const UInt8)ComputeRootSignatureDesc::EUAV_COUNT] = { nullptr, };
		UInt32				UAVSubresourceIndex[(const UInt8)ComputeRootSignatureDesc::EUAV_COUNT] = { 0, };
		const IGSampler*	Sampler[(const UInt8)ComputeRootSignatureDesc::ESAMPLER_COUNT];

		DescriptorHeap<False>	ResourceHeap;

		ComputeShaderResourceSet()
		{
			memset(CBV, 0, sizeof(CBV));
			memset(SRV, 0, sizeof(SRV));
			memset(UAV, 0, sizeof(UAV));
			memset(UAVSubresourceIndex, 0, sizeof(UAVSubresourceIndex));
			memset(Sampler, 0, sizeof(Sampler));
		}
		Void BindGResource(IGCommandList* pCommandList);
	};

	struct VertexInputDescription
	{
		UInt32								NumElements;
		VkVertexInputAttributeDescription*	pInputElementDescs;
	};

	struct BlendStateCreateInfo : public VkPipelineColorBlendStateCreateInfo
	{
		VkPipelineColorBlendAttachmentState RenderTarget[8];

		BlendStateCreateInfo()
		{
			pAttachments = RenderTarget;
		}
		BlendStateCreateInfo(const BlendStateCreateInfo& other)
		{
			memcpy(this, &other, sizeof(BlendStateCreateInfo));
			pAttachments = RenderTarget;
		}
	};
}

#endif