#pragma once
#ifndef _ROOTSIGNATURE_
#define _ROOTSIGNATURE_
#include "MXPlatform.h"
#include <d3d12.h>

namespace MXVisual
{
	struct RootSignature : public D3D12_ROOT_SIGNATURE_DESC
	{
		Void Init(D3D12_ROOT_SIGNATURE_FLAGS flags, UInt32 uCount, D3D12_ROOT_PARAMETER* pParams, UInt32 uStaticSmplersCount, D3D12_STATIC_SAMPLER_DESC* pSamplers)
		{
			Flags = flags;
			NumParameters = uCount;
			pParameters = pParams;
			NumStaticSamplers = uStaticSmplersCount;
			pStaticSamplers = pSamplers;
		}
	};

	struct RootParam : public D3D12_ROOT_PARAMETER
	{
		Void InitAsDescriptorTalbe(D3D12_SHADER_VISIBILITY visibility, UInt32 uCount, D3D12_DESCRIPTOR_RANGE* pRange)
		{
			ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
			DescriptorTable.NumDescriptorRanges = uCount;
			DescriptorTable.pDescriptorRanges = pRange;
			ShaderVisibility = visibility;
		}
		Void InitAs32BitConstants(D3D12_SHADER_VISIBILITY visibility, UInt32 uRegisterIndex)
		{
			ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
			Constants.Num32BitValues = 0;
			Constants.RegisterSpace = 0;
			Constants.ShaderRegister = uRegisterIndex;
			ShaderVisibility = visibility;
		}
		Void InitAsCBV(D3D12_SHADER_VISIBILITY visibility, UInt32 uRegisterIndex)
		{
			ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
			Descriptor.RegisterSpace = 0;
			Descriptor.ShaderRegister = uRegisterIndex;
			ShaderVisibility = visibility;
		}
		Void InitAsSRV(D3D12_SHADER_VISIBILITY visibility, UInt32 uRegisterIndex)
		{
			ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
			Descriptor.RegisterSpace = 0;
			Descriptor.ShaderRegister = uRegisterIndex;
			ShaderVisibility = visibility;
		}
		Void InitAsUAV(D3D12_SHADER_VISIBILITY visibility, UInt32 uRegisterIndex)
		{
			ParameterType = D3D12_ROOT_PARAMETER_TYPE_UAV;
			Descriptor.RegisterSpace = 0;
			Descriptor.ShaderRegister = uRegisterIndex;
			ShaderVisibility = visibility;
		}
	};

	struct DescriptorRange : public D3D12_DESCRIPTOR_RANGE
	{
		Void InitAs(D3D12_DESCRIPTOR_RANGE_TYPE type, UInt32 uBaseShaderRegister, UInt32 uCount, UInt32 uOffset)
		{
			NumDescriptors = uCount;
			BaseShaderRegister = uBaseShaderRegister;
			OffsetInDescriptorsFromTableStart = uOffset;
			RangeType = type;
			RegisterSpace = 0;
		}
	};
}

#endif