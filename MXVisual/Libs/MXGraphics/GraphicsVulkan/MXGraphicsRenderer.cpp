#include "GRenderer.h"
#include "MXConstant.h"
#include "MXCommon.h"

#include "GShaderManager.h"

#include "GViewFrustum.h"
#include "GTexture.h"
#include "GPrimitive.h"
#include "GMaterial.h"
#include "GCommandList.h"

#if defined(DEBUG)
VkDebugReportCallbackEXT	DebugReport = nullptr;
PFN_vkCmdBeginDebugUtilsLabelEXT	BeginEvent = nullptr;
PFN_vkCmdEndDebugUtilsLabelEXT		EndEvent = nullptr;
//PFN_vkSetDebugUtilsObjectNameEXT	DebugNameObject = nullptr;
#endif

namespace MXVisual
{
	Boolean	GPipeline::InitForRender(
		PrimitiveIAFactory			eIAFactory,
		TopologyType				eTopology,
		const GRenderPass&				RP,
		const VkPipelineRasterizationStateCreateInfo&    RS,
		const VkPipelineDepthStencilStateCreateInfo&     DS,
		const BlendStateCreateInfo&		BS,
		const ShaderCompiledResult*	pVS,
		const ShaderCompiledResult*	pPS)
	{
		VkVertexInputBindingDescription bindingDesc_Instance[] = {
			{0, 0, VK_VERTEX_INPUT_RATE_VERTEX},
			{1, sizeof(Float4), VK_VERTEX_INPUT_RATE_INSTANCE},
		};

		VkVertexInputBindingDescription bindingDesc[] = {
			{0, 0, VK_VERTEX_INPUT_RATE_VERTEX},
		};

		VkPipelineVertexInputStateCreateInfo viInfo = {
			VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO };
		switch (eIAFactory)
		{
		case PrimitiveIAFactory::EStatic:
			viInfo.pVertexAttributeDescriptions = GInputDescStatic;
			viInfo.vertexAttributeDescriptionCount = 5;

			bindingDesc[0].stride = sizeof(GVertexRigid);
			viInfo.pVertexBindingDescriptions = bindingDesc;
			viInfo.vertexBindingDescriptionCount = ARRAYSIZE(bindingDesc);
			break;
		case PrimitiveIAFactory::EStatic_Instance:
			viInfo.pVertexAttributeDescriptions = GInputDescStatic_Instance;
			viInfo.vertexAttributeDescriptionCount = 5;

			bindingDesc[0].stride = sizeof(GVertexRigid);
			viInfo.pVertexBindingDescriptions = bindingDesc_Instance;
			viInfo.vertexBindingDescriptionCount = ARRAYSIZE(bindingDesc_Instance);
			break;
		case PrimitiveIAFactory::ESkin:
			viInfo.pVertexAttributeDescriptions = GInputDescSkin;
			viInfo.vertexAttributeDescriptionCount = 7;

			bindingDesc[0].stride = sizeof(GVertexSkin);
			viInfo.pVertexBindingDescriptions = bindingDesc;
			viInfo.vertexBindingDescriptionCount = ARRAYSIZE(bindingDesc);
			break;
		case PrimitiveIAFactory::EStatic2D:
			viInfo.pVertexAttributeDescriptions = GInputDesc2D;
			viInfo.vertexAttributeDescriptionCount = 3;

			bindingDesc[0].stride = sizeof(GVertexRigid2D);
			viInfo.pVertexBindingDescriptions = bindingDesc;
			viInfo.vertexBindingDescriptionCount = ARRAYSIZE(bindingDesc);
			break;
		case PrimitiveIAFactory::ECount:
			viInfo.pVertexAttributeDescriptions = nullptr;
			viInfo.vertexAttributeDescriptionCount = 0;
			viInfo.pVertexBindingDescriptions = nullptr;
			viInfo.vertexBindingDescriptionCount = 0;
			break;
		}

		VkPipelineInputAssemblyStateCreateInfo iaInfo = {
			VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO };
		switch (eTopology)
		{
		case TopologyType::ETriangle:
			iaInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
			break;
		case TopologyType::ELine:
			iaInfo.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
			break;
		case TopologyType::EPoint:
			iaInfo.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
			break;
		}
		VkShaderModule VSObject = nullptr;
		if (pVS != nullptr)
		{
			VSObject = ((GShaderManager*)GRendererResource::ShaderManager)->GetShaderObject(*pVS);
		}
		else
		{
			VSObject = ((GShaderManager*)GRendererResource::ShaderManager)->GetShaderObject(
				GRendererResource::DefaultVertexShader[(const UInt32)eIAFactory][(const UInt32)MaterialDomain::EWorldSpace]);
		}

		VkGraphicsPipelineCreateInfo pso_desc = { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO };
		pso_desc.renderPass = GRendererResource::GetGResource(&RP);
		pso_desc.stageCount = 2;
		pso_desc.subpass = 0;

		VkPipelineViewportStateCreateInfo vpInfo = { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO };
		vpInfo.viewportCount = 1;
		vpInfo.scissorCount = 1;
		pso_desc.pViewportState = &vpInfo;

		VkPipelineMultisampleStateCreateInfo msInfo = { VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO };
		msInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
		pso_desc.pMultisampleState = &msInfo;

		pso_desc.layout = GRendererResource::RasterizerRootSignature;

		ASSERT_IF_FAILED(pPS != nullptr);
		if (pPS == nullptr)return False;
		VkShaderModule PSObject = ((GShaderManager*)GRendererResource::ShaderManager)->GetShaderObject(*pPS);

		VkPipelineShaderStageCreateInfo stageInfo[] = {
			{
				VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				nullptr, 0,
				VK_SHADER_STAGE_VERTEX_BIT,
				VSObject,
				"VSMain",
				nullptr,
			},
			{
				VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				nullptr, 0,
				VK_SHADER_STAGE_FRAGMENT_BIT,
				PSObject,
				"PSMain",
				nullptr,
			},
		};
		pso_desc.pStages = stageInfo;
		pso_desc.stageCount = ARRAY_ELEMENT_COUNT(stageInfo);

		pso_desc.pVertexInputState = &viInfo;
		pso_desc.pInputAssemblyState = &iaInfo;
		pso_desc.pRasterizationState = &RS;
		pso_desc.pDepthStencilState = &DS;
		pso_desc.pColorBlendState = &BS;

		VkDynamicState state[] = {
			VK_DYNAMIC_STATE_VIEWPORT,
			VK_DYNAMIC_STATE_SCISSOR,
			//VK_DYNAMIC_STATE_PRIMITIVE_TOPOLOGY_EXT
		};
		VkPipelineDynamicStateCreateInfo dyncState = { VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO };
		dyncState.dynamicStateCount = ARRAY_ELEMENT_COUNT(state);
		dyncState.pDynamicStates = state;
		pso_desc.pDynamicState = &dyncState;

		if (VK_SUCCESS !=
			vkCreateGraphicsPipelines(GRenderer::Device, nullptr, 1, &pso_desc, nullptr, &PSO))
		{
			return False;
		}
		return True;
	}

	Boolean GPipeline::InitForCompute(
		const ShaderCompiledResult*		pCS)
	{
		ASSERT_IF_FAILED(pCS != nullptr);
		if (pCS == nullptr)return False;

		VkComputePipelineCreateInfo cpso = { VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO };

		VkShaderModule CSObject = ((GShaderManager*)GRendererResource::ShaderManager)->GetShaderObject(*pCS);
		cpso.stage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		cpso.stage.stage = VK_SHADER_STAGE_COMPUTE_BIT;
		cpso.stage.module = CSObject;
		cpso.stage.pName = "CSMain";

		cpso.layout = GRendererResource::ComputeRootSignature;

		if (VK_SUCCESS !=
			vkCreateComputePipelines(GRenderer::Device, nullptr, 1, &cpso, nullptr, &PSO))
		{
			return False;
		}
		return True;
	}

	Void	GPipeline::Release()
	{
		if (PSO != nullptr)
		{
			vkDestroyPipeline(GRenderer::Device, PSO, nullptr);
			PSO = nullptr;
		}
	}

#if DEBUG
	VkBool32 FDebugReportCallback(
		VkDebugReportFlagsEXT                       flags,
		VkDebugReportObjectTypeEXT                  objectType,
		uint64_t                                    object,
		size_t                                      location,
		int32_t                                     messageCode,
		const char* pLayerPrefix,
		const char* pMessage,
		void* pUserData)
	{
		Log::Output("GraphicsVulkan", pMessage);
		return VK_FALSE;
	}
#endif

	void CheckSupportedExt(
		const Char* RequiredExt[],
		unsigned RequiredExtCount,
		VkExtensionProperties SupportedExt[],
		unsigned SupportedExtCount,
		Array<const Char*>& SupportedExtResult)
	{
		SupportedExtResult.Clear();
		Array<UInt32> RequireExtIndexArray;
		for (UInt32 i = 0; i < RequiredExtCount; i++)
		{
			RequireExtIndexArray.Add(i);
		}

		for (int i = 0; i < SupportedExtCount; i++)
		{
			for (int j = 0; j < RequireExtIndexArray.ElementCount(); j++)
			{
				if (_stricmp(SupportedExt[i].extensionName, RequiredExt[RequireExtIndexArray[j]]) == 0)
				{
					SupportedExtResult.Add(RequiredExt[RequireExtIndexArray[j]]);
					RequireExtIndexArray.Erase(j, 1);
					break;
				}
			}
		}
	}

	namespace GRenderer
	{
		VkInstance			Instance = nullptr;

		VkPhysicalDevice	SelectedAdapter = nullptr;
		VkDevice			Device = nullptr;

		UInt32				RenderQueueFamilyIndex = -1;
		UInt32				ComputeQueueFamilyIndex = -1;
		UInt32				PresentQueueFamilyIndex = -1;

		VkQueue				RenderQueue = nullptr;
		VkQueue				ComputeQueue = nullptr;
		VkQueue				PresentQueue = nullptr;

		VkCommandPool		CommandPool = nullptr;

		VkFence				SyncGPUFence = nullptr;
		VkSemaphore			Semaphore = nullptr;

		UInt32				CommandlistOffset = 0;
		GCommandList*		Commandlist[2] = { nullptr, };

		Boolean				SupportRayTracing = False;


		Void	SyncGPU()
		{
			vkWaitForFences(Device, 1, &SyncGPUFence, VK_TRUE, UINT64_MAX);
			vkResetFences(Device, 1, &SyncGPUFence);
		}
		Boolean InitAPI(const Char* szAdapterName)
		{
			const char* InstanceRequireExt[] = {
#if DEBUG
				VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
				VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
#endif
				VK_KHR_SURFACE_EXTENSION_NAME,
				VK_KHR_WIN32_SURFACE_EXTENSION_NAME
			};

			const char* RequireLayer[] = {
				"VK_LAYER_KHRONOS_validation"
			};
			Array<const Char*> SupportedExt;

			UInt32					ExtensionCount = 0;
			VkExtensionProperties*	Extensions = nullptr;
			vkEnumerateInstanceExtensionProperties(nullptr, &ExtensionCount, nullptr);
			Extensions = new VkExtensionProperties[ExtensionCount];
			vkEnumerateInstanceExtensionProperties(nullptr, &ExtensionCount, Extensions);

			CheckSupportedExt(InstanceRequireExt, ARRAY_ELEMENT_COUNT(InstanceRequireExt),
				Extensions, ExtensionCount, SupportedExt);

			delete[] Extensions;

			VkInstanceCreateInfo InstanceInfo;
			InstanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
			InstanceInfo.pNext = nullptr;
			InstanceInfo.flags = 0;
			InstanceInfo.pApplicationInfo = nullptr;
			InstanceInfo.ppEnabledExtensionNames = SupportedExt.GetRawData();
			InstanceInfo.enabledExtensionCount = SupportedExt.ElementCount();
			InstanceInfo.ppEnabledLayerNames = RequireLayer;
			InstanceInfo.enabledLayerCount = ARRAYSIZE(RequireLayer);
			VkResult result = vkCreateInstance(&InstanceInfo, nullptr, &Instance);

#if DEBUG
			VkDebugReportCallbackCreateInfoEXT debugInfo;
			memset(&debugInfo, 0, sizeof(debugInfo));
			debugInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
			debugInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
			debugInfo.pfnCallback = FDebugReportCallback;

			PFN_vkCreateDebugReportCallbackEXT FCreateDebugReport = nullptr;
			FCreateDebugReport = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(Instance, "vkCreateDebugReportCallbackEXT");
			if (VK_SUCCESS != FCreateDebugReport(Instance, &debugInfo, nullptr, &DebugReport))
			{
				return False;
			}
#endif

			uint32_t uAdapterCount = 0;
			if (VK_SUCCESS != vkEnumeratePhysicalDevices(Instance, &uAdapterCount, nullptr))
			{
				return False;
			}

			VkPhysicalDevice* pAdapters = nullptr;
			pAdapters = new VkPhysicalDevice[uAdapterCount];
			if (VK_SUCCESS != vkEnumeratePhysicalDevices(Instance, &uAdapterCount, pAdapters))
			{
				return False;
			}

			for (int i = 0; i < uAdapterCount; i++)
			{
				VkPhysicalDeviceProperties property;
				vkGetPhysicalDeviceProperties(pAdapters[i], &property);

				if (property.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
				{
					SelectedAdapter = pAdapters[i];
				}
			}
			delete[] pAdapters;

			unsigned queueFamilyCount = 0;
			vkGetPhysicalDeviceQueueFamilyProperties(SelectedAdapter, &queueFamilyCount, nullptr);
			VkQueueFamilyProperties* queueFamilies = new VkQueueFamilyProperties[queueFamilyCount];
			vkGetPhysicalDeviceQueueFamilyProperties(SelectedAdapter, &queueFamilyCount, queueFamilies);

			bool bFoundRender = false;
			bool bFoundCompute = false;
			bool bFoundPresent = false;
			for (int i = 0; i < queueFamilyCount; i++)
			{
				if (!bFoundRender)
				{
					bFoundRender |= (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) > 0;
					RenderQueueFamilyIndex = bFoundRender ? i : RenderQueueFamilyIndex;
				}

				if (!bFoundCompute)
				{
					bFoundCompute |= (queueFamilies[i].queueFlags & VK_QUEUE_COMPUTE_BIT) > 0;
					ComputeQueueFamilyIndex = bFoundCompute ? i : ComputeQueueFamilyIndex;
				}

				if (!bFoundPresent)
				{
					VkBool32 bSupportPresent = false;
#if PLATFORM == PLATFORM_WINDOWS
					bSupportPresent = vkGetPhysicalDeviceWin32PresentationSupportKHR(SelectedAdapter, i);
#else
					//bSupportPresent = vkGetPhysicalDeviceWin32PresentationSupportKHR(SelectedAdapter, i);
#endif
					bFoundPresent |= bSupportPresent;
					PresentQueueFamilyIndex = bFoundPresent ? i : PresentQueueFamilyIndex;
				}
			}
			delete[] queueFamilies;

			if (RenderQueueFamilyIndex != ComputeQueueFamilyIndex ||
				RenderQueueFamilyIndex != PresentQueueFamilyIndex)
			{
				Log::Output("GraphicsVulkan", "Render|Compute|Present Not Supported On The Same Queue");
				return False;
			}

			float QueuePriority = 0.0f;
			VkDeviceQueueCreateInfo queueInfo = {
				VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO, nullptr, 0,
				RenderQueueFamilyIndex, 1, &QueuePriority
			};

			SupportedExt.Clear();
			ExtensionCount = 0;
			Extensions = nullptr;

			const char* DeviceRequiredExt[] = {
				VK_KHR_SWAPCHAIN_EXTENSION_NAME,
			};

			vkEnumerateDeviceExtensionProperties(SelectedAdapter, nullptr, &ExtensionCount, nullptr);
			Extensions = new VkExtensionProperties[ExtensionCount];
			vkEnumerateDeviceExtensionProperties(SelectedAdapter, nullptr, &ExtensionCount, Extensions);

			CheckSupportedExt(DeviceRequiredExt, ARRAYSIZE(DeviceRequiredExt),
				Extensions, ExtensionCount, SupportedExt);

			delete[] Extensions;

			VkDeviceCreateInfo deviceInfo;
			memset(&deviceInfo, 0, sizeof(deviceInfo));
			deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
			deviceInfo.flags = 0;
			deviceInfo.pNext = nullptr;
			deviceInfo.pEnabledFeatures = nullptr;
			deviceInfo.ppEnabledExtensionNames = SupportedExt.GetRawData();
			deviceInfo.enabledExtensionCount = SupportedExt.ElementCount();
			deviceInfo.pQueueCreateInfos = &queueInfo;
			deviceInfo.queueCreateInfoCount = 1;
			if (VK_SUCCESS != vkCreateDevice(SelectedAdapter, &deviceInfo, nullptr, &Device))
			{
				return False;
			}

			vkGetDeviceQueue(Device, RenderQueueFamilyIndex, 0, &RenderQueue);
			vkGetDeviceQueue(Device, ComputeQueueFamilyIndex, 0, &ComputeQueue);
			vkGetDeviceQueue(Device, PresentQueueFamilyIndex, 0, &PresentQueue);

#if DEBUG
			BeginEvent = (PFN_vkCmdBeginDebugUtilsLabelEXT)vkGetDeviceProcAddr(Device, "vkCmdBeginDebugUtilsLabelEXT");
			EndEvent = (PFN_vkCmdEndDebugUtilsLabelEXT)vkGetDeviceProcAddr(Device, "vkCmdEndDebugUtilsLabelEXT");
#endif


			VkFenceCreateInfo fenceInfo;
			memset(&fenceInfo, 0, sizeof(fenceInfo));
			fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
			if (VK_SUCCESS != vkCreateFence(Device, &fenceInfo, nullptr, &SyncGPUFence))
			{
				return False;
			}

			VkCommandPoolCreateInfo cpInfo = { VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO };
			cpInfo.queueFamilyIndex = RenderQueueFamilyIndex;
			cpInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
			vkCreateCommandPool(Device, &cpInfo, nullptr, &CommandPool);

			VkCommandBuffer CL[2] = { nullptr };
			VkCommandBufferAllocateInfo cbInfo;
			memset(&cbInfo, 0, sizeof(cbInfo));
			cbInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
			cbInfo.commandBufferCount = 2;
			cbInfo.commandPool = CommandPool;
			cbInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
			vkAllocateCommandBuffers(Device, &cbInfo, CL);

			Commandlist[0] = new GCommandList(CommandPool, CL[0]);
			Commandlist[1] = new GCommandList(CommandPool, CL[1]);

			VkCommandBuffer BeginCL = ((GCommandList*)GRenderer::CommandList())->APICommandlist;
			VkCommandBufferBeginInfo beginInfo;
			memset(&beginInfo, 0, sizeof(beginInfo));
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
			vkBeginCommandBuffer(BeginCL, &beginInfo);

			return True;
		}

		Void	ReleaseAPI()
		{
			delete GRendererResource::ShaderManager;


			MX_DELETE(Commandlist[0]);
			MX_DELETE(Commandlist[1]);
			/*
			MX_RELEASE_INTERFACE(HardwarePipeline);
			MX_RELEASE_INTERFACE(SyncGPUFence);
			MX_RELEASE_INTERFACE(Device);
			*/
		}

		IGCommandList*	CommandList()
		{
			return Commandlist[CommandlistOffset];
		}

		Void			Execute()
		{
			GRenderer::CommandList()->Flush();

			VkCommandBuffer CL = ((GCommandList*)GRenderer::CommandList())->APICommandlist;
			if (VK_SUCCESS == vkEndCommandBuffer(CL))
			{
				VkSubmitInfo sInfo;
				memset(&sInfo, 0, sizeof(sInfo));
				sInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
				sInfo.commandBufferCount = 1;
				sInfo.pCommandBuffers = &CL;
				vkQueueSubmit(GRenderer::RenderQueue, 1, &sInfo, GRenderer::SyncGPUFence);
			}

			SyncGPU();
			CommandlistOffset = (CommandlistOffset + 1) % 2;

			CL = ((GCommandList*)GRenderer::CommandList())->APICommandlist;
			VkCommandBufferBeginInfo beginInfo;
			memset(&beginInfo, 0, sizeof(beginInfo));
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
			vkBeginCommandBuffer(CL, &beginInfo);

			GRendererResource::ResetFrameBuffer();
			GRendererResource::ResetShaderResourceSet();
		}
	}

#if defined(DEBUG)
	Void DebugPushEvent(const Char* eventName)
	{
		VkDebugUtilsLabelEXT label = {
			VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT,
			nullptr,
			eventName,
			{1.0f, 1.0f, 1.0f, 1.0f}
		};

		BeginEvent(((GCommandList*)GRenderer::CommandList())->APICommandlist, &label);
	}
	Void DebugPopEvent()
	{
		EndEvent(((GCommandList*)GRenderer::CommandList())->APICommandlist);
	}
#endif
}