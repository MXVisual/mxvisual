#pragma once
#ifndef _G_RENDERER_
#define _G_RENDERER_
#include "GCommonRenderer.h"
#if PLATFORM == PLATFORM_WINDOWS
#define VK_USE_PLATFORM_WIN32_KHR
#endif
#include "vulkan/vulkan.h"

#include "MXThread.h"
#include "MXFile.h"
#include "MXLog.h"
#include "MXMath.h"
#include "GAPIWrapper.h"

#define REVERT_Z 1

#define DEPTH_ALWAYS VK_COMPARE_OP_ALWAYS
#define DEPTH_EQUAL VK_COMPARE_OP_EQUAL
#if REVERT_Z
#define DEPTH_NEARER VK_COMPARE_OP_GREATER_OR_EQUAL
#define DEPTH_FARTHER VK_COMPARE_OP_LESS
#define DEPTH_CLEAR_VALUE 0.0f
#else
#define DEPTH_NEARER VK_COMPARE_OP_LESS_OR_EQUAL
#define DEPTH_FARTHER VK_COMPARE_OP_GREATER
#define DEPTH_CLEAR_VALUE 1.0f
#endif

#if defined(DEBUG)
#define NAME_API_OBJECT(obj, name)	(obj)->SetName(L#name)
#else
#define NAME_API_OBJECT(obj, name)
#endif

namespace MXVisual
{
	const UInt32 Max_RTV_Count = 8;
	const UInt32 Max_SRV_Count = 32;

	enum ResourceType
	{
		ERT_Resouce,
		ERT_ShaderResourceView,
		ERT_RenderTargetView,
		ERT_DepthStencilTargetView,
		ERT_Count
	};

	extern VkFormat PixelFormatMap[ERT_Count][EPF_Count + 1];
	extern UInt8	PixelFormatSize[EPF_Count + 1];

	extern VkVertexInputAttributeDescription GInputDesc2D[3];
	extern VkVertexInputAttributeDescription GInputDescStatic[5];
	extern VkVertexInputAttributeDescription GInputDescStatic_Instance[9];
	extern VkVertexInputAttributeDescription GInputDescSkin[7];
	extern VkVertexInputAttributeDescription GInputDescSkin_Instance[11];

	struct GPipeline
	{
		Boolean		PSOForRasterizer = True;
		VkPipeline	PSO = nullptr;
		GRenderPass* RefRenderPass;

		Boolean	InitForRender(
			PrimitiveIAFactory			eIAFactory,
			TopologyType				eTopology,
			const GRenderPass&				RP,
			const VkPipelineRasterizationStateCreateInfo&    RS,
			const VkPipelineDepthStencilStateCreateInfo&     DS,
			const BlendStateCreateInfo&		BS,
			const ShaderCompiledResult* VS,
			const ShaderCompiledResult* PS
		);

		Boolean InitForCompute(
			const ShaderCompiledResult* CS
		);

		Void	Release();
	};
	struct GSampler : public IGSampler
	{
		VkSampler	Real;

		UInt32 GetUniqueID() const { return -1; }
	};

	class GShaderManager;

	namespace GRenderer
	{
		extern VkInstance		Instance;

		extern VkPhysicalDevice	SelectedAdapter;
		extern VkDevice			Device;

		extern UInt32			RenderQueueFamilyIndex;
		extern UInt32			ComputeQueueFamilyIndex;
		extern UInt32			PresentQueueFamilyIndex;

		extern VkQueue			RenderQueue;
		extern VkQueue			ComputeQueue;
		extern VkQueue			PresentQueue;

		extern VkCommandPool	CommandPool;

		extern VkFence			SyncGPUFence;
		extern VkSemaphore		Semaphore;

		Void SyncGPU();
	};

	struct GBuffer : public IGBuffer
	{
		VkBuffer				Real;
		VkDeviceMemory			Memory;
		UInt32					Stride;
		UInt32					Size;

		UInt32 GetUniqueID() const { return -1; }
		GBuffer()
			: Real(nullptr)
			, Memory(nullptr)
			, Stride(0)
			, Size(0)
		{

		}
		~GBuffer()
		{
			if (Real != nullptr)
			{
				vkDestroyBuffer(GRenderer::Device, Real, nullptr);
			}
			if (Memory != nullptr)
			{
				vkFreeMemory(GRenderer::Device, Memory, nullptr);
			}
		}
	};

	class GTexture;

	namespace GRendererResource
	{
		VkImageView		GetImageView(const GTexture* pTexture);

		VkFramebuffer	GetGResource(const GRenderPass* pRP, const FrameBuffer* pFrameBuf);
		VkRenderPass	GetGResource(const GRenderPass* pRenderPass);

		VkPipelineRasterizationStateCreateInfo RasterizerState(
			VkCullModeFlagBits eCullMode = VK_CULL_MODE_FRONT_BIT,
			Boolean bWireFrame = False,
			Boolean bDepthClip = True,
			UInt32 uDepthBias = 0,
			Boolean bScissor = False);

		VkPipelineDepthStencilStateCreateInfo DepthStencilState(
			VkCompareOp eDepthOp = DEPTH_NEARER,
			Boolean bDepthWrite = True,
			Boolean bStencil = False,
			UInt32	uStencilReadMask = 0,
			UInt32	uStencilWriteMask = 0);

		BlendStateCreateInfo BlendState(
			Boolean bBlend = False,
			VkBlendOp eAlphaOp = VK_BLEND_OP_ADD,
			VkBlendOp eColorOp = VK_BLEND_OP_ADD,
			VkBlendFactor eAlphaSrc = VK_BLEND_FACTOR_ONE,
			VkBlendFactor eAlphaDest = VK_BLEND_FACTOR_ZERO,
			VkBlendFactor eColorSrc = VK_BLEND_FACTOR_SRC_ALPHA,
			VkBlendFactor eColorDest = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
			UInt8	uWriteMask = EWM_AllChannel);

		VkDeviceMemory		  AllocateMemory(
			UInt32 uSize,
			Boolean bTextureMemory,
			UInt32 TypeIndexMask,
			VkMemoryPropertyFlags PropertyFlags);

		Void								ResetFrameBuffer();
		Void								NextFrameBuffer();
		FrameBuffer*						GetCurFrameBuffer();

		Void								ResetShaderResourceSet();
		Void								NextRasterizerShaderResourceSet();
		RasterizerShaderResourceSet*		GetCurRasterizerShaderResourceSet();
		Void								NextComputeShaderResourceSet();
		ComputeShaderResourceSet*			GetCurComputeShaderResourceSet();

		extern UInt32						uResourceID;
		extern Array<IGResource*>			Resources;

		extern VkPipelineLayout				RasterizerRootSignature;
		extern VkPipelineLayout				ComputeRootSignature;

		extern GBuffer*						UploadBuffer;

		extern VertexInputDescription	DefaultInputLayout[(const UInt8)PrimitiveIAFactory::ECount];
		extern ShaderCompiledResult		DefaultVertexShader[(const UInt8)PrimitiveIAFactory::ECount][(const UInt8)MaterialDomain::ECount];
	};
#define BLENDSTATE_OFF False,\
					VK_BLEND_OP_ADD,\
					VK_BLEND_OP_ADD,\
					VK_BLEND_FACTOR_ONE,\
				 	VK_BLEND_FACTOR_ZERO,\
				    VK_BLEND_FACTOR_SRC_ALPHA,\
					VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA

#define BLENDSTATE_ADDITIVE True,\
					 VK_BLEND_OP_ADD,\
					 VK_BLEND_OP_ADD,\
					 VK_BLEND_FACTOR_ZERO,\
					 VK_BLEND_FACTOR_ONE,\
					 VK_BLEND_FACTOR_SRC_ALPHA,\
					 VK_BLEND_FACTOR_ONE

#define BLENDSTATE_BLEND  True,\
					 VK_BLEND_OP_ADD,\
					 VK_BLEND_OP_ADD,\
					 VK_BLEND_FACTOR_ZERO,\
					 VK_BLEND_FACTOR_ZERO,\
					 VK_BLEND_FACTOR_SRC_ALPHA,\
					 VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA
}

#endif