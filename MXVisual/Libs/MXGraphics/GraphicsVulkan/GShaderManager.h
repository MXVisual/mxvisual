#pragma once
#ifndef _G_SHADER_MANAGER_
#define _G_SHADER_MANAGER_
#include "GCommonShaderManager.h"
#include "shaderc/shaderc.h"
#include "vulkan/vulkan.h"

#include <unordered_map>

namespace MXVisual
{
	class GShaderManager : public GCommonShaderManager
	{
	public:
		GShaderManager(Boolean bShaderDebug = False);
		~GShaderManager();

		//Return Compile Result
		Boolean CompileShader(
			const Char* szShaderName,
			ShaderMacro& macros,
			ShaderCompiledResult& result,
			const Void* pShaderCode, UInt32 uShaderSize,
			ShaderType eShaderType,
			Boolean bRecompile);

		VkShaderModule GetShaderObject(const ShaderCompiledResult& CompiledCode)
		{
			auto FindRes = m_mapShaderObjects.find(CompiledCode.pCode);
			if (FindRes != m_mapShaderObjects.end())
			{
				return FindRes->second;
			}
			return nullptr;
		}
	protected:
		struct shaderc_compiler* m_pShaderCompiler;

		Void	CheckAndCreateShaderObjects(ShaderType eType, const ShaderCompiledResult& CompiledCode);
		std::unordered_map<const Void*, VkShaderModule> m_mapShaderObjects;
	};
}

#endif