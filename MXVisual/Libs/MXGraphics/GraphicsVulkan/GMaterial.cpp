#include "GRenderer.h"
#include "GMaterial.h"
#include "GTexture.h"
#include "MXMath.h"
#include "stdio.h"
#include "MXFile.h"
#include "MXXmlFile.h"
#include "MXGraphicsRenderer.h"
#include "GShaderManager.h"
#include "MXLog.h"

void Material_spvc_error_callback(void *userdata, const char *error)
{
	MXVisual::Log::Output("GraphicsVulkan", error);
}

namespace MXVisual
{
	GMaterial::GMaterial(UInt32 UniqueID)
		: GCommonMaterial(UniqueID)
	{
	}

	Boolean GMaterial::ScanMaterialParameter(const Array<IGMaterialParam::Description>& arrMaterialParams)
	{
		spvc_context context;
		spvc_context_create(&context);
		spvc_context_set_error_callback(context, Material_spvc_error_callback, nullptr);

		MaterialShaderType LinkShaderParamType = MaterialShaderType::EMaterialShaderCount;
		if (m_eDomain == MaterialDomain::EPostProcess)
		{
			LinkShaderParamType = ECShader;
		}
		else
		{
			if (m_arrShaderCompiledResult[MaterialShaderType::EVShaderStatic].IsValid())
			{
				spvc_compiler compiler;
				spvc_parsed_ir ir;
				if (SPVC_SUCCESS == spvc_context_parse_spirv(context,
					(const SpvId*)m_arrShaderCompiledResult[MaterialShaderType::EVShaderStatic].pCode,
					m_arrShaderCompiledResult[MaterialShaderType::EVShaderStatic].uSize / 4, &ir) &&
					SPVC_SUCCESS == spvc_context_create_compiler(context,
						SPVC_BACKEND_NONE, ir, SPVC_CAPTURE_MODE_TAKE_OWNERSHIP, &compiler))
				{
					LinkShaderParams(compiler, nullptr, nullptr, nullptr,
						m_arrMaterialBufferBeginSlot[(const UInt8)ShaderType::EVertexShader],
						m_arrMaterialTextureBeginSlot[(const UInt8)ShaderType::EVertexShader]);

				}
				spvc_context_release_allocations(context);
			}

			LinkShaderParamType = EPShaderMaterial;
			ASSERT_IF_FAILED(m_arrShaderCompiledResult[LinkShaderParamType].IsValid());
		}

		spvc_compiler compiler;
		spvc_parsed_ir ir;
		if ((SPVC_SUCCESS != spvc_context_parse_spirv(context,
			(const SpvId*)m_arrShaderCompiledResult[LinkShaderParamType].pCode,
			m_arrShaderCompiledResult[LinkShaderParamType].uSize / 4, &ir)) ||
			(SPVC_SUCCESS != spvc_context_create_compiler(context,
				SPVC_BACKEND_HLSL, ir, SPVC_CAPTURE_MODE_TAKE_OWNERSHIP, &compiler)))
		{
			spvc_context_release_allocations(context);
			spvc_context_destroy(context);
			return False;
		}

		LinkShaderParams(compiler, &m_arrDyncParam, &m_arrTexParam, &m_arrSamplerParam,
			m_arrMaterialBufferBeginSlot[(const UInt8)ShaderType::EPixelShader],
			m_arrMaterialTextureBeginSlot[(const UInt8)ShaderType::EPixelShader]);

		spvc_context_release_allocations(context);
		spvc_context_destroy(context);

		for (UInt32 u = 0; u < arrMaterialParams.ElementCount(); u++)
		{
			GMaterialParam* param = nullptr;
			for (UInt32 uParam = 0; uParam < m_arrDyncParam.ElementCount(); uParam++)
			{
				if (param != nullptr)break;
				if (strcmp(m_arrDyncParam[uParam]->GetName(), arrMaterialParams[u].strName.CString()) == 0)
				{
					param = m_arrDyncParam[uParam];
				}
			}
			for (UInt32 uParam = 0; uParam < m_arrTexParam.ElementCount(); uParam++)
			{
				if (param != nullptr)break;
				if (strcmp(m_arrTexParam[uParam]->GetName(), arrMaterialParams[u].strName.CString()) == 0)
					param = m_arrTexParam[uParam];
			}
		}
		return True;
	}

	Void	GMaterial::LinkShaderParams(spvc_compiler compiler,
		Array<GMaterialParam*>* arrDyncParams,
		Array<GMaterialParam*>* arrTexParams,
		Array<GMaterialSamplerDesc>* arrSamplerParams,
		SInt32& uMaterialBufferBeginSlot,
		SInt32& uMaterialTextureBeginSlot)
	{
		spvc_set sets;
		spvc_compiler_get_active_interface_variables(compiler, &sets);
		spvc_resources resources;
		spvc_compiler_create_shader_resources_for_active_variables(compiler, &resources, sets);

		spvc_resource_type enumTypes[] = {
			SPVC_RESOURCE_TYPE_UNIFORM_BUFFER,
			SPVC_RESOURCE_TYPE_SEPARATE_IMAGE,
			SPVC_RESOURCE_TYPE_SEPARATE_SAMPLERS,
			SPVC_RESOURCE_TYPE_STORAGE_IMAGE
		};

		UInt32 uLastUserTextureOffset = 0;
		UInt32 uLastUserSamplerOffset = 0;
		for (UInt32 uType = 0; uType < ARRAY_ELEMENT_COUNT(enumTypes); uType++)
		{
			const spvc_reflected_resource* Reflected = nullptr;
			size_t ReflectedCount = 0;
			spvc_resources_get_resource_list_for_type(resources, enumTypes[uType], &Reflected, &ReflectedCount);
			if (enumTypes[uType] == SPVC_RESOURCE_TYPE_UNIFORM_BUFFER)
			{
				for (UInt32 u = 0; u < ReflectedCount; u++)
				{
					if (strcmp(Reflected[u].name, "MaterialBuffer") == 0)
					{
						uMaterialBufferBeginSlot = spvc_compiler_get_decoration(compiler, Reflected[u].id, SpvDecoration::SpvDecorationBinding);
						if (arrDyncParams == nullptr)continue;

						spvc_type hType = spvc_compiler_get_type_handle(compiler, Reflected[u].base_type_id);
						UInt32 uLastOffset = 0;
						size_t bufferSize;
						spvc_compiler_get_declared_struct_size(compiler, hType, &bufferSize);
						m_uMaterialBufferSize = bufferSize;

						UInt32 uMemberCount = spvc_type_get_num_member_types(hType);
						for (UInt32 uParam = 0; uParam < uMemberCount; uParam++)
						{
							const Char* ParamName = spvc_compiler_get_member_name(compiler, Reflected[u].base_type_id, uParam);
							UInt32 uComponent = spvc_compiler_get_member_decoration(compiler, Reflected[u].base_type_id, uParam, SpvDecorationComponent);

							IGMaterialParam::MaterialParamType vectorType = IGMaterialParam::EMPT_Invalid;
							switch (uComponent)
							{
							case 1: vectorType = IGMaterialParam::EMPT_Float1; break;
							case 2: vectorType = IGMaterialParam::EMPT_Float2; break;
							case 3: vectorType = IGMaterialParam::EMPT_Float3; break;
							case 4: vectorType = IGMaterialParam::EMPT_Float4; break;
							}

							GMaterialParam* pParam = nullptr;
							if (arrDyncParams->ElementCount() <= uParam)
							{
								pParam = new GMaterialParam(ParamName, NULL, vectorType);

								arrDyncParams->Add(pParam);
							}
							ASSERT_IF_FAILED(strcmp((*arrDyncParams)[uParam]->GetName(), ParamName) == 0);
						}
						break;
					}
				}
			}
			else if (enumTypes[uType] == SPVC_RESOURCE_TYPE_SEPARATE_IMAGE)
			{
				for (UInt32 u = 0; u < ReflectedCount; u++)
				{
					if (strstr(Reflected[u].name, "ENGINE_") != nullptr)
					{
						continue;
					}
					UInt32 uSlot = spvc_compiler_get_decoration(compiler, Reflected[u].id, SpvDecoration::SpvDecorationBinding);
					if (arrTexParams != nullptr)
					{
						spvc_type hType = spvc_compiler_get_type_handle(compiler, Reflected[u].base_type_id);
						SpvDim dim = spvc_type_get_image_dimension(hType);

						IGMaterialParam::MaterialParamType textureType = IGMaterialParam::EMPT_Invalid;
						switch (dim)
						{
						case SpvDim::SpvDim1D: textureType = IGMaterialParam::EMPT_Texture1D; break;
						case SpvDim::SpvDim2D: textureType = IGMaterialParam::EMPT_Texture2D; break;
						case SpvDim::SpvDim3D: textureType = IGMaterialParam::EMPT_Texture3D; break;
						case SpvDim::SpvDimCube: textureType = IGMaterialParam::EMPT_TextureCube; break;
						}

						GMaterialParam* pParam = nullptr;
						if (arrTexParams->ElementCount() <= uLastUserTextureOffset)
						{
							pParam = new GMaterialParam(Reflected[u].name, NULL, textureType);
							arrTexParams->Add(pParam);
						}

						ASSERT_IF_FAILED(strcmp((*arrTexParams)[uLastUserTextureOffset]->GetName(), Reflected[u].name) == 0);
					}

					uMaterialTextureBeginSlot = uMaterialTextureBeginSlot == -1 ? uSlot :
						Math::Min<UInt32>(uMaterialTextureBeginSlot, uSlot);
					uLastUserTextureOffset++;
				}
			}
			else if (enumTypes[uType] == SPVC_RESOURCE_TYPE_SEPARATE_SAMPLERS)
			{
				for (UInt32 u = 0; u < ReflectedCount; u++)
				{
					if (arrSamplerParams == nullptr)continue;
					if (strstr(Reflected[u].name, "ENGINE_") != nullptr)
						continue;

					UInt32 a = -1, b = -1;
					sscanf(Reflected[u].name, "sampler_%d_%d", &a, &b);

					UInt32 uSlot = spvc_compiler_get_decoration(compiler, Reflected[u].id, SpvDecoration::SpvDecorationBinding);

					if (arrSamplerParams->ElementCount() <= uLastUserSamplerOffset)
					{
						GMaterialSamplerDesc desc;
						desc.uBindingPoint = uSlot;
						desc.uAddressMode = (UInt8)a;
						desc.uFilterMode = (UInt8)b;
						arrSamplerParams->Add(desc);
					}

					ASSERT_IF_FAILED((*arrSamplerParams)[uLastUserSamplerOffset].uAddressMode == a);
					ASSERT_IF_FAILED((*arrSamplerParams)[uLastUserSamplerOffset].uFilterMode == b);

					uLastUserSamplerOffset++;
				}
			}
		}
	}

	Boolean GMaterial::InitPSO()
	{
		Array<PrimitiveRenderStage> RenderStages = GetRenderStages();

		for (UInt32 uIAFactory = 0; uIAFactory < (const UInt32)PrimitiveIAFactory::ECount; uIAFactory++)
		{
			if ((uIAFactory == (const UInt32)PrimitiveIAFactory::EStatic2D && (m_eDomain == MaterialDomain::EDecal || m_eDomain == MaterialDomain::EWorldSpace)) ||
				(uIAFactory != (const UInt32)PrimitiveIAFactory::EStatic2D && (m_eDomain == MaterialDomain::EScreenSpace || m_eDomain == MaterialDomain::EPostProcess)))
			{
				continue;
			}

			for (UInt32 uRenderStage = 0; uRenderStage < RenderStages.ElementCount(); uRenderStage++)
			{
				ASSERT_IF_FAILED(m_eDomain != MaterialDomain::EPostProcess);

				ShaderCompiledResult* VS = nullptr;
				switch ((PrimitiveIAFactory)uIAFactory)
				{
				case PrimitiveIAFactory::EStatic:
					VS = &m_arrShaderCompiledResult[MaterialShaderType::EVShaderStatic];
					break;
				case PrimitiveIAFactory::EStatic_Instance:
					VS = &m_arrShaderCompiledResult[MaterialShaderType::EVShaderStatic_Instance];
					break;
				case PrimitiveIAFactory::ESkin:
					VS = &m_arrShaderCompiledResult[MaterialShaderType::EVShaderSkin];
					break;
				case PrimitiveIAFactory::EStatic2D:
					break;
				}
				if (VS != nullptr && !VS->IsValid())
					VS = nullptr;

				ShaderCompiledResult* PS = nullptr;
				VkPipelineDepthStencilStateCreateInfo DS;
				switch (RenderStages[uRenderStage])
				{
				case PrimitiveRenderStage::EDepthPrePass:
					PS = &m_arrShaderCompiledResult[MaterialShaderType::EPShaderDepth];
					DS = GRendererResource::DepthStencilState();
					break;
				case PrimitiveRenderStage::EBasePass:
					PS = &m_arrShaderCompiledResult[MaterialShaderType::EPShaderMaterial];
					DS = GRendererResource::DepthStencilState(DEPTH_EQUAL, False);
					break;
				case PrimitiveRenderStage::EForwardTranslucent:
				case PrimitiveRenderStage::EDecalPass:
					PS = &m_arrShaderCompiledResult[MaterialShaderType::EPShaderMaterial];
					DS = GRendererResource::DepthStencilState(DEPTH_NEARER, False);
					break;
				case PrimitiveRenderStage::EForward2DPrimitive:
					PS = &m_arrShaderCompiledResult[MaterialShaderType::EPShaderMaterial];
					DS = GRendererResource::DepthStencilState(DEPTH_ALWAYS, False);
					break;
				}
				if (PS == nullptr || !PS->IsValid())
				{
					ASSERT_IF_FAILED("Not Support No PS");
					continue;
				}	

				GRenderPass RP;
				switch (RenderStages[uRenderStage])
				{
				case PrimitiveRenderStage::EDepthPrePass:
					RP.DSVFormat = GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0]->GetFormat();
					break;
				case PrimitiveRenderStage::EBasePass:
					RP.RTVCount = 5;
					RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]->GetFormat();
					RP.RTVFormats[1] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferA]->GetFormat();
					RP.RTVFormats[2] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferB]->GetFormat();
					RP.RTVFormats[3] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferC]->GetFormat();
					RP.RTVFormats[4] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferD]->GetFormat();
					RP.DSVFormat = GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0]->GetFormat();
					break;
				case PrimitiveRenderStage::EDecalPass:
					RP.RTVCount = 4;
					RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]->GetFormat();
					RP.RTVFormats[1] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferA]->GetFormat();
					RP.RTVFormats[2] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferB]->GetFormat();
					RP.RTVFormats[3] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferC]->GetFormat();
					//pso_desc.DSVFormat =
					//	PixelFormatMap[ResourceType::ERT_DepthStencilTargetView][GetOutputTargetFormat(OutputTargetType::ESceneDepth0)];
					break;
				case PrimitiveRenderStage::EForwardTranslucent:
					RP.RTVCount = 1;
					RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]->GetFormat();
					RP.DSVFormat = GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0]->GetFormat();
					break;
				case PrimitiveRenderStage::EForward2DPrimitive:
					RP.RTVCount = 1;
					RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]->GetFormat();
					break;
				}

				VkPipelineRasterizationStateCreateInfo RS;
				switch (GetFaceType())
				{
				case MaterialFaceType::EFrontface:
					RS = GRendererResource::RasterizerState();
					break;
				case MaterialFaceType::EBackface:
					RS = GRendererResource::RasterizerState(VK_CULL_MODE_BACK_BIT);
					break;
				case MaterialFaceType::ETwoSide:
					RS = GRendererResource::RasterizerState(VK_CULL_MODE_NONE);
					break;
				case MaterialFaceType::EWireframe:
					RS = GRendererResource::RasterizerState(VK_CULL_MODE_NONE, True);
					break;
				}

				BlendStateCreateInfo BS;
				switch (GetBlendMode())
				{
				case MaterialBlendMode::EOpaque:
				case MaterialBlendMode::EMasked:
				case MaterialBlendMode::EMaskTranslucency:
					BS = GRendererResource::BlendState(BLENDSTATE_OFF);
					break;
				case MaterialBlendMode::EAdditive:
					BS = GRendererResource::BlendState(BLENDSTATE_ADDITIVE);
					break;
				case MaterialBlendMode::ETranslucency:
					BS = GRendererResource::BlendState(BLENDSTATE_BLEND);
					break;
				}

				for (UInt32 uTopology = 0; uTopology < (const UInt32)TopologyType::ECount; uTopology++)
				{
					GPipeline* pNewPSO = new GPipeline;
					if (!pNewPSO->InitForRender(
						(PrimitiveIAFactory)uIAFactory,
						(TopologyType)uTopology,
						RP, RS, DS, BS, VS, PS))
					{
						return False;
					}

					const UInt32 uIndex = CalculatePSOIndex(
						(PrimitiveRenderStage)RenderStages[uRenderStage],
						(PrimitiveIAFactory)uIAFactory,
						(TopologyType)uTopology);
					m_arrPSO[uIndex] = pNewPSO;
				}
			}
		}
		return True;
	}
}
