#include "GShaderManager.h"
#include "GRenderer.h"
#include "MXMath.h"
#include "MXSystem.h"
#include "GMaterial.h"


namespace MXVisual
{
	struct ShaderData
	{
		UInt32 Size = 0;
		Char* Code = nullptr;
	};
	std::unordered_map<std::string, ShaderData*> m_mapCacheShaderFileContent;

	shaderc_include_result* ShaderIncludeCallback(
		void* user_data, const char* requested_source, int type,
		const char* requesting_source, size_t include_depth)
	{
		Void *pData = nullptr;
		UInt32 uBytes = 0;

		auto it = m_mapCacheShaderFileContent.find(requested_source);
		if (it != m_mapCacheShaderFileContent.end())
		{
			uBytes = it->second->Size;
			pData = it->second->Code;
		}
		else
		{
			String strFileName = ENGINE_SHADER_DIR;
			strFileName += requested_source;
			IFile* pFile = IFile::Open(strFileName.CString(), IFile::EFDT_Text);
			if (!pFile)return nullptr;

			ShaderData* pNewData = new ShaderData;
			pNewData->Size = pFile->GetFileSize();
			pNewData->Code = new Char[pNewData->Size]{ 0 };
			pNewData->Size = pFile->Read(pNewData->Code, pNewData->Size);
			pFile->Close();

			uBytes = pNewData->Size;
			pData = pNewData->Code;

			m_mapCacheShaderFileContent[requested_source] = pNewData;
		}

		shaderc_include_result* res = nullptr;
		res = new shaderc_include_result;
		res->content_length = uBytes;
		res->content = (const Char*)pData;
		res->user_data = nullptr;
		res->source_name = requested_source;
		res->source_name_length = strlen(requested_source);
		return res;
	}

	void ShaderIncludeReleaseCallback(
		void* user_data, shaderc_include_result* include_result)
	{
		if (include_result != nullptr)
		{
			delete include_result;
		}
	}

	GShaderManager::GShaderManager(Boolean bShaderDebug)
		: GCommonShaderManager(bShaderDebug)
	{
		m_strCachePath = ".\\.CachesVK";

		m_pShaderCompiler = shaderc_compiler_initialize();
	}
	GShaderManager::~GShaderManager()
	{
		shaderc_compiler_release(m_pShaderCompiler);

		if (!m_bUpdateToDate)
		{
			//TODO!! Cache Version
			SaveMaterialCache();
			SaveShaderCache();
		}

		for (auto& it : m_mapMaterialCache)
		{
			delete it.second;
		}
		m_mapMaterialCache.clear();

		for (auto& it : m_mapShaderCache)
		{
			delete it.second;
		}
		m_mapShaderCache.clear();
	}

	Boolean GShaderManager::CompileShader(
		const Char* szShaderName,
		ShaderMacro& macros,
		ShaderCompiledResult& result,
		const Void* pShaderCode, UInt32 uShaderSize,
		ShaderType eShaderType,
		Boolean bRecompile)
	{
		if (!GRendererResource::HasRawShaderFolder() || !bRecompile)
		{
			GetShaderCache(szShaderName, result);

			if (result.IsValid())
			{
				CheckAndCreateShaderObjects(eShaderType, result);
				return True;
			}
		}

		if (pShaderCode == nullptr || uShaderSize == 0)return False;

		shaderc_compile_options* compiler_options = shaderc_compile_options_initialize();

		shaderc_compile_options_set_include_callbacks(
			compiler_options, ShaderIncludeCallback, ShaderIncludeReleaseCallback, nullptr);

		shaderc_compile_options_set_auto_bind_uniforms(compiler_options, true);
		shaderc_compile_options_set_auto_map_locations(compiler_options, true);

		shaderc_compile_options_set_optimization_level(
			compiler_options,
			shaderc_optimization_level_zero
			//m_bShaderDebug ? shaderc_optimization_level_zero : shaderc_optimization_level_size
		);
		//shaderc_optimization_level_performance shaderc_optimization_level_size都会将变量名优化掉

		shaderc_compile_options_set_source_language(
			compiler_options, shaderc_source_language_hlsl);

		Char buf[16] = { 0 };
		sprintf(buf, "%d", eShaderType);
		shaderc_compile_options_add_macro_definition(
			compiler_options,
			"SHADER_TYPE", strlen("SHADER_TYPE"),
			buf, strlen(buf)
		);

		shaderc_shader_kind shaderType;
		const Char* szEntry = nullptr;

		macros.AddMacro("SHADER_PLATFORM", "0");//HLSL
		switch (eShaderType)
		{
		case ShaderType::EVertexShader:
			shaderType = shaderc_vertex_shader;
			szEntry = "VSMain";
			macros.AddMacro("SHADER_TYPE", "0");
			shaderc_compile_options_set_binding_base(
				compiler_options, shaderc_uniform_kind_buffer,
				(const UInt32)RasterizerRootSignatureDesc::EVS_CBV_BEGIN);
			shaderc_compile_options_set_binding_base(
				compiler_options, shaderc_uniform_kind_texture,
				(const UInt32)RasterizerRootSignatureDesc::EVS_SRV_BEGIN);
			shaderc_compile_options_set_binding_base(
				compiler_options, shaderc_uniform_kind_sampler,
				(const UInt32)RasterizerRootSignatureDesc::EVS_SAMPLER_BEGIN);
			break;
		case ShaderType::EPixelShader:
			shaderType = shaderc_fragment_shader;
			szEntry = "PSMain";
			macros.AddMacro("SHADER_TYPE", "1");
			shaderc_compile_options_set_binding_base(
				compiler_options, shaderc_uniform_kind_buffer,
				(const UInt32)RasterizerRootSignatureDesc::EPS_CBV_BEGIN);
			shaderc_compile_options_set_binding_base(
				compiler_options, shaderc_uniform_kind_texture,
				(const UInt32)RasterizerRootSignatureDesc::EPS_SRV_BEGIN);
			shaderc_compile_options_set_binding_base(
				compiler_options, shaderc_uniform_kind_sampler,
				(const UInt32)RasterizerRootSignatureDesc::EPS_SAMPLER_BEGIN);
			break;
		case ShaderType::EComputeShader:
			shaderType = shaderc_compute_shader;
			szEntry = "CSMain";
			macros.AddMacro("SHADER_TYPE", "2");
			shaderc_compile_options_set_binding_base(
				compiler_options, shaderc_uniform_kind_buffer,
				(const UInt32)ComputeRootSignatureDesc::ECBV_BEGIN);
			shaderc_compile_options_set_binding_base(
				compiler_options, shaderc_uniform_kind_texture,
				(const UInt32)ComputeRootSignatureDesc::ESRV_BEGIN);
			shaderc_compile_options_set_binding_base(
				compiler_options, shaderc_uniform_kind_unordered_access_view,
				(const UInt32)ComputeRootSignatureDesc::EUAV_BEGIN);
			shaderc_compile_options_set_binding_base(
				compiler_options, shaderc_uniform_kind_sampler,
				(const UInt32)ComputeRootSignatureDesc::ESAMPLER_BEGIN);
			break;
		}

		for (UInt32 u = 0; u < macros.GetMacroCount(); u++)
		{
			shaderc_compile_options_add_macro_definition(
				compiler_options,
				macros.GetMacroName(u), strlen(macros.GetMacroName(u)),
				macros.GetMacroValue(u), strlen(macros.GetMacroValue(u))
			);
		}

		macros.AddMacro(nullptr, nullptr);
		shaderc_compilation_result* res = shaderc_compile_into_spv(
			m_pShaderCompiler,
			(const Char*)pShaderCode, uShaderSize,
			shaderType, szShaderName, szEntry,
			compiler_options);
		if (shaderc_result_get_compilation_status(res) !=
			shaderc_compilation_status_success)
		{
			const char* err = shaderc_result_get_error_message(res);
			Log::Output("GraphicsVulkan", err);

			shaderc_result_release(res);
			return False;
		}

		UInt32 uNewShaderCompiledSize = shaderc_result_get_length(res);
		UInt8* pNewShaderCompiledCode = new UInt8[uNewShaderCompiledSize];
		memcpy(pNewShaderCompiledCode, shaderc_result_get_bytes(res), uNewShaderCompiledSize);

		result.pCode = pNewShaderCompiledCode;
		result.uSize = uNewShaderCompiledSize;

		CacheShader(szShaderName, pNewShaderCompiledCode, uNewShaderCompiledSize);
		CheckAndCreateShaderObjects(eShaderType, result);

		shaderc_result_release(res);
		return True;
	}

	Void	GShaderManager::CheckAndCreateShaderObjects(ShaderType eType, const ShaderCompiledResult& CompiledCode)
	{
		if (m_mapShaderObjects.find(CompiledCode.pCode) != m_mapShaderObjects.end())
		{
			return;
		}

		VkShaderModuleCreateInfo info;
		info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		info.pNext = nullptr;
		info.flags = 0;
		info.pCode = (const UInt32*)CompiledCode.pCode;
		info.codeSize = CompiledCode.uSize;

		VkShaderModule shaderobject = nullptr;
		if (VK_SUCCESS != vkCreateShaderModule(GRenderer::Device, &info, nullptr, &shaderobject))
		{
			Log::Output("Graphics Vulkan", "Create Shader Module Failed");
			return;
		}

		m_mapShaderObjects[CompiledCode.pCode] = shaderobject;
	}

}