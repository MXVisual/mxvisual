#pragma once
#ifndef _G_MATERIAL_
#define _G_MATERIAL_
#include "GCommonMaterial.h"
#include "spirv_cross/spirv_cross_c.h"

namespace MXVisual
{
	class GMaterial : public GCommonMaterial
	{
	public:
		GMaterial(UInt32 UniqueID);
		~GMaterial() {}

	protected:
	
		Boolean ScanMaterialParameter(const Array<IGMaterialParam::Description>& arrMaterialParams);

		//Only Call On ShaderCache
		Void	LinkShaderParams(spvc_compiler compiler,
			Array<GMaterialParam*>* arrDyncParams,
			Array<GMaterialParam*>* arrTexParams,
			Array<GMaterialSamplerDesc>* arrSamplerParams,
			SInt32& uMaterialBufferBeginSlot,
			SInt32& uMaterialTextureBeginSlot);

		Boolean InitPSO();
	};
}

#endif