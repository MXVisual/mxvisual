#pragma once
#ifndef _G_COMMAND_LIST_
#define _G_COMMAND_LIST_
#include "GRenderer.h"

namespace MXVisual
{
	class GCommandList : public IGCommandList
	{
	public:
		class ID3D11DeviceContext*  APICommandlist;
#if defined(DEBUG)
		class ID3DUserDefinedAnnotation* APIStateMarker;//DX11_1
#endif	

		GCommandList(ID3D11DeviceContext* APICL);
		~GCommandList();

		virtual Void	SetViewport(const GViewport& Viewport) override;
		virtual Void	SetScissor(const Region& Scissor) override;

		virtual Void	BindPipeline(GPipeline* Pipeline) override;
		virtual Void	BindRenderTarget(
			Boolean bClearDepthTarget,
			const IGTexture* DT, UInt32 uDTSubresourceIdx,
			Boolean bClearColorTarget,
			const IGTexture* RT0, UInt32 uRT0SubresourceIdx,
			const IGTexture* RT1 = nullptr, UInt32 uRT1SubresourceIdx = 0,
			const IGTexture* RT2 = nullptr, UInt32 uRT2SubresourceIdx = 0,
			const IGTexture* RT3 = nullptr, UInt32 uRT3SubresourceIdx = 0,
			const IGTexture* RT4 = nullptr, UInt32 uRT4SubresourceIdx = 0,
			const IGTexture* RT5 = nullptr, UInt32 uRT5SubresourceIdx = 0,
			const IGTexture* RT6 = nullptr, UInt32 uRT6SubresourceIdx = 0,
			const IGTexture* RT7 = nullptr, UInt32 uRT7SubresourceIdx = 0) override;

		virtual Void	BindVertexBuffer(UInt32 uSlot, IGBuffer* pBuffer, UInt32 uStride) override;
		virtual Void	BindIndexBuffer(IGBuffer* pBuffer, Boolean b32Bit) override;

		virtual Void	BindShaderResource(ShaderType eType, UInt32 uSlot, const IGTexture* pTexture) override;
		virtual Void	BindUnorderResource(ShaderType eType, UInt32 uSlot, const  IGTexture* pTexture, UInt32 uSubresourceIdx) override;
		virtual Void	BindConstantBuffer(ShaderType eType, UInt32 uSlot, const IGBuffer* pBuffer) override;
		virtual Void	BindSampler(ShaderType eType, UInt32 uSlot, const IGSampler* pSampler) override;

		virtual Void	Draw(UInt32 uVertexCount, UInt32 uInstanceCount, Boolean bTriangleNotLine) override;
		virtual Void	DrawIndex(UInt32 uIndexCount, UInt32 uIndexOffset, UInt32 uInstanceCount, Boolean bTriangleNotLine) override;

		virtual Void	Dispatch(UInt32 X, UInt32 Y, UInt32 Z) override;

		virtual Void	Flush() override;

		virtual Void	CopyResource(IGTexture* pDest, IGTexture* pSrc) override;

		virtual Boolean	Map(IGTexture* pTexture, UInt32 uSubResouce, Boolean bRead, MapRegion& region) override;
		virtual Void	Unmap(IGTexture* pTexture, UInt32 uSubResouce) override;
		virtual Boolean	Map(IGBuffer* pBuffer, Boolean bRead, MapRegion& region) override;
		virtual Void	Unmap(IGBuffer* pBuffer) override;

		virtual Boolean	SubmitData(IGTexture* pTexture, UInt32 uSubResouce, UInt32 uSize, const Void* pData) override;
		virtual Boolean	SubmitData(IGBuffer* pBuffer, UInt32 uSize, const Void* pData) override;
	};
}

#endif