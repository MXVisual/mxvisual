#include "GRenderer.h"
#include "MXConstant.h"
#include "MXCommon.h"

#include "GShaderManager.h"

#include "GViewFrustum.h"
#include "GTexture.h"
#include "GPrimitive.h"
#include "GMaterial.h"
#include "GCommandList.h"

namespace MXVisual
{

#if defined(DEBUG)
	Void DebugPushEvent(const Char* eventName)
	{
		Array<UInt8> w = String::Transcode_GB2312_To_UTF16(eventName);
		((GCommandList*)GRenderer::CommandList())->APIStateMarker->BeginEvent((const wchar_t*)w.GetRawData());
	}
	Void DebugPopEvent()
	{
		((GCommandList*)GRenderer::CommandList())->APIStateMarker->EndEvent();
	}
#endif

	Boolean	GPipeline::InitForRender(
		PrimitiveIAFactory			eIAFactory,
		TopologyType				eTopology,
		const GRenderPass&			RP,
		ID3D11RasterizerState*		pRS,
		ID3D11DepthStencilState*	pDS,
		ID3D11BlendState*			pBS,
		const ShaderCompiledResult*	pVS,
		const ShaderCompiledResult*	pPS)
	{
		switch (eIAFactory)
		{
		case PrimitiveIAFactory::EStatic_Instance:
		case PrimitiveIAFactory::EStatic:
		case PrimitiveIAFactory::ESkin:
		case PrimitiveIAFactory::EStatic2D:
			VD = GRendererResource::DefaultInputLayout[(const UInt32)eIAFactory];
			break;
		default:
			ASSERT_IF_FAILED(!"Invalid IAFactory");
			return False;
		}

		if (pVS != nullptr)
		{
			VS = (ID3D11VertexShader*)((GShaderManager*)GRendererResource::ShaderManager)->GetShaderObject(*pVS);
		}
		else
		{
			VS = GRendererResource::DefaultVertexShader[(const UInt32)eIAFactory][(const UInt32)MaterialDomain::EWorldSpace];
		}

		RS = pRS;
		DS = pDS;
		BS = pBS;

		ASSERT_IF_FAILED(pPS != nullptr);
		if (pPS == nullptr || !pPS->IsValid())return False;
		PS = (ID3D11PixelShader*)((GShaderManager*)GRendererResource::ShaderManager)->GetShaderObject(*pPS);

		return PS != nullptr;
	}

	Boolean GPipeline::InitForCompute(
		const ShaderCompiledResult*		pCS)
	{
		ASSERT_IF_FAILED(pCS != nullptr);
		if (pCS == nullptr)return False;

		CS = (ID3D11ComputeShader*)((GShaderManager*)GRendererResource::ShaderManager)->GetShaderObject(*pCS);

		return CS != nullptr;
	}

	Void	GPipeline::Release()
	{
		//Do Nothing
	}

	namespace GRenderer
	{
		UInt32	uMaxGPUResourceSize;

		IDXGIFactory6*	DXGIFactory = nullptr;
		ID3D11Device*	Device = nullptr;

		ID3D11DeviceContext*	HardwarePipeline = nullptr;

		UInt32					CommandlistOffset = 0;
		GCommandList*			Commandlist[2] = { nullptr, };

		D3D_FEATURE_LEVEL	FeatureLevel = D3D_FEATURE_LEVEL_11_1;

		Boolean InitAPI(const Char* szAdapterName)
		{
			if (FAILED(CreateDXGIFactory(IID_PPV_ARGS(&DXGIFactory))))
			{
				return False;
			}

			DXGI_ADAPTER_DESC adapter_desc;
			IDXGIAdapter* pAdapter = nullptr;
			UInt32 uAdapterIndex = 0;
			wchar_t wsz[Constant_MAX_PATH];
			if (szAdapterName != nullptr)
				wsprintfW(wsz, L"%S", szAdapterName);

			while (SUCCEEDED(DXGIFactory->EnumAdapterByGpuPreference(
				uAdapterIndex, DXGI_GPU_PREFERENCE_HIGH_PERFORMANCE, IID_PPV_ARGS(&pAdapter))))
			{
				if (szAdapterName == nullptr)
					break;

				pAdapter->GetDesc(&adapter_desc);
				if (lstrcmpiW(wsz, adapter_desc.Description))
				{
					break;
				}

				uAdapterIndex++;
				MX_RELEASE_INTERFACE(pAdapter);
			}

			D3D_FEATURE_LEVEL features[] = {
				D3D_FEATURE_LEVEL_11_1,
				D3D_FEATURE_LEVEL_11_0,
			};

			if (FAILED(D3D11CreateDevice(pAdapter,
				pAdapter != nullptr ? D3D_DRIVER_TYPE_UNKNOWN : D3D_DRIVER_TYPE_HARDWARE,
				NULL,
				D3D11_CREATE_DEVICE_DEBUG,
				features, ARRAY_ELEMENT_COUNT(features),
				D3D11_SDK_VERSION,
				&Device,
				&FeatureLevel,
				&HardwarePipeline)))
			{
				MX_RELEASE_INTERFACE(pAdapter);
				return False;
			}

			//û���ҵ���ӦAdapter��dx�Զ���������Ҫ��DXGIFactory�ҳ���
			if (pAdapter == nullptr)
			{
				IDXGIDevice *pDXGIDevice = nullptr;
				IDXGIAdapter *pDXGIAdapter = nullptr;
				if (FAILED(Device->QueryInterface(IID_PPV_ARGS(&pDXGIDevice))) ||
					FAILED(pDXGIDevice->GetAdapter(&pDXGIAdapter)) ||
					FAILED(pDXGIAdapter->GetParent(IID_PPV_ARGS(&DXGIFactory))))
				{
					MX_RELEASE_INTERFACE(pDXGIAdapter);
					MX_RELEASE_INTERFACE(pDXGIDevice);
					return False;
				}
			}

			MX_RELEASE_INTERFACE(pAdapter);

			ID3D11DeviceContext* pDC0 = nullptr;
			ID3D11DeviceContext* pDC1 = nullptr;
			if (FAILED(Device->CreateDeferredContext(0, &pDC0)) ||
				FAILED(Device->CreateDeferredContext(0, &pDC1)))
			{
				Log::Output("GraphicsD3D11", "Create CommandList Failed");
				return False;
			}

			Commandlist[0] = new GCommandList(pDC0);
			Commandlist[1] = new GCommandList(pDC1);
			return True;
		}
		Void	ReleaseAPI()
		{
			delete GRendererResource::ShaderManager;

			MX_RELEASE_INTERFACE(HardwarePipeline);
			MX_DELETE(Commandlist[0]);
			MX_DELETE(Commandlist[1]);
			MX_RELEASE_INTERFACE(Device);
			MX_RELEASE_INTERFACE(DXGIFactory);
		}

		IGCommandList*	CommandList()
		{
			return Commandlist[CommandlistOffset];
		}

		Void			Execute()
		{
			ID3D11CommandList* pCL = nullptr;
			if (SUCCEEDED(((GCommandList*)CommandList())->APICommandlist->FinishCommandList(FALSE, &pCL)))
			{
				HardwarePipeline->ExecuteCommandList(pCL, FALSE);
			}
			MX_RELEASE_INTERFACE(pCL);
			HardwarePipeline->ClearState();
			CommandlistOffset = (CommandlistOffset + 1) % 2;
		}
	}
}