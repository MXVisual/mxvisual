#include "MXGraphicsRenderer.h"
#include "GRenderer.h"

#include "GRenderWindow.h"
#include "GTexture.h"
#include "GPrimitive.h"
#include "GMaterial.h"
#include "GViewFrustum.h"

#include "GShaderManager.h"

namespace MXVisual
{
	D3D11_INPUT_ELEMENT_DESC GInputDesc2D[3] = {
		{ "POSITION",	0, DXGI_FORMAT_R32G32_FLOAT,		0, 0,	D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 8,	D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0, 24,	D3D11_INPUT_PER_VERTEX_DATA,0 }
	};

	D3D11_INPUT_ELEMENT_DESC GInputDescStatic[5] = {
		{ "POSITION",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 0, D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "NORMAL",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,16, D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "TANGENT",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,32, D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,48, D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0,64, D3D11_INPUT_PER_VERTEX_DATA,0 },
	};

	D3D11_INPUT_ELEMENT_DESC GInputDescStatic_Instance[9] = {
		{ "POSITION",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 0, D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "NORMAL",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,16, D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "TANGENT",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,32, D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,48, D3D11_INPUT_PER_VERTEX_DATA,0 },
		{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0,64, D3D11_INPUT_PER_VERTEX_DATA,0 },
		//Instance Data
		{ "TRANSFORM",	0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0,	D3D11_INPUT_PER_INSTANCE_DATA,1 },
		{ "TRANSFORM",	1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16,	D3D11_INPUT_PER_INSTANCE_DATA,1 },
		{ "TRANSFORM",	2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32,	D3D11_INPUT_PER_INSTANCE_DATA,1 },
		{ "TRANSFORM",	3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48,	D3D11_INPUT_PER_INSTANCE_DATA,1 },
	};

	D3D11_INPUT_ELEMENT_DESC GInputDescSkin[7] = {
	   { "POSITION",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 0, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "NORMAL",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,16, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "TANGENT",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,32, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,48, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0,64, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "BLENDINDICES",0, DXGI_FORMAT_R32G32B32A32_UINT,	0,72, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "BLENDWEIGHT", 0,DXGI_FORMAT_R32G32B32A32_FLOAT,	0,88, D3D11_INPUT_PER_VERTEX_DATA,0 },
	};

	D3D11_INPUT_ELEMENT_DESC GInputDescSkin_Instance[11] = {
	   { "POSITION",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 0, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "NORMAL",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,16, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "TANGENT",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,32, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,48, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0,64, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "BLENDINDICES",0, DXGI_FORMAT_R32G32B32A32_UINT,	0,72, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   { "BLENDWEIGHT", 0,DXGI_FORMAT_R32G32B32A32_FLOAT,	0,88, D3D11_INPUT_PER_VERTEX_DATA,0 },
	   //Instance Data
	   { "TRANSFORM",	0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0,  D3D11_INPUT_PER_INSTANCE_DATA,1 },
	   { "TRANSFORM",	1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA,1 },
	   { "TRANSFORM",	2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA,1 },
	   { "TRANSFORM",	3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA,1 },
	};


	DXGI_FORMAT PixelFormatMap[ERT_Count][EPF_Count] = {
		//EPFT_Resource
		{
			DXGI_FORMAT_R8_UNORM,
			DXGI_FORMAT_R8G8_UNORM,
			DXGI_FORMAT_R8G8B8A8_UNORM,

			DXGI_FORMAT_R16_FLOAT,
			DXGI_FORMAT_R16G16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_UNORM,

			DXGI_FORMAT_R32_FLOAT,
			DXGI_FORMAT_R32G32_FLOAT,
			DXGI_FORMAT_R32G32B32A32_FLOAT,

			DXGI_FORMAT_BC1_UNORM,
			DXGI_FORMAT_BC3_UNORM,
			DXGI_FORMAT_BC4_UNORM,
			DXGI_FORMAT_BC5_UNORM,
			DXGI_FORMAT_BC6H_UF16,
			DXGI_FORMAT_BC7_UNORM,

			DXGI_FORMAT_R24G8_TYPELESS,
			DXGI_FORMAT_R32G8X24_TYPELESS,
		},

		//EPFT_ShaderResourceView
		{
			DXGI_FORMAT_R8_UNORM,
			DXGI_FORMAT_R8G8_UNORM,
			DXGI_FORMAT_R8G8B8A8_UNORM,

			DXGI_FORMAT_R16_FLOAT,
			DXGI_FORMAT_R16G16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_UNORM,

			DXGI_FORMAT_R32_FLOAT,
			DXGI_FORMAT_R32G32_FLOAT,
			DXGI_FORMAT_R32G32B32A32_FLOAT,

			DXGI_FORMAT_BC1_UNORM,
			DXGI_FORMAT_BC3_UNORM,
			DXGI_FORMAT_BC4_UNORM,
			DXGI_FORMAT_BC5_UNORM,
			DXGI_FORMAT_BC6H_UF16,
			DXGI_FORMAT_BC7_UNORM,

			DXGI_FORMAT_R24_UNORM_X8_TYPELESS,
			DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS,
		},

		//EPFT_RenderTargetView
		{
			DXGI_FORMAT_R8_UNORM,
			DXGI_FORMAT_R8G8_UNORM,
			DXGI_FORMAT_R8G8B8A8_UNORM,

			DXGI_FORMAT_R16_FLOAT,
			DXGI_FORMAT_R16G16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_UNORM,

			DXGI_FORMAT_R32_FLOAT,
			DXGI_FORMAT_R32G32_FLOAT,
			DXGI_FORMAT_R32G32B32A32_FLOAT,

			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,

			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
		},

		//EPFT_DepthStencilTargetView
		{
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,

			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,

			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,

			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,

			DXGI_FORMAT_D24_UNORM_S8_UINT,
			DXGI_FORMAT_D32_FLOAT_S8X24_UINT,
		}
	};
	UInt8		PixelFormatSize[EPF_Count] = {
	   32,
	   32,
	   32,
	   64,
	   64,

	   32,
	   64,
	   128,
	   //DX Format
	   4,
	   8,
	   //Depth
	   32,
	   32,
	};


	namespace GRendererResource
	{
		Boolean					InitAPIDependencyResource()
		{
			ShaderManager = new GShaderManager;

			const String strVertexShader3D = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVVertexShaderTemplate.msr");
			const String strVertexShader2D = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVVertexShader2D.msr");

			D3D11_INPUT_ELEMENT_DESC* arrDesc[(const UInt8)PrimitiveIAFactory::ECount] = {
				GInputDescStatic,//Line No Instance
				GInputDescStatic_Instance,
				GInputDescSkin,
				GInputDesc2D
			};

			UInt32 arrDescElementCount[(const UInt8)PrimitiveIAFactory::ECount] = {
				ARRAY_ELEMENT_COUNT(GInputDescStatic),
				ARRAY_ELEMENT_COUNT(GInputDescStatic_Instance),
				ARRAY_ELEMENT_COUNT(GInputDescSkin),
				ARRAY_ELEMENT_COUNT(GInputDesc2D),
			};

			const Char* arrShaderTemplateFileName[(const UInt8)PrimitiveIAFactory::ECount] = {
				strVertexShader3D.CString(),
				strVertexShader3D.CString(),
				strVertexShader3D.CString(),
				strVertexShader2D.CString()
			};

			UInt32 primitiveTypeMap[(const UInt8)PrimitiveIAFactory::ECount] = {
				0,
				0,
				1,
				2
			};

			ShaderCompiledResult VS;
			for (UInt32 uDomain = 0; uDomain < (const UInt32)MaterialDomain::ECount; uDomain++)
			{
				for (UInt32 uIAFactory = 0; uIAFactory < (const UInt32)PrimitiveIAFactory::ECount; uIAFactory++)
				{
					String strCacheName = String::Format("DefaultVertexShader_%d_%d.msr", uIAFactory, uDomain);

					ShaderMacro macros;
					if (!GRendererResource::ShaderManager->CompileShader(
						strCacheName.CString(),
						macros, VS, nullptr, 0, ShaderType::EVertexShader, False))
					{
						IFile* pShaderFile = IFile::Open(arrShaderTemplateFileName[uIAFactory], IFile::EFDT_Text);
						if (pShaderFile == nullptr)return False;

						Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
						pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

						String strDomain = String::Format("%d", uDomain);
						macros.AddMacro("DOMAIN", strDomain.CString());
						switch ((PrimitiveIAFactory)uIAFactory)
						{
						case PrimitiveIAFactory::EStatic_Instance:
							macros.AddMacro("INSTANCE_RENDER", "1");
							break;
						case PrimitiveIAFactory::ESkin:
						case PrimitiveIAFactory::EStatic:
						case PrimitiveIAFactory::EStatic2D:
							macros.AddMacro("INSTANCE_RENDER", "0");
							break;
						}
						String strPrimitiveType = String::Format("%d", primitiveTypeMap[uIAFactory]);
						macros.AddMacro("PRIMITIVE_TYPE", strPrimitiveType.CString());
						String strTransformCount = String::Format("%d", GVarMaxGPUBlendMatrixPerPass.nValue);
						macros.AddMacro("MAX_TRANSFORMS_COUNT", strTransformCount.CString());

						String strCode = String::Format(pBuf, "", "");//Template With No Custom Code

						delete[] pBuf;
						pShaderFile->Close();

						GRendererResource::ShaderManager->CompileShader(
							strCacheName.CString(),
							macros, VS, strCode.CString(), strCode.Length(), ShaderType::EVertexShader, False);
					}

					DefaultVertexShader[uIAFactory][uDomain] = (ID3D11VertexShader*)((GShaderManager*)GRendererResource::ShaderManager)->GetShaderObject(VS);
					if (uDomain == 0)
					{
						if (FAILED(GRenderer::Device->CreateInputLayout(
							arrDesc[uIAFactory],
							arrDescElementCount[uIAFactory],
							VS.pCode, VS.uSize, &GRendererResource::DefaultInputLayout[uIAFactory])))
						{
							return False;
						}
					}
				}
			}
			return True;
		}
		Void					ReleaseAPIDependencyResource()
		{
			for (UInt32 u = 0; u < IGPrimitive::Type::EPrimT_Invalid; u++)
			{
				MX_RELEASE_INTERFACE(DefaultInputLayout[u]);
			}
			MX_DELETE(ShaderManager);
		}

		IGBuffer*				CreateBuffer(BufferType eType, Boolean bDynamic, UInt32 uSize)
		{
			ID3D11Buffer* pBuffer = NULL;
			D3D11_BUFFER_DESC desc;
			switch (eType)
			{
			case BufferType::EVertexBuffer:
				desc.BindFlags = D3D11_BIND_VERTEX_BUFFER; break;
			case BufferType::EIndexBuffer:
				desc.BindFlags = D3D11_BIND_INDEX_BUFFER; break;
			case BufferType::EConstantBuffer:
				desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER; break;
			default:
				return nullptr;
			}

			desc.CPUAccessFlags = bDynamic ? D3D11_CPU_ACCESS_WRITE : 0;
			desc.Usage = bDynamic ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT;
			desc.ByteWidth = Math::AlignmentedSize(uSize, 64);
			desc.StructureByteStride = 0;
			desc.MiscFlags = 0;
			if (FAILED(GRenderer::Device->CreateBuffer(&desc, NULL, &pBuffer)))
			{
				return nullptr;
			}

			GBuffer* pNewBuffer = new GBuffer;
			pNewBuffer->Real = pBuffer;
			Resources.Add(pNewBuffer);

			GRendererResource::FrameStatistics.uGPUMemoryOccupy += desc.ByteWidth;
			return pNewBuffer;
		}
		IGSampler*				GetSampler(TextureAddressMode eAddressMode, TextureFilterMode eFilterMode, UInt32 uMaxAnisotropicLevel)
		{
			UInt32 ID = (eFilterMode & 0xfff) | ((eAddressMode & 0xf) << 12) | ((uMaxAnisotropicLevel & 0xff) << 16);

			if (m_mapSState[ID] == nullptr)
			{
				ID3D11SamplerState* pNewState = nullptr;

				D3D11_TEXTURE_ADDRESS_MODE am = D3D11_TEXTURE_ADDRESS_WRAP;
				switch (eAddressMode)
				{
				case TextureAddressMode::ETexAM_Clamp:
					am = D3D11_TEXTURE_ADDRESS_CLAMP;
					break;
				case TextureAddressMode::ETexAM_Repeat:
					am = D3D11_TEXTURE_ADDRESS_WRAP;
					break;
				case TextureAddressMode::ETexAM_Mirror:
					am = D3D11_TEXTURE_ADDRESS_MIRROR;
					break;
				}

				D3D11_FILTER fm = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
				switch (eFilterMode)
				{
				case TextureFilterMode::ETexFM_Point:
					fm = D3D11_FILTER_MIN_MAG_MIP_POINT;;
					break;
				case TextureFilterMode::ETexFM_Bilinear:
					fm = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
					break;
				case TextureFilterMode::ETexFM_Anisotropic:
					fm = D3D11_FILTER_ANISOTROPIC;
					break;
				}

				D3D11_SAMPLER_DESC desc;
				desc.AddressU = am;
				desc.AddressV = am;
				desc.AddressW = am;
				desc.BorderColor[0] = 0;
				desc.BorderColor[1] = 0;
				desc.BorderColor[2] = 0;
				desc.BorderColor[3] = 0;
				desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
				desc.Filter = fm;
				desc.MaxAnisotropy = uMaxAnisotropicLevel;
				desc.MinLOD = 0.0f;
				desc.MaxLOD = 1.0f;
				desc.MipLODBias = 0.0f;

				if (FAILED(GRenderer::Device->CreateSamplerState(&desc, &pNewState)))
					return nullptr;

				GSampler* pNew = new GSampler;
				pNew->Real = pNewState;
				m_mapSState[ID] = pNew;
				Resources.Add(pNew);
				return pNew;
			}

			return m_mapSState[ID];
		}

		ID3D11RasterizerState* RasterizerState(
			D3D11_CULL_MODE eCullMode,
			Boolean bWireFrame,
			Boolean bDepthClip,
			UInt32 uDepthBias,
			Boolean bScissor)
		{
			String str = String::Format("%d%d%d%d%d", eCullMode, bWireFrame, bDepthClip, uDepthBias, bScissor);
			if (m_mapRState.find(str) == m_mapRState.end())
			{
				D3D11_RASTERIZER_DESC rs_desc;
				memset(&rs_desc, 0, sizeof(rs_desc));
				rs_desc.CullMode = eCullMode;
				rs_desc.FillMode = bWireFrame ? D3D11_FILL_WIREFRAME : D3D11_FILL_SOLID;
				rs_desc.FrontCounterClockwise = FALSE;
				rs_desc.MultisampleEnable = FALSE;
				rs_desc.ScissorEnable = bScissor;
				rs_desc.SlopeScaledDepthBias = 0;
				rs_desc.AntialiasedLineEnable = TRUE;
				rs_desc.DepthClipEnable = bDepthClip;
				rs_desc.DepthBias = uDepthBias;
				rs_desc.DepthBiasClamp = 0;

				ID3D11RasterizerState *pState;
				if (FAILED(GRenderer::Device->CreateRasterizerState(&rs_desc, &pState)))
				{
					return nullptr;
				}

				m_mapRState[str] = pState;
			}
			return m_mapRState[str];
		}

		ID3D11DepthStencilState* DepthStencilState(
			D3D11_COMPARISON_FUNC eDepthOp,
			Boolean bDepthWrite,
			Boolean bStencil,
			UInt32	uStencilReadMask,
			UInt32	uStencilWriteMask)
		{
			String str = String::Format("%d%d%d%d%d", eDepthOp, bDepthWrite, bStencil, uStencilReadMask, uStencilWriteMask);
			if (m_mapDSState.find(str) == m_mapDSState.end())
			{
				D3D11_DEPTH_STENCIL_DESC ds_desc;
				ds_desc.DepthEnable = True;/* eDepthOp != D3D11_COMPARISON_ALWAYS && eDepthOp != D3D11_COMPARISON_NEVER;*/
				ds_desc.DepthWriteMask = bDepthWrite ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;
				ds_desc.DepthFunc = eDepthOp;
				ds_desc.StencilEnable = bStencil;
				ds_desc.StencilReadMask = uStencilReadMask;
				ds_desc.StencilWriteMask = uStencilWriteMask;
				ds_desc.BackFace = { D3D11_STENCIL_OP_KEEP,D3D11_STENCIL_OP_KEEP,D3D11_STENCIL_OP_KEEP,D3D11_COMPARISON_NEVER };
				ds_desc.FrontFace = { D3D11_STENCIL_OP_KEEP,D3D11_STENCIL_OP_KEEP,D3D11_STENCIL_OP_KEEP,D3D11_COMPARISON_NEVER };

				ID3D11DepthStencilState *pState;
				if (FAILED(GRenderer::Device->CreateDepthStencilState(&ds_desc, &pState)))
				{
					return nullptr;
				}
				m_mapDSState[str] = pState;
			}
			return m_mapDSState[str];
		}

		ID3D11BlendState* BlendState(
			Boolean bBlend,
			D3D11_BLEND_OP eAlphaOp,
			D3D11_BLEND_OP eColorOp,
			D3D11_BLEND eAlphaSrc,
			D3D11_BLEND eAlphaDest,
			D3D11_BLEND eColorSrc,
			D3D11_BLEND eColorDest,
			UInt8	uWriteMask)
		{
			String str = String::Format("%d%d%d%d%d%d%d%d", bBlend, eAlphaOp, eColorOp, eAlphaSrc, eAlphaDest, eColorSrc, eColorDest, uWriteMask);
			if (m_mapBState.find(str) == m_mapBState.end())
			{
				D3D11_BLEND_DESC bs_desc;
				bs_desc.AlphaToCoverageEnable = FALSE;
				bs_desc.IndependentBlendEnable = FALSE;
				bs_desc.RenderTarget[0].BlendEnable = bBlend;
				bs_desc.RenderTarget[0].RenderTargetWriteMask = uWriteMask;

				bs_desc.RenderTarget[0].BlendOpAlpha = eAlphaOp;
				bs_desc.RenderTarget[0].DestBlendAlpha = eAlphaDest;
				bs_desc.RenderTarget[0].SrcBlendAlpha = eAlphaSrc;

				bs_desc.RenderTarget[0].BlendOp = eColorOp;
				bs_desc.RenderTarget[0].DestBlend = eColorDest;
				bs_desc.RenderTarget[0].SrcBlend = eColorSrc;

				ID3D11BlendState *pState;
				if (FAILED(GRenderer::Device->CreateBlendState(&bs_desc, &pState)))
				{
					return nullptr;
				}
				m_mapBState[str] = pState;
			}
			return m_mapBState[str];
		}

		ID3D11ShaderResourceView*	GetShaderResource(const IGTexture* pTexture)
		{
			auto& it = m_mapViews[pTexture];
			if (it.SRV == nullptr)
			{
				GTexture* pReal = (GTexture*)pTexture;

				D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
				srv_desc.Format = PixelFormatMap[ResourceType::ERT_ShaderResourceView][pReal->GetFormat()];
				switch (pReal->GetType())
				{
				case TextureType::ETexture1D:
					srv_desc.ViewDimension = D3D_SRV_DIMENSION_TEXTURE1D;
					srv_desc.Texture1D.MipLevels = (UINT)-1; //-1 ��ʾʹ�������е�mip�����ж���ʹ�ö��٣�  
					srv_desc.Texture1D.MostDetailedMip = 0; //ָ���ϸ��mip�㣬0��ʾ�߲�  
					break;
				case TextureType::ETexture2D:
					srv_desc.ViewDimension = D3D_SRV_DIMENSION_TEXTURE2D;
					srv_desc.Texture2D.MipLevels = (UINT)-1; //-1 ��ʾʹ�������е�mip�����ж���ʹ�ö��٣�  
					srv_desc.Texture2D.MostDetailedMip = 0; //ָ���ϸ��mip�㣬0��ʾ�߲�  
					break;
				case TextureType::ETextureCube:
					srv_desc.ViewDimension = D3D_SRV_DIMENSION_TEXTURECUBE;
					srv_desc.TextureCube.MipLevels = (UINT)-1; //-1 ��ʾʹ�������е�mip�����ж���ʹ�ö��٣�  
					srv_desc.TextureCube.MostDetailedMip = 0; //ָ���ϸ��mip�㣬0��ʾ�߲�  
					break;
				case TextureType::ETexture3D:
					srv_desc.ViewDimension = D3D_SRV_DIMENSION_TEXTURE3D;
					srv_desc.Texture3D.MipLevels = (UINT)-1; //-1 ��ʾʹ�������е�mip�����ж���ʹ�ö��٣�  
					srv_desc.Texture3D.MostDetailedMip = 0; //ָ���ϸ��mip�㣬0��ʾ�߲�  
					break;
				}
				//D3D_SRV_DIMENSION_TEXTURECUBEARRAY

				GRenderer::Device->CreateShaderResourceView(
					pReal->GetGResurce(), &srv_desc, &it.SRV);

				ASSERT_IF_FAILED(it.SRV != nullptr);
			}
			return it.SRV;
		}
		ID3D11UnorderedAccessView*	GetUnorderedAccess(const IGTexture* pTexture, UInt32 uSubresource)
		{
			GTexture* pReal = (GTexture*)pTexture;
			ASSERT_IF_FAILED(pReal->UnorderAccessable());
			auto& it = m_mapViews[pTexture];
			if (it.UAVs == nullptr)
			{
				const UInt32 uUAVCount = pReal->GetDepth() * pReal->GetMipmapCount();
				it.UAVs = new ID3D11UnorderedAccessView*[uUAVCount] { nullptr, };
				UInt32 uUAVIndex = 0;

				D3D11_UNORDERED_ACCESS_VIEW_DESC uav_desc;
				uav_desc.Format = PixelFormatMap[ResourceType::ERT_RenderTargetView][pReal->GetFormat()];
				switch (pReal->GetType())
				{
				case TextureType::ETexture1D:
					uav_desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE1D;
					for (UInt32 u = 0; u < pReal->GetMipmapCount(); u++)
					{
						uav_desc.Texture1D.MipSlice = u;
						ASSERT_IF_FAILED(SUCCEEDED(GRenderer::Device->CreateUnorderedAccessView(pReal->GetGResurce(), &uav_desc, &it.UAVs[uUAVIndex])));
						uUAVIndex++;
					}
					break;
				case TextureType::ETexture2D:
					uav_desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
					for (UInt32 u = 0; u < pReal->GetMipmapCount(); u++)
					{
						uav_desc.Texture2D.MipSlice = u;
						ASSERT_IF_FAILED(SUCCEEDED(GRenderer::Device->CreateUnorderedAccessView(pReal->GetGResurce(), &uav_desc, &it.UAVs[uUAVIndex])));
						uUAVIndex++;
					}
					break;
				case TextureType::ETextureCube:
					uav_desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2DARRAY;
					uav_desc.Texture2DArray.ArraySize = 1;
					for (UInt32 uMip = 0; uMip < pReal->GetMipmapCount(); uMip++)
					{
						uav_desc.Texture2DArray.MipSlice = uMip;
						for (UInt32 uFace = 0; uFace < pReal->GetDepth(); uFace++)
						{
							uav_desc.Texture2DArray.FirstArraySlice = uFace;
							ASSERT_IF_FAILED(SUCCEEDED(GRenderer::Device->CreateUnorderedAccessView(pReal->GetGResurce(), &uav_desc, &it.UAVs[uUAVIndex])));
							uUAVIndex++;
						}
					}
					break;
				case TextureType::ETexture3D:
					uav_desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE3D;
					uav_desc.Texture3D.WSize = 0;
					for (UInt32 uMip = 0; uMip < pReal->GetMipmapCount(); uMip++)
					{
						uav_desc.Texture3D.MipSlice = uMip;
						for (UInt32 uFace = 0; uFace < pReal->GetDepth(); uFace++)
						{
							uav_desc.Texture3D.FirstWSlice = uFace;
							ASSERT_IF_FAILED(SUCCEEDED(GRenderer::Device->CreateUnorderedAccessView(pReal->GetGResurce(), &uav_desc, &it.UAVs[uUAVIndex])));
							uUAVIndex++;
						}
					}
					break;
				}
			}
			return it.UAVs[uSubresource];
		}
		ID3D11RenderTargetView*		GetRenderTarget(const IGTexture* pTexture, UInt32 uSubresource)
		{
			GTexture* pReal = (GTexture*)pTexture;
			ASSERT_IF_FAILED(pReal->Targetable());
			auto& it = m_mapViews[pTexture];
			if (it.RTVs == nullptr)
			{
				const UInt32 uRTVCount = pReal->GetDepth() * pReal->GetMipmapCount();
				it.RTVs = new ID3D11RenderTargetView*[uRTVCount] { nullptr, };
				UInt32 uRTVIndex = 0;

				D3D11_RENDER_TARGET_VIEW_DESC rtv_desc;
				rtv_desc.Format = PixelFormatMap[ResourceType::ERT_RenderTargetView][pReal->GetFormat()];
				switch (pReal->GetType())
				{
				case TextureType::ETexture1D:
					rtv_desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE1D;
					for (UInt32 u = 0; u < pReal->GetMipmapCount(); u++)
					{
						rtv_desc.Texture1D.MipSlice = u;
						ASSERT_IF_FAILED(SUCCEEDED(GRenderer::Device->CreateRenderTargetView(pReal->GetGResurce(), &rtv_desc, &it.RTVs[uRTVIndex])));
						uRTVIndex++;
					}
					break;
				case TextureType::ETexture2D:
					rtv_desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
					for (UInt32 u = 0; u < pReal->GetMipmapCount(); u++)
					{
						rtv_desc.Texture2D.MipSlice = u;
						ASSERT_IF_FAILED(SUCCEEDED(GRenderer::Device->CreateRenderTargetView(pReal->GetGResurce(), &rtv_desc, &it.RTVs[uRTVIndex])));
						uRTVIndex++;
					}
					break;
				case TextureType::ETextureCube:
					rtv_desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
					rtv_desc.Texture2DArray.ArraySize = 1;
					for (UInt32 uMip = 0; uMip < pReal->GetMipmapCount(); uMip++)
					{
						rtv_desc.Texture2DArray.MipSlice = uMip;
						for (UInt32 uFace = 0; uFace < pReal->GetDepth(); uFace++)
						{
							rtv_desc.Texture2DArray.FirstArraySlice = uFace;
							ASSERT_IF_FAILED(SUCCEEDED(GRenderer::Device->CreateRenderTargetView(pReal->GetGResurce(), &rtv_desc, &it.RTVs[uRTVIndex])));
							uRTVIndex++;
						}
					}
					break;
				case TextureType::ETexture3D:
					rtv_desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE3D;
					rtv_desc.Texture3D.WSize = 0;
					for (UInt32 uMip = 0; uMip < pReal->GetMipmapCount(); uMip++)
					{
						rtv_desc.Texture3D.MipSlice = uMip;
						for (UInt32 uFace = 0; uFace < pReal->GetDepth(); uFace++)
						{
							rtv_desc.Texture3D.FirstWSlice = uFace;
							ASSERT_IF_FAILED(SUCCEEDED(GRenderer::Device->CreateRenderTargetView(pReal->GetGResurce(), &rtv_desc, &it.RTVs[uRTVIndex])));
							uRTVIndex++;
						}
					}
					break;
				}
			}
			return it.RTVs[uSubresource];
		}
		ID3D11DepthStencilView*		GetDepthTarget(const IGTexture* pTexture, UInt32 uSubresource)
		{
			GTexture* pReal = (GTexture*)pTexture;
			ASSERT_IF_FAILED(pReal->Targetable());
			auto& it = m_mapViews[pTexture];
			if (it.DSVs == nullptr)
			{
				const UInt32 uDSVCount = pReal->GetDepth() * pReal->GetMipmapCount();
				it.DSVs = new ID3D11DepthStencilView*[uDSVCount] { nullptr, };
				UInt32 uDSVIndex = 0;

				D3D11_DEPTH_STENCIL_VIEW_DESC dsv_desc;
				dsv_desc.Flags = 0;
				dsv_desc.Format = PixelFormatMap[ResourceType::ERT_DepthStencilTargetView][pReal->GetFormat()];
				dsv_desc.Texture2D.MipSlice = 0;
				switch (pReal->GetType())
				{
				case TextureType::ETexture1D:
					dsv_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE1D;
					for (UInt32 u = 0; u < pReal->GetMipmapCount(); u++)
					{
						dsv_desc.Texture2D.MipSlice = u;
						GRenderer::Device->CreateDepthStencilView(pReal->GetGResurce(), &dsv_desc, &it.DSVs[uDSVIndex]);
						uDSVIndex++;
					}
					break;
				case TextureType::ETexture2D:
					dsv_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
					for (UInt32 u = 0; u < pReal->GetMipmapCount(); u++)
					{
						dsv_desc.Texture2D.MipSlice = u;
						GRenderer::Device->CreateDepthStencilView(pReal->GetGResurce(), &dsv_desc, &it.DSVs[uDSVIndex]);
						uDSVIndex++;
					}
					break;
				case TextureType::ETextureCube:
					dsv_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
					dsv_desc.Texture2DArray.ArraySize = 1;
					for (UInt32 uMip = 0; uMip < pReal->GetMipmapCount(); uMip++)
					{
						dsv_desc.Texture2DArray.MipSlice = uMip;
						for (UInt32 uFace = 0; uFace < pReal->GetDepth(); uFace++)
						{
							dsv_desc.Texture2DArray.FirstArraySlice = uFace;
							GRenderer::Device->CreateDepthStencilView(pReal->GetGResurce(), &dsv_desc, &it.DSVs[uDSVIndex]);
							uDSVIndex++;
						}
					}
					break;
				}
			}
			return it.DSVs[uSubresource];
		}
		Void						ReleaseTextureViews(const IGTexture* pTexture)
		{
			auto it = m_mapViews.find(pTexture);
			if (it != m_mapViews.end())
			{
				TextureViews& views = it->second;
				MX_RELEASE_INTERFACE(views.SRV);

				const UInt32 uRTVorDSVCount = pTexture->GetDepth() * pTexture->GetMipmapCount();
				if (views.RTVs != nullptr)
				{
					for (UInt32 u = 0; u < uRTVorDSVCount; u++)
					{
						MX_RELEASE_INTERFACE(views.RTVs[u]);
					}
					delete[] views.RTVs;
					ASSERT_IF_FAILED(views.DSVs == nullptr);
				}
				else if (views.DSVs != nullptr)
				{
					for (UInt32 u = 0; u < uRTVorDSVCount; u++)
					{
						MX_RELEASE_INTERFACE(views.DSVs[u]);
					}
					delete[] views.DSVs;
					ASSERT_IF_FAILED(views.RTVs == nullptr);
				}
				
				if (views.UAVs != nullptr)
				{
					for (UInt32 u = 0; u < uRTVorDSVCount; u++)
					{
						MX_RELEASE_INTERFACE(views.UAVs[u]);
					}
					delete[] views.UAVs;
				}
				m_mapViews.erase(pTexture);
			}
		}

		UInt32 uResourceID;
		Array<IGResource*> Resources;

		std::map<const IGTexture*, TextureViews> m_mapViews;
		std::map<String, ID3D11RasterizerState*> m_mapRState;
		std::map<String, ID3D11DepthStencilState*> m_mapDSState;
		std::map<String, ID3D11BlendState*> m_mapBState;
		std::map<UInt32, IGSampler*> m_mapSState;

		ID3D11InputLayout*	DefaultInputLayout[(const UInt32)PrimitiveIAFactory::ECount] = { nullptr, };
		ID3D11VertexShader* DefaultVertexShader[(const UInt32)PrimitiveIAFactory::ECount][(const UInt32)MaterialDomain::ECount] = { nullptr, };
	}

	IGRenderWindow*		GraphicsResource::CreateRenderWindow(const IGRenderWindow::Description& desc)
	{
		GRenderWindow* pTemp = new GRenderWindow(GRendererResource::uResourceID);
		if (!pTemp->Init(desc))
		{
			Log::Output("GraphicsD3D11", "RenderWindow Create Failed");
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		return pTemp;
	}
	IGTexture*			GraphicsResource::CreateTexture(const IGTexture::Description& desc)
	{
		GTexture* pTemp = new GTexture(GRendererResource::uResourceID);
		if (!pTemp->Init(desc))
		{
			Log::Output("GraphicsD3D11", "Texture Create Failed");
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		return pTemp;
	}
	IGPrimitive*		GraphicsResource::CreatePrimitive(const IGPrimitive::Description& desc)
	{
		GPrimitive* pTemp = nullptr;
		switch (desc.eType)
		{
		case IGPrimitive::EPrimT_Line:pTemp = new GPrimitiveLine(GRendererResource::uResourceID);	break;
		case IGPrimitive::EPrimT_Rigid:	pTemp = new GPrimitiveRigid(GRendererResource::uResourceID); break;
		case IGPrimitive::EPrimT_Skin:pTemp = new GPrimitiveSkin(GRendererResource::uResourceID);	break;
		case IGPrimitive::EPrimT_Rigid2D: pTemp = new GPrimitive2D(GRendererResource::uResourceID); break;
		default:
		{
			Log::Output("GraphicsD3D11", "Invalid Primitive Type");
			return nullptr;
		}
		}

		if (!pTemp->Init(desc.bDynamic, desc.uVertexCount, desc.uIndexCount))
		{
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		return pTemp;
	}
	IGMaterial*			GraphicsResource::CreateMaterial(const IGMaterial::Description& desc, Boolean bReCreate)
	{
		IGMaterial::Description ModifiedDesc = desc;
		const GShaderManager::MaterialCache* cache = nullptr;

		bReCreate &= GRendererResource::HasRawShaderFolder();
		//û��ShaderĿ¼�������´���Shader
		if (!bReCreate)
			cache = GRendererResource::ShaderManager->GetMaterialPropertyCache(desc.strUniqueName.CString());

		if (cache != nullptr)
		{
			ModifiedDesc.eDomain = (MaterialDomain)cache->uDomain;
			ModifiedDesc.eShadingModel = (MaterialShadingModel)cache->uShadingModel;
			ModifiedDesc.eFaceType = (MaterialFaceType)cache->uFaceType;
			ModifiedDesc.eBlendMode = (MaterialBlendMode)cache->uBlendMode;
		}

		GMaterial* pTemp = new GMaterial(GRendererResource::uResourceID);
		if (!pTemp->Init(ModifiedDesc, bReCreate))
		{
			Log::Output("GraphicsD3D11", "Material Create Failed");
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		if (cache == nullptr)
			pTemp->Cache();
		return pTemp;
	}
	IGMaterialParameterBuffer* GraphicsResource::CreateMaterialParameterBuffer(IGMaterial* pMaterial)
	{
		GCommonMaterialParameterBuffer* pBlock = new GCommonMaterialParameterBuffer(GRendererResource::uResourceID);
		if (!((GMaterial*)pMaterial)->ConvertMaterialParameterBlock(pBlock))
		{
			Log::Output("GraphicsD3D11", "MaterialParameterBlock Create Failed");
			delete pBlock;
			return nullptr;
		}
		GRendererResource::Resources.Add(pBlock);
		GRendererResource::uResourceID++;
		return pBlock;
	}
	IGPrimitiveTransformBuffer*GraphicsResource::CreatePrimitiveTransformBuffer(Boolean bForInstance)
	{
		GPrimitiveTransformBuffer* pBlock = new GPrimitiveTransformBuffer(GRendererResource::uResourceID);
		if (!pBlock->Init(bForInstance, GVarMaxGPUBlendMatrixPerPass.nValue))
		{
			Log::Output("GraphicsD3D11", "PrimitiveTransformBlock Create Failed");
			delete pBlock;
			return nullptr;
		}
		GRendererResource::Resources.Add(pBlock);
		GRendererResource::uResourceID++;
		return pBlock;
	}
	IGViewFrustum*		GraphicsResource::CreateViewFrustum(const IGViewFrustum::Description& desc)
	{
		GViewFrustum* pViewFrustum = new GViewFrustum(GRendererResource::uResourceID);
		if (!pViewFrustum->Init(desc))
		{
			Log::Output("GraphicsD3D11", "ViewFrustum Create Failed");
			delete pViewFrustum;
			return nullptr;
		}

		GRendererResource::Resources.Add(pViewFrustum);
		GRendererResource::uResourceID++;
		return pViewFrustum;
	}
}