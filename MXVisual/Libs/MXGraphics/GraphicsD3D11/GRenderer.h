#pragma once
#ifndef _G_RENDERER_
#define _G_RENDERER_
#include "GCommonRenderer.h"
#include "dxgi1_6.h"
#include "d3d11_1.h"
#include "d3dcompiler.h"

#include "MXThread.h"
#include "MXFile.h"
#include "MXLog.h"
#include "MXMath.h"
#include <vector>

#define REVERT_Z 1

#define DEPTH_ALWAYS D3D11_COMPARISON_ALWAYS
#if REVERT_Z
#define DEPTH_EQUAL D3D11_COMPARISON_EQUAL
#define DEPTH_NEARER D3D11_COMPARISON_GREATER_EQUAL
#define DEPTH_FARTHER D3D11_COMPARISON_LESS
#define DEPTH_CLEAR_VALUE 0.0f
#else
#define DEPTH_EQUAL D3D11_COMPARISON_EQUAL
#define DEPTH_NEARER D3D11_COMPARISON_LESS_EQUAL
#define DEPTH_FARTHER D3D11_COMPARISON_GREATER
#define DEPTH_CLEAR_VALUE 1.0f
#endif

#if defined(DEBUG)
#define NAME_API_OBJECT(obj, name)	obj->SetName(#name);
#else
#define NAME_API_OBJECT(obj, name)	
#endif

namespace MXVisual
{
	const UInt32 Max_RTV_Count = 8;
	const UInt32 Max_SRV_Count = 32;

	enum ResourceType
	{
		ERT_Resouce,
		ERT_ShaderResourceView,
		ERT_RenderTargetView,
		ERT_DepthStencilTargetView,
		ERT_Count
	};

	extern DXGI_FORMAT	PixelFormatMap[ERT_Count][EPF_Count];
	extern UInt8		PixelFormatSize[EPF_Count];

	extern D3D11_INPUT_ELEMENT_DESC GInputDesc2D[3];
	extern D3D11_INPUT_ELEMENT_DESC GInputDescStatic[5];
	extern D3D11_INPUT_ELEMENT_DESC GInputDescStatic_Instance[9];
	extern D3D11_INPUT_ELEMENT_DESC GInputDescSkin[7];
	extern D3D11_INPUT_ELEMENT_DESC GInputDescSkin_Instance[11];

	struct GPipeline
	{
		ID3D11InputLayout*			VD = nullptr;
		ID3D11RasterizerState*		RS = nullptr;
		ID3D11DepthStencilState*	DS = nullptr;
		ID3D11BlendState*			BS = nullptr;
		ID3D11VertexShader*			VS = nullptr;
		ID3D11PixelShader*			PS = nullptr;

		ID3D11ComputeShader*		CS = nullptr;

		Boolean	InitForRender(
			PrimitiveIAFactory			eIAFactory,
			TopologyType				eTopology,
			const GRenderPass&			RP,
			ID3D11RasterizerState*		pRS,
			ID3D11DepthStencilState*	pDS,
			ID3D11BlendState*			pBS,
			const ShaderCompiledResult*	pVS,
			const ShaderCompiledResult*	pPS
		);

		Boolean InitForCompute(
			const ShaderCompiledResult*		pCS
		);

		Void	Release();
	};
	struct GBuffer : public IGBuffer
	{
		ID3D11Buffer* Real;
		UInt32		  Stride;

		UInt32 GetUniqueID() const { return -1; }
		~GBuffer()
		{
			MX_RELEASE_INTERFACE(Real);
		}
	};
	struct GSampler : public IGSampler
	{
		ID3D11SamplerState* Real;

		UInt32 GetUniqueID() const { return -1; }
		~GSampler()
		{
			MX_RELEASE_INTERFACE(Real);
		}
	};

	class GShaderManager;

	namespace GRenderer
	{
		//Device Limit Info
		extern UInt32	uMaxGPUResourceSize;

		extern IDXGIFactory6*	DXGIFactory;
		extern ID3D11Device*	Device;

		extern ID3D11DeviceContext* HardwarePipeline;

		extern D3D_FEATURE_LEVEL	FeatureLevel;
	};

	struct TextureViews
	{
		ID3D11ShaderResourceView*	SRV = nullptr;
		ID3D11UnorderedAccessView**	UAVs = nullptr;
		ID3D11RenderTargetView**	RTVs = nullptr;
		ID3D11DepthStencilView**	DSVs = nullptr;
	};
	namespace GRendererResource
	{
		ID3D11RasterizerState* RasterizerState(
			D3D11_CULL_MODE eCullMode = D3D11_CULL_BACK,
			Boolean bWireFrame = False,
			Boolean bDepthClip = True,
			UInt32 uDepthBias = 0,
			Boolean bScissor = False);

		ID3D11DepthStencilState* DepthStencilState(
			D3D11_COMPARISON_FUNC eDepthOp = DEPTH_NEARER,
			Boolean bDepthWrite = True,
			Boolean bStencil = False,
			UInt32	uStencilReadMask = 0,
			UInt32	uStencilWriteMask = 0);

		ID3D11BlendState* BlendState(
			Boolean bBlend = False,
			D3D11_BLEND_OP eAlphaOp = D3D11_BLEND_OP_ADD,
			D3D11_BLEND_OP eColorOp = D3D11_BLEND_OP_ADD,
			D3D11_BLEND eAlphaSrc = D3D11_BLEND_ONE,
			D3D11_BLEND eAlphaDest = D3D11_BLEND_ZERO,
			D3D11_BLEND eColorSrc = D3D11_BLEND_SRC_ALPHA,
			D3D11_BLEND eColorDest = D3D11_BLEND_INV_SRC_ALPHA,
			UInt8	uWriteMask = EWM_AllChannel);

		extern UInt32 uResourceID;
		extern Array<IGResource*> Resources;

		extern std::map<const IGTexture*, TextureViews> m_mapViews;
		ID3D11ShaderResourceView*	GetShaderResource(const IGTexture* pTexture);
		ID3D11UnorderedAccessView*	GetUnorderedAccess(const IGTexture* pTexture, UInt32 uSubresource);
		ID3D11RenderTargetView*		GetRenderTarget(const IGTexture* pTexture, UInt32 uSubresource);
		ID3D11DepthStencilView*		GetDepthTarget(const IGTexture* pTexture, UInt32 uSubresource);
		Void						ReleaseTextureViews(const IGTexture* pTexture);

		extern std::map<String, ID3D11RasterizerState*> m_mapRState;
		extern std::map<String, ID3D11DepthStencilState*> m_mapDSState;
		extern std::map<String, ID3D11BlendState*> m_mapBState;
		extern std::map<UInt32, IGSampler*> m_mapSState;
		extern ID3D11InputLayout*	DefaultInputLayout[(const UInt8)PrimitiveIAFactory::ECount];
		extern ID3D11VertexShader*  DefaultVertexShader[(const UInt8)PrimitiveIAFactory::ECount][(const UInt8)MaterialDomain::ECount];
	};
#define BLENDSTATE_OFF False,\
					D3D11_BLEND_OP_ADD,\
					D3D11_BLEND_OP_ADD,\
					D3D11_BLEND_ONE,\
					D3D11_BLEND_ZERO,\
					D3D11_BLEND_SRC_ALPHA,\
					D3D11_BLEND_INV_SRC_ALPHA

#define BLENDSTATE_ADDITIVE True,\
					 D3D11_BLEND_OP_ADD,\
					 D3D11_BLEND_OP_ADD,\
					 D3D11_BLEND_ZERO,\
					 D3D11_BLEND_ONE,\
					 D3D11_BLEND_SRC_ALPHA,\
					 D3D11_BLEND_ONE

#define BLENDSTATE_BLEND  True,\
					 D3D11_BLEND_OP_ADD,\
					 D3D11_BLEND_OP_ADD,\
					 D3D11_BLEND_ONE,\
					 D3D11_BLEND_ZERO,\
					 D3D11_BLEND_SRC_ALPHA,\
					 D3D11_BLEND_INV_SRC_ALPHA
}

#endif