#include "GShaderManager.h"
#include "GRenderer.h"
#include "MXMath.h"
#include "MXSystem.h"
#include "GMaterial.h"

#include <unordered_map>

namespace MXVisual
{
	class ShaderInclude : public ID3DInclude
	{
	public:
		ShaderInclude() {}
		~ShaderInclude()
		{
			for (auto it = m_mapCacheShaderFileContent.begin();
				it != m_mapCacheShaderFileContent.end();
				it++)
			{
				delete[] it->second->Code;
				delete it->second;
			}
			m_mapCacheShaderFileContent.clear();
		}
		STDMETHOD(Open)(THIS_ D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes)
		{
			auto it = m_mapCacheShaderFileContent.find(pFileName);
			if (it != m_mapCacheShaderFileContent.end())
			{
				*pBytes = it->second->Size;
				*ppData = it->second->Code;
			}
			else
			{
				String strFileName = ENGINE_SHADER_DIR;
				strFileName += pFileName;
				IFile* pFile = IFile::Open(strFileName.CString(), IFile::EFDT_Text);
				if (!pFile)return E_FAIL;

				ShaderData* pNewData = new ShaderData;
				pNewData->Size = pFile->GetFileSize();
				pNewData->Code = new Char[pNewData->Size]{ 0 };
				pNewData->Size = pFile->Read(pNewData->Code, pNewData->Size);
				pFile->Close();

				*pBytes = pNewData->Size;
				*ppData = pNewData->Code;

				m_mapCacheShaderFileContent[pFileName] = pNewData;
			}
			return S_OK;
		}
		STDMETHOD(Close)(THIS_ LPCVOID pData)
		{
			return S_OK;
		}
	protected:
		struct ShaderData
		{
			UInt32 Size = 0;
			Char* Code = nullptr;
		};
		std::unordered_map<std::string, ShaderData*> m_mapCacheShaderFileContent;
	};
	static ShaderInclude g_ShaderInclude;

	GShaderManager::GShaderManager(Boolean bShaderDebug)
		: GCommonShaderManager(bShaderDebug)
	{
		m_strCachePath = ".\\.CachesD3D11";
	}
	GShaderManager::~GShaderManager()
	{
		if (!m_bUpdateToDate)
		{
			//TODO!! Cache Version
			SaveMaterialCache();
			SaveShaderCache();
		}

		for (auto& it : m_mapShaderObjects)
		{
			((ID3D11DeviceChild*)it.second->pShader)->Release();
			delete it.second;
		}
		m_mapShaderObjects.clear();

		for (auto& it : m_mapMaterialCache)
		{
			delete it.second;
		}
		m_mapMaterialCache.clear();

		for (auto& it : m_mapShaderCache)
		{
			delete it.second;
		}
		m_mapShaderCache.clear();
	}

	Boolean GShaderManager::CompileShader(
		const Char* szShaderName,
		ShaderMacro& macros,
		ShaderCompiledResult& result,
		const Void* pShaderCode, UInt32 uShaderSize,
		ShaderType eShaderType,
		Boolean bRecompile)
	{
		if (!GRendererResource::HasRawShaderFolder() || !bRecompile)
		{
			GetShaderCache(szShaderName, result);

			if (result.IsValid())
			{
				CheckAndCreateShaderObjects(eShaderType, result);
				return True;
			}
		}

		if (pShaderCode == nullptr || uShaderSize == 0)return False;

		ID3DBlob *pCompiledResult = nullptr;
		ID3DBlob *pErrorResult = nullptr;

		//FLAG1 
		//D3DCOMPILE_OPTIMIZATION_LEVEL3 For Release, D3DCOMPILE_OPTIMIZATION_LEVEL0 For Debug
		//D3DCOMPILE_WARNINGS_ARE_ERRORS	
		UINT Flag = D3DCOMPILE_OPTIMIZATION_LEVEL3;
#if MX_BUILD_EDITOR
		Flag |= m_bShaderDebug ? D3DCOMPILE_DEBUG : 0;
#endif

		const Char* szType = nullptr;
		const Char* szEntry = nullptr;

		macros.AddMacro("SHADER_PLATFORM", "0");//HLSL
		switch (eShaderType)
		{
		case ShaderType::EVertexShader:
			szType = "vs_5_0";
			szEntry = "VSMain";
			macros.AddMacro("SHADER_TYPE", "0");
			break;
		case ShaderType::EPixelShader:
			szType = "ps_5_0";
			szEntry = "PSMain";
			macros.AddMacro("SHADER_TYPE", "1");
			break;
		case ShaderType::EComputeShader:
			szType = "cs_5_0";
			szEntry = "CSMain";
			macros.AddMacro("SHADER_TYPE", "2");
			break;
		}
		macros.AddMacro(nullptr, nullptr);

		if (FAILED(D3DCompile(pShaderCode, uShaderSize,
			szShaderName,
			(const D3D_SHADER_MACRO*)macros.GetMacroData(),
			&g_ShaderInclude,
			szEntry, szType,
			Flag/*FLAG1*/,
			0,
			&pCompiledResult, &pErrorResult)))
		{
			Log::Output("GraphicsD3D11", (const Char*)pErrorResult->GetBufferPointer());

			MX_RELEASE_INTERFACE(pCompiledResult);
			MX_RELEASE_INTERFACE(pErrorResult);
			return False;
		}

		UInt32 uNewShaderCompiledSize = pCompiledResult->GetBufferSize();
		UInt8* pNewShaderCompiledCode = new UInt8[uNewShaderCompiledSize];
		memcpy(pNewShaderCompiledCode, pCompiledResult->GetBufferPointer(), uNewShaderCompiledSize);

		result.pCode = pNewShaderCompiledCode;
		result.uSize = uNewShaderCompiledSize;

		CacheShader(szShaderName, pNewShaderCompiledCode, uNewShaderCompiledSize);
		CheckAndCreateShaderObjects(eShaderType, result);

		MX_RELEASE_INTERFACE(pCompiledResult);
		MX_RELEASE_INTERFACE(pErrorResult);
		return True;
	}

	Void	GShaderManager::CheckAndCreateShaderObjects(ShaderType eType, const ShaderCompiledResult& CompiledCode)
	{
		if(m_mapShaderObjects.find(CompiledCode.pCode) != m_mapShaderObjects.end())
		{ 
			return;
		}

		ShaderObject* pNewShaderObjet = new ShaderObject;
		pNewShaderObjet->eType = eType;

		switch (eType)
		{
		case MXVisual::ShaderType::EVertexShader:
		{
			ID3D11VertexShader* pVS = nullptr;
			GRenderer::Device->CreateVertexShader(CompiledCode.pCode, CompiledCode.uSize, nullptr, &pVS);
			pNewShaderObjet->pShader = pVS;
			break;
		}
		case MXVisual::ShaderType::EPixelShader:
		{
			ID3D11PixelShader* pPS = nullptr;
			GRenderer::Device->CreatePixelShader(CompiledCode.pCode, CompiledCode.uSize, nullptr, &pPS);
			pNewShaderObjet->pShader = pPS;
			break;
		}
		case MXVisual::ShaderType::EComputeShader:
		{
			ID3D11ComputeShader* pCS = nullptr;
			GRenderer::Device->CreateComputeShader(CompiledCode.pCode, CompiledCode.uSize, nullptr, &pCS);
			pNewShaderObjet->pShader = pCS;
			break;
		}
		}
		m_mapShaderObjects[CompiledCode.pCode] = pNewShaderObjet;
	}

}