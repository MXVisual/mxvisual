#include "GCommandList.h"
#include "GTexture.h"

namespace MXVisual
{
	GCommandList::GCommandList(ID3D11DeviceContext* APICL)
		: APICommandlist(APICL)
	{
#if defined(DEBUG)
		if (FAILED(APICommandlist->QueryInterface(IID_PPV_ARGS(&APIStateMarker))))
		{
			Log::Output("GraphicsD3D11", "Not Support D3D11.1");
		}
#endif
	}
	GCommandList::~GCommandList()
	{
#if defined(DEBUG)
		MX_RELEASE_INTERFACE(APIStateMarker);
#endif
		MX_RELEASE_INTERFACE(APICommandlist);
	}

	Void	GCommandList::SetViewport(const GViewport& Viewport)
	{
		D3D11_VIEWPORT	 vp;
		vp.TopLeftX = Viewport.uX;
		vp.TopLeftY = Viewport.uY;
		vp.Width = Viewport.uWidth;
		vp.Height = Viewport.uHeight;
		vp.MinDepth = Viewport.fMinDepth;
		vp.MaxDepth = Viewport.fMaxDepth;
		APICommandlist->RSSetViewports(1, &vp);
	}
	Void	GCommandList::SetScissor(const Region& Scissor)
	{
		D3D11_RECT	 rect;
		rect.left = Scissor.uLeft;
		rect.top = Scissor.uTop;
		rect.right = Scissor.uRight;
		rect.bottom = Scissor.uBottom;
		APICommandlist->RSSetScissorRects(1, &rect);
	}

	Void	GCommandList::BindPipeline(GPipeline* Pipeline)
	{
		if (Pipeline->CS == nullptr)//Rasterzier
		{
			ASSERT_IF_FAILED(Pipeline->VS != nullptr && Pipeline->PS != nullptr);
			APICommandlist->IASetInputLayout(Pipeline->VD);
			APICommandlist->VSSetShader(Pipeline->VS, nullptr, 0);
			APICommandlist->PSSetShader(Pipeline->PS, nullptr, 0);

			APICommandlist->RSSetState(Pipeline->RS);
			float blend[4] = { 1.0f, 1.0f, 1.0f, 1.0 };
			APICommandlist->OMSetBlendState(Pipeline->BS, blend, 0xffffffff);
			APICommandlist->OMSetDepthStencilState(Pipeline->DS, 0xff);
		}
		else
		{
			ASSERT_IF_FAILED(Pipeline->CS != nullptr);
			APICommandlist->CSSetShader(Pipeline->CS, nullptr, 0);
		}
	}
	Void	GCommandList::BindRenderTarget(
		Boolean bClearDepthTarget,
		const IGTexture* DT, UInt32 uDTSubresourceIdx,
		Boolean bClearColorTarget,
		const IGTexture* RT0, UInt32 uRT0SubresourceIdx,
		const IGTexture* RT1, UInt32 uRT1SubresourceIdx,
		const IGTexture* RT2, UInt32 uRT2SubresourceIdx,
		const IGTexture* RT3, UInt32 uRT3SubresourceIdx,
		const IGTexture* RT4, UInt32 uRT4SubresourceIdx,
		const IGTexture* RT5, UInt32 uRT5SubresourceIdx,
		const IGTexture* RT6, UInt32 uRT6SubresourceIdx,
		const IGTexture* RT7, UInt32 uRT7SubresourceIdx)
	{
		if (DT != nullptr && bClearDepthTarget)
		{
			APICommandlist->ClearDepthStencilView(GRendererResource::GetDepthTarget(DT, uDTSubresourceIdx),
				D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, DEPTH_CLEAR_VALUE, 0);
		}

		static ID3D11RenderTargetView*	RTVs[8] = { nullptr, };
		static Float4					RTVClearColors[8] = { };
		UInt32							uRTVCount = 0;
#define SETUP_COLOR_TARGET(rt, subidx) \
			if (rt != nullptr)\
			{\
				RTVClearColors[uRTVCount] = ((GTexture*)rt)->GetClearValue();\
				RTVs[uRTVCount++] = GRendererResource::GetRenderTarget(rt, subidx);\
			}

		SETUP_COLOR_TARGET(RT0, uRT0SubresourceIdx);
		SETUP_COLOR_TARGET(RT1, uRT1SubresourceIdx);
		SETUP_COLOR_TARGET(RT2, uRT2SubresourceIdx);
		SETUP_COLOR_TARGET(RT3, uRT3SubresourceIdx);
		SETUP_COLOR_TARGET(RT4, uRT4SubresourceIdx);
		SETUP_COLOR_TARGET(RT5, uRT5SubresourceIdx);
		SETUP_COLOR_TARGET(RT6, uRT6SubresourceIdx);
		SETUP_COLOR_TARGET(RT7, uRT7SubresourceIdx);

#undef SETUP_COLOR_TARGET

		if (bClearColorTarget)
		{
			for (UInt32 u = 0; u < uRTVCount; u++)
			{
				APICommandlist->ClearRenderTargetView(RTVs[u], &RTVClearColors[u][0]);
			}
		}

		APICommandlist->OMSetRenderTargets(
			uRTVCount, RTVs,
			DT != nullptr ? GRendererResource::GetDepthTarget(DT, uDTSubresourceIdx) : nullptr);
	}
	Void	GCommandList::BindVertexBuffer(UInt32 uSlot, IGBuffer* pBuffer, UInt32 uStride)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);
		UInt32 s = uStride;
		UInt32 o = 0;
		APICommandlist->IASetVertexBuffers(uSlot, 1, &((GBuffer*)pBuffer)->Real, &s, &o);
	}
	Void	GCommandList::BindIndexBuffer(IGBuffer* pBuffer, Boolean b32Bit)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);

		APICommandlist->IASetIndexBuffer(((GBuffer*)pBuffer)->Real, b32Bit ? DXGI_FORMAT_R32_UINT : DXGI_FORMAT_R16_UINT, 0);
	}

	Void	GCommandList::BindShaderResource(ShaderType eType, UInt32 uSlot, const IGTexture* pTexture)
	{
		ASSERT_IF_FAILED(pTexture != nullptr);
		ID3D11ShaderResourceView* SRV = GRendererResource::GetShaderResource(pTexture);
		switch (eType)
		{
		case MXVisual::ShaderType::EVertexShader:
			APICommandlist->VSSetShaderResources(uSlot, 1, &SRV);
			break;
		case MXVisual::ShaderType::EPixelShader:
			APICommandlist->PSSetShaderResources(uSlot, 1, &SRV);
			break;
		case MXVisual::ShaderType::EComputeShader:
			APICommandlist->CSSetShaderResources(uSlot, 1, &SRV);
			break;
		}
	}
	Void	GCommandList::BindUnorderResource(ShaderType eType, UInt32 uSlot, const IGTexture* pTexture, UInt32 uSubresourceIdx)
	{
		ASSERT_IF_FAILED(pTexture != nullptr);
		ID3D11UnorderedAccessView* UAV = GRendererResource::GetUnorderedAccess(pTexture, uSubresourceIdx);
		switch (eType)
		{
		case MXVisual::ShaderType::EVertexShader:
		case MXVisual::ShaderType::EPixelShader:
			ASSERT_IF_FAILED("Not Supported");
			break;
		case MXVisual::ShaderType::EComputeShader:
			APICommandlist->CSSetUnorderedAccessViews(uSlot, 1, &UAV, nullptr);
			break;
		}
	}
	Void	GCommandList::BindConstantBuffer(ShaderType eType, UInt32 uSlot, const IGBuffer* pBuffer)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);
		ID3D11Buffer* CB = ((GBuffer*)pBuffer)->Real;
		switch (eType)
		{
		case MXVisual::ShaderType::EVertexShader:
			APICommandlist->VSSetConstantBuffers(uSlot, 1, &CB);
			break;
		case MXVisual::ShaderType::EPixelShader:
			APICommandlist->PSSetConstantBuffers(uSlot, 1, &CB);
			break;
		case MXVisual::ShaderType::EComputeShader:
			APICommandlist->CSSetConstantBuffers(uSlot, 1, &CB);
			break;
		}
	}
	Void	GCommandList::BindSampler(ShaderType eType, UInt32 uSlot, const IGSampler* pSampler)
	{
		ASSERT_IF_FAILED(pSampler != nullptr);
		ID3D11SamplerState* Sampler = ((GSampler*)pSampler)->Real;
		switch (eType)
		{
		case MXVisual::ShaderType::EVertexShader:
			APICommandlist->VSSetSamplers(uSlot, 1, &Sampler);
			break;
		case MXVisual::ShaderType::EPixelShader:
			APICommandlist->PSSetSamplers(uSlot, 1, &Sampler);
			break;
		case MXVisual::ShaderType::EComputeShader:
			APICommandlist->CSSetSamplers(uSlot, 1, &Sampler);
			break;
		}
	}

	Void	GCommandList::Draw(UInt32 uVertexCount, UInt32 uInstanceCount, Boolean bTriangleNotLine)
	{
		APICommandlist->IASetPrimitiveTopology(bTriangleNotLine ? D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST : D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
		APICommandlist->DrawInstanced(uVertexCount, uInstanceCount, 0, 0);
		GRendererResource::FrameStatistics.uDrawCall++;
	}
	Void	GCommandList::DrawIndex(UInt32 uIndexCount, UInt32 uIndexOffset, UInt32 uInstanceCount, Boolean bTriangleNotLine)
	{
		APICommandlist->IASetPrimitiveTopology(bTriangleNotLine ? D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST : D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
		APICommandlist->DrawIndexedInstanced(uIndexCount, uInstanceCount, uIndexOffset, 0, 0);
		GRendererResource::FrameStatistics.uDrawCall++;
	}

	Void	GCommandList::Dispatch(UInt32 X, UInt32 Y, UInt32 Z)
	{
		APICommandlist->Dispatch(X, Y, Z);
	}

	Void	GCommandList::Flush()
	{
		APICommandlist->ClearState();
	}

	Void	GCommandList::CopyResource(IGTexture* pDest, IGTexture* pSrc)
	{
		ASSERT_IF_FAILED(pDest != nullptr && pSrc != nullptr);

		APICommandlist->CopyResource(((GTexture*)pDest)->GetGResurce(), ((GTexture*)pSrc)->GetGResurce());
	}

	Boolean	GCommandList::Map(IGTexture* pTexture, UInt32 uSubResouce, Boolean bRead, MapRegion& region)
	{
		D3D11_MAPPED_SUBRESOURCE subresource;
		if (SUCCEEDED(APICommandlist->Map(((GTexture*)pTexture)->GetGResurce(),
			uSubResouce, bRead ? D3D11_MAP_READ : D3D11_MAP_WRITE_DISCARD, 0, &subresource)))
		{
			switch (pTexture->GetType())
			{
			case TextureType::ETexture1D:
				region.uPitch = pTexture->GetWidth();//FIXME!!
				break;
			case TextureType::ETexture2D:
			case TextureType::ETexture3D:
			case TextureType::ETextureCube:
			{
				UInt32 uMipMap = uSubResouce / pTexture->GetDepth();
				UInt32 uHeight = pTexture->GetHeight();
				uHeight = Math::Max<SInt32>(1, uHeight >> uMipMap);

				region.uPitch = Math::Min<SInt32>(subresource.DepthPitch / uHeight, subresource.RowPitch);
				break;
			}
			}
			region.pAddress = subresource.pData;
			return True;
		}
		return False;
	}
	Void	GCommandList::Unmap(IGTexture* pTexture, UInt32 uSubResouce)
	{
		APICommandlist->Unmap(((GTexture*)pTexture)->GetGResurce(), uSubResouce);
	}

	Boolean	GCommandList::Map(IGBuffer* pBuffer, Boolean bRead, MapRegion& region)
	{
		D3D11_MAPPED_SUBRESOURCE subresource;
		if (SUCCEEDED(APICommandlist->Map(((GBuffer*)pBuffer)->Real,
			0, bRead ? D3D11_MAP_READ : D3D11_MAP_WRITE_DISCARD, 0, &subresource)))
		{
			region.pAddress = subresource.pData;
			region.uPitch = subresource.RowPitch;
			return True;
		}
		return False;
	}
	Void	GCommandList::Unmap(IGBuffer* pBuffer)
	{
		APICommandlist->Unmap(((GBuffer*)pBuffer)->Real, 0);
	}

	Boolean	GCommandList::SubmitData(IGTexture* pTexture, UInt32 uSubResouce, UInt32 uSize, const Void* pData)
	{
		ASSERT_IF_FAILED(pTexture != nullptr);
		APICommandlist->UpdateSubresource(
			((GTexture*)pTexture)->GetGResurce(), uSubResouce, nullptr, pData, 0, uSize);
		return True;
	}
	Boolean GCommandList::SubmitData(IGBuffer* pBuffer, UInt32 uSize, const Void* pData)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);
		APICommandlist->UpdateSubresource(
			((GBuffer*)pBuffer)->Real, 0, nullptr, pData, 0, uSize);
		return True;
	}
}