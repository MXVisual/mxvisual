#include "GTexture.h"
#include "GRenderer.h"

namespace MXVisual
{
	GTexture::GTexture(UInt32 UniqueID)
		: m_uUniqueID(UniqueID)
		, m_pResource(nullptr)
		, m_uMappedSubresource(-1)
		, m_bTargetable(False)
		, m_bUnorderAccessable(False)
	{
		m_nRefCount = 1;
	}
	GTexture::~GTexture()
	{
		GRendererResource::ReleaseTextureViews(this);
		MX_RELEASE_INTERFACE(m_pResource);
	}

	Boolean	GTexture::Init(const IGTexture::Description& desc)
	{
		ASSERT_IF_FAILED(desc.uMipLevel > 0 && desc.eType != TextureType::EInvalid && desc.eFormat != EPF_Invalid);

		Boolean bGenTarget = desc.bTargetable | desc.bGenerateMipMap;
		m_bTargetable = bGenTarget;
		m_bUnorderAccessable = desc.bUnorderAccessable;
		m_eType = desc.eType;
		m_eFormat = desc.eFormat;
		m_uWidth = desc.uWidth;
		m_uHeight = desc.uHeight;
		m_uDepth = desc.eType == TextureType::ETextureCube ? 6 : desc.uDepth;
		m_uMipMapCount = Math::Min<UInt32>(desc.uMipLevel, Math::Min<UInt32>(log2(desc.uWidth), log2(desc.uHeight)));
		m_f4ClearValue = (m_eFormat == EPF_D24 || m_eFormat == EPF_D32) ?
			Float4(DEPTH_CLEAR_VALUE, DEPTH_CLEAR_VALUE, DEPTH_CLEAR_VALUE, DEPTH_CLEAR_VALUE) : desc.f4ClearValue;

		if (m_bTargetable && desc.bDynamic)
		{
			Log::Output("GraphicsD3D11", "Targetable Texture Cannot be Dyanmic");
			return False;
		}

		UInt32 BindFlags = D3D11_BIND_SHADER_RESOURCE;
		if (desc.eFormat == EPF_D24 || desc.eFormat == EPF_D32)
		{
			BindFlags |= desc.bTargetable ? D3D11_BIND_DEPTH_STENCIL : 0;
		}
		else
		{
			BindFlags |= desc.bTargetable ? D3D11_BIND_RENDER_TARGET : 0;
		}

		if (m_bUnorderAccessable)
		{
			BindFlags |= D3D11_BIND_UNORDERED_ACCESS;
		}

		switch (desc.eType)
		{
		case TextureType::ETexture1D:
		{
			D3D11_TEXTURE1D_DESC tex_desc;
			tex_desc.CPUAccessFlags = desc.bDynamic ? D3D11_CPU_ACCESS_WRITE : 0;
			tex_desc.Usage = desc.bDynamic ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT;
			tex_desc.ArraySize = m_uDepth;
			tex_desc.MipLevels = m_uMipMapCount;
			tex_desc.MiscFlags = 0;
			tex_desc.Width = m_uWidth;
			tex_desc.Format = PixelFormatMap[ResourceType::ERT_Resouce][m_eFormat];
			tex_desc.BindFlags = BindFlags;

			GRenderer::Device->CreateTexture1D(&tex_desc, nullptr, (ID3D11Texture1D**)&m_pResource);
			break;
		}
		case TextureType::ETexture2D:
		{
			D3D11_TEXTURE2D_DESC tex_desc;
			tex_desc.CPUAccessFlags = desc.bDynamic ? D3D11_CPU_ACCESS_WRITE : 0;
			tex_desc.Usage = desc.bDynamic ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT;
			tex_desc.ArraySize = m_uDepth;
			tex_desc.MipLevels = m_uMipMapCount;
			tex_desc.MiscFlags = 0;
			tex_desc.Width = m_uWidth;
			tex_desc.Height = m_uHeight;
			tex_desc.Format = PixelFormatMap[ResourceType::ERT_Resouce][m_eFormat];
			tex_desc.BindFlags = BindFlags;
			tex_desc.SampleDesc = { 1 ,0 };

			GRenderer::Device->CreateTexture2D(&tex_desc, nullptr, (ID3D11Texture2D**)&m_pResource);
			break;
		}
		case TextureType::ETextureCube:
		{
			D3D11_TEXTURE2D_DESC tex_desc;
			tex_desc.CPUAccessFlags = desc.bDynamic ? D3D11_CPU_ACCESS_WRITE : 0;
			tex_desc.Usage = desc.bDynamic ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT;
			tex_desc.ArraySize = m_uDepth;
			tex_desc.MipLevels = m_uMipMapCount;
			tex_desc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;
			tex_desc.Width = m_uWidth;
			tex_desc.Height = m_uHeight;
			tex_desc.Format = PixelFormatMap[ResourceType::ERT_Resouce][m_eFormat];
			tex_desc.BindFlags = BindFlags;
			tex_desc.SampleDesc = { 1 ,0 };

			GRenderer::Device->CreateTexture2D(&tex_desc, nullptr, (ID3D11Texture2D**)&m_pResource);
			break;
		}
		case TextureType::ETexture3D:
		{
			D3D11_TEXTURE3D_DESC tex_desc;
			tex_desc.CPUAccessFlags = desc.bDynamic ? D3D11_CPU_ACCESS_WRITE : 0;
			tex_desc.Usage = desc.bDynamic ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT;
			tex_desc.MipLevels = m_uMipMapCount;
			tex_desc.MiscFlags = 0;
			tex_desc.Width = m_uWidth;
			tex_desc.Height = m_uHeight;
			tex_desc.Depth = m_uDepth;
			tex_desc.Format = PixelFormatMap[ResourceType::ERT_Resouce][m_eFormat];
			tex_desc.BindFlags = BindFlags;

			GRenderer::Device->CreateTexture3D(&tex_desc, nullptr, (ID3D11Texture3D**)&m_pResource);
			break;
		}
		default:
			break;
		}

		if (m_pResource == nullptr)
		{
			Log::Output("GraphicsD3D11", "Create Texture Resource Failed");
			return False;
		}

		return True;
	}
	Boolean	GTexture::InitFromResource(ID3D11Resource* pResource, const IGTexture::Description& desc)
	{
		Boolean bGenTarget = desc.bTargetable | desc.bGenerateMipMap;

		m_bTargetable = bGenTarget;
		m_eType = desc.eType;
		m_eFormat = desc.eFormat;
		m_uWidth = desc.uWidth;
		m_uHeight = desc.uHeight;
		m_uDepth = desc.uDepth;
		m_uMipMapCount = Math::Min<UInt32>(desc.uMipLevel, Math::Min<UInt32>(log2(desc.uWidth), log2(desc.uHeight)));

		if (bGenTarget && desc.bDynamic)
		{
			Log::Output("GraphicsD3D11", "Texture With Target Cannot be Dyanmic");
			return False;
		}
		m_pResource = pResource;
		return True;
	}

	Boolean	GTexture::Map(UInt32 uDepth, UInt32 uMipMap, MapRegion& region, Boolean bReadBack)
	{
		ASSERT_IF_FAILED(m_uMappedSubresource == -1);
		if (m_uMappedSubresource != -1)return False;

		GRenderer::WaitUntilRendered();
		m_uMappedSubresource = m_uDepth * uMipMap + uDepth;

		return GRenderer::CommandList()->Map(this, m_uMappedSubresource, bReadBack, region);
	}
	Void	GTexture::Unmap()
	{
		ASSERT_IF_FAILED(m_uMappedSubresource != -1);
		GRenderer::WaitUntilRendered();
		GRenderer::CommandList()->Unmap(this, m_uMappedSubresource);
		m_uMappedSubresource = -1;
	}
}