#pragma once
#ifndef _G_MATERIAL_
#define _G_MATERIAL_
#include "GCommonMaterial.h"

namespace MXVisual
{
	class GMaterial : public GCommonMaterial
	{
	public:
		GMaterial(UInt32 UniqueID);
	protected:
		Boolean ScanMaterialParameter(const Array<IGMaterialParam::Description>& arrMaterialParams);

		//Only Call On ShaderCache
		Void	LinkShaderParams(ID3D11ShaderReflection* pShaderReflection,
			Array<GMaterialParam*>* arrDyncParams,
			Array<GMaterialParam*>* arrTexParams,
			Array<GMaterialSamplerDesc>* arrSamplerParams,
			SInt32& uMaterialBufferBeginSlot,
			SInt32& uMaterialTextureBeginSlot);

		Boolean InitPSO();
	};
}

#endif