#include "GRenderWindow.h"
#include "MXGraphicsRenderer.h"
#include "MXLog.h"
#include "GRenderer.h"

#include "MXTimer.h"

namespace MXVisual
{
	std::unordered_map<UInt64, GRenderWindow*> GRenderWindow::m_mapRenderWindows;

	Boolean GRenderWindow::Init(const IGRenderWindow::Description& desc)
	{
		m_uBackbufferCount = desc.uBackbufferCount;
		m_eBackbufferFormat = desc.eBackbufferFormat;
		m_uBackbufferWidth = desc.uBackbufferWidth;
		m_uBackbufferHeight = desc.uBackbufferHeight;

		WNDCLASS wndc;
		wndc.style = CS_HREDRAW | CS_VREDRAW;
		wndc.lpfnWndProc = GRenderWindow::RenderWndProc;
		wndc.cbClsExtra = 0;
		wndc.cbWndExtra = 0;
		wndc.hInstance = NULL;
		wndc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		wndc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wndc.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
		wndc.lpszMenuName = NULL;
		String strWndClassName = String::Format("RenderWindow_%d", m_uUniqueID);
		wndc.lpszClassName = strWndClassName.CString();
		if (FAILED(RegisterClass(&wndc)))
		{
			return False;
		}

		DWORD dwStyle = (desc.uParentWindowHandle == NULL || desc.uParentWindowHandle == -1) ?
			WS_OVERLAPPEDWINDOW : WS_CHILDWINDOW;

		UInt32 uX = 0, uY = 0;

		if (dwStyle == WS_OVERLAPPEDWINDOW)
		{
			UInt32 nW = GetSystemMetrics(SM_CXSCREEN);
			UInt32 nH = GetSystemMetrics(SM_CYSCREEN);
			uX = nW / 2 - m_uBackbufferWidth / 2;
			uY = nH / 2 - m_uBackbufferHeight / 2;
		}

		m_uWindowHandle = (UInt64)CreateWindowEx(
			0,// WS_EX_LAYERED,
			strWndClassName.CString(),
			desc.szWindowName != nullptr ? desc.szWindowName : "RenderWindow",
			dwStyle,
			uX, uY, m_uBackbufferWidth, m_uBackbufferHeight,
			(HWND)desc.uParentWindowHandle, NULL, NULL, &m_mapRenderWindows);

		/*
		SetLayeredWindowAttributes(
			(HWND)m_uWindowHandle,
			0, 0, LWA_COLORKEY);
			*/

		RECT wnd, client;
		GetWindowRect((HWND)m_uWindowHandle, &wnd);
		GetClientRect((HWND)m_uWindowHandle, &client);
		MoveWindow((HWND)m_uWindowHandle,
			wnd.left, wnd.top,
			wnd.right - wnd.left + m_uBackbufferWidth - client.right,
			wnd.bottom - wnd.top + m_uBackbufferHeight - client.bottom, False);

		if (m_uWindowHandle == NULL) return False;

		DXGI_SWAP_CHAIN_DESC sc_desc;
		sc_desc.OutputWindow = (HWND)m_uWindowHandle;
		sc_desc.BufferDesc.Width = m_uBackbufferWidth;
		sc_desc.BufferDesc.Height = m_uBackbufferHeight;
		sc_desc.BufferDesc.Format = PixelFormatMap[ERT_RenderTargetView][m_eBackbufferFormat];
		sc_desc.BufferDesc.RefreshRate = { 1, 60 };
		sc_desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
		sc_desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		sc_desc.BufferCount = m_uBackbufferCount;
		sc_desc.Windowed = True;
		sc_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sc_desc.SampleDesc = { 1, 0 };
		sc_desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		sc_desc.Flags = 0;
		if (FAILED(GRenderer::DXGIFactory->CreateSwapChain(
			GRenderer::Device, &sc_desc, &m_pSwapChain)))
		{
			Log::Output("GraphicsD3D11", "Create SwapChain Failed");
			return False;
		}

		ID3D11Texture2D* pColorTargetResource = nullptr;
		if (FAILED(m_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pColorTargetResource))))
		{
			Log::Output("GraphicsD3D11", "Create SwapChain RenderTarget Failed");
			return False;
		}
		GTexture* pColorTarget = new GTexture(-1);
		IGTexture::Description texDesc;
		texDesc.bGenerateMipMap = False;
		texDesc.bDynamic = False;
		texDesc.bTargetable = True;
		texDesc.eFormat = m_eBackbufferFormat;
		texDesc.eType = TextureType::ETexture2D;
		texDesc.uMipLevel = 1;
		texDesc.uDepth = 1;
		texDesc.uWidth = m_uBackbufferWidth;
		texDesc.uHeight = m_uBackbufferHeight;
		texDesc.f4ClearValue = Float4(0.0f, 0.0f, 0.0f, 0.0f);
		if (!pColorTarget->InitFromResource(pColorTargetResource, texDesc))
		{
			return False;
		}

		GRendererResource::Resources.Add(pColorTarget);
		m_pColorTarget = pColorTarget;

		m_mapRenderWindows.insert(std::pair<UInt64, GRenderWindow*>(m_uWindowHandle, this));
		return True;
	}

	Float2  GRenderWindow::ScreenToWindow(const Float2& ScreenPos) const
	{
		Float2 Res;
		POINT pt = { (LONG)ScreenPos[0], (LONG)ScreenPos[1] };
		ScreenToClient((HWND)m_uWindowHandle, &pt);
		Res = Float2((Float1)pt.x, (Float1)pt.y);
		return Res;
	}

	Void GRenderWindow::Show(Boolean bShow)
	{
		ShowWindow((HWND)m_uWindowHandle, bShow ? SW_SHOW : SW_HIDE);
		UpdateWindow((HWND)m_uWindowHandle);
	}
	Void GRenderWindow::Resize(UInt32 uWidth, UInt32 uHeight)
	{
		if (GetParent((HWND)m_uWindowHandle) != NULL)
		{
			MoveWindow((HWND)m_uWindowHandle, 0, 0, uWidth, uHeight, False);
		}
	}
	Void GRenderWindow::Maximize()
	{
		ShowWindow((HWND)m_uWindowHandle, SW_MAXIMIZE);
	}
	Void GRenderWindow::Minimize()
	{
		ShowWindow((HWND)m_uWindowHandle, SW_MINIMIZE);
	}

	Void GRenderWindow::ResizeTarget(UInt32 uWidth, UInt32 uHeight)
	{
		if (uWidth == m_uBackbufferWidth &&
			uHeight == m_uBackbufferHeight)
			return;

		GRenderer::Execute();
		MX_RELEASE_INTERFACE(m_pColorTarget);

		if (FAILED(m_pSwapChain->ResizeBuffers(m_uBackbufferCount,
			uWidth, uHeight, PixelFormatMap[ERT_Resouce][m_eBackbufferFormat], 0)))
		{
			Log::Output("GraphicsD3D11", "Resize SwapChain RenderTarget Failed");
			return;
		}

		ID3D11Texture2D* pColorTargetResource = nullptr;
		if (FAILED(m_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pColorTargetResource))))
		{
			Log::Output("GraphicsD3D11", "Create SwapChain RenderTarget Failed");
		}

		GTexture* pColorTarget = new GTexture(-1);
		IGTexture::Description texDesc;
		texDesc.bGenerateMipMap = False;
		texDesc.bDynamic = False;
		texDesc.bTargetable = True;
		texDesc.eFormat = m_eBackbufferFormat;
		texDesc.eType = TextureType::ETexture2D;
		texDesc.uMipLevel = 1;
		texDesc.uDepth = 1;
		texDesc.uWidth = m_uBackbufferWidth;
		texDesc.uHeight = m_uBackbufferHeight;
		texDesc.f4ClearValue = Float4(0.0f, 0.0f, 0.0f, 0.0f);
		if (!pColorTarget->InitFromResource(pColorTargetResource, texDesc))
		{
			return;
		}

		GRendererResource::Resources.Add(pColorTarget);
		m_pColorTarget = pColorTarget;

		m_uBackbufferWidth = uWidth;
		m_uBackbufferHeight = uHeight;
	}

	Void GRenderWindow::Present()
	{
		if (m_atomIsPresenting.Value() > 0)return;

#if 0
		m_atomIsPresenting.Exchange(1);

		ThreadManager::AddThreadTask(
			RENDER_THREAD_NAME,
			[this]() {
			GRenderer::WaitUntilRendered();
			if (m_pSwapChain != nullptr)
			{
				m_pSwapChain->Present(GVarEnableVSync.bValue ? 1 : 0, 0);
			}
			m_atomIsPresenting.Exchange(0);
		}, "Present");
#else
		GRenderer::WaitUntilRendered();
		if (m_pSwapChain != nullptr)
		{
			m_pSwapChain->Present(GVarEnableVSync.bValue ? 1 : 0, 0);
		}
#endif
	}

	LRESULT CALLBACK GRenderWindow::RenderWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		static std::unordered_map<UInt64, GRenderWindow*>* m_pMapRenderWindows = nullptr;
		static auto GetMsgWindow = [](HWND hwnd)
		{
			GRenderWindow* pRW = nullptr;
			if (m_pMapRenderWindows != nullptr)
			{
				auto pRenderWindow = m_pMapRenderWindows->find((UInt64)hwnd);
				if (pRenderWindow != m_pMapRenderWindows->end())
				{
					pRW = pRenderWindow->second;
				}
			}
			return pRW;
		};

		GRenderWindow* pMsgRW = nullptr;
		switch (msg)
		{
		case WM_CREATE:
#pragma region MSG_CREATE
		{
			if (m_pMapRenderWindows == nullptr)
			{
				CREATESTRUCT* pCreateStruct = (CREATESTRUCT*)lParam;
				m_pMapRenderWindows = (std::unordered_map<UInt64, GRenderWindow*>*)pCreateStruct->lpCreateParams;
			}
			break;
		}
#pragma endregion
		case WM_SIZE:
#pragma region MSG_SIZE
		{
			pMsgRW = GetMsgWindow(hwnd);
			if (pMsgRW != nullptr)
			{
				UInt32 cx = LOWORD(lParam);
				UInt32 cy = HIWORD(lParam);
				if (cx == 0 || cy == 0)
				{
					pMsgRW->m_bActived = False;
				}
				else
				{
					pMsgRW->ResizeTarget(cx, cy);
				}
			}
			break;
		}
#pragma endregion

		case WM_ACTIVATE:
#pragma region MSG_ACTIVE
		{
			pMsgRW = GetMsgWindow(hwnd);
			if (pMsgRW != nullptr)
			{
				pMsgRW->m_bActived = LOWORD(wParam) == WA_INACTIVE ? False : True;
			}
			break;
		}
		case WM_SETFOCUS:
		{
			pMsgRW = GetMsgWindow(hwnd);
			if (pMsgRW != nullptr)
			{
				pMsgRW->m_bActived = True;
			}
			break;
		}
		case WM_KILLFOCUS:
		{
			pMsgRW = GetMsgWindow(hwnd);
			if (pMsgRW != nullptr)
			{
				pMsgRW->m_bActived = False;
			}
			break;
		}
#pragma endregion
		case WM_PAINT:
		{
			/*
			pMsgRW = GetMsgWindow(hwnd);
			if (pMsgRW != nullptr)
			{
				pMsgRW->Present();
			}*/
			break;
		}
		case WM_MOUSEMOVE:
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_LBUTTONDBLCLK:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
		case WM_RBUTTONDBLCLK:
		case WM_MBUTTONDOWN:
		case WM_MBUTTONUP:
		case WM_MBUTTONDBLCLK:
		case WM_XBUTTONDOWN:
		case WM_XBUTTONUP:
		case WM_XBUTTONDBLCLK:
		case WM_MOUSEWHEEL:
		case WM_MOUSEHWHEEL:

		case WM_SYSKEYDOWN:
		case WM_KEYDOWN:
		case WM_SYSKEYUP:
		case WM_KEYUP:
		case WM_SYSCHAR:
		case WM_CHAR:
		{
			//让父窗口也能接收到按键消息
			HWND hParent = GetParent(hwnd);
			pMsgRW = GetMsgWindow(hwnd);
			if (hParent != NULL && pMsgRW != nullptr)
			{
				PostMessage(hParent, msg, wParam, lParam);
			}
			break;
		}
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
		default:
			break;
		}
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
}