#include "GRenderer.h"
#include "GMaterial.h"
#include "GTexture.h"
#include "MXMath.h"
#include "stdio.h"
#include "MXFile.h"
#include "MXXmlFile.h"
#include "MXGraphicsRenderer.h"
#include "GShaderManager.h"
#include "MXLog.h"

namespace MXVisual
{
	GMaterial::GMaterial(UInt32 UniqueID)
		: GCommonMaterial(UniqueID)
	{
	}

	Boolean GMaterial::ScanMaterialParameter(const Array<IGMaterialParam::Description>& arrMaterialParams)
	{
		ID3D11ShaderReflection* pShaderReflection = nullptr;
		MaterialShaderType LinkShaderParamType = MaterialShaderType::EMaterialShaderCount;
		if (m_eDomain == MaterialDomain::EPostProcess)
		{
			LinkShaderParamType = ECShader;
		}
		else
		{
			if (SUCCEEDED(D3DReflect(
				m_arrShaderCompiledResult[MaterialShaderType::EVShaderStatic].pCode,
				m_arrShaderCompiledResult[MaterialShaderType::EVShaderStatic].uSize,
				IID_ID3D11ShaderReflection, (void**)&pShaderReflection)))
			{

				LinkShaderParams(pShaderReflection, nullptr, nullptr, nullptr,
					m_arrMaterialBufferBeginSlot[(const UInt8)ShaderType::EVertexShader],
					m_arrMaterialTextureBeginSlot[(const UInt8)ShaderType::EVertexShader]);
				MX_RELEASE_INTERFACE(pShaderReflection);
			}

			LinkShaderParamType = EPShaderMaterial;
			ASSERT_IF_FAILED(m_arrShaderCompiledResult[LinkShaderParamType].IsValid());
		}

		if (FAILED(D3DReflect(
			m_arrShaderCompiledResult[LinkShaderParamType].pCode,
			m_arrShaderCompiledResult[LinkShaderParamType].uSize,
			IID_ID3D11ShaderReflection, (void**)&pShaderReflection)))
		{
			return False;
		}

		LinkShaderParams(pShaderReflection, &m_arrDyncParam, &m_arrTexParam, &m_arrSamplerParam,
			m_arrMaterialBufferBeginSlot[(const UInt8)ShaderType::EPixelShader],
			m_arrMaterialTextureBeginSlot[(const UInt8)ShaderType::EPixelShader]);
		MX_RELEASE_INTERFACE(pShaderReflection);

		for (UInt32 u = 0; u < arrMaterialParams.ElementCount(); u++)
		{
			GMaterialParam* param = nullptr;
			for (UInt32 uParam = 0; uParam < m_arrDyncParam.ElementCount(); uParam++)
			{
				if (param != nullptr)break;
				if (strcmp(m_arrDyncParam[uParam]->GetName(), arrMaterialParams[u].strName.CString()) == 0)
				{
					param = m_arrDyncParam[uParam];
				}
			}
			for (UInt32 uParam = 0; uParam < m_arrTexParam.ElementCount(); uParam++)
			{
				if (param != nullptr)break;
				if (strcmp(m_arrTexParam[uParam]->GetName(), arrMaterialParams[u].strName.CString()) == 0)
					param = m_arrTexParam[uParam];
			}
		}
		return True;
	}

	Void	GMaterial::LinkShaderParams(ID3D11ShaderReflection* pShaderReflection,
		Array<GMaterialParam*>* arrDyncParams,
		Array<GMaterialParam*>* arrTexParams,
		Array<GMaterialSamplerDesc>* arrSamplerParams,
		SInt32& uMaterialBufferBeginSlot,
		SInt32& uMaterialTextureBeginSlot)
	{
		if (pShaderReflection == nullptr)return;

		D3D11_SHADER_DESC shaderDesc;
		pShaderReflection->GetDesc(&shaderDesc);
		UInt32 uLastTextureOffset = 0;
		UInt32 uLastSamplerOffset = 0;

		UInt32 uLastUserTextureOffset = 0;
		for (UInt32 uRes = 0; uRes < shaderDesc.BoundResources; uRes++)
		{
			D3D11_SHADER_INPUT_BIND_DESC bindDesc;
			if (FAILED(pShaderReflection->GetResourceBindingDesc(uRes, &bindDesc)))
				continue;

			if (bindDesc.Type == D3D_SIT_CBUFFER &&
				strcmp(bindDesc.Name, "MaterialBuffer") == 0)
			{
				uMaterialBufferBeginSlot = bindDesc.BindPoint;

				if (arrDyncParams == nullptr)continue;

				UInt32 uLastOffset = 0;

				D3D11_SHADER_BUFFER_DESC cbDesc;
				ID3D11ShaderReflectionConstantBuffer* pConstantBuf = pShaderReflection->GetConstantBufferByName(bindDesc.Name);
				pConstantBuf->GetDesc(&cbDesc);

				m_uMaterialBufferSize = cbDesc.Size;
				for (UInt32 uParam = 0; uParam < cbDesc.Variables; uParam++)
				{
					ID3D11ShaderReflectionVariable* pVar = pConstantBuf->GetVariableByIndex(uParam);
					D3D11_SHADER_VARIABLE_DESC varDesc;
					pVar->GetDesc(&varDesc);
					ASSERT_IF_FAILED(uLastOffset <= varDesc.StartOffset);
					uLastOffset = varDesc.StartOffset;

					IGMaterialParam::MaterialParamType vectorType = IGMaterialParam::EMPT_Invalid;
					switch (varDesc.Size / sizeof(Float1))
					{
					case 1: vectorType = IGMaterialParam::EMPT_Float1; break;
					case 2: vectorType = IGMaterialParam::EMPT_Float2; break;
					case 3: vectorType = IGMaterialParam::EMPT_Float3; break;
					case 4: vectorType = IGMaterialParam::EMPT_Float4; break;
					}

					GMaterialParam* pParam = nullptr;
					if (arrDyncParams->ElementCount() <= uParam)
					{
						pParam = new GMaterialParam(varDesc.Name, NULL, vectorType);

						arrDyncParams->Add(pParam);
					}
					ASSERT_IF_FAILED(strcmp((*arrDyncParams)[uParam]->GetName(), varDesc.Name) == 0);
				}
			}
			else if (bindDesc.Type == D3D_SIT_TEXTURE)
			{
				ASSERT_IF_FAILED(uLastTextureOffset <= bindDesc.BindPoint);
				uLastTextureOffset = bindDesc.BindPoint;

				if (arrTexParams != nullptr)
				{
					IGMaterialParam::MaterialParamType textureType = IGMaterialParam::EMPT_Invalid;
					switch (bindDesc.Dimension)
					{
					case D3D_SRV_DIMENSION_TEXTURE1D: textureType = IGMaterialParam::EMPT_Texture1D; break;
					case D3D_SRV_DIMENSION_TEXTURE2D: textureType = IGMaterialParam::EMPT_Texture2D; break;
					case D3D_SRV_DIMENSION_TEXTURE3D: textureType = IGMaterialParam::EMPT_Texture3D; break;
					case D3D_SRV_DIMENSION_TEXTURECUBE: textureType = IGMaterialParam::EMPT_TextureCube; break;
					}

					GMaterialParam* pParam = nullptr;
					if (arrTexParams->ElementCount() <= uLastUserTextureOffset)
					{
						pParam = new GMaterialParam(bindDesc.Name, NULL, textureType);
						arrTexParams->Add(pParam);
					}

					ASSERT_IF_FAILED(strcmp((*arrTexParams)[uLastUserTextureOffset]->GetName(), bindDesc.Name) == 0);
				}

				if (uMaterialTextureBeginSlot == -1)
					uMaterialTextureBeginSlot = uLastTextureOffset;
				uLastUserTextureOffset++;
			}
			else if (bindDesc.Type == D3D_SIT_SAMPLER)
			{
				if (arrSamplerParams == nullptr)continue;

				GMaterialSamplerDesc desc;
				desc.uBindingPoint = bindDesc.BindPoint;
				if (strstr(bindDesc.Name, "ENGINE_") != nullptr)
				{
					desc.uAddressMode = (UInt8)TextureAddressMode::ETexAM_Clamp;
					desc.uFilterMode = (UInt8)TextureFilterMode::ETexFM_Bilinear;
					arrSamplerParams->Add(desc);
				}
				else
				{
					UInt32 a, b;
					sscanf(bindDesc.Name, "sampler_%d_%d", &a, &b);

					desc.uAddressMode = (UInt8)a;
					desc.uFilterMode = (UInt8)b;
					arrSamplerParams->Add(desc);
				}
			}
		}
	}

	Boolean GMaterial::InitPSO()
	{
		Array<PrimitiveRenderStage> RenderStages = GetRenderStages();

		for (UInt32 uIAFactory = 0; uIAFactory < (const UInt32)PrimitiveIAFactory::ECount; uIAFactory++)
		{
			if ((uIAFactory == (const UInt32)PrimitiveIAFactory::EStatic2D && (m_eDomain == MaterialDomain::EDecal || m_eDomain == MaterialDomain::EWorldSpace)) ||
				(uIAFactory != (const UInt32)PrimitiveIAFactory::EStatic2D && (m_eDomain == MaterialDomain::EScreenSpace || m_eDomain == MaterialDomain::EPostProcess)))
			{
				continue;
			}

			for (UInt32 uRenderStage = 0; uRenderStage < RenderStages.ElementCount(); uRenderStage++)
			{
				ASSERT_IF_FAILED(m_eDomain != MaterialDomain::EPostProcess);

				ShaderCompiledResult* VS = nullptr;
				switch ((PrimitiveIAFactory)uIAFactory)
				{
				case PrimitiveIAFactory::EStatic_Instance:
					VS = &m_arrShaderCompiledResult[MaterialShaderType::EVShaderStatic_Instance];
					break;
				case PrimitiveIAFactory::EStatic:
					VS = &m_arrShaderCompiledResult[MaterialShaderType::EVShaderStatic];
					break;
				case PrimitiveIAFactory::ESkin:
					VS = &m_arrShaderCompiledResult[MaterialShaderType::EVShaderSkin];
					break;
				case PrimitiveIAFactory::EStatic2D:
					break;
				}
				if (VS != nullptr && !VS->IsValid())
					VS = nullptr;

				ID3D11DepthStencilState* DS = nullptr;
				ShaderCompiledResult* PS = nullptr;
				switch (RenderStages[uRenderStage])
				{
				case PrimitiveRenderStage::EDepthPrePass:
					PS = &m_arrShaderCompiledResult[MaterialShaderType::EPShaderDepth];
					DS = GRendererResource::DepthStencilState();
					break;
				case PrimitiveRenderStage::EBasePass:
					PS = &m_arrShaderCompiledResult[MaterialShaderType::EPShaderMaterial];
					DS = GRendererResource::DepthStencilState(DEPTH_EQUAL, False);
					break;
				case PrimitiveRenderStage::EForwardTranslucent:
					PS = &m_arrShaderCompiledResult[MaterialShaderType::EPShaderMaterial];
					DS = GRendererResource::DepthStencilState(DEPTH_NEARER, False);
					break;
				case PrimitiveRenderStage::EDecalPass:
					PS = &m_arrShaderCompiledResult[MaterialShaderType::EPShaderMaterial];
					DS = GRendererResource::DepthStencilState(DEPTH_FARTHER, False);
					break;
				case PrimitiveRenderStage::EForward2DPrimitive:
					PS = &m_arrShaderCompiledResult[MaterialShaderType::EPShaderMaterial];
					DS = GRendererResource::DepthStencilState(DEPTH_ALWAYS, False);
					break;
				}
				if (PS == nullptr)
				{
					ASSERT_IF_FAILED("Not Support No PS");
					continue;
				}

				ID3D11RasterizerState* RS = nullptr;
				Boolean bScissor = uRenderStage == (const UInt32)PrimitiveRenderStage::EForward2DPrimitive;
				switch (GetFaceType())
				{
				case MaterialFaceType::EFrontface:
					RS = GRendererResource::RasterizerState(D3D11_CULL_BACK, False, True, 0, bScissor);
					break;
				case MaterialFaceType::EBackface:
					RS = GRendererResource::RasterizerState(D3D11_CULL_FRONT, False, True, 0, bScissor);
					break;
				case MaterialFaceType::ETwoSide:
					RS = GRendererResource::RasterizerState(D3D11_CULL_NONE, False, True, 0, bScissor);
					break;
				case MaterialFaceType::EWireframe:
					RS = GRendererResource::RasterizerState(D3D11_CULL_NONE, True, True, 0, bScissor);
					break;
				}

				ID3D11BlendState* BS = nullptr;
				switch (GetBlendMode())
				{
				case MaterialBlendMode::EOpaque:
				case MaterialBlendMode::EMasked:
				case MaterialBlendMode::EMaskTranslucency:
					BS = GRendererResource::BlendState(BLENDSTATE_OFF);
					break;
				case MaterialBlendMode::EAdditive:
					BS = GRendererResource::BlendState(BLENDSTATE_ADDITIVE);
					break;
				case MaterialBlendMode::ETranslucency:
					BS = GRendererResource::BlendState(BLENDSTATE_BLEND);
					break;
				}

				GRenderPass RP;//D3D11 RenderPass Not Used
				for (UInt32 uTopology = 0; uTopology < (const UInt32)TopologyType::ECount; uTopology++)
				{
					GPipeline* pNewPSO = new GPipeline;
					if (!pNewPSO->InitForRender(
						(PrimitiveIAFactory)uIAFactory,
						(TopologyType)uTopology,
						RP, RS, DS, BS, VS, PS))
					{
						return False;
					}

					const UInt32 uIndex = CalculatePSOIndex(
						(PrimitiveRenderStage)RenderStages[uRenderStage],
						(PrimitiveIAFactory)uIAFactory,
						(TopologyType)uTopology);
					m_arrPSO[uIndex] = pNewPSO;
				}
			}
		}
		return True;
	}
}
