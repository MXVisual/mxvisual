#pragma once
#ifndef _G_SHADER_MANAGER_
#define _G_SHADER_MANAGER_
#include "GCommonShaderManager.h"
#include <unordered_map>

namespace MXVisual
{
	class GShaderManager : public GCommonShaderManager
	{
	public:
		GShaderManager(Boolean bShaderDebug = False);
		~GShaderManager();

		//Return Compile Result
		Boolean CompileShader(
			const Char* szShaderName,
			ShaderMacro& macros,
			ShaderCompiledResult& result,
			const Void* pShaderCode, UInt32 uShaderSize,
			ShaderType eShaderType,
			Boolean bRecompile);

		Void* GetShaderObject(const ShaderCompiledResult& CompiledCode)
		{
			auto FindRes = m_mapShaderObjects.find(CompiledCode.pCode);
			if (FindRes != m_mapShaderObjects.end())
			{
				return FindRes->second->pShader;
			}
			return nullptr;
		}

	protected:
		Void	CheckAndCreateShaderObjects(ShaderType eType, const ShaderCompiledResult& CompiledCode);
		struct ShaderObject
		{
			ShaderType	eType = ShaderType::ECount;
			Void*		pShader = nullptr;
		};
		std::unordered_map<const Void*, ShaderObject*> m_mapShaderObjects;
	};
}

#endif