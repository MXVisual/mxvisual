#include "MXGraphicsRenderer.h"
#include "GRenderer.h"

#include "GRenderWindow.h"
#include "GTexture.h"
#include "GPrimitive.h"
#include "GMaterial.h"
#include "GViewFrustum.h"

#include "GAPIWrapper.h"
#include "GCommandList.h"
#include "GShaderManager.h"

namespace MXVisual
{
	D3D12_INPUT_ELEMENT_DESC GInputDesc2D[3] = {
		{ "POSITION",	0, DXGI_FORMAT_R32G32_FLOAT,		0, 0,	D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 8,	D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0, 24,	D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 }
	};

	D3D12_INPUT_ELEMENT_DESC GInputDescStatic[5] = {
		{ "POSITION",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "NORMAL",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "TANGENT",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,48, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0,64, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	};

	D3D12_INPUT_ELEMENT_DESC GInputDescStatic_Instance[9] = {
		{ "POSITION",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "NORMAL",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "TANGENT",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,48, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0,64, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		//Instance Data
		{ "TRANSFORM",	0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0,	D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA,1 },
		{ "TRANSFORM",	1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16,	D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA,1 },
		{ "TRANSFORM",	2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32,	D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA,1 },
		{ "TRANSFORM",	3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48,	D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA,1 },
	};

	D3D12_INPUT_ELEMENT_DESC GInputDescSkin[7] = {
	   { "POSITION",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "NORMAL",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "TANGENT",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,48, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0,64, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "BLENDINDICES",0, DXGI_FORMAT_R32G32B32A32_UINT,	0,72, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "BLENDWEIGHT", 0,DXGI_FORMAT_R32G32B32A32_FLOAT,	0,88, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	};

	D3D12_INPUT_ELEMENT_DESC GInputDescSkin_Instance[11] = {
	   { "POSITION",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "NORMAL",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "TANGENT",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0,48, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0,64, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "BLENDINDICES",0, DXGI_FORMAT_R32G32B32A32_UINT,	0,72, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   { "BLENDWEIGHT", 0,DXGI_FORMAT_R32G32B32A32_FLOAT,	0,88, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	   //Instance Data
	   { "TRANSFORM",	0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0,  D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA,1 },
	   { "TRANSFORM",	1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA,1 },
	   { "TRANSFORM",	2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA,1 },
	   { "TRANSFORM",	3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA,1 },
	};


	DXGI_FORMAT PixelFormatMap[ERT_Count][EPF_Count + 1] = {
		//EPFT_Resource
		{
			DXGI_FORMAT_R8_UNORM,
			DXGI_FORMAT_R8G8_UNORM,
			DXGI_FORMAT_R8G8B8A8_UNORM,

			DXGI_FORMAT_R16_FLOAT,
			DXGI_FORMAT_R16G16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_UNORM,

			DXGI_FORMAT_R32_FLOAT,
			DXGI_FORMAT_R32G32_FLOAT,
			DXGI_FORMAT_R32G32B32A32_FLOAT,

			DXGI_FORMAT_BC1_UNORM,
			DXGI_FORMAT_BC3_UNORM,
			DXGI_FORMAT_BC4_UNORM,
			DXGI_FORMAT_BC5_UNORM,
			DXGI_FORMAT_BC6H_UF16,
			DXGI_FORMAT_BC7_UNORM,

			DXGI_FORMAT_R24G8_TYPELESS,
			DXGI_FORMAT_R32G8X24_TYPELESS,

			//EPF_Count
			DXGI_FORMAT_UNKNOWN,
		},

		//EPFT_ShaderResourceView
		{
			DXGI_FORMAT_R8_UNORM,
			DXGI_FORMAT_R8G8_UNORM,
			DXGI_FORMAT_R8G8B8A8_UNORM,

			DXGI_FORMAT_R16_FLOAT,
			DXGI_FORMAT_R16G16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_UNORM,

			DXGI_FORMAT_R32_FLOAT,
			DXGI_FORMAT_R32G32_FLOAT,
			DXGI_FORMAT_R32G32B32A32_FLOAT,

			DXGI_FORMAT_BC1_UNORM,
			DXGI_FORMAT_BC3_UNORM,
			DXGI_FORMAT_BC4_UNORM,
			DXGI_FORMAT_BC5_UNORM,
			DXGI_FORMAT_BC6H_UF16,
			DXGI_FORMAT_BC7_UNORM,

			DXGI_FORMAT_R24_UNORM_X8_TYPELESS,
			DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS,

			//EPF_Count
			DXGI_FORMAT_UNKNOWN,
		},

		//EPFT_RenderTargetView
		{
			DXGI_FORMAT_R8_UNORM,
			DXGI_FORMAT_R8G8_UNORM,
			DXGI_FORMAT_R8G8B8A8_UNORM,

			DXGI_FORMAT_R16_FLOAT,
			DXGI_FORMAT_R16G16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_UNORM,

			DXGI_FORMAT_R32_FLOAT,
			DXGI_FORMAT_R32G32_FLOAT,
			DXGI_FORMAT_R32G32B32A32_FLOAT,

			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,

			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,

			//EPF_Count
			DXGI_FORMAT_UNKNOWN,
		},

		//EPFT_DepthStencilTargetView
		{
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,

			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,

			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,

			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,
			DXGI_FORMAT_UNKNOWN,

			DXGI_FORMAT_D24_UNORM_S8_UINT,
			DXGI_FORMAT_D32_FLOAT_S8X24_UINT,

			//EPF_Count
			DXGI_FORMAT_UNKNOWN,
		}
	};
	UInt8		PixelFormatSize[EPF_Count + 1] = {
	   32,
	   32,
	   32,
	   64,
	   64,

	   32,
	   64,
	   128,
	   //DX Format
	   4,
	   8,
	   //Depth
	   32,
	   32,

	   //EPF_Count
	   0,
	};

	namespace GRendererResource
	{
		UInt32 uResourceID;
		Array<IGResource*> Resources;

		ID3D12RootSignature*				RasterizerRootSignature = nullptr;
		ID3D12RootSignature*				ComputeRootSignature = nullptr;

		UInt32								RTVDescriptorSize = 0;
		UInt32								DSVDescriptorSize = 0;
		UInt32								CBVSRVUAVDescriptorSize = 0;
		UInt32								SamplerDescriptorSize = 0;

		GBuffer*							UploadBuffer = nullptr;
		UInt32								FrameBufferIndex = 0;
		Array<FrameBuffer*>					FrameBuffers;
		UInt32								RShaderResourceSetIndex = 0;
		UInt32								CShaderResourceSetIndex = 0;
		Array<RasterizerShaderResourceSet*>	RShaderResourceSet;
		Array<ComputeShaderResourceSet*>	CShaderResourceSet;

		std::map<UInt32, GSamplerDesc*> m_mapSState;

		D3D12_INPUT_LAYOUT_DESC	DefaultInputLayout[(const UInt8)PrimitiveIAFactory::ECount] = { 0, };
		ShaderCompiledResult	DefaultVertexShader[(const UInt8)PrimitiveIAFactory::ECount][(const UInt32)MaterialDomain::ECount] = { nullptr, };

		Boolean					InitRasterizerRootSignature()
		{
			Array<DescriptorRange> ranges;
			ranges.Add(DescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 0, (const UInt8)RasterizerRootSignatureDesc::EVS_CBV_COUNT, (const UInt8)RasterizerRootSignatureDesc::EVS_CBV_BEGIN));
			ranges.Add(DescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 0, (const UInt8)RasterizerRootSignatureDesc::EVS_SRV_COUNT, (const UInt8)RasterizerRootSignatureDesc::EVS_SRV_BEGIN));
			ranges.Add(DescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 0, (const UInt8)RasterizerRootSignatureDesc::EVS_SAMPLER_COUNT, (const UInt8)RasterizerRootSignatureDesc::EVS_SAMPLER_BEGIN));
			ranges.Add(DescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 0, (const UInt8)RasterizerRootSignatureDesc::EPS_CBV_COUNT, (const UInt8)RasterizerRootSignatureDesc::EPS_CBV_BEGIN));
			ranges.Add(DescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 0, (const UInt8)RasterizerRootSignatureDesc::EPS_SRV_COUNT, (const UInt8)RasterizerRootSignatureDesc::EPS_SRV_BEGIN));
			ranges.Add(DescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 0, (const UInt8)RasterizerRootSignatureDesc::EPS_SAMPLER_COUNT, (const UInt8)RasterizerRootSignatureDesc::EPS_SAMPLER_BEGIN));

			Array<RootParam> params;
			params.Add(RootParam(D3D12_SHADER_VISIBILITY_VERTEX, 2, &ranges[0]));
			params.Add(RootParam(D3D12_SHADER_VISIBILITY_VERTEX, 1, &ranges[2]));
			params.Add(RootParam(D3D12_SHADER_VISIBILITY_PIXEL, 2, &ranges[3]));
			params.Add(RootParam(D3D12_SHADER_VISIBILITY_PIXEL, 1, &ranges[5]));

			RootSignature rootsignature(
				D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS,
				params.ElementCount(), params.GetRawData(),
				0, nullptr
			);

			ID3DBlob* signature = nullptr;
			ID3DBlob* errormsg = nullptr;
			if (FAILED(D3D12SerializeRootSignature(&rootsignature, D3D_ROOT_SIGNATURE_VERSION_1_0, &signature, &errormsg)))
			{
				if (errormsg != nullptr)
					Log::Output("GraphicsD3D12", (const Char*)errormsg->GetBufferPointer());

				return False;
			}

			if (FAILED(GRenderer::Device->CreateRootSignature(
				0, signature->GetBufferPointer(), signature->GetBufferSize(),
				IID_PPV_ARGS(&GRendererResource::RasterizerRootSignature))))
			{
				return False;
			}

			NAME_API_OBJECT(GRendererResource::RasterizerRootSignature, "RasterizerRootSignature");
			MX_RELEASE_INTERFACE(signature);
			MX_RELEASE_INTERFACE(errormsg);

			return True;
		}
		Boolean					InitComputeRootSignature()
		{
			Array<DescriptorRange> ranges;
			ranges.Add(DescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 0, (const UInt8)ComputeRootSignatureDesc::ECBV_COUNT, (const UInt8)ComputeRootSignatureDesc::ECBV_BEGIN));
			ranges.Add(DescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 0, (const UInt8)ComputeRootSignatureDesc::ESRV_COUNT, (const UInt8)ComputeRootSignatureDesc::ESRV_BEGIN));
			ranges.Add(DescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 0, (const UInt8)ComputeRootSignatureDesc::EUAV_COUNT, (const UInt8)ComputeRootSignatureDesc::ECBV_BEGIN));
			ranges.Add(DescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 0, (const UInt8)ComputeRootSignatureDesc::ESAMPLER_COUNT, (const UInt8)ComputeRootSignatureDesc::ESAMPLER_BEGIN));

			Array<RootParam> params;
			params.Add(RootParam(D3D12_SHADER_VISIBILITY_ALL, 3, &ranges[0]));
			params.Add(RootParam(D3D12_SHADER_VISIBILITY_ALL, 1, &ranges[3]));

			RootSignature rootsignature(
				D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT,
				params.ElementCount(), params.GetRawData(),
				0, nullptr
			);

			ID3DBlob* signature = nullptr;
			ID3DBlob* errormsg = nullptr;
			HRESULT hr;
			if (FAILED(hr = D3D12SerializeRootSignature(&rootsignature, D3D_ROOT_SIGNATURE_VERSION_1_0, &signature, &errormsg)))
			{
				if (errormsg != nullptr)
					Log::Output("GraphicsD3D12", (const Char*)errormsg->GetBufferPointer());

				return False;
			}

			if (FAILED(GRenderer::Device->CreateRootSignature(
				0, signature->GetBufferPointer(), signature->GetBufferSize(),
				IID_PPV_ARGS(&GRendererResource::ComputeRootSignature))))
			{
				return False;
			}

			NAME_API_OBJECT(GRendererResource::ComputeRootSignature, "ComputeRootSignature");
			MX_RELEASE_INTERFACE(signature);
			MX_RELEASE_INTERFACE(errormsg);

			return True;
		}

		Boolean					InitAPIDependencyResource()
		{
			RTVDescriptorSize = GRenderer::Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
			DSVDescriptorSize = GRenderer::Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
			CBVSRVUAVDescriptorSize = GRenderer::Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
			SamplerDescriptorSize = GRenderer::Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER);

			if (!InitRasterizerRootSignature() ||
				!InitComputeRootSignature())
				return False;

			ShaderManager = new GShaderManager;

			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic].NumElements = ARRAY_ELEMENT_COUNT(GInputDescStatic);
			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic].pInputElementDescs = GInputDescStatic;
			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic_Instance].NumElements = ARRAY_ELEMENT_COUNT(GInputDescStatic_Instance);
			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic_Instance].pInputElementDescs = GInputDescStatic_Instance;

			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::ESkin].NumElements = ARRAY_ELEMENT_COUNT(GInputDescSkin);
			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::ESkin].pInputElementDescs = GInputDescSkin;

			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic2D].NumElements = ARRAY_ELEMENT_COUNT(GInputDesc2D);
			DefaultInputLayout[(const UInt8)PrimitiveIAFactory::EStatic2D].pInputElementDescs = GInputDesc2D;

			const String strVertexShader3D = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVVertexShaderTemplate.msr");
			const String strVertexShader2D = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVVertexShader2D.msr");

			const Char* arrShaderTemplateFileName[(const UInt8)PrimitiveIAFactory::ECount] = {
				strVertexShader3D.CString(),
				strVertexShader3D.CString(),
				strVertexShader3D.CString(),
				strVertexShader2D.CString()
			};

			UInt32 primitiveTypeMap[(const UInt8)PrimitiveIAFactory::ECount] = {
				0,
				0,
				1,
				2
			};

			for (UInt32 uDomain = 0; uDomain < (const UInt32)MaterialDomain::ECount; uDomain++)
			{
				for (UInt32 uIAFactory = 0; uIAFactory < (const UInt32)PrimitiveIAFactory::ECount; uIAFactory++)
				{
					String strCacheName = String::Format("DefaultVertexShader_%d_%d.msr", uIAFactory, uDomain);

					ShaderMacro macros;
					if (!GRendererResource::ShaderManager->CompileShader(
						strCacheName.CString(),
						macros, DefaultVertexShader[uIAFactory][uDomain], nullptr, 0, ShaderType::EVertexShader, False))
					{
						IFile* pShaderFile = IFile::Open(arrShaderTemplateFileName[uIAFactory], IFile::EFDT_Text);
						if (pShaderFile == nullptr)return False;

						Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
						pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

						String strDomain = String::Format("%d", uDomain);
						macros.AddMacro("DOMAIN", strDomain.CString());
						switch ((PrimitiveIAFactory)uIAFactory)
						{
						case PrimitiveIAFactory::EStatic_Instance:
							macros.AddMacro("INSTANCE_RENDER", "1");
							break;
						case PrimitiveIAFactory::ESkin:
						case PrimitiveIAFactory::EStatic:
						case PrimitiveIAFactory::EStatic2D:
							macros.AddMacro("INSTANCE_RENDER", "0");
							break;
						}
						String strPrimitiveType = String::Format("%d", primitiveTypeMap[uIAFactory]);
						macros.AddMacro("PRIMITIVE_TYPE", strPrimitiveType.CString());
						String strTransformCount = String::Format("%d", GVarMaxGPUBlendMatrixPerPass.nValue);
						macros.AddMacro("MAX_TRANSFORMS_COUNT", strTransformCount.CString());

						String strCode = String::Format(pBuf, "", "");//Template With No Custom Code

						delete[] pBuf;
						pShaderFile->Close();

						GRendererResource::ShaderManager->CompileShader(
							strCacheName.CString(),
							macros, DefaultVertexShader[uIAFactory][uDomain], strCode.CString(), strCode.Length(), ShaderType::EVertexShader, False);
					}
				}
			}
			return True;
		}
		Void					ReleaseAPIDependencyResource()
		{
			MX_DELETE(ShaderManager);

			MX_RELEASE_INTERFACE(RasterizerRootSignature);
			MX_RELEASE_INTERFACE(ComputeRootSignature);
		}

		IGBuffer*				CreateBuffer(BufferType eType, Boolean bDynamic, UInt32 uSize)
		{
			ID3D12Resource* pBuffer = NULL;

			D3D12_HEAP_PROPERTIES heap_prop;
			heap_prop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
			heap_prop.CreationNodeMask = 1;
			heap_prop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
			heap_prop.Type = bDynamic ? D3D12_HEAP_TYPE_UPLOAD : D3D12_HEAP_TYPE_DEFAULT;
			heap_prop.VisibleNodeMask = 1;

			D3D12_RESOURCE_DESC resource_desc;
			resource_desc.Alignment = 0;
			resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
			resource_desc.Format = DXGI_FORMAT_UNKNOWN;
			resource_desc.Width = Math::AlignmentedSize(uSize, 256);
			resource_desc.Height = 1;
			resource_desc.DepthOrArraySize = 1;
			resource_desc.MipLevels = 1;
			resource_desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
			resource_desc.SampleDesc.Count = 1;
			resource_desc.SampleDesc.Quality = 0;
			resource_desc.Flags = D3D12_RESOURCE_FLAG_NONE;

			D3D12_RESOURCE_STATES InitState;
			if (bDynamic)
			{
				InitState = D3D12_RESOURCE_STATE_GENERIC_READ;
			}
			else
			{
				switch (eType)
				{
				case BufferType::EVertexBuffer:
				case BufferType::EConstantBuffer:
					InitState = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
					break;
				case BufferType::EIndexBuffer:
					InitState = D3D12_RESOURCE_STATE_INDEX_BUFFER;
					break;
				default:
					return False;
				}
			}

			if (FAILED(GRenderer::Device->CreateCommittedResource(
				&heap_prop, D3D12_HEAP_FLAG_NONE,
				&resource_desc, InitState, NULL,
				IID_PPV_ARGS(&pBuffer))))
			{
				return False;
			}

			GBuffer* pNewBuffer = new GBuffer;
			pNewBuffer->Real = pBuffer;
			pNewBuffer->State = InitState;
			pNewBuffer->Stride = 0;
			pNewBuffer->Size = resource_desc.Width;

			GRendererResource::Resources.Add(pNewBuffer);
			GRendererResource::FrameStatistics.uGPUMemoryOccupy += pNewBuffer->Size;
			return pNewBuffer;
		}

		Void					SetConstantBuffer(D3D12_CPU_DESCRIPTOR_HANDLE address, const GBuffer* pBuffer)
		{
			ASSERT_IF_FAILED((pBuffer->State & D3D12_RESOURCE_STATE_GENERIC_READ) > 0);

			D3D12_CONSTANT_BUFFER_VIEW_DESC desc;
			desc.BufferLocation = pBuffer->Real->GetGPUVirtualAddress();
			desc.SizeInBytes = Math::AlignmentedSize(pBuffer->Size, 256);

			GRenderer::Device->CreateConstantBufferView(&desc, address);
		}
		Void					SetShaderResource(D3D12_CPU_DESCRIPTOR_HANDLE address, const GTexture* pTexture)
		{
			ASSERT_IF_FAILED((pTexture->State & D3D12_RESOURCE_STATE_GENERIC_READ) > 0);

			D3D12_SHADER_RESOURCE_VIEW_DESC srv_desc;
			srv_desc.Format = PixelFormatMap[ResourceType::ERT_ShaderResourceView][pTexture->GetFormat()];
			srv_desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
			switch (pTexture->GetType())
			{
			case TextureType::ETexture1D:
				srv_desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE1D;
				srv_desc.Texture1D.MipLevels = -1;
				srv_desc.Texture1D.MostDetailedMip = 0;
				break;
			case TextureType::ETexture2D:
				srv_desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
				srv_desc.Texture2D.MipLevels = -1;
				srv_desc.Texture2D.MostDetailedMip = 0;
				srv_desc.Texture2D.PlaneSlice = 0;
				break;
			case TextureType::ETextureCube:
				srv_desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
				srv_desc.TextureCube.MipLevels = -1;
				srv_desc.TextureCube.MostDetailedMip = 0;
				srv_desc.TextureCube.ResourceMinLODClamp = 0;
				break;
			case TextureType::ETexture3D:
				srv_desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE3D;
				srv_desc.Texture3D.MipLevels = -1;
				srv_desc.Texture3D.MostDetailedMip = 0;
				srv_desc.Texture3D.ResourceMinLODClamp = 0;
				break;
			}

			GRenderer::Device->CreateShaderResourceView(pTexture->GetGResurce(), &srv_desc, address);
		}
		Void					SetUnorderedAccess(D3D12_CPU_DESCRIPTOR_HANDLE address, const GTexture* pTexture, UInt32 uSubResourceIndex)
		{
			ASSERT_IF_FAILED(pTexture->State == D3D12_RESOURCE_STATE_UNORDERED_ACCESS);

			D3D12_UNORDERED_ACCESS_VIEW_DESC uav_desc;
			uav_desc.Format = PixelFormatMap[ResourceType::ERT_RenderTargetView][pTexture->GetFormat()];
			switch (pTexture->GetType())
			{
			case TextureType::ETexture1D:
				uav_desc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE1D;
				uav_desc.Texture1D.MipSlice = uSubResourceIndex;
				break;
			case TextureType::ETexture2D:
				uav_desc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2D;
				uav_desc.Texture2D.MipSlice = uSubResourceIndex;
				uav_desc.Texture2D.PlaneSlice = 0;
				break;
			case TextureType::ETextureCube:
				uav_desc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2DARRAY;
				uav_desc.Texture2DArray.ArraySize = 1;
				uav_desc.Texture2DArray.FirstArraySlice = uSubResourceIndex % 6;
				uav_desc.Texture2DArray.MipSlice = uSubResourceIndex / 6;
				uav_desc.Texture2DArray.PlaneSlice = 0;
				break;
			case TextureType::ETexture3D:
				uav_desc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE3D;
				uav_desc.Texture3D.MipSlice = uSubResourceIndex / pTexture->GetDepth();
				uav_desc.Texture3D.FirstWSlice = uSubResourceIndex % pTexture->GetDepth();
				uav_desc.Texture3D.WSize = 0;
				break;
			}

			GRenderer::Device->CreateUnorderedAccessView(pTexture->GetGResurce(), nullptr, &uav_desc, address);
		}
		Void					SetRenderTarget(D3D12_CPU_DESCRIPTOR_HANDLE address, const GTexture* pTexture, UInt32 uSubResourceIndex)
		{
			ASSERT_IF_FAILED(pTexture->State == D3D12_RESOURCE_STATE_RENDER_TARGET);

			D3D12_RENDER_TARGET_VIEW_DESC rtv_desc;
			rtv_desc.Format = PixelFormatMap[ResourceType::ERT_RenderTargetView][pTexture->GetFormat()];
			switch (pTexture->GetType())
			{
			case TextureType::ETexture1D:
				rtv_desc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE1D;
				rtv_desc.Texture1D.MipSlice = uSubResourceIndex;
				break;
			case TextureType::ETexture2D:
				rtv_desc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
				rtv_desc.Texture2D.MipSlice = uSubResourceIndex;
				rtv_desc.Texture2D.PlaneSlice = 0;
				break;
			case TextureType::ETextureCube:
				rtv_desc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2DARRAY;
				rtv_desc.Texture2DArray.ArraySize = 1;
				rtv_desc.Texture2DArray.FirstArraySlice = uSubResourceIndex % 6;
				rtv_desc.Texture2DArray.MipSlice = uSubResourceIndex / 6;
				rtv_desc.Texture2DArray.PlaneSlice = 0;
				break;
			case TextureType::ETexture3D:
				rtv_desc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE3D;
				rtv_desc.Texture3D.MipSlice = uSubResourceIndex / pTexture->GetDepth();
				rtv_desc.Texture3D.FirstWSlice = uSubResourceIndex % pTexture->GetDepth();
				rtv_desc.Texture3D.WSize = 0;
				break;
			}

			GRenderer::Device->CreateRenderTargetView(pTexture->GetGResurce(), &rtv_desc, address);
		}
		Void					SetDepthTarget(D3D12_CPU_DESCRIPTOR_HANDLE address, const GTexture* pTexture, UInt32 uSubResourceIndex)
		{
			ASSERT_IF_FAILED(pTexture->State == D3D12_RESOURCE_STATE_DEPTH_WRITE);

			D3D12_DEPTH_STENCIL_VIEW_DESC dsv_desc;
			dsv_desc.Flags = D3D12_DSV_FLAG_NONE;
			dsv_desc.Format = PixelFormatMap[ResourceType::ERT_DepthStencilTargetView][pTexture->GetFormat()];
			switch (pTexture->GetType())
			{
			case TextureType::ETexture1D:
				dsv_desc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE1D;
				dsv_desc.Texture1D.MipSlice = uSubResourceIndex;
				break;
			case TextureType::ETexture2D:
				dsv_desc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
				dsv_desc.Texture2D.MipSlice = uSubResourceIndex;
				break;
			case TextureType::ETextureCube:
				dsv_desc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2DARRAY;
				dsv_desc.Texture2DArray.ArraySize = 1;
				dsv_desc.Texture2DArray.MipSlice = uSubResourceIndex / 6;
				dsv_desc.Texture2DArray.FirstArraySlice = uSubResourceIndex % 6;
				break;
			}

			GRenderer::Device->CreateDepthStencilView(pTexture->GetGResurce(), &dsv_desc, address);
		}

		IGSampler*				GetSampler(TextureAddressMode eAddressMode, TextureFilterMode eFilterMode, UInt32 uMaxAnisotropicLevel)
		{
			UInt32 ID = ((eAddressMode & 0x8) << 16) | ((eFilterMode & 0x8) << 8) | (uMaxAnisotropicLevel & 0x8);

			IGSampler* pRes = nullptr;
			auto it = m_mapSState.find(ID);
			if (it == m_mapSState.end())
			{
				GSamplerDesc* pDesc = new GSamplerDesc;
				pDesc->eAddressMode = eAddressMode;
				pDesc->eFilterMode = eFilterMode;
				pDesc->uMaxAnisotropicLevel = uMaxAnisotropicLevel;

				m_mapSState[ID] = pDesc;
				GRendererResource::Resources.Add(pDesc);
				pRes = pDesc;
			}
			else
			{
				pRes = it->second;
			}
			return pRes;
		}
		Void					SetSampler(D3D12_CPU_DESCRIPTOR_HANDLE address, TextureAddressMode eAddressMode, TextureFilterMode eFilterMode, UInt32 uMaxAnisotropicLevel)
		{
			D3D12_TEXTURE_ADDRESS_MODE am = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
			switch (eAddressMode)
			{
			case TextureAddressMode::ETexAM_Repeat:
				am = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
				break;
			case TextureAddressMode::ETexAM_Clamp:
				am = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
				break;
			case TextureAddressMode::ETexAM_Mirror:
				am = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
				break;
			}

			D3D12_FILTER fm = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
			switch (eFilterMode)
			{
			case TextureFilterMode::ETexFM_Point:
				fm = D3D12_FILTER_MIN_MAG_MIP_POINT;;
				break;
			case TextureFilterMode::ETexFM_Bilinear:
				fm = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
				break;
			case TextureFilterMode::ETexFM_Anisotropic:
				fm = D3D12_FILTER_ANISOTROPIC;
				break;
			}

			D3D12_SAMPLER_DESC desc;
			desc.AddressU = am;
			desc.AddressV = am;
			desc.AddressW = am;
			desc.BorderColor[0] = 0;
			desc.BorderColor[1] = 0;
			desc.BorderColor[2] = 0;
			desc.BorderColor[3] = 0;
			desc.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
			desc.Filter = fm;
			desc.MaxAnisotropy = uMaxAnisotropicLevel;
			desc.MinLOD = 0.0f;
			desc.MaxLOD = 1.0f;
			desc.MipLODBias = 0.0f;

			GRenderer::Device->CreateSampler(&desc, address);
		}

		D3D12_RASTERIZER_DESC RasterizerState(
			D3D12_CULL_MODE eCullMode,
			Boolean bWireFrame,
			Boolean bDepthClip,
			UInt32 uDepthBias,
			Boolean bScissor)
		{
			D3D12_RASTERIZER_DESC rs_desc;
			memset(&rs_desc, 0, sizeof(rs_desc));
			rs_desc.CullMode = eCullMode;
			rs_desc.FillMode = bWireFrame ? D3D12_FILL_MODE_WIREFRAME : D3D12_FILL_MODE_SOLID;
			rs_desc.FrontCounterClockwise = FALSE;
			rs_desc.MultisampleEnable = FALSE;
			rs_desc.SlopeScaledDepthBias = 0;
			rs_desc.AntialiasedLineEnable = TRUE;
			rs_desc.DepthClipEnable = bDepthClip;
			rs_desc.DepthBias = uDepthBias;
			rs_desc.DepthBiasClamp = 0;
			rs_desc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

			return rs_desc;
		}

		D3D12_DEPTH_STENCIL_DESC DepthStencilState(
			D3D12_COMPARISON_FUNC eDepthOp,
			Boolean bDepthWrite,
			Boolean bStencil,
			UInt32	uStencilReadMask,
			UInt32	uStencilWriteMask)
		{
			D3D12_DEPTH_STENCIL_DESC ds_desc;
			ds_desc.DepthEnable = True;/* eDepthOp != D3D11_COMPARISON_ALWAYS && eDepthOp != D3D11_COMPARISON_NEVER;*/
			ds_desc.DepthWriteMask = bDepthWrite ? D3D12_DEPTH_WRITE_MASK_ALL : D3D12_DEPTH_WRITE_MASK_ZERO;
			ds_desc.DepthFunc = eDepthOp;
			ds_desc.StencilEnable = bStencil;
			ds_desc.StencilReadMask = uStencilReadMask;
			ds_desc.StencilWriteMask = uStencilWriteMask;
			ds_desc.BackFace = { D3D12_STENCIL_OP_KEEP,D3D12_STENCIL_OP_KEEP,D3D12_STENCIL_OP_KEEP,D3D12_COMPARISON_FUNC_NEVER };
			ds_desc.FrontFace = { D3D12_STENCIL_OP_KEEP,D3D12_STENCIL_OP_KEEP,D3D12_STENCIL_OP_KEEP,D3D12_COMPARISON_FUNC_NEVER };

			return ds_desc;
		}

		D3D12_BLEND_DESC BlendState(
			Boolean bBlend,
			D3D12_BLEND_OP eAlphaOp,
			D3D12_BLEND_OP eColorOp,
			D3D12_BLEND eAlphaSrc,
			D3D12_BLEND eAlphaDest,
			D3D12_BLEND eColorSrc,
			D3D12_BLEND eColorDest,
			UInt8	uWriteMask)
		{
			D3D12_BLEND_DESC bs_desc;
			bs_desc.AlphaToCoverageEnable = FALSE;
			bs_desc.IndependentBlendEnable = FALSE;
			bs_desc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_SET;
			bs_desc.RenderTarget[0].LogicOpEnable = False;
			bs_desc.RenderTarget[0].BlendEnable = bBlend;
			bs_desc.RenderTarget[0].RenderTargetWriteMask = uWriteMask;

			bs_desc.RenderTarget[0].BlendOpAlpha = eAlphaOp;
			bs_desc.RenderTarget[0].DestBlendAlpha = eAlphaDest;
			bs_desc.RenderTarget[0].SrcBlendAlpha = eAlphaSrc;

			bs_desc.RenderTarget[0].BlendOp = eColorOp;
			bs_desc.RenderTarget[0].DestBlend = eColorDest;
			bs_desc.RenderTarget[0].SrcBlend = eColorSrc;
			return bs_desc;
		}

		Void								ResetFrameBuffer()
		{
			FrameBufferIndex = 0;
		}
		Void								NextFrameBuffer()
		{
			FrameBufferIndex++;
		}
		FrameBuffer*						GetCurFrameBuffer()
		{
			if (FrameBufferIndex >= FrameBuffers.ElementCount())
			{
				FrameBuffers.Add(new FrameBuffer());
			}

			return FrameBuffers[FrameBufferIndex];
		}

		Void								ResetShaderResourceSet()
		{
			RShaderResourceSetIndex = 0;
			CShaderResourceSetIndex = 0;
		}
		Void								NextRasterizerShaderResourceSet()
		{
			RShaderResourceSetIndex++;
		}
		RasterizerShaderResourceSet*		GetCurRasterizerShaderResourceSet()
		{
			if (RShaderResourceSetIndex >= RShaderResourceSet.ElementCount())
			{
				RShaderResourceSet.Add(new RasterizerShaderResourceSet());
			}

			return RShaderResourceSet[RShaderResourceSetIndex];
		}
		Void								NextComputeShaderResourceSet()
		{
			CShaderResourceSetIndex++;
		}
		ComputeShaderResourceSet*			GetCurComputeShaderResourceSet()
		{
			if (CShaderResourceSetIndex >= CShaderResourceSet.ElementCount())
			{
				CShaderResourceSet.Add(new ComputeShaderResourceSet());
			}

			return CShaderResourceSet[CShaderResourceSetIndex];
		}
	}

	template<>
	D3D12_CPU_DESCRIPTOR_HANDLE DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_RTV>::ElementCPUHandle(UInt32 uIndex)
	{
		ASSERT_IF_FAILED(uIndex < m_uSize);
		D3D12_CPU_DESCRIPTOR_HANDLE handle = m_pHeap->GetCPUDescriptorHandleForHeapStart();
		handle.ptr += uIndex * GRendererResource::RTVDescriptorSize;
		return handle;
	}
	template<>
	D3D12_CPU_DESCRIPTOR_HANDLE DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_DSV>::ElementCPUHandle(UInt32 uIndex)
	{
		ASSERT_IF_FAILED(uIndex < m_uSize);
		D3D12_CPU_DESCRIPTOR_HANDLE handle = m_pHeap->GetCPUDescriptorHandleForHeapStart();
		handle.ptr += uIndex * GRendererResource::DSVDescriptorSize;
		return handle;
	}
	template<>
	D3D12_CPU_DESCRIPTOR_HANDLE DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV>::ElementCPUHandle(UInt32 uIndex)
	{
		ASSERT_IF_FAILED(uIndex < m_uSize);
		D3D12_CPU_DESCRIPTOR_HANDLE handle = m_pHeap->GetCPUDescriptorHandleForHeapStart();
		handle.ptr += uIndex * GRendererResource::CBVSRVUAVDescriptorSize;
		return handle;
	}
	template<>
	D3D12_CPU_DESCRIPTOR_HANDLE DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER>::ElementCPUHandle(UInt32 uIndex)
	{
		ASSERT_IF_FAILED(uIndex < m_uSize);
		D3D12_CPU_DESCRIPTOR_HANDLE handle = m_pHeap->GetCPUDescriptorHandleForHeapStart();
		handle.ptr += uIndex * GRendererResource::SamplerDescriptorSize;
		return handle;
	}

	template<>
	D3D12_GPU_DESCRIPTOR_HANDLE DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_RTV>::ElementGPUHandle(UInt32 uIndex)
	{
		ASSERT_IF_FAILED(uIndex < m_uSize);
		D3D12_GPU_DESCRIPTOR_HANDLE handle = m_pHeap->GetGPUDescriptorHandleForHeapStart();
		handle.ptr += uIndex * GRendererResource::RTVDescriptorSize;
		return handle;
	}
	template<>
	D3D12_GPU_DESCRIPTOR_HANDLE DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_DSV>::ElementGPUHandle(UInt32 uIndex)
	{
		ASSERT_IF_FAILED(uIndex < m_uSize);
		D3D12_GPU_DESCRIPTOR_HANDLE handle = m_pHeap->GetGPUDescriptorHandleForHeapStart();
		handle.ptr += uIndex * GRendererResource::DSVDescriptorSize;
		return handle;
	}
	template<>
	D3D12_GPU_DESCRIPTOR_HANDLE DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV>::ElementGPUHandle(UInt32 uIndex)
	{
		ASSERT_IF_FAILED(uIndex < m_uSize);
		D3D12_GPU_DESCRIPTOR_HANDLE handle = m_pHeap->GetGPUDescriptorHandleForHeapStart();
		handle.ptr += uIndex * GRendererResource::CBVSRVUAVDescriptorSize;
		return handle;
	}
	template<>
	D3D12_GPU_DESCRIPTOR_HANDLE DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER>::ElementGPUHandle(UInt32 uIndex)
	{
		ASSERT_IF_FAILED(uIndex < m_uSize);
		D3D12_GPU_DESCRIPTOR_HANDLE handle = m_pHeap->GetGPUDescriptorHandleForHeapStart();
		handle.ptr += uIndex * GRendererResource::SamplerDescriptorSize;
		return handle;
	}

	Void RasterizerShaderResourceSet::BindGResource(IGCommandList* pCommandList)
	{
		ID3D12GraphicsCommandList* pCL = ((GCommandList*)pCommandList)->APICommandlist;
		pCL->SetGraphicsRootSignature(GRendererResource::RasterizerRootSignature);

		ID3D12DescriptorHeap* Heaps[2] = {
			ResourceHeap.GetGResource(),
			SamplerHeap.GetGResource()
		};
		pCL->SetDescriptorHeaps(ARRAY_ELEMENT_COUNT(Heaps), Heaps);

		UInt32 uTableIdx = 0;
		pCL->SetGraphicsRootDescriptorTable(uTableIdx++,
			ResourceHeap.ElementGPUHandle((const UInt8)RasterizerRootSignatureDesc::EVS_CBV_BEGIN));
		pCL->SetGraphicsRootDescriptorTable(uTableIdx++,
			SamplerHeap.ElementGPUHandle((const UInt8)RasterizerRootSignatureDesc::EVS_SAMPLER_BEGIN));
		pCL->SetGraphicsRootDescriptorTable(uTableIdx++,
			ResourceHeap.ElementGPUHandle((const UInt8)RasterizerRootSignatureDesc::EVS_CBV_BEGIN));
		pCL->SetGraphicsRootDescriptorTable(uTableIdx++,
			SamplerHeap.ElementGPUHandle((const UInt8)RasterizerRootSignatureDesc::EVS_SAMPLER_BEGIN));
	}
	Void ComputeShaderResourceSet::BindGResource(IGCommandList* pCommandList)
	{
		ID3D12GraphicsCommandList* pCL = ((GCommandList*)pCommandList)->APICommandlist;
		pCL->SetComputeRootSignature(GRendererResource::ComputeRootSignature);

		ID3D12DescriptorHeap* Heaps[2] = {
			ResourceHeap.GetGResource(),
			SamplerHeap.GetGResource()
		};
		pCL->SetDescriptorHeaps(ARRAY_ELEMENT_COUNT(Heaps), Heaps);

		UInt32 uTableIdx = 0;
		pCL->SetComputeRootDescriptorTable(uTableIdx++,
			ResourceHeap.ElementGPUHandle((const UInt8)ComputeRootSignatureDesc::ECBV_BEGIN));
		pCL->SetComputeRootDescriptorTable(uTableIdx++,
			SamplerHeap.ElementGPUHandle((const UInt8)ComputeRootSignatureDesc::ESAMPLER_BEGIN));
	}

	IGRenderWindow*		GraphicsResource::CreateRenderWindow(const IGRenderWindow::Description& desc)
	{
		GRenderWindow* pTemp = new GRenderWindow(GRendererResource::uResourceID);
		if (!pTemp->Init(desc))
		{
			Log::Output("GraphicsD3D12", "RenderWindow Create Failed");
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		return pTemp;
	}
	IGTexture*			GraphicsResource::CreateTexture(const IGTexture::Description& desc)
	{
		GTexture* pTemp = new GTexture(GRendererResource::uResourceID);
		if (!pTemp->Init(desc))
		{
			Log::Output("GraphicsD3D12", "Texture Create Failed");
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		return pTemp;
	}
	IGPrimitive*		GraphicsResource::CreatePrimitive(const IGPrimitive::Description& desc)
	{
		GPrimitive* pTemp = nullptr;
		switch (desc.eType)
		{
		case IGPrimitive::EPrimT_Line:pTemp = new GPrimitiveLine(GRendererResource::uResourceID);	break;
		case IGPrimitive::EPrimT_Rigid:	pTemp = new GPrimitiveRigid(GRendererResource::uResourceID); break;
		case IGPrimitive::EPrimT_Skin:pTemp = new GPrimitiveSkin(GRendererResource::uResourceID);	break;
		case IGPrimitive::EPrimT_Rigid2D: pTemp = new GPrimitive2D(GRendererResource::uResourceID); break;
		default:
		{
			Log::Output("GraphicsD3D12", "Invalid Primitive Type");
			return nullptr;
		}
		}

		if (!pTemp->Init(desc.bDynamic, desc.uVertexCount, desc.uIndexCount))
		{
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		return pTemp;
	}
	IGMaterial*			GraphicsResource::CreateMaterial(const IGMaterial::Description& desc, Boolean bReCreate)
	{
		IGMaterial::Description ModifiedDesc = desc;
		const GShaderManager::MaterialCache* cache = nullptr;

		bReCreate &= GRendererResource::HasRawShaderFolder();

		if (!bReCreate)
			cache = GRendererResource::ShaderManager->GetMaterialPropertyCache(desc.strUniqueName.CString());

		if (cache != nullptr)
		{
			ModifiedDesc.eDomain = (MaterialDomain)cache->uDomain;
			ModifiedDesc.eShadingModel = (MaterialShadingModel)cache->uShadingModel;
			ModifiedDesc.eFaceType = (MaterialFaceType)cache->uFaceType;
			ModifiedDesc.eBlendMode = (MaterialBlendMode)cache->uBlendMode;
		}

		GMaterial* pTemp = new GMaterial(GRendererResource::uResourceID);
		if (!pTemp->Init(ModifiedDesc, bReCreate))
		{
			Log::Output("GraphicsD3D12", "Material Create Failed");
			delete pTemp;
			return nullptr;
		}

		GRendererResource::Resources.Add(pTemp);
		GRendererResource::uResourceID++;
		if (cache == nullptr)
			pTemp->Cache();
		return pTemp;
	}
	IGMaterialParameterBuffer* GraphicsResource::CreateMaterialParameterBuffer(IGMaterial* pMaterial)
	{
		GCommonMaterialParameterBuffer* pBlock = new GCommonMaterialParameterBuffer(GRendererResource::uResourceID);
		if (!((GMaterial*)pMaterial)->ConvertMaterialParameterBlock(pBlock))
		{
			Log::Output("GraphicsD3D12", "MaterialParameterBlock Create Failed");
			delete pBlock;
			return nullptr;
		}
		GRendererResource::Resources.Add(pBlock);
		GRendererResource::uResourceID++;
		return pBlock;
	}
	IGPrimitiveTransformBuffer*GraphicsResource::CreatePrimitiveTransformBuffer(Boolean bForInstance)
	{
		GPrimitiveTransformBuffer* pBlock = new GPrimitiveTransformBuffer(GRendererResource::uResourceID);
		if (!pBlock->Init(bForInstance, GVarMaxGPUBlendMatrixPerPass.nValue))
		{
			Log::Output("GraphicsD3D12", "PrimitiveTransformBlock Create Failed");
			delete pBlock;
			return nullptr;
		}
		GRendererResource::Resources.Add(pBlock);
		GRendererResource::uResourceID++;
		return pBlock;
	}
	IGViewFrustum*		GraphicsResource::CreateViewFrustum(const IGViewFrustum::Description& desc)
	{
		GViewFrustum* pViewFrustum = new GViewFrustum(GRendererResource::uResourceID);
		if (!pViewFrustum->Init(desc))
		{
			Log::Output("GraphicsD3D12", "ViewFrustum Create Failed");
			delete pViewFrustum;
			return nullptr;
		}

		GRendererResource::Resources.Add(pViewFrustum);
		GRendererResource::uResourceID++;
		return pViewFrustum;
	}
}