#pragma once
#ifndef _G_SHADER_MANAGER_
#define _G_SHADER_MANAGER_
#include "GCommonShaderManager.h"

namespace MXVisual
{
	class GShaderManager : public GCommonShaderManager
	{
	public:
		GShaderManager(Boolean bShaderDebug = False);
		~GShaderManager();

		//Return Compile Result
		Boolean CompileShader(
			const Char* szShaderName,
			ShaderMacro& macros,
			ShaderCompiledResult& result,
			const Void* pShaderCode, UInt32 uShaderSize,
			ShaderType eShaderType,
			Boolean bRecompile) override;
	};
}

#endif