#pragma once
#ifndef _G_RENDERER_
#define _G_RENDERER_
#include "GCommonRenderer.h"
#include "dxgi1_6.h"
#include "d3d12.h"
#include "d3dcompiler.h"
#if defined(DEBUG)
#include "WinPixEventRuntime/pix3.h"
#endif
#include "MXThread.h"
#include "MXFile.h"
#include "MXLog.h"
#include "MXMath.h"
#include "GAPIWrapper.h"

#define REVERT_Z 1

#define DEPTH_ALWAYS D3D12_COMPARISON_FUNC_ALWAYS
#define DEPTH_EQUAL D3D12_COMPARISON_FUNC_EQUAL
#if REVERT_Z
#define DEPTH_NEARER D3D12_COMPARISON_FUNC_GREATER_EQUAL
#define DEPTH_FARTHER D3D12_COMPARISON_FUNC_LESS
#define DEPTH_CLEAR_VALUE 0.0f
#else
#define DEPTH_NEARER D3D12_COMPARISON_FUNC_LESS_EQUAL
#define DEPTH_FARTHER D3D12_COMPARISON_FUNC_GREATER
#define DEPTH_CLEAR_VALUE 1.0f
#endif

#if defined(DEBUG)
#define NAME_API_OBJECT(obj, name)	(obj)->SetName(L#name)
#else
#define NAME_API_OBJECT(obj, name)
#endif

namespace MXVisual
{
	const UInt32 Max_RTV_Count = 8;
	const UInt32 Max_SRV_Count = 32;

	enum ResourceType
	{
		ERT_Resouce,
		ERT_ShaderResourceView,
		ERT_RenderTargetView,
		ERT_DepthStencilTargetView,
		ERT_Count
	};

	extern DXGI_FORMAT	PixelFormatMap[ERT_Count][EPF_Count + 1];
	extern UInt8		PixelFormatSize[EPF_Count + 1];

	extern D3D12_INPUT_ELEMENT_DESC GInputDesc2D[3];
	extern D3D12_INPUT_ELEMENT_DESC GInputDescStatic[5];
	extern D3D12_INPUT_ELEMENT_DESC GInputDescStatic_Instance[9];
	extern D3D12_INPUT_ELEMENT_DESC GInputDescSkin[7];
	extern D3D12_INPUT_ELEMENT_DESC GInputDescSkin_Instance[11];

	struct GPipeline
	{
		ID3D12PipelineState*		PSO = nullptr;

		Boolean	InitForRender(
			PrimitiveIAFactory			eIAFactory,
			TopologyType				eTopology,
			const GRenderPass&				RP,
			const D3D12_RASTERIZER_DESC&	RS,
			const D3D12_DEPTH_STENCIL_DESC&	DS,
			const D3D12_BLEND_DESC&			BS,
			const ShaderCompiledResult*	pVS,
			const ShaderCompiledResult*	pPS
		);

		Boolean InitForCompute(
			const ShaderCompiledResult*		pCS
		);

		Void	Release();
	};
	struct GBuffer : public IGBuffer
	{
		ID3D12Resource*			Real;
		UInt32					Stride;
		UInt32					Size;
		D3D12_RESOURCE_STATES	State;

		UInt32 GetUniqueID() const { return -1; }
		GBuffer()
			: Real(nullptr)
			, Stride(0)
			, Size(0)
			, State(D3D12_RESOURCE_STATE_COMMON)
		{

		}
		~GBuffer()
		{
			MX_RELEASE_INTERFACE(Real);
		}
	};
	struct GSamplerDesc : public IGSampler
	{
		TextureAddressMode eAddressMode;
		TextureFilterMode eFilterMode;
		UInt32 uMaxAnisotropicLevel;

		UInt32 GetUniqueID() const { return -1; }
	};

	class GShaderManager;

	namespace GRenderer
	{
		//Device Limit Info
		extern UInt32	uMaxGPUResourceSize;

		extern IDXGIFactory6*	DXGIFactory;
		extern ID3D12Device*	Device;
		extern ID3D12Device5*	DeviceLast;

		extern ID3D12CommandQueue*		HardwarePipeline;

		extern UInt64					SyncGPUValue;
		extern ID3D12Fence*				SyncGPUFence;
		extern HANDLE					SyncGPUEvent;

		extern D3D_FEATURE_LEVEL		FeatureLevel;
		extern Boolean					SupportRayTracing;

		Void SyncGPU();
	};

	class GTexture;
	namespace GRendererResource
	{
		Void	SetConstantBuffer(D3D12_CPU_DESCRIPTOR_HANDLE address, const GBuffer* pBuffer);
		Void	SetShaderResource(D3D12_CPU_DESCRIPTOR_HANDLE address, const GTexture* pTexture);
		Void	SetUnorderedAccess(D3D12_CPU_DESCRIPTOR_HANDLE address, const GTexture* pTexture, UInt32 uSubResourceIndex);
		Void	SetRenderTarget(D3D12_CPU_DESCRIPTOR_HANDLE address, const GTexture* pTexture, UInt32 uSubResourceIndex);
		Void	SetDepthTarget(D3D12_CPU_DESCRIPTOR_HANDLE address, const GTexture* pTexture, UInt32 uSubResourceIndex);

		Void	SetSampler(D3D12_CPU_DESCRIPTOR_HANDLE address, TextureAddressMode eAddressMode, TextureFilterMode eFilterMode, UInt32 uMaxAnisotropicLevel);

		D3D12_RASTERIZER_DESC RasterizerState(
			D3D12_CULL_MODE eCullMode = D3D12_CULL_MODE_BACK,
			Boolean bWireFrame = False,
			Boolean bDepthClip = True,
			UInt32 uDepthBias = 0,
			Boolean bScissor = False);

		D3D12_DEPTH_STENCIL_DESC DepthStencilState(
			D3D12_COMPARISON_FUNC eDepthOp = DEPTH_NEARER,
			Boolean bDepthWrite = True,
			Boolean bStencil = False,
			UInt32	uStencilReadMask = 0,
			UInt32	uStencilWriteMask = 0);

		D3D12_BLEND_DESC BlendState(
			Boolean bBlend = False,
			D3D12_BLEND_OP eAlphaOp = D3D12_BLEND_OP_ADD,
			D3D12_BLEND_OP eColorOp = D3D12_BLEND_OP_ADD,
			D3D12_BLEND eAlphaSrc = D3D12_BLEND_ONE,
			D3D12_BLEND eAlphaDest = D3D12_BLEND_ZERO,
			D3D12_BLEND eColorSrc = D3D12_BLEND_SRC_ALPHA,
			D3D12_BLEND eColorDest = D3D12_BLEND_INV_SRC_ALPHA,
			UInt8	uWriteMask = EWM_AllChannel);
		extern UInt32 uResourceID;
		extern Array<IGResource*> Resources;

		Void								ResetFrameBuffer();
		Void								NextFrameBuffer();
		FrameBuffer*						GetCurFrameBuffer();

		Void								ResetShaderResourceSet();
		Void								NextRasterizerShaderResourceSet();
		RasterizerShaderResourceSet*		GetCurRasterizerShaderResourceSet();
		Void								NextComputeShaderResourceSet();
		ComputeShaderResourceSet*			GetCurComputeShaderResourceSet();

		extern ID3D12RootSignature*			RasterizerRootSignature;
		extern ID3D12RootSignature*			ComputeRootSignature;

		extern UInt32						RTVDescriptorSize;
		extern UInt32						DSVDescriptorSize;
		extern UInt32						CBVSRVUAVDescriptorSize;
		extern UInt32						SamplerDescriptorSize;

		extern GBuffer*						UploadBuffer;

		extern D3D12_INPUT_LAYOUT_DESC		DefaultInputLayout[(const UInt8)PrimitiveIAFactory::ECount];
		extern ShaderCompiledResult			DefaultVertexShader[(const UInt8)PrimitiveIAFactory::ECount][(const UInt8)MaterialDomain::ECount];
	};
#define BLENDSTATE_OFF False,\
					D3D12_BLEND_OP_ADD,\
					D3D12_BLEND_OP_ADD,\
					D3D12_BLEND_ONE,\
					D3D12_BLEND_ZERO,\
					D3D12_BLEND_SRC_ALPHA,\
					D3D12_BLEND_INV_SRC_ALPHA

#define BLENDSTATE_ADDITIVE True,\
					 D3D12_BLEND_OP_ADD,\
					 D3D12_BLEND_OP_ADD,\
					 D3D12_BLEND_ZERO,\
					 D3D12_BLEND_ONE,\
					 D3D12_BLEND_SRC_ALPHA,\
					 D3D12_BLEND_ONE

#define BLENDSTATE_BLEND  True,\
					 D3D12_BLEND_OP_ADD,\
					 D3D12_BLEND_OP_ADD,\
					 D3D12_BLEND_ONE,\
					 D3D12_BLEND_ZERO,\
					 D3D12_BLEND_SRC_ALPHA,\
					 D3D12_BLEND_INV_SRC_ALPHA
}

#endif