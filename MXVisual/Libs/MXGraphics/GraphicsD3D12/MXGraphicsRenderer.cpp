#include "GRenderer.h"
#include "MXConstant.h"
#include "MXCommon.h"

#include "GShaderManager.h"

#include "GViewFrustum.h"
#include "GTexture.h"
#include "GPrimitive.h"
#include "GMaterial.h"
#include "GCommandList.h"

#if defined(DEBUG)
typedef HRESULT(WINAPI* BeginEventOnCommandList)(ID3D12GraphicsCommandList* commandList, UINT64 color, _In_ PCSTR formatString);
typedef HRESULT(WINAPI* EndEventOnCommandList)(ID3D12GraphicsCommandList* commandList);
#endif

namespace MXVisual
{
	Boolean	GPipeline::InitForRender(
		PrimitiveIAFactory			eIAFactory,
		TopologyType				eTopology,
		const GRenderPass&				RP,
		const D3D12_RASTERIZER_DESC&	RS,
		const D3D12_DEPTH_STENCIL_DESC&	DS,
		const D3D12_BLEND_DESC&			BS,
		const ShaderCompiledResult*	pVS,
		const ShaderCompiledResult*	pPS)
	{
		D3D12_GRAPHICS_PIPELINE_STATE_DESC gpso = { 0 };
		gpso.pRootSignature = GRendererResource::RasterizerRootSignature;
		gpso.IBStripCutValue = D3D12_INDEX_BUFFER_STRIP_CUT_VALUE_DISABLED;
		gpso.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
		gpso.NodeMask = 1;
		switch (eTopology)
		{
		case TopologyType::ETriangle:
			gpso.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
			break;
		case TopologyType::ELine:
			gpso.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE;
			break;
		case TopologyType::EPoint:
			gpso.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
			break;
		}
		gpso.SampleMask = 0xffffffff;
		gpso.SampleDesc.Count = 1;
		gpso.SampleDesc.Quality = 0;

		gpso.NumRenderTargets = RP.RTVCount;
		gpso.DSVFormat = PixelFormatMap[ResourceType::ERT_DepthStencilTargetView][RP.DSVFormat];
		for (UInt32 u = 0; u < RP.RTVCount; u++)
		{
			gpso.RTVFormats[u] = PixelFormatMap[ResourceType::ERT_RenderTargetView][RP.RTVFormats[u]];
		}

		switch (eIAFactory)
		{
		case PrimitiveIAFactory::EStatic:
		case PrimitiveIAFactory::EStatic_Instance:
		case PrimitiveIAFactory::ESkin:
		case PrimitiveIAFactory::EStatic2D:
			gpso.InputLayout = GRendererResource::DefaultInputLayout[(const UInt32)eIAFactory];
			break;
		default:
			ASSERT_IF_FAILED(!"Invalid IAFactory");
			return False;
		}

		if (pVS != nullptr)
		{
			gpso.VS.pShaderBytecode = pVS->pCode;
			gpso.VS.BytecodeLength = pVS->uSize;
		}
		else
		{
			gpso.VS.pShaderBytecode =
				GRendererResource::DefaultVertexShader[(const UInt32)eIAFactory][(const UInt32)MaterialDomain::EWorldSpace].pCode;
			gpso.VS.BytecodeLength =
				GRendererResource::DefaultVertexShader[(const UInt32)eIAFactory][(const UInt32)MaterialDomain::EWorldSpace].uSize;
		}

		gpso.RasterizerState = RS;
		gpso.DepthStencilState = DS;
		gpso.BlendState = BS;

		ASSERT_IF_FAILED(pPS != nullptr);
		if (pPS == nullptr)return False;
		gpso.PS.pShaderBytecode = pPS->pCode;
		gpso.PS.BytecodeLength = pPS->uSize;

		if (gpso.VS.pShaderBytecode == nullptr || gpso.VS.BytecodeLength == 0 ||
			gpso.PS.pShaderBytecode == nullptr || gpso.PS.BytecodeLength == 0)
		{
			Log::Output("GraphicsD3D12", "GPipeline::InitForRender Invalid Shader Code");
			return False;
		}

		if (FAILED(GRenderer::Device->CreateGraphicsPipelineState(&gpso, IID_PPV_ARGS(&PSO))))
		{
			Log::Output("GraphicsD3D12", "GPipeline::InitForRender Create PSO Failed");
			return False;
		}
		return True;
	}

	Boolean GPipeline::InitForCompute(
		const ShaderCompiledResult*		pCS)
	{
		ASSERT_IF_FAILED(pCS != nullptr);
		if (pCS == nullptr)return False;

		D3D12_COMPUTE_PIPELINE_STATE_DESC cpso = { 0, };
		cpso.pRootSignature = GRendererResource::ComputeRootSignature;
		cpso.CS.pShaderBytecode = pCS->pCode;
		cpso.CS.BytecodeLength = pCS->uSize;
		cpso.NodeMask = 1;
		cpso.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;

		return SUCCEEDED(GRenderer::Device->CreateComputePipelineState(&cpso, IID_PPV_ARGS(&PSO)));
	}

	Void	GPipeline::Release()
	{
		MX_RELEASE_INTERFACE(PSO);
	}

	namespace GRenderer
	{
		UInt32	uMaxGPUResourceSize;

		IDXGIFactory6*	DXGIFactory = nullptr;
		ID3D12Device*	Device = nullptr;
		ID3D12Device5*	DeviceLast = nullptr;

		ID3D12CommandQueue*		HardwarePipeline = nullptr;

		UInt64					SyncGPUValue;
		ID3D12Fence*			SyncGPUFence;
		HANDLE					SyncGPUEvent;

		UInt32					CommandlistOffset = 0;
		GCommandList*			Commandlist[2] = { nullptr, };

		D3D_FEATURE_LEVEL		FeatureLevel = D3D_FEATURE_LEVEL_12_0;
		Boolean					SupportRayTracing = False;

#if defined(DEBUG)
		HMODULE PIXModule = NULL;
		BeginEventOnCommandList	BeginEvent = nullptr;
		EndEventOnCommandList	EndEvent = nullptr;
#endif

		Void	SyncGPU()
		{
			HardwarePipeline->Signal(SyncGPUFence, SyncGPUValue);

			UInt64 CompletedValue = SyncGPUFence->GetCompletedValue();
			if (CompletedValue == UINT64_MAX)
			{
				return;
			}
			if (CompletedValue < SyncGPUValue)
			{
				SyncGPUFence->SetEventOnCompletion(SyncGPUValue, SyncGPUEvent);
				WaitForSingleObject(SyncGPUEvent, INFINITE);
			}
			SyncGPUValue++;
		}

		Boolean InitAPI(const Char* szAdapterName)

		{
			UInt32 uDXGIFlags = 0;
#if defined(_DEBUG)
			ID3D12Debug* debugController = nullptr;
			if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
			{
				debugController->EnableDebugLayer();
				uDXGIFlags = DXGI_CREATE_FACTORY_DEBUG;
			}

			PIXModule = LoadLibrary("WinPixEventRuntime.dll");
			if (PIXModule != NULL)
			{
				BeginEvent = (BeginEventOnCommandList)GetProcAddress(PIXModule, "PIXBeginEventOnCommandList");
				EndEvent = (EndEventOnCommandList)GetProcAddress(PIXModule, "PIXEndEventOnCommandList");
			}
#endif

			if (FAILED(CreateDXGIFactory2(uDXGIFlags, IID_PPV_ARGS(&DXGIFactory))))
			{
				return False;
			}

			DXGI_ADAPTER_DESC adapter_desc;
			IDXGIAdapter* pAdapter = nullptr;
			UInt32 uAdapterIndex = 0;
			wchar_t wsz[Constant_MAX_PATH];
			if (szAdapterName != nullptr)
				wsprintfW(wsz, L"%S", szAdapterName);

			while (SUCCEEDED(DXGIFactory->EnumAdapterByGpuPreference(
				uAdapterIndex, DXGI_GPU_PREFERENCE_HIGH_PERFORMANCE, IID_PPV_ARGS(&pAdapter))))
			{
				if (szAdapterName == nullptr)
					break;

				pAdapter->GetDesc(&adapter_desc);
				if (lstrcmpiW(wsz, adapter_desc.Description))
				{
					break;
				}

				uAdapterIndex++;
				MX_RELEASE_INTERFACE(pAdapter);
			}

			if (FAILED(D3D12CreateDevice(pAdapter,
				FeatureLevel,
				IID_PPV_ARGS(&Device))))
			{
				MX_RELEASE_INTERFACE(pAdapter);
				return False;
			}

			D3D12_FEATURE_DATA_D3D12_OPTIONS5 feature5;
			Device->CheckFeatureSupport(D3D12_FEATURE_D3D12_OPTIONS5, &feature5, sizeof(feature5));
			if (feature5.RaytracingTier != D3D12_RAYTRACING_TIER_NOT_SUPPORTED)
			{
				SupportRayTracing = True;
				Log::Output("GraphicsD3D12", "Support DXR");
			}

			if (pAdapter == nullptr)
			{
				IDXGIDevice *pDXGIDevice = nullptr;
				IDXGIAdapter *pDXGIAdapter = nullptr;
				if (FAILED(Device->QueryInterface(IID_PPV_ARGS(&pDXGIDevice))) ||
					FAILED(pDXGIDevice->GetAdapter(&pDXGIAdapter)) ||
					FAILED(pDXGIAdapter->GetParent(IID_PPV_ARGS(&DXGIFactory))))
				{
					MX_RELEASE_INTERFACE(pDXGIAdapter);
					MX_RELEASE_INTERFACE(pDXGIDevice);
					return False;
				}
			}
			MX_RELEASE_INTERFACE(pAdapter);

			D3D12_COMMAND_QUEUE_DESC queue_desc;
			queue_desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
			queue_desc.NodeMask = 1;
			queue_desc.Priority = 0;
			queue_desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
			if (FAILED(Device->CreateCommandQueue(&queue_desc, IID_PPV_ARGS(&HardwarePipeline))))
			{
				return False;
			}

			ID3D12CommandAllocator* pCA0 = nullptr;
			ID3D12CommandAllocator* pCA1 = nullptr;

			if (FAILED(Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&pCA0))) ||
				FAILED(Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&pCA1))))
			{
				return False;
			}

			ID3D12GraphicsCommandList* pCL0 = nullptr;
			ID3D12GraphicsCommandList* pCL1 = nullptr;

			if (FAILED(Device->CreateCommandList(1, D3D12_COMMAND_LIST_TYPE_DIRECT,
				pCA0, nullptr, IID_PPV_ARGS(&pCL0))) ||
				FAILED(Device->CreateCommandList(1, D3D12_COMMAND_LIST_TYPE_DIRECT,
					pCA1, nullptr, IID_PPV_ARGS(&pCL1))))
			{
				return False;
			}

			NAME_API_OBJECT(pCL0, "CommandList0");
			NAME_API_OBJECT(pCL1, "CommandList1");

			Commandlist[0] = new GCommandList(pCL0, pCA0);
			Commandlist[1] = new GCommandList(pCL1, pCA1);

			if (FAILED(Device->CreateFence(SyncGPUValue, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&SyncGPUFence))))
			{
				return False;
			}

			SyncGPUEvent = CreateEvent(nullptr, false, false, nullptr);
			if (SyncGPUEvent == INVALID_HANDLE_VALUE)
				return False;

			SyncGPU();
			pCA0->Reset();
			pCA1->Reset();
			pCL0->Close();
			pCL0->Reset(pCA0, nullptr);
			pCL1->Close();
			pCL1->Reset(pCA1, nullptr);
			return True;
		}
		Void	ReleaseAPI()
		{
			delete GRendererResource::ShaderManager;

			CloseHandle(SyncGPUEvent);

			MX_RELEASE_INTERFACE(HardwarePipeline);
			MX_RELEASE_INTERFACE(SyncGPUFence);

			MX_DELETE(Commandlist[0]);
			MX_DELETE(Commandlist[1]);
			MX_RELEASE_INTERFACE(Device);
			MX_RELEASE_INTERFACE(DXGIFactory);
		}

		IGCommandList*	CommandList()
		{
			return Commandlist[CommandlistOffset];
		}

		Void			Execute()
		{
			GCommandList* pRealCL = ((GCommandList*)GRenderer::CommandList());
			ASSERT_IF_FAILED(SUCCEEDED(pRealCL->APICommandlist->Close()));
			ID3D12CommandList* pCmdList = pRealCL->APICommandlist;
			GRenderer::HardwarePipeline->ExecuteCommandLists(1, &pCmdList);

			SyncGPU();

			ASSERT_IF_FAILED(SUCCEEDED(pRealCL->APICommandAlloactor->Reset()));
			ASSERT_IF_FAILED(SUCCEEDED(pRealCL->APICommandlist->Reset(pRealCL->APICommandAlloactor, NULL)));
			CommandlistOffset = (CommandlistOffset + 1) % 2;

			GRendererResource::ResetFrameBuffer();
			GRendererResource::ResetShaderResourceSet();
		}
	}

#if defined(DEBUG)
	Void DebugPushEvent(const Char* eventName)
	{
		GRenderer::BeginEvent(((GCommandList*)GRenderer::CommandList())->APICommandlist, 0xffffffff, eventName);
	}
	Void DebugPopEvent()
	{
		GRenderer::EndEvent(((GCommandList*)GRenderer::CommandList())->APICommandlist);
	}
#endif
}