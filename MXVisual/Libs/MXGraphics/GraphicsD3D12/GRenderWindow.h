#pragma once
#ifndef _G_RENDERWINDOW_
#define _G_RENDERWINDOW_
#include "MXPlatform.h"
#include "MXGraphicsResource.h"
#include "GTexture.h"
#include "GRenderer.h"
#include <unordered_map>
#include "Windows.h"

namespace MXVisual
{
	class GRenderWindow : public IGRenderWindow
	{
	public:
		GRenderWindow(UInt32 UniqueID)
			: m_uUniqueID(UniqueID)
			, m_bActived(True)
			, m_uBackbufferCount(0)
			, m_eBackbufferFormat(PixelFormat::EPF_Invalid)
			, m_pSwapChain(nullptr)
		{
		}
		~GRenderWindow()
		{
#if PLATFORM == PLATFORM_WINDOWS
			UnregisterClass(String::Format("RenderWindow_%d", m_uUniqueID).CString(), NULL);
#endif
			for (UInt32 u = 0; u < m_ColorTargets.ElementCount(); u++)
			{
				MX_RELEASE_INTERFACE(m_ColorTargets[u]);
			}
			MX_RELEASE_INTERFACE(m_pSwapChain);
		}

		UInt32 GetUniqueID() const { return m_uUniqueID; }

		Boolean Init(const IGRenderWindow::Description& desc);

		Float2	ScreenToWindow(const Float2& ScreenPos) const;

		const UInt32 GetWidth() const { return m_uBackbufferWidth; }
		const UInt32 GetHeight() const { return m_uBackbufferHeight; }
		const IGTexture* GetRenderTarget() const
		{
			UInt32 uIndex = m_pSwapChain->GetCurrentBackBufferIndex();
			return m_ColorTargets[uIndex];
		}

		Boolean	IsActived() const override { return m_bActived; }
		Void	Show(Boolean bShow) override;
		Void	Resize(UInt32 uWidth, UInt32 uHeight);
		Void	Maximize();
		Void	Minimize();

		Void	ResizeTarget(UInt32 uWidth, UInt32 uHeight);
		Void	Present() override;

	protected:
		UInt32 m_uUniqueID;

		static std::unordered_map<UInt64, GRenderWindow*> m_mapRenderWindows;
		static LRESULT CALLBACK RenderWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

		UInt64 m_uWindowHandle;
		Boolean m_bActived;
		UInt32 m_uBackbufferWidth;
		UInt32 m_uBackbufferHeight;
		UInt32 m_uBackbufferCount;
		PixelFormat m_eBackbufferFormat;

		IDXGISwapChain4* m_pSwapChain;
		Array<IGTexture*> m_ColorTargets;
	};
}

#endif