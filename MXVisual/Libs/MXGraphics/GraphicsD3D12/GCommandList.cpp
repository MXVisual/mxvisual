#include "GCommandList.h"
#include "GTexture.h"

namespace MXVisual
{
	GCommandList::GCommandList(class ID3D12GraphicsCommandList* APICL, class ID3D12CommandAllocator* APIAllocator)
		: APICommandlist(APICL)
		, APICommandAlloactor(APIAllocator)
	{

	}
	GCommandList::~GCommandList()
	{
		MX_RELEASE_INTERFACE(APICommandlist);
		MX_RELEASE_INTERFACE(APICommandAlloactor);
	}

	Void	GCommandList::SetViewport(const GViewport& Viewport)
	{
		D3D12_VIEWPORT	 vp;
		vp.TopLeftX = Viewport.uX;
		vp.TopLeftY = Viewport.uY;
		vp.Width = Viewport.uWidth;
		vp.Height = Viewport.uHeight;
		vp.MinDepth = Viewport.fMinDepth;
		vp.MaxDepth = Viewport.fMaxDepth;
		APICommandlist->RSSetViewports(1, &vp);
	}
	Void	GCommandList::SetScissor(const Region& Scissor)
	{
		D3D12_RECT	 rect;
		rect.left = Scissor.uLeft;
		rect.top = Scissor.uTop;
		rect.right = Scissor.uRight;
		rect.bottom = Scissor.uBottom;
		APICommandlist->RSSetScissorRects(1, &rect);
	}

	Void	GCommandList::ResourceBarrier(GTexture* pTexture, D3D12_RESOURCE_STATES targetstate)
	{
		if (pTexture->State != targetstate)
		{
			D3D12_RESOURCE_BARRIER barrier;
			barrier.Transition.pResource = pTexture->GetGResurce();
			barrier.Transition.StateBefore = pTexture->State;
			barrier.Transition.StateAfter = targetstate;
			barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			APICommandlist->ResourceBarrier(1, &barrier);

			pTexture->State = targetstate;
		}
	}
	Void	GCommandList::ResourceBarrier(GBuffer* pBuffer, D3D12_RESOURCE_STATES targetstate)
	{
		if (pBuffer->State != targetstate &&
			pBuffer->State != D3D12_RESOURCE_STATE_GENERIC_READ)
		{
			D3D12_RESOURCE_BARRIER barrier;
			barrier.Transition.pResource = pBuffer->Real;
			barrier.Transition.StateBefore = pBuffer->State;
			barrier.Transition.StateAfter = targetstate;
			barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			APICommandlist->ResourceBarrier(1, &barrier);

			pBuffer->State = targetstate;
		}
	}

	Void	GCommandList::BindPipeline(GPipeline* Pipeline)
	{
		APICommandlist->SetPipelineState(Pipeline->PSO);
	}
	Void	GCommandList::BindRenderTarget(
		Boolean bClearDepthTarget,
		const IGTexture* DT, UInt32 uDTSubresourceIdx,
		Boolean bClearColorTarget,
		const IGTexture* RT0, UInt32 uRT0SubresourceIdx,
		const IGTexture* RT1, UInt32 uRT1SubresourceIdx,
		const IGTexture* RT2, UInt32 uRT2SubresourceIdx,
		const IGTexture* RT3, UInt32 uRT3SubresourceIdx,
		const IGTexture* RT4, UInt32 uRT4SubresourceIdx,
		const IGTexture* RT5, UInt32 uRT5SubresourceIdx,
		const IGTexture* RT6, UInt32 uRT6SubresourceIdx,
		const IGTexture* RT7, UInt32 uRT7SubresourceIdx)
	{
		FrameBuffer* pFrameBuffer = GRendererResource::GetCurFrameBuffer();

		if (DT != nullptr)
		{
			ResourceBarrier((GTexture*)DT, D3D12_RESOURCE_STATE_DEPTH_WRITE);

			if (pFrameBuffer->DSV != DT || pFrameBuffer->DSVSubresourceIndex != uDTSubresourceIdx)
			{
				pFrameBuffer->DSV = DT;
				pFrameBuffer->DSVSubresourceIndex = uDTSubresourceIdx;

				GRendererResource::SetDepthTarget(
					pFrameBuffer->DSVHeap.ElementCPUHandle(0), (const GTexture*)DT, uDTSubresourceIdx);
			}

			if (bClearDepthTarget)
			{
				APICommandlist->ClearDepthStencilView(
					pFrameBuffer->DSVHeap.ElementCPUHandle(0),
					D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, DEPTH_CLEAR_VALUE, 0, 0, nullptr);
			}
		}

		const IGTexture* RTs[] = { RT0, RT1, RT2, RT3, RT4, RT5, RT6, RT7 };
		UInt32 RTSubresource[] = {
			uRT0SubresourceIdx, uRT1SubresourceIdx, uRT2SubresourceIdx, uRT3SubresourceIdx,
			uRT4SubresourceIdx, uRT5SubresourceIdx, uRT6SubresourceIdx, uRT7SubresourceIdx
		};

		UInt32 uRTCount = 0;
		for (UInt32 u = 0; u < 8; u++)
		{
			if (RTs[u] == nullptr)
				break;

			uRTCount++;
			ResourceBarrier((GTexture*)RTs[u], D3D12_RESOURCE_STATE_RENDER_TARGET);

			if (pFrameBuffer->RTV[u] != RTs[u] || pFrameBuffer->RTVSubresourceIndex[u] != RTSubresource[u])
			{
				pFrameBuffer->RTV[u] = RTs[u];
				pFrameBuffer->RTVSubresourceIndex[u] = RTSubresource[u];

				GRendererResource::SetRenderTarget(
					pFrameBuffer->RTVHeap.ElementCPUHandle(u), (const GTexture*)RTs[u], RTSubresource[u]);
			}

			if (bClearColorTarget)
			{
				APICommandlist->ClearRenderTargetView(pFrameBuffer->RTVHeap.ElementCPUHandle(u),
					&((const GTexture*)RTs[u])->GetClearValue()[0], 0, nullptr);
			}
		}

		APICommandlist->OMSetRenderTargets(
			uRTCount, &pFrameBuffer->RTVHeap.ElementCPUHandle(0), True, DT != nullptr ? &pFrameBuffer->DSVHeap.ElementCPUHandle(0) : nullptr);

		GRendererResource::NextFrameBuffer();
	}
	Void	GCommandList::BindVertexBuffer(UInt32 uSlot, IGBuffer* pBuffer, UInt32 uStride)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);

		GBuffer* pRealBuffer = (GBuffer*)pBuffer;
		ResourceBarrier(pRealBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);

		D3D12_VERTEX_BUFFER_VIEW view;
		view.BufferLocation = pRealBuffer->Real->GetGPUVirtualAddress();
		view.SizeInBytes = pRealBuffer->Size;
		view.StrideInBytes = uStride;
		APICommandlist->IASetVertexBuffers(uSlot, 1, &view);
	}
	Void	GCommandList::BindIndexBuffer(IGBuffer* pBuffer, Boolean b32Bit)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);

		GBuffer* pRealBuffer = (GBuffer*)pBuffer;
		ResourceBarrier(pRealBuffer, D3D12_RESOURCE_STATE_INDEX_BUFFER);

		D3D12_INDEX_BUFFER_VIEW view;
		view.BufferLocation = ((GBuffer*)pBuffer)->Real->GetGPUVirtualAddress();
		view.Format = b32Bit ? DXGI_FORMAT_R32_UINT : DXGI_FORMAT_R16_UINT;
		view.SizeInBytes = ((GBuffer*)pBuffer)->Size;

		APICommandlist->IASetIndexBuffer(&view);
	}

	Void	GCommandList::BindShaderResource(ShaderType eType, UInt32 uSlot, const IGTexture* pTexture)
	{
		ASSERT_IF_FAILED(pTexture != nullptr);
		RasterizerShaderResourceSet* pRSet = GRendererResource::GetCurRasterizerShaderResourceSet();

		GTexture* pRealTexture = (GTexture*)pTexture;

		D3D12_RESOURCE_STATES targetState = eType == ShaderType::EPixelShader ?
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE :
			D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE;

		ResourceBarrier(pRealTexture, targetState);

		switch (eType)
		{
		case ShaderType::EVertexShader:
			if (pRSet->VS_SRV[uSlot] != pTexture)
			{
				pRSet->VS_SRV[uSlot] = pTexture;

				GRendererResource::SetShaderResource(pRSet->ResourceHeap.ElementCPUHandle((const UInt8)RasterizerRootSignatureDesc::EVS_SRV_BEGIN + uSlot), pRealTexture);
			}
			break;
		case ShaderType::EPixelShader:
			if (pRSet->PS_SRV[uSlot] != pTexture)
			{
				pRSet->PS_SRV[uSlot] = pTexture;

				GRendererResource::SetShaderResource(pRSet->ResourceHeap.ElementCPUHandle((const UInt8)RasterizerRootSignatureDesc::EPS_SRV_BEGIN + uSlot), pRealTexture);
			}
			break;
		case ShaderType::EComputeShader:
		{
			ComputeShaderResourceSet* pCSet = GRendererResource::GetCurComputeShaderResourceSet();
			if (pCSet->SRV[uSlot] != pTexture)
			{
				pCSet->SRV[uSlot] = pTexture;

				GRendererResource::SetShaderResource(pCSet->ResourceHeap.ElementCPUHandle((const UInt8)ComputeRootSignatureDesc::ESRV_BEGIN + uSlot), pRealTexture);
			}
		}
		break;
		}
	}
	Void	GCommandList::BindUnorderResource(ShaderType eType, UInt32 uSlot, const  IGTexture* pTexture, UInt32 uSubresourceIdx)
	{
		ASSERT_IF_FAILED(pTexture != nullptr);

		GTexture* pRealTexture = (GTexture*)pTexture;
		ResourceBarrier(pRealTexture, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);

		switch (eType)
		{
		case MXVisual::ShaderType::EVertexShader:
		case MXVisual::ShaderType::EPixelShader:
			ASSERT_IF_FAILED("!Not Supported");
			break;
		case MXVisual::ShaderType::EComputeShader:
		{
			ComputeShaderResourceSet* pCSet = GRendererResource::GetCurComputeShaderResourceSet();
			if (pCSet->UAV[uSlot] != pRealTexture || pCSet->UAVSubresourceIndex[uSlot] != uSubresourceIdx)
			{
				pCSet->UAV[uSlot] = pRealTexture;
				pCSet->UAVSubresourceIndex[uSlot] = uSubresourceIdx;

				GRendererResource::SetUnorderedAccess(pCSet->ResourceHeap.ElementCPUHandle((const UInt8)ComputeRootSignatureDesc::EUAV_BEGIN + uSlot), pRealTexture, uSubresourceIdx);
			}
		}
		break;
		}
	}
	Void	GCommandList::BindConstantBuffer(ShaderType eType, UInt32 uSlot, const IGBuffer* pBuffer)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);
		RasterizerShaderResourceSet*	pRSet = GRendererResource::GetCurRasterizerShaderResourceSet();

		GBuffer* pRealBuffer = (GBuffer*)pBuffer;
		ResourceBarrier(pRealBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);

		switch (eType)
		{
		case MXVisual::ShaderType::EVertexShader:
			if (pRSet->VS_CBV[uSlot] != pRealBuffer)
			{
				pRSet->VS_CBV[uSlot] = pRealBuffer;

				GRendererResource::SetConstantBuffer(pRSet->ResourceHeap.ElementCPUHandle((const UInt8)RasterizerRootSignatureDesc::EVS_CBV_BEGIN + uSlot), pRealBuffer);
			}
			break;
		case MXVisual::ShaderType::EPixelShader:
			if (pRSet->PS_CBV[uSlot] != pRealBuffer)
			{
				pRSet->PS_CBV[uSlot] = pRealBuffer;

				GRendererResource::SetConstantBuffer(pRSet->ResourceHeap.ElementCPUHandle((const UInt8)RasterizerRootSignatureDesc::EPS_CBV_BEGIN + uSlot), pRealBuffer);
			}
			break;
		case MXVisual::ShaderType::EComputeShader:
		{
			ComputeShaderResourceSet*		pCSet = GRendererResource::GetCurComputeShaderResourceSet();
			if (pCSet->CBV[uSlot] != pRealBuffer)
			{
				pCSet->CBV[uSlot] = pRealBuffer;

				GRendererResource::SetConstantBuffer(pCSet->ResourceHeap.ElementCPUHandle((const UInt8)ComputeRootSignatureDesc::ECBV_BEGIN + uSlot), pRealBuffer);
			}
		}
		break;
		}
	}
	Void	GCommandList::BindSampler(ShaderType eType, UInt32 uSlot, const IGSampler* pSampler)
	{
		ASSERT_IF_FAILED(pSampler != nullptr);
		RasterizerShaderResourceSet* pRSet = GRendererResource::GetCurRasterizerShaderResourceSet();

		const GSamplerDesc* pRealSampler = (const GSamplerDesc*)pSampler;
		switch (eType)
		{
		case MXVisual::ShaderType::EVertexShader:
			if (pRSet->VS_Sampler[uSlot] != pSampler)
			{
				pRSet->VS_Sampler[uSlot] = pSampler;

				GRendererResource::SetSampler(pRSet->SamplerHeap.ElementCPUHandle((const UInt8)RasterizerRootSignatureDesc::EVS_SAMPLER_BEGIN + uSlot),
					pRealSampler->eAddressMode, pRealSampler->eFilterMode, GVarMaxAnisotropy.nValue);
			}
			break;
		case MXVisual::ShaderType::EPixelShader:
			if (pRSet->VS_Sampler[uSlot] != pSampler)
			{
				pRSet->VS_Sampler[uSlot] = pSampler;
				GRendererResource::SetSampler(pRSet->SamplerHeap.ElementCPUHandle((const UInt8)RasterizerRootSignatureDesc::EPS_SAMPLER_BEGIN + uSlot),
					pRealSampler->eAddressMode, pRealSampler->eFilterMode, GVarMaxAnisotropy.nValue);
			}
			break;
		case MXVisual::ShaderType::EComputeShader:
		{
			ComputeShaderResourceSet*		pCSet = GRendererResource::GetCurComputeShaderResourceSet();
			if (pCSet->Sampler[uSlot] != pSampler)
			{
				pCSet->Sampler[uSlot] = pSampler;
				GRendererResource::SetSampler(pCSet->SamplerHeap.ElementCPUHandle((const UInt8)ComputeRootSignatureDesc::ESAMPLER_BEGIN + uSlot),
					pRealSampler->eAddressMode, pRealSampler->eFilterMode, GVarMaxAnisotropy.nValue);
			}
		}
		break;
		}
	}

	Void	GCommandList::Draw(UInt32 uVertexCount, UInt32 uInstanceCount, Boolean bTriangleNotLine)
	{
		RasterizerShaderResourceSet* sets = GRendererResource::GetCurRasterizerShaderResourceSet();

		sets->BindGResource(this);

		Float4 f(1.0f, 1.0f, 1.0f, 1.0f);
		APICommandlist->OMSetBlendFactor(&f[0]);
		APICommandlist->IASetPrimitiveTopology(bTriangleNotLine ? D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST : D3D_PRIMITIVE_TOPOLOGY_LINELIST);
		APICommandlist->DrawInstanced(uVertexCount, uInstanceCount, 0, 0);

		GRendererResource::NextRasterizerShaderResourceSet();
		GRendererResource::FrameStatistics.uDrawCall++;
	}
	Void	GCommandList::DrawIndex(UInt32 uIndexCount, UInt32 uIndexOffset, UInt32 uInstanceCount, Boolean bTriangleNotLine)
	{
		RasterizerShaderResourceSet* sets = GRendererResource::GetCurRasterizerShaderResourceSet();

		sets->BindGResource(this);

		Float4 f(1.0f, 1.0f, 1.0f, 1.0f);
		APICommandlist->OMSetBlendFactor(&f[0]);
		APICommandlist->IASetPrimitiveTopology(bTriangleNotLine ? D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST : D3D_PRIMITIVE_TOPOLOGY_LINELIST);
		APICommandlist->DrawIndexedInstanced(uIndexCount, uInstanceCount, uIndexOffset, 0, 0);

		GRendererResource::NextRasterizerShaderResourceSet();
		GRendererResource::FrameStatistics.uDrawCall++;
	}

	Void	GCommandList::Dispatch(UInt32 X, UInt32 Y, UInt32 Z)
	{
		ComputeShaderResourceSet* sets = GRendererResource::GetCurComputeShaderResourceSet();

		sets->BindGResource(this);

		APICommandlist->Dispatch(X, Y, Z);
		GRendererResource::NextComputeShaderResourceSet();
	}

	Void	GCommandList::Flush()
	{
		APICommandlist->ClearState(nullptr);
	}

	Void	GCommandList::CopyResource(IGTexture* pDest, IGTexture* pSrc)
	{
		ASSERT_IF_FAILED(pDest != nullptr && pSrc != nullptr);

		GTexture* pRealDest = (GTexture*)pDest;
		GTexture* pRealSrc = (GTexture*)pSrc;

		ResourceBarrier(pRealDest, D3D12_RESOURCE_STATE_COPY_DEST);
		ResourceBarrier(pRealSrc, D3D12_RESOURCE_STATE_COPY_SOURCE);

		APICommandlist->CopyResource(pRealDest->GetGResurce(), pRealSrc->GetGResurce());
	}

	Boolean	GCommandList::Map(IGTexture* pTexture, UInt32 uSubResouce, Boolean bRead, MapRegion& region)
	{
		ID3D12Resource* pRes = ((GTexture*)pTexture)->GetGResurce();

		UInt32 uMipMap = uSubResouce / pTexture->GetDepth();
		UInt32 uWidth = pTexture->GetWidth();
		uWidth = Math::Max<SInt32>(1, uWidth >> uMipMap);
		region.uPitch = pTexture->GetWidth() * PixelFormatSize[pTexture->GetFormat()] / 8;

		D3D12_RANGE read_range;
		if (SUCCEEDED(pRes->Map(uSubResouce, bRead ? &read_range : nullptr, &region.pAddress)))
		{
			return True;
		}
		return False;
	}
	Void	GCommandList::Unmap(IGTexture* pTexture, UInt32 uSubResouce)
	{
		ID3D12Resource* pRes = ((GTexture*)pTexture)->GetGResurce();
		pRes->Unmap(uSubResouce, nullptr);
	}

	Boolean	GCommandList::Map(IGBuffer* pBuffer, Boolean bRead, MapRegion& region)
	{
		ID3D12Resource* pRes = ((GBuffer*)pBuffer)->Real;

		region.uPitch = ((GBuffer*)pBuffer)->Size;

		D3D12_RANGE read_range;
		if (SUCCEEDED(pRes->Map(0, bRead ? &read_range : nullptr, &region.pAddress)))
		{
			return True;
		}
		return False;
	}
	Void	GCommandList::Unmap(IGBuffer* pBuffer)
	{
		ID3D12Resource* pRes = ((GBuffer*)pBuffer)->Real;
		pRes->Unmap(0, nullptr);
	}

	Boolean	CheckUpdateBuffer(UInt32 uSize)
	{
		//TODO!! Ĭ�ϵ�Buffer̫����	64mb
		const UInt32 MAX_UPLOAD_BUFFER_SIZE = 4096 * 4096 * 32 / 8;
		if (uSize > MAX_UPLOAD_BUFFER_SIZE)
			return False;

		if (GRendererResource::UploadBuffer == nullptr)
		{
			MX_RELEASE_INTERFACE(GRendererResource::UploadBuffer);

			GRendererResource::UploadBuffer = new GBuffer;
			D3D12_HEAP_PROPERTIES heap_prop;
			heap_prop.CreationNodeMask = 1;
			heap_prop.VisibleNodeMask = 1;
			heap_prop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
			heap_prop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
			heap_prop.Type = D3D12_HEAP_TYPE_UPLOAD;

			D3D12_RESOURCE_DESC resource_desc;
			resource_desc.Flags = D3D12_RESOURCE_FLAG_DENY_SHADER_RESOURCE;
			resource_desc.Alignment = D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT;
			resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
			resource_desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
			resource_desc.Format = DXGI_FORMAT_UNKNOWN;
			resource_desc.DepthOrArraySize = 1;
			resource_desc.Height = 1;
			resource_desc.Width = Math::AlignmentedSize(MAX_UPLOAD_BUFFER_SIZE, 256);
			resource_desc.MipLevels = 1;
			resource_desc.SampleDesc.Count = 1;
			resource_desc.SampleDesc.Quality = 0;

			if (FAILED(GRenderer::Device->CreateCommittedResource(&heap_prop, D3D12_HEAP_FLAG_NONE,
				&resource_desc, D3D12_RESOURCE_STATE_GENERIC_READ, NULL,
				IID_PPV_ARGS(&GRendererResource::UploadBuffer->Real))))
			{
				return False;
			}

			GRendererResource::UploadBuffer->Size = resource_desc.Width;
			GRendererResource::UploadBuffer->State = D3D12_RESOURCE_STATE_COMMON;
			GRendererResource::UploadBuffer->Stride = resource_desc.Width;

			GRendererResource::Resources.Add(GRendererResource::UploadBuffer);

			NAME_API_OBJECT(GRendererResource::UploadBuffer->Real, UPLOAD_BUFFER);
		}

		return True;
	}

	Boolean	GCommandList::SubmitData(IGTexture* pTexture, UInt32 uSubResouce, UInt32 uSize, const Void* pData)
	{
		ASSERT_IF_FAILED(pTexture != nullptr);

		UInt64 uRequiredSize = 0;
		D3D12_RESOURCE_DESC Desc = ((GTexture*)pTexture)->GetGResurce()->GetDesc();

		D3D12_PLACED_SUBRESOURCE_FOOTPRINT Footprint;
		UINT NumRows = 0;
		GRenderer::Device->GetCopyableFootprints(&Desc,
			uSubResouce, 1, 0, &Footprint, &NumRows, nullptr, &uRequiredSize);

		if (!CheckUpdateBuffer(uRequiredSize))
			return False;

		GTexture* pResource = (GTexture*)pTexture;
		Void* pMappedAddress = nullptr;
		if (SUCCEEDED(GRendererResource::UploadBuffer->Real->Map(0, NULL, &pMappedAddress)))
		{
			memcpy((UInt8*)pMappedAddress + Footprint.Offset, pData, uSize);
			GRendererResource::UploadBuffer->Real->Unmap(0, nullptr);
		}

		ResourceBarrier(pResource, D3D12_RESOURCE_STATE_COPY_DEST);

		D3D12_TEXTURE_COPY_LOCATION Dst;
		Dst.pResource = pResource->GetGResurce();
		Dst.Type = D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
		Dst.SubresourceIndex = uSubResouce;

		D3D12_TEXTURE_COPY_LOCATION Src;
		Src.pResource = GRendererResource::UploadBuffer->Real;
		Src.Type = D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;
		Src.PlacedFootprint = Footprint;

		APICommandlist->CopyTextureRegion(&Dst, 0, 0, 0, &Src, nullptr);

		GRenderer::Execute();
		return True;
	}
	Boolean GCommandList::SubmitData(IGBuffer* pBuffer, UInt32 uSize, const Void* pData)
	{
		ASSERT_IF_FAILED(pBuffer != nullptr);

		if (!CheckUpdateBuffer(uSize))
			return False;

		GBuffer* pResource = (GBuffer*)pBuffer;
		Void* pMappedAddress = nullptr;
		if (SUCCEEDED(GRendererResource::UploadBuffer->Real->Map(0, NULL, &pMappedAddress)))
		{
			memcpy(pMappedAddress, pData, uSize);
			GRendererResource::UploadBuffer->Real->Unmap(0, nullptr);
		}

		ResourceBarrier(pResource, D3D12_RESOURCE_STATE_COPY_DEST);

		APICommandlist->CopyBufferRegion(pResource->Real, 0, GRendererResource::UploadBuffer->Real, 0, uSize);

		GRenderer::Execute();
		return True;
	}
}