#include "GTexture.h"
#include "GRenderer.h"

namespace MXVisual
{
	GTexture::GTexture(UInt32 UniqueID)
		: m_uUniqueID(UniqueID)
		, m_pResource(nullptr)
		, m_pMapData(nullptr)
		, m_bTargetable(False)
		, m_bUnorderAccessable(False)
	{
		m_nRefCount = 1;
	}
	GTexture::~GTexture()
	{
		MX_RELEASE_INTERFACE(m_pResource);
	}

	Boolean	GTexture::Init(const IGTexture::Description& desc)
	{
		Boolean bGenTarget = desc.bTargetable | desc.bGenerateMipMap;

		m_bTargetable = bGenTarget;
		m_bUnorderAccessable = desc.bUnorderAccessable;
		m_eType = desc.eType;
		m_eFormat = desc.eFormat;
		m_uWidth = desc.uWidth;
		m_uHeight = desc.uHeight;
		m_uDepth = desc.uDepth;
		m_uMipMapCount = Math::Min<UInt32>(desc.uMipLevel, Math::Min<UInt32>(log2(desc.uWidth), log2(desc.uHeight)));
		m_f4ClearValue = (m_eFormat == EPF_D24 || m_eFormat == EPF_D32) ?
			Float4(DEPTH_CLEAR_VALUE, DEPTH_CLEAR_VALUE, DEPTH_CLEAR_VALUE, DEPTH_CLEAR_VALUE) : desc.f4ClearValue;

		if (bGenTarget && desc.bDynamic)
		{
			Log::Output("GraphicsD3D12", "Targetable Texture Cannot be Dyanmic");
			return False;
		}

		UInt32 BindFlags = D3D12_RESOURCE_FLAG_NONE;
		D3D12_RESOURCE_STATES InitState = desc.bDynamic ? D3D12_RESOURCE_STATE_COMMON : D3D12_RESOURCE_STATE_COPY_DEST;

		D3D12_CLEAR_VALUE clear_value;
		if (m_eFormat == EPF_D24 || m_eFormat == EPF_D32)
		{
			clear_value.Format = PixelFormatMap[ResourceType::ERT_DepthStencilTargetView][m_eFormat];
			clear_value.DepthStencil.Depth = DEPTH_CLEAR_VALUE;
			clear_value.DepthStencil.Stencil = 0;
			BindFlags |= m_bTargetable ? D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL : 0;
			InitState = m_bTargetable ? D3D12_RESOURCE_STATE_DEPTH_WRITE : InitState;
		}
		else
		{
			clear_value.Format = PixelFormatMap[ResourceType::ERT_Resouce][m_eFormat];
			clear_value.Color[0] = m_f4ClearValue[0];
			clear_value.Color[1] = m_f4ClearValue[1];
			clear_value.Color[2] = m_f4ClearValue[2];
			clear_value.Color[3] = m_f4ClearValue[3];
			BindFlags |= m_bTargetable ? D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET : 0;
			InitState = m_bTargetable ? D3D12_RESOURCE_STATE_RENDER_TARGET : InitState;
		}

		if (m_bUnorderAccessable)
		{
			BindFlags |= D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;
		}

		D3D12_HEAP_PROPERTIES heap_prop;
		heap_prop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		heap_prop.CreationNodeMask = 1;
		heap_prop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		heap_prop.Type = D3D12_HEAP_TYPE_DEFAULT;
		heap_prop.VisibleNodeMask = 1;

		D3D12_RESOURCE_DESC resource_desc;
		resource_desc.Alignment = 0;
		resource_desc.Width = m_uWidth;
		resource_desc.Height = m_uHeight;
		resource_desc.DepthOrArraySize = m_uDepth;
		resource_desc.MipLevels = m_uMipMapCount;
		resource_desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
		resource_desc.SampleDesc.Count = 1;
		resource_desc.SampleDesc.Quality = 0;
		resource_desc.Flags = (D3D12_RESOURCE_FLAGS)BindFlags;
		resource_desc.Format = PixelFormatMap[ResourceType::ERT_Resouce][m_eFormat];

		switch (m_eType)
		{
		case TextureType::ETexture1D:
			resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE1D;
			resource_desc.DepthOrArraySize = 1;
			resource_desc.Height = 1;
			break;
		case TextureType::ETexture2D:
			resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
			resource_desc.DepthOrArraySize = 1;
			break;
		case TextureType::ETextureCube:
			resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
			resource_desc.DepthOrArraySize = 6;
			break;
		case TextureType::ETexture3D:
			resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE3D;
		}

		ID3D12Resource* pTexture = nullptr;
		if (FAILED(GRenderer::Device->CreateCommittedResource(
			&heap_prop, D3D12_HEAP_FLAG_NONE,
			&resource_desc, InitState, m_bTargetable ? &clear_value : nullptr,
			IID_PPV_ARGS(&pTexture))))
		{
			Log::Output("GraphicsD3D12", "Create Texture Resource Failed");
			return False;
		}

		NAME_API_OBJECT(pTexture, TEXTURE);
		State = InitState;
		m_pResource = pTexture;
		return True;
	}
	Boolean	GTexture::InitFromResource(ID3D12Resource* pResource, const IGTexture::Description& desc)
	{
		Boolean bGenTarget = desc.bTargetable | desc.bGenerateMipMap;

		m_bTargetable = bGenTarget;
		m_eType = desc.eType;
		m_eFormat = desc.eFormat;
		m_uWidth = desc.uWidth;
		m_uHeight = desc.uHeight;
		m_uDepth = desc.uDepth;
		m_uMipMapCount = Math::Min<UInt32>(desc.uMipLevel, Math::Min<UInt32>(log2(desc.uWidth), log2(desc.uHeight)));

		if (bGenTarget && desc.bDynamic)
		{
			Log::Output("GraphicsD3D12", "Texture With Target Cannot be Dyanmic");
			return False;
		}
		State = D3D12_RESOURCE_STATE_COMMON;
		m_pResource = pResource;
		return True;
	}

	Boolean	GTexture::Map(UInt32 uDepth, UInt32 uMipMap, MapRegion& region, Boolean bReadBack)
	{
		if (bReadBack)
		{
			ASSERT_IF_FAILED(!"TODO");
		}
		else
		{
			MX_DELETE(m_pMapData);
			m_pMapData = new MappedSubResource;
			m_pMapData->uDepth = uDepth;
			m_pMapData->uMip = uMipMap;

			D3D12_RESOURCE_DESC desc = m_pResource->GetDesc();

			UInt64 uBufferSize = 0;
			GRenderer::Device->GetCopyableFootprints(&desc,
				m_pMapData->uDepth + m_pMapData->uMip * m_uDepth,
				1, 0, nullptr, nullptr, nullptr, &uBufferSize);

			UInt32 uWidth = m_uWidth;
			UInt32 uHeight = m_uHeight;
			uWidth = Math::Max<SInt32>(1, uWidth >> uMipMap);
			uHeight = Math::Max<SInt32>(1, uHeight >> uMipMap);

			region.uPitch = uBufferSize / uHeight;
			m_pMapData->uSize = uBufferSize;
			m_pMapData->pCPUBuffer = new UInt8[m_pMapData->uSize];
			region.pAddress = m_pMapData->pCPUBuffer;
			return True;
		}
		return False;
	}
	Void	GTexture::Unmap()
	{
		if (m_pMapData != nullptr)
		{
			UInt32 uWidth = m_uWidth;
			UInt32 uHeight = m_uHeight;
			uWidth = Math::Max<SInt32>(1, uWidth >> m_pMapData->uMip);
			uHeight = Math::Max<SInt32>(1, uHeight >> m_pMapData->uMip);

			GRenderer::CommandList()->SubmitData(this,
				m_pMapData->uDepth + m_pMapData->uMip * m_uDepth,
				m_pMapData->uSize, m_pMapData->pCPUBuffer);

			delete m_pMapData;
			m_pMapData = nullptr;
		}
	}
}