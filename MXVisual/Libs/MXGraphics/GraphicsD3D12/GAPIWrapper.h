#pragma once
#ifndef _G_API_WRAPPER_
#define _G_API_WRAPPER_
#include "MXPlatform.h"
#include "MXInterface.h"
#include "d3d12.h"

namespace MXVisual
{
	class IGTexture;

	template<D3D12_DESCRIPTOR_HEAP_TYPE HeapType>
	class DescriptorHeap
	{
	public:
		DescriptorHeap(UInt32 DescriptorCount, Boolean bShaderVisible)
			: m_uSize(DescriptorCount)
		{
			D3D12_DESCRIPTOR_HEAP_DESC desc;
			desc.Flags = bShaderVisible ? D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE : D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
			desc.NodeMask = 1;
			desc.NumDescriptors = DescriptorCount;
			desc.Type = HeapType;

			HRESULT hr = GRenderer::Device->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&m_pHeap));
			ASSERT_IF_FAILED(SUCCEEDED(hr));
		}

		D3D12_CPU_DESCRIPTOR_HANDLE ElementCPUHandle(UInt32 uIndex);
		D3D12_GPU_DESCRIPTOR_HANDLE ElementGPUHandle(UInt32 uIndex);

		ID3D12DescriptorHeap* GetGResource() const { return m_pHeap; }
	protected:
		ID3D12DescriptorHeap*	m_pHeap;
		UInt32					m_uSize;
	};

	enum class RasterizerRootSignatureDesc : UInt8
	{
		EVS_CBV_BEGIN = 0,
		EVS_CBV_COUNT = 8,

		EVS_SRV_BEGIN = EVS_CBV_BEGIN + EVS_CBV_COUNT,
		EVS_SRV_COUNT = 8,

		EPS_CBV_BEGIN = EVS_SRV_BEGIN + EVS_SRV_COUNT,
		EPS_CBV_COUNT = 8,

		EPS_SRV_BEGIN = EPS_CBV_BEGIN + EPS_CBV_COUNT,
		EPS_SRV_COUNT = 32,

		EVS_SAMPLER_BEGIN = 0,
		EVS_SAMPLER_COUNT = 4,

		EPS_SAMPLER_BEGIN = EVS_SAMPLER_BEGIN + EVS_SAMPLER_COUNT,
		EPS_SAMPLER_COUNT = 8,
	};
	enum class ComputeRootSignatureDesc : UInt8
	{
		ECBV_BEGIN = 0,
		ECBV_COUNT = 8,

		ESRV_BEGIN = ECBV_BEGIN + ECBV_COUNT,
		ESRV_COUNT = 8,

		EUAV_BEGIN = ESRV_BEGIN + ESRV_COUNT,
		EUAV_COUNT = 16,

		ESAMPLER_BEGIN = 0,
		ESAMPLER_COUNT = 4,
	};
	struct FrameBuffer
	{
		const IGTexture*	RTV[8];
		UInt32				RTVSubresourceIndex[8];
		const IGTexture*	DSV;
		UInt32				DSVSubresourceIndex;
		DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_RTV> RTVHeap;
		DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_DSV> DSVHeap;

		FrameBuffer()
			: RTVHeap(8, False)
			, DSVHeap(1, False)
		{
			memset(RTV, 0, sizeof(RTV));
			memset(RTVSubresourceIndex, 0, sizeof(RTVSubresourceIndex));
			DSV = nullptr;
			DSVSubresourceIndex;
		}
	};

	struct RasterizerShaderResourceSet
	{
		const IGBuffer*		VS_CBV[(const UInt8)RasterizerRootSignatureDesc::EVS_CBV_COUNT];
		const IGTexture*	VS_SRV[(const UInt8)RasterizerRootSignatureDesc::EVS_SRV_COUNT];
		const IGSampler*	VS_Sampler[(const UInt8)RasterizerRootSignatureDesc::EVS_SAMPLER_COUNT];

		const IGBuffer*		PS_CBV[(const UInt8)RasterizerRootSignatureDesc::EPS_CBV_COUNT];
		const IGTexture*	PS_SRV[(const UInt8)RasterizerRootSignatureDesc::EPS_SRV_COUNT];
		const IGSampler*	PS_Sampler[(const UInt8)RasterizerRootSignatureDesc::EPS_SAMPLER_COUNT];

		DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV>	ResourceHeap;
		DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER>		SamplerHeap;

		RasterizerShaderResourceSet()
			: ResourceHeap(
			(const UInt8)RasterizerRootSignatureDesc::EVS_CBV_COUNT +
				(const UInt8)RasterizerRootSignatureDesc::EVS_SRV_COUNT +
				(const UInt8)RasterizerRootSignatureDesc::EPS_CBV_COUNT +
				(const UInt8)RasterizerRootSignatureDesc::EPS_SRV_COUNT, True)
			, SamplerHeap(
			(const UInt8)RasterizerRootSignatureDesc::EVS_SAMPLER_COUNT +
				(const UInt8)RasterizerRootSignatureDesc::EPS_SAMPLER_COUNT, True)
		{
			memset(VS_CBV, 0, sizeof(VS_CBV));
			memset(VS_SRV, 0, sizeof(VS_SRV));
			memset(VS_Sampler, 0, sizeof(VS_Sampler));
			memset(PS_CBV, 0, sizeof(PS_CBV));
			memset(PS_SRV, 0, sizeof(PS_SRV));
			memset(PS_Sampler, 0, sizeof(PS_Sampler));
		}
		Void BindGResource(IGCommandList* pCommandList);
	};

	struct ComputeShaderResourceSet
	{
		const IGBuffer*		CBV[(const UInt8)ComputeRootSignatureDesc::ECBV_COUNT] = { nullptr, };
		const IGTexture*	SRV[(const UInt8)ComputeRootSignatureDesc::ESRV_COUNT] = { nullptr, };
		const IGTexture*	UAV[(const UInt8)ComputeRootSignatureDesc::EUAV_COUNT] = { nullptr, };
		UInt32				UAVSubresourceIndex[(const UInt8)ComputeRootSignatureDesc::EUAV_COUNT] = { 0, };
		const IGSampler*	Sampler[(const UInt8)ComputeRootSignatureDesc::ESAMPLER_COUNT];

		DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV>	ResourceHeap;
		DescriptorHeap<D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER>		SamplerHeap;

		ComputeShaderResourceSet()
			: ResourceHeap(
			(const UInt8)ComputeRootSignatureDesc::ECBV_COUNT +
				(const UInt8)ComputeRootSignatureDesc::ESRV_COUNT +
				(const UInt8)ComputeRootSignatureDesc::EUAV_COUNT, True)
			, SamplerHeap(
			(const UInt8)ComputeRootSignatureDesc::ESAMPLER_COUNT +
				(const UInt8)ComputeRootSignatureDesc::ESAMPLER_COUNT, True)
		{
			memset(CBV, 0, sizeof(CBV));
			memset(SRV, 0, sizeof(SRV));
			memset(UAV, 0, sizeof(UAV));
			memset(UAVSubresourceIndex, 0, sizeof(UAVSubresourceIndex));
			memset(Sampler, 0, sizeof(Sampler));
		}
		Void BindGResource(IGCommandList* pCommandList);
	};

	struct RootSignature : public D3D12_ROOT_SIGNATURE_DESC
	{
		RootSignature() {};
		RootSignature(D3D12_ROOT_SIGNATURE_FLAGS flags, UInt32 uCount, const D3D12_ROOT_PARAMETER* pParams, UInt32 uStaticSmplersCount, D3D12_STATIC_SAMPLER_DESC* pSamplers)
		{
			Flags = flags;
			NumParameters = uCount;
			pParameters = pParams;
			NumStaticSamplers = uStaticSmplersCount;
			pStaticSamplers = pSamplers;
		}
	};

	struct RootParam : public D3D12_ROOT_PARAMETER
	{
		RootParam() {};
		RootParam(D3D12_SHADER_VISIBILITY visibility, UInt32 uCount, const D3D12_DESCRIPTOR_RANGE* pRange)
		{
			ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
			DescriptorTable.NumDescriptorRanges = uCount;
			DescriptorTable.pDescriptorRanges = pRange;
			ShaderVisibility = visibility;
		}
		RootParam(D3D12_SHADER_VISIBILITY visibility, UInt32 uRegisterIndex, D3D12_ROOT_PARAMETER_TYPE type)
		{
			ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
			ShaderVisibility = visibility;
			switch (ParameterType)
			{
			case D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS:
				Constants.Num32BitValues = 0;
				Constants.RegisterSpace = 0;
				Constants.ShaderRegister = uRegisterIndex;
				break;
			case D3D12_ROOT_PARAMETER_TYPE_CBV:
			case D3D12_ROOT_PARAMETER_TYPE_SRV:
			case D3D12_ROOT_PARAMETER_TYPE_UAV:
				Descriptor.RegisterSpace = 0;
				Descriptor.ShaderRegister = uRegisterIndex;
				break;
			}
		}
	};

	struct DescriptorRange : public D3D12_DESCRIPTOR_RANGE
	{
		DescriptorRange() {};
		DescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE type, UInt32 uBaseShaderRegister, UInt32 uCount, UInt32 uOffset)
		{
			NumDescriptors = uCount;
			BaseShaderRegister = uBaseShaderRegister;
			OffsetInDescriptorsFromTableStart = uOffset;
			RangeType = type;
			RegisterSpace = 0;
		}
	};
}

#endif