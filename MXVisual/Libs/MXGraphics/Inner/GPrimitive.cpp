#include "GPrimitive.h"
#include "GBoundingVolume.h"
#include "MXCommon.h"
#include "MXMath.h"
#include "GShaderManager.h"

namespace MXVisual
{
	GPrimitive::GPrimitive(UInt32 UniqueID)
		: m_uUniqueID(UniqueID)
		, m_pVertexBuffer(nullptr)
		, m_pIndexBuffer(nullptr)
		, m_pBoundingVolume(nullptr)
		, m_bDynamic(False)
		, m_b32BitIndex(True)
	{
	}
	GPrimitive::~GPrimitive()
	{
		if (m_pBoundingVolume != nullptr)
		{
			delete m_pBoundingVolume;
			m_pBoundingVolume = nullptr;
		}
	}

	Void GPrimitive::UpdateIndexBuffer(const UInt32* pIndex, UInt32 uIndexCount)
	{
		if (pIndex == nullptr) return;

		GRenderer::WaitUntilRendered();
		GRenderer::LockThread();
		if (m_bDynamic)
		{
			MapRegion region;
			if (GRenderer::CommandList()->Map(m_pIndexBuffer, False, region))
			{
				memcpy(region.pAddress, pIndex, uIndexCount * sizeof(UInt32));
				GRenderer::CommandList()->Unmap(m_pIndexBuffer);
			}
#if defined(DEBUG)
			else
			{
				Log::Output("GraphicsRenderer", "UpdateIndexBuffer Failed");
			}
#endif
		}
		else
		{
			GRenderer::CommandList()->SubmitData(m_pIndexBuffer, uIndexCount * sizeof(UInt32), pIndex);
		}
		GRenderer::UnlockThread();
		m_uIndexCount = uIndexCount;
	}
	Void GPrimitive::BindGResource(IGCommandList* pCommandList)
	{
		pCommandList->BindIndexBuffer(m_pIndexBuffer, m_b32BitIndex);
	}

	Boolean GPrimitiveLine::Init(Boolean bDynamic,
		UInt32 uVertexCapacity,
		UInt32 uIndexCapacity,
		String* pStrError)
	{
		m_pVertexBuffer = GRendererResource::CreateBuffer(
			BufferType::EVertexBuffer,
			bDynamic,
			uVertexCapacity * sizeof(GVertexRigid));

		m_pIndexBuffer = GRendererResource::CreateBuffer(
			BufferType::EIndexBuffer,
			bDynamic,
			uIndexCapacity * sizeof(UInt32));

		if (m_pVertexBuffer == nullptr || m_pIndexBuffer == nullptr)
			return False;

		m_pBoundingVolume = new GVolumnAABB();

		m_bDynamic = bDynamic;
		m_uVertexCapacity = uVertexCapacity;
		m_uIndexCapacity = uIndexCapacity;
		return True;
	}
	Boolean	GPrimitiveLine::CalIntersect(const Float3& f3Begin, const Float3& f3Direction, Float1 fDistance, const Matrix4* pTransform, Array<Float3>* pOutIntersectPoint) const
	{
		//Will Add Mesh Intersection
		if (m_pBoundingVolume != nullptr)
		{
			return m_pBoundingVolume->CalIntersect(f3Begin, f3Direction, fDistance, pTransform, pOutIntersectPoint);
		}
		return False;
	}
	Void GPrimitiveLine::UpdateBoundingVolume(const IGVertex* pVertex, UInt32 uVertexCount)
	{
		if (m_pBoundingVolume != nullptr)
		{
			m_pBoundingVolume->Reset();
			GVertexRigid* vertex = (GVertexRigid*)pVertex;
			for (UInt32 u = 0; u < uVertexCount; u++)
			{
				m_pBoundingVolume->AddVertexPosition(Math::Split(vertex[u].f4Position));
			}
		}
	}

	Boolean GPrimitiveRigid::Init(Boolean bDynamic,
		UInt32 uVertexCapacity,
		UInt32 uIndexCapacity,
		String* pStrError)
	{
		m_pVertexBuffer = GRendererResource::CreateBuffer(
			BufferType::EVertexBuffer,
			bDynamic,
			uVertexCapacity * sizeof(GVertexRigid));

		m_pIndexBuffer = GRendererResource::CreateBuffer(
			BufferType::EIndexBuffer,
			bDynamic,
			uIndexCapacity * sizeof(UInt32));

		if (m_pVertexBuffer == nullptr || m_pIndexBuffer == nullptr)
			return False;

		m_pBoundingVolume = new GVolumnAABB();

		m_bDynamic = bDynamic;
		m_uVertexCapacity = uVertexCapacity;
		m_uIndexCapacity = uIndexCapacity;
		return True;
	}
	Boolean	GPrimitiveRigid::CalIntersect(const Float3& f3Begin, const Float3& f3Direction, Float1 fDistance, const Matrix4* pTransform, Array<Float3>* pOutIntersectPoint) const
	{
		//Will Add Mesh Intersection
		if (m_pBoundingVolume != nullptr)
		{
			return m_pBoundingVolume->CalIntersect(f3Begin, f3Direction, fDistance, pTransform, pOutIntersectPoint);
		}
		return False;
	}
	Void	GPrimitiveRigid::UpdateBoundingVolume(const IGVertex* pVertex, UInt32 uVertexCount)
	{
		if (m_pBoundingVolume != nullptr)
		{
			m_pBoundingVolume->Reset();
			GVertexRigid* vertex = (GVertexRigid*)pVertex;
			for (UInt32 u = 0; u < uVertexCount; u++)
			{
				m_pBoundingVolume->AddVertexPosition(Math::Split(vertex[u].f4Position));
			}
		}
	}

	Boolean GPrimitiveSkin::Init(Boolean bDynamic,
		UInt32 uVertexCapacity,
		UInt32 uIndexCapacity,
		String* pStrError)
	{
		m_pVertexBuffer = GRendererResource::CreateBuffer(
			BufferType::EVertexBuffer,
			bDynamic,
			uVertexCapacity * sizeof(GVertexSkin));

		m_pIndexBuffer = GRendererResource::CreateBuffer(
			BufferType::EIndexBuffer,
			bDynamic,
			uIndexCapacity * sizeof(UInt32));

		if (m_pVertexBuffer == nullptr || m_pIndexBuffer == nullptr)
			return False;

		m_bDynamic = bDynamic;
		m_uVertexCapacity = uVertexCapacity;
		m_uIndexCapacity = uIndexCapacity;
		return True;
	}
	Boolean	GPrimitiveSkin::CalIntersect(const Float3& f3Begin, const Float3& f3Direction, Float1 fDistance, const Matrix4* pTransform, Array<Float3>* pOutIntersectPoint) const
	{

		return False;
	}
	Void	GPrimitiveSkin::UpdateBoundingVolume(const IGVertex* pVertex, UInt32 uVertexCount)
	{
		//TODO!!
	}

	Boolean GPrimitive2D::Init(Boolean bDynamic,
		UInt32 uVertexCapacity,
		UInt32 uIndexCapacity,
		String* pStrError)
	{
		m_pVertexBuffer = GRendererResource::CreateBuffer(
			BufferType::EVertexBuffer,
			bDynamic,
			uVertexCapacity * sizeof(GVertexRigid2D));

		m_pIndexBuffer = GRendererResource::CreateBuffer(
			BufferType::EIndexBuffer,
			bDynamic,
			uIndexCapacity * sizeof(UInt32));

		if (m_pVertexBuffer == nullptr || m_pIndexBuffer == nullptr)
			return False;

		m_bDynamic = bDynamic;
		m_uVertexCapacity = uVertexCapacity;
		m_uIndexCapacity = uIndexCapacity;
		return True;
	}
	Boolean	GPrimitive2D::CalIntersect(const Float3& f3Begin, const Float3& f3Direction, Float1 fDistance, const Matrix4* pTransform, Array<Float3>* pOutIntersectPoint) const
	{

		return False;
	}
	Void	GPrimitive2D::UpdateBoundingVolume(const IGVertex* pVertex, UInt32 uVertexCount)
	{
		//TODO!!
	}

	GPrimitiveTransformBuffer::GPrimitiveTransformBuffer(UInt32 UniqueID)
		: m_uUniqueID(UniqueID)
		, m_uMaxTransforms(0)
		, m_pBuffer(nullptr)
		, m_uTransforms(0)
	{
	}
	GPrimitiveTransformBuffer::~GPrimitiveTransformBuffer()
	{
		m_pBuffer = nullptr;
	}

	Boolean GPrimitiveTransformBuffer::Init(Boolean bForInstance, UInt32 uMaxTransform)
	{
		m_uMaxTransforms = uMaxTransform;
		UInt32 uSize = Math::AlignmentedSize(uMaxTransform * sizeof(Matrix4), 256);
		if (bForInstance)
		{
			m_pBuffer = GRendererResource::CreateBuffer(
				BufferType::EVertexBuffer, True, uSize);
		}
		else
		{
			m_pBuffer = GRendererResource::CreateBuffer(
				BufferType::EConstantBuffer, True, uSize);
		}
		m_bForInstance = bForInstance;
		return True;
	}
	Void	GPrimitiveTransformBuffer::SetTransforms(const Matrix4* pTransforms, UInt32 uTransforms)
	{
		ASSERT_IF_FAILED(pTransforms != nullptr && uTransforms > 0);
		GRenderer::WaitUntilRendered();

		m_uTransforms = Math::Min(uTransforms, m_uMaxTransforms);
		MapRegion region;
		GRenderer::LockThread();
		if (GRenderer::CommandList()->Map(m_pBuffer, False, region))
		{
			Array<Matrix4> arrOrigin;
			for (UInt32 u = 0; u < m_uTransforms; u++)
			{
				arrOrigin.Add(pTransforms[u]);

				Matrix4& refMat = arrOrigin[u];
				Float3 scale = Math::MatrixGetScale(pTransforms[u]);
				refMat.Set(0, 3, 1.0f / scale[0]);
				refMat.Set(1, 3, 1.0f / scale[1]);
				refMat.Set(2, 3, 1.0f / scale[2]);
			}
			UInt32 uSize = m_uTransforms * sizeof(Matrix4);

			memcpy(region.pAddress, arrOrigin.GetRawData(), uSize);

			GRenderer::CommandList()->Unmap(m_pBuffer);
		}
		GRenderer::UnlockThread();
	}
	Void	GPrimitiveTransformBuffer::BindGResource(IGCommandList* pCommandList)
	{
		if (m_bForInstance)
		{
			pCommandList->BindVertexBuffer(1, m_pBuffer, sizeof(Matrix4));
		}
		else
		{
			pCommandList->BindConstantBuffer(ShaderType::EVertexShader, 1, m_pBuffer);
		}
	}
}