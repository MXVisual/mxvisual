#pragma once
#ifndef _R_BOUNDING_VOLUME_
#define _R_BOUNDING_VOLUME_
#include "MXGraphicsResource.h"

namespace MXVisual
{
	class GVolumnAABB : public IGBoundingVolume
	{
	public:
		GVolumnAABB();
		~GVolumnAABB();
		Void	Merge(const IGBoundingVolume* pOther) override;
		Boolean CalIntersect(const Float3& f3Begin, const Float3& f3Direction, Float1 fDistance, const Matrix4* pTransform = nullptr, Array<Float3>* pOutIntersectPoint = nullptr) const override;
		Void	Reset() override;
		Void	AddVertexPosition(const Float3& f3Point) override;

		UInt32  GetVolumePointCount() const override { return 8; }
		Float3  GetVolumePoint(UInt32 uIndex) const override;

	protected:
		Float3 m_f3Max;
		Float3 m_f3Min;
	};
}

#endif