#pragma once
#ifndef _G_VIEW_FRUSTUM_
#define _G_VIEW_FRUSTUM_
#include "GCommonRenderer.h"
#include "MXGraphicsResource.h"
#include "MXMatrix.h"

namespace MXVisual
{
	struct Plane
	{
		Float3 Normal;
		Float3 Origin;
	};

	class GRenderWindow;
	class GViewFrustum : public IGViewFrustum
	{
	public:
		UInt32		GetUniqueID() const override { return m_uUniqueID; }

		GViewFrustum(UInt32 UniqueID);
		~GViewFrustum();

		Boolean Init(const IGViewFrustum::Description& desc);

		Void SetPosition(const Float3& f3Position) override;
		Void SetForwardAndUp(const Float3& f3Forward, const Float3& f3Up) override;

		Void SetPerspective(Float1 fFovW, Float1 fAspectWH, Float1 fZNear, Float1 fZFar) override;
		Void SetOrthographic(Float1 fWidth, Float1 fHeight, Float1 fZNear, Float1 fZFar) override;

		Boolean IsBoundingInside(IGBoundingVolume* pVolume) const;

		const Float3&		GetPosition() const { return m_f3Position; }

		Matrix4		GetWorldToView() const override { return m_matWorldToView; }
		Matrix4		GetViewToClip() const override { return m_matViewToClip; }
		Float2		GetViewportSize() const override;

		Float1 GetZFar() const { return m_fZFar; }
		Float1 GetZNear() const { return m_fZNear; }

		const GViewport		GetViewport() const { return m_Viewport; }
		const IGTexture*	GetColorTarget() const;
		const IGTexture*	GetDepthTarget() const;
		Void				GetCornerPositions(Float3 nearCorners[4], Float3 farCorners[4]) const override;

	protected:
		UInt32		m_uUniqueID;

		Float3		m_f3Position;
		Float3		m_f3Right;
		Float3		m_f3Up;
		Float3		m_f3Forward;
		Float1		m_fZFar;
		Float1		m_fZNear;

		GViewport	m_Viewport;
		Matrix4		m_matWorldToView;
		Matrix4		m_matViewToClip;

		GRenderWindow*			m_pRenderWindow;
		//Or
		IGTexture*				m_pColorTarget;
		IGTexture*				m_pDepthTarget;

		enum PlaneEnum
		{
			EPlaneTop = 0,
			EPlaneBottom = 1,
			EPlaneLeft = 2,
			EPlaneRight = 3,
			EPlaneNear = 4,
			EPlaneFar = 5,
			EPlaneCount = 6
		};
		Plane	m_arrFrustumPlane[EPlaneCount];
		enum class CornerType : UInt8
		{
			ENearLeftTop,
			ENearRightTop,
			ENearLeftBottom,
			ENearRightBottom,
			EFarLeftTop,
			EFarRightTop,
			EFarLeftBottom,
			EFarRightBottom,
			ECount
		};
		Float4	m_arrFrustumCornersInView[(const UInt8)CornerType::ECount];
	};
}

#endif