#include "GViewFrustum.h"
#include "MXMath.h"
#include "math.h"
#include "GRenderer.h"
#include "GRenderWindow.h"

namespace MXVisual
{
	GViewFrustum::GViewFrustum(UInt32 UniqueID)
		: m_uUniqueID(UniqueID)
		, m_pColorTarget(nullptr)
		, m_pDepthTarget(nullptr)
		, m_fZFar(0.0f)
		, m_fZNear(0.0f)
	{
		m_nRefCount = 1;
	}
	GViewFrustum::~GViewFrustum()
	{
		MX_RELEASE_INTERFACE(m_pColorTarget);
		MX_RELEASE_INTERFACE(m_pDepthTarget);
	}

	Boolean GViewFrustum::Init(const IGViewFrustum::Description& desc)
	{
		if (desc.pTargetWindow != nullptr)
		{
			m_pRenderWindow = dynamic_cast<GRenderWindow*>(desc.pTargetWindow);;
			if (m_pRenderWindow != nullptr)
			{
				m_Viewport.uWidth = m_pRenderWindow->GetWidth();
				m_Viewport.uHeight = m_pRenderWindow->GetHeight();
				m_Viewport.uX = 0;
				m_Viewport.uY = 0;
				m_Viewport.fMinDepth = 0.0f;
				m_Viewport.fMaxDepth = 1.0f;

				m_pDepthTarget = nullptr;
			}
			else
			{
				Log::Output("GraphicsRenderer", "ViewFrustum Init Failed");
				return False;
			}
		}
		else
		{
			//Create RenderTarget
			//FIXME!!
		}
		return True;
	}

	Void GViewFrustum::SetPosition(const Float3& f3Position)
	{
		m_f3Position = f3Position;

		Matrix4 T(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			-m_f3Position[0], -m_f3Position[1], -m_f3Position[2], 1
		);

		m_matWorldToView = T * Matrix4(
			m_f3Right[0], m_f3Up[0], m_f3Forward[0], 0,
			m_f3Right[1], m_f3Up[1], m_f3Forward[1], 0,
			m_f3Right[2], m_f3Up[2], m_f3Forward[2], 0,
			0, 0, 0, 1
		);
	}
	Void GViewFrustum::SetForwardAndUp(const Float3& f3Forward, const Float3& f3Up)
	{
		m_f3Up = Math::Normalize(f3Up);
		m_f3Forward = Math::Normalize(f3Forward);
		m_f3Right = Math::CrossProduct(m_f3Up, m_f3Forward);

		Matrix4 T(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			-m_f3Position[0], -m_f3Position[1], -m_f3Position[2], 1
		);

		m_matWorldToView = T * Matrix4(
			m_f3Right[0], m_f3Up[0], m_f3Forward[0], 0,
			m_f3Right[1], m_f3Up[1], m_f3Forward[1], 0,
			m_f3Right[2], m_f3Up[2], m_f3Forward[2], 0,
			0, 0, 0, 1
		);
	}

	Void GViewFrustum::SetPerspective(Float1 fFovW, Float1 fAspectWH, Float1 fZNear, Float1 fZFar)
	{
		m_fZFar = fZFar;
		m_fZNear = fZNear;
		m_matViewToClip = GenPerspectiveMatrix(fFovW, fAspectWH, fZNear, fZFar);

		Float1 tw = tanf(fFovW * 0.5f);
		Float1 th = tw / fAspectWH;

		Float1 fWidth = tw * fZNear;
		Float1 fHeight = th * fZNear;

		m_arrFrustumCornersInView[(const UInt8)CornerType::ENearLeftTop] =
			Float4(-fWidth, fHeight, fZNear, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::ENearRightTop] =
			Float4(fWidth, fHeight, fZNear, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::ENearLeftBottom] =
			Float4(-fWidth, -fHeight, fZNear, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::ENearRightBottom] =
			Float4(fWidth, -fHeight, fZNear, 1.0f);

		fWidth = tw * fZFar;
		fHeight = th * fZFar;

		m_arrFrustumCornersInView[(const UInt8)CornerType::EFarLeftTop] =
			Float4(-fWidth, fHeight, fZFar, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::EFarRightTop] =
			Float4(fWidth, fHeight, fZFar, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::EFarLeftBottom] =
			Float4(-fWidth, -fHeight, fZFar, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::EFarRightBottom] =
			Float4(fWidth, -fHeight, fZFar, 1.0f);

		m_arrFrustumPlane[EPlaneTop].Origin = Float3(0, 0, 0);
		m_arrFrustumPlane[EPlaneBottom].Origin = Float3(0, 0, 0);
		m_arrFrustumPlane[EPlaneLeft].Origin = Float3(0, 0, 0);
		m_arrFrustumPlane[EPlaneRight].Origin = Float3(0, 0, 0);
		m_arrFrustumPlane[EPlaneNear].Origin = Float3(0, 0, fZNear);
		m_arrFrustumPlane[EPlaneFar].Origin = Float3(0, 0, fZFar);

		Float1 c = cosf(fFovW * 0.5f);
		Float1 s = sinf(fFovW * 0.5f);

		m_arrFrustumPlane[EPlaneLeft].Normal = Float3(-c, 0, -s);
		m_arrFrustumPlane[EPlaneRight].Normal = Float3(c, 0, -s);

		s = s / fAspectWH;
		c = sqrt(1.0f - s * s);

		m_arrFrustumPlane[EPlaneTop].Normal = Float3(0, c, -s);
		m_arrFrustumPlane[EPlaneBottom].Normal = Float3(0, -c, -s);

		m_arrFrustumPlane[EPlaneNear].Normal = Float3(0, 0, -1);
		m_arrFrustumPlane[EPlaneFar].Normal = Float3(0, 0, 1);
	}
	Void GViewFrustum::SetOrthographic(Float1 fWidth, Float1 fHeight, Float1 fZNear, Float1 fZFar)
	{
		m_fZFar = fZFar;
		m_fZNear = fZNear;
		m_matViewToClip = GenOrthographicMatrix(fWidth, fHeight, fZNear, fZFar);

		m_arrFrustumCornersInView[(const UInt8)CornerType::ENearLeftTop] =
			Float4(-fWidth * 0.5f, fHeight * 0.5f, fZNear, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::ENearRightTop] =
			Float4(fWidth * 0.5f, fHeight * 0.5f, fZNear, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::ENearLeftBottom] =
			Float4(-fWidth * 0.5f, -fHeight * 0.5f, fZNear, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::ENearRightBottom] =
			Float4(fWidth * 0.5f, -fHeight * 0.5f, fZNear, 1.0f);

		m_arrFrustumCornersInView[(const UInt8)CornerType::EFarLeftTop] =
			Float4(-fWidth * 0.5f, fHeight * 0.5f, fZFar, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::EFarRightTop] =
			Float4(fWidth * 0.5f, fHeight * 0.5f, fZFar, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::EFarLeftBottom] =
			Float4(-fWidth * 0.5f, -fHeight * 0.5f, fZFar, 1.0f);
		m_arrFrustumCornersInView[(const UInt8)CornerType::EFarRightBottom] =
			Float4(fWidth * 0.5f, -fHeight * 0.5f, fZFar, 1.0f);

		m_arrFrustumPlane[EPlaneTop].Origin = Float3(0, fHeight * 0.5f, 0);
		m_arrFrustumPlane[EPlaneBottom].Origin = Float3(0, -fHeight * 0.5f, 0);
		m_arrFrustumPlane[EPlaneLeft].Origin = Float3(-fWidth * 0.5f, 0, 0);
		m_arrFrustumPlane[EPlaneRight].Origin = Float3(fWidth * 0.5f, 0, 0);
		m_arrFrustumPlane[EPlaneNear].Origin = Float3(0, 0, fZNear);
		m_arrFrustumPlane[EPlaneFar].Origin = Float3(0, 0, fZFar);

		m_arrFrustumPlane[EPlaneLeft].Normal = Float3(-1, 0, 0);
		m_arrFrustumPlane[EPlaneRight].Normal = Float3(1, 0, 0);
		m_arrFrustumPlane[EPlaneTop].Normal = Float3(0, 1, 0);
		m_arrFrustumPlane[EPlaneBottom].Normal = Float3(0, -1, 0);
		m_arrFrustumPlane[EPlaneNear].Normal = Float3(0, 0, -1);
		m_arrFrustumPlane[EPlaneFar].Normal = Float3(0, 0, 1);
	}

	Boolean GViewFrustum::IsBoundingInside(IGBoundingVolume* pVolume) const
	{
		static Array<Float3> PointInViewSpace;
		for (UInt32 u = 0; u < pVolume->GetVolumePointCount(); u++)
		{
			Float4 p = Math::MatrixTransform(Math::Combine(pVolume->GetVolumePoint(u), 1.0f), m_matWorldToView);
			PointInViewSpace.Add(Math::Split(p));
		}

		for (UInt32 uPIndex = 0; uPIndex < PointInViewSpace.ElementCount(); uPIndex++)
		{
			for (UInt32 u = 0; u < EPlaneCount; u++)
			{
				Float3 Dir = PointInViewSpace[uPIndex] - m_arrFrustumPlane[u].Origin;
				float Res = Math::DotProduct(Dir, m_arrFrustumPlane[u].Normal);
				if (Res > 0)
				{
					PointInViewSpace.Clear();
					return False;
				}
			}
		}

		PointInViewSpace.Clear();
		return True;
	}
	Void	GViewFrustum::GetCornerPositions(Float3 nearCorners[4], Float3 farCorners[4]) const
	{
		Matrix4 ViewToWorld = Math::MatrixInverse(m_matWorldToView);

		nearCorners[0] = Math::Split(Math::MatrixTransform(m_arrFrustumCornersInView[(const UInt8)CornerType::ENearLeftTop], ViewToWorld));
		nearCorners[1] = Math::Split(Math::MatrixTransform(m_arrFrustumCornersInView[(const UInt8)CornerType::ENearRightTop], ViewToWorld));
		nearCorners[2] = Math::Split(Math::MatrixTransform(m_arrFrustumCornersInView[(const UInt8)CornerType::ENearLeftBottom], ViewToWorld));
		nearCorners[3] = Math::Split(Math::MatrixTransform(m_arrFrustumCornersInView[(const UInt8)CornerType::ENearRightBottom], ViewToWorld));

		farCorners[0] = Math::Split(Math::MatrixTransform(m_arrFrustumCornersInView[(const UInt8)CornerType::EFarLeftTop], ViewToWorld));
		farCorners[1] = Math::Split(Math::MatrixTransform(m_arrFrustumCornersInView[(const UInt8)CornerType::EFarRightTop], ViewToWorld));
		farCorners[2] = Math::Split(Math::MatrixTransform(m_arrFrustumCornersInView[(const UInt8)CornerType::EFarLeftBottom], ViewToWorld));
		farCorners[3] = Math::Split(Math::MatrixTransform(m_arrFrustumCornersInView[(const UInt8)CornerType::EFarRightBottom], ViewToWorld));
	}

	const IGTexture* GViewFrustum::GetColorTarget() const
	{
		return m_pRenderWindow == nullptr ? m_pColorTarget : m_pRenderWindow->GetRenderTarget();
	}
	const IGTexture* GViewFrustum::GetDepthTarget() const
	{
		return m_pRenderWindow == nullptr ? m_pDepthTarget : nullptr;
	}

	Float2	GViewFrustum::GetViewportSize() const
	{
		if (m_pRenderWindow != nullptr)
			return Float2(m_pRenderWindow->GetWidth(), m_pRenderWindow->GetHeight());

		return Float2(m_Viewport.uWidth, m_Viewport.uHeight);
	}
}