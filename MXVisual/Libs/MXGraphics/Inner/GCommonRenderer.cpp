#include "GCommonRenderer.h"
#include "MXFile.h"
#include "MXThread.h"
#include "MXTimer.h"

#include "GViewFrustum.h"
#include "GPrimitive.h"
#include "GCommonMaterial.h"
#include "GBoundingVolume.h"

#include "GSSAOPass.h"
#include "GIndirectLightPass.h"
#include "GBlurPass.h"
#include "GBrightnessFilterPass.h"
#include "GBloomPass.h"
#include "GRadialBlurGodRayPass.h"
#include "GHeightFogPass.h"
#include "GTonemapPass.h"
#include "GGammaPass.h"

namespace MXVisual
{
#pragma region GVarImp
#if MX_BUILD_EDITOR
	GlobalVariable GVarShadingDebugView("ShadingDebugView", GlobalVariable::Property::eReadWrite, False);
#endif
	GlobalVariable GVarEnableVSync("Render.VSync.Enable", GlobalVariable::Property::eReadWrite, (Boolean)False);
	GlobalVariable GVarEnableShadow("Shadow.Enable", GlobalVariable::Property::eReadWrite, (Boolean)False);
	GlobalVariable GVarCascadeShadowDebug("Shadow.CascadeDebug", GlobalVariable::Property::eReadWrite, (Boolean)False);
	GlobalVariable GVarCascadeShadowDistance("Shadow.CascadeDistance", GlobalVariable::Property::eReadWrite, (Float1)0.1f);
	GlobalVariable GVarShadowDepthOffset("Shadow.DepthOffset", GlobalVariable::Property::eReadWrite, (Float1)0.000012f);
	GlobalVariable GVarShadowMapResolution("ShadowMapResolution", GlobalVariable::Property::eReadOnly, (SInt32)2048);
	GlobalVariable GVarShadowCascadeNum("Shadow.CascadeNum", GlobalVariable::Property::eReadOnly, (SInt32)3);

	GlobalVariable GVarMaxAnisotropy("MaxAnisotropy", GlobalVariable::Property::eReadWrite, (SInt32)16);

	GlobalVariable GVarAmbientColor("Atmospheric.AmbientColor", GlobalVariable::Property::eReadWrite, Float3(0.2f, 0.2f, 0.25f));
	GlobalVariable GVarAmbientScale("Atmospheric.AmbientScale", GlobalVariable::Property::eReadWrite, (Float1)1.0f);

	GlobalVariable GVarEnableSSR("PostProcess.SSR", GlobalVariable::Property::eReadWrite, (Boolean)True);
	GlobalVariable GVarEnableSSAO("PostProcess.SSAO", GlobalVariable::Property::eReadWrite, (Boolean)True);
	GlobalVariable GVarSSAORange("PostProcess.SSAO.Range", GlobalVariable::Property::eReadWrite, 1.0f);

	GlobalVariable GVarEnableBloom("PostProcess.Bloom", GlobalVariable::Property::eReadWrite, (Boolean)True);
	GlobalVariable GVarBloomExporedLuminance("PostProcess.Bloom.ExporedLuminance", GlobalVariable::Property::eReadWrite, (Float1)1.5f);

	GlobalVariable GVarEnableRBGodRay("PostProcess.RadialBlurGodRay", GlobalVariable::Property::eReadWrite, (Boolean)True);
	GlobalVariable GVarRBGodRaySampleCount("PostProcess.RadialBlurGodRay.SampleCount", GlobalVariable::Property::eReadWrite, (SInt32)16);
	GlobalVariable GVarRBGodRayDepthBegin("PostProcess.RadialBlurGodRay.DepthBegin", GlobalVariable::Property::eReadWrite, (Float1)1500.0f);
	GlobalVariable GVarRBGodRayAttenuation("PostProcess.RadialBlurGodRay.Attenuation", GlobalVariable::Property::eReadWrite, (Float1)1.0f);
	GlobalVariable GVarRBGodRayStrength("PostProcess.RadialBlurGodRay.Strength", GlobalVariable::Property::eReadWrite, (Float1)2.2f);
	GlobalVariable GVarRBGodRayRange("PostProcess.RadialBlurGodRay.Range", GlobalVariable::Property::eReadWrite, (Float1)512.0f);

	GlobalVariable GVarEnableToneMapping("PostProcess.ToneMap", GlobalVariable::Property::eReadWrite, (Boolean)True);

	GlobalVariable GVarMaxGPUBlendMatrixPerPass("MaxGPUSkin", GlobalVariable::Property::eReadOnly, (SInt32)256);

	GlobalVariable GVarHeightFogEnable("HeightFog.Enable", GlobalVariable::Property::eReadWrite, (Boolean)False);
	GlobalVariable GVarHeightFogColor("HeightFog.Color", GlobalVariable::Property::eReadWrite, Float3(0.8f, 0.8f, 0.9f));
	GlobalVariable GVarHeightFogDensity("HeightFog.Density", GlobalVariable::Property::eReadWrite, (Float1)1.0f);
	GlobalVariable GVarHeightFogStartDistance("HeightFog.StartDistance", GlobalVariable::Property::eReadWrite, (Float1)16.0f);
	GlobalVariable GVarHeightFogDistanceFalloff("HeightFog.DistanceFalloff", GlobalVariable::Property::eReadWrite, (Float1)1.0f);
	GlobalVariable GVarHeightFogHeight("HeightFog.Height", GlobalVariable::Property::eReadWrite, (Float1)8.0f);
	GlobalVariable GVarHeightFogHeightFalloff("HeightFog.HeightFalloff", GlobalVariable::Property::eReadWrite, (Float1)0.5f);

	GlobalVariable GVarEnableTemporalAA("TemporalAA.Enable", GlobalVariable::Property::eReadWrite, (Boolean)False);
#pragma endregion

	Mutex				ThreadMutex;
	Atomic<SInt16>		GIsRendering(0);
	Boolean				GHasRawShaderFolder = False;
	Void		GRenderer::WaitUntilRendered()
	{
		while (GIsRendering.Value() != 0);
	}
	Void		GRenderer::LockThread() { ThreadMutex.Lock(); }
	Void		GRenderer::UnlockThread() { ThreadMutex.UnLock(); }


	Matrix4 GenPerspectiveMatrix(Float1 fFovW, Float1 fAspectWH, Float1 fZNear, Float1 fZFar)
	{
		Float1 cotW = 1.0f / (Float1)tan(fFovW / 2);
#if REVERT_Z
		Float1 fNmF = fZNear - fZFar;
		return Matrix4(
			cotW, 0, 0, 0,
			0, cotW * fAspectWH, 0, 0,
			0, 0, fZNear / fNmF, 1,
			0, 0, -fZFar * fZNear / fNmF, 0
		);
#else
		Float1 fFmN = fZFar - fZNear;
		return Matrix4(
			cotW, 0, 0, 0,
			0, cotW * fAspectWH, 0, 0,
			0, 0, fZFar / fFmN, 1,
			0, 0, -fZFar * fZNear / fFmN, 0
		);
#endif
	}
	Matrix4 GenOrthographicMatrix(Float1 fWidth, Float1 fHeight, Float1 fZNear, Float1 fZFar)
	{
#if REVERT_Z
		return Matrix4(
			2.0f / fWidth, 0, 0, 0,
			0, 2.0f / fHeight, 0, 0,
			0, 0, 1 / (fZNear - fZFar), 0,
			0, 0, -fZFar / (fZNear - fZFar), 1
		);
#else
		return Matrix4(
			2.0f / fWidth, 0, 0, 0,
			0, 2.0f / fHeight, 0, 0,
			0, 0, 1 / (fZFar - fZNear), 0,
			0, 0, -fZNear / (fZFar - fZNear), 1
		);
#endif
	}
	Matrix4 GenOrthographicMatrix(Float1 fLeft, Float1 fTop, Float1 fRight, Float1 fBottom, Float1 fZNear, Float1 fZFar)
	{
		Float1 divX = 2.0f / (fRight - fLeft);
		Float1 divY = 2.0f / (fTop - fBottom);

#if REVERT_Z
		return Matrix4(
			divX, 0, 0, 0,
			0, divY, 0, 0,
			0, 0, 1.0f / (fZNear - fZFar), 0,
			1.0f - fRight * divX, 1.0f - fTop * divY, -fZFar / (fZNear - fZFar), 1
		);
#else
		return Matrix4(
			divX, 0, 0, 0,
			0, divY, 0, 0,
			0, 0, 1.0f / (fZFar - fZNear), 0,
			1.0f - fRight * divX, 1.0f - fTop * divY, -fZNear / (fZFar - fZNear), 1
		);
#endif
	}

	Void	IGResource::Release()
	{
		m_nRefCount--;
		if (m_nRefCount <= 0)
		{
			Boolean bFound = False;
			for (UInt32 u = 0; u != GRendererResource::Resources.ElementCount(); u++)
			{
				if (GRendererResource::Resources[u] == this)
				{
					GRendererResource::Resources.EraseOutOfOrder(u, 1);
					bFound = True;
					break;
				}
			}
			ASSERT_IF_FAILED(bFound);
			delete this;
		}
	}

	namespace GRendererResource
	{
		Boolean		HasRawShaderFolder() { return GHasRawShaderFolder; }

		Void		SingleScattering()
		{

		}

		Boolean		GenerateAtmosphericScatterLUT()
		{
			IGTexture::Description desc;
			desc.bDynamic = False;
			desc.bGenerateMipMap = False;
			desc.bTargetable = False;
			desc.eFormat = EPF_R8G8B8A8;
			desc.eType = TextureType::ETexture2D;
			AtmosphericScatterLUT = GraphicsResource::CreateTexture(desc);

			//光学深度

			return True;
		}

		Float2						BackbufferSize;
		GStatistics					FrameStatistics;
		IGTexture*					ColorTargets[(const UInt32)ColorTargetType::ECount];
		IGTexture*					DepthTargets[(const UInt32)DepthTargetType::ECount];
		IGTexture*					AtmosphericScatterLUT;

		IGBuffer*					BufferCommon;
		IGBuffer*					BufferForwardLight;

		GPipeline*					PSOScreenSpaceShadow = nullptr;
		GPipeline*					PSODeferredLighting = nullptr;

		IGPrimitiveTransformBuffer*	IdentityTransformBuffer = nullptr;
		IGSampler*					EngineCommonSampler = nullptr;
		GCommonShaderManager*		ShaderManager = nullptr;
		ShaderCompiledResult		DefaultScreenQuadVertexShader;

		UInt32						DirectLightCount = 0;
		Array<DirectLightInfo>		DirectLights;

		UInt32						CastingShadowLightCount = 0;
		Array<DirectLightInfo>		CastingShadowLights;

		Array<IGPrimitive*>			RegisteredPrimitives;
		Array<DrawInstance>			DrawInstanceList[(const UInt8)DrawInstanceType::ECount];
		Array<IGReflectionProbe*>	ReflectionProbeList;
	}

	Boolean InitOutputTargets(UInt32 uWidth, UInt32 uHeight)
	{
		PixelFormat ColorTargetFmt[(const UInt32)ColorTargetType::ECount] = {
			EPF_R16G16B16A16_F,
			EPF_R16G16B16A16_F,
			EPF_R16G16B16A16_F,
			EPF_R8G8B8A8,
			EPF_R8G8B8A8,
			EPF_R8G8B8A8,
			EPF_R16G16B16A16,
			EPF_R8G8B8A8
		};
		Float4 ColorTargetCC[(const UInt32)ColorTargetType::ECount] = {
			Float4(),
			Float4(),
			Float4(),
			Float4(0.0f, 0.0f, 0.0f, 1.0f),
			Float4(1.0f, 0.0f, 1.0f, 0.0f),
			Float4(0.0f, 1.0f,0.0f, 0.0f),
			Float4(),
			Float4(0.0f, 0.0f, 1.0f, 0.0f),
		};

		IGTexture::Description desc;
		desc.bDynamic = False;
		desc.uDepth = 1;
		desc.uMipLevel = 1;
		desc.uWidth = uWidth;
		desc.uHeight = uHeight;
		desc.bTargetable = True;
		desc.bUnorderAccessable = True;
		desc.eType = TextureType::ETexture2D;

		for (UInt32 u = 0; u < (const UInt32)ColorTargetType::ECount; u++)
		{
			if (u == (const UInt32)ColorTargetType::ESceneColor0 || u == (const UInt32)ColorTargetType::ESceneColor1)
			{
				desc.uMipLevel = -1;
			}
			else
			{
				desc.uMipLevel = 1;
			}

			desc.f4ClearValue = ColorTargetCC[u];
			desc.eFormat = ColorTargetFmt[u];

			GRendererResource::ColorTargets[u] = GraphicsResource::CreateTexture(desc);
			if (GRendererResource::ColorTargets[u] == nullptr)
				return False;
		}

		desc.bUnorderAccessable = False;
		PixelFormat DepthTargetFmt[(const UInt32)DepthTargetType::ECount] = {
			EPF_D32,
			EPF_D32
		};
		desc.uMipLevel = 1;
		for (UInt32 u = 0; u < (const UInt32)DepthTargetType::ECount; u++)
		{
			desc.eFormat = DepthTargetFmt[u];

			GRendererResource::DepthTargets[u] = GraphicsResource::CreateTexture(desc);
			if (GRendererResource::DepthTargets[u] == nullptr)
				return False;
		}
		return True;
	}
	Boolean	InitCommonResource()
	{
		GRendererResource::IdentityTransformBuffer = GraphicsResource::CreatePrimitiveTransformBuffer(False);
		GRendererResource::IdentityTransformBuffer->SetTransforms(&Matrix4::Identity, 1);

		GRendererResource::BufferCommon = GRendererResource::CreateBuffer(
			BufferType::EConstantBuffer, True, sizeof(CBSCommon));

		GRendererResource::BufferForwardLight = GRendererResource::CreateBuffer(
			BufferType::EConstantBuffer, True, sizeof(CBSForwardLight));

		GRendererResource::EngineCommonSampler = GRendererResource::GetSampler(
			ETexAM_Clamp, ETexFM_Bilinear, GVarMaxAnisotropy.nValue);


		//ScreenQuad VS
		String strShaderName = String::Format("%s_%d.msr", "MXVVertexShaderSimpleMesh", 0);
		ShaderMacro macro;
		if (!GRendererResource::ShaderManager->CompileShader(
			strShaderName.CString(), macro, GRendererResource::DefaultScreenQuadVertexShader,
			nullptr, 0, ShaderType::EVertexShader, False))
		{
			const String PixelShaderName = String::Format("%sMXVVertexShaderSimpleMesh.msr", ENGINE_SHADER_DIR);
			IFile * pShaderFile = IFile::Open(PixelShaderName.CString(), IFile::EFDT_Text);
			if (!pShaderFile)return False;

			macro.Clear();
			String strMeshType = String::Format("%d", 0);
			macro.AddMacro("SIMPLE_MESH_TYPE", strMeshType.CString());

			Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
			pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

			String strCode = pBuf;

			delete[] pBuf;
			pShaderFile->Close();

			GRendererResource::ShaderManager->CompileShader(
				strShaderName.CString(), macro, GRendererResource::DefaultScreenQuadVertexShader,
				strCode.CString(), strCode.Length(), ShaderType::EVertexShader, False);
		}
		if (!GRendererResource::DefaultScreenQuadVertexShader.IsValid())	return False;

		macro.Clear();
		ShaderCompiledResult PS;
		if (!GRendererResource::ShaderManager->CompileShader(
			"MXVPassDirectLighting.msr",
			macro, PS, nullptr, 0, ShaderType::EPixelShader, False))
		{
			const String LightPixelShaderName = String::Format("%sMXVPassDirectLighting.msr", ENGINE_SHADER_DIR);

			IFile * pShaderFile = IFile::Open(LightPixelShaderName.CString(), IFile::EFDT_Text);
			if (!pShaderFile)return False;

			String strDomain = String::Format("%d", MaterialDomain::EPostProcess);
			macro.AddMacro("DOMAIN", strDomain.CString());

			Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
			pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

			String strCode = pBuf;

			delete[] pBuf;
			pShaderFile->Close();

			GRendererResource::ShaderManager->CompileShader(
				"MXVPassDirectLighting.msr",
				macro, PS, strCode.CString(), strCode.Length(), ShaderType::EPixelShader, False);
		}

		GRenderPass RP;
		RP.RTVCount = 1;
		RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]->GetFormat();
		GRendererResource::PSODeferredLighting = new GPipeline;

		if (!GRendererResource::PSODeferredLighting->InitForRender(
			PrimitiveIAFactory::EStatic2D,
			TopologyType::ETriangle, RP,
			GRendererResource::RasterizerState(),
			GRendererResource::DepthStencilState(DEPTH_ALWAYS, False),
			GRendererResource::BlendState(BLENDSTATE_ADDITIVE, WriteMask::EWM_RGBChannel),
			&GRendererResource::DefaultScreenQuadVertexShader, &PS))
		{
			return False;
		}

		macro.Clear();
		PS.pCode = nullptr;
		PS.uSize = 0;
		if (!GRendererResource::ShaderManager->CompileShader(
			"MXVPassScreenSpaceShadow.msr",
			macro, PS, nullptr, 0, ShaderType::EPixelShader, False))
		{
			const String LightPixelShaderName = String::Format("%sMXVPassScreenSpaceShadow.msr", ENGINE_SHADER_DIR);

			IFile * pShaderFile = IFile::Open(LightPixelShaderName.CString(), IFile::EFDT_Text);
			if (!pShaderFile)return False;

			String strDomain = String::Format("%d", MaterialDomain::EPostProcess);
			macro.AddMacro("DOMAIN", strDomain.CString());
			String strZMode = String::Format("%d", REVERT_Z);
			macro.AddMacro("REVERT_Z", strZMode.CString());
			String strFaceCount = String::Format("%d", MAX_FACE_IN_ONE_MAP);
			macro.AddMacro("MAX_FACE_IN_ONE_MAP", strFaceCount.CString());

			Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
			pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

			String strCode = pBuf;

			delete[] pBuf;
			pShaderFile->Close();

			GRendererResource::ShaderManager->CompileShader(
				"MXVPassScreenSpaceShadow.msr",
				macro, PS, strCode.CString(), strCode.Length(), ShaderType::EPixelShader, False);
		}

		RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EScreenSpaceShadow]->GetFormat();
		GRendererResource::PSOScreenSpaceShadow = new GPipeline;
		if (!GRendererResource::PSOScreenSpaceShadow->InitForRender(
			PrimitiveIAFactory::EStatic2D,
			TopologyType::ETriangle, RP,
			GRendererResource::RasterizerState(),
			GRendererResource::DepthStencilState(DEPTH_ALWAYS, False),
			GRendererResource::BlendState(BLENDSTATE_BLEND, WriteMask::EWM_BlueChannel),
			&GRendererResource::DefaultScreenQuadVertexShader, &PS))
		{
			return False;
		}

		return True;
	}
	Void	ReleaseOutputTargets()
	{
		for (UInt32 u = 0; u < (const UInt32)ColorTargetType::ECount; u++)
		{
			MX_RELEASE_INTERFACE(GRendererResource::ColorTargets[u]);
		}

		for (UInt32 u = 0; u < (const UInt32)DepthTargetType::ECount; u++)
		{
			MX_RELEASE_INTERFACE(GRendererResource::DepthTargets[u]);
		}
	}
	Void	ReleaseCommonResource()
	{
		if (GRendererResource::PSODeferredLighting != nullptr)
		{
			GRendererResource::PSODeferredLighting->Release();
			delete GRendererResource::PSODeferredLighting;
		}
		if (GRendererResource::PSOScreenSpaceShadow != nullptr)
		{
			GRendererResource::PSOScreenSpaceShadow->Release();
			delete GRendererResource::PSOScreenSpaceShadow;
		}

		MX_RELEASE_INTERFACE(GRendererResource::IdentityTransformBuffer);

		MX_RELEASE_INTERFACE(GRendererResource::BufferCommon);
		MX_RELEASE_INTERFACE(GRendererResource::BufferForwardLight);

		MX_RELEASE_INTERFACE(GRendererResource::EngineCommonSampler);
	}

	Boolean GraphicsRenderer::Init(InitParam& param)
	{
		GRendererResource::FrameStatistics.uGPUMemoryOccupy = 0;
		GRendererResource::FrameStatistics.uMemoryOccupy = 0;
		GRendererResource::FrameStatistics.uPrimitiveCount = 0;
		GRendererResource::FrameStatistics.uDrawCall = 0;
		GRendererResource::BackbufferSize = Float2(param.uBackBufferWidth, param.uBackBufferHeight);

		GHasRawShaderFolder = CheckFile("./Shaders");
		if (!GRenderer::InitAPI(param.szAdapterName) ||
			!GRendererResource::InitAPIDependencyResource() ||
			!InitOutputTargets(param.uBackBufferWidth, param.uBackBufferHeight) ||
			!InitCommonResource())
		{
			GraphicsRenderer::Release();
			return False;
		}

		if (!GSSAOPass::Init() ||
			!GIndirectLightPass::Init() ||
			!GTonemapPass::Init() ||
			!GBlurPass::Init() ||
			!GBrightnessFilterPass::Init() ||
			!GBloomPass::Init() ||
			!GRadialBlurGodRayPass::Init(param.uBackBufferWidth, param.uBackBufferHeight) ||
			!GHeightFogPass::Init() ||
			!GGammaPass::Init())
		{
			GraphicsRenderer::Release();
			return False;
		}

		return True;
	}

	Void	GraphicsRenderer::Release()
	{
		GSSAOPass::Release();
		GIndirectLightPass::Init();
		GTonemapPass::Release();
		GBlurPass::Release();
		GBrightnessFilterPass::Release();
		GBloomPass::Release();
		GRadialBlurGodRayPass::Release();
		GHeightFogPass::Release();
		GGammaPass::Release();

		ReleaseCommonResource();
		ReleaseOutputTargets();
		GRendererResource::ReleaseAPIDependencyResource();
		GRenderer::ReleaseAPI();
	}

	Void	GraphicsRenderer::RegisterPostProcess(
		IGMaterial* pMaterial,
		IGMaterialParameterBuffer* pParameterBlock,
		GRendererPostProcessInsertionPoint eIPoint)
	{
		if (eIPoint < EPPIP_Count &&
			pMaterial->GetDomainType() == MaterialDomain::EPostProcess)
		{
			//PostProcessInstance[eIPoint].Add(pMaterialIns);
		}
	}

	Void	GraphicsRenderer::RegisterLight(IGLight* pLight)
	{
		if (pLight == nullptr)return;

		DirectLightInfo* pInfo = nullptr;
		if (pLight->IsCastShadow())
		{
			GRendererResource::CastingShadowLightCount++;
			if (GRendererResource::CastingShadowLightCount >= GRendererResource::CastingShadowLights.ElementCount())
			{
				GRendererResource::CastingShadowLights.Add(DirectLightInfo());
			}

			pInfo = &GRendererResource::CastingShadowLights[GRendererResource::CastingShadowLightCount - 1];
		}
		else
		{
			GRendererResource::DirectLightCount++;
			if (GRendererResource::DirectLightCount >= GRendererResource::DirectLights.ElementCount())
			{
				GRendererResource::DirectLights.Add(DirectLightInfo());
			}

			pInfo = &GRendererResource::DirectLights[GRendererResource::DirectLightCount - 1];
		}

		pInfo->pDirLight = dynamic_cast<IGDirectionLight*>(pLight);
		pInfo->pPtLight = dynamic_cast<IGPointLight*>(pLight);
		pInfo->pSpLight = dynamic_cast<IGSpotLight*>(pLight);

		if (pLight->IsCastShadow())
		{
			if (pInfo->pDirLight != nullptr)
			{
				pInfo->uFaceCountInMap = GVarShadowCascadeNum.nValue;
			}
			if (pInfo->pPtLight != nullptr)
			{
				pInfo->uFaceCountInMap = 6;

				pInfo->WorldToLights[0] = Math::MatrixView(pInfo->pPtLight->GetLightPosition(), Math::AXIS_X, Math::AXIS_Y);
				pInfo->WorldToLights[1] = Math::MatrixView(pInfo->pPtLight->GetLightPosition(), Math::AXIS_X_Inverse, Math::AXIS_Y);
				pInfo->WorldToLights[2] = Math::MatrixView(pInfo->pPtLight->GetLightPosition(), Math::AXIS_Y, Math::AXIS_Z_Inverse);
				pInfo->WorldToLights[3] = Math::MatrixView(pInfo->pPtLight->GetLightPosition(), Math::AXIS_Y_Inverse, Math::AXIS_Z);
				pInfo->WorldToLights[4] = Math::MatrixView(pInfo->pPtLight->GetLightPosition(), Math::AXIS_Z, Math::AXIS_Y);
				pInfo->WorldToLights[5] = Math::MatrixView(pInfo->pPtLight->GetLightPosition(), Math::AXIS_Z_Inverse, Math::AXIS_Y);

				pInfo->LightToClips[0] = GenPerspectiveMatrix(Constant_PI * 0.5f, 1.0f, 0.01f, pInfo->pPtLight->GetLightRange());
				pInfo->LightToClips[1] = GenPerspectiveMatrix(Constant_PI * 0.5f, 1.0f, 0.01f, pInfo->pPtLight->GetLightRange());
				pInfo->LightToClips[2] = GenPerspectiveMatrix(Constant_PI * 0.5f, 1.0f, 0.01f, pInfo->pPtLight->GetLightRange());
				pInfo->LightToClips[3] = GenPerspectiveMatrix(Constant_PI * 0.5f, 1.0f, 0.01f, pInfo->pPtLight->GetLightRange());
				pInfo->LightToClips[4] = GenPerspectiveMatrix(Constant_PI * 0.5f, 1.0f, 0.01f, pInfo->pPtLight->GetLightRange());
				pInfo->LightToClips[5] = GenPerspectiveMatrix(Constant_PI * 0.5f, 1.0f, 0.01f, pInfo->pPtLight->GetLightRange());
			}
			else if (pInfo->pSpLight != nullptr)
			{
				pInfo->uFaceCountInMap = 1;

				Float3 Dir = Math::Normalize(pInfo->pSpLight->GetLightDirection());
				Dir *= -1;

				Float3 up = Math::CrossProduct(Dir, Math::AXIS_Y);//right
				up = Math::CrossProduct(up, Dir);
				pInfo->WorldToLights[0] = Math::MatrixView(pInfo->pSpLight->GetLightPosition(), Dir, up);
				pInfo->LightToClips[0] = GenPerspectiveMatrix(
					Math::Clamp(pInfo->pSpLight->GetLightSpotAngle(), 0.0f, Constant_PI), 1, 0.01f,
					pInfo->pSpLight->GetLightRange());
			}

			if (pInfo->pShadowDepthMap == nullptr)
			{
				IGTexture::Description desc;
				desc.uWidth = GVarShadowMapResolution.nValue;
				desc.uHeight = GVarShadowMapResolution.nValue;
				desc.uDepth = 1;
				desc.bDynamic = False;
				desc.bTargetable = True;
				desc.eType = TextureType::ETexture2D;
				desc.uMipLevel = 1;
				desc.eFormat = EPF_D32;//TODO!! 如果此处和DepthBuffer不一样格式，就需要添加新的PSO
				pInfo->pShadowDepthMap = GraphicsResource::CreateTexture(desc);
			}

			if (pInfo->pScreenSpaceShadowBuffer == nullptr)
			{
				pInfo->pScreenSpaceShadowBuffer = GRendererResource::CreateBuffer(
					BufferType::EConstantBuffer, True, sizeof(CBSShadowMapCommon));
			}

			for (UInt32 u = 0; u < pInfo->uFaceCountInMap; u++)
			{
				if (pInfo->ViewBuffers[u] == nullptr)
				{
					pInfo->ViewBuffers[u] = GRendererResource::CreateBuffer(
						BufferType::EConstantBuffer, True, sizeof(CBSCommon));
				}
			}
		}

		if (pInfo->pLightBuffer == nullptr)
		{
			pInfo->pLightBuffer = GRendererResource::CreateBuffer(
				BufferType::EConstantBuffer, True, sizeof(CBSDeferredLight));
		}
	}

	Void	GraphicsRenderer::RegisterEnvironmentProbe(IGEnvironmentProbe* pProbe)
	{
		if (pProbe == nullptr)return;

		IGReflectionProbe* pREProbe = dynamic_cast<IGReflectionProbe*>(pProbe);
		if (pREProbe)
		{
			GRendererResource::ReflectionProbeList.Add(pREProbe);
		}
	}

	//For Skin And UI Instance Alway Off
	Void	GraphicsRenderer::RegisterPrimitive(
		IGPrimitive* pPrimitive,
		UInt32 uVertexOffset,
		UInt32 uVertexCount,
		IGMaterial* pMaterial,
		IGMaterialParameterBuffer* pParameterBlock,
		IGPrimitiveTransformBuffer* pTransformBlock,
		Region regionClip)
	{
		if (pPrimitive == nullptr ||
			pMaterial == nullptr ||
			pParameterBlock == nullptr)
		{
			return;
		}

		ASSERT_IF_FAILED(uVertexOffset + uVertexCount <= pPrimitive->GetIndexCapacity());
		if (uVertexOffset + uVertexCount > pPrimitive->GetIndexCapacity())
		{
			return;
		}

		GPrimitiveTransformBuffer* pTBlock = pTransformBlock == nullptr ?
			(GPrimitiveTransformBuffer*)GRendererResource::IdentityTransformBuffer :
			(GPrimitiveTransformBuffer*)pTransformBlock;

		if (pPrimitive->GetType() == IGPrimitive::EPrimT_Rigid2D)
		{
			GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::E2D].Add(DrawInstance(
				(GPrimitive*)pPrimitive,
				pTBlock,
				(GCommonMaterial*)pMaterial,
				(GCommonMaterialParameterBuffer*)pParameterBlock,
				uVertexOffset, uVertexCount, regionClip));
		}
		else if (pMaterial->GetDomainType() == MaterialDomain::EDecal)
		{
			GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::EDecal].Add(DrawInstance(
				(GPrimitive*)pPrimitive,
				pTBlock,
				(GCommonMaterial*)pMaterial,
				(GCommonMaterialParameterBuffer*)pParameterBlock,
				uVertexOffset, uVertexCount, regionClip));
		}
		else
		{
			switch (pMaterial->GetBlendMode())
			{
			case MaterialBlendMode::EOpaque:
			case MaterialBlendMode::EMasked:
			case MaterialBlendMode::EMaskTranslucency:
				GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::EOpaque].Add(DrawInstance(
					(GPrimitive*)pPrimitive,
					pTBlock,
					(GCommonMaterial*)pMaterial,
					(GCommonMaterialParameterBuffer*)pParameterBlock,
					uVertexOffset, uVertexCount, regionClip));
				break;
			case MaterialBlendMode::ETranslucency:
			case MaterialBlendMode::EAdditive:
				GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::ETranslucency].Add(DrawInstance(
					(GPrimitive*)pPrimitive,
					pTBlock,
					(GCommonMaterial*)pMaterial,
					(GCommonMaterialParameterBuffer*)pParameterBlock,
					uVertexOffset, uVertexCount, regionClip));
				break;
			}
		}

		if (pPrimitive == nullptr)return;
		for (UInt32 u = 0; u < GRendererResource::RegisteredPrimitives.ElementCount(); u++)
		{
			if (GRendererResource::RegisteredPrimitives[u] == pPrimitive)
				return;
		}

		GRendererResource::RegisteredPrimitives.Add(pPrimitive);
	}

	namespace Pass
	{
		ViewInfo MainViewInfo;
		UInt8	 TAAJitterFrameIndex = 0;
		Float2	 TAAJitter[8] = {
			Float2(-0.25f, -0.25f),
			Float2(0.5f, -0.125f),
			Float2(-0.125f, -0.5f),
			Float2(0.125f, 0.5f),
			Float2(-0.5f, 0.125f),
			Float2(0.25f, -0.25f),
			Float2(0.25f, 0.25f),
			Float2(-0.25f, -0.25f)
		};

		Void PreRender(IGCommandList* pCommandList, GRendererConfigure& configure)
		{
			Array<Float1> CascadeSplitRate(GVarShadowCascadeNum.nValue + 1);
			Array<Float4> CameraCascadeCorner((GVarShadowCascadeNum.nValue + 1) * 4);

			Float3 CameraFrustumCenter = MainViewInfo.FrustumFarCorners[0] - MainViewInfo.FrustumNearCorners[3];
			Float1 CameraFrustumRadius = Math::Length(CameraFrustumCenter);
			CameraFrustumCenter = MainViewInfo.FrustumNearCorners[3] + CameraFrustumCenter * 0.5f;

			CameraCascadeCorner[0] = Math::Combine(MainViewInfo.FrustumNearCorners[0], 1.0f);
			CameraCascadeCorner[1] = Math::Combine(MainViewInfo.FrustumNearCorners[1], 1.0f);
			CameraCascadeCorner[2] = Math::Combine(MainViewInfo.FrustumNearCorners[2], 1.0f);
			CameraCascadeCorner[3] = Math::Combine(MainViewInfo.FrustumNearCorners[3], 1.0f);

			Float4 CameraCornerDeltas[4] = {
				Math::Combine(MainViewInfo.FrustumFarCorners[0] - MainViewInfo.FrustumNearCorners[0], 0.0f),
				Math::Combine(MainViewInfo.FrustumFarCorners[1] - MainViewInfo.FrustumNearCorners[1], 0.0f),
				Math::Combine(MainViewInfo.FrustumFarCorners[2] - MainViewInfo.FrustumNearCorners[2], 0.0f),
				Math::Combine(MainViewInfo.FrustumFarCorners[3] - MainViewInfo.FrustumNearCorners[3], 0.0f)
			};

			for (UInt32 uCascade = 1; uCascade <= GVarShadowCascadeNum.nValue; uCascade++)
			{
				CascadeSplitRate[uCascade] = GVarCascadeShadowDistance.fValue * uCascade /
					((GVarShadowCascadeNum.nValue + 1.0f) *
						GVarShadowCascadeNum.nValue * 0.5f);

				CameraCascadeCorner[uCascade * 4] = CameraCascadeCorner[(uCascade - 1) * 4] + CameraCornerDeltas[0] * CascadeSplitRate[uCascade];
				CameraCascadeCorner[uCascade * 4 + 1] = CameraCascadeCorner[(uCascade - 1) * 4 + 1] + CameraCornerDeltas[1] * CascadeSplitRate[uCascade];
				CameraCascadeCorner[uCascade * 4 + 2] = CameraCascadeCorner[(uCascade - 1) * 4 + 2] + CameraCornerDeltas[2] * CascadeSplitRate[uCascade];
				CameraCascadeCorner[uCascade * 4 + 3] = CameraCascadeCorner[(uCascade - 1) * 4 + 3] + CameraCornerDeltas[3] * CascadeSplitRate[uCascade];
			}

			for (UInt32 uLight = 0; uLight < GRendererResource::CastingShadowLightCount; uLight++)
			{
				DirectLightInfo& Info = GRendererResource::CastingShadowLights[uLight];
				if (Info.pDirLight == nullptr)continue;

				Float3 LightDirection = Math::Normalize(Info.pDirLight->GetLightDirection()) * -1.0f;
				Float3 LightPosition = CameraFrustumCenter - LightDirection * CameraFrustumRadius;

				Float3 up = Math::CrossProduct(LightDirection, Math::AXIS_Y);//right
				up = Math::CrossProduct(up, LightDirection);

				Matrix4 WorldToLight = Math::MatrixView(LightPosition, LightDirection, up);

				for (UInt32 uCascade = 0; uCascade < GVarShadowCascadeNum.nValue; uCascade++)
				{
					Info.WorldToLights[uCascade] = WorldToLight;

					GVolumnAABB AABB;
					AABB.AddVertexPosition(Math::Split(Math::MatrixTransform(CameraCascadeCorner[uCascade * 4], WorldToLight)));
					AABB.AddVertexPosition(Math::Split(Math::MatrixTransform(CameraCascadeCorner[uCascade * 4 + 1], WorldToLight)));
					AABB.AddVertexPosition(Math::Split(Math::MatrixTransform(CameraCascadeCorner[uCascade * 4 + 2], WorldToLight)));
					AABB.AddVertexPosition(Math::Split(Math::MatrixTransform(CameraCascadeCorner[uCascade * 4 + 3], WorldToLight)));
					AABB.AddVertexPosition(Math::Split(Math::MatrixTransform(CameraCascadeCorner[(uCascade + 1) * 4], WorldToLight)));
					AABB.AddVertexPosition(Math::Split(Math::MatrixTransform(CameraCascadeCorner[(uCascade + 1) * 4 + 1], WorldToLight)));
					AABB.AddVertexPosition(Math::Split(Math::MatrixTransform(CameraCascadeCorner[(uCascade + 1) * 4 + 2], WorldToLight)));
					AABB.AddVertexPosition(Math::Split(Math::MatrixTransform(CameraCascadeCorner[(uCascade + 1) * 4 + 3], WorldToLight)));

					Float3 LightFrustumCornerMinMax[2] = {
						AABB.GetVolumePoint(2),
						AABB.GetVolumePoint(5)
					};

					Info.LightToClips[uCascade] = GenOrthographicMatrix(
						LightFrustumCornerMinMax[0][0],
						LightFrustumCornerMinMax[1][1],
						LightFrustumCornerMinMax[1][0],
						LightFrustumCornerMinMax[0][1],
						0.0f,
						LightFrustumCornerMinMax[1][2]);
				}
			}

			MapRegion region;
			if (pCommandList->Map(GRendererResource::BufferCommon, False, region))
			{
				CBSCommon* pData = (CBSCommon*)region.pAddress;

				pData->TimeParam = Float4(MainViewInfo.CurrentTime, MainViewInfo.DeltaTime, 0, 0);

				pData->ViewportParam = Float4(
					Pass::MainViewInfo.PresentViewPort.uWidth,
					Pass::MainViewInfo.PresentViewPort.uHeight,
					1.0f / Pass::MainViewInfo.PresentViewPort.uWidth,
					1.0f / Pass::MainViewInfo.PresentViewPort.uHeight);
				pData->WorldToView = Math::MatrixTranspose(MainViewInfo.WorldToView);
				pData->ViewToClip = Math::MatrixTranspose(MainViewInfo.ViewToClip);
				pData->WorldToClip = Math::MatrixTranspose(MainViewInfo.WorldToView * MainViewInfo.ViewToClip);
				pData->ClipToView = Math::MatrixTranspose(Math::MatrixInverse(MainViewInfo.ViewToClip));
				pData->ViewToWorld = Math::MatrixTranspose(Math::MatrixInverse(MainViewInfo.WorldToView));

				pData->RenderTargetParam = Float4(
					GRendererResource::BackbufferSize[0],
					GRendererResource::BackbufferSize[1],
					1.0f / GRendererResource::BackbufferSize[0],
					1.0f / GRendererResource::BackbufferSize[1]
				);

				pData->CameraParam = Math::Combine(MainViewInfo.ViewFrustumPosition, MainViewInfo.ZFar);

				pCommandList->Unmap(GRendererResource::BufferCommon);
			}

			//Only One Forward Light For Translucency
			if (pCommandList->Map(GRendererResource::BufferForwardLight, False, region))
			{
				CBSForwardLight* pData = (CBSForwardLight*)region.pAddress;
				pData->f4LightColor = MainViewInfo.LightColor;
				pData->f4LightDirection = MainViewInfo.LightDirection;

				pData->EnableSSR = GVarEnableSSR.bValue ? 1.0f : 0.0f;
				pData->IndirectLightColor_Intensity = Math::Combine(GVarAmbientColor.f3Value, GVarAmbientScale.fValue);

				if (GRendererResource::ReflectionProbeList.ElementCount() > 0)
				{
					pData->EnvReflectionCount = 1;
					pData->EnvReflectionCenter_InvRadius2 =
						Math::Combine(GRendererResource::ReflectionProbeList[0]->GetProbePosition(),
							1.0f / (GRendererResource::ReflectionProbeList[0]->GetProbeRange()*GRendererResource::ReflectionProbeList[0]->GetProbeRange()));
				}
				else
				{
					pData->EnvReflectionCount = 0;
				}
				pCommandList->Unmap(GRendererResource::BufferForwardLight);
			}
		}

		Void DeferredRender(IGCommandList* pCommandList, PrimitiveRenderStage eStage, DrawInstance& refDrawInstance)
		{
			refDrawInstance.pPrimitive->BindGResource(pCommandList);
			refDrawInstance.pTransformBlock->BindGResource(pCommandList);

			refDrawInstance.pMaterial->BindGResource(pCommandList,
				eStage, refDrawInstance.pPrimitive->GetType(), refDrawInstance.pTransformBlock->IsInstance());
			refDrawInstance.pParameterBlock->BindGResource(pCommandList);

			UInt32 uInstanceCount = refDrawInstance.pTransformBlock->IsInstance() ?
				refDrawInstance.pTransformBlock->GetTransformsCount() : 1;

			pCommandList->DrawIndex(refDrawInstance.uVertexCount, refDrawInstance.uVertexOffset, uInstanceCount,
				refDrawInstance.pPrimitive->GetType() == IGPrimitive::EPrimT_Line ? False : True);
		}
		Void ForwardRender(IGCommandList* pCommandList, PrimitiveRenderStage eStage, DrawInstance& refDrawInstance)
		{
			refDrawInstance.pPrimitive->BindGResource(pCommandList);
			refDrawInstance.pTransformBlock->BindGResource(pCommandList);

			refDrawInstance.pMaterial->BindGResource(pCommandList,
				eStage, refDrawInstance.pPrimitive->GetType(), refDrawInstance.pTransformBlock->IsInstance());
			refDrawInstance.pParameterBlock->BindGResource(pCommandList);

			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 2, GRendererResource::BufferForwardLight);

			if (GRendererResource::ReflectionProbeList.ElementCount() > 0)
			{
				pCommandList->BindShaderResource(ShaderType::EPixelShader, 2,
					GRendererResource::ReflectionProbeList[0]->GetReflectionTexture());
			}

			UInt32 uInstanceCount = refDrawInstance.pTransformBlock->IsInstance() ?
				refDrawInstance.pTransformBlock->GetTransformsCount() : 1;

			pCommandList->DrawIndex(refDrawInstance.uVertexCount, refDrawInstance.uVertexOffset, uInstanceCount,
				refDrawInstance.pPrimitive->GetType() == IGPrimitive::EPrimT_Line ? False : True);
		}

		Void ForwardRender2D(IGCommandList* pCommandList, DrawInstance& refDrawInstance)
		{
			refDrawInstance.pPrimitive->BindGResource(pCommandList);
			refDrawInstance.pTransformBlock->BindGResource(pCommandList);

			refDrawInstance.pMaterial->BindGResource(pCommandList,
				PrimitiveRenderStage::EForward2DPrimitive, refDrawInstance.pPrimitive->GetType(), refDrawInstance.pTransformBlock->IsInstance());
			refDrawInstance.pParameterBlock->BindGResource(pCommandList);

			pCommandList->BindConstantBuffer(ShaderType::EVertexShader, 0, GRendererResource::BufferCommon);
			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);

			Region scissor;
			if (refDrawInstance.regionClip == Region())
			{
				scissor.uLeft = 0;
				scissor.uTop = 0;
				scissor.uRight = GRendererResource::BackbufferSize[0];
				scissor.uBottom = GRendererResource::BackbufferSize[1];
			}
			else
			{
				scissor.uLeft = refDrawInstance.regionClip.uLeft;
				scissor.uTop = refDrawInstance.regionClip.uTop;
				scissor.uRight = refDrawInstance.regionClip.uRight;
				scissor.uBottom = refDrawInstance.regionClip.uBottom;
			}
			pCommandList->SetScissor(scissor);

			UInt32 uInstanceCount = refDrawInstance.pTransformBlock->IsInstance() ?
				refDrawInstance.pTransformBlock->GetTransformsCount() : 1;

			pCommandList->DrawIndex(refDrawInstance.uVertexCount, refDrawInstance.uVertexOffset, uInstanceCount, True);
		}

		Void DeferredPassOpaque(IGCommandList* pCommandList, PrimitiveRenderStage eStage)
		{
			for (UInt32 u = 0; u < GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::EOpaque].ElementCount(); u++)
			{
				pCommandList->BindConstantBuffer(ShaderType::EVertexShader, 0, GRendererResource::BufferCommon);
				pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);

				DeferredRender(pCommandList, eStage, GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::EOpaque][u]);
			}
		}

		Void DispatchPostProcessInstance(IGCommandList* pCommandList, GRendererPostProcessInsertionPoint eIPoint, UInt32 uWidth, UInt32 uHeight)
		{
			/*
			ID3D11UnorderedAccessView* Output[1] = { nullptr };
			for (UInt32 u = 0; u < PostProcessInstance[eIPoint].ElementCount(); u++)
			{
				Output[0] = GetOutputColorTargetUAV(ESceneColor0);

				IGMaterial* pMtl = PostProcessInstance[eIPoint][u]->GetRefMaterial();
				((GMaterial*)pMtl)->GetPSO(
					PrimitiveRenderStage::EPrimitiveRenderStageCount,
					IGPrimitive::Type::EPrimT_Rigid2D);

				GRenderer::Commandlist_Record->CSSetUnorderedAccessViews(0,
					ARRAY_ELEMENT_COUNT(Output), Output, nullptr);

				GRenderer::Commandlist_Record->Dispatch(uWidth, uHeight, 1);

				//交换两个ColorBuffer
				ColorTarget t = ColorTargets[ESceneColor0];
				ColorTargets[ESceneColor0] = ColorTargets[ESceneColor1];
				ColorTargets[ESceneColor1] = t;
			}
			*/
		}

		Void PreDepth(IGCommandList* pCommandList)
		{
			pCommandList->SetViewport(MainViewInfo.ViewPort);
			pCommandList->SetScissor(MainViewInfo.ViewPortScissor);

			pCommandList->BindRenderTarget(True,
				GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0], 0,
				False, nullptr, 0);
			DeferredPassOpaque(pCommandList, PrimitiveRenderStage::EDepthPrePass);

			pCommandList->Flush();
		}

		Void ShadowMap(IGCommandList* pCommandList)
		{
			CBSCommon lightProp;
			MapRegion region;

			for (UInt32 uLight = 0; uLight < GRendererResource::CastingShadowLightCount; uLight++)
			{
				DirectLightInfo& Info = GRendererResource::CastingShadowLights[uLight];
				pCommandList->BindRenderTarget(True, Info.pShadowDepthMap, 0, False, nullptr, 0);

				UInt32 FaceNumOnTextureWidth = Math::Ceil(sqrtf((Float1)Info.uFaceCountInMap));
				Float1 FaceWidth = GVarShadowMapResolution.nValue / FaceNumOnTextureWidth;

				for (UInt32 uShadowFace = 0; uShadowFace < Info.uFaceCountInMap; uShadowFace++)
				{
					pCommandList->Map(Info.ViewBuffers[uShadowFace], False, region);
					lightProp.WorldToView = Math::MatrixTranspose(Info.WorldToLights[uShadowFace]);
					lightProp.ViewToClip = Math::MatrixTranspose(Info.LightToClips[uShadowFace]);
					lightProp.WorldToClip = Math::MatrixTranspose(Info.WorldToLights[uShadowFace] *
						Info.LightToClips[uShadowFace]);
					lightProp.ClipToView = Math::MatrixTranspose(Math::MatrixInverse(Info.LightToClips[uShadowFace]));

					memcpy(region.pAddress, &lightProp, sizeof(lightProp));
					pCommandList->Unmap(Info.ViewBuffers[uShadowFace]);

					GViewport VP;
					VP.uX = (uShadowFace % FaceNumOnTextureWidth) * FaceWidth;
					VP.uY = (uShadowFace / FaceNumOnTextureWidth) * FaceWidth;
					VP.uWidth = FaceWidth;
					VP.uHeight = FaceWidth;
					VP.fMinDepth = 0.0f;
					VP.fMaxDepth = 1.0f;

					Region R;
					R.uLeft = VP.uX;
					R.uTop = VP.uY;
					R.uRight = R.uLeft + FaceWidth;
					R.uBottom = R.uTop + FaceWidth;

					Array<DrawInstance>& DrawInstances = GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::EOpaque];
					for (UInt32 u = 0; u < DrawInstances.ElementCount(); u++)
					{
						if (DrawInstances[u].pMaterial->GetShadingModel() ==
							MaterialShadingModel::ENolighting)
						{
							continue;
						}

						pCommandList->SetViewport(VP);
						pCommandList->SetScissor(R);

						pCommandList->BindConstantBuffer(ShaderType::EVertexShader, 0, Info.ViewBuffers[uShadowFace]);
						pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, Info.ViewBuffers[uShadowFace]);

						DeferredRender(pCommandList, PrimitiveRenderStage::EDepthPrePass, DrawInstances[u]);
					}
				}
			}
			pCommandList->Flush();
		}

		Void DeferredBase(IGCommandList* pCommandList)
		{
			pCommandList->BindRenderTarget(False,
				GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0], 0,
				True,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0], 0,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferA], 0,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferB], 0,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferC], 0,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferD], 0);

			pCommandList->SetViewport(MainViewInfo.ViewPort);
			pCommandList->SetScissor(MainViewInfo.ViewPortScissor);

			pCommandList->BindConstantBuffer(ShaderType::EVertexShader, 0, GRendererResource::BufferCommon);
			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);
			//for all Frustrum(Camera、Light)
			DeferredPassOpaque(pCommandList, PrimitiveRenderStage::EBasePass);

			pCommandList->Flush();
		}

		Void PP_SSAO(IGCommandList* pCommandList)
		{
			pCommandList->SetViewport(MainViewInfo.ViewPort);
			pCommandList->SetScissor(MainViewInfo.ViewPortScissor);

			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);
			GSSAOPass::Render(
				pCommandList,
				GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferC],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EScreenSpaceShadow],
				GVarSSAORange.fValue, GVarAmbientScale.fValue, 1.56f);
			pCommandList->Flush();

			GBlurPass::GaussianBlur(
				pCommandList,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EScreenSpaceShadow],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferB],
				GRendererResource::BackbufferSize[0], GRendererResource::BackbufferSize[1], 1.0f);

			pCommandList->Flush();
		}

		Void DeferredDecal(IGCommandList* pCommandList)
		{
			if (GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::EDecal].ElementCount() > 0)
			{
				pCommandList->SetViewport(MainViewInfo.ViewPort);
				pCommandList->SetScissor(MainViewInfo.ViewPortScissor);

				pCommandList->BindRenderTarget(
					False,
					nullptr, 0,
					False,
					GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0], 0,
					GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferA], 0,
					GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferB], 0,
					GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferC], 0
				);

				for (UInt32 u = 0;
					u < GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::EDecal].ElementCount();
					u++)
				{
					pCommandList->BindConstantBuffer(ShaderType::EVertexShader, 0, GRendererResource::BufferCommon);
					pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);
					pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0]);

					DeferredRender(pCommandList, PrimitiveRenderStage::EDecalPass,
						GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::EDecal][u]);
				}

				pCommandList->Flush();
			}
		}

		Void DirectLightingShadow(IGCommandList* pCommandList, DirectLightInfo& Info)
		{
			MapRegion region;
			CBSShadowMapCommon smc;

			pCommandList->Flush();
			smc.ShadowMapInvResolution = Float2(1.0f / GVarShadowMapResolution.nValue, 1.0f / GVarShadowMapResolution.nValue);
			smc.ShadowMapDepthOffset = GVarShadowDepthOffset.fValue;

			UInt32 FaceNumOnTextureWidth = Math::Ceil(sqrtf((Float1)Info.uFaceCountInMap));
			Float1 FaceWidth = GVarShadowMapResolution.nValue / FaceNumOnTextureWidth;
			Float1 FaceUVScale = FaceWidth / GVarShadowMapResolution.nValue;

			pCommandList->SetViewport(MainViewInfo.ViewPort);
			pCommandList->SetScissor(MainViewInfo.ViewPortScissor);

			pCommandList->BindRenderTarget(False, nullptr, 0,
				True, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EScreenSpaceShadow], 0);
			pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);

			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);

			pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0]);
			pCommandList->BindShaderResource(ShaderType::EPixelShader, 1, Info.pShadowDepthMap);

			pCommandList->BindPipeline(GRendererResource::PSOScreenSpaceShadow);

			for (UInt32 uShadowFace = 0; uShadowFace < Info.uFaceCountInMap; uShadowFace++)
			{
				smc.ShadowMapWorldToClip[uShadowFace] =
					Math::MatrixTranspose(Info.WorldToLights[uShadowFace] * Info.LightToClips[uShadowFace]);
				smc.ShadowMapUVTransform[uShadowFace] =
					Float4((uShadowFace % FaceNumOnTextureWidth) * FaceUVScale,
					(uShadowFace / FaceNumOnTextureWidth) * FaceUVScale,
						FaceUVScale, FaceUVScale);
			}

			smc.ShadowMapDebugFlag_FaceCount =
				(GVarCascadeShadowDebug.bValue ? (1 << 31) : 0) | Info.uFaceCountInMap;

			pCommandList->Map(Info.pScreenSpaceShadowBuffer, False, region);
			memcpy(region.pAddress, &smc, sizeof(CBSShadowMapCommon));
			pCommandList->Unmap(Info.pScreenSpaceShadowBuffer);
			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 1, Info.pScreenSpaceShadowBuffer);

			pCommandList->Draw(3, 1, True);
		}
		Void DirectSingleLighting(IGCommandList* pCommandList, DirectLightInfo& Info)
		{
			UInt32 LightCombineFlags = Info.pShadowDepthMap != nullptr ?
				(4 | (GVarCascadeShadowDebug.bValue ? 0x80000000 : 0)) : 0;

			MapRegion region;
			CBSDeferredLight lightProp;
			if (Info.pDirLight != nullptr)
			{
				lightProp.LightCombineFlags = 0 | LightCombineFlags |
					(Info.pDirLight->IsAtmosphericScatter() ? 0x00000010 : 0);

				lightProp.Color_Intensity = Math::Combine(
					Info.pDirLight->GetColor(),
					Info.pDirLight->GetCandela());

				lightProp.Position_Attenuation = Float4(0.0f, 0.0f, 0.0f, 1.0f);

				lightProp.Direction_Range = Math::Combine(
					Info.pDirLight->GetLightDirection(), 9999.0f);
			}
			else if (Info.pPtLight != nullptr)
			{
				lightProp.LightCombineFlags = 1 | LightCombineFlags |
					(Info.pPtLight->IsAtmosphericScatter() ? 0x00000010 : 0);

				lightProp.Color_Intensity = Math::Combine(
					Info.pPtLight->GetColor(),
					Info.pPtLight->GetCandela());

				lightProp.Position_Attenuation = Math::Combine(
					Info.pPtLight->GetLightPosition(),
					Info.pPtLight->GetLightAttenuation());

				lightProp.Direction_Range = Math::Combine(
					Float3(), Info.pPtLight->GetLightRange());
			}
			else if (Info.pSpLight != nullptr)
			{
				lightProp.LightCombineFlags = 2 | LightCombineFlags |
					(Info.pSpLight->IsAtmosphericScatter() ? 0x00000010 : 0);

				lightProp.Color_Intensity = Math::Combine(
					Info.pSpLight->GetColor(),
					Info.pSpLight->GetCandela());

				lightProp.Position_Attenuation = Math::Combine(
					Info.pSpLight->GetLightPosition(),
					Info.pSpLight->GetLightAttenuation());

				lightProp.Direction_Range = Math::Combine(
					Info.pSpLight->GetLightDirection(),
					Info.pSpLight->GetLightRange());

				lightProp.Angle_XXX = Float4(
					Math::Clamp(Info.pSpLight->GetLightSpotAngle(), 0.0f, Constant_PI),
					0.0f, 0.0f, 0.0f);
			}
			else
			{
				return;
			}

			pCommandList->Map(Info.pLightBuffer, False, region);
			memcpy(region.pAddress, &lightProp, sizeof(lightProp));
			pCommandList->Unmap(Info.pLightBuffer);

			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 1, Info.pLightBuffer);
			pCommandList->Draw(3, 1, True);
		}
		Void DirectLighting(IGCommandList* pCommandList)
		{
			MapRegion region;
			CBSDeferredLight lightProp;
			CBSShadowMapCommon smc;

			MARK_EVENT_BEGIN(DirectLighting_NoCastingShadow);
			{
				pCommandList->SetViewport(MainViewInfo.ViewPort);
				pCommandList->SetScissor(MainViewInfo.ViewPortScissor);

				pCommandList->BindRenderTarget(
					False, nullptr, 0,
					False, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0], 0);

				pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);

				pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0]);
				pCommandList->BindShaderResource(ShaderType::EPixelShader, 1, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferA]);
				pCommandList->BindShaderResource(ShaderType::EPixelShader, 2, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferB]);
				pCommandList->BindShaderResource(ShaderType::EPixelShader, 3, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferC]);
				pCommandList->BindShaderResource(ShaderType::EPixelShader, 4, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EScreenSpaceShadow]);

				pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);

				pCommandList->BindPipeline(GRendererResource::PSODeferredLighting);

				for (UInt32 u = 0; u < GRendererResource::DirectLightCount; u++)
				{
					DirectLightInfo& Info = GRendererResource::DirectLights[u];

					DirectSingleLighting(pCommandList, Info);
				}
			}
			MARK_EVENT_END();

			MARK_EVENT_BEGIN(DirectLighting_CastingShadow);
			{
				pCommandList->Flush();

				for (UInt32 uLight = 0; uLight < GRendererResource::CastingShadowLightCount; uLight++)
				{
					DirectLightInfo& Info = GRendererResource::CastingShadowLights[uLight];

					UInt32 FaceNumOnTextureWidth = Math::Ceil(sqrtf((Float1)Info.uFaceCountInMap));
					Float1 FaceWidth = GVarShadowMapResolution.nValue / FaceNumOnTextureWidth;
					Float1 FaceUVScale = FaceWidth / GVarShadowMapResolution.nValue;

					DirectLightingShadow(pCommandList, Info);

					//Render Light
					pCommandList->Flush();

					pCommandList->SetViewport(MainViewInfo.ViewPort);
					pCommandList->SetScissor(MainViewInfo.ViewPortScissor);

					pCommandList->BindRenderTarget(
						False, nullptr, 0,
						False, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0], 0);

					pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);
					pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0]);
					pCommandList->BindShaderResource(ShaderType::EPixelShader, 1, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferA]);
					pCommandList->BindShaderResource(ShaderType::EPixelShader, 2, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferB]);
					pCommandList->BindShaderResource(ShaderType::EPixelShader, 3, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferC]);
					pCommandList->BindShaderResource(ShaderType::EPixelShader, 4, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EScreenSpaceShadow]);

					pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);

					pCommandList->BindPipeline(GRendererResource::PSODeferredLighting);

					DirectSingleLighting(pCommandList, Info);
					pCommandList->Flush();
				}
			}
			MARK_EVENT_END();
		}
		Void InDirectLighting(IGCommandList* pCommandList)
		{
			pCommandList->SetViewport(MainViewInfo.ViewPort);
			pCommandList->SetScissor(MainViewInfo.ViewPortScissor);

			GIndirectLightPass::Render(
				pCommandList,
				GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferA],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferB],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferC],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1],
				Math::Combine(GVarAmbientColor.f3Value, GVarAmbientScale.fValue),
				GVarEnableSSR.bValue,
				GRendererResource::ReflectionProbeList);

			pCommandList->Flush();
		}

		Void MaterialPP_AfterOpaque(IGCommandList* pCommandList)
		{
			DispatchPostProcessInstance(pCommandList, EPPIP_AfterOpaque,
				MainViewInfo.ViewPort.uWidth, MainViewInfo.ViewPort.uHeight);
		}

		Void DirectionBlur(IGCommandList* pCommandList)
		{
			pCommandList->BindConstantBuffer(ShaderType::EComputeShader, 0, GRendererResource::BufferCommon);

			GBlurPass::DirectionBlur(
				pCommandList,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferC],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferD],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0],
				MainViewInfo.ViewPort.uWidth,
				MainViewInfo.ViewPort.uHeight,
				Float2(-0.5f, -0.5f),
				8, (const UInt32)MaterialShadingModel::EPostprocessFur);

			pCommandList->Flush();
		}
		Void PP_HeightFog(IGCommandList* pCommandList)
		{
			pCommandList->SetViewport(MainViewInfo.ViewPort);
			pCommandList->SetScissor(MainViewInfo.ViewPortScissor);

			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);
			GHeightFogPass::Render(
				pCommandList,
				GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0],
				GVarHeightFogColor.f3Value,
				GVarHeightFogDensity.fValue,
				GVarHeightFogStartDistance.fValue,
				GVarHeightFogDistanceFalloff.fValue,
				GVarHeightFogHeight.fValue,
				GVarHeightFogHeightFalloff.fValue);

			pCommandList->Flush();
		}

		Void SyncSceneTexture(IGCommandList* pCommandList)
		{
			//半透要用scenecolor和depth，需要保证0和1完全一致
			pCommandList->CopyResource(
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]);
			pCommandList->CopyResource(
				GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth1],
				GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0]);
			/*
			//Pre Filter Environment Reflect
			GRenderer::Commandlist_Record->GenerateMips(
				GetOutputColorTarget(OutputTargetType::ESceneColor1)->Srv);
				*/
		}

		Void ForwardTranslucent(IGCommandList* pCommandList)
		{
			pCommandList->BindRenderTarget(False,
				GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0], 0,
				False,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0], 0,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferD], 0);

			pCommandList->SetViewport(MainViewInfo.ViewPort);
			pCommandList->SetScissor(MainViewInfo.ViewPortScissor);

			for (UInt32 u = 0; u < GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::ETranslucency].ElementCount(); u++)
			{
				pCommandList->BindShaderResource(ShaderType::EPixelShader, 0,
					GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth1]);
				pCommandList->BindShaderResource(ShaderType::EPixelShader, 1,
					GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1]);

				pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);

				pCommandList->BindConstantBuffer(ShaderType::EVertexShader, 0, GRendererResource::BufferCommon);
				pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);

				ForwardRender(pCommandList, PrimitiveRenderStage::EForwardTranslucent,
					GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::ETranslucency][u]);
			}

			pCommandList->Flush();
		}

		Void MaterialPP_AfterTranslucent(IGCommandList* pCommandList)
		{
			DispatchPostProcessInstance(pCommandList, EPPIP_AfterTranslucency,
				MainViewInfo.ViewPort.uWidth, MainViewInfo.ViewPort.uHeight);
		}

		Void PP_RadialBlurGodRay(IGCommandList* pCommandList, IGViewFrustum* pViewFrustum)
		{
			if (Pass::MainViewInfo.MainLight == nullptr)return;

			GViewport VP;
			VP.uX = 0;
			VP.uY = 0;
			VP.fMinDepth = 0.0f;
			VP.fMaxDepth = 1.0f;
			VP.uWidth = GRendererResource::BackbufferSize[0];
			VP.uHeight = GRendererResource::BackbufferSize[1];
			pCommandList->SetViewport(VP);
			Region R;
			R.uLeft = 0;
			R.uTop = 0;
			R.uRight = VP.uWidth;
			R.uBottom = VP.uHeight;
			pCommandList->SetScissor(R);

			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);

			GBrightnessFilterPass::Render(
				pCommandList,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0],
				GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1],
				0.0f, GVarRBGodRayDepthBegin.fValue);

			Float4 lightP = Math::Combine(Pass::MainViewInfo.MainLight->GetLightDirection() * ((GViewFrustum*)pViewFrustum)->GetZFar(), 1.0f);
			lightP = Math::MatrixTransform(lightP, pViewFrustum->GetWorldToView() * pViewFrustum->GetViewToClip());
			lightP /= lightP[3];

			pCommandList->SetViewport(MainViewInfo.ViewPort);
			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);
			GRadialBlurGodRayPass::Render(
				pCommandList,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0],
				2,
				GVarRBGodRaySampleCount.nValue,
				GVarRBGodRayAttenuation.fValue,
				GVarRBGodRayStrength.fValue,
				GVarRBGodRayRange.fValue,
				Float2(lightP[0] * 0.5f + 0.5f, lightP[1] * -0.5f - 0.5f));

			pCommandList->Flush();
		}
		Void PP_Bloom(IGCommandList* pCommandList)
		{
			GViewport VP;
			VP.uX = 0;
			VP.uY = 0;
			VP.fMinDepth = 0.0f;
			VP.fMaxDepth = 1.0f;
			VP.uWidth = GRendererResource::BackbufferSize[0];
			VP.uHeight = GRendererResource::BackbufferSize[1];
			pCommandList->SetViewport(VP);

			Region R;
			R.uLeft = 0;
			R.uTop = 0;
			R.uRight = GRendererResource::BackbufferSize[0];
			R.uBottom = GRendererResource::BackbufferSize[1];
			pCommandList->SetScissor(R);

			GBrightnessFilterPass::Render(
				pCommandList,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0],
				GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1],
				3.14f, 0.0f);

			pCommandList->Flush();

			GBloomPass::Render(
				pCommandList,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0],
				GRendererResource::BackbufferSize[0], GRendererResource::BackbufferSize[1],
				GVarBloomExporedLuminance.fValue);

			pCommandList->Flush();
		}

		Void MaterialPP_BeforeToneMap(IGCommandList* pCommandList)
		{
			DispatchPostProcessInstance(pCommandList, EPPIP_BeforeToneMap,
				MainViewInfo.ViewPort.uWidth, MainViewInfo.ViewPort.uHeight);
		}

		Void PP_ToneMap(IGCommandList* pCommandList)
		{
			pCommandList->SetViewport(MainViewInfo.ViewPort);
			pCommandList->SetScissor(MainViewInfo.ViewPortScissor);

			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);

			GTonemapPass::Render(
				pCommandList,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0],
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1],
				1.0f, 0.5f);
		}

		Void TAA(IGCommandList* pCommandList)
		{

		}

		Void ForwardScreenSpace(IGCommandList* pCommandList)
		{
			pCommandList->SetViewport(MainViewInfo.ViewPort);
			//Render All 2DPrimitive
			pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0]);
			pCommandList->BindShaderResource(ShaderType::EPixelShader, 1, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]);
			pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);

			pCommandList->BindRenderTarget(
				False, nullptr, 0,
				False, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1], 0);
			for (UInt32 u = 0; u < GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::E2D].ElementCount(); u++)
			{
				if (GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::E2D][u].pMaterial->GetBlendMode() ==
					MaterialBlendMode::EOpaque)
					ForwardRender2D(pCommandList, GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::E2D][u]);
			}

			for (UInt32 u = 0; u < GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::E2D].ElementCount(); u++)
			{
				if (GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::E2D][u].pMaterial->GetBlendMode() !=
					MaterialBlendMode::EOpaque)
					ForwardRender2D(pCommandList, GRendererResource::DrawInstanceList[(const UInt8)DrawInstanceType::E2D][u]);
			}
		}

		Void ResultPresent(IGCommandList* pCommandList, const IGTexture* PresentTarget)
		{
			pCommandList->SetViewport(MainViewInfo.PresentViewPort);
			pCommandList->SetScissor(MainViewInfo.PresentViewPortScissor);

			pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);

			Float1 Gamma = 2.2f;
			GGammaPass::Render(
				pCommandList,
				GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1],
				PresentTarget, Gamma);

			pCommandList->Flush();
		}

		Void PostRender()
		{
			Pass::TAAJitterFrameIndex = (Pass::TAAJitterFrameIndex + 1) % 8;

			for (UInt32 u = 0; u < (const UInt32)DrawInstanceType::ECount; u++)
			{
				GRendererResource::DrawInstanceList[u].Clear();
			}

			for (UInt32 u = 0; u < GRendererPostProcessInsertionPoint::EPPIP_Count; u++)
			{
				//PostProcessInstance[u].Clear();
			}

			GRendererResource::DirectLightCount = 0;
			GRendererResource::CastingShadowLightCount = 0;
			GRendererResource::RegisteredPrimitives.Clear();
		}
	}

	Void	GraphicsRenderer::Render(IGViewFrustum* pFrustum, GRendererConfigure& configure)
	{
		//Before Render
		GRenderer::WaitUntilRendered();
		if (pFrustum == nullptr)
		{
			Pass::PostRender();
			return;
		}
		Float2 vpSize = pFrustum->GetViewportSize();
		if (vpSize[0] < 1.0f || vpSize[1] < 1.0f)
		{
			Pass::PostRender();
			return;
		}
		GRendererResource::FrameStatistics.uPrimitiveCount =
			GRendererResource::RegisteredPrimitives.ElementCount();
		GRendererResource::FrameStatistics.uDrawCall = 0;

		Float1 vpAspect = vpSize[0] / vpSize[1];
		Pass::MainViewInfo.PresentViewPort.uX = 0;
		Pass::MainViewInfo.PresentViewPort.uY = 0;
		Pass::MainViewInfo.PresentViewPort.fMinDepth = 0.0f;
		Pass::MainViewInfo.PresentViewPort.fMaxDepth = 1.0f;
		Pass::MainViewInfo.PresentViewPort.uWidth = vpSize[0];
		Pass::MainViewInfo.PresentViewPort.uHeight = vpSize[1];

		Pass::MainViewInfo.ViewPort = Pass::MainViewInfo.PresentViewPort;
		Pass::MainViewInfo.ViewPort.uWidth = vpSize[0] * configure.fResolutionRatio;
		Pass::MainViewInfo.ViewPort.uHeight = vpSize[1] * configure.fResolutionRatio;
		if (Pass::MainViewInfo.ViewPort.uWidth > GRendererResource::BackbufferSize[0])
		{
			Pass::MainViewInfo.ViewPort.uWidth = GRendererResource::BackbufferSize[0];
			Pass::MainViewInfo.ViewPort.uHeight = GRendererResource::BackbufferSize[0] / vpAspect;
		}
		if (Pass::MainViewInfo.ViewPort.uHeight > GRendererResource::BackbufferSize[1])
		{
			Pass::MainViewInfo.ViewPort.uHeight = GRendererResource::BackbufferSize[1];
			Pass::MainViewInfo.ViewPort.uWidth = GRendererResource::BackbufferSize[1] * vpAspect;
		}
		Pass::MainViewInfo.ViewPortScissor.uLeft = 0;
		Pass::MainViewInfo.ViewPortScissor.uTop = 0;
		Pass::MainViewInfo.ViewPortScissor.uRight = Pass::MainViewInfo.ViewPort.uWidth;
		Pass::MainViewInfo.ViewPortScissor.uBottom = Pass::MainViewInfo.ViewPort.uHeight;

		Pass::MainViewInfo.PresentViewPortScissor.uLeft = 0;
		Pass::MainViewInfo.PresentViewPortScissor.uTop = 0;
		Pass::MainViewInfo.PresentViewPortScissor.uRight = Pass::MainViewInfo.PresentViewPort.uWidth;
		Pass::MainViewInfo.PresentViewPortScissor.uBottom = Pass::MainViewInfo.PresentViewPort.uHeight;

		//Time
		Pass::MainViewInfo.CurrentTime = (Float1)Timer::GetTime();
		Pass::MainViewInfo.DeltaTime = (Float1)Timer::GetDeltaTime();
		//ViewFrustum
		Pass::MainViewInfo.WorldToView = pFrustum->GetWorldToView();
		Pass::MainViewInfo.ViewToClip = pFrustum->GetViewToClip();
		//ViewToClip.Set(3, 0, ViewToClip.Get(3, 0) + Pass::TAAJitter[Pass::TAAJitterFrameIndex][0] * pData->ViewportParam[2]);
		//ViewToClip.Set(3, 1, ViewToClip.Get(3, 1) + Pass::TAAJitter[Pass::TAAJitterFrameIndex][1] * pData->ViewportParam[3]);
		Pass::MainViewInfo.ViewFrustumPosition = ((GViewFrustum*)pFrustum)->GetPosition();
		Pass::MainViewInfo.ZNear = ((GViewFrustum*)pFrustum)->GetZNear();
		Pass::MainViewInfo.ZFar = ((GViewFrustum*)pFrustum)->GetZFar();
		pFrustum->GetCornerPositions(
			Pass::MainViewInfo.FrustumNearCorners,
			Pass::MainViewInfo.FrustumFarCorners);

		//Forward Main Light
		Pass::MainViewInfo.MainLight = nullptr;
		if (GRendererResource::CastingShadowLightCount > 0 || GRendererResource::DirectLightCount > 0)
		{
			for (UInt32 u = 0; u < GRendererResource::CastingShadowLightCount; u++)
			{
				if (GRendererResource::CastingShadowLights[u].pDirLight != nullptr)
				{
					Pass::MainViewInfo.MainLight = GRendererResource::CastingShadowLights[u].pDirLight;
					break;
				}
			}
			if (Pass::MainViewInfo.MainLight == nullptr)
			{
				for (UInt32 u = 0; u < GRendererResource::DirectLightCount; u++)
				{
					if (GRendererResource::DirectLights[u].pDirLight != nullptr)
					{
						Pass::MainViewInfo.MainLight = GRendererResource::DirectLights[u].pDirLight;
						break;
					}
				}
			}
		}

		if (Pass::MainViewInfo.MainLight != nullptr)
		{
			Pass::MainViewInfo.LightColor = Math::Combine(Pass::MainViewInfo.MainLight->GetColor(), Pass::MainViewInfo.MainLight->GetCandela());
			Pass::MainViewInfo.LightDirection = Math::Combine(Pass::MainViewInfo.MainLight->GetLightDirection(), 0);
		}
		else
		{
			Pass::MainViewInfo.LightColor = Float4(0, 0, 0, 0);
			Pass::MainViewInfo.LightDirection = Float4(0, 0, 0, 0);
		}

		GRenderer::LockThread();
		//Begin Render
		MARK_EVENT_BEGIN(Prepare);
		Pass::PreRender(GRenderer::CommandList(), configure);
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(DepthPrePass);
		Pass::PreDepth(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(LightShadowMapPass);
		Pass::ShadowMap(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(OpaqueBasePass);
		Pass::DeferredBase(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(SSAOPass);
		if (GVarEnableSSAO.bValue)
		{
			Pass::PP_SSAO(GRenderer::CommandList());
		}
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(Decal);
		Pass::DeferredDecal(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(DirectLighting);
		Pass::DirectLighting(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(IndirectLighting);
		Pass::InDirectLighting(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(PostProcess_AfterOpaque);
		Pass::MaterialPP_AfterOpaque(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(DirectionBlur);
		Pass::DirectionBlur(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(HeightFog);
		if (GVarHeightFogEnable.bValue)
		{
			Pass::PP_HeightFog(GRenderer::CommandList());
		}
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(SyncSceneTexture);
		Pass::SyncSceneTexture(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(ForwardTranslucent);
		Pass::ForwardTranslucent(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(PostProcess_AfterTranslucency);
		Pass::MaterialPP_AfterTranslucent(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(RadialBlurGodRay);
		if (GVarEnableRBGodRay.bValue)
		{
			Pass::PP_RadialBlurGodRay(GRenderer::CommandList(), pFrustum);
		}
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(Bloom);
		if (GVarEnableBloom.bValue)
		{
			Pass::PP_Bloom(GRenderer::CommandList());
		}
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(PostProcess_BeforeToneMap);
		Pass::MaterialPP_BeforeToneMap(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(TAA);
		Pass::TAA(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(ToneMap);
		Pass::PP_ToneMap(GRenderer::CommandList());
		MARK_EVENT_END();

#if MX_BUILD_EDITOR
		if (GVarShadingDebugView.bValue)
		{
			UInt32 uX = 0;
			UInt32 uW = Pass::MainViewInfo.ViewPort.uWidth / 6;
			UInt32 uH = Pass::MainViewInfo.ViewPort.uHeight / 6;

#define BITBLIT_TARGET(target) \
			GBlurPass::BitBlit(GRenderer::CommandList(), target, GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor1], uX, 0, uW, uH, False);\
			uX += uW;

			BITBLIT_TARGET(GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]);
			BITBLIT_TARGET(GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferA]);
			BITBLIT_TARGET(GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferB]);
			BITBLIT_TARGET(GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferC]);
			BITBLIT_TARGET(GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferD]);
			BITBLIT_TARGET(GRendererResource::DepthTargets[(const UInt32)DepthTargetType::ESceneDepth0]);

#undef BITBLIT_TARGET
		}
#endif

		MARK_EVENT_BEGIN(Primitive_ScreenSpace);
		Pass::ForwardScreenSpace(GRenderer::CommandList());
		MARK_EVENT_END();

		MARK_EVENT_BEGIN(Present);
		Pass::ResultPresent(GRenderer::CommandList(), ((GViewFrustum*)pFrustum)->GetColorTarget());
		MARK_EVENT_END();

		GRenderer::UnlockThread();

		GIsRendering.Exchange(1);
		//Push Command
#if 0
		ThreadManager::AddThreadTask(
			RENDER_THREAD_NAME,
			[]() {
			GRenderer::Execute();
			GIsRendering.Exchange(0);
	},
			"RenderExecution");
#else
		GRenderer::Execute();
		GIsRendering.Exchange(0);
#endif

		Pass::PostRender();
}

	Void	GraphicsRenderer::GetStatistics(GStatistics* pStatis)
	{
		if (pStatis == nullptr)return;

		GRenderer::LockThread();
		memcpy(pStatis, &GRendererResource::FrameStatistics, sizeof(GStatistics));
		GRenderer::UnlockThread();
	}
}