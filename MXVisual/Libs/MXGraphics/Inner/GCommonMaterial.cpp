#include "GCommonMaterial.h"
#include "GTexture.h"
#include "MXMath.h"
#include "stdio.h"
#include "MXFile.h"
#include "MXXmlFile.h"
#include "MXGraphicsRenderer.h"
#include "GShaderManager.h"
#include "MXLog.h"

#include <string>

namespace MXVisual
{
	const Char* arrMaterialFunctionMacro[] = {
		"",
		"#define CALLBACK_SURFACEPARAMS 1",
		"#define CALLBACK_MESHPARAMS 1",
	};

	const Char* arrMaterialFunctionInterface[] = {
		"",
		"void CBSurfaceParams(in PixelParams input, inout SurfaceParams output)",
		"void CBMeshParams(in VertexParams input, inout MeshParams output)",
	};
	GMaterialParam InvalidParam("InvalidParam", NULL, IGMaterialParam::EMPT_Invalid);

	GCommonMaterialParameterBuffer::GCommonMaterialParameterBuffer(UInt32 UniqueID)
		: m_uUniqueID(UniqueID)
		, m_pRefMaterial(nullptr)
	{

	}
	GCommonMaterialParameterBuffer::~GCommonMaterialParameterBuffer()
	{
	}
	IGMaterialParam*	GCommonMaterialParameterBuffer::GetParamByName(const Char* szName) const
	{
		for (UInt32 u = 0; u < m_arrDyncParam.ElementCount(); u++)
		{
			if (strcmp(m_arrDyncParam[u]->GetName(), szName) == 0)
			{
				return m_arrDyncParam[u];
			}
		}
		for (UInt32 u = 0; u < m_arrTexParam.ElementCount(); u++)
		{
			if (strcmp(m_arrTexParam[u]->GetName(), szName) == 0)
			{
				return m_arrTexParam[u];
			}
		}
		return &InvalidParam;
	}
	IGMaterialParam*	GCommonMaterialParameterBuffer::GetParamByIndex(UInt32 uParam) const
	{
		if (uParam < m_arrDyncParam.ElementCount())
		{
			return m_arrDyncParam[uParam];
		}
		else if (uParam < (m_arrDyncParam.ElementCount() + m_arrTexParam.ElementCount()))
		{
			return m_arrTexParam[uParam - m_arrDyncParam.ElementCount()];
		}
		return &InvalidParam;
	}
	Void				GCommonMaterialParameterBuffer::BindGResource(IGCommandList* pCommandList)
	{
		if (!m_bBufferUpdateToDate && m_pBuffer != nullptr)
		{
			MapRegion region;
			pCommandList->Map(m_pBuffer, False, region);

			memcpy(region.pAddress, m_MaterialBufferData.GetRawData(), m_pRefMaterial->GetMaterialBufferSize());

			pCommandList->Unmap(m_pBuffer);

			m_bBufferUpdateToDate = True;
		}

		if (m_pRefMaterial->GetMaterialBufferBeginSlot(ShaderType::EVertexShader) != -1)
		{
			pCommandList->BindConstantBuffer(ShaderType::EVertexShader,
				m_pRefMaterial->GetMaterialBufferBeginSlot(ShaderType::EVertexShader), m_pBuffer);
		}

		if (m_pRefMaterial->GetMaterialBufferBeginSlot(ShaderType::EPixelShader) != -1)
		{
			pCommandList->BindConstantBuffer(ShaderType::EPixelShader,
				m_pRefMaterial->GetMaterialBufferBeginSlot(ShaderType::EPixelShader), m_pBuffer);
		}

		UInt32 UserTextureSlotBegin = m_pRefMaterial->GetMaterialTextureBeginSlot(ShaderType::EPixelShader);
		if (UserTextureSlotBegin != -1)
		{
			for (UInt32 u = 0; u < m_arrTextureSlot.ElementCount(); u++)
			{
				pCommandList->BindShaderResource(ShaderType::EPixelShader, UserTextureSlotBegin + u, m_arrTextureSlot[u]);
			}
		}
	}
	Boolean				GCommonMaterialParameterBuffer::Init(UInt32 uBufferSize)
	{
		if (uBufferSize > 0)
		{
			m_pBuffer = GRendererResource::CreateBuffer(
				BufferType::EConstantBuffer,
				True, uBufferSize);

			return m_pBuffer != nullptr;
		}
		return True;
	}


	GMaterialParam::GMaterialParam(const Char* szName, GCommonMaterialParameterBuffer* pBlock, IGMaterialParam::MaterialParamType eType)
		: m_eType(eType)
		, m_pDataSlot(nullptr)
		, m_pRefBlock(pBlock)
	{
		strcpy(m_szName, szName);
	}
	GMaterialParam::~GMaterialParam()
	{
	}

	GMaterialParam::MaterialParamType GMaterialParam::GetType() const
	{
		return m_eType;
	}

	Void GMaterialParam::SetTextureSlot(GTexture** pSlot)
	{
		if (m_eType & GMaterialParam::EMPT_Texture_Mask)
		{
			if (m_pTextureSlot != nullptr)
			{
				MX_RELEASE_INTERFACE(*m_pTextureSlot);
			}

			m_pTextureSlot = pSlot;
		}
	}
	Void GMaterialParam::SetDataSlot(Void* pSlot)
	{
		if (m_eType & GMaterialParam::EMPT_Float_Mask)
		{
			m_pDataSlot = pSlot;
			if (m_pDataSlot != nullptr)
			{
				*(Float4*)m_pDataSlot = m_fValue;
				if (m_pRefBlock != nullptr)
					m_pRefBlock->FlagDataOutOfDate();
			}
		}
	}

	Void GMaterialParam::Copy(IGMaterialParam* pOther)
	{
		if (m_eType != pOther->GetType() || pOther->GetType() == IGMaterialParam::EMPT_Invalid)
		{
			assert(!"Copy Material Param Type Mismatch");
			return;
		}

		GMaterialParam* pReal = dynamic_cast<GMaterialParam*>(pOther);
		if (m_eType & EMPT_Float_Mask)//Float
		{
			m_fValue = pReal->m_fValue;
		}
		else if (m_eType & EMPT_Texture_Mask)
		{
			MX_RELEASE_INTERFACE(*m_pTextureSlot);
			if (*pReal->m_pTextureSlot != nullptr)
			{
				*m_pTextureSlot = *pReal->m_pTextureSlot;
				(*m_pTextureSlot)->AddRef();
			}
		}
	}
	Void GMaterialParam::Clone(const GMaterialParam& param)
	{
		strcpy(m_szName, param.m_szName);
		m_eType = param.m_eType;
		if (m_eType & IGMaterialParam::EMPT_Float_Mask)
		{
			m_fValue = param.m_fValue;
			if (m_pDataSlot != nullptr)
			{
				*(Float4*)m_pDataSlot = m_fValue;
			}
		}
		else if (m_eType & IGMaterialParam::EMPT_Texture_Mask)
		{
			if (m_pTextureSlot != nullptr && *m_pTextureSlot != nullptr)
				(*m_pTextureSlot)->Release();

			if (param.m_pTextureSlot != nullptr && *param.m_pTextureSlot != nullptr)
			{
				*m_pTextureSlot = *param.m_pTextureSlot;
				(*m_pTextureSlot)->AddRef();
			}
		}
	}

	Float1 GMaterialParam::GetFloat1() const
	{
		if (m_eType == MaterialParamType::EMPT_Float1)
		{
			return m_fValue[0];
		}

		Log::Output("GMaterialParam", "Param Type Is Not Float1");
		return 0;
	}
	Float2 GMaterialParam::GetFloat2() const
	{
		if (m_eType == MaterialParamType::EMPT_Float2)
		{
			return Float2(m_fValue[0], m_fValue[1]);
		}

		Log::Output("GMaterialParam", "Param Type Is Not Float2");
		return Float2();
	}
	Float3 GMaterialParam::GetFloat3() const
	{
		if (m_eType == MaterialParamType::EMPT_Float3)
		{
			return Float3(m_fValue[0], m_fValue[1], m_fValue[2]);
		}

		Log::Output("GMaterialParam", "Param Type Is Not Float3");
		return Float3();
	}
	Float4 GMaterialParam::GetFloat4() const
	{
		if (m_eType == MaterialParamType::EMPT_Float4)
		{
			return m_fValue;
		}

		Log::Output("GMaterialParam", "Param Type Is Not Float4");
		return Float4();
	}

	IGTexture* GMaterialParam::GetTexture1D() const
	{
		if (m_eType == MaterialParamType::EMPT_Texture1D)
		{
			return m_pTextureSlot != nullptr && *m_pTextureSlot != nullptr ?
				(*m_pTextureSlot) : nullptr;
		}

		Log::Output("GMaterialParam", "Param Type Is Not Texture1D");
		return nullptr;
	}
	IGTexture* GMaterialParam::GetTexture2D() const
	{
		if (m_eType == MaterialParamType::EMPT_Texture2D)
		{
			return m_pTextureSlot != nullptr && *m_pTextureSlot != nullptr ?
				(*m_pTextureSlot) : nullptr;
		}

		Log::Output("GMaterialParam", "Param Type Is Not Texture2D");
		return nullptr;
	}
	IGTexture* GMaterialParam::GetTexture3D() const
	{
		if (m_eType == MaterialParamType::EMPT_Texture3D)
		{
			return m_pTextureSlot != nullptr && *m_pTextureSlot != nullptr ?
				(*m_pTextureSlot) : nullptr;
		}

		Log::Output("GMaterialParam", "Param Type Is Not Texture3D");
		return nullptr;
	}
	IGTexture* GMaterialParam::GetTextureCube() const
	{
		if (m_eType == MaterialParamType::EMPT_TextureCube)
		{
			return m_pTextureSlot != nullptr && *m_pTextureSlot != nullptr ?
				(*m_pTextureSlot) : nullptr;
		}

		Log::Output("GMaterialParam", "Param Type Is Not TextureCube");
		return nullptr;
	}

	Boolean GMaterialParam::SetFloat1(Float1 f1Value)
	{
		if (m_eType == MaterialParamType::EMPT_Float1)
		{
			m_fValue[0] = Math::Clamp(f1Value, 0.0f, 1.0f);
			if (m_pDataSlot != nullptr)
			{
				*(Float4*)m_pDataSlot = m_fValue;

				if (m_pRefBlock != nullptr)
					m_pRefBlock->FlagDataOutOfDate();
			}
			return True;
		}

		Log::Output("GMaterialParam", "Param Type Is Not Float1");
		return False;
	}
	Boolean GMaterialParam::SetFloat2(const Float2& f2Value)
	{
		if (m_eType == MaterialParamType::EMPT_Float2)
		{
			m_fValue[0] = Math::Clamp(f2Value[0], 0.0f, 1.0f);
			m_fValue[1] = Math::Clamp(f2Value[1], 0.0f, 1.0f);
			if (m_pDataSlot != nullptr)
			{
				*(Float4*)m_pDataSlot = m_fValue;

				if (m_pRefBlock != nullptr)
					m_pRefBlock->FlagDataOutOfDate();
			}
			return True;
		}

		Log::Output("GMaterialParam", "Param Type Is Not Float2");
		return False;
	}
	Boolean GMaterialParam::SetFloat3(const Float3& f3Value)
	{
		if (m_eType == MaterialParamType::EMPT_Float3)
		{
			m_fValue[0] = Math::Clamp(f3Value[0], 0.0f, 1.0f);
			m_fValue[1] = Math::Clamp(f3Value[1], 0.0f, 1.0f);
			m_fValue[2] = Math::Clamp(f3Value[2], 0.0f, 1.0f);
			if (m_pDataSlot != nullptr)
			{
				*(Float4*)m_pDataSlot = m_fValue;

				if (m_pRefBlock != nullptr)
					m_pRefBlock->FlagDataOutOfDate();
			}
			return True;
		}

		Log::Output("GMaterialParam", "Param Type Is Not Float3");
		return False;
	}
	Boolean GMaterialParam::SetFloat4(const Float4& f4Value)
	{
		if (m_eType == MaterialParamType::EMPT_Float4)
		{
			m_fValue = Math::Clamp(f4Value, Float4(), Float4(1, 1, 1, 1));
			if (m_pDataSlot != nullptr)
			{
				*(Float4*)m_pDataSlot = m_fValue;

				if (m_pRefBlock != nullptr)
					m_pRefBlock->FlagDataOutOfDate();
			}
			return True;
		}

		Log::Output("GMaterialParam", "Param Type Is Not Float4");
		return False;
	}

	Boolean GMaterialParam::SetTexture1D(IGTexture* pTexture)
	{
		if (m_eType == MaterialParamType::EMPT_Texture1D &&
			pTexture != nullptr &&
			pTexture->GetType() == TextureType::ETexture1D)
		{
			*m_pTextureSlot = (GTexture*)pTexture;
			return True;
		}

		Log::Output("GMaterialParam", "Param Type Is Not Texture1D");
		return False;
	}
	Boolean GMaterialParam::SetTexture2D(IGTexture* pTexture)
	{
		if (m_eType == MaterialParamType::EMPT_Texture2D &&
			pTexture != nullptr &&
			pTexture->GetType() == TextureType::ETexture2D)
		{
			*m_pTextureSlot = (GTexture*)pTexture;
			return True;
		}

		Log::Output("GMaterialParam", "Param Type Is Not Texture2D");
		return False;
	}
	Boolean GMaterialParam::SetTexture3D(IGTexture* pTexture)
	{
		if (m_eType == MaterialParamType::EMPT_Texture3D&&
			pTexture != nullptr &&
			pTexture->GetType() == TextureType::ETexture3D)
		{
			*m_pTextureSlot = (GTexture*)pTexture;
			return True;
		}

		Log::Output("GMaterialParam", "Param Type Is Not Texture3D");
		return False;
	}
	Boolean GMaterialParam::SetTextureCube(IGTexture* pTexture)
	{
		if (m_eType == MaterialParamType::EMPT_TextureCube &&
			pTexture != nullptr &&
			pTexture->GetType() == TextureType::ETextureCube)
		{
			*m_pTextureSlot = (GTexture*)pTexture;
			return True;
		}

		Log::Output("GMaterialParam", "Param Type Is Not TextureCube");
		return False;
	}

	GCommonMaterial::GCommonMaterial(UInt32 UniqueID)
		: m_uUniqueID(UniqueID)
		, m_uMaterialBufferSize(0)
	{
		memset(m_arrPSO, 0, sizeof(m_arrPSO));
		memset(m_arrMaterialBufferBeginSlot, -1, sizeof(m_arrMaterialBufferBeginSlot));
		memset(m_arrMaterialTextureBeginSlot, -1, sizeof(m_arrMaterialTextureBeginSlot));
	}
	GCommonMaterial::~GCommonMaterial()
	{
		for (UInt32 u = 0; u < m_arrDyncParam.ElementCount(); u++)
		{
			delete	m_arrDyncParam[u];
		}
		m_arrDyncParam.Clear();
		for (UInt32 u = 0; u < m_arrTexParam.ElementCount(); u++)
		{
			delete	m_arrTexParam[u];
		}
		m_arrTexParam.Clear();

		for (UInt32 uPSO = 0; uPSO < (const UInt32)PSO_SIZE; uPSO++)
		{
			if (m_arrPSO[uPSO] != nullptr)
			{
				m_arrPSO[uPSO]->Release();
				delete m_arrPSO[uPSO];
				m_arrPSO[uPSO] = nullptr;
			}
		}
	}

	Array<PrimitiveRenderStage>	GCommonMaterial::GetRenderStages()
	{
		Array<PrimitiveRenderStage> RenderStages;
		if (IsDeferredRender())
		{
			RenderStages.Add(PrimitiveRenderStage::EDepthPrePass);
			RenderStages.Add(PrimitiveRenderStage::EBasePass);
		}
		else if (m_eDomain == MaterialDomain::EDecal)
		{
			RenderStages.Add(PrimitiveRenderStage::EDecalPass);
		}
		else if (m_eDomain == MaterialDomain::EWorldSpace &&
			(m_eBlendMode == MaterialBlendMode::ETranslucency || m_eBlendMode == MaterialBlendMode::EAdditive))
		{
			RenderStages.Add(PrimitiveRenderStage::EForwardTranslucent);
		}
		else if (m_eDomain == MaterialDomain::EScreenSpace)
		{
			RenderStages.Add(PrimitiveRenderStage::EForward2DPrimitive);
		}

		return RenderStages;
	}

	Void	GCommonMaterial::ParseMaterialParams(const Array<IGMaterialParam::Description>& ParamsDesc, String& dyncParams, String& texParams)
	{
		const Char* arrMaterialParamString[] = {
			"float %s : packoffset(c%d);\n",
			"float2 %s : packoffset(c%d);\n",
			"float3 %s : packoffset(c%d);\n",
			"float4 %s : packoffset(c%d);\n",
			"Tex1D(%s);\n",
			"Tex2D(%s);\n",
			"Tex3D(%s);\n",
			"TexCube(%s);\n"
		};

		UInt32 UserTextureSlotBegin = 1;
		if (m_eBlendMode == MaterialBlendMode::ETranslucency)
			UserTextureSlotBegin = 3;

		if (GetDomainType() == MaterialDomain::EPostProcess)
			UserTextureSlotBegin = 4;

		UInt32 UserSamplerSlotBegin = 1;

		UInt32 uTexCount = 0;
		UInt32 uDataOffset = 0;

		Array<GMaterialSamplerDesc> tempMaterialSamplerDesc;

		for (UInt32 u = 0; u < ParamsDesc.ElementCount(); u++)
		{
			if (ParamsDesc[u].eType & IGMaterialParam::EMPT_Float_Mask)
			{
				dyncParams += String::Format(
					arrMaterialParamString[(UInt32)log2f((Float1)ParamsDesc[u].eType)],
					ParamsDesc[u].strName.CString(),
					uDataOffset / sizeof(Float4)
				);

				uDataOffset += sizeof(Float4);
			}
			else if (ParamsDesc[u].eType & IGMaterialParam::EMPT_Texture_Mask)
			{
				texParams += String::Format(
					arrMaterialParamString[(UInt32)log2f((Float1)ParamsDesc[u].eType)],
					ParamsDesc[u].strName.CString(),
					UserTextureSlotBegin + uTexCount
				);

				GMaterialSamplerDesc SamplerDesc;
				SamplerDesc.uAddressMode = (UInt8)ParamsDesc[u].eAddressMode;
				SamplerDesc.uFilterMode = (UInt8)ParamsDesc[u].eFilterMode;
				if (SamplerDesc.uAddressMode >= TextureAddressMode::ETexAM_Count)
					SamplerDesc.uAddressMode = TextureAddressMode::ETexAM_Repeat;
				if (SamplerDesc.uFilterMode >= TextureFilterMode::ETexFM_Count)
					SamplerDesc.uFilterMode = TextureFilterMode::ETexFM_Bilinear;

				SInt32 nSamplerExist = -1;
				for (UInt32 uSampler = 0; uSampler < tempMaterialSamplerDesc.ElementCount(); uSampler++)
				{
					if (tempMaterialSamplerDesc[uSampler] == SamplerDesc)
					{
						nSamplerExist = uSampler;
						break;
					}
				}

				if (nSamplerExist == -1)
				{
					nSamplerExist = tempMaterialSamplerDesc.ElementCount();
					tempMaterialSamplerDesc.Add(SamplerDesc);

					texParams += String::Format(
						"Sampler(sampler_%d_%d);\n",
						SamplerDesc.uAddressMode,
						SamplerDesc.uFilterMode,
						nSamplerExist + UserSamplerSlotBegin);
				}

				texParams += String::Format(
					"#define s_%s sampler_%d_%d\n",
					ParamsDesc[u].strName.CString(),
					SamplerDesc.uAddressMode,
					SamplerDesc.uFilterMode);

				uTexCount++;
			}
		}
	}

	Boolean GCommonMaterial::CompileVShader(const String arrMaterialFunction[(const UInt32)MaterialFunctionType::ECount], const String& dyncParams, const String& texParams)
	{
		ASSERT_IF_FAILED(m_eDomain == MaterialDomain::EWorldSpace || m_eDomain == MaterialDomain::EDecal);

		String strVSMacroCode;
		String strVShaderCode =
			String::Format("CONSTANT_BUFFER_BEGIN(MaterialBuffer, 2)\n%sCONSTANT_BUFFER_END\n",
				dyncParams.CString());
		strVShaderCode += "\n";
		strVShaderCode += texParams.CString();
		strVShaderCode += "\n";

		if (!arrMaterialFunction[(const UInt32)MaterialFunctionType::ECustom].IsEmpty())
		{
			strVShaderCode += arrMaterialFunction[(const UInt32)MaterialFunctionType::ECustom];
			strVShaderCode += "\n";
		}

		if (!arrMaterialFunction[(const UInt32)MaterialFunctionType::EMeshParams].IsEmpty())
		{
			strVSMacroCode += arrMaterialFunctionMacro[(const UInt32)MaterialFunctionType::EMeshParams];
			strVSMacroCode += "\n";

			strVShaderCode += arrMaterialFunctionInterface[(const UInt32)MaterialFunctionType::EMeshParams];
			strVShaderCode += "\n{\n";
			strVShaderCode += arrMaterialFunction[(const UInt32)MaterialFunctionType::EMeshParams];
			strVShaderCode += "\n";
			strVShaderCode += "\n}\n";
		}

		const String strShaderTemplate =
			String::Format("%s%s", ENGINE_SHADER_DIR, "MXVVertexShaderTemplate.msr");

		IFile* pFile = IFile::Open(strShaderTemplate.CString(), IFile::EFDT_Text);
		if (pFile == nullptr) return False;

		Char* pBuf = new Char[pFile->GetFileSize() + 1]{ 0 };
		pFile->Read(pBuf, pFile->GetFileSize());

		String strCode = String::Format(pBuf,
			strVSMacroCode.CString(),
			strVShaderCode.CString());
		delete[] pBuf;
		pFile->Close();

		for (UInt32 u = 0; u <= MaterialShaderType::EVShaderTypeEnd; u++)
		{
			String strShaderName = String::Format("%s_%d", m_strUniqueName.CString(), u);

			ShaderMacro macro;
			String strDomain = String::Format("%d", GetDomainType());
			macro.AddMacro("DOMAIN", strDomain.CString());
			String strShadingModel = String::Format("%d", GetShadingModel());
			macro.AddMacro("SHADINGMODEL", strShadingModel.CString());
			String strBlendMode = String::Format("%d", (SInt32)GetBlendMode());
			macro.AddMacro("BLENDMODE", strBlendMode.CString());
			switch (u)
			{
			case MaterialShaderType::EVShaderStatic_Instance:
				macro.AddMacro("INSTANCE_RENDER", "1");
				macro.AddMacro("PRIMITIVE_TYPE", "0");
				break;
			case MaterialShaderType::EVShaderStatic:
				macro.AddMacro("INSTANCE_RENDER", "0");
				macro.AddMacro("PRIMITIVE_TYPE", "0");
				break;
			case MaterialShaderType::EVShaderSkin:
				macro.AddMacro("INSTANCE_RENDER", "0");
				macro.AddMacro("PRIMITIVE_TYPE", "1");
				break;
			}
			String strTransformCount = String::Format("%d", GVarMaxGPUBlendMatrixPerPass.nValue);
			macro.AddMacro("MAX_TRANSFORMS_COUNT", strTransformCount.CString());

			ShaderCompiledResult VS;
			if (!GRendererResource::ShaderManager->CompileShader(
				strShaderName.CString(),
				macro, VS, strCode.CString(), strCode.Length(), ShaderType::EVertexShader, True))
			{
				return False;
			}
			m_arrShaderCompiledResult[u] = VS;
		}
		return True;
	}
	Boolean GCommonMaterial::CompilePShader(const String arrMaterialFunction[(const UInt32)MaterialFunctionType::ECount], const String& dyncParams, const String& texParams)
	{
		ASSERT_IF_FAILED(m_eDomain == MaterialDomain::EWorldSpace || m_eDomain == MaterialDomain::EScreenSpace || m_eDomain == MaterialDomain::EDecal);

		String strPSMacroCode;
		String strPShaderCode =
			String::Format("CONSTANT_BUFFER_BEGIN(MaterialBuffer, 1)\n%sCONSTANT_BUFFER_END\n",
				dyncParams.CString());
		strPShaderCode += "\n";
		strPShaderCode += texParams.CString();
		strPShaderCode += "\n";

		if (!arrMaterialFunction[(const UInt32)MaterialFunctionType::ECustom].IsEmpty())
		{
			strPShaderCode += arrMaterialFunction[(const UInt32)MaterialFunctionType::ECustom];
			strPShaderCode += "\n";
		}

		strPSMacroCode += arrMaterialFunctionMacro[(const UInt32)MaterialFunctionType::ESurfaceParams];
		strPSMacroCode += "\n";

		strPShaderCode += arrMaterialFunctionInterface[(const UInt32)MaterialFunctionType::ESurfaceParams];
		strPShaderCode += "\n{\n";
		strPShaderCode += arrMaterialFunction[(const UInt32)MaterialFunctionType::ESurfaceParams];
		strPShaderCode += "\n}\n";

		const String strShaderTemplate = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVPixelShaderTemplate.msr");
		IFile* pFile = IFile::Open(strShaderTemplate.CString(), IFile::EFDT_Text);
		if (pFile == nullptr)return False;
		Char* pBuf = new Char[pFile->GetFileSize() + 1]{ 0 };
		pFile->Read(pBuf, pFile->GetFileSize());
		String strCode = String::Format(pBuf,
			strPSMacroCode.CString(),
			strPShaderCode.CString());
		delete[] pBuf;
		pFile->Close();

		UInt32 uPSCodeSlot = IsDeferredRender() ?
			MaterialShaderType::EPShaderDepth :
			MaterialShaderType::EPShaderMaterial;
		for (; uPSCodeSlot <= MaterialShaderType::EPShaderTypeEnd; uPSCodeSlot++)
		{
			String strShaderName = String::Format("%s_%d", m_strUniqueName.CString(), uPSCodeSlot);

			ShaderMacro macro;
			String strDomain = String::Format("%d", GetDomainType());
			macro.AddMacro("DOMAIN", strDomain.CString());
			String strShadingModel = String::Format("%d", GetShadingModel());
			macro.AddMacro("SHADINGMODEL", strShadingModel.CString());
			String strBlendMode = String::Format("%d", (SInt32)GetBlendMode());
			macro.AddMacro("BLENDMODE", strBlendMode.CString());

			if (IsDeferredRender())
			{
				if (uPSCodeSlot == MaterialShaderType::EPShaderDepth)
					macro.AddMacro("RENDERPASS", "0");
				else
					macro.AddMacro("RENDERPASS", "1");
			}
			else
			{
				ASSERT_IF_FAILED(uPSCodeSlot == MaterialShaderType::EPShaderMaterial);
				macro.AddMacro("RENDERPASS", "2");
			}

			ShaderCompiledResult PS;
			if (!GRendererResource::ShaderManager->CompileShader(
				strShaderName.CString(),
				macro, PS, strCode.CString(), strCode.Length(), ShaderType::EPixelShader, True))
			{
				return False;
			}

			m_arrShaderCompiledResult[uPSCodeSlot] = PS;
		}
		return True;
	}
	Boolean GCommonMaterial::CompileCShader(const String arrMaterialFunction[(const UInt32)MaterialFunctionType::ECount], const String& dyncParams, const String& texParams)
	{
		ASSERT_IF_FAILED(m_eDomain == MaterialDomain::EPostProcess);

		String strCSMacroCode;
		String strCShaderCode =
			String::Format("CONSTANT_BUFFER_BEGIN(MaterialBuffer, 1)\n%sCONSTANT_BUFFER_END\n",
				dyncParams.CString());
		strCShaderCode += "\n";
		strCShaderCode += texParams.CString();
		strCShaderCode += "\n";

		if (arrMaterialFunction[(const UInt32)MaterialFunctionType::ECustom] != nullptr)
		{
			strCShaderCode += arrMaterialFunction[(const UInt32)MaterialFunctionType::ECustom];
			strCShaderCode += "\n";
		}

		if (arrMaterialFunction[(const UInt32)MaterialFunctionType::ESurfaceParams] != nullptr)
		{
			strCSMacroCode += arrMaterialFunctionMacro[(const UInt32)MaterialFunctionType::ESurfaceParams];
			strCSMacroCode += "\n";

			strCShaderCode += arrMaterialFunctionInterface[(const UInt32)MaterialFunctionType::ESurfaceParams];
			strCShaderCode += "\n{\n";
			strCShaderCode += arrMaterialFunction[(const UInt32)MaterialFunctionType::ESurfaceParams];
			strCShaderCode += "\n}\n";
		}

		String strShaderName = String::Format("%s_%d", m_strUniqueName.CString(), MaterialShaderType::ECShader);

		const String strShaderTemplate = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVPostProcessTemplate.msr");
		IFile* pFile = nullptr;

		pFile = IFile::Open(strShaderTemplate.CString(), IFile::EFDT_Text);
		if (pFile == nullptr)return False;

		Char* pBuf = new Char[pFile->GetFileSize() + 1]{ 0 };
		pFile->Read(pBuf, pFile->GetFileSize());

		String strCode = String::Format(pBuf,
			strCSMacroCode.CString(),
			strCShaderCode.CString());

		delete[] pBuf;
		pFile->Close();

		ShaderMacro macro;
		String strDomain = String::Format("%d", GetDomainType());
		macro.AddMacro("DOMAIN", strDomain.CString());
		String strShadingModel = String::Format("%d", GetShadingModel());
		macro.AddMacro("SHADINGMODEL", strShadingModel.CString());

		ShaderCompiledResult CS;
		if (!GRendererResource::ShaderManager->CompileShader(
			strShaderName.CString(),
			macro, CS, strCode.CString(), strCode.Length(), ShaderType::EComputeShader, True))
		{
			return False;
		}
		m_arrShaderCompiledResult[MaterialShaderType::ECShader] = CS;
		return True;
	}

	Boolean GCommonMaterial::Init(const IGMaterial::Description& desc, Boolean bRecreate)
	{
		if (desc.eDomain == MaterialDomain::EInvalid ||
			desc.eFaceType == MaterialFaceType::EInvalid ||
			desc.eShadingModel == MaterialShadingModel::EInvalid ||
			desc.eBlendMode == MaterialBlendMode::EInvalid)
		{
			Log::Output("GraphicsRenderer", "Invalid Material Property");
			return False;
		}

		m_strUniqueName = desc.strUniqueName;
		m_bCanCache = desc.bCanCache;
		m_eDomain = desc.eDomain;
		m_eShadingModel = desc.eShadingModel;
		m_eBlendMode = desc.eBlendMode;
		m_eFaceType = desc.eFaceType;
		if (m_eDomain == MaterialDomain::EPostProcess)
		{
			m_eShadingModel = MaterialShadingModel::ENolighting;
			m_eBlendMode = MaterialBlendMode::EOpaque;
			m_eFaceType = MaterialFaceType::EFrontface;
		}
		else if (m_eDomain == MaterialDomain::EDecal)
		{
			m_eShadingModel = MaterialShadingModel::ENolighting;
			m_eBlendMode = MaterialBlendMode::ETranslucency;
			m_eFaceType = MaterialFaceType::EBackface;
		}

		Boolean bCached = m_bCanCache &&
			!bRecreate &&
			GRendererResource::ShaderManager->GetMaterialPropertyCache(m_strUniqueName.CString()) != nullptr;
		if (bCached)
		{
			ShaderType ST[MaterialShaderType::EMaterialShaderCount] = {
				ShaderType::EVertexShader,
				ShaderType::EVertexShader,
				ShaderType::EVertexShader,
				ShaderType::EPixelShader,
				ShaderType::EPixelShader,
				ShaderType::EComputeShader,
			};

			ShaderMacro macro;
			for (UInt32 u = 0; u < MaterialShaderType::EMaterialShaderCount; u++)
			{
				String strShaderName = String::Format("%s_%d", m_strUniqueName.CString(), u);

				GRendererResource::ShaderManager->CompileShader(
					strShaderName.CString(),
					macro,
					m_arrShaderCompiledResult[u],
					nullptr, 0,
					ST[u], //现在cache了的shader，这个类型没有任何作用
					False);
			}
		}
		else
		{
			String strMessage = String::Format("Compile Material %s", m_strUniqueName.CString());
			Log::Output("GraphicsRenderer", strMessage.CString());

			String strDyncParams;
			String strTexParams;
			ParseMaterialParams(desc.arrMaterialParams, strDyncParams, strTexParams);

			if (m_eDomain != MaterialDomain::EPostProcess)
			{
				if (m_eDomain != MaterialDomain::EScreenSpace &&
					(m_eDomain == MaterialDomain::EDecal || !desc.arrMaterialFunction[(const UInt32)MaterialFunctionType::EMeshParams].IsEmpty()) &&
					!CompileVShader(desc.arrMaterialFunction, strDyncParams, strTexParams))
				{
					return False;
				}

				if (!desc.arrMaterialFunction[(const UInt32)MaterialFunctionType::ESurfaceParams].IsEmpty() &&
					!CompilePShader(desc.arrMaterialFunction, strDyncParams, strTexParams))
				{
					return False;
				}
			}
			else if (!desc.arrMaterialFunction[(const UInt32)MaterialFunctionType::ESurfaceParams].IsEmpty() &&
				!CompileCShader(desc.arrMaterialFunction, strDyncParams, strTexParams))
			{
				return False;
			}
		}

		if (!ScanMaterialParameter(desc.arrMaterialParams) ||
			!InitPSO())
		{
			String strMessage = String::Format("Compile Material %s", m_strUniqueName.CString());
			Log::Output("GraphicsRenderer", strMessage.CString());
			return False;
		}
		return True;
	}

	Void	GCommonMaterial::Cache()
	{
		if (!m_bCanCache) return;

		GRendererResource::ShaderManager->CacheMaterialProperty(this);
	}
	Void	GCommonMaterial::BindGResource(IGCommandList* pCommandList, PrimitiveRenderStage eStage, IGPrimitive::Type eType, Boolean bInstanceRender)
	{
		PrimitiveIAFactory eIAFactory;
		TopologyType eTopology = TopologyType::ETriangle;
		switch (eType)
		{
		case MXVisual::IGPrimitive::EPrimT_Line:
			eIAFactory = PrimitiveIAFactory::EStatic;
			eTopology = TopologyType::ELine;
			break;
		case MXVisual::IGPrimitive::EPrimT_Rigid:
			eIAFactory = bInstanceRender ? PrimitiveIAFactory::EStatic_Instance : PrimitiveIAFactory::EStatic;
			eTopology = TopologyType::ETriangle;
			break;
		case MXVisual::IGPrimitive::EPrimT_Skin:
			eIAFactory = PrimitiveIAFactory::ESkin;
			eTopology = TopologyType::ETriangle;
			break;
		case MXVisual::IGPrimitive::EPrimT_Rigid2D:
			eIAFactory = PrimitiveIAFactory::EStatic2D;
			eTopology = TopologyType::ETriangle;
			break;
		}
		const UInt32 Index = CalculatePSOIndex(eStage, eIAFactory, eTopology);
		pCommandList->BindPipeline(m_arrPSO[Index]);

		for (UInt32 u = 0; u < m_arrSamplerParam.ElementCount(); u++)
		{
			IGSampler* sampler = GRendererResource::GetSampler(
				(TextureAddressMode)m_arrSamplerParam[u].uAddressMode,
				(TextureFilterMode)m_arrSamplerParam[u].uFilterMode, 16);

			pCommandList->BindSampler(ShaderType::EPixelShader,
				m_arrSamplerParam[u].uBindingPoint, sampler);
		}
	}

	Boolean	GCommonMaterial::ConvertMaterialParameterBlock(GCommonMaterialParameterBuffer* pParameterBlock)
	{
		pParameterBlock->m_pRefMaterial = this;

		for (UInt32 uOldParam = 0; uOldParam < pParameterBlock->m_arrTexParam.ElementCount(); uOldParam++)
		{
			GMaterialParam* pParam = dynamic_cast<GMaterialParam*>(pParameterBlock->m_arrTexParam[uOldParam]);
			pParam->SetTextureSlot(nullptr);
		}
		pParameterBlock->m_arrDyncParam.Clear();
		pParameterBlock->m_arrTexParam.Clear();

		pParameterBlock->m_MaterialBufferData.SetElementCount(GetMaterialBufferSize());
		if (!pParameterBlock->Init(GetMaterialBufferSize()))
		{
			return False;
		}

		if (GetMaterialBufferSize() > 0)
		{
		}

		for (UInt32 u = 0; u < m_arrDyncParam.ElementCount(); u++)
		{
			UInt8* pSlot = &pParameterBlock->m_MaterialBufferData[sizeof(Float4) * u];
			GMaterialParam* pCloneParam = nullptr;

			if (pCloneParam == nullptr)
			{
				pCloneParam = new GMaterialParam(m_arrDyncParam[u]->GetName(), pParameterBlock, m_arrDyncParam[u]->GetType());
				pCloneParam->Clone(*m_arrDyncParam[u]);
			}
			pCloneParam->SetDataSlot(pSlot);
			pParameterBlock->m_arrDyncParam.Add(pCloneParam);
		}

		pParameterBlock->m_arrTextureSlot.SetElementCount(m_arrTexParam.ElementCount());
		for (UInt32 u = 0; u < m_arrTexParam.ElementCount(); u++)
		{
			pParameterBlock->m_arrTextureSlot[u] = nullptr;
			GMaterialParam* pCloneParam = nullptr;

			if (pCloneParam == nullptr)
			{
				pCloneParam = new GMaterialParam(m_arrTexParam[u]->GetName(), pParameterBlock, m_arrTexParam[u]->GetType());
				pCloneParam->Clone(*m_arrTexParam[u]);
			}
			pCloneParam->SetTextureSlot(&(pParameterBlock->m_arrTextureSlot[u]));
			pParameterBlock->m_arrTexParam.Add(pCloneParam);
		}
		return True;
	}
}
