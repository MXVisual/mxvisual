#pragma once
#ifndef _G_COMMON_RENDERER_
#define _G_COMMON_RENDERER_
#include "GCommonShaderManager.h"
#include "MXGraphicsRenderer.h"
#include "MXGraphicsResource.h"
#include "MXMemory.h"

namespace MXVisual
{
#define ENGINE_SHADER_DIR "./Shaders/"
#define	MAX_FACE_IN_ONE_MAP  8

	Matrix4 GenPerspectiveMatrix(Float1 fFovW, Float1 fAspectWH, Float1 fZNear, Float1 fZFar);
	Matrix4 GenOrthographicMatrix(Float1 fWidth, Float1 fHeight, Float1 fZNear, Float1 fZFar);
	Matrix4 GenOrthographicMatrix(Float1 fLeft, Float1 fTop, Float1 fRight, Float1 fBottom, Float1 fZNear, Float1 fZFar);

	enum WriteMask
	{
		EWM_NoChannel = 0,
		EWM_RedChannel = 0x01,
		EWM_GreenChannel = 0x02,
		EWM_BlueChannel = 0x04,
		EWM_AlphaChannel = 0x08,
		EWM_RGBChannel = EWM_RedChannel | EWM_GreenChannel | EWM_BlueChannel,
		EWM_AllChannel = EWM_RedChannel | EWM_GreenChannel | EWM_BlueChannel | EWM_AlphaChannel
	};

#pragma region CONSTANT_BUFFER_DEFINE
	struct CBSCommon
	{
		Matrix4	WorldToView;
		Matrix4 ViewToClip;
		Matrix4 WorldToClip;
		Matrix4 ClipToView;
		Matrix4 ViewToWorld;

		Float4 TimeParam;
		Float4 ViewportParam;
		Float4 RenderTargetParam;

		Float4 CameraParam;
	};

	struct CBSForwardLight
	{
		Float4	f4LightColor;
		Float4	f4LightDirection;
		Float4	IndirectLightColor_Intensity;
		Float1	EnableSSR;
		Float2	Dump;
		UInt32	EnvReflectionCount;
		Float4	EnvReflectionCenter_InvRadius2;
	};

	struct CBSDeferredLight
	{
		Float4	Color_Intensity;
		Float4	Position_Attenuation;
		Float4	Direction_Range;
		Float4	Angle_XXX;
		UInt32	LightCombineFlags;//High 1 bit ShadowDebug; Low 3. bit Is Cast Shadow; Low 2 bit Light Type;
	};

	struct CBSShadowMapCommon
	{
		Matrix4	ShadowMapWorldToClip[MAX_FACE_IN_ONE_MAP];
		Float4	ShadowMapUVTransform[MAX_FACE_IN_ONE_MAP];
		UInt32	ShadowMapDebugFlag_FaceCount;//High Bit Is Cascade Debug
		Float1	ShadowMapDepthOffset;
		Float2	ShadowMapInvResolution;
	};
#pragma endregion

	struct GViewport
	{
		UInt32 uX, uY, uWidth, uHeight;
		Float1 fMinDepth, fMaxDepth;
	};

	struct ViewInfo
	{
		IGDirectionLight* MainLight;

		GViewport	ViewPort;
		GViewport	PresentViewPort;
		Region		ViewPortScissor;
		Region		PresentViewPortScissor;

		//Time
		Float1		CurrentTime;
		Float1		DeltaTime;
		//ViewFrustum
		Matrix4		WorldToView;
		Matrix4		ViewToClip;
		Float3		ViewFrustumPosition;
		Float1		ZNear;
		Float1		ZFar;
		Float3		FrustumNearCorners[4];
		Float3		FrustumFarCorners[4];
		//Forward Main Light
		Float4		LightColor;
		Float4		LightDirection;
	};

	enum class TopologyType : UInt8
	{
		ETriangle,
		ELine,
		EPoint,
		ECount,
	};

	struct GRasterizerState
	{
		TopologyType eType;

	};

	struct GPipeline;
	struct GRenderPass
	{
		UInt32		RTVCount = 0;
		PixelFormat RTVFormats[8] = {
			EPF_Invalid, EPF_Invalid, EPF_Invalid , EPF_Invalid,
			EPF_Invalid, EPF_Invalid, EPF_Invalid , EPF_Invalid };
		PixelFormat DSVFormat = EPF_Invalid;
	};
	struct IGBuffer : public IGResource {};
	struct IGSampler : public IGResource {};

	class IGCommandList
	{
	public:
		virtual Void	SetViewport(const GViewport& Viewport) = 0;
		virtual Void	SetScissor(const Region& Scissor) = 0;

		virtual Void	BindPipeline(GPipeline* Pipeline) = 0;
		virtual Void	BindRenderTarget(
			Boolean bClearDepthTarget,
			const IGTexture* DT, UInt32 uDTSubresourceIdx,
			Boolean bClearColorTarget,
			const IGTexture* RT0, UInt32 uRT0SubresourceIdx,
			const IGTexture* RT1 = nullptr, UInt32 uRT1SubresourceIdx = 0,
			const IGTexture* RT2 = nullptr, UInt32 uRT2SubresourceIdx = 0,
			const IGTexture* RT3 = nullptr, UInt32 uRT3SubresourceIdx = 0,
			const IGTexture* RT4 = nullptr, UInt32 uRT4SubresourceIdx = 0,
			const IGTexture* RT5 = nullptr, UInt32 uRT5SubresourceIdx = 0,
			const IGTexture* RT6 = nullptr, UInt32 uRT6SubresourceIdx = 0,
			const IGTexture* RT7 = nullptr, UInt32 uRT7SubresourceIdx = 0) = 0;

		virtual Void	BindVertexBuffer(UInt32 uSlot, IGBuffer* pBuffer, UInt32 uStride) = 0;
		virtual Void	BindIndexBuffer(IGBuffer* pBuffer, Boolean b32Bit) = 0;

		virtual Void	BindShaderResource(ShaderType eType, UInt32 uSlot, const IGTexture* pTexture) = 0;
		virtual Void	BindUnorderResource(ShaderType eType, UInt32 uSlot, const  IGTexture* pTexture, UInt32 uSubresourceIdx) = 0;
		virtual Void	BindConstantBuffer(ShaderType eType, UInt32 uSlot, const IGBuffer* pBuffer) = 0;
		virtual Void	BindSampler(ShaderType eType, UInt32 uSlot, const IGSampler* pSampler) = 0;

		virtual Void	Draw(UInt32 uVertexCount, UInt32 uInstanceCount, Boolean bTriangleNotLine) = 0;
		virtual Void	DrawIndex(UInt32 uIndexCount, UInt32 uIndexOffset, UInt32 uInstanceCount, Boolean bTriangleNotLine) = 0;

		virtual Void	Dispatch(UInt32 X, UInt32 Y, UInt32 Z) = 0;

		virtual Void	Flush() = 0;

		virtual Void	CopyResource(IGTexture* pDest, IGTexture* pSrc) = 0;

		virtual Boolean	Map(IGTexture* pTexture, UInt32 uSubResouce, Boolean bRead, MapRegion& region) = 0;
		virtual Void	Unmap(IGTexture* pTexture, UInt32 uSubResouce) = 0;
		virtual Boolean	Map(IGBuffer* pBuffer, Boolean bRead, MapRegion& region) = 0;
		virtual Void	Unmap(IGBuffer* pBuffer) = 0;

		virtual Boolean	SubmitData(IGTexture* pTexture, UInt32 uSubResouce, UInt32 uSize, const Void* pData) = 0;
		virtual Boolean	SubmitData(IGBuffer* pBuffer, UInt32 uSize, const Void* pData) = 0;
	};

	namespace GRenderer
	{
		Void	WaitUntilRendered();

		Void	LockThread();
		Void	UnlockThread();

		IGCommandList*	CommandList();
		Void			Execute();

		Boolean InitAPI(const Char* szAdapterName);
		Void	ReleaseAPI();
	};

#if defined(DEBUG)
	Void DebugPushEvent(const Char* eventName);
	Void DebugPopEvent();

#define MARK_EVENT_BEGIN(e) DebugPushEvent(#e);
#define MARK_EVENT_END() 	DebugPopEvent();
#else
#define MARK_EVENT_BEGIN(e) 
#define MARK_EVENT_END()  
#endif

	enum class PrimitiveRenderStage : UInt8
	{
		EDepthPrePass,
		EBasePass,
		EDecalPass,
		EForwardTranslucent,
		EForward2DPrimitive,
		ECount
	};

	enum class PrimitiveIAFactory : UInt8
	{
		EStatic,
		EStatic_Instance,
		ESkin,
		EStatic2D,
		ECount
	};

	enum class ColorTargetType : UInt8
	{
		ETAAHistorySceneColor,//Only 3D

		ESceneColor0,
		ESceneColor1,

		EGBufferA,//Albedo_Alpha
		EGBufferB,//Roughness Metallic  AO
		EGBufferC,//Normal_PrimitiveData
		EGBufferD,//CustomData

		EScreenSpaceShadow,
		ECount,
	};

	enum class DepthTargetType : UInt8
	{
		ESceneDepth0,
		ESceneDepth1,
		ECount
	};

	enum class BufferType : UInt8
	{
		EVertexBuffer,
		EIndexBuffer,
		EConstantBuffer,
		ECount
	};

	enum class DrawInstanceType : UInt8
	{
		EOpaque,
		EDecal,
		ETranslucency,
		E2D,
		ECount
	};

	class GPrimitive;
	class GPrimitiveTransformBuffer;
	class GCommonMaterial;
	class GCommonMaterialParameterBuffer;
	struct DrawInstance : public MemoryBlock
	{
		GPrimitive* pPrimitive;
		GPrimitiveTransformBuffer* pTransformBlock;
		GCommonMaterial*	pMaterial;
		GCommonMaterialParameterBuffer* pParameterBlock;
		UInt32 uVertexOffset;
		UInt32 uVertexCount;
		Region regionClip;

		DrawInstance()
			: pPrimitive(nullptr)
			, pMaterial(nullptr)
			, pParameterBlock(nullptr)
			, uVertexOffset(0)
			, uVertexCount(0)
			, regionClip()
		{}
		DrawInstance(GPrimitive* pPrim, GPrimitiveTransformBuffer* pTBlock,
			GCommonMaterial* pMtl, GCommonMaterialParameterBuffer* pPBlock,
			UInt32 uVOffset, UInt32 uVCount, Region inRegionClip)
			: pPrimitive(pPrim)
			, pTransformBlock(pTBlock)
			, pMaterial(pMtl)
			, pParameterBlock(pPBlock)
			, uVertexOffset(uVOffset)
			, uVertexCount(uVCount)
			, regionClip(inRegionClip)
		{}
	};

	struct DirectLightInfo
	{
		IGDirectionLight*	pDirLight;
		IGPointLight*		pPtLight;
		IGSpotLight*		pSpLight;

		IGBuffer*			pLightBuffer;

		IGTexture*			pShadowDepthMap;
		UInt32				uFaceCountInMap;
		IGBuffer*			pScreenSpaceShadowBuffer;

		IGBuffer*			ViewBuffers[MAX_FACE_IN_ONE_MAP];
		Matrix4				WorldToLights[MAX_FACE_IN_ONE_MAP];
		Matrix4				LightToClips[MAX_FACE_IN_ONE_MAP];

		Void Reset()
		{
			uFaceCountInMap = 0;
			pDirLight = nullptr;
			pPtLight = nullptr;
			pSpLight = nullptr;
		}
	};

	namespace GRendererResource
	{
		Boolean						HasRawShaderFolder();
		Boolean						InitAPIDependencyResource();
		Void						ReleaseAPIDependencyResource();

		IGBuffer*					CreateBuffer(BufferType eType, Boolean bDynamic, UInt32 uSize);
		IGSampler*					GetSampler(TextureAddressMode eAddressMode, TextureFilterMode eFilterMode, UInt32 uMaxAnisotropicLevel);

		extern Float2						BackbufferSize;
		extern GStatistics					FrameStatistics;
		extern IGTexture*					ColorTargets[(const UInt32)ColorTargetType::ECount];
		extern IGTexture*					DepthTargets[(const UInt32)DepthTargetType::ECount];
		extern IGTexture*					AtmosphericScatterLUT;

		extern IGBuffer*					BufferCommon;
		extern IGBuffer*					BufferForwardLight;

		extern GPipeline*					PSOScreenSpaceShadow;
		extern GPipeline*					PSODeferredLighting;

		extern IGPrimitiveTransformBuffer*	IdentityTransformBuffer;
		extern IGSampler*					EngineCommonSampler;
		extern GCommonShaderManager*		ShaderManager;
		extern ShaderCompiledResult			DefaultScreenQuadVertexShader;

		extern UInt32						DirectLightCount;
		extern Array<DirectLightInfo>		DirectLights;

		extern UInt32						CastingShadowLightCount;
		extern Array<DirectLightInfo>		CastingShadowLights;

		extern Array<IGPrimitive*>			RegisteredPrimitives;
		extern Array<DrawInstance>			DrawInstanceList[(const UInt8)DrawInstanceType::ECount];
		extern Array<IGReflectionProbe*>	ReflectionProbeList;
		//Array<IGMaterialInstance*>	PostProcessInstance[EPPIP_Count];
	};

	namespace Pass
	{
		extern ViewInfo MainViewInfo;
	}
}

#endif