#include "GBoundingVolume.h"
#include "MXMath.h"
#include "MXArray.h"

namespace MXVisual
{
	Boolean PlaneIntersect(const Float3& f3PlanePos, const Float3& f3PlaneN, const Float3& f3Begin, const Float3& f3Direction, Float1& t)
	{
		if (Math::Abs(Math::DotProduct(f3PlaneN, f3Direction)) < Constant_MIN_FLOAT)
		{
			return False;
		}

		t = (Math::DotProduct(f3PlanePos, f3PlaneN) - Math::DotProduct(f3Begin, f3PlaneN)) / Math::DotProduct(f3Direction, f3PlaneN);
		return True;
	}

	GVolumnAABB::GVolumnAABB()
		: m_f3Max(-Constant_MAX_FLOAT, -Constant_MAX_FLOAT, -Constant_MAX_FLOAT)
		, m_f3Min(Constant_MAX_FLOAT, Constant_MAX_FLOAT, Constant_MAX_FLOAT)
	{

	}
	GVolumnAABB::~GVolumnAABB()
	{
	}
	Void GVolumnAABB::Merge(const IGBoundingVolume* pOther)
	{

	}
	Boolean GVolumnAABB::CalIntersect(const Float3& f3Begin, const Float3& f3Direction, Float1 fDistance, const Matrix4* pTransform, Array<Float3>* pOutIntersectPoint) const
	{
		Float4 f4Min = Math::Combine(m_f3Min, 1.0f);
		Float4 f4Max = Math::Combine(m_f3Max, 1.0f);

		if (pTransform)
		{
			Float4 newmin = Math::MatrixTransform(f4Min, *pTransform);
			Float4 newmax = Math::MatrixTransform(f4Max, *pTransform);
			f4Min = Math::Min(newmin, newmax);
			f4Max = Math::Max(newmin, newmax);
		}

		Float3 p0[6] = {
			Float3(f4Min[0], 0.0f, 0.0f),
			Float3(f4Max[0], 0.0f, 0.0f),
			Float3(0.0f, f4Min[1], 0.0f),
			Float3(0.0f, f4Max[1], 0.0f),
			Float3(0.0f, 0.0f, f4Min[2]),
			Float3(0.0f, 0.0f, f4Max[2])
		};

		Float3 n[6] = {
			Math::AXIS_X,
			Math::AXIS_X_Inverse,
			Math::AXIS_Y,
			Math::AXIS_Y_Inverse,
			Math::AXIS_Z,
			Math::AXIS_Z_Inverse
		};

		Boolean bIntersection = False;
		Float1 t = -1.0f;
		for (UInt32 u = 0; u < 6; u++)
		{
			t = -1.0f;
			if (PlaneIntersect(p0[u], n[u], f3Begin, f3Direction, t) &&
				t > 0.0f &&
				t < fDistance)
			{
				Float3 IntersectionPt = Float3(f3Begin) + Float3(f3Direction) * t;
				//Intersection Point Is Not On Plane
				if (IntersectionPt[0] > f4Max[0] ||
					IntersectionPt[1] > f4Max[1] ||
					IntersectionPt[2] > f4Max[2] ||
					IntersectionPt[0] < f4Min[0] ||
					IntersectionPt[1] < f4Min[1] ||
					IntersectionPt[2] < f4Min[2])
				{
					continue;
				}

				//Intersection
				bIntersection = True;
				if (pOutIntersectPoint)
				{
					pOutIntersectPoint->Add(IntersectionPt);
				}
				else
				{
					break;
				}
			}
		}

		return bIntersection;
	}
	Void	GVolumnAABB::Reset()
	{
		m_f3Max = Float3(0.0f, 0.0f, 0.0f);
		m_f3Min = Float3(0.0f, 0.0f, 0.0f);
	}
	Void	GVolumnAABB::AddVertexPosition(const Float3& f3Point)
	{
		m_f3Max = Math::Max(m_f3Max, f3Point);
		m_f3Min = Math::Min(m_f3Min, f3Point);
	}
	Float3  GVolumnAABB::GetVolumePoint(UInt32 uIndex) const
	{
		if (uIndex > 7)return Float3();

		Float3 Res[8] = {
			Float3(m_f3Min[0], m_f3Max[1], m_f3Min[2]),
			Float3(m_f3Max[0], m_f3Max[1], m_f3Min[2]),
			Float3(m_f3Min[0], m_f3Min[1], m_f3Min[2]),
			Float3(m_f3Max[0], m_f3Min[1], m_f3Min[2]),

			Float3(m_f3Min[0], m_f3Max[1], m_f3Max[2]),
			Float3(m_f3Max[0], m_f3Max[1], m_f3Max[2]),
			Float3(m_f3Min[0], m_f3Min[1], m_f3Max[2]),
			Float3(m_f3Max[0], m_f3Min[1], m_f3Max[2])
		};

		return Res[uIndex];
	}

}