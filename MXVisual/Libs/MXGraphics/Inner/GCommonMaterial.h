#pragma once
#ifndef _G_COMMON_MATERIAL_
#define _G_COMMON_MATERIAL_
#include "MXCommon.h"
#include "GCommonShaderManager.h"
#include "GRenderer.h"
#include "MXGraphicsResource.h"
#include "MXConstant.h"
#include "map"

namespace MXVisual
{
	class GTexture;
	class GCommonMaterialParameterBuffer;

	class GMaterialParam : public IGMaterialParam
	{
	public:
		GMaterialParam(const Char* szName, GCommonMaterialParameterBuffer* pBlock, MaterialParamType eType);
		~GMaterialParam();

		const Char*	GetName() const override { return m_szName; }

		MaterialParamType GetType() const override;

		Float1 GetFloat1() const override;
		Float2 GetFloat2() const override;
		Float3 GetFloat3() const override;
		Float4 GetFloat4() const override;

		IGTexture* GetTexture1D() const override;
		IGTexture* GetTexture2D() const override;
		IGTexture* GetTexture3D() const override;
		IGTexture* GetTextureCube() const override;

		Boolean SetFloat1(Float1 f1Value);
		Boolean SetFloat2(const Float2& f2Value);
		Boolean SetFloat3(const Float3& f3Value);
		Boolean SetFloat4(const Float4& f4Value);

		Boolean SetTexture1D(IGTexture* pTexture);
		Boolean SetTexture2D(IGTexture* pTexture);
		Boolean SetTexture3D(IGTexture* pTexture);
		Boolean SetTextureCube(IGTexture* pTexture);

		Void	SetTextureSlot(GTexture** pSlot);
		Void	SetDataSlot(Void* pSlot);

		Void	Copy(IGMaterialParam* pOther);
		Void	Clone(const GMaterialParam& param);
	protected:
		Char m_szName[Constant_MAX_PATH];
		IGMaterialParam::MaterialParamType m_eType;
		GCommonMaterialParameterBuffer* m_pRefBlock;

		struct//For Float
		{
			Float4 m_fValue;
			Void* m_pDataSlot;
		};
		struct//For Texture
		{
			GTexture** m_pTextureSlot;
		};
	};

	struct GMaterialSamplerDesc
	{
		UInt8 uBindingPoint;
		UInt8 uAddressMode;
		UInt8 uFilterMode;

		Boolean operator==(const GMaterialSamplerDesc& other) const
		{
			return other.uBindingPoint == uBindingPoint &&
				other.uAddressMode == uAddressMode &&
				other.uFilterMode == uFilterMode;
		}
	};

	class GCommonMaterial : public IGMaterial
	{
	public:
		GCommonMaterial(UInt32 UniqueID);
		virtual ~GCommonMaterial();

		UInt32	GetUniqueID() const { return m_uUniqueID; }
		const Char* GetUniqueName() const { return m_strUniqueName.CString(); }
		UInt32	GetVersion() const { return m_uVersion; }

		MaterialDomain			GetDomainType() const override { return m_eDomain; }
		MaterialShadingModel	GetShadingModel() const override { return m_eShadingModel; }
		MaterialFaceType		GetFaceType() const override { return m_eFaceType; }
		MaterialBlendMode		GetBlendMode() const override { return m_eBlendMode; }
		UInt32		GetParameterCount() const override { return m_arrDyncParam.ElementCount() + m_arrTexParam.ElementCount(); }

		Boolean Init(const IGMaterial::Description& desc, Boolean bRecreate);

		enum MaterialShaderType
		{
			EVShaderStatic = 0,
			EVShaderStatic_Instance,
			EVShaderSkin,
			EVShaderTypeEnd = EVShaderSkin,

			EPShaderDepth,
			EPShaderMaterial,
			EPShaderTypeEnd = EPShaderMaterial,

			ECShader,//For PostProcess
			ECShaderTypeEnd = ECShader,
			EMaterialShaderCount
		};

		UInt32	GetMaterialBufferSize() const { return m_uMaterialBufferSize; }
		UInt32	GetMaterialBufferBeginSlot(ShaderType eShaderType) const { return m_arrMaterialBufferBeginSlot[(const UInt8)eShaderType]; }
		UInt32	GetMaterialTextureBeginSlot(ShaderType eShaderType) const { return m_arrMaterialTextureBeginSlot[(const UInt8)eShaderType]; }

		Boolean	ConvertMaterialParameterBlock(GCommonMaterialParameterBuffer* pParameterBlock);

		Void	BindGResource(IGCommandList* pCommandList, PrimitiveRenderStage eStage, IGPrimitive::Type eType, Boolean bInstanceRender);
		Void	Cache();
	protected:
		UInt32	m_uUniqueID;
		String	m_strUniqueName;
		UInt32	m_uVersion;
		Boolean m_bCanCache;

		MaterialDomain m_eDomain;
		MaterialShadingModel m_eShadingModel;
		MaterialFaceType m_eFaceType;
		MaterialBlendMode m_eBlendMode;

		UInt32 m_uMaterialBufferSize;

#define PSO_SIZE (const UInt8)PrimitiveRenderStage::ECount *\
				 (const UInt8)PrimitiveIAFactory::ECount * \
				 (const UInt8)TopologyType::ECount

		GPipeline* m_arrPSO[PSO_SIZE];
		inline UInt32 CalculatePSOIndex(PrimitiveRenderStage eStage, PrimitiveIAFactory eIA, TopologyType eTopology)
		{
			return (const UInt8)eStage * (const UInt8)PrimitiveIAFactory::ECount * (const UInt8)TopologyType::ECount +
				(const UInt8)eIA * (const UInt8)TopologyType::ECount +
				(const UInt8)eTopology;
		}

		ShaderCompiledResult m_arrShaderCompiledResult[MaterialShaderType::EMaterialShaderCount];

		SInt32	m_arrMaterialBufferBeginSlot[(const UInt32)ShaderType::ECount];
		SInt32	m_arrMaterialTextureBeginSlot[(const UInt32)ShaderType::ECount];

		Array<GMaterialParam*> m_arrDyncParam;
		Array<GMaterialParam*> m_arrTexParam;
		Array<GMaterialSamplerDesc> m_arrSamplerParam;
	protected:
		inline Boolean IsDeferredRender()
		{
			return m_eDomain == MaterialDomain::EWorldSpace &&
				(m_eBlendMode == MaterialBlendMode::EOpaque ||
					m_eBlendMode == MaterialBlendMode::EMasked ||
					m_eBlendMode == MaterialBlendMode::EMaskTranslucency);
		}
		Array<PrimitiveRenderStage>	GetRenderStages();

		Void	ParseMaterialParams(const Array<IGMaterialParam::Description>& ParamsDesc, String& dyncParams, String& texParams);

		Boolean CompileVShader(const String arrMaterialFunction[(const UInt32)MaterialFunctionType::ECount], const String& dyncParams, const String& texParams);
		Boolean CompilePShader(const String arrMaterialFunction[(const UInt32)MaterialFunctionType::ECount], const String& dyncParams, const String& texParams);
		Boolean CompileCShader(const String arrMaterialFunction[(const UInt32)MaterialFunctionType::ECount], const String& dyncParams, const String& texParams);

		virtual Boolean InitPSO() = 0;
		virtual Boolean ScanMaterialParameter(const Array<IGMaterialParam::Description>& arrMaterialParams) = 0;
	};

	class GCommonMaterialParameterBuffer : public IGMaterialParameterBuffer
	{
	public:
		GCommonMaterialParameterBuffer(UInt32 UniqueID);
		virtual ~GCommonMaterialParameterBuffer();

		UInt32	GetUniqueID() const { return m_uUniqueID; }
		virtual Boolean Init(UInt32 uBufferSize);

		IGMaterialParam*	GetParamByName(const Char* szName) const;
		IGMaterialParam*	GetParamByIndex(UInt32 uParam) const;
		Void				FlagDataOutOfDate() { m_bBufferUpdateToDate = False; }

		virtual Void		BindGResource(IGCommandList* pCommandList);

		friend Boolean	GCommonMaterial::ConvertMaterialParameterBlock(GCommonMaterialParameterBuffer* pParameterBlock);
	protected:
		UInt32					m_uUniqueID;
		GCommonMaterial*		m_pRefMaterial;
		Boolean					m_bBufferUpdateToDate;
		IGBuffer*				m_pBuffer;

		Array<IGMaterialParam*> m_arrDyncParam;
		Array<UInt8>			m_MaterialBufferData;

		Array<IGMaterialParam*> m_arrTexParam;
		Array<GTexture*>		m_arrTextureSlot;
	};
}

#endif