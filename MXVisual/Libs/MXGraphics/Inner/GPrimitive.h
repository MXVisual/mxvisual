#pragma once
#ifndef _G_PRIMITIVE_
#define _G_PRIMITIVE_
#include "MXGraphicsResource.h"
#include "MXGraphicsRenderer.h"
#include "MXConstant.h"
#include "GRenderer.h"
#include "d3d11.h"

namespace MXVisual
{
	class GPrimitive : public IGPrimitive
	{
	public:
		GPrimitive(UInt32 UniqueID);
		virtual ~GPrimitive();

		virtual Boolean Init(Boolean bDynamic,
			UInt32 uVertexCapacity,
			UInt32 uIndexCapacity,
			String* pStrError = nullptr) = 0;

		virtual Boolean	CalIntersect(const Float3& f3Begin, const Float3& f3Direction, Float1 fDistance, const Matrix4* pTransform = nullptr, Array<Float3>* pOutIntersectPoint = nullptr) const = 0;

		IGPrimitive::Type GetType() { return m_eType; }

		Void UpdateIndexBuffer(const UInt32* pIndex, UInt32 uIndexCount) override;
		virtual Void UpdateBoundingVolume(const IGVertex* pVertex, UInt32 uVertexCount) = 0;

		UInt32 GetVertexCapacity() const override { return m_uVertexCapacity; }
		UInt32 GetIndexCapacity() const override { return m_uIndexCapacity; }

		UInt32 GetUniqueID() const { return m_uUniqueID; }

		virtual Void	BindGResource(IGCommandList* pCommandList);
	protected:
		UInt32 m_uUniqueID;

		IGBoundingVolume*	m_pBoundingVolume;
		IGPrimitive::Type	m_eType;

		Boolean				m_bDynamic;
		IGBuffer*	m_pVertexBuffer;
		Boolean				m_b32BitIndex;
		IGBuffer*	m_pIndexBuffer;

		UInt32 m_uVertexCapacity;
		UInt32 m_uIndexCapacity;

		UInt32 m_uVertexCount;
		UInt32 m_uIndexCount;
	};

	template<typename VertexType, IGPrimitive::Type ePrimType>
	class GPrimitiveTemplate : public GPrimitive
	{
	public:
		GPrimitiveTemplate(UInt32 UniqueID)
			: GPrimitive(UniqueID)
		{
			m_eType = ePrimType;
		}
		~GPrimitiveTemplate()
		{
		}

		Boolean Init(Boolean bDynamic,
			UInt32 uVertexCapacity,
			UInt32 uIndexCapacity,
			String* pStrError = nullptr);
		Boolean	CalIntersect(const Float3& f3Begin, const Float3& f3Direction, Float1 fDistance, const Matrix4* pTransform = nullptr, Array<Float3>* pOutIntersectPoint = nullptr) const;

		Void UpdateBoundingVolume(const IGVertex* pVertex, UInt32 uVertexCount);
		Void UpdateVertexBuffer(const IGVertex* pVertex, UInt32 uVertexCount)
		{
			if (pVertex == nullptr || m_pVertexBuffer == nullptr)return;

			GRenderer::WaitUntilRendered();
			GRenderer::LockThread();
			if (m_bDynamic)
			{
				MapRegion region;
				if (SUCCEEDED(GRenderer::CommandList()->Map(m_pVertexBuffer, False, region)))
				{
					memcpy(region.pAddress, pVertex, uVertexCount * sizeof(VertexType));
					GRenderer::CommandList()->Unmap(m_pVertexBuffer);
				}
#if defined(DEBUG)
				else
				{
					Log::Output("GraphicsRenderer", "UpdateVertexBuffer Failed");
				}
#endif
			}
			else
			{
				GRenderer::CommandList()->SubmitData(m_pVertexBuffer, uVertexCount * sizeof(VertexType), pVertex);
			}
			GRenderer::UnlockThread();

			m_uVertexCount = uVertexCount;
			UpdateBoundingVolume(pVertex, uVertexCount);
		}

		Void	BindGResource(IGCommandList* pCommandList) override
		{
			pCommandList->BindVertexBuffer(0, m_pVertexBuffer, sizeof(VertexType));
			GPrimitive::BindGResource(pCommandList);
		}
	};

	typedef GPrimitiveTemplate<GVertexRigid, IGPrimitive::EPrimT_Line> GPrimitiveLine;
	typedef GPrimitiveTemplate<GVertexRigid, IGPrimitive::EPrimT_Rigid> GPrimitiveRigid;
	typedef GPrimitiveTemplate<GVertexSkin, IGPrimitive::EPrimT_Skin> GPrimitiveSkin;
	typedef GPrimitiveTemplate<GVertexRigid2D, IGPrimitive::EPrimT_Rigid2D> GPrimitive2D;

	class GPrimitiveTransformBuffer : public IGPrimitiveTransformBuffer
	{
	public:
		UInt32 GetUniqueID() const { return m_uUniqueID; }

		GPrimitiveTransformBuffer(UInt32 UniqueID);
		~GPrimitiveTransformBuffer();

		//Create Double Transform Size, LocalToWorld & WorldToLocalT
		Boolean Init(Boolean bForInstance, UInt32 uMaxTransform);

		Void	SetTransforms(const Matrix4* pTransforms, UInt32 uTransforms) override;

		Boolean	IsInstance() const { return m_bForInstance; }
		UInt32	GetTransformsCapacity() const { return m_uMaxTransforms; }
		UInt32	GetTransformsCount() const { return m_uTransforms; }

		Void	BindGResource(IGCommandList* pCommandList);
	protected:
		UInt32 m_uUniqueID;
		IGBuffer* m_pBuffer;
		UInt32 m_uMaxTransforms;
		UInt32 m_uTransforms;
		Boolean m_bForInstance;
	};
}

#endif