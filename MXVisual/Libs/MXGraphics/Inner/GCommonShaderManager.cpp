#include "GCommonShaderManager.h"
#include "MXMath.h"
#include "MXSystem.h"
#include "GMaterial.h"
#include "MXFile.h"

#include <unordered_map>

namespace MXVisual
{
	Void ShaderMacro::Clear()
	{
		m_arrMacros.Clear();
	}
	Void ShaderMacro::AddMacro(const Char* szMacro, const Char* szValue)
	{
		//同时为空或同时不为空
		ASSERT_IF_FAILED((szMacro == nullptr && szValue == nullptr) || (szMacro != nullptr && szValue != nullptr));
		m_arrMacros.Add(MacroElement{ szMacro , szValue });
	}
	const Void* ShaderMacro::GetMacroData() const
	{
		return m_arrMacros.GetRawData();
	}
	UInt32		ShaderMacro::GetMacroCount() const
	{
		return m_arrMacros.ElementCount();
	}
	const Char* ShaderMacro::GetMacroName(UInt32 uIndex) const
	{
		return m_arrMacros[uIndex].szName;
	}
	const Char* ShaderMacro::GetMacroValue(UInt32 uIndex) const
	{
		return m_arrMacros[uIndex].szValue;
	}

	GCommonShaderManager::GCommonShaderManager(Boolean bShaderDebug)
		: m_bUpdateToDate(True)
	{
		m_strCachePath = ".\\.CachesDefault";

		m_bUpdateToDate = True;
#if MX_BUILD_EDITOR
		m_bShaderDebug = bShaderDebug;
#endif
	}
	GCommonShaderManager::~GCommonShaderManager()
	{
		if (!m_bUpdateToDate)
		{
			//TODO!! Cache Version
			SaveMaterialCache();
			SaveShaderCache();
		}

		for (auto& it : m_mapMaterialCache)
		{
			delete it.second;
		}
		m_mapMaterialCache.clear();

		for (auto& it : m_mapShaderCache)
		{
			delete it.second;
		}
		m_mapShaderCache.clear();
	}

	Void GCommonShaderManager::SaveShaderCache()
	{
		System::CreateFolder(m_strCachePath.CString());

		//TODO!!
		//Write Caches Version

		for (auto it = m_mapShaderCache.begin(); it != m_mapShaderCache.end(); it++)
		{
			String strCacheFileName = String::Format("%s\\%x-%x-%x-%x.bin", m_strCachePath.CString(),
				it->first.ID[0],
				it->first.ID[1],
				it->first.ID[2],
				it->first.ID[3]);

			IFile* pCacheFile = IFile::New(strCacheFileName.CString(), IFile::EFDT_Binary);

			pCacheFile->Write(it->second->Code, it->second->Size);

			pCacheFile->Close();
		}
	}

	Void GCommonShaderManager::CacheShader(const Char* ShaderName, UInt8* ShaderCode, UInt32 ShaderCodeSize)
	{
		if (ShaderCode == nullptr || ShaderCodeSize == 0)return;

		Math::MD5 md5 = Math::CalculateMD5_128(ShaderName);
		if (m_mapShaderCache.find(md5) == m_mapShaderCache.end())
		{
			ShaderCache* cache = new ShaderCache(ShaderCodeSize, ShaderCode);
			m_mapShaderCache[md5] = cache;
		}
		else
		{
			ShaderCache* cache = m_mapShaderCache[md5];
			MX_DELETE_ARRAY(cache->Code);
			cache->Size = ShaderCodeSize;
			cache->Code = ShaderCode;
		}
		m_bUpdateToDate = False;
	}
	Void GCommonShaderManager::GetShaderCache(const Char* ShaderName, ShaderCompiledResult& result)
	{
		Math::MD5 md5 = Math::CalculateMD5_128(ShaderName);

		auto it = m_mapShaderCache.find(md5);
		if (it != m_mapShaderCache.end())
		{
			ShaderCache* cache = it->second;
			result.pCode = cache->Code;
			result.uSize = cache->Size;
		}
		else
		{
			IFile* pFile = IFile::Open(
				String::Format("%s\\%x-%x-%x-%x.bin", m_strCachePath.CString(),
					md5.ID[0],
					md5.ID[1],
					md5.ID[2],
					md5.ID[3]).CString(),
				IFile::EFDT_Binary);

			if (pFile != nullptr)
			{
				UInt8* pBuf = new UInt8[pFile->GetFileSize()]{ 0 };
				UInt32 uBinarySize = pFile->Read(pBuf, pFile->GetFileSize());
				ShaderCache* cache = new ShaderCache(uBinarySize, pBuf);
				pFile->Close();

				m_mapShaderCache[md5] = cache;

				result.pCode = cache->Code;
				result.uSize = cache->Size;
			}
			else
			{
				result.uSize = 0;
			}
		}
	}

	Void GCommonShaderManager::SaveMaterialCache()
	{
		System::CreateFolder(m_strCachePath.CString());

		for (auto it = m_mapMaterialCache.begin(); it != m_mapMaterialCache.end(); it++)
		{
			String strCacheFileName = String::Format("%s\\%x-%x-%x-%x.prop", m_strCachePath.CString(),
				it->first.ID[0],
				it->first.ID[1],
				it->first.ID[2],
				it->first.ID[3]);

			IFile* pCacheFile = IFile::New(strCacheFileName.CString(), IFile::EFDT_Binary);

			pCacheFile->Write(it->second, sizeof(MaterialCache));

			pCacheFile->Close();
		}
	}

	Void GCommonShaderManager::CacheMaterialProperty(const GCommonMaterial* pMaterial)
	{
		if (pMaterial == nullptr)return;

		Math::MD5 md5 = Math::CalculateMD5_128(pMaterial->GetUniqueName());
		if (m_mapMaterialCache.find(md5) == m_mapMaterialCache.end())
		{
			MaterialCache* cache = new MaterialCache;
			cache->uDomain = (const UInt8)pMaterial->GetDomainType();
			cache->uShadingModel = (const UInt8)pMaterial->GetShadingModel();
			cache->uFaceType = (const UInt8)pMaterial->GetFaceType();
			cache->uBlendMode = (const UInt8)pMaterial->GetBlendMode();

			m_mapMaterialCache[md5] = cache;
		}
		else
		{
			MaterialCache* cache = m_mapMaterialCache[md5];
			cache->uDomain = (const UInt8)pMaterial->GetDomainType();
			cache->uShadingModel = (const UInt8)pMaterial->GetShadingModel();
			cache->uFaceType = (const UInt8)pMaterial->GetFaceType();
			cache->uBlendMode = (const UInt8)pMaterial->GetBlendMode();
		}

		m_bUpdateToDate = False;
	}
	const GCommonShaderManager::MaterialCache* GCommonShaderManager::GetMaterialPropertyCache(const Char* MaterialHashName)
	{
		Math::MD5 md5 = Math::CalculateMD5_128(MaterialHashName);
		if (m_mapMaterialCache.find(md5) != m_mapMaterialCache.end())
		{
			return m_mapMaterialCache[md5];
		}
		else
		{
			IFile* pFile = IFile::Open(
				String::Format("%s\\%x-%x-%x-%x.prop", m_strCachePath.CString(),
					md5.ID[0],
					md5.ID[1],
					md5.ID[2],
					md5.ID[3]).CString(),
				IFile::EFDT_Binary);

			if (pFile != nullptr)
			{
				MaterialCache* pCache = new MaterialCache;
				pFile->Read(pCache, sizeof(MaterialCache));

				m_mapMaterialCache[md5] = pCache;

				pFile->Close();
				return pCache;
			}
		}
		return nullptr;
	}
}