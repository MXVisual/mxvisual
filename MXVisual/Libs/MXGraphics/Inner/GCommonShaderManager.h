#pragma once
#ifndef _G_COMMON_SHADER_MANAGER_
#define _G_COMMON_SHADER_MANAGER_
#include "MXCommon.h"
#include "MXMath.h"
#include "MXGraphicsResource.h"
#include "MXConstant.h"
#include "map"

namespace MXVisual
{
	enum class ShaderType : UInt8
	{
		EVertexShader,
		EPixelShader,
		EComputeShader,
		ECount
	};

	class ShaderMacro
	{
	public:
		Void Clear();
		Void AddMacro(const Char* szMacro, const Char* szValue);
		const Void* GetMacroData() const;

		UInt32		GetMacroCount() const;
		const Char* GetMacroName(UInt32 uIndex) const;
		const Char* GetMacroValue(UInt32 uIndex) const;
	protected:
		struct MacroElement
		{
			const Char* szName = nullptr;
			const Char* szValue = nullptr;
		};
		Array<MacroElement> m_arrMacros;
	};

	struct ShaderCompiledResult
	{
		const Void* pCode = nullptr;
		UInt32		uSize = 0;

		Boolean IsValid() const
		{
			return pCode != nullptr && uSize > 0;
		}
	};

	struct MD5IDCmp
	{
		Boolean operator()(const Math::MD5& a, const Math::MD5& b) const
		{
			for (UInt32 u = 0; u < 4; u++)
			{
				if (a.ID[u] != b.ID[0])
					return a.ID[u] < b.ID[u];
			}
			return False;
		}
	};

	class GCommonShaderManager
	{
	public:
		GCommonShaderManager(Boolean bShaderDebug = False);
		~GCommonShaderManager();

		struct MaterialCache
		{
			UInt8 uDomain;
			UInt8 uShadingModel;
			UInt8 uFaceType;
			UInt8 uBlendMode;
		};

		Void CacheMaterialProperty(const class GCommonMaterial* pMaterial);
		const MaterialCache* GetMaterialPropertyCache(const Char* MaterialHashName);

		//Return Compile Result
		virtual	Boolean CompileShader(
			const Char* szShaderName,
			ShaderMacro& macros,
			ShaderCompiledResult& result,
			const Void* pShaderCode, UInt32 uShaderSize,
			ShaderType eShaderType,
			Boolean bRecompile) = 0;

	protected:
		Void SaveShaderCache();
		Void SaveMaterialCache();

		Void CacheShader(const Char* ShaderName, UInt8*, UInt32 ShaderCodeSize);
		Void GetShaderCache(const Char* ShaderName, ShaderCompiledResult& result);

#if MX_BUILD_EDITOR
		Boolean	m_bShaderDebug;
#endif
		Boolean m_bUpdateToDate;
		String m_strCachePath;

		struct ShaderCache
		{
			UInt32 Size;
			UInt8* Code;

			ShaderCache() = delete;
			ShaderCache(const ShaderCache&) = delete;
			ShaderCache& operator=(const ShaderCache&) = delete;

			ShaderCache(UInt32 uSize, UInt8* pCode)
				: Size(uSize)
				, Code(pCode)
			{

			}
			~ShaderCache()
			{
				MX_DELETE_ARRAY(Code);
				Size = 0;
			}
		};
		std::map<Math::MD5, ShaderCache*, MD5IDCmp> m_mapShaderCache;
		std::map<Math::MD5, MaterialCache*, MD5IDCmp> m_mapMaterialCache;
	};
}

#endif