#include "GRadialBlurGodRayPass.h"
#include "MXFile.h"
#include "MXMath.h"
#include "GShaderManager.h"
#include "MXMath.h"
#include "MXCommon.h"

namespace MXVisual
{
	struct CBSGodRayProperty
	{
		UInt32	SampleCount;//SampleCount
		Float1	InvSampleCount;
		Float1	StepRate;
		Float1	Attenuation;
		Float2	LightPositionInScreen;
		Float1	Weight;
		Float1	RadialRange;
	};

	namespace
	{
		GPipeline	PSO;
		IGBuffer*	Buffer = nullptr;
		IGTexture*	RadialBlurTargets[2];
	}

	Boolean GRadialBlurGodRayPass::Init(UInt32 uInnerRTWidth, UInt32 uInnerRTHeight)
	{
		ShaderCompiledResult PS;
		ShaderMacro macro;
		if (!GRendererResource::ShaderManager->CompileShader(
			"MXVDeferredPassRadialBlurGodRay.msr",
			macro, PS,
			nullptr, 0, ShaderType::EPixelShader, False))
		{
			const String PixelShaderName = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVDeferredPassRadialBlurGodRay.msr");
			IFile * pShaderFile = IFile::Open(PixelShaderName.CString(), IFile::EFDT_Text);
			if (!pShaderFile)return False;

			String strDomain = String::Format("%d", MaterialDomain::EPostProcess);
			macro.AddMacro("DOMAIN", strDomain.CString());
			Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
			pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

			String strCode = pBuf;

			delete[] pBuf;
			pShaderFile->Close();

			GRendererResource::ShaderManager->CompileShader(
				"MXVDeferredPassRadialBlurGodRay.msr",
				macro, PS, strCode.CString(), strCode.Length(), ShaderType::EPixelShader, False);
		}

		GRenderPass RP;
		RP.RTVCount = 1;
		RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]->GetFormat();

		PSO.InitForRender(
			PrimitiveIAFactory::EStatic2D, 
			TopologyType::ETriangle, RP,
			GRendererResource::RasterizerState(),
			GRendererResource::DepthStencilState(DEPTH_ALWAYS, False),
			GRendererResource::BlendState(BLENDSTATE_ADDITIVE),
			&GRendererResource::DefaultScreenQuadVertexShader, &PS);

		Buffer = GRendererResource::CreateBuffer(
			BufferType::EConstantBuffer,
			True,
			sizeof(CBSGodRayProperty));

		IGTexture::Description tex_desc;
		tex_desc.bDynamic = False;
		tex_desc.bGenerateMipMap = False;
		tex_desc.bTargetable = True;
		tex_desc.eType = TextureType::ETexture2D;
		tex_desc.eFormat = EPF_R16G16B16A16_F;
		tex_desc.uWidth = uInnerRTWidth;
		tex_desc.uHeight = uInnerRTHeight;
		tex_desc.uDepth = 1;
		tex_desc.uMipLevel = 1;
		tex_desc.f4ClearValue = Float4(0.0f, 0.0f, 0.0f, 0.0f);

		RadialBlurTargets[0] = GraphicsResource::CreateTexture(tex_desc);
		RadialBlurTargets[1] = GraphicsResource::CreateTexture(tex_desc);

		return RadialBlurTargets[0] != nullptr && RadialBlurTargets[1] != nullptr;
	}
	Void	GRadialBlurGodRayPass::Release()
	{
		PSO.Release();
	}

	Void GRadialBlurGodRayPass::Render(
		IGCommandList* pCommandList,
		const IGTexture*	Source,
		const IGTexture*	Dest,
		UInt32 RepeatTime,
		UInt32 SampleCount,
		Float1 Attenuation,
		Float1 Strength,
		Float1 RadialRange,
		Float2 LightPositionInScreen)
	{
		CBSGodRayProperty data;
		data.SampleCount = SampleCount;
		data.InvSampleCount = 1.0f / SampleCount;
		data.StepRate = 1.0f;
		data.Attenuation = Attenuation;
		data.LightPositionInScreen = LightPositionInScreen;
		data.Weight = Strength;
		data.RadialRange = RadialRange;

		MapRegion region;
		pCommandList->Map(Buffer, False, region);
		memcpy(region.pAddress, &data, sizeof(CBSGodRayProperty));
		pCommandList->Unmap(Buffer);

		pCommandList->BindPipeline(&PSO);

		pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 1, Buffer);
		pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);

		const IGTexture* srv = Source;
		const IGTexture* rtv = RadialBlurTargets[0];
		for (UInt32 u = 0; u < RepeatTime; u++)
		{
			pCommandList->BindRenderTarget(False, nullptr, 0, True, rtv, 0);
			pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, srv);
			pCommandList->Draw(3, 1, True);

			rtv = RadialBlurTargets[(u + 1) % 2];
			srv = RadialBlurTargets[u % 2];
		}
		rtv = Dest;

		pCommandList->BindRenderTarget(False, nullptr, 0, False, rtv, 0);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, srv);
		pCommandList->Draw(3, 1, True);
	}
}
