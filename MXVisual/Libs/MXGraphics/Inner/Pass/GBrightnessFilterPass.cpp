#include "GBrightnessFilterPass.h"
#include "MXFile.h"
#include "MXMath.h"
#include "GShaderManager.h"
#include "GBlurPass.h"

namespace MXVisual
{
	struct CBSBrightnessFilterProperty
	{
		Float1	FilterOutBrightness;
		Float1	FilterOutDepth;
	};

	namespace
	{
		GPipeline PSO;
		IGBuffer* Buffer = nullptr;
	}
	Boolean	GBrightnessFilterPass::Init()
	{
		ShaderCompiledResult PS;
		ShaderMacro macro;
		if (!GRendererResource::ShaderManager->CompileShader(
			"MXVDeferredPassBrightnessFilter.msr",
			macro, PS, nullptr, 0, ShaderType::EPixelShader, False))
		{
			const String PixelShaderName = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVDeferredPassBrightnessFilter.msr");
			IFile * pShaderFile = IFile::Open(PixelShaderName.CString(), IFile::EFDT_Text);
			if (!pShaderFile)return False;


			String strDomain = String::Format("%d", MaterialDomain::EPostProcess);
			macro.AddMacro("DOMAIN", strDomain.CString());
			Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
			pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

			String strCode = pBuf;

			delete[] pBuf;
			pShaderFile->Close();

			GRendererResource::ShaderManager->CompileShader(
				"MXVDeferredPassBrightnessFilter.msr",
				macro, PS, strCode.CString(), strCode.Length(), ShaderType::EPixelShader, False);
		}

		GRenderPass RP;
		RP.RTVCount = 1;
		RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]->GetFormat();
		if (!PSO.InitForRender(
			PrimitiveIAFactory::EStatic2D, 
			TopologyType::ETriangle, RP,
			GRendererResource::RasterizerState(),
			GRendererResource::DepthStencilState(DEPTH_ALWAYS, False),
			GRendererResource::BlendState(),
			&GRendererResource::DefaultScreenQuadVertexShader, &PS))
		{
			return False;
		}

		Buffer = GRendererResource::CreateBuffer(
			BufferType::EConstantBuffer,
			True, sizeof(CBSBrightnessFilterProperty));

		return True;
	}
	Void	GBrightnessFilterPass::Release()
	{
		PSO.Release();
	}

	Void GBrightnessFilterPass::Render(
		IGCommandList* pCommandList,
		const IGTexture* SceneColor,
		const IGTexture* SceneDepth,
		const IGTexture* PresentTarget,
		Float1 fFilterOutBrightness,
		Float1 fFilterOutDepth)
	{
		CBSBrightnessFilterProperty data;
		data.FilterOutBrightness = fFilterOutBrightness;
		data.FilterOutDepth = fFilterOutDepth;

		MapRegion region;
		pCommandList->Map(Buffer, False, region);
		memcpy(region.pAddress, &data, sizeof(CBSBrightnessFilterProperty));
		pCommandList->Unmap(Buffer);

		pCommandList->BindRenderTarget(False,
			nullptr, 0,
			False, PresentTarget, 0);

		pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 1, Buffer);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, SceneColor);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 1, SceneDepth);
		pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);
		pCommandList->BindPipeline(&PSO);
		pCommandList->Draw(3, 1, True);
	}
}
