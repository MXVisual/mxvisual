#include "GSSAOPass.h"
#include "GShaderManager.h"
#include "MXCommon.h"

namespace MXVisual
{
#define AO_SAMPLE_COUNT 32
#define AO_NOISE_WIDTH_COUNT 8
#define AO_NOISE_HEIGHT_COUNT 8

	struct CBSSSAO
	{
		Float1 SampleCoreRange;
		Float3 _dump;
		Float4 SampleDir[AO_SAMPLE_COUNT];
		Float4 NoiseDir[AO_NOISE_WIDTH_COUNT * AO_NOISE_HEIGHT_COUNT];
	};
	namespace
	{
		GPipeline PSO;
		IGBuffer* BufferSSAO = nullptr;
		CBSSSAO data;
	}

	Boolean	GSSAOPass::Init()
	{
		for (UInt32 u = 0; u < AO_SAMPLE_COUNT; u++)
		{
			data.SampleDir[u] = Float4(
				Math::Random01() * 2.0f - 1.0f,
				Math::Random01() * 2.0f - 1.0f,
				Math::Random01(),
				0.0f);
		}

		for (UInt32 u = 0; u < AO_NOISE_WIDTH_COUNT * AO_NOISE_HEIGHT_COUNT; u++)
		{
			data.NoiseDir[u] = Float4(
				Math::Random01() * 2.0f - 1.0f,
				Math::Random01() * 2.0f - 1.0f, 0, 0);
		}

		ShaderCompiledResult PS;
		ShaderMacro macro;
		if (!GRendererResource::ShaderManager->CompileShader(
			"MXVDeferredPassSSAO.msr",
			macro, PS, nullptr, 0, ShaderType::EPixelShader, False))
		{
			const String PixelShaderName = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVDeferredPassSSAO.msr");

			IFile * pShaderFile = IFile::Open(PixelShaderName.CString(), IFile::EFDT_Text);
			if (!pShaderFile)return False;

			String strDomain = String::Format("%d", MaterialDomain::EPostProcess);
			macro.AddMacro("DOMAIN", strDomain.CString());
			String strSampleCount = String::Format("%d", AO_SAMPLE_COUNT);
			macro.AddMacro("AO_SAMPLE_COUNT", strSampleCount.CString());
			String strNoiseWidth = String::Format("%d", AO_NOISE_WIDTH_COUNT);
			macro.AddMacro("AO_NOISE_WIDTH_COUNT", strNoiseWidth.CString());
			String strNoiseHeight = String::Format("%d", AO_NOISE_HEIGHT_COUNT);
			macro.AddMacro("AO_NOISE_HEIGHT_COUNT", strNoiseHeight.CString());

			Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
			pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

			String strCode = pBuf;

			delete[] pBuf;
			pShaderFile->Close();

			GRendererResource::ShaderManager->CompileShader(
				"MXVDeferredPassSSAO.msr",
				macro, PS, strCode.CString(), strCode.Length(), ShaderType::EPixelShader, False);
		}

		GRenderPass RP;
		RP.RTVCount = 1;
		RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::EGBufferB]->GetFormat();

		if (!PSO.InitForRender(
			PrimitiveIAFactory::EStatic2D, 
			TopologyType::ETriangle, RP,
			GRendererResource::RasterizerState(),
			GRendererResource::DepthStencilState(DEPTH_ALWAYS, False),
			GRendererResource::BlendState(BLENDSTATE_OFF, EWM_BlueChannel),
			&GRendererResource::DefaultScreenQuadVertexShader, &PS))
		{
			return False;
		}

		BufferSSAO = GRendererResource::CreateBuffer(
			BufferType::EConstantBuffer,
			True, sizeof(CBSSSAO));

		return BufferSSAO != nullptr;
	}
	Void	GSSAOPass::Release()
	{
		PSO.Release();
		MX_RELEASE_INTERFACE(BufferSSAO);
	}

	Void GSSAOPass::Render(
		IGCommandList* pCommandList,
		const IGTexture* SceneDepth,
		const IGTexture* Normal_ShadingModel,
		const IGTexture* PresentTarget,
		Float1 fSampleCoreRange,
		Float1 fIndirectLightIntensity,
		Float1 fAttenuation)
	{
		data.SampleCoreRange = fSampleCoreRange;

		MapRegion region;
		pCommandList->Map(BufferSSAO, False, region);
		memcpy(region.pAddress, &data, sizeof(CBSSSAO));
		pCommandList->Unmap(BufferSSAO);

		pCommandList->BindRenderTarget(True,
			nullptr, 0,
			False, PresentTarget, 0);

		pCommandList->BindPipeline(&PSO);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, SceneDepth);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 1, Normal_ShadingModel);

		pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);
		pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 1, BufferSSAO);
		pCommandList->Draw(3, 1, True);
	}
}
