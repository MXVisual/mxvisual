#include "GIndirectLightPass.h"
#include "GShaderManager.h"
#include "GTexture.h"
#include "MXCommon.h"

namespace MXVisual
{
	const UInt32 MAX_ENV_REFLECTION = 4;

	struct CBSIndirectLight
	{
		Float4	f4IndirectLightColor;
		Float1	fEnableSSR;
		Float2	Dump;
		UInt32	EnvReflectionCount;
		Float4	EnvReflectionCenter_InvRadius2[MAX_ENV_REFLECTION];
	};

	static GPipeline	PSO;
	static IGBuffer*	Buffer = nullptr;

	Boolean	GIndirectLightPass::Init()
	{
		ShaderCompiledResult PS;
		ShaderMacro macro;
		if (!GRendererResource::ShaderManager->CompileShader(
			"MXVDeferredPassIndirectLight.msr",
			macro, PS, nullptr, 0, ShaderType::EPixelShader, False))
		{
			const String PixelShaderName = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVDeferredPassIndirectLight.msr");

			IFile * pShaderFile = IFile::Open(PixelShaderName.CString(), IFile::EFDT_Text);
			if (!pShaderFile)return False;

			String strDomain = String::Format("%d", MaterialDomain::EPostProcess);
			macro.AddMacro("DOMAIN", strDomain.CString());
			String strEnvCount = String::Format("%d", MAX_ENV_REFLECTION);
			macro.AddMacro("MAX_ENV_REFLECTION", strEnvCount.CString());

			Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
			pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

			String strCode = pBuf;

			delete[] pBuf;
			pShaderFile->Close();

			GRendererResource::ShaderManager->CompileShader(
				"MXVDeferredPassIndirectLight.msr",
				macro, PS, strCode.CString(), strCode.Length(), ShaderType::EPixelShader, False);
		}

		GRenderPass RP;
		RP.RTVCount = 1;
		RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]->GetFormat();

		if (!PSO.InitForRender(
			PrimitiveIAFactory::EStatic2D, 
			TopologyType::ETriangle, RP,
			GRendererResource::RasterizerState(),
			GRendererResource::DepthStencilState(DEPTH_ALWAYS, False),
			GRendererResource::BlendState(),
			&GRendererResource::DefaultScreenQuadVertexShader, &PS))
		{
			return False;
		}

		Buffer = GRendererResource::CreateBuffer(
			BufferType::EConstantBuffer,
			True, sizeof(CBSIndirectLight));

		return True;
	}
	Void	GIndirectLightPass::Release()
	{
		PSO.Release();
	}

	Void GIndirectLightPass::Render(
		IGCommandList* pCommandList,
		const IGTexture* SceneDepth,
		const IGTexture* SceneColor,
		const IGTexture* GBufferAlbedo_Opacity,
		const IGTexture* GBufferR_M_AO_X,
		const IGTexture* GBufferNormal_ShadingModel,
		const IGTexture* PresentTarget,
		Float4 f4IndirectLightColor,
		Boolean bEnableSSR,
		Array<IGReflectionProbe*>& REProbeList)
	{
		pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 0, GRendererResource::BufferCommon);

		CBSIndirectLight data;
		data.f4IndirectLightColor = f4IndirectLightColor;
		data.fEnableSSR = bEnableSSR ? 1.0f : 0.0f;

		data.EnvReflectionCount = Math::Min(REProbeList.ElementCount(), MAX_ENV_REFLECTION);
		for (UInt32 u = 0; u < data.EnvReflectionCount; u++)
		{
			data.EnvReflectionCenter_InvRadius2[u] =
				Math::Combine(REProbeList[u]->GetProbePosition(),
					1.0f / (REProbeList[u]->GetProbeRange() * REProbeList[u]->GetProbeRange()));
		}

		MapRegion region;
		pCommandList->Map(Buffer, False, region);
		memcpy(region.pAddress, &data, sizeof(CBSIndirectLight));
		pCommandList->Unmap(Buffer);

		pCommandList->BindRenderTarget(
			False, nullptr, 0,
			False, PresentTarget, 0);

		pCommandList->BindPipeline(&PSO);

		pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, SceneDepth);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 1, SceneColor);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 2, GBufferAlbedo_Opacity);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 3, GBufferR_M_AO_X);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 4, GBufferNormal_ShadingModel);

		for (UInt32 u = 0; u < data.EnvReflectionCount; u++)
		{
			GTexture* pTex = (GTexture*)REProbeList[u]->GetReflectionTexture();
			if (pTex != nullptr)
			{
				pCommandList->BindShaderResource(ShaderType::EPixelShader, u + 5, pTex);
			}
		}

		pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);
		pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 1, Buffer);
		pCommandList->Draw(3, 1, True);
	}
}
