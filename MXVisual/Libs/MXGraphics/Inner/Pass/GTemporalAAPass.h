#pragma once
#ifndef _G_TEMPORAL_PASS_
#define _G_TEMPORAL_PASS_
#include "GRenderer.h"

namespace MXVisual
{
	struct GTemporalAAPass
	{
		static Boolean	Init();
		static Void		Release();

		static Void Render(
			const IGTexture* HistorySceneColor,
			const IGTexture* SceneColor);
	};
}

#endif