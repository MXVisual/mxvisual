#include "GTonemapPass.h"
#include "GShaderManager.h"

namespace MXVisual
{
	struct CBSTonemap
	{
		Float1 Exposure;
		Float1 AverageLuminance;
	};
	namespace
	{
		GPipeline			PSO;
		IGBuffer*	BufferTonemap = nullptr;
	}

	Boolean	GTonemapPass::Init()
	{
		ShaderCompiledResult PS;
		ShaderMacro macro;
		if (!GRendererResource::ShaderManager->CompileShader(
			"MXVDeferredPassTonemap.msr",
			macro, PS, nullptr, 0, ShaderType::EPixelShader, False))
		{
			const String PixelShaderName = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVDeferredPassTonemap.msr");

			IFile * pShaderFile = IFile::Open(PixelShaderName.CString(), IFile::EFDT_Text);
			if (!pShaderFile)return False;

			String strDomain = String::Format("%d", MaterialDomain::EPostProcess);
			macro.AddMacro("DOMAIN", strDomain.CString());

			Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
			pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

			String strCode = pBuf;

			delete[] pBuf;
			pShaderFile->Close();

			GRendererResource::ShaderManager->CompileShader(
				"MXVDeferredPassTonemap.msr",
				macro, PS, strCode.CString(), strCode.Length(), ShaderType::EPixelShader, False);
		}

		GRenderPass RP;
		RP.RTVCount = 1;
		RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]->GetFormat();

		if (!PSO.InitForRender(
			PrimitiveIAFactory::EStatic2D,
			TopologyType::ETriangle, RP,
			GRendererResource::RasterizerState(),
			GRendererResource::DepthStencilState(DEPTH_ALWAYS, False),
			GRendererResource::BlendState(),
			&GRendererResource::DefaultScreenQuadVertexShader, &PS))
		{
			return False;
		}

		BufferTonemap = GRendererResource::CreateBuffer(
			BufferType::EConstantBuffer, True, sizeof(CBSTonemap));

		return True;
	}
	Void		GTonemapPass::Release()
	{
		PSO.Release();
	}

	Void GTonemapPass::Render(
		IGCommandList* pCommandList,
		const IGTexture* SceneColor,
		const IGTexture* PresentTarget,
		Float1 fExposure,
		Float1 fAverageLuminance)
	{
		CBSTonemap data;
		data.Exposure = fExposure;
		data.AverageLuminance = fAverageLuminance;

		MapRegion region;
		if (pCommandList->Map(BufferTonemap, False, region))
		{
			memcpy(region.pAddress, &data, sizeof(CBSTonemap));
			pCommandList->Unmap(BufferTonemap);
		}

		pCommandList->BindRenderTarget(False,
			nullptr, 0,
			False, PresentTarget, 0);

		pCommandList->BindPipeline(&PSO);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, SceneColor);

		pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);
		pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 1, BufferTonemap);

		pCommandList->Draw(3, 1, True);
	}
}
