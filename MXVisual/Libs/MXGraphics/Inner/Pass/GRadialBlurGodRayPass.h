#pragma once
#ifndef _G_DIRECTION_BLUR_GOD_RAY_PASS_
#define _G_DIRECTION_BLUR_GOD_RAY_PASS_
#include "GRenderer.h"

namespace MXVisual
{
	struct GRadialBlurGodRayPass
	{
		static Boolean Init(UInt32 uInnerRTWidth, UInt32 uInnerRTHeight);
		static Void	Release();

		static Void	Render(
			IGCommandList* pCommandList,
			const IGTexture*	Source,
			const IGTexture*	Dest,
			UInt32 RepeatTime,
			UInt32 SampleCount,
			Float1 Attenuation,
			Float1 Strength,
			Float1 RadialRange,
			Float2 LightPositionInScreen);//0~1
	};
}

#endif