#include "GHeightFogPass.h"
#include "GShaderManager.h"
#include "MXCommon.h"

namespace MXVisual
{
	struct CBSHeightFogProperty
	{
		Float3 Color;
		Float1 Density;

		Float1 StartDistance;
		Float1 DistanceFalloff;
		Float1 Height;
		Float1 HeightFalloff;
	};
	namespace
	{
		GPipeline		PSO;
		IGBuffer*		Buffer = nullptr;
	}

	Boolean	GHeightFogPass::Init()
	{
		ShaderCompiledResult PS;
		ShaderMacro macro;
		if (!GRendererResource::ShaderManager->CompileShader(
			"MXVDeferredPassHeightFog.msr",
			macro, PS, nullptr, 0, ShaderType::EPixelShader, False))
		{
			const String PixelShaderName = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVDeferredPassHeightFog.msr");

			IFile * pShaderFile = IFile::Open(PixelShaderName.CString(), IFile::EFDT_Text);
			if (!pShaderFile)return False;

			String strDomain = String::Format("%d", MaterialDomain::EPostProcess);
			macro.AddMacro("DOMAIN", strDomain.CString());
			Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
			pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

			String strCode = pBuf;

			delete[] pBuf;
			pShaderFile->Close();

			GRendererResource::ShaderManager->CompileShader(
				"MXVDeferredPassHeightFog.msr",
				macro, PS, strCode.CString(), strCode.Length(), ShaderType::EPixelShader, False);
		}

		GRenderPass RP;
		RP.RTVCount = 1;
		RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]->GetFormat();

		if (!PSO.InitForRender(
			PrimitiveIAFactory::EStatic2D, 
			TopologyType::ETriangle, RP,
			GRendererResource::RasterizerState(),
			GRendererResource::DepthStencilState(DEPTH_EQUAL, False),
			GRendererResource::BlendState(
				BLENDSTATE_BLEND,
				EWM_RedChannel | EWM_GreenChannel | EWM_BlueChannel),
			&GRendererResource::DefaultScreenQuadVertexShader, &PS))
		{
			return False;
		}

		Buffer = GRendererResource::CreateBuffer(
			BufferType::EConstantBuffer, True, sizeof(CBSHeightFogProperty));

		return True;
	}
	Void	GHeightFogPass::Release()
	{
		PSO.Release();
	}

	Void GHeightFogPass::Render(
		IGCommandList* pCommandList,
		const IGTexture* SceneDepth,
		const IGTexture* PresentTarget,
		Float3 Color,
		Float1 Density,
		Float1 StartDistance,
		Float1 DistanceFalloff,
		Float1 Height,
		Float1 HeightFalloff)
	{
		CBSHeightFogProperty data;
		data.Color = Color;
		data.Density = Density;
		data.StartDistance = StartDistance;
		data.DistanceFalloff = DistanceFalloff;
		data.Height = Height;
		data.HeightFalloff = HeightFalloff;

		MapRegion region;
		pCommandList->Map(Buffer, False, region);
		memcpy(region.pAddress, &data, sizeof(CBSHeightFogProperty));
		pCommandList->Unmap(Buffer);;

		pCommandList->BindRenderTarget(False,
			nullptr, 0,
			False, PresentTarget, 0);

		pCommandList->BindPipeline(&PSO);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, SceneDepth);
		pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);
		pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 1, Buffer);
		pCommandList->Draw(3, 1, True);
	}
}
