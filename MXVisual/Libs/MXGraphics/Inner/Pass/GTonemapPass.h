#pragma once
#ifndef _G_TONEMAP_PASS_
#define _G_TONEMAP_PASS_
#include "GRenderer.h"

namespace MXVisual
{
	struct GTonemapPass
	{
		static Boolean	Init();
		static Void		Release();

		static Void Render(
			IGCommandList* pCommandList,
			const IGTexture* SceneColor,
			const IGTexture* PresentTarget,
			Float1 fExposure,
			Float1 fAverageLuminance);
	};
}

#endif