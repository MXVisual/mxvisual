#pragma once
#ifndef _G_BLOOM_PASS_
#define _G_BLOOM_PASS_
#include "GRenderer.h"

namespace MXVisual
{
	struct GBloomPass
	{
		static Boolean	Init();
		static Void		Release();

		static Void Render(
			IGCommandList* pCommandList,
			const IGTexture* SceneColor,
			const IGTexture* PresentTarget,
			UInt32 uTargetWidth,
			UInt32 uTargetHeight,
			Float1 fExporedLuminance
		);
	};
}

#endif