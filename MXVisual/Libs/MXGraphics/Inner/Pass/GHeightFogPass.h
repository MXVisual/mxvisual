#pragma once
#ifndef _G_HEIGHTFOG_PASS_
#define _G_HEIGHTFOG_PASS_
#include "GRenderer.h"

namespace MXVisual
{
	struct GHeightFogPass
	{
		static Boolean	Init();
		static Void		Release();

		static Void Render(
			IGCommandList* pCommandList,
			const IGTexture* SceneDepth,
			const IGTexture* PresentTarget,
			Float3 Color,
			Float1 Density,
			Float1 StartDistance,
			Float1 DistanceFalloff,
			Float1 Height,
			Float1 HeightFalloff
		);
	};
}

#endif