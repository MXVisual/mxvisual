#pragma once
#ifndef _G_INDIRECTLIGHT_PASS_
#define _G_INDIRECTLIGHT_PASS_
#include "GRenderer.h"

namespace MXVisual
{
	struct GIndirectLightPass
	{
		static Boolean	Init();
		static Void		Release();

		static Void Render(
			IGCommandList* pCommandList,
			const IGTexture* SceneDepth,
			const IGTexture* SceneColor,
			const IGTexture* GBufferAlbedo_Opacity,
			const IGTexture* GBufferR_M_AO_X,
			const IGTexture* GBufferNormal_ShadingModel,
			const IGTexture* PresentTarget,
			Float4 f4IndirectLightColor,
			Boolean bEnableSSR,
			Array<IGReflectionProbe*>& REProbeList
		);
	};
}

#endif