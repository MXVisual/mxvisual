#pragma once
#ifndef _G_GAMMA_PASS_
#define _G_GAMMA_PASS_
#include "GRenderer.h"

namespace MXVisual
{
	struct GGammaPass
	{
		static Boolean	Init();
		static Void		Release();

		static Void Render(
			IGCommandList* pCommandList,
			const IGTexture* SceneColor,
			const IGTexture* PresentTarget,
			Float1 fGamma);
	};
}

#endif