#include "GBlurPass.h"
#include "MXFile.h"
#include "MXMath.h"
#include "GShaderManager.h"
#include "MXMath.h"
#include "MXCommon.h"

namespace MXVisual
{
	enum BlurType
	{
		EDownSample,
		EDownSample_Additive,
		EDirectionBlur,
		ESampleCore,
		EBlurTypeCount
	};

	const UInt32 MAX_SAMPLE_CORE_COUNT = 64;
	struct CBSBlurProperty
	{
		Float2	InvPixelSizeOrOffset;
		UInt32	SampleCount;
		UInt32	ShadingModelMask;
		Float4 	SampleCore[MAX_SAMPLE_CORE_COUNT];
	};

	static GPipeline	PSO[BlurType::EBlurTypeCount];
	static IGBuffer*	BufferSampleCore = nullptr;
	static UInt32		ThreadGroupX = 32;
	static UInt32		ThreadGroupY = 32;

	Boolean GBlurPass::Init()
	{
		const UInt32 uBlurType[] = {
			0,
			0,
			1,
			2,
		};
		const String ShaderName = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVDeferredPassBlur.msr");

		for (UInt32 u = 0; u < BlurType::EBlurTypeCount; u++)
		{
			String strShaderName = String::Format("MXVDeferredPassBlur_%d.msr", uBlurType[u]);
			ShaderCompiledResult ShaderCode;
			ShaderMacro macro;
			String strDomain = String::Format("%d", MaterialDomain::EPostProcess);
			macro.AddMacro("DOMAIN", strDomain.CString());
			String strSampleCount = String::Format("%d", MAX_SAMPLE_CORE_COUNT);
			macro.AddMacro("MAX_SAMPLE_CORE_COUNT", strSampleCount.CString());
			String strBlueType = String::Format("%d", uBlurType[u]);
			macro.AddMacro("BLUR_TYPE", strBlueType.CString());

			String strThreadGroupX = String::Format("%d", ThreadGroupX);
			String strThreadGroupY = String::Format("%d", ThreadGroupY);

			ShaderType eType = ShaderType::EPixelShader;
			if (uBlurType[u] == 0)
			{
				eType = ShaderType::EPixelShader;
			}
			else
			{
				macro.AddMacro("THREAD_GROUP_X", strThreadGroupX.CString());
				macro.AddMacro("THREAD_GROUP_Y", strThreadGroupY.CString());

				eType = ShaderType::EComputeShader;
			}

			if (!GRendererResource::ShaderManager->CompileShader(
				strShaderName.CString(),
				macro, ShaderCode, nullptr, 0, eType, False))
			{
				IFile * pShaderFile = IFile::Open(ShaderName.CString(), IFile::EFDT_Text);
				if (!pShaderFile)return False;

				Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
				pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

				String strCode = pBuf;

				delete[] pBuf;
				pShaderFile->Close();

				GRendererResource::ShaderManager->CompileShader(
					strShaderName.CString(),
					macro, ShaderCode, strCode.CString(), strCode.Length(), eType, False);
			}
			if (!ShaderCode.IsValid())
				return False;

			GRenderPass RP;
			RP.RTVCount = 1;
			RP.RTVFormats[0] = PixelFormat::EPF_R16G16B16A16_F;
			if (uBlurType[u] == 0)
			{
				if (!PSO[u].InitForRender(
					PrimitiveIAFactory::EStatic2D, 
					TopologyType::ETriangle, RP,
					GRendererResource::RasterizerState(),
					GRendererResource::DepthStencilState(DEPTH_ALWAYS, False),
					u == EDownSample_Additive ? GRendererResource::BlendState(BLENDSTATE_ADDITIVE) : GRendererResource::BlendState(),
					&GRendererResource::DefaultScreenQuadVertexShader, &ShaderCode))
				{
					return False;
				}
			}
			else if (!PSO[u].InitForCompute(&ShaderCode))
			{
				return False;
			}
		}

		BufferSampleCore = GRendererResource::CreateBuffer(
			BufferType::EConstantBuffer,
			True, sizeof(CBSBlurProperty));

		return True;
	}
	Void	GBlurPass::Release()
	{
		for (UInt32 u = 0; u < BlurType::EBlurTypeCount; u++)
		{
			PSO[u].Release();
		}
	}

	Void GBlurPass::BitBlit(
		IGCommandList* pCommandList,
		const IGTexture* Source,
		const IGTexture* Dest,
		UInt32	OffsetX,
		UInt32	OffsetY,
		UInt32	DestWidth,
		UInt32	DestHeight,
		Boolean bAdditive)
	{
		GViewport VP;
		VP.uWidth = DestWidth;
		VP.uHeight = DestHeight;
		VP.fMinDepth = 0.0f;
		VP.fMaxDepth = 1.0f;
		VP.uX = OffsetX;
		VP.uY = OffsetY;

		Region Scissor;
		Scissor.uLeft = 0;
		Scissor.uTop = 0;
		Scissor.uRight = DestWidth;
		Scissor.uBottom = DestHeight;

		pCommandList->SetViewport(VP);
		pCommandList->SetScissor(Scissor);
		pCommandList->BindRenderTarget(False,
			nullptr, 0, False, Dest, 0);

		pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, Source);
		pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);
		pCommandList->BindPipeline(&PSO[bAdditive ? BlurType::EDownSample_Additive : BlurType::EDownSample]);
		pCommandList->Draw(3, 1, True);
	}

	Void GBlurPass::DirectionBlur(
		IGCommandList* pCommandList,
		const IGTexture*	Source,
		const IGTexture*	ShadingModelID,
		const IGTexture*	Direction,
		const IGTexture*	Dest,
		UInt32 DestWidth,
		UInt32 DestHeight,
		Float2 DirOffset,
		UInt32 SampleCount,
		UInt32 ShadingModelMask)
	{
		CBSBlurProperty data;
		data.InvPixelSizeOrOffset = DirOffset;
		data.SampleCount = SampleCount;
		data.ShadingModelMask = ShadingModelMask;

		MapRegion region;
		if (pCommandList->Map(BufferSampleCore, False, region))
		{
			memcpy(region.pAddress, &data, sizeof(CBSBlurProperty));
			pCommandList->Unmap(BufferSampleCore);
		}
		pCommandList->BindUnorderResource(ShaderType::EComputeShader, 0, Dest, 0);

		pCommandList->BindShaderResource(ShaderType::EComputeShader, 0, Source);
		pCommandList->BindShaderResource(ShaderType::EComputeShader, 1, ShadingModelID);
		pCommandList->BindShaderResource(ShaderType::EComputeShader, 2, Direction);


		pCommandList->BindSampler(ShaderType::EComputeShader, 0, GRendererResource::EngineCommonSampler);
		pCommandList->BindConstantBuffer(ShaderType::EComputeShader, 1, BufferSampleCore);
		pCommandList->BindPipeline(&PSO[BlurType::EDirectionBlur]);

		pCommandList->Dispatch(DestWidth / ThreadGroupX, DestHeight/ ThreadGroupY, 1);
	}

	Void GBlurPass::GaussianBlur(
		IGCommandList*		pCommandList,
		const IGTexture*	Source,
		const IGTexture*	Dest,
		UInt32	DestWidth,
		UInt32	DestHeight,
		Float1	BlurStrong)
	{
		CBSBlurProperty data;
		data.InvPixelSizeOrOffset = Float2(1.0f / DestWidth, 1.0f / DestHeight);
		data.InvPixelSizeOrOffset *= BlurStrong;
		data.SampleCount = MAX_SAMPLE_CORE_COUNT;
		data.ShadingModelMask = 0;

		SInt32 CoreWidth = sqrt(MAX_SAMPLE_CORE_COUNT);
		SInt32 CoreHeight = MAX_SAMPLE_CORE_COUNT / CoreWidth;
		for (SInt32 v = 0; v < CoreHeight; v++)
		{
			for (SInt32 u = 0; u < CoreWidth; u++)
			{
				Float2 p = Float2(u, v);
				p -= Float2(CoreWidth / 2.0f, CoreHeight / 2.0f);
				data.SampleCore[u + v * CoreWidth] =
					Float4(p[0], p[1], Math::Gaussian(p, 1.0f, Float2(), Float2(2, 2)), 0);
			}
		}

		MapRegion region;
		if (pCommandList->Map(BufferSampleCore, False, region))
		{
			memcpy(region.pAddress, &data, sizeof(CBSBlurProperty));
			pCommandList->Unmap(BufferSampleCore);
		}

		pCommandList->BindUnorderResource(ShaderType::EComputeShader, 0, Dest, 0);

		pCommandList->BindPipeline(&PSO[BlurType::ESampleCore]);
		pCommandList->BindSampler(ShaderType::EComputeShader, 0, GRendererResource::EngineCommonSampler);

		pCommandList->BindShaderResource(ShaderType::EComputeShader, 0, Source);
		pCommandList->BindConstantBuffer(ShaderType::EComputeShader, 1, BufferSampleCore);

		pCommandList->Dispatch(DestWidth / ThreadGroupX, DestHeight / ThreadGroupY, 1);
	}
}
