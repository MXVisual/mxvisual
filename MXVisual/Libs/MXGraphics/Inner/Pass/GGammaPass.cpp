#include "GGammaPass.h"
#include "GShaderManager.h"

namespace MXVisual
{
	struct CBSGamma
	{
		Float1 fGamma;
	};
	namespace
	{
		GPipeline PSO;
		IGBuffer* Buffer = nullptr;
	}

	Boolean	GGammaPass::Init()
	{
		ShaderCompiledResult PS;
		ShaderMacro macro;
		if (!GRendererResource::ShaderManager->CompileShader(
			"MXVDeferredPassGamma.msr",
			macro, PS, nullptr, 0, ShaderType::EPixelShader, False))
		{
			const String PixelShaderName = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVDeferredPassGamma.msr");

			IFile * pShaderFile = IFile::Open(PixelShaderName.CString(), IFile::EFDT_Text);
			if (!pShaderFile)return False;

			String strDomain = String::Format("%d", MaterialDomain::EPostProcess);
			macro.AddMacro("DOMAIN", strDomain.CString());
			Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
			pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

			String strCode = pBuf;
			delete[] pBuf;
			pShaderFile->Close();

			GRendererResource::ShaderManager->CompileShader(
				"MXVDeferredPassGamma.msr",
				macro, PS, strCode.CString(), strCode.Length(), ShaderType::EPixelShader, False);
		}

		GRenderPass RP;
		RP.RTVCount = 1;
		RP.RTVFormats[0] = EPF_R8G8B8A8;

		PSO.InitForRender(
			PrimitiveIAFactory::EStatic2D, 
			TopologyType::ETriangle, RP,
			GRendererResource::RasterizerState(),
			GRendererResource::DepthStencilState(DEPTH_ALWAYS, False),
			GRendererResource::BlendState(),
			&GRendererResource::DefaultScreenQuadVertexShader, &PS);

		Buffer = GRendererResource::CreateBuffer(
			BufferType::EConstantBuffer,
			True,
			sizeof(CBSGamma));

		return True;
	}
	Void	GGammaPass::Release()
	{
		PSO.Release();
	}

	Void GGammaPass::Render(
		IGCommandList* pCommandList,
		const IGTexture* SceneColor,
		const IGTexture* PresentTarget,
		Float1 fGamma)
	{
		CBSGamma data;
		data.fGamma = fGamma;

		MapRegion region;
		pCommandList->Map(Buffer, False, region);
		memcpy(region.pAddress, &data, sizeof(CBSGamma));
		pCommandList->Unmap(Buffer);

		pCommandList->BindRenderTarget(
			False,
			nullptr, 0,
			False, PresentTarget, 0);

		pCommandList->BindPipeline(&PSO);
		pCommandList->BindShaderResource(ShaderType::EPixelShader, 0, SceneColor);

		pCommandList->BindSampler(ShaderType::EPixelShader, 0, GRendererResource::EngineCommonSampler);
		pCommandList->BindConstantBuffer(ShaderType::EPixelShader, 1, Buffer);
		pCommandList->Draw(3, 1, True);
	}
}
