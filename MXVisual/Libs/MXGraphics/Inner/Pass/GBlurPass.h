#pragma once
#ifndef _G_BLUR_PASS_
#define _G_BLUR_PASS_
#include "GRenderer.h"

namespace MXVisual
{
	struct GBlurPass
	{
		static Boolean Init();
		static Void	Release();

		//Must Set BlendState Before
		static Void BitBlit(
			IGCommandList* pCommandList,
			const IGTexture*	Source,
			const IGTexture*	Dest,
			UInt32	OffsetX,
			UInt32	OffsetY,
			UInt32	DestWidth,
			UInt32	DestHeight,
			Boolean bAdditive);

		static Void	DirectionBlur(
			IGCommandList* pCommandList,
			const IGTexture*	Source,
			const IGTexture*	ShadingModelID,
			const IGTexture*	Direction,
			const IGTexture*	Dest,
			UInt32 DestWidth,
			UInt32 DestHeight,
			Float2 DirOffset,
			UInt32 SampleCount,
			UInt32 ShadingModelMask);//-1 For All ShadingModel

		static Void	GaussianBlur(
			IGCommandList* pCommandList,
			const IGTexture*	Source,
			const IGTexture*	Dest,
			UInt32	DestWidth,
			UInt32	DestHeight,
			Float1	BlurStrong);
	};
}

#endif