#pragma once
#ifndef _G_SSAO_PASS_
#define _G_SSAO_PASS_
#include "GRenderer.h"

namespace MXVisual
{
	struct GSSAOPass
	{
		static Boolean	Init();
		static Void		Release();

		//Output On PresentTarget B Channel
		static Void Render(
			IGCommandList* pCommandList,
			const IGTexture* SceneDepth,
			const IGTexture* Normal_ShadingModel,
			const IGTexture* PresentTarget,
			Float1 fSampleCoreRange,
			Float1 fIndirectLightIntensity,
			Float1 fAttenuation);
	};
}

#endif