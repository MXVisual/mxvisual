#pragma once
#ifndef _G_BRIGHTNESS_FILTER_PASS_
#define _G_BRIGHTNESS_FILTER_PASS_
#include "GRenderer.h"

namespace MXVisual
{
	struct GBrightnessFilterPass
	{
		static Boolean	Init();
		static Void		Release();

		//Filter Out By Depth & Brightness
		static Void Render(
			IGCommandList* pCommandList,
			const IGTexture* SceneColor,
			const IGTexture* SceneDepth,
			const IGTexture* PresentTarget,
			Float1 fFilterOutThresold,
			Float1 fFilterOutDepth//Less Depth Pixel Will Be Filter Out
		);
	};
}

#endif