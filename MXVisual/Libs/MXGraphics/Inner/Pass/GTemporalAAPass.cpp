#include "GTemporalAAPass.h"
#include "GShaderManager.h"

namespace MXVisual
{
	namespace
	{
		GPipeline			PSO;
	}

	Boolean	GTemporalAAPass::Init()
	{
		ShaderCompiledResult PS;
		ShaderMacro macro;
		if (!GRendererResource::ShaderManager->CompileShader(
			"MXVPassTemporalAA.msr",
			macro, PS, nullptr, 0, ShaderType::EPixelShader, False))
		{
			const String PixelShaderName = String::Format("%s%s", ENGINE_SHADER_DIR, "MXVPassTemporalAA.msr");

			IFile * pShaderFile = IFile::Open(PixelShaderName.CString(), IFile::EFDT_Text);
			if (!pShaderFile)return False;

			String strDomain = String::Format("%d", MaterialDomain::EPostProcess);
			macro.AddMacro("DOMAIN", strDomain.CString());

			Char* pBuf = new Char[pShaderFile->GetFileSize() + 1]{ 0 };
			pShaderFile->Read(pBuf, pShaderFile->GetFileSize());

			String strCode = pBuf;

			delete[] pBuf;
			pShaderFile->Close();

			GRendererResource::ShaderManager->CompileShader(
				"MXVPassTemporalAA.msr",
				macro, PS, strCode.CString(), strCode.Length(), ShaderType::EPixelShader, False);
		}

		GRenderPass RP;
		RP.RTVCount = 1;
		RP.RTVFormats[0] = GRendererResource::ColorTargets[(const UInt32)ColorTargetType::ESceneColor0]->GetFormat();

		if (!PSO.InitForRender(
			PrimitiveIAFactory::EStatic2D, 
			TopologyType::ETriangle, RP,
			GRendererResource::RasterizerState(),
			GRendererResource::DepthStencilState(DEPTH_ALWAYS, False),
			GRendererResource::BlendState(BLENDSTATE_OFF, WriteMask::EWM_RedChannel | WriteMask::EWM_GreenChannel | WriteMask::EWM_BlueChannel),
			&GRendererResource::DefaultScreenQuadVertexShader, &PS))
		{
			return False;
		}

		return True;
	}
	Void	GTemporalAAPass::Release()
	{
		PSO.Release();
	}

	Void GTemporalAAPass::Render(
		const IGTexture* HistorySceneColor,
		const IGTexture* SceneColor)
	{
	}
}
