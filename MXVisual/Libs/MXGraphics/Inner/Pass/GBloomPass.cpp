#include "GBloomPass.h"
#include "MXFile.h"
#include "MXMath.h"
#include "GShaderManager.h"
#include "GBlurPass.h"

namespace MXVisual
{
	static Array<IGTexture*> DownSampleTargets;

	Boolean	GBloomPass::Init()
	{
		SInt32 DownSampleWidth = GRendererResource::BackbufferSize[0] * 0.5f;
		SInt32 DownSampleHeight = GRendererResource::BackbufferSize[1] * 0.5f;

		IGTexture::Description desc;
		desc.bDynamic = False;
		desc.bGenerateMipMap = False;
		desc.bTargetable = True;
		desc.bUnorderAccessable = True;
		desc.eFormat = EPF_R16G16B16A16_F;
		desc.eType = TextureType::ETexture2D;
		desc.uDepth = 1.0f;
		desc.uMipLevel = 1.0f;
		while (DownSampleWidth > 128 && DownSampleHeight > 128)
		{
			UInt32 uDownSampleTargetIndex = DownSampleTargets.ElementCount();

			desc.uWidth = DownSampleWidth;
			desc.uHeight = DownSampleHeight;

			IGTexture* pTex = GraphicsResource::CreateTexture(desc);

			DownSampleTargets.Add(pTex);

			DownSampleWidth >>= 1;
			DownSampleHeight >>= 1;
		}

		return True;
	}
	Void	GBloomPass::Release()
	{
		for (UInt32 u = 0; u < DownSampleTargets.ElementCount(); u++)
		{
			MX_RELEASE_INTERFACE(DownSampleTargets[u]);
		}
	}

	Void GBloomPass::Render(
		IGCommandList* pCommandList,
		const IGTexture* SceneColor,
		const IGTexture* PresentTarget,
		UInt32 uTargetWidth,
		UInt32 uTargetHeight,
		Float1 fBlurStrong)
	{
		SInt32 HighestRTWidth = GRendererResource::BackbufferSize[0] * 0.5f;
		SInt32 HighestRTHeight = GRendererResource::BackbufferSize[1] * 0.5f;

		//DownSample
		SInt32 DownSampleWidth = HighestRTWidth;
		SInt32 DownSampleHeight = HighestRTHeight;

		GBlurPass::BitBlit(pCommandList, SceneColor, DownSampleTargets[0], 0, 0, (UInt32)DownSampleWidth, (UInt32)DownSampleHeight, False);

		DownSampleWidth >>= 1;
		DownSampleHeight >>= 1;
		for (UInt32 u = 1; u < DownSampleTargets.ElementCount(); u++)
		{
			const IGTexture* Dest = nullptr;
			const IGTexture* Source = nullptr;
			Source = DownSampleTargets[u - 1];
			Dest = DownSampleTargets[u];

			GBlurPass::BitBlit(pCommandList, Source, Dest, 0, 0, (UInt32)DownSampleWidth, (UInt32)DownSampleHeight, False);
			DownSampleWidth >>= 1;
			DownSampleHeight >>= 1;
		}
		pCommandList->Flush();

		//GaussianBlur On Min Target
		GBlurPass::GaussianBlur(
			pCommandList,
			DownSampleTargets[DownSampleTargets.ElementCount() - 1],
			DownSampleTargets[0],
			HighestRTWidth, HighestRTHeight, fBlurStrong);

		pCommandList->Flush();
		GBlurPass::BitBlit(pCommandList, DownSampleTargets[0],
			PresentTarget, 0, 0, uTargetWidth, uTargetHeight, True);
	}
}
