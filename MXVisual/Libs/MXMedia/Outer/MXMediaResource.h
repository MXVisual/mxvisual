#pragma once
#ifndef _MX_MEDIA_RESOURCE_
#define _MX_MEDIA_RESOURCE_
#include "MXCommon.h"
#include "MXInterface.h"
#include "MXArray.h"

#if defined(USE_DLL_MEDIA)
#define DLL_DELCARE __declspec(dllimport)
#elif defined(DLL_MEDIA)
#define DLL_DELCARE __declspec(dllexport)
#else 
#define DLL_DELCARE
#endif

namespace MXVisual
{
	enum MAudioChannelType
	{
		EMACT_Mono,
		EMACT_Stero,
		EMACT_Count
	};

	class IMResource : public IInterface
	{
	public:
		virtual ~IMResource() {};

		Void Release();

		virtual UInt32 GetUniqueID() const = 0;
	};

	class IMListener : public IMResource
	{
	public:
		struct Description
		{
		};

	};

	class IMAudio : public IMResource
	{
	public:
		struct Description
		{
			UInt32				uFrequency;
			MAudioChannelType	eChannelType;
			UInt32				uPCMDataSize;
			const UInt16*		pPCMData;
		};

		virtual MAudioChannelType GetChannelType() const = 0;
		virtual Float1	GetTotalSecond() const = 0;
		virtual Float1	GetCurrentSecond() const = 0;
		virtual UInt32	GetSampleRate() const = 0;

		virtual Void	SetCurrentSecond(Float1 fSecond) = 0;

		virtual Void	Stop() = 0;
	};

	class IMVideo : public IMResource
	{
	public:

	};

	struct DLL_DELCARE MediaResource
	{
		static IMAudio*	CreateAudio(const IMAudio::Description& desc);
	};
}
#endif