#pragma once
#ifndef _MX_MEDIA_PLAYER_
#define _MX_MEDIA_PLAYER_
#include "MXCommon.h"
#include "MXInterface.h"
#include "MXArray.h"

#if defined(USE_DLL_MEDIA)
#define DLL_DELCARE __declspec(dllimport)
#elif defined(DLL_MEDIA)
#define DLL_DELCARE __declspec(dllexport)
#else 
#define DLL_DELCARE
#endif

namespace MXVisual
{
	struct DLL_DELCARE MediaPlayer
	{
		struct InitParam
		{
		};
		static Boolean Init(InitParam& param);
		static Void	Release();

		static Void RegisterResource(class IMResource* pResource);

		static Void CycleUpdate(Float1 fDeltaTime);
	};
}
#endif