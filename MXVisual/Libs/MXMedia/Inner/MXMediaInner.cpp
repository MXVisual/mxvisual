#include "MXPlatform.h"
#include "MXMediaResource.h"
#include "MXMediaPlayer.h"
#include "MXLog.h"

#include "MXFile.h"
#include "MXArray.h"

#include "MCommon.h"

#include "MAudio.h"

#define FLAC__NO_DLL
#include "FLAC/stream_decoder.h"

#include "MXConstant.h"

namespace MXVisual
{
	PlayerContent*			g_pPlayer = nullptr;
	PlayerResourceContent*	g_pPlayerResource = nullptr;

	Void	DeleteResource(IMResource* pResource)
	{
		for (auto i = g_pPlayerResource->Resources.begin(); i != g_pPlayerResource->Resources.end(); i++)
		{
			if (*i == pResource)
			{
				g_pPlayerResource->Resources.erase(i);
				delete pResource;
				break;
			}
		}
	}

	Void IMResource::Release()
	{
		m_nRefCount--;
		if (m_nRefCount <= 0)
		{
			DeleteResource(this);
		}
	}

	Boolean MediaPlayer::Init(InitParam& param)
	{
		g_pPlayerResource = new PlayerResourceContent;
		g_pPlayerResource->uResourceID = 0;
		g_pPlayerResource->Device = alcOpenDevice(NULL);

		g_pPlayer = new PlayerContent;
		g_pPlayer->Context = alcCreateContext(g_pPlayerResource->Device, NULL);

		alcMakeContextCurrent(g_pPlayer->Context);

		return True;
	}
	Void	MediaPlayer::Release()
	{
		ASSERT_IF_FAILED(g_pPlayerResource->Resources.size() == 0);

		for (UInt32 u = 0; u < g_pPlayerResource->Resources.size(); u++)
		{
			delete g_pPlayerResource->Resources[u];
		}
		g_pPlayerResource->Resources.clear();

		alcMakeContextCurrent(NULL);
		alcDestroyContext(g_pPlayer->Context);
		alcCloseDevice(g_pPlayerResource->Device);
	}

	Void MediaPlayer::RegisterResource(class IMResource* pResource)
	{
		MAudio* pAudio = dynamic_cast<MAudio*>(pResource);

		if (pAudio != nullptr)
		{
			g_pPlayer->AudioResource.push_back(pAudio);
		}
	}

	Void MediaPlayer::CycleUpdate(Float1 fDeltaTime)
	{
		for (auto i = g_pPlayer->AudioResource.begin();
			i != g_pPlayer->AudioResource.end(); i++)
		{
			MAudio* pRefAudio = *i;
			ALuint uALResource = pRefAudio->GetALResource();

			ALint curState = 0;
			alGetSourcei(uALResource, AL_SOURCE_STATE, &curState);

			if (curState != AL_PLAYING)
			{
				alSourcePlay(uALResource);
			}
		}

		g_pPlayer->AudioResource.clear();
	}


	IMAudio*	MediaResource::CreateAudio(const IMAudio::Description& desc)
	{
		MAudio* pSound = new MAudio(g_pPlayerResource->uResourceID);
		if (pSound->Init(desc.pPCMData,
			desc.uPCMDataSize,
			desc.uFrequency,
			desc.eChannelType))
		{
			g_pPlayerResource->Resources.push_back(pSound);
			g_pPlayerResource->uResourceID++;
		}
		else
		{
			delete pSound;
			pSound = nullptr;
		}
		return pSound;
	}
}
