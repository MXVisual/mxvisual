#include "MAudio.h"
#include "MXMath.h"
#include "math.h"
#include "MCommon.h"

namespace MXVisual
{
	MAudio::MAudio(UInt32 uUniqueID)
		: m_uUniqueID(uUniqueID)
		, m_pPCMData(nullptr)
	{
	}
	MAudio::~MAudio()
	{
		MX_DELETE_ARRAY(m_pPCMData);

		Stop();
		alDeleteBuffers(1, &m_AudioSourceID);
		alDeleteSources(1, &m_AudioID);
	}

	Boolean	MAudio::Init(const UInt16* pPCMData, UInt32 uPCMDataSize, UInt32 uFreq, MAudioChannelType eChannel)
	{
		if (pPCMData == nullptr)
			return False;

		m_uPCMDataSize = uPCMDataSize;
		m_pPCMData = new UInt16[uPCMDataSize / 2];
		memcpy(m_pPCMData, pPCMData, m_uPCMDataSize);

		ALenum format;
		if (eChannel == EMACT_Mono)
		{
			format = AL_FORMAT_MONO16;
			m_eChannelType = EMACT_Mono;
			m_fTotalSecond = uPCMDataSize / 2.0f / uFreq;
		}
		else// if (Format == AL_FORMAT_STEREO16)
		{
			format = AL_FORMAT_STEREO16;
			m_eChannelType = EMACT_Stero;
			m_fTotalSecond = uPCMDataSize / 4.0f / uFreq;
		}

		m_uSampleRate = uFreq;
		alGenSources(1, &m_AudioID);
		alGenBuffers(1, &m_AudioSourceID);
		alBufferData(m_AudioSourceID, format, m_pPCMData, m_uPCMDataSize, uFreq);
		CHECK_AL_ERROR(return False;);

		alSourcei(m_AudioID, AL_BUFFER, m_AudioSourceID);
		CHECK_AL_ERROR(return False;);
		alSourcei(m_AudioID, AL_LOOPING, false);

		return True;
	}

	Void	MAudio::SetCurrentSecond(Float1 fSecond)
	{
		ALfloat fCurrentTime = Math::Clamp(fSecond, 0.0f, m_fTotalSecond);
		alSourcef(m_AudioID, AL_SEC_OFFSET, fCurrentTime);
	}
	Float1	MAudio::GetCurrentSecond() const
	{
		ALfloat time;
		alGetSourcef(m_AudioID, AL_SEC_OFFSET, &time);
		ALint state;
		alGetSourcei(m_AudioID, AL_SOURCE_STATE, &state);
		return state == AL_STOPPED ? m_fTotalSecond : time;
	}
	Void	MAudio::Stop()
	{
		alSourceStop(m_AudioID);
	}
}
