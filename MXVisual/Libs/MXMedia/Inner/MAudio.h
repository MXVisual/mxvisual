#pragma once
#ifndef _MX_MEDIA_COMMON_
#define _MX_MEDIA_COMMON_
#include "MXMediaResource.h"

//OpenAL
#include "OpenAL1.1/al.h"

#include "MCommon.h"
#include "MXMemory.h"


namespace MXVisual
{
	class MAudio : public IMAudio, public MemoryBlock
	{
	public:
		MAudio(UInt32 uUniqueID);
		~MAudio();

		Boolean Init(const UInt16* pPCMData, UInt32 uPCMDataSize, UInt32 uFreq, MAudioChannelType eChannel);

		UInt32 GetUniqueID() const override { return m_uUniqueID; }

		MAudioChannelType GetChannelType() const override { return m_eChannelType; }
		Float1	GetTotalSecond() const override { return m_fTotalSecond; }
		Float1	GetCurrentSecond() const override;
		UInt32	GetSampleRate() const override { return m_uSampleRate; }
		Void	SetCurrentSecond(Float1 fSecond) override;

		Void	Stop();

		ALuint	GetALResource() const { return m_AudioID; }
	protected:
		UInt32 m_uUniqueID;

		ALuint m_AudioID;
		ALuint m_AudioSourceID;

		UInt16*	m_pPCMData;
		UInt32	m_uPCMDataSize;

		MAudioChannelType m_eChannelType;
		Float1 m_fGain;//����
		Float1 m_fSpeed;
		Float1 m_fTotalSecond;
		UInt32 m_uSampleRate;
	};
}

#endif