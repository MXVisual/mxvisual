#pragma once
#ifndef _M_COMMON_
#define _M_COMMON_
#include "MXMediaResource.h"
#include <vector>
//OpenAL
#include "OpenAL1.1/al.h"
#include "OpenAL1.1/alc.h"

namespace MXVisual
{
	struct PlayerContent
	{
		ALCcontext* Context;

		std::vector<class MAudio*>		AudioResource;
		//std::vector<IMVideo*>		VideoResource;
	};

	struct PlayerResourceContent
	{
		UInt32 uResourceID;
		String strResourcePath;

		ALCdevice*	Device;
		std::vector<IMResource*> Resources;
	};

#define CHECK_AL_ERROR(execute)  {ALenum error = alGetError(); if(error != AL_NO_ERROR){ ASSERT_IF_FAILED(error == AL_NO_ERROR); {execute} }}

}

#endif