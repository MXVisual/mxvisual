#pragma once
#ifndef _MX_C_COMPONENT_2D_
#define _MX_C_COMPONENT_2D_
#include "MXPlatform.h"
#include "MXCObject.h"
#include "MXCComponent.h"

#if defined(USE_DLL_RESOURCE)
#define DLL_DELCARE __declspec(dllimport)
#elif defined(DLL_RESOURCE)
#define DLL_DELCARE __declspec(dllexport)
#else 
#define DLL_DELCARE
#endif

namespace MXVisual
{
	class IFile;
	struct IEditorProperty;
	class IEditorPropertyPanel;
#if MX_BUILD_EDITOR
	class ISerializer;
	class IEditorGraphics;
#endif
	class IDeserializer;

	class DLL_DELCARE CComponent2D : public ICComponent
	{
		friend class CObject2D;
		friend struct CSceneRender;

		DECLARE_RTTI_INFO(CComponent2D);

	public:
		CComponent2D();
		~CComponent2D();

	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRender() override;
		Void	OnRelease() override;

		virtual Void OnClick(Input::Key eTriggerKey) {}
		virtual Void OnFocused() {}
		virtual Void OnLostFocus() {}

	protected:
		class IAppMesh*			m_pMesh;
		class IAppMeshInstance*	m_pMeshInstance;
		class IAppMaterialInstance* m_pMaterialIns;
	};

}
#endif