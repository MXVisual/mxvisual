#pragma once
#ifndef _C_RIGIDBODY_COMPONENT_
#define _C_RIGIDBODY_COMPONENT_
#include "MXPlatform.h"
#include "MXVector.h"

namespace MXVisual
{
	class CRigidBodyComponent
	{
	public:
		virtual Void AddForce(const Float3& force) = 0;

		virtual Float3 GetVelocity() const = 0;
	};
}
#endif