#pragma once
#ifndef _MX_C_SERIALIZER_
#define _MX_C_SERIALIZER_
#include "MXCommon.h"
#include "MXArray.h"

namespace MXVisual
{
	class IFile;

#if MX_BUILD_EDITOR
	class ISerializerBlock
	{
	public:

	};

	class ISerializer
	{
	public:
		virtual Void Close() = 0;

		virtual Void Save(const Char* szLine) = 0;
		virtual Void Save(const Char* szKeyName, const Char* szValue) = 0;
		virtual Void Save(const Char* szKeyName, const Boolean bValue) = 0;
		virtual Void Save(const Char* szKeyName, const UInt32 uValue) = 0;
		virtual Void Save(const Char* szKeyName, const Float1 fValue) = 0;
		virtual Void Save(const Char* szKeyName, const Float2& f2Value) = 0;
		virtual Void Save(const Char* szKeyName, const Float3& f3Value) = 0;
		virtual Void Save(const Char* szKeyName, const Float4& f4Value) = 0;
		virtual Void Save(const Char* szKeyName, const Matrix4& mat4Value) = 0;
	};

	class TextSerializer : public MXVisual::ISerializer
	{
	public:
		static ISerializer* Open(const char* szFileName);
		void Close();

	public:
		Void Save(const Char* szLine) override;
		Void Save(const Char* szKeyName, const Char* szValue) override;
		Void Save(const Char* szKeyName, const Boolean bValue) override;
		Void Save(const Char* szKeyName, const UInt32 uValue) override;
		Void Save(const Char* szKeyName, const Float1 fValue) override;
		Void Save(const Char* szKeyName, const Float2& f2Value) override;
		Void Save(const Char* szKeyName, const Float3& f3Value) override;
		Void Save(const Char* szKeyName, const Float4& f4Value) override;
		Void Save(const Char* szKeyName, const Matrix4& mat4Value) override;

	protected:
		TextSerializer();
		~TextSerializer();

		IFile* m_pFile;
	};
#endif

	class IDeserializerBlock
	{
	public:
		/*
		SupportedType Include
		String
		Boolean
		Float1 ~ Float4
		Matrix4
		*/
		virtual ~IDeserializerBlock();
		const Char* GetBlockName() const;
		const IDeserializerBlock* GetChildBlock(UInt32 uIndex) const;
		UInt32 GetChildBlocksCount() const;

		virtual Boolean Load(const Char* varName, String& szRefValue) const = 0;
		virtual Boolean Load(const Char* varName, Boolean& bRefValue) const = 0;
		virtual Boolean Load(const Char* varName, UInt32& uRefValue) const = 0;
		virtual Boolean Load(const Char* varName, Float1& fRefValue) const = 0;
		virtual Boolean Load(const Char* varName, Float2& f2RefValue) const = 0;
		virtual Boolean Load(const Char* varName, Float3& f3RefValue) const = 0;
		virtual Boolean Load(const Char* varName, Float4& f4RefValue) const = 0;
		virtual Boolean Load(const Char* varName, Matrix4& mat4RefValue) const = 0;

	protected:
		String m_strBlockName;
		Array<IDeserializerBlock*> m_arrChildBlock;
	};

	class IDeserializer
	{
	public:
		virtual Void Close() = 0;

		const IDeserializerBlock* GetRootDeserializerBlock() const { return m_pRootBlock; }
	protected:
		virtual ~IDeserializer();

		IDeserializerBlock* m_pRootBlock;
	};

	//明文反序列化器
	class TextDeserializer : public IDeserializer
	{
	public:
		static IDeserializer* Open(const Char* szFileName);
		Void Close() override;

	protected:
		TextDeserializer();
		~TextDeserializer();
	};


	class ICSerializable
	{
	public:
		virtual ~ICSerializable() {}
		virtual Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) = 0;
#if MX_BUILD_EDITOR
		virtual Boolean	OnSerialize(ISerializer& Serializer) = 0;
#endif
	};
}
#endif