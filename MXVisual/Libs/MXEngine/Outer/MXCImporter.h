#pragma once
#ifndef _MX_C_IMPORTER_
#define _MX_C_IMPORTER_
#include "MXPlatform.h"

#if defined(USE_DLL_RESOURCE)
#define DLL_DELCARE __declspec(dllimport)
#elif defined(DLL_RESOURCE)
#define DLL_DELCARE __declspec(dllexport)
#else 
#define DLL_DELCARE
#endif

namespace MXVisual
{
	class IEditorPropertyPanel;
	struct IEditorProperty;

	class DLL_DELCARE ICImporter
	{
	public:
		virtual ~ICImporter() {}

		virtual Void	SetImportFileName(const Char* szFileName) = 0;
		virtual Void	SetImportTargetPath(const Char* szTargetPath) = 0;
		virtual Boolean Import() = 0;

#if MX_BUILD_EDITOR
		virtual Void	OnPanelActived(IEditorPropertyPanel& Panel) = 0;
		virtual Void	OnPanelChanged(const IEditorProperty* pProperty) = 0;
#endif
	};
}
#endif