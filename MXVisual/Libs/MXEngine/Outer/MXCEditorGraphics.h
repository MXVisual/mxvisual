#pragma once
#ifndef _MX_C_EDITOR_GRAPHICS_
#define _MX_C_EDITOR_GRAPHICS_
#include "MXCommon.h"

namespace MXVisual
{
	class IEditorGraphics
	{
	public:
		virtual ~IEditorGraphics() {};

		virtual Void ChangeColor(const Float4& f4Color) = 0;

		virtual Void RenderLine(
			const Float3& f3Position0,
			const Float3& f3Position1,
			const Float1 fWidth = 1
		) = 0;

		virtual Void RenderLine2D(
			const Float2& f2Position0,
			const Float2& f2Position1,
			const Float1 fWidth = 1
		) = 0;

		virtual Void RenderArrow(
			const Float3& f3Position0,
			const Float3& f3Position1,
			const Float3& f3Up,
			const Float1 fWidth = 1
		) = 0;

		virtual Void RenderArrow2D(
			const Float2& f2Position0,
			const Float2& f2Position1,
			const Float1 fWidth = 1
		) = 0;

		virtual Void RenderPlane(
			const Float3& f3Position,
			const Float3& f3Scale,
			const Float3& f3Right,
			const Float3& f3Up
		) = 0;

		virtual Void RenderPlane2D(
			const Float2& f3Position,
			const Float2& f3Scale,
			Float1 fRotate
		) = 0;

		virtual Void RenderCube(
			const Float3& f3Position,
			const Float3& f3Scale,
			const Float3& f3Right,
			const Float3& f3Up
		) = 0;

		virtual Void RenderSphere(
			const Float3& f3Position,
			const Float3& f3Scale,
			const Float3& f3Right,
			const Float3& f3Up
		) = 0;

		virtual Void RenderText2D(
			const Float2& Position,
			const Float2& CharScale,
			const Char* szText
		) = 0;
	};

	class ICSupportedEditorGraphics
	{
	public:
		virtual Void OnEditorRender(IEditorGraphics& Graphics) = 0;;
	};
}
#endif