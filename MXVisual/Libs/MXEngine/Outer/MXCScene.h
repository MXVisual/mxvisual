#pragma once
#ifndef _MX_C_SCENE_
#define _MX_C_SCENE_
#include "MXCommon.h"
#include "MXMemory.h"
#include "MXCSerializer.h"
#if MX_BUILD_EDITOR
#include "MXCEditorPanel.h"
#endif

#include <unordered_map>
#include <unordered_set>

#if defined(USE_DLL_RESOURCE)
#define DLL_DELCARE __declspec(dllimport)
#elif defined(DLL_RESOURCE)
#define DLL_DELCARE __declspec(dllexport)
#else 
#define DLL_DELCARE
#endif

namespace MXVisual
{
	class ICObject;
	class CSceneEventSystem;
	class CCamera;

	class IFile;
	class IGTexture;
	class IGPrimitive;
	class IGMaterialInstance;

	class DLL_DELCARE CScene 
		: public MemoryBlock
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
	public:
		static CScene* ActivedScene();

	public:
		CScene(const Char* szName);
		virtual ~CScene();

		CScene(const CScene&) = delete;
		const CScene& operator=(const CScene&) = delete;

		Void			Active();
		Void			Inactive();

		CCamera*		MainCamera();

		Void			Rename(const Char* szName) { m_strName = szName; }
		const Char*		GetName() { return m_strName.CString(); }
		UInt32			GetUniqueID() { return m_uUniqueID; }

		virtual Void	OnInit();
		virtual Void	OnUpdate();
		virtual Void	OnRelease();

		virtual Void	RegisterObject(ICObject* pObj);

		ICObject* GetObject(const Char* szName);
		ICObject* GetObject(UInt32 uID);

		UInt32	  GetRootObjectCount() { return (UInt32)m_arrObject.size(); }
		ICObject* GetRootObject(UInt32 uIndex);

		ICObject* RemoveObject(const Char* szName);
		ICObject* RemoveObject(UInt32 uID);

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Boolean	OnSerialize(ISerializer& Serializer);
	
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		//Has Primitive And Can Render Mean is Visible
		ICObject* PickVisibleObject(const Float3& f3Position, const Float3& f3Direction);
#endif

	protected:
		Void	OnTickInputEvent();

		friend struct CSceneRender;
	protected:
		UInt32	m_uUniqueID;
		String	m_strName;

		std::unordered_set<ICObject*> m_arrObject;

		CCamera*	m_pMainCamera;

		struct EnvironmentProperty
		{
			Float3	f3AmbientColor;
			Float1	fAmbientScale;
			Boolean bSSAO;
			Float1	fSSAORange;
			Boolean bSSR;
			Boolean bBloom;
			Float1	fBloomLuminance;
			Boolean bHeightFog;
			Float3	f3HeightFogColor;
			Float1	fHeightFogDensity;
			Float1	fHeightFogStartDistance;
			Float1	fHeightFogDistanceFalloff;
			Float1	fHeightFogHeight;
			Float1	fHeightFogHeightFalloff;
		};
		EnvironmentProperty	m_envProperty;
	};
}
#endif