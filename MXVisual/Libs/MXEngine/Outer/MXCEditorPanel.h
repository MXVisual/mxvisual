#pragma once
#ifndef _MX_C_EDITOR_PANEL_
#define _MX_C_EDITOR_PANEL_
#include "MXPlatform.h"
#include "MXArray.h"
#include "MXConstant.h"
#include "MXString.h"

namespace MXVisual
{
	struct IEditorProperty
	{
		enum EditorPropertyType
		{
			//EGroup,
			EString,
			ELongString,
			EFile,
			EColor,
			ETrigger,
			EEnumeration,
			EFloat1,
			EFloat2,
			EFloat3,
			EFloat4,
			EEditorPropertyTypeCount
		};

		IEditorProperty() :Name(""), PropertyType(EEditorPropertyTypeCount) {}
		Char Name[Constant_MAX_PATH];
		EditorPropertyType PropertyType;
	};
	struct EditorPropertyString : public IEditorProperty
	{
		EditorPropertyString() : Value("") { PropertyType = EString; }
		Char Value[Constant_MAX_PATH];
	};
	struct EditorPropertyLongString : public IEditorProperty
	{
		EditorPropertyLongString() : Value("") { PropertyType = ELongString; }
		String Value;
	};
	struct EditorPropertyFile : public EditorPropertyString
	{
		EditorPropertyFile() { PropertyType = EFile; }
	};
	struct EditorPropertyTrigger : public IEditorProperty
	{
		EditorPropertyTrigger() : Value(False) { PropertyType = ETrigger; }
		Boolean Value;
	};
	struct EditorPropertyEnumeration : public IEditorProperty
	{
		EditorPropertyEnumeration() : Value(0), ValueText("") { PropertyType = EEnumeration; }
		SInt32	Value;
		Char	ValueText[Constant_MAX_PATH];
	};
	struct EditorPropertyFloat1 : public IEditorProperty
	{
		EditorPropertyFloat1() : Value(0) { PropertyType = EFloat1; }
		Float1 Value;
	};
	struct EditorPropertyFloat2 : public IEditorProperty
	{
		EditorPropertyFloat2() : Value(0, 0) { PropertyType = EFloat2; }
		Float2 Value;
	};
	struct EditorPropertyFloat3 : public IEditorProperty
	{
		EditorPropertyFloat3() : Value(0, 0, 0) { PropertyType = EFloat3; }
		Float3 Value;
	};
	struct EditorPropertyFloat4 : public IEditorProperty
	{
		EditorPropertyFloat4() : Value(0, 0, 0, 0) { PropertyType = EFloat4; }
		Float4 Value;
	};
	struct EditorPropertyColor : public EditorPropertyFloat4
	{
		EditorPropertyColor() { PropertyType = EColor; }
	};

	class IEditorPropertyPanel
	{
	public:
		virtual ~IEditorPropertyPanel() {};

		virtual Void BeginGroup(
			const Char* szName) = 0;
		virtual Void EndGroup() = 0;

		virtual Void AddPropertyString(
			const Char* szName,
			const Char* szValue) = 0;

		virtual Void AddPropertyLongString(
			const Char* szName,
			const Char* szValue) = 0;

		virtual Void AddPropertyFile(
			const Char* szName,
			const Char* szValue,
			const Char* szFilter = "*.*") = 0;

		virtual Void AddPropertyColor(
			const Char* szName,
			const Float4& color = Float4(0, 0, 0, 1)) = 0;

		virtual Void AddPropertyTrigger(
			const Char* szName,
			const Boolean bValue) = 0;

		virtual Void AddPropertyEnumeration(
			const Char* szName,
			Array<String>& arrEnumLabel,
			Array<SInt32>& arrEnumValue,
			SInt32 nValue) = 0;

		virtual Void AddPropertyFloat1(
			const Char* szName,
			const Float1 fValue = 0.0f,
			const Float1 fStep = 0.01f,
			const Float1 fMin = 0.0f,
			const Float1 fMax = 1.0f) = 0;

		virtual Void AddPropertyFloat2(
			const Char* szName,
			const Float2& f2Value = Float2(0, 0),
			const Float1 fStep = 0.01f,
			const Float1 fMin = 0.0f,
			const Float1 fMax = 1.0f) = 0;

		virtual Void AddPropertyFloat3(
			const Char* szName,
			const Float3& f3Value = Float3(0.0f, 0.0f, 0.0f),
			const Float1 fStep = 0.01f,
			const Float1 fMin = 0.0f,
			const Float1 fMax = 1.0f) = 0;

		virtual Void AddPropertyFloat4(
			const Char* szName,
			const Float4& f4Value = Float4(0.0f, 0.0f, 0.0f, 0.0f),
			const Float1 fStep = 0.01f,
			const Float1 fMin = 0.0f,
			const Float1 fMax = 1.0f) = 0;
	};

	class ICSupportedEditorPropertyPanel
	{
	public:
		virtual Void	OnPanelActived(IEditorPropertyPanel& Panel) = 0;
		virtual Void	OnPanelInactived() {};
		virtual Void	OnPanelChanged(const IEditorProperty* pProperty) = 0;
	};
}
#endif