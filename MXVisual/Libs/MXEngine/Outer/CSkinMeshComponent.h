#pragma once
#ifndef _C_SKINMESH_COMPONENT_
#define _C_SKINMESH_COMPONENT_
#include "MXPlatform.h"

namespace MXVisual
{
	class CSkinMeshComponent
	{
	public:
		virtual Void SetAnimator() = 0;
	};
}
#endif