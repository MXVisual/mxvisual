#pragma once
#ifndef _C_AUDIO_COMPONENT_
#define _C_AUDIO_COMPONENT_
#include "MXPlatform.h"
#include "MXVector.h"

namespace MXVisual
{
	class CAudioComponent
	{
	public:
		virtual Void Play() = 0;
		virtual Void Stop() = 0;
		virtual Boolean IsPlaying() = 0;
	};
}
#endif