#pragma once
#ifndef _MX_CCAMERA_
#define _MX_CCAMERA_
#include "MXCObject.h"
#include "MXMath.h"
#include "MXCType.h"
#include "MXCEditorGraphics.h"

namespace MXVisual
{
	class IGViewFrustum;
	class IGRenderWindow;

	class DLL_DELCARE CCamera 
		: public CObject
#if MX_BUILD_EDITOR
		, public ICSupportedEditorGraphics
#endif
	{
		DECLARE_RTTI_INFO(CCamera);
	public:
		OBJECT_TYPE(CObjectType::eCamera);

		CCamera(const Char* szName);
		~CCamera();

		Void	SetRenderWindow(IGRenderWindow* pRW);
		IGViewFrustum* GetViewFrustum() const { return m_pViewFrustum; }

		Void SetPerspective(Float1 fFovW, Float1 fAspectWH, Float1 fZNear, Float1 fZFar);
		Void SetOrthographic(Float1 fWidth, Float1 fHeight, Float1 fZNear, Float1 fZFar);

		Float3 ScreenPositionToWorldPosition(const Float2& ScreenPos, Float1 Distance);

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override;

		//Editor
#if MX_BUILD_EDITOR
		Void	OnEditorRender(IEditorGraphics& Graphics);

		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelInactived();
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif
		//TODO!! Editor Need
		Void	OnUpdate() override;

	protected:
		IGViewFrustum*	m_pViewFrustum;
		struct
		{
			Boolean bPerspective;
			Float1 fZNear;
			Float1 fZFar;
			union
			{
				struct
				{
					Float1 fFovW;
					Float1 fAspectWH;
				};
				struct
				{
					Float1 fWidth;
					Float1 fHeight;
				};
			};
		} m_CameraProperty;
	};
}
#endif