#pragma once
#ifndef _MX_C_COMPONENT_
#define _MX_C_COMPONENT_
#include "MXPlatform.h"
#include "MXCObject.h"

#if defined(USE_DLL_RESOURCE)
#define DLL_DELCARE __declspec(dllimport)
#elif defined(DLL_RESOURCE)
#define DLL_DELCARE __declspec(dllexport)
#else 
#define DLL_DELCARE
#endif

namespace MXVisual
{
	class DLL_DELCARE ICComponent 
		: public MemoryBlock
		, public IRuntimeTypeIndentification
	{
		friend class ICObject;
		friend struct CSceneRender;
	private:
		ICObject* m_pRefObj = nullptr;

	protected:
		ICObject* GetObject();
		Matrix4	GetObjectTransform(Boolean bLocal) const;
		Float3	GetObjectDirForward() const;
		Float3	GetObjectDirRight() const;
		Float3	GetObjectDirUp() const;
		Float3	GetObjectPosition(Boolean bLocal) const;
		Float3	GetObjectScale(Boolean bLocal) const;
		Quaternion	GetObjectRotation(Boolean bLocal) const;

		Void	SetObjectTransform(const Matrix4& transform, Boolean bLocal);

	public:
		static Void					Enumerate(Array<String>& arrComponentName);
		static ICComponent*			Create(const Char* szComponentName);

		ICComponent();
		virtual ~ICComponent();

		Void			SetEnable(Boolean bEnable);
		Boolean			GetEnable();

	protected:
		virtual Void	OnInit() = 0;
		virtual Void	OnUpdate() {}
		virtual Void	OnUpdateLate() {}
		virtual Void	OnUpdateFixedDeltaTime() {}
		virtual Void	OnRender() {}
		virtual Void	OnRelease() = 0;

		Boolean m_bEnable;
	};
}
#endif