#pragma once
#ifndef _MX_ASSET_
#define _MX_ASSET_
#include "MXPlatform.h"
#include "MXInterface.h"
#include "MXMemory.h"
#include "MXMatrix.h"
#include "MXVector.h"
#include "MXString.h"
#include "MXArray.h"
#include "MXCEditorPanel.h"

#if defined(DYNAMIC_LINK)
#define DLL_DECLARE DLL_EXPORT
#else 
#define DLL_DECLARE
#endif

namespace MXVisual
{
	namespace DefaultResource
	{
		DLL_DECLARE extern const Char* MESH_PLANE;
		DLL_DECLARE extern const Char* MESH_CUBE;
		DLL_DECLARE extern const Char* MESH_SPHERE;

		DLL_DECLARE extern const Char* MATERIAL;
		DLL_DECLARE extern const Char* MATERIALINSTANCE;

		DLL_DECLARE extern const Char* MOSAIC_TEXTURE;//格子图
		DLL_DECLARE extern const Char* NOISE_TEXTURE;

		DLL_DECLARE extern const Char* RED_TEXTURE;
		DLL_DECLARE extern const Char* GREEN_TEXTURE;
		DLL_DECLARE extern const Char* BLUE_TEXTURE;
		DLL_DECLARE extern const Char* BLACK_TEXTURE;
		DLL_DECLARE extern const Char* WHITE_TEXTURE;
		DLL_DECLARE extern const Char* FLATNORMAL_TEXTURE;

		DLL_DECLARE extern const Char* COMPONENT_NAME_DIRECTION_LIGHT;
		DLL_DECLARE extern const Char* COMPONENT_NAME_SPOT_LIGHT;
		DLL_DECLARE extern const Char* COMPONENT_NAME_POINT_LIGHT;
	}

	class ISerializer;
	class IDeserializerBlock;

	DLL_DECLARE class IAppAsset
		: public MemoryBlock
		, public IInterface
	{
	public:
		Void Release();

		virtual const Char* GetName() const = 0;

		friend struct AppAssetManager;
	protected:
		virtual Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) = 0;
#if MX_BUILD_EDITOR
		virtual Boolean	OnSerialize(ISerializer& Serializer) = 0;
#endif
	};
	DLL_DECLARE	class IAppTexture : public IAppAsset
	{
	public:
		static const Char* Suffix;

		enum Format
		{
			//UNORM
			ETF_R8,
			ETF_RG8,
			ETF_RGBA8,

			//Float
			ETF_R16,
			ETF_RG16,
			ETF_RGBA16,

			//Float
			ETF_R32,
			ETF_RG32,
			ETF_RGBA32,

			//Game
			ETF_BC1,
			ETF_BC3,
			ETF_BC4,
			ETF_BC5,
			ETF_BC6H,
			ETF_BC7,

			ETF_Count,
			ETF_Unknown = ETF_Count
		};
		static IAppTexture* Create(const Char* szAssetName, Format eFormat,
			UInt32 uWidth, UInt32 uHeight, UInt32 uDepth, UInt32 uMipLevel,
			Boolean bArrayTex, Boolean bCubeTex, Boolean bTargetable = False);

		virtual UInt32 GetWidth() const = 0;
		virtual UInt32 GetHeight() const = 0;
		virtual UInt32 GetDepth() const = 0;
		virtual UInt32 GetMipLevel() const = 0;
		virtual Format GetFormat() const = 0;
		virtual Void   GetPixelData(UInt32 uMipmap, UInt32 uDepth, Array<UInt8>& arrResult) const = 0;

		virtual Void   SetPixelData(UInt32 uMipmap, UInt32 uDepth, const UInt8* pPixelDatas) = 0;
	};
	DLL_DECLARE	class IAppMaterial : public IAppAsset
	{
	public:
		static const Char* Suffix;
		static IAppMaterial* Create(const Char* szAssetName, Boolean bCanCache = True);

		enum class Domain : SInt32
		{
			EDM_WorldSpace = 0,
			EDM_Decal,
			EDM_ScreenSpace,
			EDM_PostProcess,
			EDM_Count,
			EDM_Invalid = EDM_Count
		};
		enum class FaceType : SInt32
		{
			EFT_Frontface,
			EFT_Backface,
			EFT_TwoSide,
			EFT_Wireframe,
			EFT_Count,
			EFT_Invalid = EFT_Count
		};
		enum class ShadingModel : SInt32
		{
			ESM_Nolighting,
			ESM_Default,
			ESM_PostprocessFur,//Custom XYZ For FurDirection In WorldSpace  W For FurLength
			ESM_Count,
			ESM_Invalid = ESM_Count
		};
		enum class BlendMode : SInt32
		{
			EBM_Opaque = 0,
			EBM_Masked,
			EBM_Translucency,
			EBM_Additive,
			EBM_MaskTranslucency,
			EBM_Count,
			EBM_Invalid = EBM_Count
		};
		struct SamplerState
		{
			enum Address
			{
				ESS_Address_Repeat,
				ESS_Address_Clamp,
				ESS_Address_Mirror,
				ESS_Address_Invalid,
			};
			enum Filter
			{
				ESS_Filter_Point,
				ESS_Filter_Bilinear,
				ESS_Filter_Anisotropic,
			};

			Address eAddress;
			Filter	eFilter;
		};

		virtual Domain GetDomain() const = 0;
		virtual ShadingModel GetShadingModel() const = 0;
		virtual FaceType GetFaceType() const = 0;
		virtual BlendMode GetBlendMode() const = 0;

		//For Editor
#if MX_BUILD_EDITOR
		enum ParameterType
		{
			EPT_Float1,
			EPT_Float2,
			EPT_Float3,
			EPT_Float4,

			EPT_Texture1D,
			EPT_Texture2D,
			EPT_Texture3D,
			EPT_TextureCube,

			EPT_Invalid,
		};
		struct ParameterDescription
		{
			String strName;
			ParameterType eType;

			Float4 f4Default;
			Float3 f3Default;
			Float2 f2Default;
			Float1 f1Default;
			Float1 fStep;

			String strDefault;
			SamplerState sampler;

			ParameterDescription();
			ParameterDescription(const Char* szName, ParameterType ePType = EPT_Invalid, const Float4& fDefault = Float4(), Float1 f1Step = 0.01f);
			ParameterDescription(const Char* szName, ParameterType ePType = EPT_Invalid, const Char* szDefault = nullptr, const SamplerState& ss = SamplerState());
		};
		virtual UInt32 GetParameterCount() const = 0;
		virtual	ParameterDescription GetParameterDescription(UInt32 uParameter) const = 0;
		virtual	ParameterDescription GetParameterDescription(const Char* szParameter) const = 0;

		enum MaterialFunction
		{
			EMF_Custom,
			EMF_SurfaceParams,
			EMF_MeshParams,
			EMF_Count,
			EMF_Invalid = EMF_Count
		};
		virtual Void SetDomain(Domain eType) = 0;
		virtual Void SetShadingModel(ShadingModel eType) = 0;
		virtual Void SetFaceType(FaceType eType) = 0;
		virtual Void SetBlendMode(BlendMode eType) = 0;
		virtual Void SetMaterialFunc(MaterialFunction eType, const String& strCode) = 0;
		virtual const Char* GetMaterialFunc(MaterialFunction eType) const = 0;

		virtual Void RemoveParameter(const Char* szName) = 0;
		virtual Void SetParameter(const ParameterDescription& desc) = 0;

		virtual Boolean Recompile() = 0;
#endif
	};
	DLL_DECLARE	class IAppMaterialInstance : public IAppAsset
	{
	public:
		static const Char* Suffix;
		static IAppMaterialInstance* Create(const Char* szAssetName, const Char* szMaterial);

		virtual const Char* GetRefMaterialName() const = 0;

		virtual Float1 GetFloat1(const Char* szName) const = 0;
		virtual Float2 GetFloat2(const Char* szName) const = 0;
		virtual Float3 GetFloat3(const Char* szName) const = 0;
		virtual Float4 GetFloat4(const Char* szName) const = 0;

		virtual const Char* GetTexture1D(const Char* szName) const = 0;
		virtual const Char* GetTexture2D(const Char* szName) const = 0;
		virtual const Char* GetTexture3D(const Char* szName) const = 0;
		virtual const Char* GetTextureCube(const Char* szName) const = 0;

		virtual Void SetFloat1(const Char* szName, Float1 f1Value) = 0;
		virtual Void SetFloat2(const Char* szName, const Float2& f2Value) = 0;
		virtual Void SetFloat3(const Char* szName, const Float3& f3Value) = 0;
		virtual Void SetFloat4(const Char* szName, const Float4& f4Value) = 0;

		virtual Void SetTexture1D(const Char* szName, const Char* szTextureName) = 0;
		virtual Void SetTexture2D(const Char* szName, const Char* szTextureName) = 0;
		virtual Void SetTexture3D(const Char* szName, const Char* szTextureName) = 0;
		virtual Void SetTextureCube(const Char* szName, const Char* szTextureName) = 0;
	};
	DLL_DECLARE	class IAppFont : public IAppAsset
	{
	public:
		static const Char* Suffix;

		virtual Void SetCharacterSize(UInt32 uSize) = 0;
		virtual Void SetBold(Boolean bBold) = 0;
		virtual Void SetItalic(Boolean bItalic) = 0;

		virtual	UInt32	GetCharacterSize() const = 0;
		virtual	Boolean GetBold() const = 0;
		virtual	Boolean GetItalic() const = 0;

		virtual Float4 GetCharacterRect(UInt16 uCharacter) = 0;
	};
	DLL_DECLARE	class IAppSkeleton : public IAppAsset
	{
	public:
		static const Char* Suffix;
		static IAppSkeleton* Create(const Char* szAssetName);

		virtual UInt32	GetBoneCount() const = 0;
		//不能添加根骨骼
		virtual	Boolean	AddBone(UInt32 uParentBoneIndex, const Char* szBoneName, const Matrix4& matInit) = 0;
		//szParentBoneName = nullptr 设置根骨骼
		virtual	Boolean	AddBone(const Char* szParentBoneName, const Char* szBoneName, const Matrix4& matInit) = 0;

		virtual const Char* GetBoneName(UInt32 uBoneIndex) const = 0;
		virtual UInt32	GetBoneIndex(const Char* szBoneName) const = 0;
	};
	DLL_DECLARE	class IAppAnimationClip : public IAppAsset
	{
	public:
		static const Char* Suffix;
		static IAppAnimationClip* Create(const Char* szAssetName, Float1 fSampleRate);

		virtual Float1	GetTotalTime() const = 0;
		virtual	Float1  GetSampleRate() const = 0;

		virtual Void	SetSamplingTime(Float1 fTime) = 0;
		virtual Float1	GetSamplingTime() const = 0;
		virtual Void	Sampling(const class IAppMesh* pMesh, const class IAppSkeleton* pSkeleton, Array<Matrix4>& arrSampleResult) const = 0;

		//For Editor
		virtual Void	SetSampleRate(Float1 fRate) = 0;
		virtual Void	AddSample(const Char* szNodeName, Float1 fSampleTime, Matrix4 matTransform) = 0;
	};
	DLL_DECLARE	class IAppMesh : public IAppAsset
	{
	public:
		static const Char* Suffix;
		enum MeshType
		{
			EAPPMT_Rigid2D,
			EAPPMT_Skin2D,
			EAPPMT_Rigid,
			EAPPMT_Skin,
			EAPPMT_Line,
		};
		static IAppMesh* Create(const Char* szAssetName, MeshType eType, Boolean b32BitIndex, UInt32 uVertex, UInt32 uIndex, UInt32 uSubMeshCount);

		struct IVertex
		{};
		struct VertexRigid2D : public IVertex
		{
			Float2 f2Position;
			Float4 f4Color;
			Float2 f2TexCoord0;

			Boolean operator==(const VertexRigid2D& other) const;
		};
		struct VertexSkin2D : public IVertex
		{
			Boolean operator==(const VertexSkin2D& other) const;
		};
		struct VertexRigid : public IVertex
		{
			Float4 f4Position;//第四位先占位，可能以后有用
			Float4 f4Normal;
			Float4 f4Tangent;
			Float4 f4Color;
			Float2 f2TexCoord0;

			Boolean operator==(const VertexRigid& other) const;
		};
		struct VertexSkin : public IVertex
		{
			Float4 f4Position;
			Float4 f4Normal;
			Float4 f4Tangent;
			Float4 f4Color;
			Float2 f2TexCoord0;
			UInt32 uRefBoneIndex[4];
			Float1 fRefBoneWeight[4];

			Boolean operator==(const VertexSkin& other) const;
		};

		virtual Void SetVertices(UInt32 uVertexCount, const IVertex* pVertices) = 0;
		virtual Void SetIndices(UInt32 uIndexCount, const UInt8* pIndices) = 0;
		virtual Void SetSubMesh(UInt32 uSubMesh, UInt32 uOffset, UInt32 uSize) = 0;

		virtual UInt32 GetVertexCapacity() const = 0;
		virtual UInt32 GetIndexCapacity() const = 0;
		virtual UInt32 GetSubMeshCount() const = 0;

		//Tring To Remove
		virtual Boolean AddBindingBone(const Char* szBoneName, const Matrix4& matLocalToBone) = 0;
		virtual UInt32	GetBindingBoneCount() const = 0;
		virtual const Char*	GetBindingBoneName(UInt32 uBoneIndex) const = 0;
	};
	DLL_DECLARE	class IAppMeshInstance : public IAppAsset
	{
	public:
		static const Char* Suffix;
		static IAppMeshInstance* Create(const Char* szAssetName, const Char* szMeshAssetName);

		virtual Void SetMesh(const Char* szMeshAssetName) = 0;
		virtual Void SetTransforms(UInt32 uTransformCount, const Matrix4* pTransforms) = 0;
		virtual Void SetMaterialInstance(UInt32 uSubMesh, const Char* szMaterialInstanceAssetName) = 0;

		virtual const IAppMesh* GetMesh() const = 0;
		virtual const Char* GetMaterialInstanceName(UInt32 uSubMesh) const = 0;
	};
	DLL_DECLARE	class IAppAudio : public IAppAsset
	{
	public:
		static const Char* Suffix;
		//static IAppAudio* Create(const Char* szAssetName);

		enum class ChannelType : UInt8
		{
			EMono,
			EStero,
			EInvalid
		};

		virtual ChannelType GetChannelType() const = 0;
		virtual Float1	GetCurrentSecond() const = 0;
		virtual UInt32	GetSampleRate() const = 0;
		virtual Void	SamplePCF(Float1 fSecond, Array<Float1>& arrPCFData, UInt32 uSampleCount) const = 0;
	};

	DLL_DECLARE	class IAppUICanvas : public IAppAsset
	{
	public:
		static IAppUICanvas* Create();
	};

	DLL_DECLARE	struct AppAssetManager
	{
		struct InitParams
		{
			const Char* szAssetsPath;//Must Be Absolute Path
		};
		static Boolean	Init(InitParams& params);
		static Void		Release();

		static IAppAsset*	GetAsset(const Char* szAssetName);

#if MX_BUILD_EDITOR
		static Boolean		SaveAsset(IAppAsset* pAsset, const Char* szAssetSavedName);
		static Boolean		RenameAsset(const Char* szAssetName, const Char* szAssetNewName);
#endif
	};
}
#endif