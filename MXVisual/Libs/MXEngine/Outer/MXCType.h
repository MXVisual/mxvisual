#pragma once
#ifndef _MXC_TYPE_
#define _MXC_TYPE_
#include "MXCommon.h"

//Unique

namespace MXVisual
{
	enum CObjectType
	{
		eBase,
		eBase2D,
		eCamera,
	};

#define OBJECT_TYPE(t)		virtual CObjectType GetType() const override { return t; }

	class ICAutoVariable
	{
	public:

	};

	template<typename CVarType>
	class CAutoVariable : public ICAutoVariable
	{
	public:
		const Char* VarName;

		CAutoVariable(const Char* AutoVarName)
			: VarName(AutoVarName)
		{}

		CAutoVariable(const CAutoVariable<CVarType>& other)
		{
			VarName = other.VarName;
			m_Data = other.m_Data;
		}

		operator const CVarType&() const { return m_Data; }
		operator CVarType&() { return m_Data; }

		CVarType& operator=(const CVarType& Value) { m_Data = Value; return m_Data; }

	protected:
		CVarType m_Data;
	};

#define DEFINE_CAUTOVAR(type, name) CAutoVariable<type> name = CAutoVariable<type>(#name);
}
#endif