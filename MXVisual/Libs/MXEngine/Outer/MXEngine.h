#pragma once
#ifndef _MX_ENGINE_
#define _MX_ENGINE_
#include "MXPlatform.h"

namespace MXVisual
{
	Boolean	EngineInit();
	Void	EngineRelease();

	Void EngineRenderMesh(class IAppMeshInstance* pMeshInstance);

	//Can only Render Identity Transform
	Void EngineRenderMesh(class IAppMesh* pMesh, UInt32 uSubMesh,
		class IAppMaterialInstance* pMtlIns);

	Void EnginePlayAudio(class IAppAudio* pAudio);
}
#endif