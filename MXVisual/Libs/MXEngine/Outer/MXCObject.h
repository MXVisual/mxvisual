#pragma once
#ifndef _MX_C_OBJECT_
#define _MX_C_OBJECT_
#include "MXCommon.h"
#include "MXCType.h"
#include "MXMemory.h"
#include "MXInput.h"
#include "MXMath.h"
#include "MXArray.h"
#include "MXCSerializer.h"
#include "MXCEditorPanel.h"
#include "vector"
#include "map"

#if defined(USE_DLL_RESOURCE)
#define DLL_DELCARE __declspec(dllimport)
#elif defined(DLL_RESOURCE)
#define DLL_DELCARE __declspec(dllexport)
#else 
#define DLL_DELCARE
#endif

namespace MXVisual
{
	class ICComponent;
	class ICRenderCommandList;
#if MX_BUILD_EDITOR
	class IEditorGraphics;
#endif

	class DLL_DELCARE ICObject 
		: public MemoryBlock
		, public IRuntimeTypeIndentification
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		friend class CScene;
		DECLARE_RTTI_INFO(ICObject);
	public:

		virtual CObjectType GetType() const = 0;

		ICObject(const Char* szName);
		virtual ~ICObject();

		Void			SetName(const Char* szName) { m_strName = szName; }
		const Char*		GetName() const { return m_strName.CString(); }
		UInt32			GetUniqueID() const { return m_uUniqueID; }

		Void			SetEnable(Boolean bEnable);
		Boolean			GetEnable() { return m_bEnable; }

		ICObject*		GetParent() const { return m_pParent; }

		//KeepTransform -> After Child Added, Child GlobalTransform Not Change
		Void			AddChild(ICObject* pChild, Boolean bKeepTransform = False);
		UInt32			GetChildCount() const { return (UInt32)m_arrChilden.size(); }
		ICObject*		GetChild(UInt32 uIndex) const;
		ICObject*		GetChild(const Char* szChildName, Boolean bRecursion) const;

		ICObject*		RemoveChild(UInt32 uIndex);

		Void			AddComponent(ICComponent* pComponent);
		ICComponent*	GetComponent(const Char* szComponentClass) const;
		ICComponent*	GetComponent(UInt32 uIndex) const;
		
		ICComponent*	RemoveComponent(const Char* szComponentClass);
		ICComponent*	RemoveComponent(UInt32 uIndex);

		UInt32			GetComponentCount() const { return m_arrComponent.ElementCount(); }

		virtual Void	SetTransform(const Matrix4& mat, Boolean bLocal);
		Matrix4			GetTransform(Boolean bLocal) const;

		virtual Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);
#if MX_BUILD_EDITOR
		virtual Boolean	OnSerialize(ISerializer& Serializer);
#endif
	protected:
		virtual Void	OnInit();
		virtual Void	OnUpdate();
		virtual Void	OnUpdateLate();
		virtual Void	OnUpdateFixedDeltaTime();

		virtual Void	OnRender();
		virtual Void	OnRelease();

	protected:
		UInt32 m_uUniqueID;
		String m_strName;
		Boolean m_bEnable;

		Matrix4 m_matLocal;
		Matrix4 m_matGlobal;

		ICObject*					m_pParent;
		std::vector<ICObject*>		m_arrChilden;
		Array<ICComponent*>			m_arrComponent;
	};

	class DLL_DELCARE CObject : public ICObject
	{
		DECLARE_RTTI_INFO(CObject);

	public:
		OBJECT_TYPE(CObjectType::eBase);

		CObject(const Char* szName);

		Void			SetPosition(const Float3& f3Position, Boolean bLocal);
		Void			SetRotation(const Float3& f3Rotation, Boolean bLocal);
		Void			SetScale(const Float3& f3Scale, Boolean bLocal);
		Void			SetTransform(const Matrix4& mat, Boolean bLocal) override;

		const Float3			GetPosition(Boolean bLocal) const;
		const Quaternion		GetRotation(Boolean bLocal) const;
		const Float3			GetScale(Boolean bLocal) const;

		const Float3			GetDirRight() const { return m_f3DirRight; }
		const Float3			GetDirUp() const { return m_f3DirUp; }
		const Float3			GetDirForward() const { return m_f3DirForward; }

		Void			TransMove(const Float3& f3Direction, Float1 fDistance);
		Void			TransRotate(const Float3& f3Axis, Float1 fRadius);
		Void			TransScale(const Float3& f3Direction, Float1 fRate);

		//Editor
#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelInactived();
		Void	OnPanelChanged(const IEditorProperty* pProperty);
#endif

	protected:
		Float3 m_f3Position;
		Float3 m_f3Scale;
		Quaternion m_qRotation;

		Float3 m_f3DirRight;
		Float3 m_f3DirUp;
		Float3 m_f3DirForward;
	};

	class DLL_DELCARE CObject2D : public ICObject
	{
		friend class CScene;

		DECLARE_RTTI_INFO(CObject2D);
	public:
		OBJECT_TYPE(CObjectType::eBase2D);

		CObject2D(const Char* szName);

		Void			SetPosition(const Float2& f2Position, Boolean bLocal);
		Void			SetAnchorPosition(const Float2& f2Anchor);
		Void			SetRotation(Float1 fRotation, Boolean bLocal);
		Void			SetScale(const Float2& f2Scale, Boolean bLocal);
		Void			SetTransform(const Matrix4& mat, Boolean bLocal) override;

		Float2			GetPosition(Boolean bLocal);
		Float2			GetAnchorPosition() { return m_f2AnchorPosition; }

		Float1			GetRotation(Boolean bLocal);
		Float2			GetScale(Boolean bLocal);

		Float2			GetDirRight() { return m_f2DirRight; }
		Float2			GetDirUp() { return m_f2DirUp; }

		Void			TransMove(const Float2& f2Direction, Float1 fDistance);
		Void			TransRotate(Float1 fRadius);
		Void			TransScale(const Float2& f2Direction, Float1 fRate);

		//Editor
#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelInactived();
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Void	OnEditorRender(IEditorGraphics& Graphics);
#endif

	protected:
		Void OnClick(Input::Key eTriggerKey);
		Void OnFocused();
		Void OnLostFocus();

	protected:
		Float2 m_f2Position;
		Float2 m_f2AnchorPosition;
		Float2 m_f2Scale;
		Float1 m_fRotation;

		Float2 m_f2DirRight;
		Float2 m_f2DirUp;

		UInt32 m_uLayer;
	};

#define DECLARE_SCENEOBJECT_BEGINE(cname, basename)\
	class cname : public basename\
	{\
	public:\
		cname(const Char* szName)\
			:basename(szName){};

#define DECLARE_SCENEOBJECT_END() };
}
#endif