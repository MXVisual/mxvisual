#pragma once
#ifndef _MX_C_SCENERENDER_
#define _MX_C_SCENERENDER_
#include "MXCommon.h"
#include "MXMemory.h"

namespace MXVisual
{
	class CScene;
	class CCamera;

	struct CSceneRender
	{
		static Void Render(CScene* pScene, CCamera* pViewCamera);
	};
}
#endif