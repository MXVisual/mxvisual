#include "MXInterface.h"
#include "MXAsset.h"
#include "MXGraphicsRenderer.h"

#include "MXFile.h"
#include "MXCTerrainComponent.h"

#include "MXPhysicsResource.h"
#include "MXPhysicsSimulator.h"
#include "MXEngine.h"

namespace MXVisual
{
	CTerrainComponent::CTerrainComponent()
		: m_pMesh(nullptr)
		, m_pMaterialInstance(nullptr)
		, m_uGridCount(64)
		, m_f3Scale(100.0f, 25.0f, 100.0f)
		, m_pHeightMap(nullptr)
		, m_pPhysicsBody(nullptr)
		, m_pCollision(nullptr)
	{
		m_pPhysicsBody = PhysicsResourceManager::CreatePhysicsBody();
		ICollisionShape::Description desc;
		desc.eType = ICollisionShape::ETerrain;
		m_pCollision = dynamic_cast<ICollisionTerrain*>(PhysicsResourceManager::CreateCollisionShape(desc));
	}
	CTerrainComponent::~CTerrainComponent()
	{
		OnRelease();
	}

	Void	CTerrainComponent::OnInit()
	{
		if (m_pHeightMap == nullptr)
		{
			SetTerrainMap(DefaultResource::BLACK_TEXTURE);
		}
		if (m_pMaterialInstance == nullptr)
		{
			m_pMaterialInstance = (IAppMaterialInstance*)AppAssetManager::GetAsset(DefaultResource::MATERIALINSTANCE);
		}
	}
	Void	CTerrainComponent::OnUpdate()
	{
		PhysicsSimulator::Unregister(m_pPhysicsBody);

		if (m_pPhysicsBody != nullptr)
		{
			Matrix4 mat = GetObjectTransform(False);
			Quaternion q = Math::MatrixGetQuaternion(mat);
			Float3 t = Math::MatrixGetTranslation(mat);

			m_pPhysicsBody->SetTransform(q, t);
		}

		PhysicsSimulator::Register(m_pPhysicsBody);
	}
	Void	CTerrainComponent::OnRender()
	{
		if (m_pMeshInstance != nullptr && m_pMaterialInstance != nullptr)
		{
			Matrix4 matL2W = GetObjectTransform(False);
			m_pMeshInstance->SetTransforms(1, &matL2W);
			EngineRenderMesh(m_pMeshInstance);
		}
	}
	Void	CTerrainComponent::OnRelease()
	{
		MX_RELEASE_INTERFACE(m_pMeshInstance);
		MX_RELEASE_INTERFACE(m_pMesh);
		MX_RELEASE_INTERFACE(m_pMaterialInstance);
		MX_RELEASE_INTERFACE(m_pHeightMap);
		MX_RELEASE_INTERFACE(m_pPhysicsBody);
		MX_RELEASE_INTERFACE(m_pCollision);
	}

	Void CTerrainComponent::SetTerrainGridCount(UInt32 uCount)
	{
		if (m_uGridCount == uCount)return;

		m_uGridCount = uCount;
		ConvertMesh();
	}
	Void CTerrainComponent::SetTerrainScale(const Float3& f3Scale)
	{
		if (m_f3Scale == f3Scale)return;

		m_f3Scale = f3Scale;
		ConvertMesh();
	}
	Void CTerrainComponent::SetTerrainMap(const Char* szName)
	{
		if (!m_pHeightMap || strcmp(m_pHeightMap->GetName(), szName) != 0)
		{
			MX_RELEASE_INTERFACE(m_pHeightMap);
			m_pHeightMap = (IAppTexture*)AppAssetManager::GetAsset(szName);
		}
		ConvertMesh();
	}

	UInt32				CTerrainComponent::GetTerrainGridCount() const
	{
		return m_uGridCount;
	}
	const Float3&		CTerrainComponent::GetTerrainScale() const
	{
		return m_f3Scale;
	}
	const Char*			CTerrainComponent::GetTerrainMap() const
	{
		return m_pHeightMap != nullptr ? m_pHeightMap->GetName() : "";
	}

	Float1 GetTextureHeight(IAppTexture::Format ePixelFormat,
		const UInt8* pTextureData, UInt32 uTextureWidth, UInt32 uTextureHeight,
		const Float2& uv)
	{
		Float1 fAbsolute[2] = {
			uv[0] * (uTextureWidth - 1),
			uv[1] * (uTextureHeight - 1)
		};

		UInt32 uAddress[4] = {
			Math::Floor(fAbsolute[0]) + Math::Floor(fAbsolute[1]) * uTextureWidth,
			Math::Ceil(fAbsolute[0]) + Math::Floor(fAbsolute[1]) * uTextureWidth,
			Math::Floor(fAbsolute[0]) + Math::Floor(fAbsolute[1]) * uTextureWidth,
			Math::Ceil(fAbsolute[0]) + Math::Ceil(fAbsolute[1]) * uTextureWidth
		};

		Float1 t[2] = {
			fAbsolute[0] - Math::Floor(fAbsolute[0]),
			fAbsolute[1] - Math::Floor(fAbsolute[1])
		};

		Float1 fHeight = 0;
		switch (ePixelFormat)
		{
		case IAppTexture::ETF_RGBA8:
		{
			Float1 Height[4] = {
				(Float1)pTextureData[uAddress[0] * 4],
				(Float1)pTextureData[uAddress[1] * 4],
				(Float1)pTextureData[uAddress[2] * 4],
				(Float1)pTextureData[uAddress[3] * 4]
			};

			fHeight = Math::Lerp(Math::Lerp(Height[0], Height[1], t[0]), Math::Lerp(Height[2], Height[3], t[0]), t[1]);
			fHeight /= 255.0f;
			break;
		}
		case IAppTexture::ETF_R32:
		{
			Float1 Height[4] = {
				 ((Float1*)pTextureData)[uAddress[0]],
				 ((Float1*)pTextureData)[uAddress[1]],
				 ((Float1*)pTextureData)[uAddress[2]],
				 ((Float1*)pTextureData)[uAddress[3]]
			};

			fHeight = Math::Lerp(Math::Lerp(Height[0], Height[1], t[0]), Math::Lerp(Height[2], Height[3], t[0]), t[1]);
			break;
		}
		}
		return fHeight;
	};

	Boolean CTerrainComponent::ConvertMesh()
	{
		if (m_uGridCount == 0 ||
			m_f3Scale == Float3(0, 0, 0) ||
			m_pHeightMap == nullptr)
		{
			return False;
		}
		UInt32 uVCount = (m_uGridCount + 1) * (m_uGridCount + 1);
		UInt32 uICount = m_uGridCount * m_uGridCount * 6;

		if (m_pMesh != nullptr &&
			(uVCount > m_pMesh->GetVertexCapacity() || uICount > m_pMesh->GetIndexCapacity()))
		{
			MX_RELEASE_INTERFACE(m_pMeshInstance);
			MX_RELEASE_INTERFACE(m_pMesh);
		}

		if (m_pMesh == nullptr || m_pMeshInstance == nullptr)
		{
			MX_RELEASE_INTERFACE(m_pMeshInstance);
			MX_RELEASE_INTERFACE(m_pMesh);

			m_pMesh = IAppMesh::Create(
				String::Format("terrain_%p", this).CString(), IAppMesh::EAPPMT_Rigid, True,
				uVCount, uICount, 1);
			m_pMeshInstance = IAppMeshInstance::Create(
				String::Format("terrain_%p_ins", this).CString(), m_pMesh->GetName());
		}

		UInt32 uMapDataSize = 0;
		Array<UInt8> PixelDatas;
		m_pHeightMap->GetPixelData(0, 0, PixelDatas);

		UInt32 uMapWidth = m_pHeightMap->GetWidth();
		UInt32 uMapHeight = m_pHeightMap->GetHeight();

		Array<IAppMesh::VertexRigid> aVertex(m_pMesh->GetVertexCapacity());
		Float3 f3PosOffset = Float3(m_f3Scale[0] / m_uGridCount, m_f3Scale[1], m_f3Scale[2] / m_uGridCount);
		UInt32 uVertexIndex = 0;

		Array<Float1> aHeightData((m_uGridCount + 1) * (m_uGridCount + 1));
		for (UInt32 u = 0; u <= m_uGridCount; u++)
		{
			for (UInt32 v = 0; v <= m_uGridCount; v++)
			{
				Float1 fHeight = GetTextureHeight(m_pHeightMap->GetFormat(), PixelDatas.GetRawData(),
					m_pHeightMap->GetWidth(), m_pHeightMap->GetHeight(),
					Float2((Float1)u / m_uGridCount, (Float1)v / m_uGridCount));

				aHeightData[u + v * (m_uGridCount + 1)] = fHeight;

				aVertex[uVertexIndex].f4Position =
					Float4(f3PosOffset[0] * u - m_uGridCount * 0.5f * f3PosOffset[0],
						f3PosOffset[1] * fHeight,
						f3PosOffset[2] * v - m_uGridCount * 0.5f * f3PosOffset[2], 0);
				aVertex[uVertexIndex].f4Normal = Float4(0, 0, 0, 0);
				aVertex[uVertexIndex].f4Tangent = Float4(0, 0, 0, 1);
				aVertex[uVertexIndex].f4Color = Float4(1, 1, 1, 1);
				aVertex[uVertexIndex].f2TexCoord0 = Float2((Float1)u / m_uGridCount, (Float1)v / m_uGridCount);
				uVertexIndex++;
			}
		}

		auto UpdateTN = [](IAppMesh::VertexRigid& v0, IAppMesh::VertexRigid& v1, IAppMesh::VertexRigid& v2)
		{
			Float3 n = Math::ComputeNormal(Math::Split(v0.f4Position),
				Math::Split(v1.f4Position),
				Math::Split(v2.f4Position));

			Float3 vn0 = Math::Split(v0.f4Normal) + n;
			Float3 vn1 = Math::Split(v1.f4Normal) + n;
			Float3 vn2 = Math::Split(v2.f4Normal) + n;
			vn0 = Math::Normalize(vn0);
			vn1 = Math::Normalize(vn1);
			vn2 = Math::Normalize(vn2);
			v0.f4Normal = Math::Combine(vn0, 0);
			v1.f4Normal = Math::Combine(vn1, 0);
			v2.f4Normal = Math::Combine(vn2, 0);

			Float3 t = Math::Split(Math::ComputeTangent(Math::Split(v0.f4Position),
				Math::Split(v1.f4Position),
				Math::Split(v2.f4Position),
				v0.f2TexCoord0,
				v1.f2TexCoord0,
				v2.f2TexCoord0));
			Float3 vt0 = Math::Split(v0.f4Tangent) + t;
			Float3 vt1 = Math::Split(v1.f4Tangent) + t;
			Float3 vt2 = Math::Split(v2.f4Tangent) + t;
			vt0 = Math::Normalize(vt0);
			vt1 = Math::Normalize(vt1);
			vt2 = Math::Normalize(vt2);
			v0.f4Tangent = Math::Combine(vt0, 1);
			v1.f4Tangent = Math::Combine(vt1, 1);
			v2.f4Tangent = Math::Combine(vt2, 1);
		};

		Array<UInt32> aIndex(m_pMesh->GetIndexCapacity());
		UInt32 uVertexIndex0 = 0;
		UInt32 uVertexIndex1 = m_uGridCount + 1;

		for (UInt32 u = 0; u < m_uGridCount * m_uGridCount; u++)
		{
			aIndex[u * 6] = uVertexIndex0;
			aIndex[u * 6 + 1] = uVertexIndex0 + 1;
			aIndex[u * 6 + 2] = uVertexIndex1;

			aIndex[u * 6 + 3] = uVertexIndex0 + 1;
			aIndex[u * 6 + 4] = uVertexIndex1 + 1;
			aIndex[u * 6 + 5] = uVertexIndex1;

			UpdateTN(aVertex[aIndex[u * 6]], aVertex[aIndex[u * 6 + 1]], aVertex[aIndex[u * 6 + 2]]);
			UpdateTN(aVertex[aIndex[u * 6 + 3]], aVertex[aIndex[u * 6 + 4]], aVertex[aIndex[u * 6 + 5]]);

			uVertexIndex0++;
			uVertexIndex1++;
			if ((u + 1) % m_uGridCount == 0)
			{
				uVertexIndex0++;
				uVertexIndex1++;
			}
		}

		m_pMesh->SetVertices(aVertex.ElementCount(), aVertex.GetRawData());
		m_pMesh->SetIndices(aIndex.ElementCount(), (UInt8*)aIndex.GetRawData());
		m_pMesh->SetSubMesh(0, 0, aIndex.ElementCount());

		PhysicsSimulator::Unregister(m_pPhysicsBody);
		m_pCollision->SetTerrainHeightData(m_uGridCount + 1, m_uGridCount + 1, 1.0f, aHeightData.GetRawData());
		m_pCollision->SetScale(f3PosOffset);
		m_pPhysicsBody->SetCollision(m_pCollision);
		PhysicsSimulator::Register(m_pPhysicsBody);

		return True;
	}

	Float1	CTerrainComponent::GetWorldHeight(const Float2& pos)
	{
		Float3 glbPos = GetObjectPosition(False);

		if (pos[0] < glbPos[0] || pos[1] < glbPos[2] ||
			pos[0] > glbPos[0] + m_f3Scale[0] || pos[1] > glbPos[2] + m_f3Scale[2])
			return 0;

		Array<UInt8> PixelDatas;
		m_pHeightMap->GetPixelData(0, 0, PixelDatas);

		UInt32 uMapWidth = m_pHeightMap->GetWidth();
		UInt32 uMapHeight = m_pHeightMap->GetHeight();

		auto GetMapHeight = [uMapWidth, uMapHeight, PixelDatas](Float1 r0, Float1 r1)
		{
			Float1 fHeight = 0;
			UInt32 u0 = (UInt32)(r0 * (uMapWidth - 1) + Math::Ceil(r1 * (uMapHeight - 1))* uMapWidth) * 4;
			assert(u0 < PixelDatas.ElementCount());
			fHeight = PixelDatas[u0];

			fHeight /= 256.0f;
			return fHeight;
		};

		Float1 H = GetMapHeight((pos[0] - glbPos[0]) / m_f3Scale[0], (pos[1] - glbPos[2]) / m_f3Scale[2]) * m_f3Scale[1];

		return H;
	}
	Boolean CTerrainComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		Float1 fValue;
		DeserializerBlock.Load("GridCount", fValue);
		m_uGridCount = (UInt32)fValue;
		SetTerrainGridCount(m_uGridCount);

		DeserializerBlock.Load("Scale", m_f3Scale);
		SetTerrainScale(m_f3Scale);

		String strValue;
		DeserializerBlock.Load("HeightMap", strValue);
		SetTerrainMap(strValue.CString());

		DeserializerBlock.Load("MaterialInstance", strValue);
		MX_RELEASE_INTERFACE(m_pMaterialInstance);
		m_pMaterialInstance = (IAppMaterialInstance*)AppAssetManager::GetAsset(strValue.CString());
		if (m_pMaterialInstance != nullptr)
			m_pMeshInstance->SetMaterialInstance(0, strValue.CString());

		return True;
	}

#if MX_BUILD_EDITOR
	Boolean CTerrainComponent::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("GridCount", (Float1)m_uGridCount);
		Serializer.Save("Scale", m_f3Scale);
		Serializer.Save("HeightMap", GetTerrainMap());
		Serializer.Save("MaterialInstance", m_pMaterialInstance == nullptr ? "" : m_pMaterialInstance->GetName());
		return True;
	}

	Void CTerrainComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyFloat1("GridCount",
			(Float1)m_uGridCount, 1.0f, 1.0f, 256.0f);
		Panel.AddPropertyFloat3("TerrainScale",
			m_f3Scale, 0.01f,
			0.0f, 2048.0f);
		Panel.AddPropertyFile("TerrainMap",
			m_pHeightMap == nullptr ? "" : m_pHeightMap->GetName(), "*.tga,*.dds");
		Panel.AddPropertyFile("MaterialIns",
			m_pMaterialInstance == nullptr ? "" : m_pMaterialInstance->GetName(), "*.mins");

		m_pPhysicsBody->SetDebugShow(True);
	}
	Void CTerrainComponent::OnPanelInactived()
	{
		m_pPhysicsBody->SetDebugShow(False);
	}
	Void CTerrainComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFloat1:
			if (strcmp(pProperty->Name, "GridCount") == 0)
				SetTerrainGridCount((UInt32)((EditorPropertyFloat1*)pProperty)->Value);
			break;
		case IEditorProperty::EFloat3:
			if (strcmp(pProperty->Name, "TerrainScale") == 0)
				SetTerrainScale(((EditorPropertyFloat3*)pProperty)->Value);
			break;
		case IEditorProperty::EFile:
			if (strcmp(pProperty->Name, "TerrainMap") == 0)
				SetTerrainMap(((EditorPropertyFile*)pProperty)->Value);
			else if (strcmp(pProperty->Name, "MaterialIns") == 0)
			{
				MX_RELEASE_INTERFACE(m_pMaterialInstance);
				m_pMaterialInstance =
					(IAppMaterialInstance*)AppAssetManager::GetAsset(((EditorPropertyFile*)pProperty)->Value);

				if(m_pMaterialInstance != nullptr)
					m_pMeshInstance->SetMaterialInstance(0, ((EditorPropertyFile*)pProperty)->Value);
			}
			break;
		default:
			break;
		}
	}
#endif
}