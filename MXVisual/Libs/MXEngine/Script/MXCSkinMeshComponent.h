#pragma once
#ifndef _MX_C_SKIN_MESH_COMPONENT_
#define _MX_C_SKIN_MESH_COMPONENT_
#include "MXCComponent.h"
#include "MXMath.h"
#include "MXArray.h"
#include "MXCEditorPanel.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	class CSkinMeshComponent
		: public ICComponent
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(CSkinMeshComponent);
	public:
		CSkinMeshComponent();
		~CSkinMeshComponent();

		Boolean				SetMesh(const Char* szMeshName);
		Boolean				SetSkeleton(const Char* szSkeletonName);
		Boolean				SetAnimator(const Char* szAnimatorName);

		const Char*			GetMeshName();
		const Char*			GetSkeletonName();
		const Char*			GetAnimatorName();

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif

	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnUpdateLate() override;
		Void	OnRender() override;
		Void	OnRelease() override;

		Void	UpdateTransform(Float1 fAnimationTime);
	protected:
		class IAppSkeleton* m_pSkeleton;
		class IAppMeshInstance* m_pMeshInstance;
		class IAppAnimationClip* m_pAnimator;

		UInt32 m_uMaterialInsCount;
	};
}
#endif