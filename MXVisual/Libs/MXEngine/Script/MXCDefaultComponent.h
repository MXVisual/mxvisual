#pragma once
#ifndef _MXC_DEFAULT_COMPONENT_
#define _MXC_DEFAULT_COMPONENT_

//Engine Basic Component
#include "MXCRigidMeshComponent.h"
#include "MXCSkinMeshComponent.h"
#include "MXCTerrainComponent.h"
#include "MXCRigidBodyComponent.h"
#include "MXCAnimatorComponent.h"
#include "MXCUIButtonComponent.h"
#include "MXCUIImageComponent.h"
#include "MXCUITextComponent.h"

#include "MXCAudioComponent.h"

#include "MXCDirectionLightComponent.h"
#include "MXCSpotLightComponent.h"
#include "MXCPointLightComponent.h"

#include "MXCParticleSystemComponent.h"

#include "MXCReflectionProbeComponent.h"

#include "MXCPostProcessAreaComponent.h"

#endif