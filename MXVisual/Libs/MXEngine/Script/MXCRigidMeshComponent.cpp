#include "MXCRigidMeshComponent.h"
#include "MXInterface.h"
#include "MXAsset.h"
#include "MXFile.h"
#include "MXEngine.h"

namespace MXVisual
{
	CRigidMeshComponent::CRigidMeshComponent()
		: m_pMeshInstance(nullptr)
	{
	}
	CRigidMeshComponent::~CRigidMeshComponent()
	{
		OnRelease();
	}

	Void	CRigidMeshComponent::OnInit()
	{

	}
	Void	CRigidMeshComponent::OnRender()
	{
		if (m_pMeshInstance != nullptr)
		{
			Matrix4 matL2W = GetObjectTransform(False);
			m_pMeshInstance->SetTransforms(1, &matL2W);
			EngineRenderMesh(m_pMeshInstance);
		}
	}
	Void	CRigidMeshComponent::OnRelease()
	{
		MX_RELEASE_INTERFACE(m_pMeshInstance);
	}

	Boolean	CRigidMeshComponent::SetMesh(const Char* szMeshName)
	{
		if (m_pMeshInstance == nullptr ||
			m_strMeshAssetName != szMeshName)
		{
			IAppMesh* pMesh = (IAppMesh*)AppAssetManager::GetAsset(szMeshName);
			m_uMaterialInsCount = pMesh->GetSubMeshCount();
			m_strMeshAssetName = pMesh->GetName();

			MX_RELEASE_INTERFACE(m_pMeshInstance);
			m_pMeshInstance = IAppMeshInstance::Create(
				String::Format("%s_Instance", szMeshName).CString(), szMeshName);

			MX_RELEASE_INTERFACE(pMesh)
		}
		return m_pMeshInstance != nullptr;
	}

	Boolean CRigidMeshComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		String strValue;
		DeserializerBlock.Load("Mesh", strValue);
		if (!strValue.IsEmpty())
		{
			SetMesh(strValue.CString());
		}

		for (UInt32 u = 0; u < m_uMaterialInsCount; u++)
		{
			String strMaterialInstanceName;
			DeserializerBlock.Load(
				String::Format("MaterialInstance%d", u).CString(), strMaterialInstanceName);
			if (!strMaterialInstanceName.IsEmpty())
			{
				m_pMeshInstance->SetMaterialInstance(u, strMaterialInstanceName.CString());
			}
		}

		return True;
	}

#if MX_BUILD_EDITOR
	Boolean CRigidMeshComponent::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("Mesh", GetMeshName());

		for (UInt32 u = 0; u < m_uMaterialInsCount; u++)
		{
			Serializer.Save(
				String::Format("MaterialInstance%d", u).CString(),
				m_pMeshInstance->GetMaterialInstanceName(u));
		}
		return True;
	}
	Void CRigidMeshComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyFile("Mesh", GetMeshName(), IAppMesh::Suffix);

		for (UInt32 u = 0; u < m_uMaterialInsCount; u++)
		{
			Panel.AddPropertyFile(
				String::Format("MaterialInstance%d", u).CString(),
				m_pMeshInstance->GetMaterialInstanceName(u), IAppMaterialInstance::Suffix);
		}
	}
	Void CRigidMeshComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFile:
			if (strcmp(pProperty->Name, "Mesh") == 0)
			{
				SetMesh(((EditorPropertyFile*)pProperty)->Value);
			}
			else
			{
				SInt32 nIndex;
				sscanf(pProperty->Name, "MaterialInstance%d", &nIndex);
				m_pMeshInstance->SetMaterialInstance(nIndex, ((EditorPropertyFile*)pProperty)->Value);
			}
			break;
		}
	}
#endif
}