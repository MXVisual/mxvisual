#include "MXCPointLightComponent.h"
#include "MXMath.h"
#include "MXGraphicsResource.h"
#include "MXGraphicsRenderer.h"

namespace MXVisual
{
	struct CPointLight : public IGPointLight
	{
		Float3 f3Color;
		Float1 fCandela;
		Float3 f3Position;
		Float1 fRange;
		Float1 fAttenuation;
		Boolean bCastShadow;
		Boolean bAtmosphericScatter;

	public:
		Boolean IsCastShadow() const override
		{
			return bCastShadow;
		}
		Boolean IsAtmosphericScatter() const override
		{
			return bAtmosphericScatter;
		}
		Float3	GetColor() const override
		{
			return f3Color;
		}
		Float1	GetCandela() const override
		{
			return fCandela;
		}
		Float3	GetLightPosition() const override
		{
			return f3Position;
		}
		Float1	GetLightRange() const override
		{
			return fRange;
		}
		Float1	GetLightAttenuation() const override
		{
			return fAttenuation;
		}
	};

	CPointLightComponent::CPointLightComponent()
		: m_pLight(nullptr)
	{
		CPointLight* pLight = new CPointLight;
		pLight->bCastShadow = False;
		pLight->f3Color = Float3(1, 1, 1);
		pLight->fCandela = 3.0f * Constant_PI * 4.0f;
		pLight->f3Position = Float3();
		pLight->fRange = 8;
		pLight->fAttenuation = 2;
		pLight->bAtmosphericScatter = False;

		m_pLight = pLight;
	}

	CPointLightComponent::~CPointLightComponent()
	{
		delete m_pLight;
	}

	Void CPointLightComponent::OnInit()
	{

	}
	Void CPointLightComponent::OnRender()
	{
		CPointLight* pLight = (CPointLight*)m_pLight;
		pLight->f3Position = GetObjectPosition(False);

		GraphicsRenderer::RegisterLight(m_pLight);
	}

	Boolean CPointLightComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		CPointLight* pLight = (CPointLight*)m_pLight;

		String strName;
		DeserializerBlock.Load("Candela", pLight->fCandela);
		DeserializerBlock.Load("Color", pLight->f3Color);
		DeserializerBlock.Load("Range", pLight->fRange);
		DeserializerBlock.Load("Attenuation", pLight->fAttenuation);
		DeserializerBlock.Load("CastShadow", pLight->bCastShadow);
		DeserializerBlock.Load("AtmosphericScatter", pLight->bCastShadow);

		return True;
	}

#if MX_BUILD_EDITOR
	Void CPointLightComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		CPointLight* pLight = (CPointLight*)m_pLight;

		Panel.AddPropertyFloat1("Candela", pLight->fCandela, 0.01f, 0.0f, 512.0f);
		Panel.AddPropertyColor("Color",
			Float4(pLight->f3Color[0], pLight->f3Color[1], pLight->f3Color[2], 1));
		Panel.AddPropertyFloat1("Range", pLight->fRange, 0.01f, 0.0f, 1000.0f);
		Panel.AddPropertyFloat1("Attenuation", pLight->fAttenuation, 0.01f, 0.0f, 1000.0f);
		Panel.AddPropertyTrigger("CastShadow", pLight->bCastShadow);
		Panel.AddPropertyTrigger("AtmosphericScatter", pLight->bAtmosphericScatter);
	}

	Void CPointLightComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		CPointLight* pLight = (CPointLight*)m_pLight;

		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFloat1:
		{
			Float1 value = ((EditorPropertyFloat1*)pProperty)->Value;
			if (strcmp(pProperty->Name, "Candela") == 0)
			{
				pLight->fCandela = value;
			}
			else if (strcmp(pProperty->Name, "Range") == 0)
			{
				pLight->fRange = value;
			}
			else if (strcmp(pProperty->Name, "Attenuation") == 0)
			{
				pLight->fAttenuation = value;
			}
			break;
		}
		case IEditorProperty::EColor:
			pLight->f3Color = MXVisual::Math::Split(((EditorPropertyColor*)pProperty)->Value);
			break;
		case IEditorProperty::ETrigger:
		{
			Boolean value = ((EditorPropertyTrigger*)pProperty)->Value;
			if (strcmp(pProperty->Name, "CastShadow") == 0)
			{
				pLight->bCastShadow = value;
			}
			else if (strcmp(pProperty->Name, "AtmosphericScatter") == 0)
			{
				pLight->bAtmosphericScatter = value;
			}
			break;
		}
		default: break;
		}
	}

	Boolean CPointLightComponent::OnSerialize(ISerializer& Serializer)
	{
		CPointLight* pLight = (CPointLight*)m_pLight;

		Serializer.Save("Candela", pLight->fCandela);
		Serializer.Save("Color", pLight->f3Color);
		Serializer.Save("Range", pLight->fRange);
		Serializer.Save("Attenuation", pLight->fAttenuation);
		Serializer.Save("CastShadow", pLight->bCastShadow);

		return True;
	}

	Void CPointLightComponent::OnEditorRender(IEditorGraphics& Graphics)
	{
		CPointLight* pLight = (CPointLight*)m_pLight;

		Graphics.ChangeColor(Float4(0.8f, 0.8f, 0.0f, 0.2f));
		Graphics.RenderSphere(pLight->f3Position,
			Float3(pLight->fRange, pLight->fRange, pLight->fRange),
			Math::AXIS_X, Math::AXIS_Y);
	}
#endif
}