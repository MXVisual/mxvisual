#pragma once
#ifndef _MX_C_POINTLIGHT_COMPONENT_
#define _MX_C_POINTLIGHT_COMPONENT_
#include "MXCComponent.h"
#include "MXCEditorGraphics.h"
#include "MXCEditorPanel.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	class IGPointLight;
	class CPointLightComponent
		: public ICComponent
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorGraphics
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(CPointLightComponent);
	public:
		CPointLightComponent();
		~CPointLightComponent();

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
		Void	OnEditorRender(IEditorGraphics& Graphics);
#endif

	protected:
		Void	OnInit() override;
		Void	OnRender() override;
		Void	OnRelease() override {};

	protected:
		IGPointLight* m_pLight;
	};
}
#endif