#pragma once
#ifndef _MX_C_AUDIO_COMPONENT_
#define _MX_C_AUDIO_COMPONENT_
#include "MXCComponent.h"
#include "CAudioComponent.h"

namespace MXVisual
{
	class MXCAudioComponent
		: public ICComponent
		, public CAudioComponent
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(CAudioComponent);
	public:
		MXCAudioComponent();
		~MXCAudioComponent();

		Void Play();
		Void Stop();
		Boolean IsPlaying();

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif

	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRelease() override;

	protected:
		Boolean		m_bAutoPlay;
		class IAppAudio*	m_pAudio;
	};
}
#endif