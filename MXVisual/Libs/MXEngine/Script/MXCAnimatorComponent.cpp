#include "MXCAnimatorComponent.h"
#include "MXMath.h"
#include "MXTimer.h"

namespace MXVisual
{
	CAnimatorComponent::CAnimatorComponent()
		:m_nActiveAnimTrack(-1)
	{
	}
	CAnimatorComponent::~CAnimatorComponent()
	{}
	Void	CAnimatorComponent::OnInit()
	{}
	Void	CAnimatorComponent::OnUpdate()
	{
	}
	Void	CAnimatorComponent::OnRelease()
	{}
	Void	CAnimatorComponent::Stop()
	{
	}

	Void	CAnimatorComponent::PlayAnimation(const Char* szTrackName)
	{
	}

	Void	CAnimatorComponent::AddAnimation(const Char* szTrackName)
	{
	}

}