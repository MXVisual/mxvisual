#pragma once
#ifndef _MX_PARTICLESYSTEM_COMPONENT_
#define _MX_PARTICLESYSTEM_COMPONENT_
#include "MXCComponent.h"
#include "MXMath.h"
#include "MXChain.h"
#include "MXAsset.h"

#include "MXCEditorPanel.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	struct Particle
	{
		Float1 fCurTime;
		Float3 f3Position;

		Float4 f4Color;

		Float2 f2Scale;

		Float3 f3Speed;//Init By Forward

		Float3 f3Accelerate;//Init By Right

		Float3 f3DirUp;//Init By Up
	};

	enum EmitterType
	{
		EET_Sphere,
		EET_Cylinder,
	};

	enum ParticleRenderType
	{
		EPRT_Billboard,
		EPRT_VelocityBillboard,
		EPRT_UprightBillboard,
		EPRT_LieBillboard,
		EPRT_Free,
		EPRT_Count
	};

	struct BaseEmitter
	{
		Float1 fEmitSpeed = 1.0f;
		Float1 fEmitAccelerate = 0.0f;

		virtual	Void	Emit(Particle* pParticle);

		virtual	Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		virtual	Void	OnPanelActived(IEditorPropertyPanel& Panel);
		virtual	Void	OnPanelChanged(const IEditorProperty* pProperty);

		virtual	Boolean	OnSerialize(ISerializer& Serializer);
#endif
	};

	class CParticleSystemComponent 
		: public ICComponent
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(CParticleSystemComponent);
	public:
		CParticleSystemComponent();
		~CParticleSystemComponent();

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif

	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRender() override;
		Void	OnRelease() override;

		Void EmitParticle();
		Void ConvertPrimitive();
		Void UpdatePrimitive();

		IAppMesh *m_pMesh;
		IAppMeshInstance *m_pMeshInstance;
		IAppMaterialInstance* m_pMaterialInstance;

		IAppMesh::VertexRigid* m_pVertexData;

		ParticleRenderType m_eRenderType;
		UInt32	m_uActiveParticleCount;
		Chain<Particle> m_ActiveParticle;
		Chain<Particle> m_DumpParticle;

		DEFINE_CAUTOVAR(Float1, LifeTime);

		DEFINE_CAUTOVAR(Float4, BaseColor);

		DEFINE_CAUTOVAR(UInt32, MaxSurviveParticleCount);
		DEFINE_CAUTOVAR(UInt32, EmitRate);

		DEFINE_CAUTOVAR(Float2, BaseScale);

		DEFINE_CAUTOVAR(Float2, UVScale);
		DEFINE_CAUTOVAR(UInt32, UVAnimationCount);

		EmitterType		m_eEmitterType;
		BaseEmitter*	m_pEmitter;
	};
}
#endif