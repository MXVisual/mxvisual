#pragma once
#ifndef _MX_C_POSTPROCESS_AREA_COMPONENT_
#define _MX_C_POSTPROCESS_AREA_COMPONENT_
#include "MXCComponent.h"
#include "MXCEditorGraphics.h"
#include "MXCEditorPanel.h"

namespace MXVisual
{
	class CPostProcessAreaComponent 
		: public ICComponent
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
		, public ICSupportedEditorGraphics
#endif
	{
		DECLARE_RTTI_INFO(CPostProcessAreaComponent);
	public:
		CPostProcessAreaComponent();
		~CPostProcessAreaComponent();

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelInactived();
		Void	OnPanelChanged(const IEditorProperty* pProperty);
		Void	OnEditorRender(IEditorGraphics& Graphics);
#endif
	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRelease() override;

	protected:
		Float3 m_f3BoxRange;
		Float3 m_f3AmbientColor;
		Float1 m_fAmbientIntensity;
	};
}
#endif