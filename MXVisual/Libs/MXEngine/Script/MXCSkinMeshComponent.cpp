#include "MXCSkinMeshComponent.h"
#include "MXInterface.h"
#include "MXTimer.h"
#include "MXAsset.h"
#include "MXFile.h"
#include "MXEngine.h"

namespace MXVisual
{
	CSkinMeshComponent::CSkinMeshComponent()
		: m_pMeshInstance(nullptr)
		, m_pSkeleton(nullptr)
		, m_pAnimator(nullptr)
		, m_uMaterialInsCount(0)
	{
	}
	CSkinMeshComponent::~CSkinMeshComponent()
	{
		OnRelease();
	}

	Void	CSkinMeshComponent::OnInit()
	{
		UpdateTransform(0.0f);
	}
	Void	CSkinMeshComponent::OnUpdate()
	{
		Float1 fTime = 0.0f;
		if (m_pAnimator != nullptr)
		{
			fTime = m_pAnimator->GetSamplingTime() + (Float1)Timer::GetDeltaTime();
			fTime = fTime - (SInt32)(fTime / m_pAnimator->GetTotalTime()) * m_pAnimator->GetTotalTime();
		}

		UpdateTransform(fTime);
	}
	Void	CSkinMeshComponent::OnUpdateLate()
	{

	}
	Void	CSkinMeshComponent::OnRender()
	{
		if (m_pMeshInstance != nullptr)
		{
			EngineRenderMesh(m_pMeshInstance);
		}
	}
	Void	CSkinMeshComponent::OnRelease()
	{
		MX_RELEASE_INTERFACE(m_pMeshInstance);
		MX_RELEASE_INTERFACE(m_pSkeleton);
		MX_RELEASE_INTERFACE(m_pAnimator);
	}
	Boolean			CSkinMeshComponent::SetMesh(const Char* szMeshName)
	{
		if (m_pMeshInstance == nullptr ||
			strcmp(GetMeshName(), szMeshName) != 0)
		{
			IAppMesh* pMesh = (IAppMesh*)AppAssetManager::GetAsset(szMeshName);
			if (pMesh == nullptr)return False;

			m_uMaterialInsCount = pMesh->GetSubMeshCount();

			MX_RELEASE_INTERFACE(m_pMeshInstance);
			m_pMeshInstance = IAppMeshInstance::Create(
				String::Format("%s_Instance", szMeshName).CString(), szMeshName);

			MX_RELEASE_INTERFACE(pMesh);
		}
		return m_pMeshInstance != nullptr;
	}
	Boolean			CSkinMeshComponent::SetSkeleton(const Char* szSkeletonName)
	{
		if (m_pSkeleton == nullptr ||
			strcmp(m_pSkeleton->GetName(), szSkeletonName) != 0)
		{
			MX_RELEASE_INTERFACE(m_pSkeleton);
			m_pSkeleton = (IAppSkeleton*)AppAssetManager::GetAsset(szSkeletonName);
		}
		return m_pSkeleton != nullptr;
	}
	Boolean			CSkinMeshComponent::SetAnimator(const Char* szAnimatorName)
	{
		if (m_pAnimator == nullptr ||
			strcmp(m_pAnimator->GetName(), szAnimatorName) != 0)
		{
			MX_RELEASE_INTERFACE(m_pAnimator);
			m_pAnimator = (IAppAnimationClip*)AppAssetManager::GetAsset(szAnimatorName);
		}
		return m_pAnimator != nullptr;
	}

	Void	CSkinMeshComponent::UpdateTransform(Float1 fAnimationTime)
	{
		if (m_pMeshInstance == nullptr)return;

		Matrix4 matGlobal = GetObjectTransform(False);
		Array<Matrix4> arrTransform;
		if (m_pAnimator != nullptr && m_pSkeleton != nullptr)
		{
			m_pAnimator->Sampling(
				m_pMeshInstance->GetMesh(),
				m_pSkeleton,
				arrTransform);
			for (UInt32 u = 0; u < m_pMeshInstance->GetMesh()->GetBindingBoneCount(); u++)
			{
				arrTransform[u] = arrTransform[u] * matGlobal;
			}
			m_pAnimator->SetSamplingTime(fAnimationTime);
		}
		else if (m_pMeshInstance != nullptr)
		{
			for (UInt32 u = 0; u < m_pMeshInstance->GetMesh()->GetBindingBoneCount(); u++)
			{
				arrTransform.Add(matGlobal);
			}
		}
		m_pMeshInstance->SetTransforms(arrTransform.ElementCount(), arrTransform.GetRawData());
	}

	const Char*		CSkinMeshComponent::GetMeshName()
	{
		return m_pMeshInstance != nullptr &&  m_pMeshInstance->GetMesh() != nullptr ?
			m_pMeshInstance->GetMesh()->GetName() : "";
	}
	const Char*		CSkinMeshComponent::GetSkeletonName()
	{
		return m_pSkeleton != nullptr ? m_pSkeleton->GetName() : "";
	}
	const Char*		CSkinMeshComponent::GetAnimatorName()
	{
		return m_pAnimator != nullptr ? m_pAnimator->GetName() : "";
	}

	Boolean CSkinMeshComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		String strValue;

		DeserializerBlock.Load("Skeleton", strValue);
		SetSkeleton(strValue.CString());

		DeserializerBlock.Load("Mesh", strValue);
		SetMesh(strValue.CString());

		for (UInt32 u = 0; u < m_uMaterialInsCount; u++)
		{
			String strMaterialInstanceName;
			DeserializerBlock.Load(
				String::Format("MaterialInstance%d", u).CString(), strMaterialInstanceName);
			if (!strMaterialInstanceName.IsEmpty())
			{
				m_pMeshInstance->SetMaterialInstance(u, strMaterialInstanceName.CString());
			}
		}

		DeserializerBlock.Load("Animator", strValue);
		SetAnimator(strValue.CString());

		return True;
	}

#if MX_BUILD_EDITOR
	Boolean CSkinMeshComponent::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("Skeleton", GetSkeletonName());
		Serializer.Save("Mesh", GetMeshName());
		Serializer.Save("Animator", GetAnimatorName());

		for (UInt32 u = 0; u < m_uMaterialInsCount; u++)
		{
			Serializer.Save(
				String::Format("MaterialInstance%d", u).CString(),
				m_pMeshInstance->GetMaterialInstanceName(u));
		}
		return True;
	}
	Void CSkinMeshComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyFile("Skeleton", GetSkeletonName(), String::Format("*.%s", IAppSkeleton::Suffix).CString());
		Panel.AddPropertyFile("Mesh", GetMeshName(), String::Format("*.%s", IAppMesh::Suffix).CString());
		Panel.AddPropertyFile("Animator", GetAnimatorName(), String::Format("*.%s", IAppAnimationClip::Suffix).CString());

		for (UInt32 u = 0; u < m_uMaterialInsCount; u++)
		{
			Panel.AddPropertyFile(String::Format("MaterialInstance%d", u).CString(),
				m_pMeshInstance->GetMaterialInstanceName(u),
				String::Format("*.%s", IAppMaterialInstance::Suffix).CString());
		}
	}
	Void CSkinMeshComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFile:
			if (strcmp(pProperty->Name, "Skeleton") == 0)
			{
				SetSkeleton(((EditorPropertyFile*)pProperty)->Value);
			}
			else if (strcmp(pProperty->Name, "Mesh") == 0)
			{
				SetMesh(((EditorPropertyFile*)pProperty)->Value);
			}
			else if (strcmp(pProperty->Name, "Animator") == 0)
			{
				SetAnimator(((EditorPropertyFile*)pProperty)->Value);
			}
			else
			{
				SInt32 nIndex;
				sscanf(pProperty->Name, "MaterialInstance%d", &nIndex);
				m_pMeshInstance->SetMaterialInstance(nIndex, ((EditorPropertyFile*)pProperty)->Value);
			}
			break;
		}

		UpdateTransform(0.0f);
	}
#endif
}