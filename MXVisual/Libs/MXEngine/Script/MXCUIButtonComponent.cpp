#include "MXCUIButtonComponent.h"
#include "MXCScene.h"
#include "MXAsset.h"

namespace MXVisual
{
	IAppMaterial* g_pUIButtonMtl = nullptr;
	UInt32	g_IDButtonPrimitive = 0;

	CUIButtonComponent::CUIButtonComponent()
		: m_eButtonState(ButtonState::eIdle)
		, m_f4Color(1, 1, 1, 1)
	{

	}
	CUIButtonComponent::~CUIButtonComponent()
	{
		OnRelease();
	}

	Void CUIButtonComponent::SetIdleTextureName(const Char* szTextureName)
	{
		if (m_strIdleTextureName != szTextureName)
		{
			m_strIdleTextureName = szTextureName;
			ASSERT_IF_FAILED(m_pMaterialIns != nullptr);
			m_pMaterialIns->SetTexture2D("NormalTex", szTextureName);
		}
	}
	Void CUIButtonComponent::SetHoverTextureName(const Char* szTextureName)
	{
		if (m_strHoverTextureName != szTextureName)
		{
			m_strHoverTextureName = szTextureName;
			ASSERT_IF_FAILED(m_pMaterialIns != nullptr);
			m_pMaterialIns->SetTexture2D("HoverTex", szTextureName);
		}
	}
	Void CUIButtonComponent::SetClickTextureName(const Char* szTextureName)
	{
		if (m_strClickTextureName != szTextureName)
		{
			m_strClickTextureName = szTextureName;
			ASSERT_IF_FAILED(m_pMaterialIns != nullptr);
			m_pMaterialIns->SetTexture2D("ClickTex", szTextureName);
		}
	}

	Void CUIButtonComponent::SetColor(const Float4& f4Color)
	{
		if (m_f4Color != f4Color)
		{
			m_f4Color = f4Color;

			IAppMesh::VertexRigid2D vertex[4];
			vertex[0].f2Position = Float2(0, 0);
			vertex[0].f2TexCoord0 = Float2(0, 0);
			vertex[0].f4Color = m_f4Color;

			vertex[1].f2Position = Float2(1.0f, 0);
			vertex[1].f2TexCoord0 = Float2(1, 0);
			vertex[1].f4Color = m_f4Color;

			vertex[2].f2Position = Float2(0, 1.0f);
			vertex[2].f2TexCoord0 = Float2(0, 1);
			vertex[2].f4Color = m_f4Color;

			vertex[3].f2Position = Float2(1.0f, 1.0f);
			vertex[3].f2TexCoord0 = Float2(1, 1);
			vertex[3].f4Color = m_f4Color;

			m_pMesh->SetVertices(4, vertex);
		}
	}

	Void CUIButtonComponent::OnInit()
	{
		CComponent2D::OnInit();

		if (g_pUIButtonMtl == nullptr)
		{
			g_pUIButtonMtl = IAppMaterial::Create("_UIButton_mtl");
			g_pUIButtonMtl->SetDomain(IAppMaterial::Domain::EDM_ScreenSpace);
			g_pUIButtonMtl->SetBlendMode(IAppMaterial::BlendMode::EBM_Translucency);
			g_pUIButtonMtl->SetFaceType(IAppMaterial::FaceType::EFT_Frontface);
			g_pUIButtonMtl->SetShadingModel(IAppMaterial::ShadingModel::ESM_Nolighting);

			g_pUIButtonMtl->SetParameter(
				IAppMaterial::ParameterDescription(
					"NormalTex", IAppMaterial::EPT_Texture2D, DefaultResource::MOSAIC_TEXTURE)
			);

			g_pUIButtonMtl->SetParameter(
				IAppMaterial::ParameterDescription(
					"ClickTex", IAppMaterial::EPT_Texture2D, DefaultResource::MOSAIC_TEXTURE)
			);

			g_pUIButtonMtl->SetParameter(
				IAppMaterial::ParameterDescription(
					"HoverTex", IAppMaterial::EPT_Texture2D, DefaultResource::MOSAIC_TEXTURE)
			);

			g_pUIButtonMtl->SetParameter(
				IAppMaterial::ParameterDescription(
					"ButtonState", IAppMaterial::EPT_Float1, Float4())
			);

			g_pUIButtonMtl->SetMaterialFunc(IAppMaterial::EMF_SurfaceParams,
				"float4 color = float4(0,0,0,1);\n\
				float State = ButtonState * 3;\n\
				if (State < 0.5f)\n\
					color = Tex2D_Sample(NormalTex, input.TexCoord0);\n\
				else if (State < 1.5f)\n\
					color = Tex2D_Sample(ClickTex, input.TexCoord0);\n\
				else if (State < 2.5f)\n\
					color = Tex2D_Sample(HoverTex, input.TexCoord0);\n\
				OUTPUT_EMISSIVE(PCFunc_Color_GammaCorrect(color.rgb, 2.2f) * PCFunc_Color_GammaCorrect(input.VertexColor.rgb, 2.2f));\n\
				OUTPUT_OPACITY(color.a * input.VertexColor.a);\n\
				OUTPUT_ROUGHNESS(0.5f);\n\
				OUTPUT_METALLIC(0.0f);\n");

			g_pUIButtonMtl->Recompile();
		}

		if (m_pMaterialIns == nullptr)
		{
			m_pMaterialIns = IAppMaterialInstance::Create(
				String::Format("UIButton_mtlins_%d", g_IDButtonPrimitive).CString(), g_pUIButtonMtl->GetName());

			m_pMaterialIns->SetTexture2D("NormalTex", m_strIdleTextureName.CString());
			m_pMaterialIns->SetTexture2D("ClickTex", m_strClickTextureName.CString());
			m_pMaterialIns->SetTexture2D("HoverTex", m_strHoverTextureName.CString());
		}

		if (m_pMesh == nullptr)
		{
			m_pMesh = IAppMesh::Create(
				String::Format("UIButton_mesh_%d", g_IDButtonPrimitive).CString(),
				IAppMesh::EAPPMT_Rigid2D, True, 4, 6, 1);

			m_pMeshInstance = IAppMeshInstance::Create(
				String::Format("UIButton_mesh_ins_%d", g_IDButtonPrimitive).CString(),
				m_pMesh->GetName());

			IAppMesh::VertexRigid2D vertex[4];
			vertex[0].f2Position = Float2(0, 0);
			vertex[0].f2TexCoord0 = Float2(0, 0);
			vertex[0].f4Color = m_f4Color;

			vertex[1].f2Position = Float2(1.0f, 0);
			vertex[1].f2TexCoord0 = Float2(1, 0);
			vertex[1].f4Color = m_f4Color;

			vertex[2].f2Position = Float2(0, 1.0f);
			vertex[2].f2TexCoord0 = Float2(0, 1);
			vertex[2].f4Color = m_f4Color;

			vertex[3].f2Position = Float2(1.0f, 1.0f);
			vertex[3].f2TexCoord0 = Float2(1, 1);
			vertex[3].f4Color = m_f4Color;

			m_pMesh->SetVertices(4, vertex);

			UInt32 uIndex[6] = { 0,1,2,1,3,2 };
			m_pMesh->SetIndices(6, (UInt8*)uIndex);
			m_pMesh->SetSubMesh(0, 0, 6);

			g_IDButtonPrimitive++;
		}
	}
	Void CUIButtonComponent::OnUpdate()
	{
		CComponent2D::OnUpdate();

		m_pMaterialIns->SetFloat1("ButtonState", (Float1)m_eButtonState / 3.0f);
	}
	Void CUIButtonComponent::OnRelease()
	{
		CComponent2D::OnRelease();

		if (g_pUIButtonMtl != nullptr && g_pUIButtonMtl->RefCount() <= 1)
		{
			g_pUIButtonMtl->Release();
			g_pUIButtonMtl = nullptr;
		}
	}

	Void CUIButtonComponent::OnFocused()
	{
		if (Input::GetKeyDown(Input::eLButton))
		{
			m_eButtonState = ButtonState::eClick;
		}
		else
		{
			m_eButtonState = ButtonState::eHover;
		}
	}
	Void CUIButtonComponent::OnLostFocus()
	{
		m_eButtonState = ButtonState::eIdle;
	}

	Boolean CUIButtonComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		String strValue;

		DeserializerBlock.Load("IdleTexture", m_strIdleTextureName);
		DeserializerBlock.Load("ClickTexture", m_strClickTextureName);
		DeserializerBlock.Load("HoverTexture", m_strHoverTextureName);

		Float4 fValue;
		DeserializerBlock.Load("Color", fValue);
		SetColor(fValue);
		return True;
	}

#if MX_BUILD_EDITOR
	Boolean CUIButtonComponent::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("IdleTexture", m_strIdleTextureName.CString());
		Serializer.Save("ClickTexture", m_strClickTextureName.CString());
		Serializer.Save("HoverTexture", m_strHoverTextureName.CString());
		Serializer.Save("Color", m_f4Color);
		return True;
	}
	Void CUIButtonComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyFile("IdleTexture", m_strIdleTextureName.CString(), IAppTexture::Suffix);
		Panel.AddPropertyFile("ClickTexture", m_strClickTextureName.CString(), IAppTexture::Suffix);
		Panel.AddPropertyFile("HoverTexture", m_strHoverTextureName.CString(), IAppTexture::Suffix);
		Panel.AddPropertyColor("Color", m_f4Color);
	}
	Void CUIButtonComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFile:
			if (strcmp(pProperty->Name, "IdleTexture") == 0)
			{
				SetIdleTextureName(((EditorPropertyFile*)pProperty)->Value);
			}
			else if (strcmp(pProperty->Name, "ClickTexture") == 0)
			{
				SetClickTextureName(((EditorPropertyFile*)pProperty)->Value);
			}
			else if (strcmp(pProperty->Name, "HoverTexture") == 0)
			{
				SetHoverTextureName(((EditorPropertyFile*)pProperty)->Value);
			}
			break;
		case IEditorProperty::EColor:
			if (strcmp(pProperty->Name, "Color") == 0)
			{
				SetColor(((EditorPropertyColor*)pProperty)->Value);
			}
			break;
		}
	}
#endif

}