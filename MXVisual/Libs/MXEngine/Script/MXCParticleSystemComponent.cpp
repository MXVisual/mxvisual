#include "MXCParticleSystemComponent.h"
#include "MXGraphicsResource.h"
#include "MXTimer.h"
#include "MXCScene.h"
#include "MXCCamera.h"
#include "MXGraphicsRenderer.h"
#include "MXEngine.h"

#include "MXFile.h"

namespace MXVisual
{
#pragma region EMITTER
	struct SphereEmitter : public BaseEmitter
	{
		DEFINE_CAUTOVAR(Float2, EmitAngle);

		SphereEmitter()
		{
			EmitAngle = Float2(Math::Degree2Radian(360.0f), Math::Degree2Radian(360.0f));
		}

		Void	Emit(Particle* pParticle) override
		{
			Float2 randAngle = Float2(Math::Random01(), Math::Random01()) * EmitAngle - 0.5f;

			Matrix4 mat = Math::MatrixAxisRotation(pParticle->f3Accelerate, randAngle[0]) *
				Math::MatrixAxisRotation(pParticle->f3DirUp, randAngle[1]);

			Float3 EmitDir = Math::MatrixTransform(pParticle->f3Speed, Math::Split(mat));

			pParticle->f3Position = Float3();
			pParticle->f3Speed = EmitDir * fEmitSpeed;
			pParticle->f3Accelerate = EmitDir * fEmitAccelerate;
		}

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override
		{
			BaseEmitter::OnDeserialize(DeserializerBlock);

			Float2 data;
			DeserializerBlock.Load(EmitAngle.VarName, data);
			EmitAngle = Float2(Math::Degree2Radian(data[0]), Math::Degree2Radian(data[1]));

			return True;
		}

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel)
		{
			BaseEmitter::OnPanelActived(Panel);

			Float2 data = Float2(Math::Radian2Degree(((Float2&)EmitAngle)[0]), Math::Radian2Degree(((Float2&)EmitAngle)[1]));
			Panel.AddPropertyFloat2(EmitAngle.VarName, data, 0.01f, 0.0f, 360.0f);
		}
		Void	OnPanelChanged(const IEditorProperty* pProperty)override
		{
			BaseEmitter::OnPanelChanged(pProperty);

			switch (pProperty->PropertyType)
			{
			case IEditorProperty::EFloat2:
				if (strcmp(pProperty->Name, EmitAngle.VarName) == 0)
				{
					Float2 data = ((EditorPropertyFloat2*)pProperty)->Value;

					EmitAngle = Float2(Math::Degree2Radian(data[0]), Math::Degree2Radian(data[1]));
				}
				return;
			}
		}

		Boolean	OnSerialize(ISerializer& Serializer)override
		{
			BaseEmitter::OnSerialize(Serializer);

			Float2 data = Float2(Math::Radian2Degree(((Float2&)EmitAngle)[0]), Math::Radian2Degree(((Float2&)EmitAngle)[1]));
			Serializer.Save(EmitAngle.VarName, data);

			return True;
		}
#endif
	};
	struct CylinderEmitter : public BaseEmitter
	{
		DEFINE_CAUTOVAR(Float1, CylinderRadius);

		CylinderEmitter()
		{
			CylinderRadius = 1.0f;
		}

		Void	Emit(Particle* pParticle) override
		{
			pParticle->f3Position = pParticle->f3Accelerate * Math::Random01() *  CylinderRadius +
				pParticle->f3DirUp * Math::Random01() * CylinderRadius;
			pParticle->f3Accelerate = pParticle->f3Speed * fEmitAccelerate;
			pParticle->f3Speed = pParticle->f3Speed * fEmitSpeed;
		}

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override
		{
			BaseEmitter::OnDeserialize(DeserializerBlock);

			DeserializerBlock.Load(CylinderRadius.VarName, CylinderRadius);
			return True;
		}

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel)
		{
			BaseEmitter::OnPanelActived(Panel);

			Panel.AddPropertyFloat1(CylinderRadius.VarName, CylinderRadius, 0.01f, 0.0f, 100.0f);
		}
		Void	OnPanelChanged(const IEditorProperty* pProperty)override
		{
			BaseEmitter::OnPanelChanged(pProperty);

			switch (pProperty->PropertyType)
			{
			case IEditorProperty::EFloat1:
				if (strcmp(pProperty->Name, "Radius") == 0)
				{
					CylinderRadius = ((EditorPropertyFloat1*)pProperty)->Value;
				}
				return;
			}
		}

		Boolean	OnSerialize(ISerializer& Serializer)override
		{
			BaseEmitter::OnSerialize(Serializer);

			Serializer.Save(CylinderRadius.VarName, CylinderRadius);

			return True;
		}
#endif
	};

	Void	BaseEmitter::Emit(Particle* pParticle)
	{
		pParticle->f3Position = Float3();
		pParticle->f3Accelerate = pParticle->f3Speed * fEmitAccelerate;
		pParticle->f3Speed = pParticle->f3Speed * fEmitSpeed;
		pParticle->f3DirUp = Math::AXIS_Y;
	}

	Boolean	BaseEmitter::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		DeserializerBlock.Load("Speed", fEmitSpeed);
		DeserializerBlock.Load("Accelerate", fEmitAccelerate);

		return True;
	}

#if MX_BUILD_EDITOR
	Void	BaseEmitter::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyFloat1("Speed", fEmitSpeed, 0.01f, 0.0f, 100.0f);
		Panel.AddPropertyFloat1("Accelerate", fEmitAccelerate, 0.01f, 0.0f, 100.0f);
	}
	Void	BaseEmitter::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFloat1:
			if (strcmp(pProperty->Name, "Speed") == 0)
			{
				fEmitSpeed = ((EditorPropertyFloat1*)pProperty)->Value;
			}
			else if (strcmp(pProperty->Name, "Accelerate") == 0)
			{
				fEmitAccelerate = ((EditorPropertyFloat1*)pProperty)->Value;
			}
			return;
		}
	}
	Boolean	BaseEmitter::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("Speed", fEmitSpeed);
		Serializer.Save("Accelerate", fEmitAccelerate);
		return True;
	}
#endif
#pragma endregion

	UInt32 g_IDParticle = 0;

	CParticleSystemComponent::CParticleSystemComponent()
		: m_pMesh(nullptr)
		, m_pMeshInstance(nullptr)
		, m_pMaterialInstance(nullptr)
		, m_pVertexData(nullptr)
		, m_uActiveParticleCount(0)
		, m_eEmitterType(EmitterType::EET_Sphere)
		, m_eRenderType(EPRT_Billboard)
	{
		EmitRate = 0;
		MaxSurviveParticleCount = 512;
		BaseScale = Float2(1, 1);
		BaseColor = Float4(1, 1, 1, 1);

		UVScale = Float2(1, 1);
		UVAnimationCount = 1;

		m_pEmitter = new SphereEmitter;
	}
	CParticleSystemComponent::~CParticleSystemComponent()
	{
		if (m_pEmitter != nullptr)
		{
			delete m_pEmitter;
			m_pEmitter = nullptr;
		}
		MX_RELEASE_INTERFACE(m_pMeshInstance);
		MX_RELEASE_INTERFACE(m_pMaterialInstance);
	}

	Void	CParticleSystemComponent::OnInit()
	{
		ConvertPrimitive();
	}
	Void	CParticleSystemComponent::OnUpdate()
	{
		Float1 fDeltaTime = (Float1)Timer::GetDeltaTime();

		m_uActiveParticleCount = 0;
		//Update Exit Particle Life cycle
		ChainNode<Particle>* pParticle = m_ActiveParticle.GetNode(m_uActiveParticleCount);
		while (pParticle != nullptr)
		{
			pParticle->Data.fCurTime -= fDeltaTime;
			if (pParticle->Data.fCurTime <= 0.0f)
			{
				m_ActiveParticle.Pop(m_uActiveParticleCount);

				m_DumpParticle.PushBack(pParticle);

				pParticle = m_ActiveParticle.GetNode(m_uActiveParticleCount);
			}
			else
			{
				pParticle->Data.f3Speed += pParticle->Data.f3Accelerate * fDeltaTime;
				pParticle->Data.f3Position += pParticle->Data.f3Speed * fDeltaTime;

				m_uActiveParticleCount++;

				pParticle = pParticle->Next;
			}
		}

		if (m_uActiveParticleCount < MaxSurviveParticleCount)
		{
			EmitParticle();
			m_uActiveParticleCount++;
		}

		UpdatePrimitive();
	}
	Void	CParticleSystemComponent::OnRender()
	{
		if (m_pMeshInstance != nullptr &&
			m_pMaterialInstance != nullptr)
		{
			Matrix4 matL2W = Math::MatrixTranslation(GetObjectPosition(False));
			m_pMeshInstance->SetTransforms(1, &matL2W);
			m_pMeshInstance->SetMaterialInstance(1, m_pMaterialInstance->GetName());

			EngineRenderMesh(m_pMeshInstance);
		}
	}
	Void	CParticleSystemComponent::OnRelease()
	{
		if (m_pVertexData)
		{
			delete[] m_pVertexData;
			m_pVertexData = nullptr;
		}
		MX_RELEASE_INTERFACE(m_pMeshInstance);
		MX_RELEASE_INTERFACE(m_pMaterialInstance);
	}

	Void	CParticleSystemComponent::EmitParticle()
	{
		ChainNode<Particle>* pParticle = m_DumpParticle.Pop(0);
		if (pParticle == nullptr)
			pParticle = new ChainNode<Particle>;

		pParticle->Data.fCurTime = LifeTime;
		pParticle->Data.f3Speed = GetObjectDirForward();
		pParticle->Data.f3Accelerate = GetObjectDirRight();
		pParticle->Data.f3DirUp = GetObjectDirUp();

		pParticle->Data.f4Color = BaseColor;
		pParticle->Data.f2Scale = BaseScale;

		m_pEmitter->Emit(&pParticle->Data);

		m_ActiveParticle.PushBack(pParticle);
	}

	Void	CParticleSystemComponent::ConvertPrimitive()
	{
		if (m_pVertexData)
		{
			delete[] m_pVertexData;
			m_pVertexData = nullptr;
		}
		m_pVertexData = new IAppMesh::VertexRigid[MaxSurviveParticleCount * 4];

		MX_RELEASE_INTERFACE(m_pMesh);

		m_pMesh = IAppMesh::Create(
			String::Format("Particle_primitive_%d", g_IDParticle).CString(),
			IAppMesh::EAPPMT_Rigid, True,
			MaxSurviveParticleCount * 4,
			MaxSurviveParticleCount * 6, 1);

		if (m_pMesh != nullptr)
		{
			UInt32 *uIndex = new UInt32[MaxSurviveParticleCount * 6]{ 0 };
			for (UInt32 u = 0; u < MaxSurviveParticleCount; u++)
			{
				uIndex[u * 6] = u * 4;
				uIndex[u * 6 + 1] = u * 4 + 1;
				uIndex[u * 6 + 2] = u * 4 + 2;

				uIndex[u * 6 + 3] = u * 4 + 1;
				uIndex[u * 6 + 4] = u * 4 + 3;
				uIndex[u * 6 + 5] = u * 4 + 2;
			}

			m_pMesh->SetIndices(MaxSurviveParticleCount * 6, (UInt8*)uIndex);
			delete[] uIndex;
		}

	}

#pragma region ParticlePrimitiveExpendFunc
	typedef Void(*FCompuateParticleExpend)(
		Particle* pParticle, const CCamera* pCamera,
		Float4& f4Normal,
		Float3& f3Up,
		Float3& f3Right);

	Void	CompuateParticleExpend_Billboard(
		Particle* pParticle, const CCamera* pCamera,
		Float4& f4Normal,
		Float3& f3Up,
		Float3& f3Right)
	{
		f4Normal = Math::Combine(pCamera->GetDirForward() * -1.0f, 0.0f);
		f3Up = Math::Normalize(pCamera->GetDirUp()) * pParticle->f2Scale[1] * 0.5f;
		f3Right = Math::Normalize(pCamera->GetDirRight()) * pParticle->f2Scale[0] * 0.5f;
	}

	Void	CompuateParticleExpend_VelocityBillboard(
		Particle* pParticle, const CCamera* pCamera,
		Float4& f4Normal,
		Float3& f3Up,
		Float3& f3Right)
	{
		f4Normal = Math::Combine(pCamera->GetDirForward() * -1.0f, 0.0f);
		f3Up = Math::Normalize(pParticle->f3Speed);
		f3Right = Math::CrossProduct(Math::Split(f4Normal), f3Up) * pParticle->f2Scale[0] * 0.5f;
		f3Up *= pParticle->f2Scale[1] * 0.5f;
	}

	Void	CompuateParticleExpend_UprightBillboard(
		Particle* pParticle, const CCamera* pCamera,
		Float4& f4Normal,
		Float3& f3Up,
		Float3& f3Right)
	{
		f4Normal = Math::Combine(Math::Normalize(pCamera->GetDirForward() * Float3(-1, 0, -1)), 0.0f);
		f3Right = pCamera->GetDirRight();
		f3Right[1] = 0;
		f3Right = Math::Normalize(f3Right) * pParticle->f2Scale[0] * 0.5f;;
		f3Up = Math::AXIS_Y * pParticle->f2Scale[1] * 0.5f;
	}

	Void	CompuateParticleExpend_LieBillboard(
		Particle* pParticle, const CCamera* pCamera,
		Float4& f4Normal,
		Float3& f3Up,
		Float3& f3Right)
	{
		f4Normal = Float4(0, 1, 0, 0);
		f3Right = pCamera->GetDirRight();
		f3Right[1] = 0;
		f3Right = Math::Normalize(f3Right) * pParticle->f2Scale[0] * 0.5f;

		f3Up = pCamera->GetDirForward();
		f3Up[1] = 0;
		f3Up = Math::Normalize(f3Up) * pParticle->f2Scale[1] * 0.5f;
	}

	Void	CompuateParticleExpend_Free(
		Particle* pParticle, const CCamera* pCamera,
		Float4& f4Normal,
		Float3& f3Up,
		Float3& f3Right)
	{
		f4Normal = Math::Combine(pParticle->f3DirUp, 0.0f);
		f3Up = Math::Normalize(pParticle->f3Speed);
		f3Right = Math::CrossProduct(pParticle->f3DirUp, f3Up) * pParticle->f2Scale[0] * 0.5f;
		f3Up *= pParticle->f2Scale[1] * 0.5f;
	}


	FCompuateParticleExpend CompuateParticleExpendFuncs[ParticleRenderType::EPRT_Count] = {
		CompuateParticleExpend_Billboard,
		CompuateParticleExpend_VelocityBillboard,
		CompuateParticleExpend_UprightBillboard,
		CompuateParticleExpend_LieBillboard,
		CompuateParticleExpend_Free
	};
#pragma endregion

	Void	CParticleSystemComponent::UpdatePrimitive()
	{
		if (!m_pVertexData)
		{
			return;
		}

		auto ComputeUV = [this](Float1 CurLifeTime, Float1 LifeTime) -> Float4
		{
			if (UVAnimationCount == 1)
			{
				return Float4(0, 0, 1, 1);
			}

			UInt32 WidthAnimCount = Math::Ceil(1.0f / ((Float2&)UVScale)[0]);

			UInt32 AnimIndex = (1.0f - CurLifeTime / LifeTime) * UVAnimationCount;

			UInt32 WidthAnimIndex = AnimIndex % WidthAnimCount;
			UInt32 HeightAnimIndex = AnimIndex / WidthAnimCount;

			return Float4(((Float2&)UVScale)[0] * WidthAnimIndex, ((Float2&)UVScale)[1] * HeightAnimIndex,
				((Float2&)UVScale)[0] * (WidthAnimIndex + 1), ((Float2&)UVScale)[1] * (HeightAnimIndex + 1));
		};

		CCamera* pMainCamera = CScene::ActivedScene()->MainCamera();

		if (pMainCamera != nullptr)
		{
			UInt32 uParticle = 0;
			ChainNode<Particle>* pParticle = m_ActiveParticle.GetNode(0);
			while (pParticle != nullptr)
			{
				Float4 f4Normal;
				Float3 f3Up, f3Right;

				CompuateParticleExpendFuncs[m_eRenderType](
					&pParticle->Data, pMainCamera,
					f4Normal, f3Up, f3Right);

				Float4 uv = ComputeUV(pParticle->Data.fCurTime, LifeTime);

				m_pVertexData[uParticle * 4].f4Position =
					Math::Combine(pParticle->Data.f3Position - f3Right + f3Up, 0.0f);
				m_pVertexData[uParticle * 4].f2TexCoord0 = Float2(uv[0], uv[1]);
				m_pVertexData[uParticle * 4].f4Color = pParticle->Data.f4Color;
				m_pVertexData[uParticle * 4].f4Normal = f4Normal;
				m_pVertexData[uParticle * 4].f4Tangent = Math::Combine(f3Right, 0);

				m_pVertexData[uParticle * 4 + 1].f4Position =
					Math::Combine(pParticle->Data.f3Position + f3Right + f3Up, 0.0f);
				m_pVertexData[uParticle * 4 + 1].f2TexCoord0 = Float2(uv[2], uv[1]);
				m_pVertexData[uParticle * 4 + 1].f4Color = pParticle->Data.f4Color;
				m_pVertexData[uParticle * 4 + 1].f4Normal = f4Normal;
				m_pVertexData[uParticle * 4 + 1].f4Tangent = Math::Combine(f3Right, 0);

				m_pVertexData[uParticle * 4 + 2].f4Position =
					Math::Combine(pParticle->Data.f3Position - f3Right - f3Up, 0.0f);
				m_pVertexData[uParticle * 4 + 2].f2TexCoord0 = Float2(uv[0], uv[3]);
				m_pVertexData[uParticle * 4 + 2].f4Color = pParticle->Data.f4Color;
				m_pVertexData[uParticle * 4 + 2].f4Normal = f4Normal;
				m_pVertexData[uParticle * 4 + 2].f4Tangent = Math::Combine(f3Right, 0);

				m_pVertexData[uParticle * 4 + 3].f4Position =
					Math::Combine(pParticle->Data.f3Position + f3Right - f3Up, 0.0f);
				m_pVertexData[uParticle * 4 + 3].f2TexCoord0 = Float2(uv[2], uv[3]);
				m_pVertexData[uParticle * 4 + 3].f4Color = pParticle->Data.f4Color;
				m_pVertexData[uParticle * 4 + 3].f4Normal = f4Normal;
				m_pVertexData[uParticle * 4 + 3].f4Tangent = Math::Combine(f3Right, 0);

				uParticle++;
				pParticle = pParticle->Next;
			}
		}

		if (m_pMesh != nullptr)
		{
			m_pMesh->SetVertices(m_uActiveParticleCount * 4, m_pVertexData);
			m_pMesh->SetSubMesh(0, 0, m_uActiveParticleCount * 6);
		}
	}

	Boolean CParticleSystemComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		String strName, strValue;

		Float1 fValue;
		DeserializerBlock.Load("ParticleRenderType", fValue);
		m_eRenderType = (ParticleRenderType)(UInt32)fValue;

		DeserializerBlock.Load(MaxSurviveParticleCount.VarName, fValue);
		MaxSurviveParticleCount = fValue;
		DeserializerBlock.Load(LifeTime.VarName, LifeTime);
		DeserializerBlock.Load(EmitRate.VarName, fValue);
		EmitRate = fValue;

		DeserializerBlock.Load(BaseScale.VarName, BaseScale);
		DeserializerBlock.Load(BaseColor.VarName, BaseColor);

		DeserializerBlock.Load(UVScale.VarName, UVScale);

		DeserializerBlock.Load(UVAnimationCount.VarName, fValue);
		UVAnimationCount = fValue;

		DeserializerBlock.Load("MaterialInstance", strValue);
		MX_RELEASE_INTERFACE(m_pMaterialInstance);
		m_pMaterialInstance = (IAppMaterialInstance*)AppAssetManager::GetAsset(strValue.CString());

		DeserializerBlock.Load("EmitterType", fValue);
		m_eEmitterType = (EmitterType)(UInt32)fValue;
		if (m_pEmitter != nullptr)
			delete m_pEmitter;
		switch (m_eEmitterType)
		{
		case MXVisual::EET_Sphere:
			m_pEmitter = new SphereEmitter;
			break;
		case MXVisual::EET_Cylinder:
			m_pEmitter = new CylinderEmitter;
			break;
		}
		return m_pEmitter->OnDeserialize(DeserializerBlock);
	}

#if MX_BUILD_EDITOR
	Boolean CParticleSystemComponent::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("ParticleRenderType", (UInt32)m_eRenderType);
		Serializer.Save(MaxSurviveParticleCount.VarName, MaxSurviveParticleCount);
		Serializer.Save(LifeTime.VarName, LifeTime);
		Serializer.Save(EmitRate.VarName, EmitRate);
		Serializer.Save(BaseScale.VarName, BaseScale);
		Serializer.Save(BaseColor.VarName, BaseColor);
		Serializer.Save(UVScale.VarName, UVScale);
		Serializer.Save(UVAnimationCount.VarName, UVAnimationCount);

		Serializer.Save("MaterialInstance", m_pMaterialInstance != nullptr ? m_pMaterialInstance->GetName() : "");

		Serializer.Save("EmitterType", (UInt32)m_eEmitterType);

		return m_pEmitter->OnSerialize(Serializer);
	}
	Void CParticleSystemComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Array<SInt32> arrEnum;
		Array<String> arrEnumString;
		arrEnum.Add(ParticleRenderType::EPRT_Billboard); arrEnumString.Add("Billboard");
		arrEnum.Add(ParticleRenderType::EPRT_VelocityBillboard); arrEnumString.Add("VelocityBillboard");
		arrEnum.Add(ParticleRenderType::EPRT_UprightBillboard); arrEnumString.Add("UprightBillboard");
		arrEnum.Add(ParticleRenderType::EPRT_LieBillboard); arrEnumString.Add("LieBillboard");
		arrEnum.Add(ParticleRenderType::EPRT_Free); arrEnumString.Add("Free");
		Panel.AddPropertyEnumeration("ParticleRenderType", arrEnumString, arrEnum, m_eRenderType);
		arrEnum.Clear(); arrEnumString.Clear();

		Panel.AddPropertyFloat1(MaxSurviveParticleCount.VarName, MaxSurviveParticleCount, 1, 0, 1e6f);//100w Particle
		Panel.AddPropertyFloat1(LifeTime.VarName, LifeTime, 0.01f, 0, 60.0f);//60second 
		Panel.AddPropertyFloat1(EmitRate.VarName, EmitRate, 1, 0, 1e6f);//100w Particle Per Second
		Panel.AddPropertyFloat2(BaseScale.VarName, BaseScale, 0.01f, 0.0f, 100.0f);//1m
		Panel.AddPropertyColor(BaseColor.VarName, BaseColor);
		Panel.AddPropertyFloat2(UVScale.VarName, UVScale, 0.01f, 0.0f, 1.0f);
		Panel.AddPropertyFloat1(UVAnimationCount.VarName, UVAnimationCount, 1, 0, 512);
		Panel.AddPropertyFile("MaterialInstance", m_pMaterialInstance != nullptr ? m_pMaterialInstance->GetName() : "", IAppMaterialInstance::Suffix);

		arrEnum.Add(EmitterType::EET_Sphere); arrEnumString.Add("Sphere");
		arrEnum.Add(EmitterType::EET_Cylinder); arrEnumString.Add("Cylinder");
		Panel.AddPropertyEnumeration("EmitterType", arrEnumString, arrEnum, m_eEmitterType);

		Panel.BeginGroup("Emitter");
		m_pEmitter->OnPanelActived(Panel);
		Panel.EndGroup();
	}
	Void CParticleSystemComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFile:
		{
			if (strcmp(pProperty->Name, "MaterialInstance") == 0)
			{
				MX_RELEASE_INTERFACE(m_pMaterialInstance);
				m_pMaterialInstance = (IAppMaterialInstance*)AppAssetManager::GetAsset(((EditorPropertyFile*)pProperty)->Value);
			}
			else break;
			return;
		}
		case IEditorProperty::EFloat1:
		{
			if (strcmp(pProperty->Name, MaxSurviveParticleCount.VarName) == 0)
			{
				MaxSurviveParticleCount = ((EditorPropertyFloat1*)pProperty)->Value;
			}
			else if (strcmp(pProperty->Name, LifeTime.VarName) == 0)
			{
				LifeTime = ((EditorPropertyFloat1*)pProperty)->Value;
			}
			else if (strcmp(pProperty->Name, EmitRate.VarName) == 0)
			{
				EmitRate = ((EditorPropertyFloat1*)pProperty)->Value;
			}
			else if (strcmp(pProperty->Name, UVAnimationCount.VarName) == 0)
			{
				UVAnimationCount = ((EditorPropertyFloat1*)pProperty)->Value;
			}
			else break;
			return;
		}
		case IEditorProperty::EEnumeration:
		{
			if (strcmp(pProperty->Name, "EmitterType") == 0)
			{
				m_eEmitterType = (EmitterType)((EditorPropertyEnumeration*)pProperty)->Value;
				if (m_pEmitter != nullptr)
				{
					delete m_pEmitter;
				}
				switch (m_eEmitterType)
				{
				case MXVisual::EET_Sphere:m_pEmitter = new SphereEmitter; break;
				case MXVisual::EET_Cylinder:m_pEmitter = new CylinderEmitter; break;
				}
			}
			else if (strcmp(pProperty->Name, "ParticleRenderType") == 0)
			{
				m_eRenderType = (ParticleRenderType)((EditorPropertyEnumeration*)pProperty)->Value;
			}
			else break;
			return;
		}
		case IEditorProperty::EFloat2:
			if (strcmp(pProperty->Name, BaseScale.VarName) == 0)
			{
				BaseScale = ((EditorPropertyFloat2*)pProperty)->Value;
			}
			else if (strcmp(pProperty->Name, UVScale.VarName) == 0)
			{
				UVScale = ((EditorPropertyFloat2*)pProperty)->Value;
			}
			else break;
			return;
		case IEditorProperty::EColor:
			if (strcmp(pProperty->Name, BaseColor.VarName) == 0)
			{
				BaseColor = ((EditorPropertyColor*)pProperty)->Value;
			}
			else break;
			return;
		}

		m_pEmitter->OnPanelChanged(pProperty);
	}
#endif
}
