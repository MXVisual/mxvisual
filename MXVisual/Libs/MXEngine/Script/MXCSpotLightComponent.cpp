#include "MXCSpotLightComponent.h"
#include "MXMath.h"
#include "MXGraphicsResource.h"
#include "MXGraphicsRenderer.h"

namespace MXVisual
{
	struct CSpotLight : public IGSpotLight
	{
		Float3 f3Color;
		Float1 fCandela;
		Float3 f3Position;
		Float1 fSpotAngle;
		Float3 f3Direction;
		Float1 fRange;
		Float1 fAttenuation;
		Boolean bCastShadow;
		Boolean bAtmosphericScatter;

	public:
		Boolean IsCastShadow() const override
		{
			return bCastShadow;
		}
		Boolean IsAtmosphericScatter() const override
		{
			return bAtmosphericScatter;
		}
		Float3	GetColor() const override
		{
			return f3Color;
		}
		Float1	GetCandela() const override
		{
			return fCandela;
		}
		Float3	GetLightPosition() const override
		{
			return f3Position;
		}
		Float3	GetLightDirection() const override
		{
			return f3Direction;
		}
		Float1	GetLightRange() const override
		{
			return fRange;
		}
		Float1	GetLightSpotAngle() const override
		{
			return Math::Degree2Radian(fSpotAngle);
		}
		Float1	GetLightAttenuation() const override
		{
			return fAttenuation;
		}

	};

	CSpotLightComponent::CSpotLightComponent()
		: m_pLight(nullptr)
	{
		CSpotLight* pLight = new CSpotLight;
		pLight->bCastShadow = False;
		pLight->f3Color = Float3(1, 1, 1);
		pLight->fCandela = 3.0f * Constant_PI * 0.25f;
		pLight->f3Position = Float3();
		pLight->fRange = 8;
		pLight->fAttenuation = 2;
		pLight->fSpotAngle = 45.0f;
		pLight->bAtmosphericScatter = False;
		m_pLight = pLight;
	}

	CSpotLightComponent::~CSpotLightComponent()
	{
		delete m_pLight;
	}

	Void CSpotLightComponent::OnInit()
	{

	}
	Void CSpotLightComponent::OnRender()
	{
		CSpotLight* pLight = (CSpotLight*)m_pLight;
		pLight->f3Position = GetObjectPosition(False);
		pLight->f3Direction = GetObjectDirForward() * -1.0f;
		GraphicsRenderer::RegisterLight(m_pLight);
	}

	Boolean CSpotLightComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		CSpotLight* pLight = (CSpotLight*)m_pLight;

		DeserializerBlock.Load("Candela", pLight->fCandela);
		DeserializerBlock.Load("Color", pLight->f3Color);
		DeserializerBlock.Load("Range", pLight->fRange);
		DeserializerBlock.Load("Attenuation", pLight->fAttenuation);
		DeserializerBlock.Load("SpotAngle", pLight->fSpotAngle);
		DeserializerBlock.Load("CastShadow", pLight->bCastShadow);
		DeserializerBlock.Load("AtmosphericScatter", pLight->bCastShadow);

		return True;
	}

#if MX_BUILD_EDITOR
	Void CSpotLightComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		CSpotLight* pLight = (CSpotLight*)m_pLight;

		Panel.AddPropertyFloat1("Candela", pLight->fCandela, 0.01f, 0.0f, 512.0f);
		Panel.AddPropertyColor("Color",
			Float4(pLight->f3Color[0], pLight->f3Color[1], pLight->f3Color[2], 1));
		Panel.AddPropertyFloat1("Range", pLight->fRange, 0.01f, 0.0f, 1000.0f);
		Panel.AddPropertyFloat1("Attenuation", pLight->fAttenuation, 0.01f, 0.0f, 1000.0f);
		Panel.AddPropertyFloat1("SpotAngle", pLight->fSpotAngle, 0.01f, 1.0f, 170.0f);
		Panel.AddPropertyTrigger("CastShadow", pLight->bCastShadow);
		Panel.AddPropertyTrigger("AtmosphericScatter", pLight->bAtmosphericScatter);
	}

	Void CSpotLightComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		CSpotLight* pLight = (CSpotLight*)m_pLight;

		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFloat1:
		{
			Float1 value = ((EditorPropertyFloat1*)pProperty)->Value;
			if (strcmp(pProperty->Name, "Candela") == 0)
			{
				pLight->fCandela = value;
			}
			else if (strcmp(pProperty->Name, "Range") == 0)
			{
				pLight->fRange = value;
			}
			else if (strcmp(pProperty->Name, "SpotAngle") == 0)
			{
				pLight->fSpotAngle = value;
			}
			else if (strcmp(pProperty->Name, "Attenuation") == 0)
			{
				pLight->fAttenuation = value;
			}
			break;
		}
		case IEditorProperty::EColor:
			pLight->f3Color = MXVisual::Math::Split(((EditorPropertyColor*)pProperty)->Value);
			break;
		case IEditorProperty::ETrigger:
		{
			Boolean value = ((EditorPropertyTrigger*)pProperty)->Value;
			if (strcmp(pProperty->Name, "CastShadow") == 0)
			{
				pLight->bCastShadow = value;
			}
			else if (strcmp(pProperty->Name, "AtmosphericScatter") == 0)
			{
				pLight->bAtmosphericScatter = value;
			}
			break;
		}
		default: break;
		}
	}

	Boolean CSpotLightComponent::OnSerialize(ISerializer& Serializer)
	{
		CSpotLight* pLight = (CSpotLight*)m_pLight;

		Serializer.Save("Candela", pLight->fCandela);
		Serializer.Save("Color", pLight->f3Color);
		Serializer.Save("Range", pLight->fRange);
		Serializer.Save("Attenuation", pLight->fAttenuation);
		Serializer.Save("SpotAngle", pLight->fSpotAngle);
		Serializer.Save("CastShadow", pLight->bCastShadow);

		return True;
	}

	Void CSpotLightComponent::OnEditorRender(IEditorGraphics& Graphics)
	{
		CSpotLight* pLight = (CSpotLight*)m_pLight;

		Graphics.ChangeColor(Float4(0.8f, 0.8f, 0.0f, 1.0f));
		Graphics.RenderArrow(pLight->f3Position,
			pLight->f3Position - pLight->f3Direction * 5.0f, GetObjectDirUp() ,1.0f);
	}
#endif
}