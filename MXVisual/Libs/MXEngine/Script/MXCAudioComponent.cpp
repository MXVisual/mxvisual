#include "MXCAudioComponent.h"
#include "MXAsset.h"

namespace MXVisual
{
	MXCAudioComponent::MXCAudioComponent()
		: m_pAudio(nullptr)
		, m_bAutoPlay(False)
	{
	}
	MXCAudioComponent::~MXCAudioComponent()
	{
		MX_RELEASE_INTERFACE(m_pAudio);
	}

	Void	MXCAudioComponent::OnInit()
	{
		if (m_bAutoPlay && m_pAudio != nullptr)
		{
			//m_pAudio->Play();
		}
	}

	Void	MXCAudioComponent::OnUpdate()
	{
	}

	Void	MXCAudioComponent::OnRelease()
	{
	}

	Void	MXCAudioComponent::Play()
	{
		//m_pAudio->Play();
	}
	Void	MXCAudioComponent::Stop()
	{
		//m_pAudio->Stop();
	}
	Boolean MXCAudioComponent::IsPlaying()
	{
		return True;
	}

	Boolean	MXCAudioComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		MX_RELEASE_INTERFACE(m_pAudio);

		DeserializerBlock.Load("AutoPlay", m_bAutoPlay);

		String file;
		DeserializerBlock.Load("Audio", file);

		m_pAudio = (IAppAudio*)AppAssetManager::GetAsset(file.CString());

		return True;
	}

#if MX_BUILD_EDITOR
	Void	MXCAudioComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyTrigger("AutoPlay", m_bAutoPlay);
		Panel.AddPropertyFile("Audio", m_pAudio != nullptr ? m_pAudio->GetName() : "", "flac");
	}
	Void	MXCAudioComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		if (strcmp(pProperty->Name, "AutoPlay") == 0)
		{
			m_bAutoPlay = ((EditorPropertyTrigger*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "Audio") == 0)
		{
			String str = ((EditorPropertyFile*)pProperty)->Value;
			MX_RELEASE_INTERFACE(m_pAudio);
			m_pAudio = (IAppAudio*)AppAssetManager::GetAsset(str.CString());
		}
	}

	Boolean	MXCAudioComponent::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("AutoPlay", m_bAutoPlay);
		Serializer.Save("Audio", m_pAudio->GetName());

		return True;
	}
#endif
}