#include "MXCUIImageComponent.h"
#include "MXCScene.h"
#include "MXAsset.h"

namespace MXVisual
{
	IAppMaterial* g_pUIImageMtl = nullptr;
	UInt32	g_IDImagePrimitive = 0;

	CUIImageComponent::CUIImageComponent()
		: m_strTextureName("")
		, m_f4Color(1, 1, 1, 1)
	{

	}

	CUIImageComponent::~CUIImageComponent()
	{
		OnRelease();
	}

	Void	CUIImageComponent::OnInit()
	{
		CComponent2D::OnInit();

		if (g_pUIImageMtl == nullptr)
		{
			g_pUIImageMtl = IAppMaterial::Create("_UIImage_mtl");

			g_pUIImageMtl->SetDomain(IAppMaterial::Domain::EDM_ScreenSpace);
			g_pUIImageMtl->SetBlendMode(IAppMaterial::BlendMode::EBM_Translucency);
			g_pUIImageMtl->SetFaceType(IAppMaterial::FaceType::EFT_Frontface);
			g_pUIImageMtl->SetShadingModel(IAppMaterial::ShadingModel::ESM_Nolighting);

			g_pUIImageMtl->SetParameter(
				IAppMaterial::ParameterDescription(
					"BaseTex", IAppMaterial::EPT_Texture2D, DefaultResource::MOSAIC_TEXTURE
				)
			);

			g_pUIImageMtl->SetMaterialFunc(IAppMaterial::EMF_SurfaceParams,
				"float4 color = Tex2D_Sample(BaseTex, input.TexCoord0);\n\
				OUTPUT_EMISSIVE(PCFunc_Color_GammaCorrect(color.rgb, 2.2f) * PCFunc_Color_GammaCorrect(input.VertexColor.rgb, 2.2f));\n\
				OUTPUT_OPACITY(color.a * input.VertexColor.a);\n\
				OUTPUT_ROUGHNESS(0.5f);\n\
				OUTPUT_METALLIC(0.0f);\n");

			g_pUIImageMtl->Recompile();
		}

		if (m_pMaterialIns == nullptr)
		{
			m_pMaterialIns = IAppMaterialInstance::Create(
				String::Format("UIImage_mtlins_%d", g_IDImagePrimitive).CString(),
				g_pUIImageMtl->GetName());

			m_pMaterialIns->SetTexture2D("BaseTex", m_strTextureName.CString());
		}

		if (m_pMesh == nullptr)
		{
			m_pMesh = IAppMesh::Create(
				String::Format("UIImage_mesh_%d", g_IDImagePrimitive).CString(),
				IAppMesh::EAPPMT_Rigid2D, True, 4, 6, 1);
			m_pMeshInstance = IAppMeshInstance::Create(
				String::Format("UIImage_mesh_%d", g_IDImagePrimitive).CString(),
				m_pMesh->GetName());

			IAppMesh::VertexRigid2D vertex[4];
			vertex[0].f2Position = Float2(0, 0);
			vertex[0].f2TexCoord0 = Float2(0, 0);
			vertex[0].f4Color = m_f4Color;

			vertex[1].f2Position = Float2(1.0f, 0);
			vertex[1].f2TexCoord0 = Float2(1, 0);
			vertex[1].f4Color = m_f4Color;

			vertex[2].f2Position = Float2(0, 1.0f);
			vertex[2].f2TexCoord0 = Float2(0, 1);
			vertex[2].f4Color = m_f4Color;

			vertex[3].f2Position = Float2(1.0f, 1.0f);
			vertex[3].f2TexCoord0 = Float2(1, 1);
			vertex[3].f4Color = m_f4Color;

			m_pMesh->SetVertices(4, vertex);

			UInt32 uIndex[6] = { 0,1,2,1,3,2 };
			m_pMesh->SetIndices(6, (UInt8*)uIndex);
			m_pMesh->SetSubMesh(0, 0, 6);

			g_IDImagePrimitive++;
		}
	}
	Void	CUIImageComponent::OnUpdate()
	{
		CComponent2D::OnUpdate();
	}
	Void	CUIImageComponent::OnRelease()
	{
		CComponent2D::OnRelease();

		if (g_pUIImageMtl != nullptr && g_pUIImageMtl->RefCount() <= 1)
		{
			g_pUIImageMtl->Release();
			g_pUIImageMtl = nullptr;
		}
	}

	Void	CUIImageComponent::SetTexture(const Char* szTextureName)
	{
		if (m_strTextureName != szTextureName)
		{
			m_strTextureName = szTextureName;

			if (m_pMaterialIns != nullptr)
			{
				m_pMaterialIns->SetTexture2D("BaseTex", szTextureName);
			}
		}
	}

	Void	CUIImageComponent::SetColor(const Float4& f4Color)
	{
		if (m_f4Color != f4Color)
		{
			m_f4Color = f4Color;

			if (m_pMesh != nullptr)
			{
				IAppMesh::VertexRigid2D vertex[4];
				vertex[0].f2Position = Float2(0, 0);
				vertex[0].f2TexCoord0 = Float2(0, 0);
				vertex[0].f4Color = m_f4Color;

				vertex[1].f2Position = Float2(1.0f, 0);
				vertex[1].f2TexCoord0 = Float2(1, 0);
				vertex[1].f4Color = m_f4Color;

				vertex[2].f2Position = Float2(0, 1.0f);
				vertex[2].f2TexCoord0 = Float2(0, 1);
				vertex[2].f4Color = m_f4Color;

				vertex[3].f2Position = Float2(1.0f, 1.0f);
				vertex[3].f2TexCoord0 = Float2(1, 1);
				vertex[3].f4Color = m_f4Color;

				m_pMesh->SetVertices(4, vertex);
			}
		}
	}

	Boolean CUIImageComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		String strValue;

		DeserializerBlock.Load("Texture", strValue);
		SetTexture(strValue.CString());

		Float4 fValue;
		DeserializerBlock.Load("Color", fValue);
		SetColor(fValue);
		return True;
	}

#if MX_BUILD_EDITOR
	Boolean CUIImageComponent::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("Texture", m_strTextureName.CString());
		Serializer.Save("Color", m_f4Color);
		return True;
	}
	Void CUIImageComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyFile("Texture", m_strTextureName.CString(), IAppTexture::Suffix);
		Panel.AddPropertyColor("Color", m_f4Color);
	}
	Void CUIImageComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFile:
			if (strcmp(pProperty->Name, "Texture") == 0)
			{
				SetTexture(((EditorPropertyFile*)pProperty)->Value);
			}
			break;
		case IEditorProperty::EColor:
			if (strcmp(pProperty->Name, "Color") == 0)
			{
				SetColor(((EditorPropertyColor*)pProperty)->Value);
			}
			break;
		}
	}
#endif
}