#pragma once
#ifndef _MX_C_REFLECTION_PROBE_COMPONENT_
#define _MX_C_REFLECTION_PROBE_COMPONENT_
#include "MXCComponent.h"
#include "MXCEditorGraphics.h"
#include "MXCEditorPanel.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	class IGReflectionProbe;

	class CReflectionProbeComponent
		: public ICComponent
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
		, public ICSupportedEditorGraphics
#endif
	{
		DECLARE_RTTI_INFO(CReflectionProbeComponent);
	public:
		CReflectionProbeComponent();
		~CReflectionProbeComponent();

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);
		Void	OnEditorRender(IEditorGraphics& Graphics);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif
	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRender() override;
		Void	OnRelease() override;

	protected:
		IGReflectionProbe* m_pProbe;
	};
}
#endif