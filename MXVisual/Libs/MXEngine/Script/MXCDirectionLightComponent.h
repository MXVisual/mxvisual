#pragma once
#ifndef _MX_C_DIRECTIONLIGHT_COMPONENT_
#define _MX_C_DIRECTIONLIGHT_COMPONENT_
#include "MXCComponent.h"
#include "MXCEditorGraphics.h"
#include "MXCEditorPanel.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	class IGDirectionLight;
	class CDirectionLightComponent 
		: public ICComponent
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
		, public ICSupportedEditorGraphics
#endif
	{
		DECLARE_RTTI_INFO(CDirectionLightComponent);
	public:
		CDirectionLightComponent();
		~CDirectionLightComponent();

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Boolean	OnSerialize(ISerializer& Serializer);
	
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Void	OnEditorRender(IEditorGraphics& Graphics);
#endif
	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRender() override;
		Void	OnRelease() override {}

	protected:
		IGDirectionLight* m_pLight;
	};
}
#endif