#include "MXCRigidBodyComponent.h"
#include "MXMath.h"
#include "MXPhysicsSimulator.h"

namespace MXVisual
{
	MXCRigidBodyComponent::MXCRigidBodyComponent()
	{
	}
	MXCRigidBodyComponent::~MXCRigidBodyComponent()
	{
		PhysicsSimulator::Unregister(m_pPhysicsBody);
		MX_RELEASE_INTERFACE(m_pCollision);
		MX_RELEASE_INTERFACE(m_pPhysicsBody);
	}

	Void	MXCRigidBodyComponent::OnInit()
	{
		PhysicsSimulator::Unregister(m_pPhysicsBody);
		if (m_pPhysicsBody == nullptr)
		{
			m_pPhysicsBody = PhysicsResourceManager::CreatePhysicsBody();
		}
		Quaternion q = GetObjectRotation(False);
		Float3 t = GetObjectPosition(False);
		t += m_f3CollisionCenterOffset;
		m_pPhysicsBody->SetTransform(q, t);

		if (m_pCollision == nullptr || m_pCollision->GetShapeType() != m_eCSType)
		{
			if (m_eCSType == ICollisionShape::CollisionShapeType::ECapsule)
			{
				ICollisionShape::Description desc;
				desc.eType = ICollisionShape::ECapsule;
				desc.fCapsuleHeight = 2;
				desc.fCapsuleRadius = 1;
				m_pCollision = PhysicsResourceManager::CreateCollisionShape(desc);
			}
			else
			{
				ICollisionShape::Description desc;
				desc.eType = ICollisionShape::EBox;
				m_pCollision = PhysicsResourceManager::CreateCollisionShape(desc);
			}
			m_pPhysicsBody->SetCollision(m_pCollision);
		}

		PhysicsSimulator::Register(m_pPhysicsBody);
	}
	Void	MXCRigidBodyComponent::OnUpdateLate()
	{
		if (m_pPhysicsBody != nullptr)
		{
			Quaternion q = GetObjectRotation(False);
			Float3 t = GetObjectPosition(False);
			t += m_f3CollisionCenterOffset;
			m_pPhysicsBody->SetTransform(q, t);
		}
	}
	Void	MXCRigidBodyComponent::OnUpdateFixedDeltaTime()
	{
		if (m_pPhysicsBody != nullptr)
		{
			Matrix4 t = m_pPhysicsBody->GetTransform();
			t *= Math::MatrixTranslation(m_f3CollisionCenterOffset * -1.0f);
			Matrix4 s = Math::MatrixScale(GetObjectScale(False));
			SetObjectTransform(s * t, False);
		}
	}

	Void	MXCRigidBodyComponent::OnRelease()
	{

	}

	Void	MXCRigidBodyComponent::SetStatic(Boolean bStatic)
	{
		if (m_pPhysicsBody != nullptr)
		{
			PhysicsSimulator::Unregister(m_pPhysicsBody);
			m_pPhysicsBody->SetStatic(bStatic);
			PhysicsSimulator::Register(m_pPhysicsBody);
		}
	}
	Boolean MXCRigidBodyComponent::IsStatic()
	{
		if (m_pPhysicsBody != nullptr)
		{
			return m_pPhysicsBody->GetStatic();
		}
		return False;
	}

	Void	MXCRigidBodyComponent::SetMass(Float1 fMass)
	{
		if (m_pPhysicsBody != nullptr)
		{
			PhysicsSimulator::Unregister(m_pPhysicsBody);
			m_pPhysicsBody->SetMass(fMass);
			PhysicsSimulator::Register(m_pPhysicsBody);
		}
	}
	Float1	MXCRigidBodyComponent::GetMass()
	{
		if (m_pPhysicsBody != nullptr)
		{
			return m_pPhysicsBody->GetMass();
		}
		return 0.0f;
	}

	Void	MXCRigidBodyComponent::SetFriction(Float1 fFriction)
	{
		if (m_pPhysicsBody != nullptr)
		{
			PhysicsSimulator::Unregister(m_pPhysicsBody);
			m_pPhysicsBody->SetFriction(fFriction);
			PhysicsSimulator::Register(m_pPhysicsBody);
		}
	}
	Float1	MXCRigidBodyComponent::GetFriction()
	{
		if (m_pPhysicsBody != nullptr)
		{
			return m_pPhysicsBody->GetFriction();
		}
		return 1.0f;
	}

	Void	MXCRigidBodyComponent::SetRestitution(Float1 fRestitution)
	{
		if (m_pPhysicsBody != nullptr)
		{
			PhysicsSimulator::Unregister(m_pPhysicsBody);
			m_pPhysicsBody->SetRestitution(fRestitution);
			PhysicsSimulator::Register(m_pPhysicsBody);
		}
	}
	Float1	MXCRigidBodyComponent::GetRestitution()
	{
		if (m_pPhysicsBody != nullptr)
		{
			return m_pPhysicsBody->GetRestitution();
		}
		return 0.0f;
	}

	Void	MXCRigidBodyComponent::AddForce(const Float3& force)
	{
		m_pPhysicsBody->AddForce(force);
	}
	Float3	MXCRigidBodyComponent::GetVelocity() const
	{
		return m_pPhysicsBody->GetVelocity();
	}

	Boolean	MXCRigidBodyComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		if (m_pPhysicsBody == nullptr)
		{
			m_pPhysicsBody = PhysicsResourceManager::CreatePhysicsBody();
		}

		Boolean bRes;
		Float1	fRes;
		DeserializerBlock.Load("Static", bRes);
		m_pPhysicsBody->SetStatic(bRes);
		DeserializerBlock.Load("Mass", fRes);
		m_pPhysicsBody->SetMass(fRes);
		DeserializerBlock.Load("Friction", fRes);
		m_pPhysicsBody->SetFriction(fRes);
		DeserializerBlock.Load("Restitution", fRes);
		m_pPhysicsBody->SetRestitution(fRes);

		Float3 f3Min, f3Max;
		DeserializerBlock.Load("ForceMin", f3Min);
		DeserializerBlock.Load("ForceMax", f3Max);
		m_pPhysicsBody->SetForceLimitations(f3Min, f3Max);

		DeserializerBlock.Load("TorqueMin", f3Min);
		DeserializerBlock.Load("TorqueMax", f3Max);
		m_pPhysicsBody->SetTorqueLimitations(f3Min, f3Max);

		DeserializerBlock.Load("CenterOffset", m_f3CollisionCenterOffset);
		String strRes;
		DeserializerBlock.Load("CollisionShapeType", strRes);
		if (strcmp(strRes.CString(), "Capsule") == 0)
		{
			m_eCSType = ICollisionShape::CollisionShapeType::ECapsule;
		}
		else if (strcmp(strRes.CString(), "Box") == 0)
		{
			m_eCSType = ICollisionShape::CollisionShapeType::EBox;
		}

		MX_RELEASE_INTERFACE(m_pCollision);
		if (m_eCSType == ICollisionShape::CollisionShapeType::ECapsule)
		{
			ICollisionShape::Description desc;
			desc.eType = ICollisionShape::ECapsule;
			desc.fCapsuleHeight = 2;
			desc.fCapsuleRadius = 1;
			DeserializerBlock.Load("Radius", desc.fCapsuleRadius);
			DeserializerBlock.Load("Height", desc.fCapsuleHeight);

			m_pCollision = PhysicsResourceManager::CreateCollisionShape(desc);
		}
		else//ICollisionShape::CollisionShapeType::EBox
		{
			ICollisionShape::Description desc;
			desc.eType = ICollisionShape::EBox;
			m_pCollision = PhysicsResourceManager::CreateCollisionShape(desc);

			Float3 f3Res;
			DeserializerBlock.Load("Scale", f3Res);
			((ICollisionBox*)m_pCollision)->SetScale(f3Res);
		}
		m_pPhysicsBody->SetCollision(m_pCollision);

		return True;
	}

#if MX_BUILD_EDITOR
	Void	MXCRigidBodyComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyTrigger("Static", IsStatic());
		Panel.AddPropertyFloat1("Mass", GetMass(), 0.01f, 0.0f, 9999.0f);
		Panel.AddPropertyFloat1("Friction", GetFriction(), 0.01f, 0.0f, 9999.0f);
		Panel.AddPropertyFloat1("Restitution", GetRestitution(), 0.01f, 0.0f, 9999.0f);

		Float3 f3Min, f3Max;
		m_pPhysicsBody->GetForceLimitations(f3Min, f3Max);

		Panel.AddPropertyFloat3("ForceMin", f3Min, 0.01f,
			-9999.0f, 9999.0f);
		Panel.AddPropertyFloat3("ForceMax", f3Max, 0.01f,
			-9999.0f, 9999.0f);

		m_pPhysicsBody->GetTorqueLimitations(f3Min, f3Max);

		Panel.AddPropertyFloat3("TorqueMin", f3Min, 0.01f,
			-9999.0f, 9999.0f);
		Panel.AddPropertyFloat3("TorqueMax", f3Max, 0.01f,
			-9999.0f, 9999.0f);

		Panel.BeginGroup("CollisionShape");
		Panel.AddPropertyFloat3("CenterOffset", m_f3CollisionCenterOffset, 0.01f,
			-9999.0f, 9999.0f);

		Array<SInt32> arrEnum;
		Array<String> arrEnumString;
		arrEnum.Add(ICollisionShape::EBox); arrEnumString.Add("Box");
		arrEnum.Add(ICollisionShape::ECapsule); arrEnumString.Add("Capsule");
		Panel.AddPropertyEnumeration("CollisionShapeType", arrEnumString, arrEnum, m_eCSType);

		if (m_eCSType == ICollisionShape::CollisionShapeType::ECapsule)
		{
			Panel.AddPropertyFloat1("Radius",
				((ICollisionCapsule*)m_pCollision)->GetRadius(),
				0.01f,
				0,
				9999);

			Panel.AddPropertyFloat1("Height",
				((ICollisionCapsule*)m_pCollision)->GetHeight(),
				0.01f,
				0,
				9999);
		}
		else//ICollisionShape::CollisionShapeType::EBox
		{
			Panel.AddPropertyFloat3("Scale",
				((ICollisionBox*)m_pCollision)->GetScale(),
				0.01f,
				0.0f, 9999.0f);
		}
		Panel.EndGroup();

		m_pPhysicsBody->SetDebugShow(True);
	}
	Void	MXCRigidBodyComponent::OnPanelInactived()
	{
		m_pPhysicsBody->SetDebugShow(False);
	}
	Void	MXCRigidBodyComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFloat1:
			if (strcmp(pProperty->Name, "Mass") == 0)
			{
				SetMass(((EditorPropertyFloat1*)pProperty)->Value);
			}
			else if (strcmp(pProperty->Name, "Friction") == 0)
			{
				SetFriction(((EditorPropertyFloat1*)pProperty)->Value);
			}
			else if (strcmp(pProperty->Name, "Restitution") == 0)
			{
				SetRestitution(((EditorPropertyFloat1*)pProperty)->Value);
			}
			else if (m_eCSType == ICollisionShape::ECapsule)
			{
				ICollisionShape::Description desc;
				desc.eType = ICollisionShape::ECapsule;
				desc.fCapsuleHeight = ((ICollisionCapsule*)m_pCollision)->GetHeight();
				desc.fCapsuleRadius = ((ICollisionCapsule*)m_pCollision)->GetRadius();
				if (strcmp(pProperty->Name, "Height") == 0)
				{
					desc.fCapsuleHeight = ((const EditorPropertyFloat1*)pProperty)->Value;
					MX_RELEASE_INTERFACE(m_pCollision);
					m_pCollision = PhysicsResourceManager::CreateCollisionShape(desc);
					m_pPhysicsBody->SetCollision(m_pCollision);
				}
				else if (strcmp(pProperty->Name, "Radius") == 0)
				{
					desc.fCapsuleRadius = ((const EditorPropertyFloat1*)pProperty)->Value;
					MX_RELEASE_INTERFACE(m_pCollision);
					m_pCollision = PhysicsResourceManager::CreateCollisionShape(desc);
					m_pPhysicsBody->SetCollision(m_pCollision);
				}
			}
			break;
		case IEditorProperty::ETrigger:
			SetStatic(((EditorPropertyTrigger*)pProperty)->Value);
			break;
		case IEditorProperty::EFloat3:
			if (strcmp(pProperty->Name, "ForceMin") == 0)
			{
				Float3 f3Min, f3Max;
				m_pPhysicsBody->GetForceLimitations(f3Min, f3Max);
				f3Min = ((EditorPropertyFloat3*)pProperty)->Value;
				m_pPhysicsBody->SetForceLimitations(f3Min, f3Max);
			}
			else if (strcmp(pProperty->Name, "ForceMax") == 0)
			{
				Float3 f3Min, f3Max;
				m_pPhysicsBody->GetForceLimitations(f3Min, f3Max);
				f3Max = ((EditorPropertyFloat3*)pProperty)->Value;
				m_pPhysicsBody->SetForceLimitations(f3Min, f3Max);
			}
			else if (strcmp(pProperty->Name, "TorqueMin") == 0)
			{
				Float3 f3Min, f3Max;
				m_pPhysicsBody->GetTorqueLimitations(f3Min, f3Max);
				f3Min = ((EditorPropertyFloat3*)pProperty)->Value;
				m_pPhysicsBody->SetTorqueLimitations(f3Min, f3Max);
			}
			else if (strcmp(pProperty->Name, "TorqueMax") == 0)
			{
				Float3 f3Min, f3Max;
				m_pPhysicsBody->GetTorqueLimitations(f3Min, f3Max);
				f3Max = ((EditorPropertyFloat3*)pProperty)->Value;
				m_pPhysicsBody->SetTorqueLimitations(f3Min, f3Max);
			}
			else if (strcmp(pProperty->Name, "CenterOffset") == 0)
			{
				m_f3CollisionCenterOffset = ((EditorPropertyFloat3*)pProperty)->Value;
				Quaternion q = GetObjectRotation(False);
				Float3 t = GetObjectPosition(False);
				t += m_f3CollisionCenterOffset;
				m_pPhysicsBody->SetTransform(q, t);
			}
			else if (m_eCSType == ICollisionShape::EBox)
			{
				if (strcmp(pProperty->Name, "Scale") == 0)
				{
					((ICollisionBox*)m_pCollision)->SetScale(((EditorPropertyFloat3*)pProperty)->Value);
				}
			}
			break;
		case IEditorProperty::EEnumeration:
		{
			if (strcmp(pProperty->Name, "CollisionShapeType") == 0)
			{
				m_eCSType = (ICollisionShape::CollisionShapeType)((EditorPropertyEnumeration*)pProperty)->Value;
				MX_RELEASE_INTERFACE(m_pCollision);
				switch (m_eCSType)
				{
				case ICollisionShape::ECapsule:
				{
					ICollisionShape::Description desc;
					desc.eType = ICollisionShape::ECapsule;
					desc.fCapsuleHeight = 2;
					desc.fCapsuleRadius = 1;
					m_pCollision = PhysicsResourceManager::CreateCollisionShape(desc);
					break;
				}
				case ICollisionShape::EBox:
				{
					ICollisionShape::Description desc;
					desc.eType = ICollisionShape::EBox;
					m_pCollision = PhysicsResourceManager::CreateCollisionShape(desc);
					break;
				}
				}
				m_pPhysicsBody->SetCollision(m_pCollision);
			}
			break;
		}
		}
	}

	Boolean	MXCRigidBodyComponent::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("Static", IsStatic());
		Serializer.Save("Mass", GetMass());
		Serializer.Save("Friction", GetFriction());
		Serializer.Save("Restitution", GetRestitution());

		Float3 f3Min, f3Max;
		m_pPhysicsBody->GetForceLimitations(f3Min, f3Max);
		Serializer.Save("ForceMin", f3Min);
		Serializer.Save("ForceMax", f3Max);

		m_pPhysicsBody->GetTorqueLimitations(f3Min, f3Max);
		Serializer.Save("TorqueMin", f3Min);
		Serializer.Save("TorqueMax", f3Max);

		Serializer.Save("CenterOffset", m_f3CollisionCenterOffset);

		if (m_eCSType == ICollisionShape::CollisionShapeType::ECapsule)
		{
			Serializer.Save("CollisionShapeType", "Capsule");

			Serializer.Save("Radius",
				((ICollisionCapsule*)m_pCollision)->GetRadius());

			Serializer.Save("Height",
				((ICollisionCapsule*)m_pCollision)->GetHeight());
		}
		else//ICollisionShape::CollisionShapeType::EBox
		{
			Serializer.Save("CollisionShapeType", "Box");

			Serializer.Save("Scale",
				((ICollisionBox*)m_pCollision)->GetScale());
		}

		return True;
	}
#endif
}