#pragma once
#ifndef _MX_C_ANIMATOR_COMPONENT_
#define _MX_C_ANIMATOR_COMPONENT_
#include "MXCComponent.h"
#include "MXMath.h"

namespace MXVisual
{
	class CObject;

	class CAnimatorComponent : public ICComponent
	{
		DECLARE_RTTI_INFO(CAnimatorComponent);
	public:
		CAnimatorComponent();
		~CAnimatorComponent();

		Void	Stop();

		Void	PlayAnimation(const Char* szAnimName);

		Void	AddAnimation(const Char* szAnimName);

	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRelease() override;

	protected:
		CObject* m_pSkeletonRoot;
		Float1	 m_fAnimTime;
		SInt32	 m_nActiveAnimTrack;
	};
}
#endif