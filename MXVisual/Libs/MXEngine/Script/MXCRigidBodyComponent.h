#pragma once
#ifndef _MX_C_RIGIDBODY_COMPONENT_
#define _MX_C_RIGIDBODY_COMPONENT_
#include "MXCComponent.h"
#include "MXPhysicsResource.h"
#include "CRigidBodyComponent.h"
#include "MXCEditorPanel.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	class MXCRigidBodyComponent 
		: public ICComponent
		, public CRigidBodyComponent
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(CRigidBodyComponent);
	public:
		MXCRigidBodyComponent();
		~MXCRigidBodyComponent();

		Void	SetStatic(Boolean bStatic);
		Boolean IsStatic();

		Float1	GetMass();
		Void	SetMass(Float1 fMass);

		Void	SetFriction(Float1 fFriction);
		Float1	GetFriction();

		Void	SetRestitution(Float1 fRestitution);
		Float1	GetRestitution();

		Void	AddForce(const Float3& force) override;
		Float3	GetVelocity() const override;

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelInactived();
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif

	protected:
		Void	OnInit() override;
		Void	OnUpdateLate() override;
		Void	OnUpdateFixedDeltaTime() override;
		Void	OnRelease() override;

	protected:
		IPhysicsBody* m_pPhysicsBody;
		ICollisionShape* m_pCollision;
		ICollisionShape::CollisionShapeType m_eCSType;
		Float3			m_f3CollisionCenterOffset;
	};

}
#endif