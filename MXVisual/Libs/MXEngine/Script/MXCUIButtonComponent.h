#pragma once
#ifndef _MX_UI_BUTTON_COMPONENT_
#define _MX_UI_BUTTON_COMPONENT_
#include "MXCComponent2D.h"
#include "MXMath.h"
#include "MXCSerializer.h"
#include "MXCEditorPanel.h"

namespace MXVisual
{
	class CUIButtonComponent 
		: public CComponent2D
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(CUIButtonComponent);
	public:
		CUIButtonComponent();
		~CUIButtonComponent();

		Void	SetIdleTextureName(const Char* szTextureName);
		Void	SetHoverTextureName(const Char* szTextureName);
		Void	SetClickTextureName(const Char* szTextureName);

		Void	SetColor(const Float4& f4Color);

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif

	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRelease() override;

		Void	OnFocused() override;
		Void	OnLostFocus() override;

	protected:
		Float4 m_f4Color;
		enum ButtonState
		{
			eIdle = 0,
			eHover = 1,
			eClick = 2,
		}m_eButtonState;
		String m_strIdleTextureName;
		String m_strHoverTextureName;
		String m_strClickTextureName;
	};
}
#endif