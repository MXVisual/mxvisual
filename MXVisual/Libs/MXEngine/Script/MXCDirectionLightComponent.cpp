#include "MXCDirectionLightComponent.h"
#include "MXMath.h"
#include "MXGraphicsResource.h"
#include "MXGraphicsRenderer.h"

namespace MXVisual
{
	struct CDirectionLight : public IGDirectionLight
	{
		Float3 f3Color;
		Float1 fCandela;
		Float3 f3Direction;
		Boolean bCastShadow;
		Boolean bAtmosphericScatter;

	public:
		Boolean IsCastShadow() const override
		{
			return bCastShadow;
		}
		Boolean IsAtmosphericScatter() const override
		{
			return bAtmosphericScatter;
		}
		Float3	GetColor() const override
		{
			return f3Color;
		}
		Float1	GetCandela() const override
		{
			return fCandela;
		}
		Float3	GetLightDirection() const override
		{
			return f3Direction * -1;
		}
	};

	CDirectionLightComponent::CDirectionLightComponent()
		: m_pLight(nullptr)
	{
		CDirectionLight* pLight = new CDirectionLight;
		pLight->bCastShadow = False;
		pLight->f3Color = Float3(1, 1, 1);
		pLight->fCandela = 3.0f * Constant_PI * 4.0f;
		pLight->f3Direction = Math::Normalize(Float3(0, -1, 1));
		pLight->bAtmosphericScatter = False;
		m_pLight = pLight;
	}

	CDirectionLightComponent::~CDirectionLightComponent()
	{
		delete m_pLight;
	}

	Void CDirectionLightComponent::OnInit()
	{

	}
	Void CDirectionLightComponent::OnUpdate()
	{
	}
	Void CDirectionLightComponent::OnRender()
	{
		CDirectionLight* pLight = (CDirectionLight*)m_pLight;
		pLight->f3Direction = GetObjectDirForward();
		GraphicsRenderer::RegisterLight(m_pLight);
	}

	Boolean CDirectionLightComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		CDirectionLight* pLight = (CDirectionLight*)m_pLight;

		String strName;
		DeserializerBlock.Load("Candela", pLight->fCandela);
		DeserializerBlock.Load("Color", pLight->f3Color);
		DeserializerBlock.Load("CastShadow", pLight->bCastShadow);
		DeserializerBlock.Load("AtmosphericScatter", pLight->bCastShadow);

		return True;
	}

#if MX_BUILD_EDITOR
	Void CDirectionLightComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		CDirectionLight* pLight = (CDirectionLight*)m_pLight;

		Panel.AddPropertyFloat1("Candela", pLight->fCandela, 0.01f, 0.0f, 512.0f);
		Panel.AddPropertyColor("Color",
			Float4(pLight->f3Color[0], pLight->f3Color[1], pLight->f3Color[2], 1));
		Panel.AddPropertyTrigger("CastShadow", pLight->bCastShadow);
		Panel.AddPropertyTrigger("AtmosphericScatter", pLight->bAtmosphericScatter);
	}

	Void CDirectionLightComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		CDirectionLight* pLight = (CDirectionLight*)m_pLight;

		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFloat1:
			pLight->fCandela = ((EditorPropertyFloat1*)pProperty)->Value;
			break;
		case IEditorProperty::EColor:
			pLight->f3Color = MXVisual::Math::Split(((EditorPropertyColor*)pProperty)->Value);
			break;
		case IEditorProperty::ETrigger:
		{
			Boolean value = ((EditorPropertyTrigger*)pProperty)->Value;
			if (strcmp(pProperty->Name, "CastShadow") == 0)
			{
				pLight->bCastShadow = value;
			}
			else if (strcmp(pProperty->Name, "AtmosphericScatter") == 0)
			{
				pLight->bAtmosphericScatter = value;
			}
			break;
		}
		default: break;
		}
	}

	Boolean CDirectionLightComponent::OnSerialize(ISerializer& Serializer)
	{
		CDirectionLight* pLight = (CDirectionLight*)m_pLight;

		Serializer.Save("Candela", pLight->fCandela);
		Serializer.Save("Color", pLight->f3Color);
		Serializer.Save("CastShadow", pLight->bCastShadow);
		return True;
	}

	Void CDirectionLightComponent::OnEditorRender(IEditorGraphics& Graphics)
	{
		CDirectionLight* pLight = (CDirectionLight*)m_pLight;
		Float3 p = GetObjectPosition(False);

		Graphics.ChangeColor(Float4(0.8f, 0.8f, 0.0f, 1.0f));
		Graphics.RenderArrow(p,
			p + pLight->f3Direction * 5.0f, GetObjectDirUp(), 1.0f);
	}
#endif


}