#include "MXCReflectionProbeComponent.h"
#include "MXMath.h"
#include "MXCSerializer.h"
#include "MXGraphicsRenderer.h"
#include "MXGraphicsResource.h"
#include "MXAsset.h"
#include "../MXAppTexture.h"

#if MX_BUILD_EDITOR
#include "MXCEditorGraphics.h"
#include "MXCEditorPanel.h"
#endif

namespace MXVisual
{
	struct ReflectionProbe : public IGReflectionProbe
	{
		Boolean bRealTime;
		Float3	f3ProbeCenter;
		Float1	fProbeRange;
		AppTexture* pReflectionTexture;

		Boolean	GetRealTime() override { return bRealTime; }
		Float3	GetProbePosition() override { return f3ProbeCenter; }
		Float1	GetProbeRange() override { return fProbeRange; }
		IGTexture* GetReflectionTexture() override { return pReflectionTexture->GetGraphicsResource(); }
	};

	CReflectionProbeComponent::CReflectionProbeComponent()
	{
		ReflectionProbe* pProbe = new ReflectionProbe;
		pProbe->bRealTime = False;
		pProbe->fProbeRange = 2048.0f;
		pProbe->pReflectionTexture = nullptr;

		m_pProbe = pProbe;
	}
	CReflectionProbeComponent::~CReflectionProbeComponent()
	{
		ReflectionProbe* pProbe = (ReflectionProbe*)m_pProbe;
		MX_RELEASE_INTERFACE(pProbe->pReflectionTexture);
	}
	Void	CReflectionProbeComponent::OnInit()
	{
	}
	Void	CReflectionProbeComponent::OnUpdate()
	{
		ReflectionProbe* pProbe = (ReflectionProbe*)m_pProbe;
		pProbe->f3ProbeCenter = GetObjectPosition(False);
	}
	Void	CReflectionProbeComponent::OnRender()
	{
		GraphicsRenderer::RegisterEnvironmentProbe(m_pProbe);
	}
	Void	CReflectionProbeComponent::OnRelease()
	{
	}

	Boolean	CReflectionProbeComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		ReflectionProbe* pProbe = (ReflectionProbe*)m_pProbe;
		String strValue;

		DeserializerBlock.Load("RealTime", pProbe->bRealTime);
		DeserializerBlock.Load("ProbeRange", pProbe->fProbeRange);
		DeserializerBlock.Load("TextureFile", strValue);
		if (strValue.Length() > 0)
		{
			pProbe->pReflectionTexture = (AppTexture*)AppAssetManager::GetAsset(strValue.CString());
		}

		return True;
	}

#if MX_BUILD_EDITOR
	Void	CReflectionProbeComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		ReflectionProbe* pProbe = (ReflectionProbe*)m_pProbe;

		Panel.AddPropertyTrigger("RealTime", pProbe->bRealTime);
		Panel.AddPropertyFloat1("ProbeRange", pProbe->fProbeRange, 0.01f, 0.0f, 9999.9f);
		if (!pProbe->bRealTime)
		{
			Panel.AddPropertyFile("TextureFile",
				pProbe->pReflectionTexture == nullptr ? "" : pProbe->pReflectionTexture->GetName(), IAppTexture::Suffix);
		}
	}
	Void	CReflectionProbeComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		ReflectionProbe* pProbe = (ReflectionProbe*)m_pProbe;

		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFloat1:
			if (strcmp(pProperty->Name, "ProbeRange") == 0)
			{
				pProbe->fProbeRange = ((EditorPropertyFloat1*)pProperty)->Value;
			}
			break;
		case IEditorProperty::ETrigger:
		{
			if (strcmp(pProperty->Name, "RealTime") == 0)
			{
				pProbe->bRealTime = ((EditorPropertyTrigger*)pProperty)->Value;
			}
			break;
		}
		case IEditorProperty::EFile:
		{
			if (strcmp(pProperty->Name, "TextureFile") == 0)
			{
				MX_RELEASE_INTERFACE(pProbe->pReflectionTexture);
				pProbe->pReflectionTexture = 
					(AppTexture*)AppAssetManager::GetAsset(((EditorPropertyFile*)pProperty)->Value);
			}
			break;
		}
		}
	}
	Void	CReflectionProbeComponent::OnEditorRender(IEditorGraphics& Graphics)
	{
		ReflectionProbe* pProbe = (ReflectionProbe*)m_pProbe;

		Graphics.ChangeColor(Float4(0.5f, 0.5f, 0.5f, 0.2f));
		Graphics.RenderSphere(pProbe->f3ProbeCenter,
			Float3(pProbe->fProbeRange, pProbe->fProbeRange, pProbe->fProbeRange),
			Math::AXIS_X, Math::AXIS_Y);
	}

	Boolean	CReflectionProbeComponent::OnSerialize(ISerializer& Serializer)
	{
		ReflectionProbe* pProbe = (ReflectionProbe*)m_pProbe;
		Serializer.Save("RealTime", pProbe->bRealTime);
		Serializer.Save("ProbeRange", pProbe->fProbeRange);
		Serializer.Save("TextureFile",
			pProbe->pReflectionTexture == nullptr ? "" : pProbe->pReflectionTexture->GetName());
	
		return True;
	}
#endif

}