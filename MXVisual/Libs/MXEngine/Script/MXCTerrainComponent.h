#pragma once
#ifndef _MX_C_TERRAIN_COMPONENT_
#define _MX_C_TERRAIN_COMPONENT_
#include "MXCComponent.h"
#include "MXMath.h"
#include "MXCEditorPanel.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	class IPhysicsBody;
	class ICollisionTerrain;

	class CTerrainComponent 
		: public ICComponent
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(CTerrainComponent);
	public:
		CTerrainComponent();
		~CTerrainComponent();

		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRender() override;
		Void	OnRelease() override;

		Void SetTerrainGridCount(UInt32 uCount);
		Void SetTerrainScale(const Float3& f3Scale);
		Void SetTerrainMap(const Char* szName);

		UInt32			GetTerrainGridCount() const;
		const Float3&	GetTerrainScale() const;
		const Char*		GetTerrainMap() const;

		Float1	GetWorldHeight(const Float2& pos);

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelInactived();
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif
	protected:
		Boolean ConvertMesh();

		class IAppTexture*	m_pHeightMap;
		Float3				m_f3Scale;
		UInt32				m_uGridCount;

		class IAppMesh*			m_pMesh;
		class IAppMeshInstance*	m_pMeshInstance;
		class IAppMaterialInstance*	m_pMaterialInstance;

		IPhysicsBody*		m_pPhysicsBody;
		ICollisionTerrain*	m_pCollision;
	};
}
#endif