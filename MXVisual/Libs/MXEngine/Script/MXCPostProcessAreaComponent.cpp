#include "MXCPostProcessAreaComponent.h"
#include "MXMath.h"
#include "MXCSerializer.h"
#include "MXGraphicsRenderer.h"

namespace MXVisual
{
	CPostProcessAreaComponent::CPostProcessAreaComponent()
	{
		m_f3AmbientColor = GVarAmbientColor.f3Value;
		m_f3BoxRange = Float3(16, 8, 16);
		m_fAmbientIntensity = GVarAmbientScale.fValue;
	}
	CPostProcessAreaComponent::~CPostProcessAreaComponent()
	{
	}
	Void	CPostProcessAreaComponent::OnInit()
	{
	}
	Void	CPostProcessAreaComponent::OnUpdate()
	{
	}
	Void	CPostProcessAreaComponent::OnRelease()
	{
	}

#if MX_BUILD_EDITOR
	Void	CPostProcessAreaComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyFloat3("BoxRange", m_f3BoxRange, 0.01f, 0.0f, 9999.0f);
		Panel.AddPropertyColor("AmbientColor", Math::Combine(m_f3AmbientColor, 1.0f));
		Panel.AddPropertyFloat1("AmbientIntensity", m_fAmbientIntensity);
	}
	Void	CPostProcessAreaComponent::OnPanelInactived()
	{

	}
	Void	CPostProcessAreaComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFloat1:
			if (strcmp(pProperty->Name, "AmbientIntensity") == 0)
			{
				m_fAmbientIntensity = ((EditorPropertyFloat1*)pProperty)->Value;
			}
			break;
		case IEditorProperty::EFloat3:
			if (strcmp(pProperty->Name, "BoxRange") == 0)
			{
				m_f3BoxRange = ((EditorPropertyFloat3*)pProperty)->Value;
			}
			break;
		case IEditorProperty::EColor:
			if (strcmp(pProperty->Name, "AmbientColor") == 0)
			{
				m_f3AmbientColor = Math::Split(((EditorPropertyColor*)pProperty)->Value);
			}
			break;
		default:
			break;
		}
	}
	Void	CPostProcessAreaComponent::OnEditorRender(IEditorGraphics& Graphics)
	{
		Graphics.ChangeColor(Float4(0.5f, 0.5f, 0.5f, 0.8f));
		Graphics.RenderCube(GetObjectPosition(False),
			m_f3BoxRange, Math::AXIS_X, Math::AXIS_Y);
	}
#endif

}