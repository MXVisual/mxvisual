#pragma once
#ifndef _MX_UI_IMAGE_COMPONENT_
#define _MX_UI_IMAGE_COMPONENT_
#include "MXCComponent2D.h"
#include "MXMath.h"
#include "MXCSerializer.h"
#include "MXCEditorPanel.h"

namespace MXVisual
{
	class CUIImageComponent 
		: public CComponent2D
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(CUIImageComponent);
	public:
		CUIImageComponent();
		~CUIImageComponent();

		Void	SetTexture(const Char* szTextureName);
		Void	SetColor(const Float4& f4Color);

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif

	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRelease() override;

	protected:
		Float4 m_f4Color;
		String m_strTextureName;
	};
}
#endif