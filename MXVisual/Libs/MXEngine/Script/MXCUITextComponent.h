#pragma once
#ifndef _MX_UI_TEXT_COMPONENT_
#define _MX_UI_TEXT_COMPONENT_
#include "MXCComponent2D.h"
#include "MXMath.h"
#include "MXAsset.h"
#include "MXCSerializer.h"
#include "MXCEditorPanel.h"

namespace MXVisual
{
	class ICFont;
	class CUITextComponent
		: public CComponent2D
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(CUITextComponent);
	public:
		CUITextComponent();
		~CUITextComponent();

		Void	SetText(const Char* szText);
		Void	SetFont(const Char* szFontName);
		const Char* GetFontName() const;
		Void	SetColor(const Float4& f4Color);
		Void	SetCharacterSize(Float1 fSize) { m_fCharacterSize = fSize; }

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);

		Boolean	OnSerialize(ISerializer& Serializer);
#endif

	protected:
		Void	OnInit() override;
		Void	OnUpdate() override;
		Void	OnRelease() override;

		Void	UpdatePrimitive();

	protected:
		IAppFont* m_pFont;
		String m_strText;
		Float1 m_fCharacterSize;

		UInt32 m_uCharacterResolution;
		Boolean m_bBold;
		Boolean m_bItalic;

		Float4 m_f4Color;
	};
}
#endif