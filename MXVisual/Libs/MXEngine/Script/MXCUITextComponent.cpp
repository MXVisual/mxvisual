#include "MXCUITextComponent.h"
#include "MXInput.h"
#include "MXGraphicsResource.h"
#include "MXCScene.h"

namespace MXVisual
{
	IAppMaterial* g_pUITextMtl = nullptr;
	UInt32	g_IDTextPrimitive = 0;

	CUITextComponent::CUITextComponent()
		: m_pFont(nullptr)
		, m_strText("")
		, m_fCharacterSize(10.0f)
		, m_f4Color(1, 1, 1, 1)
		, m_uCharacterResolution(16)
		, m_bBold(False)
		, m_bItalic(False)
	{

	}

	CUITextComponent::~CUITextComponent()
	{
		OnRelease();
	}

	Void	CUITextComponent::OnInit()
	{
		CComponent2D::OnInit();

		if (g_pUITextMtl == nullptr)
		{
			g_pUITextMtl = IAppMaterial::Create("_UIText_mtl");

			g_pUITextMtl->SetDomain(IAppMaterial::Domain::EDM_ScreenSpace);
			g_pUITextMtl->SetBlendMode(IAppMaterial::BlendMode::EBM_Translucency);
			g_pUITextMtl->SetFaceType(IAppMaterial::FaceType::EFT_Frontface);
			g_pUITextMtl->SetShadingModel(IAppMaterial::ShadingModel::ESM_Nolighting);

			g_pUITextMtl->SetParameter(
				IAppMaterial::ParameterDescription(
					"FontTex", IAppMaterial::EPT_Texture2D, DefaultResource::BLACK_TEXTURE
				)
			);

			g_pUITextMtl->SetMaterialFunc(IAppMaterial::EMF_SurfaceParams,
				"float4 color = Tex2D_Sample(FontTex, input.TexCoord0);\n\
				OUTPUT_EMISSIVE(PCFunc_Color_GammaCorrect(color.rgb, 2.2f) * PCFunc_Color_GammaCorrect(input.VertexColor.rgb, 2.2f));\n\
				OUTPUT_OPACITY(color.a * input.VertexColor.a);");

			g_pUITextMtl->Recompile();
		}

		if (m_pMaterialIns == nullptr && g_pUITextMtl != nullptr)
		{
			m_pMaterialIns = IAppMaterialInstance::Create(
				String::Format("UIText_mtlins_%d", g_IDTextPrimitive).CString(), g_pUITextMtl->GetName());

			if (m_pFont != nullptr)
				m_pMaterialIns->SetTexture2D("FontTex", m_pFont->GetName());
		}

		if (m_pMesh == nullptr)
		{
			m_pMesh = IAppMesh::Create(
				String::Format("UIText_mesh_%d", g_IDTextPrimitive).CString(),
				IAppMesh::EAPPMT_Rigid2D, True, Constant_MAX_PATH * 4, Constant_MAX_PATH * 6, 1);
			m_pMeshInstance = IAppMeshInstance::Create(
				String::Format("UIImage_mesh_%d", g_IDTextPrimitive).CString(),
				m_pMesh->GetName());

			UInt32 uIndex[Constant_MAX_PATH * 6];
			for (UInt32 u = 0; u < Constant_MAX_PATH; u++)
			{
				uIndex[u * 6] = u * 4;
				uIndex[u * 6 + 1] = u * 4 + 1;
				uIndex[u * 6 + 2] = u * 4 + 2;

				uIndex[u * 6 + 3] = u * 4 + 1;
				uIndex[u * 6 + 4] = u * 4 + 3;
				uIndex[u * 6 + 5] = u * 4 + 2;
			}
			m_pMesh->SetIndices(Constant_MAX_PATH * 6, (UInt8*)uIndex);

			g_IDTextPrimitive++;
		}

		UpdatePrimitive();
	}
	Void	CUITextComponent::OnUpdate()
	{
		CComponent2D::OnUpdate();

		UpdatePrimitive();
	}
	Void	CUITextComponent::OnRelease()
	{
		CComponent2D::OnRelease();

		if (g_pUITextMtl != nullptr && g_pUITextMtl->RefCount() <= 1)
		{
			g_pUITextMtl->Release();
			g_pUITextMtl = nullptr;
		}

		MX_RELEASE_INTERFACE(m_pFont);
	}

	Void	CUITextComponent::UpdatePrimitive()
	{
		if (m_pFont == nullptr)return;

		Float3 Scale = GetObjectScale(False);
		Float2 InvScale(1.0f / Scale[0], 1.0f / Scale[1]);

		IAppMesh::VertexRigid2D vertex[Constant_MAX_PATH * 4];

		const Char* szText = m_strText.CString();
		UInt32 uLength = Math::Min(Constant_MAX_PATH, m_strText.Length());
		Float2 f2Cursor(0, 0);

		UInt32 uCharacterCount = 0;
		for (UInt32 u = 0; u < uLength; u++)
		{
			switch (szText[u])
			{
			case '\n':
			{
				f2Cursor[0] = 0;
				f2Cursor[1] += m_fCharacterSize;
				continue;
				break;
			}
			case '\t':
			{
				f2Cursor[0] += m_fCharacterSize;
				continue;
				break;
			}
			default:
			{
				Float4 uv = m_pFont->GetCharacterRect(szText[u]);
				Float1 fWHAspect = uv[2] / uv[3];

				Float4 textpos = Float4(
					f2Cursor[0],
					f2Cursor[1],
					f2Cursor[0] + m_fCharacterSize * fWHAspect,
					f2Cursor[1] + m_fCharacterSize
				);

				if (textpos[0] < Scale[0] && textpos[1] < Scale[1])
				{
					if (textpos[2] > Scale[0])
					{
						Float1 ClipRate = (Scale[0] - textpos[0]) / (textpos[2] - textpos[0]);
						textpos[2] = Scale[0];
						uv[2] *= ClipRate;
					}

					if (textpos[3] > Scale[1])
					{
						Float1 ClipRate = (Scale[1] - textpos[1]) / (textpos[3] - textpos[1]);
						textpos[3] = Scale[1];
						uv[3] *= ClipRate;
					}

					vertex[uCharacterCount * 4].f2Position = Float2(textpos[0], textpos[1]) * InvScale;
					vertex[uCharacterCount * 4].f2TexCoord0 = Float2(uv[0], uv[1]);
					vertex[uCharacterCount * 4].f4Color = m_f4Color;

					vertex[uCharacterCount * 4 + 1].f2Position = Float2(textpos[2], textpos[1])* InvScale;
					vertex[uCharacterCount * 4 + 1].f2TexCoord0 = Float2(uv[0] + uv[2], uv[1]);
					vertex[uCharacterCount * 4 + 1].f4Color = m_f4Color;

					vertex[uCharacterCount * 4 + 2].f2Position = Float2(textpos[0], textpos[3])* InvScale;
					vertex[uCharacterCount * 4 + 2].f2TexCoord0 = Float2(uv[0], uv[1] + uv[3]);
					vertex[uCharacterCount * 4 + 2].f4Color = m_f4Color;

					vertex[uCharacterCount * 4 + 3].f2Position = Float2(textpos[2], textpos[3])* InvScale;
					vertex[uCharacterCount * 4 + 3].f2TexCoord0 = Float2(uv[0] + uv[2], uv[1] + uv[3]);
					vertex[uCharacterCount * 4 + 3].f4Color = m_f4Color;
				}

				uCharacterCount++;
				f2Cursor[0] = textpos[2];
				break;
			}
			}
		}

		m_pMesh->SetVertices(uCharacterCount * 4, vertex);
		m_pMesh->SetSubMesh(0, 0, uCharacterCount * 6);
	}

	Void	CUITextComponent::SetFont(const Char* szFontName)
	{
		if (m_pFont == nullptr || strcmp(m_pFont->GetName(), szFontName) != 0)
		{
			MX_RELEASE_INTERFACE(m_pFont);
			m_pFont = (IAppFont*)AppAssetManager::GetAsset(szFontName);
			if (m_pFont != nullptr)
			{
				m_pFont->SetCharacterSize(m_uCharacterResolution);
				m_pFont->SetBold(m_bBold);
				m_pFont->SetItalic(m_bItalic);
				if (m_pMaterialIns != nullptr)
					m_pMaterialIns->SetTexture2D("FontTex", m_pFont->GetName());
			}
		}
	}

	Void	CUITextComponent::SetColor(const Float4& f4Color)
	{
		m_f4Color = f4Color;
	}

	Boolean CUITextComponent::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		String strName, strValue;

		DeserializerBlock.Load("Font", strValue);

		DeserializerBlock.Load("Bold", m_bBold);
		DeserializerBlock.Load("Italic", m_bItalic);

		Float1 fResolution;
		DeserializerBlock.Load("CharacterResolution", fResolution);
		m_uCharacterResolution = fResolution;

		Float4 fValue;
		DeserializerBlock.Load("Color", fValue);
		SetColor(fValue);

		DeserializerBlock.Load("CharacterSize", m_fCharacterSize);

		DeserializerBlock.Load("Content", m_strText);

		SetFont(strValue.CString());
		return True;
	}

#if MX_BUILD_EDITOR
	Boolean CUITextComponent::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("Font", m_pFont == nullptr ? "" : m_pFont->GetName());
		Serializer.Save("Bold", m_pFont->GetBold());
		Serializer.Save("Italic", m_pFont->GetItalic());
		Serializer.Save("CharacterResolution", m_pFont->GetCharacterSize());
		Serializer.Save("Color", m_f4Color);
		Serializer.Save("CharacterSize", m_fCharacterSize);
		Serializer.Save("Content", m_strText.CString());
		return True;
	}
	Void CUITextComponent::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyFile("Font", m_pFont == nullptr ? "" : m_pFont->GetName(), IAppFont::Suffix);
		Panel.AddPropertyTrigger("Bold", m_bBold);
		Panel.AddPropertyTrigger("Italic", m_bItalic);
		Panel.AddPropertyFloat1("CharacterResolution", m_uCharacterResolution, 1.0f, 1.0f, 512.0f);
		Panel.AddPropertyColor("Color", m_f4Color);
		Panel.AddPropertyFloat1("CharacterSize", m_fCharacterSize, 0.1f, 0.01f, 1024.0f);
		Panel.AddPropertyLongString("Content", m_strText.CString());
	}
	Void CUITextComponent::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EFile:
			if (strcmp(pProperty->Name, "Font") == 0)
			{
				SetFont(((EditorPropertyFile*)pProperty)->Value);
			}
			break;
		case IEditorProperty::EColor:
			if (strcmp(pProperty->Name, "Color") == 0)
			{
				SetColor(((EditorPropertyColor*)pProperty)->Value);
			}
			break;
		case IEditorProperty::EFloat1:
			if (strcmp(pProperty->Name, "CharacterSize") == 0)
			{
				m_fCharacterSize = ((EditorPropertyFloat1*)pProperty)->Value;
			}
			else if (strcmp(pProperty->Name, "CharacterResolution") == 0)
			{
				m_uCharacterResolution = ((EditorPropertyFloat1*)pProperty)->Value;
				m_pFont->SetCharacterSize(m_uCharacterResolution);
			}
			break;
		case IEditorProperty::ELongString:
			if (strcmp(pProperty->Name, "Content") == 0)
			{
				m_strText = ((EditorPropertyLongString*)pProperty)->Value;
			}
			break;
		case IEditorProperty::ETrigger:
			if (strcmp(pProperty->Name, "Bold") == 0)
			{
				m_bBold = ((EditorPropertyTrigger*)pProperty)->Value;
				m_pFont->SetBold(m_bBold);
			}
			else if (strcmp(pProperty->Name, "Italic") == 0)
			{
				m_bItalic = ((EditorPropertyTrigger*)pProperty)->Value;
				m_pFont->SetItalic(m_bItalic);
			}
			break;
		}
	}
#endif
}