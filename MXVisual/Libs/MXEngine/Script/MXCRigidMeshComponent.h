#pragma once
#ifndef _MX_C_RIGID_MESH_COMPONENT_
#define _MX_C_RIGID_MESH_COMPONENT_
#include "MXCComponent.h"
#include "MXCEditorPanel.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	class CRigidMeshComponent 
		: public ICComponent
		, public ICSerializable
#if MX_BUILD_EDITOR
		, public ICSupportedEditorPropertyPanel
#endif
	{
		DECLARE_RTTI_INFO(CRigidMeshComponent);
	public:
		CRigidMeshComponent();
		~CRigidMeshComponent();

		Boolean				SetMesh(const Char* szMeshName);
		const Char*			GetMeshName() { return m_strMeshAssetName.CString(); }

		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock);

#if MX_BUILD_EDITOR
		Void	OnPanelActived(IEditorPropertyPanel& Panel);
		Void	OnPanelChanged(const IEditorProperty* pProperty);
		
		Boolean	OnSerialize(ISerializer& Serializer);
#endif

	protected:
		Void	OnInit() override;
		Void	OnRender() override;
		Void	OnRelease() override;

	protected:
		class IAppMeshInstance* m_pMeshInstance;
		UInt32	m_uMaterialInsCount;

		String	m_strMeshAssetName;
	};
}
#endif