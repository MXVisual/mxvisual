#pragma once
#ifndef _MX_APP_MATERIAL_INSTANCE_
#define _MX_APP_MATERIAL_INSTANCE_
#include "MXAsset.h"
#include "MXGraphicsResource.h"

#include <unordered_map>

namespace MXVisual
{
	class AppMaterialInstance : public IAppMaterialInstance
	{
	public:
		AppMaterialInstance(const Char* szName);
		~AppMaterialInstance();
		const Char* GetName() const override { return m_strName.CString(); }

		Boolean Init(const Char* szMaterial);

		const Char* GetRefMaterialName() const override;

		const class AppMaterial* GetRefMaterial() const { return m_pRefMaterial; }

		Float1 GetFloat1(const Char* szName) const override;
		Float2 GetFloat2(const Char* szName) const override;
		Float3 GetFloat3(const Char* szName) const override;
		Float4 GetFloat4(const Char* szName) const override;

		const Char* GetTexture1D(const Char* szName) const override;
		const Char* GetTexture2D(const Char* szName) const override;
		const Char* GetTexture3D(const Char* szName) const override;
		const Char* GetTextureCube(const Char* szName) const override;

		Void SetFloat1(const Char* szName, Float1 f1Value) override;
		Void SetFloat2(const Char* szName, const Float2& f2Value) override;
		Void SetFloat3(const Char* szName, const Float3& f3Value) override;
		Void SetFloat4(const Char* szName, const Float4& f4Value) override;

		Void SetTexture1D(const Char* szName, const Char* szTextureName) override;
		Void SetTexture2D(const Char* szName, const Char* szTextureName) override;
		Void SetTexture3D(const Char* szName, const Char* szTextureName) override;
		Void SetTextureCube(const Char* szName, const Char* szTextureName) override;

		IGMaterialParameterBuffer* GetGraphicsResource();
	protected:
		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override;
#if MX_BUILD_EDITOR
		Boolean	OnSerialize(ISerializer& Serializer) override;
#endif
		Boolean UpdateParameterBuffer();

	protected:
		String m_strName;
		class AppMaterial* m_pRefMaterial;
		class IGMaterial*	m_pRealMaterial;//����ȫ 
		IGMaterialParameterBuffer* m_pParameterBlock;
		std::unordered_map<std::string, class AppTexture*> m_mapTexture;
	};
}
#endif