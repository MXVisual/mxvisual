#include "MXAppMeshInstance.h"
#include "MXMath.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	AppMeshInstance::AppMeshInstance(const Char* szName)
		: m_strName(szName)
		, m_pAppMesh(nullptr)
	{
		m_pTransformBlock = GraphicsResource::CreatePrimitiveTransformBuffer(False);
	}
	AppMeshInstance::~AppMeshInstance()
	{
		for (UInt32 u = 0; u < m_arrMaterialIns.ElementCount(); u++)
		{
			MX_RELEASE_INTERFACE(m_arrMaterialIns[u]);
		}
		m_arrMaterialIns.Clear();
		MX_RELEASE_INTERFACE(m_pTransformBlock);
		MX_RELEASE_INTERFACE(m_pAppMesh);
	}

	Void AppMeshInstance::SetTransforms(UInt32 uTransformCount, const Matrix4* pTransforms)
	{
		ASSERT_IF_FAILED(uTransformCount <= m_arrTransforms.ElementCount());
		for (UInt32 u = 0; u < uTransformCount; u++)
		{
			m_arrTransforms[u] = pTransforms[u];
		}

		m_pTransformBlock->SetTransforms(pTransforms, uTransformCount);
	}
	Void AppMeshInstance::SetMaterialInstance(UInt32 uSubMesh, const Char* szMaterialInstanceAssetName)
	{
		ASSERT_IF_FAILED(uSubMesh < m_arrMaterialIns.ElementCount());
		MX_RELEASE_INTERFACE(m_arrMaterialIns[uSubMesh]);
		m_arrMaterialIns[uSubMesh] = (AppMaterialInstance*)AppAssetManager::GetAsset(szMaterialInstanceAssetName);
		if (m_arrMaterialIns[uSubMesh] == nullptr)
		{
			m_arrMaterialIns[uSubMesh] = (AppMaterialInstance*)AppAssetManager::GetAsset(DefaultResource::MATERIALINSTANCE);
		}
	}
	Void AppMeshInstance::SetMesh(const Char* szMeshAssetName)
	{
		MX_RELEASE_INTERFACE(m_pAppMesh);
		m_pAppMesh = (AppMesh*)AppAssetManager::GetAsset(szMeshAssetName);

		m_arrTransforms.SetElementCount(m_pAppMesh->GetBindingBoneCount());
		if (m_pAppMesh->GetSubMeshCount() < m_arrMaterialIns.ElementCount())
		{
			for (UInt32 u = m_pAppMesh->GetSubMeshCount(); u < m_arrMaterialIns.ElementCount(); u++)
			{
				MX_RELEASE_INTERFACE(m_arrMaterialIns[u]);
			}
		}
		else
		{
			m_arrMaterialIns.SetElementCount(m_pAppMesh->GetSubMeshCount());
			for (UInt32 u = 0; u < m_arrMaterialIns.ElementCount(); u++)
			{
				m_arrMaterialIns[u] =
					(AppMaterialInstance*)AppAssetManager::GetAsset(DefaultResource::MATERIALINSTANCE);
			}
		}

		for (UInt32 u = 0; u < m_arrTransforms.ElementCount(); u++)
		{
			m_arrTransforms[u] = Matrix4::Identity;
		}
		m_pTransformBlock->SetTransforms(m_arrTransforms.GetRawData(), m_arrTransforms.ElementCount());
	}

	Boolean	AppMeshInstance::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		if (strcmp(DeserializerBlock.GetBlockName(), "AppMeshInstance") != 0)
		{
			return False;
		}

		String assetPath;
		DeserializerBlock.Load("MeshAsset", assetPath);
		SetMesh(assetPath.CString());

		for (UInt32 u = 0; u < m_arrMaterialIns.ElementCount(); u++)
		{
			DeserializerBlock.Load(String::Format("MaterialInstanceAsset%d", u).CString(),
				assetPath);
			m_arrMaterialIns[u] = (AppMaterialInstance*)AppAssetManager::GetAsset(assetPath.CString());
		}

		MX_RELEASE_INTERFACE(m_pTransformBlock);
		m_pTransformBlock = GraphicsResource::CreatePrimitiveTransformBuffer(False);
		return True;
	}
#if MX_BUILD_EDITOR
	Boolean	AppMeshInstance::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("AppMeshInstance");
		if (m_pAppMesh != nullptr)
			Serializer.Save("MeshAsset", m_pAppMesh->GetName());
		for (UInt32 u = 0; u < m_arrMaterialIns.ElementCount(); u++)
		{
			Serializer.Save(String::Format("MaterialInstanceAsset%d", u).CString(), m_arrMaterialIns[u]->GetName());
		}

		return True;
	}
#endif
}