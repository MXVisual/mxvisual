#pragma once
#ifndef _MX_APP_ANIMATION_CLIP_
#define _MX_APP_ANIMATION_CLIP_
#include "MXAsset.h"
#include "MXArray.h"
#include "MXString.h"

namespace MXVisual
{
	struct CAnimationTrack
	{
		String strNodeName;
		Array<Matrix4> arrTransform;
	};

	class AppAnimationClip : public IAppAnimationClip
	{
	public:
		AppAnimationClip(const Char* szName);
		~AppAnimationClip();

		const Char* GetName() const { return m_strName.CString(); }

		Float1	GetTotalTime() const override;
		Float1  GetSampleRate() const override { return m_fSampleRate; }
		Void	SetSampleRate(Float1 fRate);

		Void	SetSamplingTime(Float1 fTime) override;
		Float1	GetSamplingTime() const override { return m_fCurrentTime; }
		Void	Sampling(const IAppMesh* pMesh, const IAppSkeleton* pSkeleton, Array<Matrix4>& arrSampleResult) const override;
		Void	AddSample(const Char* szNodeName, Float1 fSampleTime, Matrix4 matTransform) override;

	protected:
		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override;
#if MX_BUILD_EDITOR
		Boolean	OnSerialize(ISerializer& Serializer) override;
#endif

	protected:
		String m_strName;

		Float1 m_fTotalTime;
		Float1 m_fCurrentTime;
		Float1 m_fSampleRate;

		Array<CAnimationTrack*> m_arrAnimStack;
	};
}
#endif