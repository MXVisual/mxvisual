#include "MXCComponent2D.h"
#include "MXMath.h"
#include "MXAsset.h"
#include "MXEngine.h"

namespace MXVisual
{
	CComponent2D::CComponent2D()
		: m_pMesh(nullptr)
		, m_pMaterialIns(nullptr)
	{
	}
	CComponent2D::~CComponent2D()
	{
	}

	Void	CComponent2D::OnRender()
	{
		if (m_pMeshInstance != nullptr && m_pMaterialIns != nullptr)
		{
			Matrix4 ToScreen = GetObjectTransform(False);
			m_pMeshInstance->SetTransforms(1, &ToScreen);
			EngineRenderMesh(m_pMeshInstance);
		}
	}

	Void	CComponent2D::OnInit()
	{
	}
	Void	CComponent2D::OnUpdate()
	{
	}
	Void	CComponent2D::OnRelease()
	{
		MX_RELEASE_INTERFACE(m_pMesh);
		MX_RELEASE_INTERFACE(m_pMaterialIns);
	}

}
