#include "MXAppSkeleton.h"
#include "MXCSerializer.h"
#include <functional>

namespace MXVisual
{
	AppSkeleton::Bone::Bone()
		: bUpdateToDate(False)
		, pParent(nullptr)
		, matBoneLocal(Matrix4::Identity)
	{

	}
	Void AppSkeleton::Bone::Update()
	{
		if (pParent != nullptr)
			pParent->Update();

		if (!bUpdateToDate)
		{
			bUpdateToDate = True;
			matBoneToModel =
				(pParent != nullptr ? matBoneLocal * pParent->matBoneToModel : matBoneLocal);
		}
	}

	AppSkeleton::AppSkeleton(const Char* szName)
		: m_strName(szName)
	{

	}
	AppSkeleton::~AppSkeleton()
	{
		for (UInt32 u = 0; u < m_arrBone.ElementCount(); u++)
		{
			delete m_arrBone[u];
		}
		m_arrBone.Clear();
	}

	UInt32	AppSkeleton::GetBoneCount() const
	{
		return m_arrBone.ElementCount();
	}
	Boolean	AppSkeleton::AddBone(UInt32 uParentBoneIndex, const Char* szBoneName, const Matrix4& matInit)
	{
		if (uParentBoneIndex >= m_arrBone.ElementCount())return False;

		Bone* pNewBone = new Bone;
		pNewBone->strName = szBoneName;
		pNewBone->bUpdateToDate = False;
		pNewBone->matInitBoneLocal = matInit;
		pNewBone->matBoneLocal = pNewBone->matInitBoneLocal;
		pNewBone->pParent = m_arrBone[uParentBoneIndex];
		m_arrBone[uParentBoneIndex]->arrChild.Add(pNewBone);
		m_arrBone.Add(pNewBone);
		return True;
	}
	Boolean	AppSkeleton::AddBone(const Char* szParentBoneName, const Char* szBoneName, const Matrix4& matInit)
	{
		if (szParentBoneName == nullptr)
		{
			if (m_arrBone.ElementCount() > 0)
			{
				for (UInt32 u = 0; u < m_arrBone.ElementCount(); u++)
				{
					delete m_arrBone[u];
				}
				m_arrBone.Clear();
			}
			Bone* pNewBone = new Bone;
			pNewBone->matInitBoneLocal = matInit;
			pNewBone->strName = szBoneName;
			m_arrBone.Add(pNewBone);

			return True;
		}
		else if (m_arrBone.ElementCount() > 0)
		{
			return AddBone(GetBoneIndex(szParentBoneName), szBoneName, matInit);
		}
		return False;
	}

	const Char* AppSkeleton::GetBoneName(UInt32 uBoneIndex) const
	{
		if (uBoneIndex >= m_arrBone.ElementCount())
			return nullptr;

		return m_arrBone[uBoneIndex]->strName.CString();
	}
	UInt32 AppSkeleton::GetBoneIndex(const Char* szBoneName) const
	{
		for (UInt32 u = 0; u < m_arrBone.ElementCount(); u++)
		{
			if (m_arrBone[u]->strName == szBoneName)
			{
				return u;
			}
		}
		return (UInt32)-1;
	}
	Void	AppSkeleton::CalculateBoneToModels(const Array<Matrix4>& arrBoneLocal, Array<Matrix4>& arrResultBoneAbs)
	{
		ASSERT_IF_FAILED(arrBoneLocal.ElementCount() == m_arrBone.ElementCount());

		for (UInt32 u = 0; u < m_arrBone.ElementCount(); u++)
		{
			m_arrBone[u]->bUpdateToDate = False;
			m_arrBone[u]->matBoneLocal = arrBoneLocal[u];
		}

		for (UInt32 u = 0; u < m_arrBone.ElementCount(); u++)
		{
			m_arrBone[u]->Update();
		}

		arrResultBoneAbs.SetElementCount(m_arrBone.ElementCount());
		for (UInt32 u = 0; u < m_arrBone.ElementCount(); u++)
		{
			arrResultBoneAbs[u] = m_arrBone[u]->matBoneToModel;
		}
	}

	Boolean	AppSkeleton::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		if (strcmp(DeserializerBlock.GetBlockName(), "Skeleton") != 0)
			return False;

		Float1 fRes;
		DeserializerBlock.Load("BoneCount", fRes);
		m_arrBone.SetElementCount((UInt32)fRes);
		for (UInt32 u = 0; u < m_arrBone.ElementCount(); u++)
		{
			m_arrBone[u] = new Bone;
		}

		for (UInt32 u = 0; u < m_arrBone.ElementCount(); u++)
		{
			const IDeserializerBlock* pChildBlock = DeserializerBlock.GetChildBlock(u);
			Bone* pNewBone = m_arrBone[u];
			pChildBlock->Load("Name", pNewBone->strName);

			pChildBlock->Load("Local", pNewBone->matInitBoneLocal);
			pNewBone->matBoneLocal = pNewBone->matInitBoneLocal;

			UInt32 uChildCount = 0;
			pChildBlock->Load("ChildCount", uChildCount);

			String strRes;
			pChildBlock->Load("Child", strRes);

			UInt32 uDotIndex = 0;
			for (UInt32 uChild = 0; uChild < uChildCount; uChild++)
			{
				UInt32 uChildIndex = 0;
				sscanf(strRes.CString() + uDotIndex, "%d,", &uChildIndex);

				while (strRes[uDotIndex] != ',' && strRes[uDotIndex] != '\0') uDotIndex++;
				if (strRes[uDotIndex] == ',') uDotIndex++;

				m_arrBone[u]->arrChild.Add(m_arrBone[uChildIndex]);
				m_arrBone[uChildIndex]->pParent = m_arrBone[u];
			}
		}

		return True;
	}

#if MX_BUILD_EDITOR
	Boolean	AppSkeleton::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("Skeleton");

		Serializer.Save("BoneCount", (Float1)m_arrBone.ElementCount());

		for (UInt32 u = 0; u < m_arrBone.ElementCount(); u++)
		{
			Serializer.Save("{");
			Serializer.Save("Bone");

			Serializer.Save("Name", m_arrBone[u]->strName.CString());
			Serializer.Save("Local", m_arrBone[u]->matInitBoneLocal);

			if (m_arrBone[u]->arrChild.ElementCount() > 0)
			{
				Serializer.Save("ChildCount", m_arrBone[u]->arrChild.ElementCount());
				String strLineData;
				for (UInt32 uChild = 0; uChild < m_arrBone[u]->arrChild.ElementCount(); uChild++)
				{
					strLineData +=
						String::Format("%d,", GetBoneIndex(m_arrBone[u]->arrChild[uChild]->strName.CString()));
				}
				Serializer.Save("Child", strLineData.CString());
			}
			Serializer.Save("}");
		}
		return True;
	}
#endif
}