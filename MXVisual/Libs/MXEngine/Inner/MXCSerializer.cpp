#include "MXCSerializer.h"
#include "MXFile.h"
#include "MXConstant.h"
#include "stdio.h"

namespace MXVisual
{
	IDeserializer::~IDeserializer()
	{
		if (m_pRootBlock != nullptr)
			delete m_pRootBlock;
	}

	IDeserializerBlock::~IDeserializerBlock()
	{
		for (UInt32 u = 0; u < m_arrChildBlock.ElementCount(); u++)
		{
			delete m_arrChildBlock[u];
		}
		m_arrChildBlock.Clear();
	}
	const Char* IDeserializerBlock::GetBlockName() const { return m_strBlockName.CString(); }
	const IDeserializerBlock* IDeserializerBlock::GetChildBlock(UInt32 uIndex) const
	{
		return m_arrChildBlock[uIndex];
	}
	UInt32 IDeserializerBlock::GetChildBlocksCount() const
	{
		return m_arrChildBlock.ElementCount();
	}
}
