#include "MXAudioLoader.h"
#include "MXString.h"
#include "MXLog.h"

#define FLAC__NO_DLL
#include "FLAC/stream_decoder.h"


namespace MXVisual
{
	FLAC__StreamDecoderWriteStatus write_callback(const FLAC__StreamDecoder *decoder, const FLAC__Frame *frame, const FLAC__int32 * const buffer[], void *client_data)
	{
		AudioFileInfo* data = (AudioFileInfo*)client_data;
		if (data->uPCMDataSize > 0 &&
			data->Channels == 2)
		{
			if (frame->header.number.sample_number == 0 &&
				data->pPCMData == nullptr)
			{
				data->pPCMData = new UInt16[data->uPCMDataSize / 2]{ 0 };
			}

			Float1 ByteScale = (Float1)(2 << 16) / (2 << data->BytePerSample);
			for (int i = 0; i < frame->header.blocksize; i++)
			{
				UInt32 offset = frame->header.number.sample_number * 2;
				data->pPCMData[offset + i * 2] = buffer[0][i] * ByteScale;
				data->pPCMData[offset + i * 2 + 1] = buffer[1][i] * ByteScale;
			}
		}
		else
		{
			ASSERT_IF_FAILED(!"FLAC File Data Error");
			return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
		}
		return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
	}
	void metadata_callback(const FLAC__StreamDecoder *decoder, const FLAC__StreamMetadata *metadata, void *client_data)
	{
		AudioFileInfo* data = (AudioFileInfo*)client_data;

		if (metadata->type == FLAC__METADATA_TYPE_STREAMINFO)
		{
			data->TotalSamples = metadata->data.stream_info.total_samples;
			data->SampleRate = metadata->data.stream_info.sample_rate;
			data->Channels = metadata->data.stream_info.channels;
			data->BytePerSample = metadata->data.stream_info.bits_per_sample;

			data->uPCMDataSize = data->TotalSamples * data->Channels * sizeof(UInt16);
		}
	}
	void error_callback(const FLAC__StreamDecoder *decoder, FLAC__StreamDecoderErrorStatus status, void *client_data)
	{
		Log::Output("MediaResource", FLAC__StreamDecoderErrorStatusString[status]);
	}

	Boolean FLACLoader(const Char* szFileName, AudioFileInfo& result)
	{
		Boolean bDecodeSucceed = False;

		FLAC__StreamDecoder *decoder = FLAC__stream_decoder_new();

		FLAC__stream_decoder_set_md5_checking(decoder, true);

		FLAC__StreamDecoderInitStatus status = FLAC__stream_decoder_init_file(
			decoder, szFileName,
			write_callback,
			metadata_callback,
			error_callback,
			&result);
		if (status == FLAC__STREAM_DECODER_INIT_STATUS_OK)
		{
			bDecodeSucceed = True;
			FLAC__stream_decoder_process_until_end_of_stream(decoder);
		}

		FLAC__stream_decoder_delete(decoder);

		return bDecodeSucceed;
	}
}
