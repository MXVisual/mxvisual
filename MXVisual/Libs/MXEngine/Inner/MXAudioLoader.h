#pragma once
#ifndef _MX_AUDIO_LOADER_
#define _MX_AUDIO_LOADER_
#include "MXAsset.h"

namespace MXVisual
{
	struct AudioFileInfo
	{
		UInt32 TotalSamples = 0;
		UInt32 SampleRate = 0;
		UInt32 Channels = 0;
		UInt32 BytePerSample = 0;
		UInt32  uPCMDataSize = 0;
		UInt16* pPCMData = nullptr;
	};

	typedef Boolean (*AudioLoader)(const Char* szFileName, AudioFileInfo& result);

	Boolean FLACLoader(const Char* szFileName, AudioFileInfo& result);
}
#endif