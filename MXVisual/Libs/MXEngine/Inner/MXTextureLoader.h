#pragma once
#ifndef _MX_TEXTURE_LOADER_
#define _MX_TEXTURE_LOADER_
#include "MXAsset.h"

namespace MXVisual
{
	struct TextureFileInfo
	{
		UInt32 uWidth;
		UInt32 uHeight;
		UInt32 uDepth;
		UInt32 uMipLevel;
		Boolean bCubeMap;
		Boolean bArrayTex;
		IAppTexture::Format eFormat;
		Array<UInt8*> arrMipPixelData;
	};

	typedef Boolean (*TextureLoader)(const Char* szFileName, TextureFileInfo& result);

	Boolean DDSLoader(const Char* szFileName, TextureFileInfo& result);
}
#endif