#include "MXCScene.h"
#include "MXCObject.h"
#include "MXCComponent.h"
#include "MXCComponent2D.h"

#include "MXPhysicsSimulator.h"
#include "MXGraphicsRenderer.h"
#include "MXMediaPlayer.h"

#include "MXCCamera.h"
#include "MXTimer.h"

#include "MXFile.h"
#include <functional>

#include "MXCSerializer.h"
#include "MXLog.h"
#include "MXThread.h"

namespace MXVisual
{
	namespace
	{
		UInt32 g_uSceneCount = 0;

		Void TraverDepthFirst(ICObject* pObj, std::function<Void(ICObject*)>& FCallback)
		{
			if (pObj != nullptr && pObj->GetEnable())
			{
				for (UInt32 u = 0; u < pObj->GetChildCount(); u++)
				{
					TraverDepthFirst(pObj->GetChild(u), FCallback);
				}

				FCallback(pObj);
			}
		}

		Mutex	IOMainMutex;
		CScene* g_pActiveScene = nullptr;
	}
	CScene* CScene::ActivedScene()
	{
		CScene * pScene = nullptr;
		IOMainMutex.Lock();
		pScene = g_pActiveScene;
		IOMainMutex.UnLock();
		return pScene;
	}

	CScene::CScene(const Char * szName)
	{
		m_uUniqueID = g_uSceneCount;
		g_uSceneCount++;

		m_arrObject.clear();

		m_strName = szName;

		m_envProperty.f3AmbientColor = Float3(0.2f, 0.2f, 0.25f);
		m_envProperty.fAmbientScale = 1.0f;
		m_envProperty.bSSAO = True;
		m_envProperty.fSSAORange = 1.0f;
		m_envProperty.bSSR = True;
		m_envProperty.bBloom = True;
		m_envProperty.fBloomLuminance = 1.0f;
		m_envProperty.bHeightFog = False;
		m_envProperty.f3HeightFogColor = Float3(0.8f, 0.8f, 0.9f);
		m_envProperty.fHeightFogDensity = 1.0f;
		m_envProperty.fHeightFogStartDistance = 16.0f;
		m_envProperty.fHeightFogDistanceFalloff = 1.0f;
		m_envProperty.fHeightFogHeight = 16.0f;
		m_envProperty.fHeightFogHeightFalloff = 1.0f;
	}
	CScene::~CScene()
	{
		Inactive();
		OnRelease();
	}

	Void CScene::Active()
	{
		IOMainMutex.Lock();
		if (g_pActiveScene != this)
		{
			g_pActiveScene = this;
		}
		OnInit();
		IOMainMutex.UnLock();
	}

	Void CScene::Inactive()
	{
		if (g_pActiveScene == this)
		{
			g_pActiveScene = nullptr;
		}
	}

	CCamera*  CScene::MainCamera()
	{
		return  m_pMainCamera;
	}

	Void CScene::OnInit()
	{
		std::function<Void(ICObject*)> CallInit = [](ICObject* pObj)
		{
			pObj->OnInit();
		};

		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			TraverDepthFirst(*it, CallInit);
		}
	}
	Void CScene::OnUpdate()
	{
		OnTickInputEvent();

		//OnTick
		std::function<Void(ICObject*)> CallUpdate = [](ICObject* pObj)
		{
			pObj->OnUpdate();
		};
		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			TraverDepthFirst(*it, CallUpdate);
		}

		//OnUpdateLate
		std::function<Void(ICObject*)> CallUpdateLate = [](ICObject* pObj)
		{
			pObj->OnUpdateLate();
		};
		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			TraverDepthFirst(*it, CallUpdateLate);
		}

		MediaPlayer::CycleUpdate((Float1)Timer::GetDeltaTime());
		PhysicsSimulator::Simulate((Float1)Timer::GetDeltaTime());

		//OnUpdateFixDeltaTime
		std::function<Void(ICObject*)> CallUpdateFixedDeltaTime = [](ICObject* pObj)
		{
			pObj->OnUpdateFixedDeltaTime();
		};
		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			TraverDepthFirst(*it, CallUpdateFixedDeltaTime);
		}
	}
	Void CScene::OnRelease()
	{
		std::function<Void(ICObject*)> CallRelease = [](ICObject* pObj)
		{
			delete pObj;
		};

		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			TraverDepthFirst(*it, CallRelease);
		}
	}

	Void CScene::OnTickInputEvent()
	{
		Float2 MousePos = Input::GetMousePosition();

		std::function<Void(ICObject*)> CallEvent;
		CallEvent = [&CallEvent, MousePos](ICObject* pObj)
		{
			if (pObj->GetType() == eBase2D)
			{
				CObject2D* pObj2D = dynamic_cast<CObject2D*>(pObj);

				Float2 Scale = pObj2D->GetScale(False);
				Float2 Anchor = pObj2D->GetAnchorPosition();
				Float2 AnchorP = pObj2D->GetPosition(False);

				Float4 ltrb(
					Scale[0] * -Anchor[0],
					Scale[1] * -Anchor[1],
					Scale[0] * (1.0f - Anchor[0]),
					Scale[1] * (1.0f - Anchor[1])
				);

				Float3 DeltaP = Float3(MousePos[0] - AnchorP[0], MousePos[1] - AnchorP[1], 0.0f);
				Float3 DirR = Float3(pObj2D->GetDirRight()[0], pObj2D->GetDirRight()[1], 0.0f);
				Float3 DirU = Float3(pObj2D->GetDirUp()[0], pObj2D->GetDirUp()[1], 0.0f);

				Float1 r = Math::DotProduct(DeltaP, DirR);
				Float1 u = Math::DotProduct(DeltaP, DirU);
				if (r > ltrb[0] && r < ltrb[2] &&
					u > ltrb[1] && u < ltrb[3])
				{
					pObj2D->OnFocused();
				}
				else
				{
					pObj2D->OnLostFocus();
				}

				for (UInt32 u = 0; u < pObj2D->GetChildCount(); u++)
				{
					CallEvent(pObj2D->GetChild(u));
				}
			}
		};

		for each(auto obj in m_arrObject)
		{
			CallEvent(obj);
		}
	}

	Void CScene::RegisterObject(ICObject* pObj)
	{
		if (pObj == nullptr) return;

		pObj->OnInit();
		m_arrObject.insert(pObj);

		std::function<Void(ICObject*)> CallFindMainCamera = [this](ICObject* pObj)
		{
			if (pObj->GetType() == CObjectType::eCamera)
			{
				m_pMainCamera = dynamic_cast<CCamera*>(pObj);
			}
		};

		TraverDepthFirst(pObj, CallFindMainCamera);
	}

	ICObject* FindNameObject(ICObject* pObj, const Char* szName)
	{
		if (strcmp(pObj->GetName(), szName) == 0)
		{
			return pObj;
		}

		ICObject* pChildObj = nullptr;
		for (UInt32 uChild = 0; uChild < pObj->GetChildCount(); uChild++)
		{
			pChildObj = FindNameObject(pObj->GetChild(uChild), szName);
			if (pChildObj != nullptr)
			{
				break;
			}
		}

		return pChildObj;
	}
	ICObject* FindIDObject(ICObject* pObj, UInt32 uID)
	{
		if (pObj->GetUniqueID() == uID)
		{
			return pObj;
		}

		ICObject* pChildObj = nullptr;
		for (UInt32 uChild = 0; uChild < pObj->GetChildCount(); uChild++)
		{
			pChildObj = FindIDObject(pObj->GetChild(uChild), uID);
			if (pChildObj != nullptr)
			{
				break;
			}
		}

		return pChildObj;
	}
	ICObject* RemoveNameChildObject(ICObject* pObj, const Char* szName)
	{
		ICObject* pChildObj = nullptr;
		for (UInt32 uChild = 0; uChild < pObj->GetChildCount(); uChild++)
		{
			ICObject* pCurObj = pObj->GetChild(uChild);
			if (strcmp(pCurObj->GetName(), szName) == 0)
			{
				pChildObj = pObj->RemoveChild(uChild);

				for (UInt32 uChildChild = 0; uChildChild < pChildObj->GetChildCount(); uChildChild++)
				{
					pObj->AddChild(pChildObj->RemoveChild(uChildChild));
				}
				break;
			}

			pChildObj = RemoveNameChildObject(pObj->GetChild(uChild), szName);
			if (pChildObj != nullptr)
			{
				break;
			}
		}

		return pChildObj;
	}
	ICObject* RemoveIDChildObject(ICObject* pObj, UInt32 uID)
	{
		ICObject* pChildObj = nullptr;
		for (UInt32 uChild = 0; uChild < pObj->GetChildCount(); uChild++)
		{
			ICObject* pCurObj = pObj->GetChild(uChild);
			if (pCurObj->GetUniqueID() == uID)
			{
				pChildObj = pObj->RemoveChild(uChild);

				for (UInt32 uChildChild = 0; uChildChild < pChildObj->GetChildCount(); uChildChild++)
				{
					pObj->AddChild(pChildObj->RemoveChild(uChildChild));
				}
				break;
			}

			pChildObj = RemoveIDChildObject(pObj->GetChild(uChild), uID);
			if (pChildObj != nullptr)
			{
				break;
			}
		}

		return pChildObj;
	}

	ICObject* CScene::GetObject(const Char* szName)
	{
		ICObject* pRes = nullptr;
		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			pRes = FindNameObject(*it, szName);
			if (pRes)
				break;
		}
		return pRes;
	}
	ICObject* CScene::GetObject(UInt32 uID)
	{
		ICObject* pRes = nullptr;
		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			pRes = FindIDObject(*it, uID);
			if (pRes)
				break;
		}
		return pRes;
	}
	ICObject* CScene::GetRootObject(UInt32 uIndex)
	{
		if (m_arrObject.size() <= uIndex)return nullptr;
		UInt32 uCount = 0;
		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			if (uCount == uIndex)return *it;
			uCount++;
		}
		return nullptr;
	}

	ICObject* CScene::RemoveObject(const Char* szName)
	{
		ICObject* pRes = nullptr;
		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			if (strcmp((*it)->GetName(), szName) == 0)
			{
				pRes = *it;
				break;
			}

			pRes = RemoveNameChildObject(*it, szName);
			if (pRes != nullptr)
				break;
		}
		if (pRes != nullptr)
		{
			if (pRes == m_pMainCamera)
			{
				m_pMainCamera = nullptr;
			}

			if (m_arrObject.erase(pRes) >= 0)
			{
				for (UInt32 uChildChild = 0; uChildChild < pRes->GetChildCount(); uChildChild++)
				{
					RegisterObject(pRes->RemoveChild(uChildChild));
				}
			}
		}
		return pRes;
	}
	ICObject* CScene::RemoveObject(UInt32 uID)
	{
		ICObject* pRes = nullptr;
		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			if ((*it)->GetUniqueID() == uID)
			{
				pRes = *it;
				break;
			}

			pRes = RemoveIDChildObject(*it, uID);
			if (pRes != nullptr)
				break;
		}
		if (pRes != nullptr)
		{
			if (pRes == m_pMainCamera)
			{
				m_pMainCamera = nullptr;
			}

			if (m_arrObject.erase(pRes) >= 0)
			{
				for (UInt32 uChildChild = 0; uChildChild < pRes->GetChildCount(); uChildChild++)
				{
					RegisterObject(pRes->RemoveChild(uChildChild));
				}
			}
		}
		return pRes;
	}

#if MX_BUILD_EDITOR
	Void	CScene::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyColor("AmbientColor",
			Math::Combine(m_envProperty.f3AmbientColor, 1.0f));
		Panel.AddPropertyFloat1("AmbientScale",
			m_envProperty.fAmbientScale, 0.01f, 0.0f, 16.0f);
		Panel.AddPropertyTrigger("SSAO", m_envProperty.bSSAO);
		Panel.AddPropertyFloat1("SSAORange", m_envProperty.fSSAORange,
			0.01f, 0.0f, 128.0f);
		Panel.AddPropertyTrigger("SSR", m_envProperty.bSSR);
		Panel.AddPropertyTrigger("Bloom", m_envProperty.bBloom);
		Panel.AddPropertyFloat1("BloomLuminance",
			m_envProperty.fBloomLuminance, 0.01f, 0.0f, 16.0f);
		Panel.AddPropertyTrigger("HeightFog", GVarHeightFogEnable.bValue);
		Panel.AddPropertyColor("HeightFogColor",
			Math::Combine(m_envProperty.f3HeightFogColor, 1.0f));
		Panel.AddPropertyFloat1("HeightFogDensity", m_envProperty.fHeightFogDensity, 0.01f, 0.0f, 4.0f);
		Panel.AddPropertyFloat1("HeightFogStartDistance", m_envProperty.fHeightFogStartDistance, 0.01f, 0.0f, 512.0f);
		Panel.AddPropertyFloat1("HeightFogDistanceFalloff", m_envProperty.fHeightFogDistanceFalloff, 0.01f, 0.0f, 4.0f);
		Panel.AddPropertyFloat1("HeightFogHeight", m_envProperty.fHeightFogHeight, 0.01f, 0.0f, 512.0f);
		Panel.AddPropertyFloat1("HeightFogHeightFalloff", m_envProperty.fHeightFogHeightFalloff, 0.01f, 0.001f, 4.0f);
	}
	Void	CScene::OnPanelChanged(const IEditorProperty* pProperty)
	{
		if (strcmp(pProperty->Name, "AmbientColor") == 0)
		{
			m_envProperty.f3AmbientColor = Math::Split(((EditorPropertyColor*)pProperty)->Value);
		}
		else if (strcmp(pProperty->Name, "AmbientScale") == 0)
		{
			m_envProperty.fAmbientScale = ((EditorPropertyFloat1*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "Bloom") == 0)
		{
			m_envProperty.bBloom = ((EditorPropertyTrigger*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "BloomLuminance") == 0)
		{
			m_envProperty.fBloomLuminance = ((EditorPropertyFloat1*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "SSAO") == 0)
		{
			m_envProperty.bSSAO = ((EditorPropertyTrigger*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "SSAORange") == 0)
		{
			m_envProperty.fSSAORange = ((EditorPropertyFloat1*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "SSR") == 0)
		{
			m_envProperty.bSSR = ((EditorPropertyTrigger*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "HeightFog") == 0)
		{
			m_envProperty.bHeightFog = ((EditorPropertyTrigger*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "HeightFogColor") == 0)
		{
			m_envProperty.f3HeightFogColor = Math::Split(((EditorPropertyColor*)pProperty)->Value);
		}
		else if (strcmp(pProperty->Name, "HeightFogDensity") == 0)
		{
			m_envProperty.fHeightFogDensity = ((EditorPropertyFloat1*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "HeightFogStartDistance") == 0)
		{
			m_envProperty.fHeightFogStartDistance = ((EditorPropertyFloat1*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "HeightFogDistanceFalloff") == 0)
		{
			m_envProperty.fHeightFogDistanceFalloff = ((EditorPropertyFloat1*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "HeightFogHeight") == 0)
		{
			m_envProperty.fHeightFogHeight = ((EditorPropertyFloat1*)pProperty)->Value;
		}
		else if (strcmp(pProperty->Name, "HeightFogHeightFalloff") == 0)
		{
			m_envProperty.fHeightFogHeightFalloff = ((EditorPropertyFloat1*)pProperty)->Value;
		}
	}

	ICObject*	TreePickObject(ICObject* pNode, const Float3& f3Position, const Float3& f3Direction)
	{
		CObject* pObj = nullptr;
		if (pNode == nullptr)return pObj;

		/*Matrix4 t = pNode->GetTransform(False);
		ICPrimitiveComponent* pComp = pNode->GetComponent<ICPrimitiveComponent>();
		if (pComp)
		{
			UInt32 uPrimIndex = 0;
			IRPrimitive* pPrim = pComp->EnumPrimitive(uPrimIndex);
			while (pPrim != nullptr)
			{
				if (pPrim->CalIntersect(f3Position, f3Direction, 10000.0f, &t))
				{
					pObj = pNode;
					break;
				}
				uPrimIndex++;
				pPrim = pComp->EnumPrimitive(uPrimIndex);
			}
		}*/

		if (pObj != nullptr)return pObj;

		for (UInt32 u = 0; u < pNode->GetChildCount(); u++)
		{
			return TreePickObject(pNode->GetChild(u), f3Position, f3Direction);
		}
		return pObj;
	}

	ICObject*	CScene::PickVisibleObject(const Float3& f3Position, const Float3& f3Direction)
	{
		//TODO!! pick from object nearest camera

		ICObject* pRes = nullptr;
		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			pRes = TreePickObject((*it), f3Position, f3Direction);
			if (pRes != nullptr)
			{
				break;
			}
		}
		return pRes;
	}

	Void	SerializeObject(ISerializer& Serializer, ICObject* pObj)
	{
		const Char* szType = "";
		switch (pObj->GetType())
		{
		case MXVisual::CObjectType::eBase:
			szType = "Object";
			break;
		case MXVisual::CObjectType::eBase2D:
			szType = "Object2D";
			break;
		case MXVisual::CObjectType::eCamera:
			szType = "Camera";
			break;
		}
		Serializer.Save("{");
		Serializer.Save(szType);

		pObj->OnSerialize(Serializer);

		for (MXVisual::UInt32 u = 0; u < pObj->GetChildCount(); u++)
		{
			SerializeObject(Serializer, pObj->GetChild(u));
		}
		Serializer.Save("}");
	}

	Boolean	CScene::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("{");
		Serializer.Save("Scene");
		Serializer.Save("Name", m_strName.CString());

		Serializer.Save("AmbientColor", m_envProperty.f3AmbientColor);
		Serializer.Save("AmbientScale", m_envProperty.fAmbientScale);
		Serializer.Save("SSAO", m_envProperty.bSSAO);
		Serializer.Save("SSAORange", m_envProperty.fSSAORange);
		Serializer.Save("SSR", m_envProperty.bSSR);
		Serializer.Save("Bloom", m_envProperty.bBloom);
		Serializer.Save("BloomLuminance", m_envProperty.fBloomLuminance);
		Serializer.Save("HeightFog", m_envProperty.bHeightFog);
		Serializer.Save("HeightFogColor", m_envProperty.f3HeightFogColor);
		Serializer.Save("HeightFogDensity", m_envProperty.fHeightFogDensity);
		Serializer.Save("HeightFogStartDistance", m_envProperty.fHeightFogStartDistance);
		Serializer.Save("HeightFogDistanceFalloff", m_envProperty.fHeightFogDistanceFalloff);
		Serializer.Save("HeightFogHeight", m_envProperty.fHeightFogHeight);
		Serializer.Save("HeightFogHeightFalloff", m_envProperty.fHeightFogHeightFalloff);

		for (auto it = m_arrObject.begin(); it != m_arrObject.end(); it++)
		{
			SerializeObject(Serializer, *it);
		}
		Serializer.Save("}");
		return True;
	}
#endif
	ICObject* DeserializeObject(const IDeserializerBlock& DeserializerBlock)
	{
		String strName = DeserializerBlock.GetBlockName();
		String strValue;
		DeserializerBlock.Load("Name", strValue);
		ICObject* pObj = nullptr;
		if (strName == "Object")
		{
			pObj = new CObject("");
		}
		else if (strName == "Object2D")
		{
			pObj = new CObject2D("");
		}
		else if (strName == "Camera")
		{
			pObj = new CCamera("");
		}
		else
		{
			Log::Output("Deserialize Scene", String::Format("Error Object Type: %s", strName.CString()).CString());
			return nullptr;
		}

		pObj->OnDeserialize(DeserializerBlock);

		for (MXVisual::UInt32 u = 0; u < DeserializerBlock.GetChildBlocksCount(); u++)
		{
			const IDeserializerBlock* pChildBlock = DeserializerBlock.GetChildBlock(u);
			if (strcmp(pChildBlock->GetBlockName(), "Component") != 0)
			{
				MXVisual::ICObject* pChild = DeserializeObject(*pChildBlock);

				pObj->AddChild(pChild);
			}
		}

		return pObj;
	}

	Boolean CScene::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		DeserializerBlock.Load("Scene", m_strName);

		DeserializerBlock.Load("AmbientColor", m_envProperty.f3AmbientColor);
		DeserializerBlock.Load("AmbientScale", m_envProperty.fAmbientScale);
		DeserializerBlock.Load("SSAO", m_envProperty.bSSAO);
		DeserializerBlock.Load("SSAORange", m_envProperty.fSSAORange);
		DeserializerBlock.Load("SSR", m_envProperty.bSSR);
		DeserializerBlock.Load("Bloom", m_envProperty.bBloom);
		DeserializerBlock.Load("BloomLuminance", m_envProperty.fBloomLuminance);
		DeserializerBlock.Load("HeightFog", m_envProperty.bHeightFog);
		DeserializerBlock.Load("HeightFogColor", m_envProperty.f3HeightFogColor);
		DeserializerBlock.Load("HeightFogDensity", m_envProperty.fHeightFogDensity);
		DeserializerBlock.Load("HeightFogStartDistance", m_envProperty.fHeightFogStartDistance);
		DeserializerBlock.Load("HeightFogDistanceFalloff", m_envProperty.fHeightFogDistanceFalloff);
		DeserializerBlock.Load("HeightFogHeight", m_envProperty.fHeightFogHeight);
		DeserializerBlock.Load("HeightFogHeightFalloff", m_envProperty.fHeightFogHeightFalloff);

		for (MXVisual::UInt32 u = 0; u < DeserializerBlock.GetChildBlocksCount(); u++)
		{
			ICObject* pObj = DeserializeObject(*DeserializerBlock.GetChildBlock(u));
			if (pObj == nullptr)return False;

			RegisterObject(pObj);
		}
		return True;
	}

}
