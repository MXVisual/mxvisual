#include "MXCSerializer.h"
#include "MXFile.h"
#include "MXConstant.h"
#include "stdio.h"
#include <map>

namespace MXVisual
{
	const Char LongStringFlagCharacter = '\a';

	class TextDeserializerBlock : public IDeserializerBlock
	{
	public:
		std::map<String, String> strVarMap;

		Boolean Load(const Char* varName, String& strRefValue) const override
		{
			auto it = strVarMap.find(varName);
			if (it != strVarMap.end())
			{
				strRefValue = it->second;
				return True;
			}
			return False;
		}
		Boolean Load(const Char* varName, Boolean& bRefValue) const override
		{
			auto it = strVarMap.find(varName);
			if (it != strVarMap.end())
			{
				bRefValue = strcmp(it->second.CString(), "T") == 0 ? True : False;
				return True;
			}
			return False;
		}
		Boolean Load(const Char* varName, UInt32& uRefValue) const override
		{
			auto it = strVarMap.find(varName);
			if (it != strVarMap.end())
			{
				sscanf_s(it->second.CString(), "%d", &uRefValue);
				return True;
			}
			return False;
		}
		Boolean Load(const Char* varName, Float1& fRefValue) const override
		{
			auto it = strVarMap.find(varName);
			if (it != strVarMap.end())
			{
				sscanf_s(it->second.CString(), "%f", &fRefValue);
				return True;
			}
			return False;
		}
		Boolean Load(const Char* varName, Float2& f2RefValue) const override
		{
			auto it = strVarMap.find(varName);
			if (it != strVarMap.end())
			{
				sscanf_s(it->second.CString(), "%f,%f", &f2RefValue[0], &f2RefValue[1]);
				return True;
			}
			return False;
		}
		Boolean Load(const Char* varName, Float3& f3RefValue) const override
		{
			auto it = strVarMap.find(varName);
			if (it != strVarMap.end())
			{
				sscanf_s(it->second.CString(), "%f,%f,%f", &f3RefValue[0], &f3RefValue[1], &f3RefValue[2]);
				return True;
			}
			return False;
		}
		Boolean Load(const Char* varName, Float4& f4RefValue) const override
		{
			auto it = strVarMap.find(varName);
			if (it != strVarMap.end())
			{
				sscanf_s(it->second.CString(), "%f,%f,%f,%f", &f4RefValue[0], &f4RefValue[1], &f4RefValue[2], &f4RefValue[3]);
				return True;
			}
			return False;
		}
		Boolean Load(const Char* varName, Matrix4& mat4RefValue) const override
		{
			auto it = strVarMap.find(varName);
			if (it != strVarMap.end())
			{
				Float1 mat[4][4];

				sscanf(it->second.CString(), "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",
					&mat[0][0], &mat[0][1], &mat[0][2], &mat[0][3],
					&mat[1][0], &mat[1][1], &mat[1][2], &mat[1][3],
					&mat[2][0], &mat[2][1], &mat[2][2], &mat[2][3],
					&mat[3][0], &mat[3][1], &mat[3][2], &mat[3][3]);

				mat4RefValue = Matrix4(
					mat[0][0], mat[0][1], mat[0][2], mat[0][3],
					mat[1][0], mat[1][1], mat[1][2], mat[1][3],
					mat[2][0], mat[2][1], mat[2][2], mat[2][3],
					mat[3][0], mat[3][1], mat[3][2], mat[3][3]
				);
				return True;
			}
			return False;
		}

		TextDeserializerBlock(const Char* szBlockName)
		{
			m_strBlockName = szBlockName;
		}
		Void AddChild(IDeserializerBlock* pBlock)
		{
			m_arrChildBlock.Add(pBlock);
		}
	};

	//明文反序列化器
	IDeserializer* TextDeserializer::Open(const Char* szFileName)
	{
		IFile* pFile = IFile::Open(szFileName, IFile::EFDT_Text);
		if (pFile == nullptr)return nullptr;

		TextDeserializer* pDeserializer = new TextDeserializer;

		String strKey;
		String strBuf;
		Char cCharacter = 0;
		Boolean bInLongString = False;
		UInt32 uBlockDepth = 0;
		Array<TextDeserializerBlock*> ObjectHierarchyStack;
		while (pFile->Read(&cCharacter, 1) > 0)
		{
			if (!bInLongString && cCharacter == '{')
			{
				uBlockDepth++;
				continue;
			}

			if (!bInLongString && cCharacter == '}')
			{
				uBlockDepth--;
				//出栈
				ObjectHierarchyStack.Erase(ObjectHierarchyStack.ElementCount() - 1, 1);
				continue;
			}

			if (cCharacter == LongStringFlagCharacter && bInLongString)
			{
				bInLongString = False;
				continue;
			}

			if (cCharacter == ':' && strKey.IsEmpty())
			{
				pFile->Read(&cCharacter, 1);
				if (cCharacter == LongStringFlagCharacter)
				{
					bInLongString = True;
				}

				strKey = strBuf;
				strBuf.Clear();
			}

			if (!bInLongString && cCharacter == '\n')
			{
				if (!strBuf.IsEmpty())
				{
					if (ObjectHierarchyStack.ElementCount() != uBlockDepth)
					{
						TextDeserializerBlock* pNewBlock = new TextDeserializerBlock(strBuf.CString());
						if (ObjectHierarchyStack.ElementCount() > 0)
						{
							ObjectHierarchyStack[ObjectHierarchyStack.ElementCount() - 1]->AddChild(pNewBlock);
						}
						else if (pDeserializer->m_pRootBlock == nullptr)
						{
							pDeserializer->m_pRootBlock = pNewBlock;
						}
						ObjectHierarchyStack.Add(pNewBlock);
					}
					else
					{
						ObjectHierarchyStack[ObjectHierarchyStack.ElementCount() - 1]->strVarMap[strKey] = strBuf;
					}
					strKey.Clear();
					strBuf.Clear();
				}
				continue;
			}

			if ((!bInLongString && cCharacter == '\t') ||
				(bInLongString && cCharacter == LongStringFlagCharacter))
			{
				continue;
			}

			strBuf += cCharacter;
		}
		return pDeserializer;
	}

	Void TextDeserializer::Close()
	{
		delete this;
	}

	TextDeserializer::TextDeserializer()
	{

	}

	TextDeserializer::~TextDeserializer()
	{
	}

#if MX_BUILD_EDITOR
	ISerializer* TextSerializer::Open(const char* szFileName)
	{
		MXVisual::IFile* pFile = MXVisual::IFile::New(szFileName, MXVisual::IFile::EFDT_Text);
		if (pFile == nullptr)return nullptr;

		TextSerializer* pSerializer = new TextSerializer;
		pSerializer->m_pFile = pFile;
		return pSerializer;
	}
	void TextSerializer::Close()
	{
		if (m_pFile != nullptr)
		{
			m_pFile->Close();
			m_pFile = nullptr;
		}
		delete this;
	}

	Void TextSerializer::Save(const Char* szLine)
	{
		ASSERT_IF_FAILED(szLine != nullptr && szLine[0] != '\0');

		String str = String::Format("%s\n", szLine);
		m_pFile->Write((const UInt8*)str.CString(), str.Length());
	}
	Void TextSerializer::Save(const Char* szName, const Char* szValue)
	{
		ASSERT_IF_FAILED(szValue != nullptr && szValue[0] != '\0');
		if (szValue[0] == '\0')return;

		Boolean bLongString = strstr(szValue, "\n") != nullptr;
		String str = bLongString ?
			String::Format("%s:%c%s%c\n", szName, LongStringFlagCharacter, szValue, LongStringFlagCharacter) :
			String::Format("%s:%s\n", szName, szValue);
		m_pFile->Write((const UInt8*)str.CString(), str.Length());
	}
	Void TextSerializer::Save(const Char* szName, const Boolean bValue)
	{
		String str = String::Format("%s:%s\n", szName, bValue ? "T" : "F");
		m_pFile->Write((const UInt8*)str.CString(), str.Length());
	}
	Void TextSerializer::Save(const Char* szName, const UInt32 uValue)
	{
		String str = String::Format("%s:%d\n", szName, uValue);
		m_pFile->Write((const UInt8*)str.CString(), str.Length());
	}
	Void TextSerializer::Save(const Char* szName, const Float1 fValue)
	{
		String str = String::Format("%s:%f\n", szName, fValue);
		m_pFile->Write((const UInt8*)str.CString(), str.Length());
	}
	Void TextSerializer::Save(const Char* szName, const Float2& f2Value)
	{
		String str = String::Format("%s:%f,%f\n", szName, f2Value[0], f2Value[1]);
		m_pFile->Write((const UInt8*)str.CString(), str.Length());
	}
	Void TextSerializer::Save(const Char* szName, const Float3& f3Value)
	{
		String str = String::Format("%s:%f,%f,%f\n", szName, f3Value[0], f3Value[1], f3Value[2]);
		m_pFile->Write((const UInt8*)str.CString(), str.Length());
	}
	Void TextSerializer::Save(const Char* szName, const Float4& f4Value)
	{
		String str = String::Format("%s:%f,%f,%f,%f\n", szName, f4Value[0], f4Value[1], f4Value[2], f4Value[3]);
		m_pFile->Write((const UInt8*)str.CString(), str.Length());
	}
	Void TextSerializer::Save(const Char* szName, const Matrix4& mat4Value)
	{
		String str = String::Format("%s:%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n", szName,
			mat4Value.Get(0, 0), mat4Value.Get(0, 1), mat4Value.Get(0, 2), mat4Value.Get(0, 3),
			mat4Value.Get(1, 0), mat4Value.Get(1, 1), mat4Value.Get(1, 2), mat4Value.Get(1, 3),
			mat4Value.Get(2, 0), mat4Value.Get(2, 1), mat4Value.Get(2, 2), mat4Value.Get(2, 3),
			mat4Value.Get(3, 0), mat4Value.Get(3, 1), mat4Value.Get(3, 2), mat4Value.Get(3, 3));
		m_pFile->Write((const UInt8*)str.CString(), str.Length());
	}

	TextSerializer::TextSerializer()
		: m_pFile(nullptr)
	{

	}
	TextSerializer::~TextSerializer()
	{
		if (m_pFile != nullptr)
		{
			m_pFile->Close();
			m_pFile = nullptr;
		}
	}
#endif
}
