#include "MXAppTexture.h"
#include "MXMath.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	const PixelFormat GraphicsPixelFormatMap[IAppTexture::ETF_Count] = {
		PixelFormat::EPF_R8,
		PixelFormat::EPF_R8G8,
		PixelFormat::EPF_R8G8B8A8,

		PixelFormat::EPF_R16_F,
		PixelFormat::EPF_R16G16_F,
		PixelFormat::EPF_R16G16B16A16_F,

		PixelFormat::EPF_R32_F,
		PixelFormat::EPF_R32G32_F,
		PixelFormat::EPF_R32G32B32A32_F,

		PixelFormat::EPF_BC1,
		PixelFormat::EPF_BC3,
		PixelFormat::EPF_BC4,
		PixelFormat::EPF_BC5,
		PixelFormat::EPF_BC6H,
		PixelFormat::EPF_BC7,
	};
	const IAppTexture::Format AppTextureFormatMap[PixelFormat::EPF_Count] = {
		IAppTexture::ETF_R8,
		IAppTexture::ETF_RG8,
		IAppTexture::ETF_RGBA8,

		IAppTexture::ETF_R16,
		IAppTexture::ETF_RG16,
		IAppTexture::ETF_RGBA16,
		IAppTexture::ETF_Unknown,

		IAppTexture::ETF_R32,
		IAppTexture::ETF_RG32,
		IAppTexture::ETF_RGBA32,

		IAppTexture::ETF_BC1,
		IAppTexture::ETF_BC3,
		IAppTexture::ETF_BC4,
		IAppTexture::ETF_BC5,
		IAppTexture::ETF_BC6H,
		IAppTexture::ETF_BC7,
	};
	const UInt32	AppTextureFormatPixelBit[PixelFormat::EPF_Count] = {
		8,
		16,
		32,

		16,
		32,
		64,

		32,
		64,
		128,

		4,
		8,
		8,
		8,
		8,
		8,
	};

	AppTexture::AppTexture(const Char* szName)
		: m_strName(szName)
		, m_pTexture(nullptr)
	{

	}
	AppTexture::~AppTexture()
	{
		MX_RELEASE_INTERFACE(m_pTexture);
	}

	Boolean AppTexture::Init(const Char* szAssetName, Format eFormat,
		UInt32 uWidth, UInt32 uHeight, UInt32 uDepth, UInt32 uMipLevel,
		Boolean bArrayTex, Boolean bCubeTex, Boolean bTargetable)
	{
		IGTexture::Description desc;
		desc.eFormat = GraphicsPixelFormatMap[eFormat];
		desc.eType = TextureType::ETexture2D;
		desc.uWidth = uWidth;
		desc.uHeight = uHeight;
		desc.uDepth = uDepth;
		desc.bDynamic = True;
		desc.bGenerateMipMap = False;
		desc.uMipLevel = uMipLevel;
		desc.bTargetable = bTargetable;
		m_pTexture = GraphicsResource::CreateTexture(desc);

		if (eFormat < IAppTexture::ETF_BC1)//Uncompress Format
		{
			m_arrUncompressMipPixelDatas.SetElementCount(uMipLevel * uDepth);
			for (UInt32 u = 0; u < m_arrUncompressMipPixelDatas.ElementCount(); u++)
			{
				m_arrUncompressMipPixelDatas[u] = nullptr;
			}
		}
		return m_pTexture != nullptr;
	}

	IAppTexture::Format AppTexture::GetFormat() const
	{
		return AppTextureFormatMap[m_pTexture->GetFormat()];
	}

	Void  AppTexture::GetPixelData(UInt32 uMipmap, UInt32 uDepth, Array<UInt8>& arrResult) const
	{
		UInt32 uWidth = GetWidth();
		UInt32 uHeight = GetHeight();

		uWidth = Math::Max<SInt32>(1, uWidth >> uMipmap);
		uHeight = Math::Max<SInt32>(1, uHeight >> uMipmap);

		UInt32 uMipIndex = uMipmap * m_pTexture->GetDepth() + uDepth;
		if (uMipIndex < m_arrUncompressMipPixelDatas.ElementCount())
		{
			UInt32 PixelByte = AppTextureFormatPixelBit[GetFormat()] / 8;

			arrResult.SetElementCount(PixelByte * uWidth * uHeight);
			memcpy(&arrResult[0], m_arrUncompressMipPixelDatas[uMipIndex], PixelByte * uWidth * uHeight);
		}
	}

	Void  AppTexture::SetPixelData(UInt32 uMipmap, UInt32 uDepth, const UInt8* pPixelDatas)
	{
		UInt32 uWidth = GetWidth();
		UInt32 uHeight = GetHeight();

		uWidth = Math::Max<SInt32>(1, uWidth >> uMipmap);
		uHeight = Math::Max<SInt32>(1, uHeight >> uMipmap);
		UInt32 PixelByte = AppTextureFormatPixelBit[GetFormat()] / 8.0f;

		MapRegion region;
		if (m_pTexture->Map(uDepth, uMipmap, region))
		{
			UInt8* pDst = (UInt8*)region.pAddress;
			const UInt8* pSrc = pPixelDatas;
			for (UInt32 uY = 0; uY < uHeight; uY++)
			{
				memcpy(pDst, pSrc, uWidth * PixelByte);

				pDst += region.uPitch;
				pSrc += PixelByte * uWidth;
			}
			m_pTexture->Unmap();
		}

		UInt32 uMipIndex = uMipmap * m_pTexture->GetDepth() + uDepth;
		if (uMipIndex < m_arrUncompressMipPixelDatas.ElementCount())
		{
			UInt8* pNewBuf = new UInt8[PixelByte * uWidth * uHeight];
			memcpy(pNewBuf, pPixelDatas, PixelByte * uWidth * uHeight);
			m_arrUncompressMipPixelDatas[uMipIndex] = pNewBuf;
		}
	}

	Boolean	AppTexture::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		return True;
	}
#if MX_BUILD_EDITOR
	Boolean	AppTexture::OnSerialize(ISerializer& Serializer)
	{
		return True;
	}
#endif
}