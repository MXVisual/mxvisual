#include "MXCComponent.h"
#include "MXMath.h"
#include "MXGraphicsRenderer.h"
#include "MXGraphicsResource.h"

namespace MXVisual
{
	ICObject* ICComponent::GetObject()
	{
		return m_pRefObj;
	}
	Matrix4	ICComponent::GetObjectTransform(Boolean bLocal) const
	{
		return m_pRefObj == nullptr ? Matrix4::Identity : m_pRefObj->GetTransform(bLocal);;
	}
	Float3	ICComponent::GetObjectDirForward() const
	{
		Float4 dir = Math::MatrixTransform(
			Math::Combine(Math::AXIS_Z, 0), GetObjectTransform(False));
		return Math::Split(dir);
	}
	Float3	ICComponent::GetObjectDirRight() const
	{
		Float4 dir = Math::MatrixTransform(
			Math::Combine(Math::AXIS_X, 0), GetObjectTransform(False));
		return Math::Split(dir);
	}
	Float3	ICComponent::GetObjectDirUp() const
	{
		Float4 dir = Math::MatrixTransform(
			Math::Combine(Math::AXIS_Y, 0), GetObjectTransform(False));
		return Math::Split(dir);
	}
	Float3	ICComponent::GetObjectPosition(Boolean bLocal) const
	{
		return Math::MatrixGetTranslation(GetObjectTransform(bLocal));
	}
	Float3	ICComponent::GetObjectScale(Boolean bLocal) const
	{
		return Math::MatrixGetScale(GetObjectTransform(bLocal));
	}
	Quaternion	ICComponent::GetObjectRotation(Boolean bLocal) const
	{
		return Math::MatrixGetQuaternion(GetObjectTransform(bLocal));
	}

	Void	ICComponent::SetObjectTransform(const Matrix4& transform, Boolean bLocal)
	{
		if (m_pRefObj != nullptr)
			m_pRefObj->SetTransform(transform, bLocal);
	}

	ICComponent::ICComponent()
		: m_bEnable(True)
	{

	}
	ICComponent::~ICComponent()
	{}

	Void ICComponent::SetEnable(Boolean bEnable)
	{
		if (m_bEnable != bEnable)
		{
			m_bEnable = bEnable;
			if (m_bEnable)
			{
				//OnEnable();
			}
			else
			{
				//OnDisable();
			}
		}
	}
	Boolean ICComponent::GetEnable()
	{
		return m_bEnable;
	}
}
