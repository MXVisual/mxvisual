#include "MXAppFont.h"
#include "MXGraphicsResource.h"
#include FT_GLYPH_H
#include FT_OUTLINE_H

namespace MXVisual
{
	AppFont::AppFont(const Char* szName)
		: m_strName(szName)
		, m_uCharacterSize(10)
		, m_bBold(False)
		, m_bItalic(False)
		, m_pFontTexture(nullptr)
	{

	}
	AppFont::~AppFont()
	{
		FT_Done_Face(m_FTFace);
		MX_RELEASE_INTERFACE(m_pFontTexture);
	}

	Void AppFont::SetCharacterSize(UInt32 uSize)
	{
		if (m_uCharacterSize != uSize)
		{
			m_uCharacterSize = uSize;
			RenderFont();
		}
	}
	Void AppFont::SetBold(Boolean bBold)
	{
		if (m_bBold != bBold)
		{
			m_bBold = bBold;
			RenderFont();
		}
	}
	Void AppFont::SetItalic(Boolean bItalic)
	{
		if (m_bItalic != bItalic)
		{
			m_bItalic = bItalic;
			RenderFont();
		}
	}

	Float4 AppFont::GetCharacterRect(UInt16 uCharacter)
	{
		if (uCharacter < m_mapCharacterUV.ElementCount())
			return m_mapCharacterUV[uCharacter];

		return Float4();
	}

	Boolean AppFont::Init(const Char* szFileName, FT_Library ftLibrary)
	{
		FT_Error error = FT_New_Face(ftLibrary, szFileName, 0, &m_FTFace);
		if (m_FTFace == nullptr)
		{
			return False;
		}

		RenderFont();

		return 	m_pFontTexture != nullptr;
	}

	Void AppFont::RenderFont()
	{
		m_mapCharacterUV.Clear();

		FT_Set_Pixel_Sizes(m_FTFace, 0, m_uCharacterSize);
		FT_Select_Charmap(m_FTFace, FT_ENCODING_GB2312);

		const Float1 fToPixel = 1.0f / 64.0f;
		SInt32 charTop = SInt32(m_FTFace->size->metrics.ascender * fToPixel);
		SInt32 charBottom = SInt32(m_FTFace->size->metrics.descender * fToPixel);
		SInt32 charWidth = SInt32(m_FTFace->size->metrics.max_advance * fToPixel);
		SInt32 charHeight = charTop - charBottom;

		//Max Width 16k
		const UInt32 uMaxCharCount = 0xfff / charWidth;
		SInt32 nTexWidth = charWidth * uMaxCharCount;
		SInt32 nTexHeight = charHeight * uMaxCharCount;
		Array<UInt32> Datas(nTexWidth * nTexHeight);

		SInt32 nXOffset = 0;
		SInt32 nYOffset = 0;
		FT_UInt uIndex = 0;
		for (UInt16 u = 0; u < 256; u++)//ASCII���
		{
			uIndex = FT_Get_Char_Index(m_FTFace, u);
			FT_Load_Glyph(m_FTFace, uIndex, FT_LOAD_DEFAULT);

			if (m_bBold)
			{
				int strength = 1 << 6;
				FT_Outline_Embolden(&m_FTFace->glyph->outline, strength);
			}

			if (m_bItalic)
			{
				FT_Matrix ItalicMatrix;
				ItalicMatrix.xx = 1 << 16;
				ItalicMatrix.xy = 0x5800;
				ItalicMatrix.yx = 0;
				ItalicMatrix.yy = 1 << 16;
				FT_Outline_Transform(&m_FTFace->glyph->outline, &ItalicMatrix);
			}

			FT_Render_Glyph(m_FTFace->glyph, FT_RENDER_MODE_NORMAL);

			SInt32 nDataWidth = m_FTFace->glyph->bitmap.width;
			SInt32 nDataHeight = m_FTFace->glyph->bitmap.rows;
			SInt32 nXPadding = m_FTFace->glyph->bitmap_left;
			SInt32 nYPadding = charTop - m_FTFace->glyph->bitmap_top;

			for (SInt32 nX = 0; nX < nDataWidth; nX++)
			{
				for (SInt32 nY = 0; nY < nDataHeight; nY++)
				{
					UInt32 uDataX = nXOffset + nX + nXPadding;
					UInt32 uDataY = nYOffset + nY + nYPadding;
					UInt8 b = m_FTFace->glyph->bitmap.buffer[nX + nY * m_FTFace->glyph->bitmap.pitch];
					Datas[uDataX + uDataY * nTexWidth] = b > 0 ? 0xffffffff : 0;
				}
			}

			m_mapCharacterUV.Add(Float4((Float1)(nXOffset - 1) / nTexWidth,
				(Float1)(nYOffset - 1) / nTexHeight,
				(Float1)(nDataWidth + 2) / nTexWidth,
				(Float1)(charHeight + 2) / nTexHeight));

			nXOffset += charWidth;
			if (nXOffset >= nTexWidth)
			{
				nYOffset += charHeight;
				nXOffset = 0;
			}
		}

		if (m_pFontTexture == nullptr)
		{
			IGTexture::Description desc;
			desc.eFormat = PixelFormat::EPF_R8G8B8A8;
			desc.eType = TextureType::ETexture2D;
			desc.uWidth = nTexWidth;
			desc.uHeight = nTexHeight;
			desc.uDepth = 1;
			desc.bDynamic = False;
			desc.bGenerateMipMap = False;
			desc.uMipLevel = 1;
			m_pFontTexture = GraphicsResource::CreateTexture(desc);
		}

		MapRegion region;
		if (m_pFontTexture->Map(0, 0, region))
		{
			for (UInt32 u = 0; u < nTexHeight; u++)
			{
				memcpy(region.pAddress, &Datas.GetRawData()[u * nTexWidth * 4], nTexWidth * 4);

				region.pAddress = (UInt8*)region.pAddress + region.uPitch;
			}
			m_pFontTexture->Unmap();
		}
	}
}