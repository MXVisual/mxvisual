#pragma once
#ifndef _MX_APP_MATERIAL_
#define _MX_APP_MATERIAL_
#include "MXAsset.h"
#include "MXString.h"
#include "MXGraphicsResource.h"

namespace MXVisual
{
	class AppMaterial
		: public IAppMaterial
	{
	public:
		AppMaterial(const Char* szName);
		~AppMaterial();
		const Char* GetName() const override { return m_strName.CString(); }

		Boolean Init(Boolean bCanCache);

		Domain GetDomain() const override { return (Domain)m_descMaterial.eDomain; }
		ShadingModel GetShadingModel() const override { return (ShadingModel)m_descMaterial.eShadingModel; }
		FaceType GetFaceType() const override { return (FaceType)m_descMaterial.eFaceType; }
		BlendMode GetBlendMode() const override { return (BlendMode)m_descMaterial.eBlendMode; }

#if MX_BUILD_EDITOR
		UInt32 GetParameterCount() const override { return m_descMaterial.arrMaterialParams.ElementCount(); }
		ParameterDescription GetParameterDescription(UInt32 uParameter) const override;
		ParameterDescription GetParameterDescription(const Char* szParameter) const override;

		Void SetDomain(Domain eType);
		Void SetShadingModel(ShadingModel eType) override;
		Void SetFaceType(FaceType eType) override;
		Void SetBlendMode(BlendMode eType) override;
		Void SetMaterialFunc(MaterialFunction eType, const String& strCode) override;
		const Char* GetMaterialFunc(MaterialFunction eType) const override;

		Void RemoveParameter(const Char* szName) override;
		Void SetParameter(const ParameterDescription& desc) override;

		Boolean AppMaterial::Recompile() override;
#endif

		IGMaterial* GetGraphicsResource() const { return m_pMaterial; }
	protected:
		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override;
#if MX_BUILD_EDITOR
		Boolean	OnSerialize(ISerializer& Serializer) override;
#endif

	protected:
		String m_strName;
		IGMaterial* m_pMaterial;
		IGMaterial::Description m_descMaterial;
#if MX_BUILD_EDITOR
		Array<ParameterDescription> m_arrParameterDesc;
#endif
	};
}
#endif