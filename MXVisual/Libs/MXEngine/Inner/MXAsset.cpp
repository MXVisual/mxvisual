#include "MXAsset.h"
#include "MXString.h"

#include "MXFile.h"
#include "MXCSerializer.h"
#include "MXLog.h"

#include "ft2build.h"
#include FT_FREETYPE_H

#include "MXAppFont.h"
#include "MXAppSkeleton.h"
#include "MXAppAnimationClip.h"
#include "MXAppMesh.h"
#include "MXAppMeshInstance.h"
#include "MXAppMaterial.h"
#include "MXAppMaterialInstance.h"
#include "MXAppTexture.h"
#include "MXAppAudio.h"

#include "MXAudioLoader.h"
#include "MXTextureLoader.h"

#include "MXCDefaultComponent.h"

#include <unordered_map>
#include <string>
namespace std
{
	template<>
	struct hash<MXVisual::String>
	{
		size_t operator() (const MXVisual::String& s) const noexcept
		{
			std::hash<string> h;
			return h(s.CString());
		}
	};
}

extern MXVisual::Void			EnumerateComponents_Script_Project(MXVisual::Array<MXVisual::String>& arrComponentName);
extern MXVisual::ICComponent*	CreateComponent_Script_Project(const MXVisual::Char* szComponentName);

namespace MXVisual
{
	namespace DefaultResource
	{
		const Char* MESH_PLANE = "DEFAULT_MESH_PLANE.mmh";
		const Char* MESH_CUBE = "DEFAULT_MESH_CUBE.mmh";
		const Char* MESH_SPHERE = "DEFAULT_MESH_SPHERE.mmh";

		const Char* MATERIAL = "DEFAULT_MATERIAL.mml";
		const Char* MATERIALINSTANCE = "DEFAULT_MATERIALINSTANCE.mmli";

		const Char* MOSAIC_TEXTURE = "DEFAULT_MOSAIC_TEXTURE.dds";
		const Char* NOISE_TEXTURE = "DEFAULT_NOISE_TEXTURE.dds";

		const Char* RED_TEXTURE = "DEFAULT_RED_TEXTURE.dds";
		const Char* GREEN_TEXTURE = "DEFAULT_GREEN_TEXTURE.dds";
		const Char* BLUE_TEXTURE = "DEFAULT_BLUE_TEXTURE.dds";
		const Char* BLACK_TEXTURE = "DEFAULT_BLACK_TEXTURE.dds";
		const Char* WHITE_TEXTURE = "DEFAULT_WHITE_TEXTURE.dds";
		const Char* FLATNORMAL_TEXTURE = "DEFAULT_FLATNORMAL_TEXTURE.dds";

		const Char* COMPONENT_NAME_DIRECTION_LIGHT = "CDirectionLightComponent";
		const Char* COMPONENT_NAME_SPOT_LIGHT = "CSpotLightComponent";
		const Char* COMPONENT_NAME_POINT_LIGHT = "CPointLightComponent";
	}

	Void			ICComponent::Enumerate(Array<String>& arrComponentName)
	{
#define MAKE_COMPONENT(name) arrComponentName.Add(#name);

		MAKE_COMPONENT(CSkinMeshComponent);
		MAKE_COMPONENT(CRigidMeshComponent);
		MAKE_COMPONENT(CTerrainComponent);
		MAKE_COMPONENT(CRigidBodyComponent);
		MAKE_COMPONENT(CAnimatorComponent);

		MAKE_COMPONENT(CDirectionLightComponent);
		MAKE_COMPONENT(CSpotLightComponent);
		MAKE_COMPONENT(CPointLightComponent);

		MAKE_COMPONENT(CParticleSystemComponent);

		MAKE_COMPONENT(CPostProcessAreaComponent);
		MAKE_COMPONENT(CReflectionProbeComponent);

		MAKE_COMPONENT(CUIButtonComponent);
		MAKE_COMPONENT(CUIImageComponent);
		MAKE_COMPONENT(CUITextComponent);

		MAKE_COMPONENT(CAudioComponent);

#undef MAKE_COMPONENT

		EnumerateComponents_Script_Project(arrComponentName);
	}
	ICComponent*	ICComponent::Create(const Char* szComponentName)
	{
#define REALISE_(type)\
	if(_stricmp(szComponentName, #type) == 0)\
		return new type;

#define REALISE_2(name, type)\
	if(_stricmp(szComponentName, #name) == 0)\
		return new type;

		REALISE_(CSkinMeshComponent);
		REALISE_(CRigidMeshComponent);
		REALISE_(CTerrainComponent);
		REALISE_2(CRigidBodyComponent, MXCRigidBodyComponent);
		REALISE_(CAnimatorComponent);

		REALISE_(CDirectionLightComponent);
		REALISE_(CSpotLightComponent);
		REALISE_(CPointLightComponent);

		REALISE_(CParticleSystemComponent);

		REALISE_(CPostProcessAreaComponent);
		REALISE_(CReflectionProbeComponent);

		REALISE_(CUIButtonComponent);
		REALISE_(CUIImageComponent);
		REALISE_(CUITextComponent);

		REALISE_2(CAudioComponent, MXCAudioComponent);

#undef REALISE_

		return CreateComponent_Script_Project(szComponentName);
	}

	namespace
	{
		String	m_strAssetsPath;
		FT_Library	m_FTLibrary;

		std::unordered_map<String, IAppAsset*> m_mapAssets;
		Void	DeleteAsset(const Char* szAssetName)
		{
			auto it = m_mapAssets.find(szAssetName);
			if (it != m_mapAssets.end())
			{
				IAppAsset* pAsset = it->second;
				m_mapAssets.erase(szAssetName);
				delete pAsset;
			}
		}
		Void	ConvertAssetName(const Char* szAssetName, String& resultAssetName, String& resultAssetPathName)
		{
			resultAssetName = szAssetName;
			if (resultAssetName.IsEmpty())return;

			if (resultAssetName[1] == ':')
			{
				Char buffer[Constant_MAX_PATH] = { 0 };
				CalRelativePath(m_strAssetsPath.CString(), resultAssetName.CString(),
					buffer, Constant_MAX_PATH);

				resultAssetName = buffer;
			}
			resultAssetPathName = String::Format("%s\\%s", m_strAssetsPath.CString(), resultAssetName.CString());
		}

		IAppMaterial* pDefaultMaterial = nullptr;
		IAppMaterialInstance* pDefaultMaterialInstance = nullptr;

		IAppTexture* pMosaicTexture = nullptr;
		IAppTexture* pNoiseTexture = nullptr;
		IAppTexture* pRedTexture = nullptr;
		IAppTexture* pGreenTexture = nullptr;
		IAppTexture* pBlueTexture = nullptr;
		IAppTexture* pBlackTexture = nullptr;
		IAppTexture* pWhiteTexture = nullptr;
		IAppTexture* pFlatNormalTexture = nullptr;

		IAppMesh* pCubeMesh = nullptr;
		IAppMesh* pPlaneMesh = nullptr;
		IAppMesh* pSphereMesh = nullptr;
	}

	Void IAppAsset::Release()
	{
		m_nRefCount--;
		if (m_nRefCount <= 0)
		{
			DeleteAsset(GetName());
		}
	}
	const Char* IAppFont::Suffix = "ttf";
	const Char* IAppTexture::Suffix = "dds";
	const Char* IAppSkeleton::Suffix = "msk";
	const Char* IAppAnimationClip::Suffix = "man";
	const Char* IAppMesh::Suffix = "mmh";
	const Char* IAppMeshInstance::Suffix = "mmhi";
	const Char* IAppMaterial::Suffix = "mml";
	const Char* IAppMaterialInstance::Suffix = "mmli";
	const Char* IAppAudio::Suffix = "flac";

	IAppTexture* IAppTexture::Create(const Char* szAssetName, Format eFormat,
		UInt32 uWidth, UInt32 uHeight, UInt32 uDepth, UInt32 uMipLevel,
		Boolean bArrayTex, Boolean bCubeTex, Boolean bTargetable)
	{
		auto it = m_mapAssets.find(szAssetName);
		if (it != m_mapAssets.end())
		{
			return nullptr;
		}

		AppTexture* pNewAsset = new AppTexture(szAssetName);
		if (!pNewAsset->Init(szAssetName, eFormat, uWidth, uHeight, uDepth, uMipLevel, bArrayTex, bCubeTex, bTargetable))
		{
			delete pNewAsset;
			return nullptr;
		}
		m_mapAssets[szAssetName] = pNewAsset;
		return pNewAsset;
	}
	IAppSkeleton*		IAppSkeleton::Create(const Char* szAssetName)
	{
		auto it = m_mapAssets.find(szAssetName);
		if (it != m_mapAssets.end())
		{
			return nullptr;
		}

		AppSkeleton* pNewAsset = new AppSkeleton(szAssetName);
		m_mapAssets[szAssetName] = pNewAsset;
		return pNewAsset;
	}
	IAppAnimationClip*	IAppAnimationClip::Create(const Char* szAssetName, Float1 fSampleRate)
	{
		auto it = m_mapAssets.find(szAssetName);
		if (it != m_mapAssets.end())
		{
			return nullptr;
		}

		AppAnimationClip* pNewAsset = new AppAnimationClip(szAssetName);
		pNewAsset->SetSampleRate(fSampleRate);

		m_mapAssets[szAssetName] = pNewAsset;
		return pNewAsset;
	}
	IAppMesh*			IAppMesh::Create(const Char* szAssetName, MeshType eType, Boolean b32BitIndex, UInt32 uVertex, UInt32 uIndex, UInt32 uSubMeshCount)
	{
		auto it = m_mapAssets.find(szAssetName);
		if (it != m_mapAssets.end())
		{
			return nullptr;
		}

		AppMesh* pNewAsset = new AppMesh(szAssetName);
		if (!pNewAsset->Init(eType, b32BitIndex, True, uVertex, uIndex, uSubMeshCount))
		{
			delete pNewAsset;
			return nullptr;
		}

		m_mapAssets[szAssetName] = pNewAsset;
		return pNewAsset;
	}
	IAppMaterial*		IAppMaterial::Create(const Char* szAssetName, Boolean bCanCache)
	{
		auto it = m_mapAssets.find(szAssetName);
		if (it != m_mapAssets.end())
		{
			return nullptr;
		}

		AppMaterial* pNewAsset = new AppMaterial(szAssetName);
		if (!pNewAsset->Init(bCanCache))
		{
			delete pNewAsset;
			return nullptr;
		}
		m_mapAssets[szAssetName] = pNewAsset;
		return pNewAsset;
	}
	IAppMeshInstance*	IAppMeshInstance::Create(const Char* szAssetName, const Char* szMeshAssetName)
	{
		String strAssetName;
		String strAssetPathName;
		ConvertAssetName(szMeshAssetName, strAssetName, strAssetPathName);

		auto it = m_mapAssets.find(szAssetName);
		if (it != m_mapAssets.end())
		{
			return nullptr;
		}

		AppMeshInstance* pNewAsset = new AppMeshInstance(szAssetName);
		pNewAsset->SetMesh(strAssetName.CString());
		m_mapAssets[szAssetName] = pNewAsset;
		return pNewAsset;
	}
	IAppMaterialInstance* IAppMaterialInstance::Create(const Char* szAssetName, const Char* szMaterial)
	{
		String strAssetName;
		String strAssetPathName;
		ConvertAssetName(szMaterial, strAssetName, strAssetPathName);

		auto it = m_mapAssets.find(szAssetName);
		if (it != m_mapAssets.end())
		{
			return nullptr;
		}

		AppMaterialInstance* pNewAsset = new AppMaterialInstance(szAssetName);
		if (!pNewAsset->Init(szMaterial))
		{
			delete pNewAsset;
			return False;
		}
		m_mapAssets[szAssetName] = pNewAsset;
		return pNewAsset;
	}

	Boolean InitCubeMesh()
	{
		const UInt32 uVertex = 24;
		const UInt32 uIndex = 36;

		pCubeMesh = IAppMesh::Create(DefaultResource::MESH_CUBE, IAppMesh::EAPPMT_Rigid, True, uVertex, uIndex, 1);

		const Float1 fUVScale = 2.0f;
		IAppMesh::VertexRigid v[uVertex];

		v[0].f4Position = Float4(-1.0f, 1.0f, -1.0f, 0.0f);
		v[0].f4Normal = Float4(0.0f, 0.0f, -1.0f, 0.0f);
		v[0].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[0].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[0].f2TexCoord0 = Float2(0.0f, 0.0f);
		v[1].f4Position = Float4(1.0f, 1.0f, -1.0f, 0.0f);
		v[1].f4Normal = Float4(0.0f, 0.0f, -1.0f, 0.0f);
		v[1].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[1].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[1].f2TexCoord0 = Float2(fUVScale, 0.0f);
		v[2].f4Position = Float4(-1.0f, -1.0f, -1.0f, 0.0f);
		v[2].f4Normal = Float4(0.0f, 0.0f, -1.0f, 0.0f);
		v[2].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[2].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[2].f2TexCoord0 = Float2(0.0f, fUVScale);
		v[3].f4Position = Float4(1.0f, -1.0f, -1.0f, 0.0f);
		v[3].f4Normal = Float4(0.0f, 0.0f, -1.0f, 0.0f);
		v[3].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[3].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[3].f2TexCoord0 = Float2(fUVScale, fUVScale);

		v[4].f4Position = Float4(1.0f, 1.0f, -1.0f, 0.0f);
		v[4].f4Normal = Float4(1.0f, 0.0f, 0.0f, 0.0f);
		v[4].f4Tangent = Float4(0.0f, 0.0f, 1.0f, 1.0f);
		v[4].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[4].f2TexCoord0 = Float2(0.0f, 0.0f);
		v[5].f4Position = Float4(1.0f, 1.0f, 1.0f, 0.0f);
		v[5].f4Normal = Float4(1.0f, 0.0f, 0.0f, 0.0f);
		v[5].f4Tangent = Float4(0.0f, 0.0f, 1.0f, 1.0f);
		v[5].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[5].f2TexCoord0 = Float2(fUVScale, 0.0f);
		v[6].f4Position = Float4(1.0f, -1.0f, -1.0f, 0.0f);
		v[6].f4Normal = Float4(1.0f, 0.0f, 0.0f, 0.0f);
		v[6].f4Tangent = Float4(0.0f, 0.0f, 1.0f, 1.0f);
		v[6].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[6].f2TexCoord0 = Float2(0.0f, fUVScale);
		v[7].f4Position = Float4(1.0f, -1.0f, 1.0f, 0.0f);
		v[7].f4Normal = Float4(1.0f, 0.0f, 0.0f, 0.0f);
		v[7].f4Tangent = Float4(0.0f, 0.0f, 1.0f, 1.0f);
		v[7].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[7].f2TexCoord0 = Float2(fUVScale, fUVScale);

		v[8].f4Position = Float4(1.0f, 1.0f, 1.0f, 0.0f);
		v[8].f4Normal = Float4(0.0f, 0.0f, 1.0f, 0.0f);
		v[8].f4Tangent = Float4(-1.0f, 0.0f, 0.0f, 1.0f);
		v[8].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[8].f2TexCoord0 = Float2(0.0f, 0.0f);
		v[9].f4Position = Float4(-1.0f, 1.0f, 1.0f, 0.0f);
		v[9].f4Normal = Float4(0.0f, 0.0f, 1.0f, 0.0f);
		v[9].f4Tangent = Float4(-1.0f, 0.0f, 0.0f, 1.0f);
		v[9].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[9].f2TexCoord0 = Float2(fUVScale, 0.0f);
		v[10].f4Position = Float4(1.0f, -1.0f, 1.0f, 0.0f);
		v[10].f4Normal = Float4(0.0f, 0.0f, 1.0f, 0.0f);
		v[10].f4Tangent = Float4(-1.0f, 0.0f, 0.0f, 1.0f);
		v[10].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[10].f2TexCoord0 = Float2(0.0f, fUVScale);
		v[11].f4Position = Float4(-1.0f, -1.0f, 1.0f, 0.0f);
		v[11].f4Normal = Float4(0.0f, 0.0f, 1.0f, 0.0f);
		v[11].f4Tangent = Float4(-1.0f, 0.0f, 0.0f, 1.0f);
		v[11].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[11].f2TexCoord0 = Float2(fUVScale, fUVScale);

		v[12].f4Position = Float4(-1.0f, 1.0f, 1.0f, 0.0f);
		v[12].f4Normal = Float4(-1.0f, 0.0f, 0.0f, 0.0f);
		v[12].f4Tangent = Float4(0.0f, 0.0f, -1.0f, 1.0f);
		v[12].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[12].f2TexCoord0 = Float2(0.0f, 0.0f);
		v[13].f4Position = Float4(-1.0f, 1.0f, -1.0f, 0.0f);
		v[13].f4Normal = Float4(-1.0f, 0.0f, 0.0f, 0.0f);
		v[13].f4Tangent = Float4(0.0f, 0.0f, -1.0f, 1.0f);
		v[13].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[13].f2TexCoord0 = Float2(fUVScale, 0.0f);
		v[14].f4Position = Float4(-1.0f, -1.0f, 1.0f, 0.0f);
		v[14].f4Normal = Float4(-1.0f, 0.0f, 0.0f, 0.0f);
		v[14].f4Tangent = Float4(0.0f, 0.0f, 1.0f, 1.0f);
		v[14].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[14].f2TexCoord0 = Float2(0.0f, fUVScale);
		v[15].f4Position = Float4(-1.0f, -1.0f, -1.0f, 0.0f);
		v[15].f4Normal = Float4(-1.0f, 0.0f, 0.0f, 0.0f);
		v[15].f4Tangent = Float4(0.0f, 0.0f, -1.0f, 1.0f);
		v[15].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[15].f2TexCoord0 = Float2(fUVScale, fUVScale);

		v[16].f4Position = Float4(-1.0f, 1.0f, 1.0f, 0.0f);
		v[16].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[16].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[16].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[16].f2TexCoord0 = Float2(0.0f, 0.0f);
		v[17].f4Position = Float4(1.0f, 1.0f, 1.0f, 0.0f);
		v[17].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[17].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[17].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[17].f2TexCoord0 = Float2(fUVScale, 0.0f);
		v[18].f4Position = Float4(-1.0f, 1.0f, -1.0f, 0.0f);
		v[18].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[18].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[18].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[18].f2TexCoord0 = Float2(0.0f, fUVScale);
		v[19].f4Position = Float4(1.0f, 1.0f, -1.0f, 0.0f);
		v[19].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[19].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[19].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[19].f2TexCoord0 = Float2(fUVScale, fUVScale);

		v[20].f4Position = Float4(-1.0f, -1.0f, -1.0f, 0.0f);
		v[20].f4Normal = Float4(0.0f, -1.0f, 0.0f, 0.0f);
		v[20].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[20].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[20].f2TexCoord0 = Float2(0.0f, 0.0f);
		v[21].f4Position = Float4(1.0f, -1.0f, -1.0f, 0.0f);
		v[21].f4Normal = Float4(0.0f, -1.0f, 0.0f, 0.0f);
		v[21].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[21].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[21].f2TexCoord0 = Float2(fUVScale, 0.0f);
		v[22].f4Position = Float4(-1.0f, -1.0f, 1.0f, 0.0f);
		v[22].f4Normal = Float4(0.0f, -1.0f, 0.0f, 0.0f);
		v[22].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[22].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[22].f2TexCoord0 = Float2(0.0f, fUVScale);
		v[23].f4Position = Float4(1.0f, -1.0f, 1.0f, 0.0f);
		v[23].f4Normal = Float4(0.0f, -1.0f, 0.0f, 0.0f);
		v[23].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[23].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[23].f2TexCoord0 = Float2(fUVScale, fUVScale);

		pCubeMesh->SetVertices(uVertex, v);

		UInt32 i[uIndex] = {
			0,1,2,		1,3,2,
			4,5,6,		5,7,6,
			8,9,10,		9,11,10,
			12,13,14,	13,15,14,
			16,17,18,	17,19,18,
			20,21,22,	21,23,22,
		};
		pCubeMesh->SetIndices(uIndex, (const UInt8*)i);
		pCubeMesh->SetSubMesh(0, 0, uIndex);
		return pCubeMesh != nullptr;
	}
	Boolean InitPlaneMesh()
	{
		const UInt32 uVertex = 4;
		const UInt32 uIndex = 6;

		pPlaneMesh = IAppMesh::Create(DefaultResource::MESH_PLANE, IAppMesh::EAPPMT_Rigid, True, uVertex, uIndex, 1);

		const Float1 fUVScale = 2.0f;
		IAppMesh::VertexRigid v[uVertex];

		v[0].f4Position = Float4(-1.0f, 0.0f, 1.0f, 0.0f);
		v[0].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[0].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[0].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[0].f2TexCoord0 = Float2(0.0f, 0.0f);
		v[1].f4Position = Float4(1.0f, 0.0f, 1.0f, 0.0f);
		v[1].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[1].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[1].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[1].f2TexCoord0 = Float2(fUVScale, 0.0f);
		v[2].f4Position = Float4(-1.0f, 0.0f, -1.0f, 0.0f);
		v[2].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[2].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[2].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[2].f2TexCoord0 = Float2(0.0f, fUVScale);
		v[3].f4Position = Float4(1.0f, 0.0f, -1.0f, 0.0f);
		v[3].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[3].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[3].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[3].f2TexCoord0 = Float2(fUVScale, fUVScale);
		pPlaneMesh->SetVertices(uVertex, v);

		UInt32 i[uIndex] = { 0,1,2,	1,3,2, };
		pPlaneMesh->SetIndices(uIndex, (const UInt8*)i);
		pPlaneMesh->SetSubMesh(0, 0, uIndex);
		return pPlaneMesh != nullptr;
	}
	Boolean InitSphereMesh()
	{
		const UInt32 uVertex = 4;
		const UInt32 uIndex = 6;

		pSphereMesh = IAppMesh::Create(DefaultResource::MESH_SPHERE, IAppMesh::EAPPMT_Rigid, True, uVertex, uIndex, 1);

		const Float1 fUVScale = 2.0f;
		IAppMesh::VertexRigid v[uVertex];

		v[0].f4Position = Float4(-1.0f, 1.0f, 1.0f, 0.0f);
		v[0].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[0].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[0].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[0].f2TexCoord0 = Float2(0.0f, 0.0f);
		v[1].f4Position = Float4(1.0f, 1.0f, 1.0f, 0.0f);
		v[1].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[1].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[1].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[1].f2TexCoord0 = Float2(fUVScale, 0.0f);
		v[2].f4Position = Float4(-1.0f, 1.0f, -1.0f, 0.0f);
		v[2].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[2].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[2].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[2].f2TexCoord0 = Float2(0.0f, fUVScale);
		v[3].f4Position = Float4(1.0f, 1.0f, -1.0f, 0.0f);
		v[3].f4Normal = Float4(0.0f, 1.0f, 0.0f, 0.0f);
		v[3].f4Tangent = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		v[3].f4Color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		v[3].f2TexCoord0 = Float2(fUVScale, fUVScale);
		pSphereMesh->SetVertices(uVertex, v);

		UInt32 i[uIndex] = { 0,1,2,	1,3,2, };
		pSphereMesh->SetIndices(uIndex, (const UInt8*)i);
		pSphereMesh->SetSubMesh(0, 0, uIndex);
		return True;
	}

	Boolean InitDefaultMaterial(IAppMaterial* pMaterial)
	{
		pMaterial->SetDomain(IAppMaterial::Domain::EDM_WorldSpace);
		pMaterial->SetFaceType(IAppMaterial::FaceType::EFT_Frontface);
		pMaterial->SetBlendMode(IAppMaterial::BlendMode::EBM_Opaque);
		pMaterial->SetShadingModel(IAppMaterial::ShadingModel::ESM_Default);

		pMaterial->SetParameter(
			IAppMaterial::ParameterDescription("MapDiffuse",
				IAppMaterial::EPT_Texture2D, DefaultResource::MOSAIC_TEXTURE));
		pMaterial->SetParameter(
			IAppMaterial::ParameterDescription("MapNormal",
				IAppMaterial::EPT_Texture2D, DefaultResource::FLATNORMAL_TEXTURE));
		pMaterial->SetParameter(
			IAppMaterial::ParameterDescription("Roughness",
				IAppMaterial::EPT_Float1, Float4(0.8f, 0.8f, 0.8f, 0.8f), 0.01f));
		pMaterial->SetParameter(
			IAppMaterial::ParameterDescription("Metallic",
				IAppMaterial::EPT_Float1, Float4(0.3f, 0.3f, 0.3f, 0.3f), 0.01f));
		pMaterial->SetMaterialFunc(IAppMaterial::EMF_SurfaceParams,
			"\
			float3 color = Tex2D_Sample(MapDiffuse, input.TexCoord0).rgb;\
			OUTPUT_ALBEDO(PCFunc_Color_GammaCorrect(color, 2.2f));\
			\
			float3 n = Tex2D_Sample(MapNormal, input.TexCoord0).xyz;\n\
			n = n * 2 - 1;\n\
			if (dot(n, n) < 0.0001)\n\
				n = float3(0, 0, 1);\n\
			OUTPUT_TANGENT_NORMAL(n);\n\
			OUTPUT_ROUGHNESS(Roughness);\n\
			OUTPUT_METALLIC(Metallic);\n");

		return pMaterial->Recompile();
	}
	Boolean InitMosaicTexture(IAppTexture* pTex)
	{
		UInt32 uColor[2] = { 0xff404040, 0xff7f7f7f };
		const UInt32 uSize = pTex->GetWidth();
		const UInt32 uGridSize = uSize / 8;
		ASSERT_IF_FAILED(uGridSize >= 2);

		Array<UInt32> Data(uSize * uSize);
		UInt32 uColorIndex = 0;
		for (UInt32 u = 0; u < uSize; u++)
		{
			if (u % uGridSize == 0)
			{
				uColorIndex = (uColorIndex + 1) % 2;
			}
			for (UInt32 v = 0; v < uSize; v++)
			{
				if (v % uGridSize == 0)
				{
					uColorIndex = (uColorIndex + 1) % 2;
				}
				Data[u + v * uSize] = uColor[uColorIndex];
			}
		}

		pTex->SetPixelData(0, 0, (const UInt8*)Data.GetRawData());
		return True;
	}
	Boolean InitPureColorTexture(IAppTexture* pTex, UInt32 color)
	{
		const UInt32 uSize = pTex->GetWidth();

		Array<UInt32> Data(uSize * uSize);
		UInt32 uColorIndex = 0;
		for (UInt32 u = 0; u < uSize; u++)
		{
			for (UInt32 v = 0; v < uSize; v++)
			{
				Data[u + v * uSize] = color;
			}
		}

		pTex->SetPixelData(0, 0, (const UInt8*)Data.GetRawData());
		return True;
	}
	Boolean InitNoiseTexture(IAppTexture* pTex)
	{
		const UInt32 uSize = pTex->GetWidth();

		Array<UInt32> Data(uSize * uSize);
		for (UInt32 u = 0; u < uSize; u++)
		{
			for (UInt32 v = 0; v < uSize; v++)
			{
				Data[u + v * uSize] = 0xff000000
					| (UInt32)(Math::Random01() * 255) << 16
					| (UInt32)(Math::Random01() * 255) << 8
					| (UInt32)(Math::Random01() * 255);
			}
		}

		pTex->SetPixelData(0, 0, (const UInt8*)Data.GetRawData());
		return True;
	}

	Boolean		AppAssetManager::Init(InitParams& params)
	{
		ASSERT_IF_FAILED(params.szAssetsPath != nullptr && params.szAssetsPath[1] == ':');
		ASSERT_IF_FAILED(CheckFile(params.szAssetsPath));
		m_strAssetsPath = params.szAssetsPath;
		FT_Error error = FT_Init_FreeType(&m_FTLibrary);

		pDefaultMaterial = IAppMaterial::Create(DefaultResource::MATERIAL);

		pMosaicTexture = IAppTexture::Create(DefaultResource::MOSAIC_TEXTURE,
			IAppTexture::ETF_RGBA8, 32, 32, 1, 1, False, False, False);

		pNoiseTexture = IAppTexture::Create(DefaultResource::NOISE_TEXTURE,
			IAppTexture::ETF_RGBA8, 256, 256, 1, 1, False, False, False);

		pRedTexture = IAppTexture::Create(DefaultResource::RED_TEXTURE,
			IAppTexture::ETF_RGBA8, 4, 4, 1, 1, False, False, False);
		pGreenTexture = IAppTexture::Create(DefaultResource::GREEN_TEXTURE,
			IAppTexture::ETF_RGBA8, 4, 4, 1, 1, False, False, False);
		pBlueTexture = IAppTexture::Create(DefaultResource::BLUE_TEXTURE,
			IAppTexture::ETF_RGBA8, 4, 4, 1, 1, False, False, False);
		pBlackTexture = IAppTexture::Create(DefaultResource::BLACK_TEXTURE,
			IAppTexture::ETF_RGBA8, 4, 4, 1, 1, False, False, False);
		pWhiteTexture = IAppTexture::Create(DefaultResource::WHITE_TEXTURE,
			IAppTexture::ETF_RGBA8, 4, 4, 1, 1, False, False, False);
		pFlatNormalTexture = IAppTexture::Create(DefaultResource::FLATNORMAL_TEXTURE,
			IAppTexture::ETF_RGBA8, 4, 4, 1, 1, False, False, False);

		if (!InitMosaicTexture(pMosaicTexture) ||
			!InitNoiseTexture(pNoiseTexture) ||
			!InitPureColorTexture(pRedTexture, 0xff0000ff) ||
			!InitPureColorTexture(pGreenTexture, 0xff00ff00) ||
			!InitPureColorTexture(pBlueTexture, 0xffff0000) ||
			!InitPureColorTexture(pBlackTexture, 0xff000000) ||
			!InitPureColorTexture(pWhiteTexture, 0xffffffff) ||
			!InitPureColorTexture(pFlatNormalTexture, 0xffff7f7f) ||
			!InitDefaultMaterial(pDefaultMaterial) ||
			!InitCubeMesh() ||
			!InitPlaneMesh() ||
			!InitSphereMesh())
		{
			return False;
		}

		pDefaultMaterialInstance = IAppMaterialInstance::Create(
			DefaultResource::MATERIALINSTANCE, DefaultResource::MATERIAL);

		return True;
	}
	Void		AppAssetManager::Release()
	{
		MX_RELEASE_INTERFACE(pDefaultMaterialInstance);
		MX_RELEASE_INTERFACE(pDefaultMaterial);
		MX_RELEASE_INTERFACE(pMosaicTexture);
		MX_RELEASE_INTERFACE(pNoiseTexture);
		MX_RELEASE_INTERFACE(pRedTexture);
		MX_RELEASE_INTERFACE(pGreenTexture);
		MX_RELEASE_INTERFACE(pBlueTexture);
		MX_RELEASE_INTERFACE(pBlackTexture);
		MX_RELEASE_INTERFACE(pWhiteTexture);
		MX_RELEASE_INTERFACE(pFlatNormalTexture);
		MX_RELEASE_INTERFACE(pCubeMesh);
		MX_RELEASE_INTERFACE(pPlaneMesh);
		MX_RELEASE_INTERFACE(pSphereMesh);

		ASSERT_IF_FAILED(m_mapAssets.size() == 0);
		for (auto it = m_mapAssets.begin(); it != m_mapAssets.end(); it++)
		{
			String strMessage = String::Format("AppAsset %s Still Being Reference",
				it->second->GetName());
			Log::Output("AppAssetManager", strMessage.CString());
		}
		m_mapAssets.clear();
		FT_Done_FreeType(m_FTLibrary);
	}

	IAppAsset*	AppAssetManager::GetAsset(const Char* szAssetName)
	{
		String strAssetName;
		String strAssetPathName;
		ConvertAssetName(szAssetName, strAssetName, strAssetPathName);

		auto it = m_mapAssets.find(strAssetName.CString());
		if (it != m_mapAssets.end())
		{
			it->second->AddRef();
			return it->second;
		}

		IAppAsset* pAsset = nullptr;
		if (CheckFileSuffix(strAssetName.CString(), IAppFont::Suffix))
		{
			AppFont* pFontAsset = new AppFont(strAssetName.CString());
			if (!pFontAsset->Init(strAssetPathName.CString(), m_FTLibrary))
			{
				delete pFontAsset;
			}
			pAsset = pFontAsset;
		}
		else if (CheckFileSuffix(strAssetName.CString(), IAppTexture::Suffix))
		{
			TextureFileInfo info;
			if (DDSLoader(strAssetPathName.CString(), info))
			{
				AppTexture* pTexture = (AppTexture*)IAppTexture::Create(
					strAssetName.CString(),
					info.eFormat, info.uWidth, info.uHeight, info.uDepth, info.uMipLevel,
					info.bArrayTex, info.bCubeMap, False);

				for (UInt32 uMip = 0; uMip < info.uMipLevel; uMip++)
				{
					for (UInt32 uDepth = 0; uDepth < info.uDepth; uDepth++)
					{
						pTexture->SetPixelData(uMip, uDepth,
							info.arrMipPixelData[uMip * info.uMipLevel + uDepth]);

						delete[] info.arrMipPixelData[uMip * info.uMipLevel + uDepth];
					}
				}
				info.arrMipPixelData.Clear();
				pAsset = pTexture;
			}
		}
		else if (CheckFileSuffix(strAssetName.CString(), IAppAudio::Suffix))
		{
			AudioFileInfo info;
			if (FLACLoader(strAssetPathName.CString(), info))
			{
				AppAudio* pAudio = new AppAudio(strAssetName.CString());
				if (pAudio->Init(info.SampleRate, info.Channels, info.uPCMDataSize, info.pPCMData))
				{
					pAsset = pAudio;
				}
				delete[] info.pPCMData;
			}
		}
		else
		{
			IDeserializer* pDeserializer = TextDeserializer::Open(strAssetPathName.CString());
			if (pDeserializer == nullptr)
			{
				String strError = String::Format("Asset %s File Not Found", szAssetName);
				Log::Output("AppAssetManager", strError.CString());
				return nullptr;
			}
			if (CheckFileSuffix(strAssetName.CString(), IAppMesh::Suffix))
			{
				pAsset = new AppMesh(strAssetName.CString());
			}
			else if (CheckFileSuffix(strAssetName.CString(), IAppMeshInstance::Suffix))
			{
				pAsset = new AppMeshInstance(strAssetName.CString());
			}
			else if (CheckFileSuffix(strAssetName.CString(), IAppSkeleton::Suffix))
			{
				pAsset = new AppSkeleton(strAssetName.CString());
			}
			else if (CheckFileSuffix(strAssetName.CString(), IAppMaterial::Suffix))
			{
				pAsset = new AppMaterial(strAssetName.CString());
			}
			else if (CheckFileSuffix(strAssetName.CString(), IAppMaterialInstance::Suffix))
			{
				pAsset = new AppMaterialInstance(strAssetName.CString());
			}
			else if (CheckFileSuffix(strAssetName.CString(), IAppAnimationClip::Suffix))
			{
				pAsset = new AppAnimationClip(strAssetName.CString());
			}
			if (pAsset != nullptr)
			{
				const IDeserializerBlock *pBlock = pDeserializer->GetRootDeserializerBlock();
				if (!pAsset->OnDeserialize(*pBlock))
				{
					delete pAsset;
					pAsset = nullptr;
				}
			}
			pDeserializer->Close();
		}

		if (pAsset != nullptr)
		{
			m_mapAssets[strAssetName.CString()] = pAsset;
		}
		return pAsset;
	}

#if MX_BUILD_EDITOR
	Boolean AppAssetManager::SaveAsset(IAppAsset* pAsset, const Char* szAssetSavedName)
	{
		//根据Asset类型修改文件后缀
		ISerializer* Serializer = TextSerializer::Open(szAssetSavedName);

		Serializer->Save("{");
		Boolean bRes = pAsset->OnSerialize(*Serializer);
		Serializer->Save("}");
		Serializer->Close();

		return bRes;
	}
	Boolean	AppAssetManager::RenameAsset(const Char* szAssetName, const Char* szAssetNewName)
	{
		//TODO
		return False;
	}
#endif

}
