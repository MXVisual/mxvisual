#pragma once
#ifndef _MX_APP_SKELETON_
#define _MX_APP_SKELETON_
#include "MXAsset.h"
#include "MXString.h"
#include "MXArray.h"

namespace MXVisual
{

	class AppSkeleton : public IAppSkeleton
	{
	public:
		AppSkeleton(const Char* szName);
		~AppSkeleton();

		const Char* GetName() const { return m_strName.CString(); }

		UInt32	GetBoneCount() const override;
		Boolean	AddBone(UInt32 uParentBoneIndex, const Char* szBoneName, const Matrix4& matInit) override;
		Boolean	AddBone(const Char* szParentBoneName, const Char* szBoneName, const Matrix4& matInit)override;

		const Char* GetBoneName(UInt32 uBoneIndex) const override;
		UInt32 GetBoneIndex(const Char* szBoneName) const override;

		Void	CalculateBoneToModels(const Array<Matrix4>& arrBoneLocal, Array<Matrix4>& arrResultBoneAbs);

	protected:
		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override;
#if MX_BUILD_EDITOR
		Boolean	OnSerialize(ISerializer& Serializer) override;
#endif

	protected:
		String m_strName;
		UInt32 m_uUniqueID;
		struct Bone
		{
			String strName;
			Matrix4 matInitBoneLocal;

			Matrix4 matBoneLocal;//Bone Local

			Boolean bUpdateToDate;
			Matrix4 matBoneToModel;//Bone Local To Model Local

			Bone* pParent;
			Array<Bone*> arrChild;

			Bone();
			Void Update();
		};
		Array<Bone*>	m_arrBone;
	};
}
#endif