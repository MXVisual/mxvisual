#pragma once
#ifndef _MX_APP_MESH_
#define _MX_APP_MESH_
#include "MXAsset.h"
#include "MXString.h"
#include "MXArray.h"
#include "MXGraphicsResource.h"

namespace MXVisual
{
	class AppMesh : public IAppMesh
	{
	public:
		AppMesh(const Char* szName);
		~AppMesh();
		const Char* GetName() const override { return m_strName.CString(); }

		Boolean Init(MeshType eType, Boolean b32BitIndex, Boolean bDynamic,
			UInt32 uVertexCapacity, UInt32 uIndexCapacity, UInt32 uSubMeshCount);

		Void SetVertices(UInt32 uVertexCount, const IVertex* pVertices) override;
		Void SetIndices(UInt32 uIndexCount, const UInt8* pIndices) override;
		Void SetSubMesh(UInt32 uSubMesh, UInt32 uOffset, UInt32 uSize) override;

		UInt32 GetVertexCapacity() const override { return m_pPrimitive->GetVertexCapacity(); }
		UInt32 GetIndexCapacity() const override { return m_pPrimitive->GetIndexCapacity(); }
		UInt32 GetSubMeshCount() const override { return m_uSubMeshCount; }

		UInt32 GetVertexOffset(UInt32 uSubMesh) const 
		{
			ASSERT_IF_FAILED(uSubMesh < m_uSubMeshCount);
			return m_arrSubMeshOffset[uSubMesh]; 
		}
		UInt32 GetVertexCount(UInt32 uSubMesh) const 
		{
			ASSERT_IF_FAILED(uSubMesh < m_uSubMeshCount);
			return m_arrSubMeshSize[uSubMesh];
		}

		Boolean AddBindingBone(const Char* szBoneName, const Matrix4& matLocalToBone);
		UInt32	GetBindingBoneCount() const;
		const Char*	GetBindingBoneName(UInt32 uBoneIndex) const;
		Matrix4	GetBindingBoneModelToBone(UInt32 uBoneIndex) const;

		IGPrimitive* GetGraphicsResource() { return m_pPrimitive; }
	protected:
		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override;
#if MX_BUILD_EDITOR
		Boolean	OnSerialize(ISerializer& Serializer) override;
#endif

	protected:
		String m_strName;
		MeshType m_eMeshType;

		IGPrimitive* m_pPrimitive;

		UInt32 m_uVertexCount;
		UInt32 m_uIndexCount;
		UInt32 m_uSubMeshCount;

		IVertex* m_pVertexBuffer;
		UInt8* m_pIndexBuffer;

		Array<UInt32> m_arrSubMeshOffset;
		Array<UInt32> m_arrSubMeshSize;

		Array<String> m_arrBindingBoneName;
		Array<Matrix4> m_arrModelToBindingBone;
	};
}
#endif