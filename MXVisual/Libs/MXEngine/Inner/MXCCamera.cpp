#include "MXCCamera.h"
#include "MXMath.h"
#include "MXCSerializer.h"
#include "MXGraphicsResource.h"

#if MX_BUILD_EDITOR
#include "MXCEditorGraphics.h"
#include "MXCEditorPanel.h"
#endif

namespace MXVisual
{
	CCamera::CCamera(const Char* szName)
		:CObject(szName)
	{
		SetPerspective(Constant_PI * 0.5f, 16.0f / 9.0f, 0.2f, 500.0f);
	}
	CCamera::~CCamera()
	{
		MX_RELEASE_INTERFACE(m_pViewFrustum);
	}

	Void	CCamera::OnUpdate()
	{
		if (m_pViewFrustum != nullptr)
		{
			m_pViewFrustum->SetPosition(CObject::GetPosition(False));
			m_pViewFrustum->SetForwardAndUp(GetDirForward(),
				GetDirUp());
		}
	}

	Void	CCamera::SetRenderWindow(IGRenderWindow* pRW)
	{
		MX_RELEASE_INTERFACE(m_pViewFrustum);

		if (pRW == nullptr)return;

		String name = String::Format("%s_%d", CObject::GetName(), CObject::GetUniqueID());
		IGViewFrustum::Description desc;
		desc.pTargetWindow = pRW;
		m_pViewFrustum = GraphicsResource::CreateViewFrustum(desc);

		if (m_pViewFrustum != nullptr)
		{
			if (m_CameraProperty.bPerspective)
			{
				m_CameraProperty.fAspectWH = pRW->GetWidth() / (Float1)pRW->GetHeight();
				m_pViewFrustum->SetPerspective(
					m_CameraProperty.fFovW,
					m_CameraProperty.fAspectWH,
					m_CameraProperty.fZNear,
					m_CameraProperty.fZFar
				);
			}
			else
			{
				m_pViewFrustum->SetOrthographic(
					m_CameraProperty.fWidth,
					m_CameraProperty.fHeight,
					m_CameraProperty.fZNear,
					m_CameraProperty.fZFar
				);
			}
		}
	}

	Void	CCamera::SetPerspective(Float1 fFovW, Float1 fAspectWH, Float1 fZNear, Float1 fZFar)
	{
		m_CameraProperty.bPerspective = True;
		m_CameraProperty.fFovW = fFovW;
		m_CameraProperty.fAspectWH = fAspectWH;
		m_CameraProperty.fZNear = fZNear;
		m_CameraProperty.fZFar = fZFar;

		if (m_pViewFrustum != nullptr)
		{
			m_pViewFrustum->SetPerspective(
				m_CameraProperty.fFovW,
				m_CameraProperty.fAspectWH,
				m_CameraProperty.fZNear,
				m_CameraProperty.fZFar
			);
		}
	}
	Void	CCamera::SetOrthographic(Float1 fWidth, Float1 fHeight, Float1 fZNear, Float1 fZFar)
	{
		m_CameraProperty.bPerspective = False;
		m_CameraProperty.fWidth = fWidth;
		m_CameraProperty.fHeight = fHeight;
		m_CameraProperty.fZNear = fZNear;
		m_CameraProperty.fZFar = fZFar;

		if (m_pViewFrustum != nullptr)
		{
			m_pViewFrustum->SetOrthographic(
				m_CameraProperty.fWidth,
				m_CameraProperty.fHeight,
				m_CameraProperty.fZNear,
				m_CameraProperty.fZFar
			);
		}
	}

	Float3	CCamera::ScreenPositionToWorldPosition(const Float2& ScreenPos, Float1 Distance)
	{
		if (m_pViewFrustum == nullptr)return Float3();

		Float2 ViewportSize = m_pViewFrustum->GetViewportSize();
		Matrix4 matView = m_pViewFrustum->GetWorldToView();
		Matrix4 matProj = m_pViewFrustum->GetViewToClip();

		Float3 viewPos((ScreenPos[0] / ViewportSize[0] * 2 - 1) * Distance / matProj.Get(0, 0),
			(-ScreenPos[1] / ViewportSize[1] * 2 + 1) * Distance / matProj.Get(1, 1),
			Distance);

		Float3 Right(matView.Get(0, 0), matView.Get(1, 0), matView.Get(2, 0));
		Float3 Up(matView.Get(0, 1), matView.Get(1, 1), matView.Get(2, 1));
		Float3 Forward(matView.Get(0, 2), matView.Get(1, 2), matView.Get(2, 2));

		Float3 pos = Math::MatrixGetTranslation(matView);

		Float3 WorldPos(
			pos[0] + Right[0] * viewPos[0] + Up[0] * viewPos[1] + Forward[0] * Distance,
			pos[1] + Right[1] * viewPos[0] + Up[1] * viewPos[1] + Forward[1] * Distance,
			pos[2] + Right[2] * viewPos[0] + Up[2] * viewPos[1] + Forward[2] * Distance
		);
		return WorldPos;
	}

	Boolean	CCamera::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		String strName;
		DeserializerBlock.Load("IsPerspective", m_CameraProperty.bPerspective);
		DeserializerBlock.Load("NearPlane", m_CameraProperty.fZNear);
		DeserializerBlock.Load("FarPlane", m_CameraProperty.fZFar);

		if (m_CameraProperty.bPerspective)
		{
			Float1 fov;
			DeserializerBlock.Load("HorizonFOV", fov);
			DeserializerBlock.Load("WidthHeightAspect", m_CameraProperty.fAspectWH);
			SetPerspective(Math::Degree2Radian(fov),
				m_CameraProperty.fAspectWH,
				m_CameraProperty.fZNear, m_CameraProperty.fZFar);
		}
		else
		{
			Float2 wh;
			DeserializerBlock.Load("WidthHeight", wh);
			SetOrthographic(wh[0], wh[1], m_CameraProperty.fZNear, m_CameraProperty.fZFar);
		}

		return ICObject::OnDeserialize(DeserializerBlock);
	}

#if MX_BUILD_EDITOR
	Void	CCamera::OnEditorRender(IEditorGraphics& Graphics)
	{
		if (m_CameraProperty.bPerspective)
		{
			Float3 f3Origin = GetPosition(False);

			Float3 f3NearCenter = f3Origin + GetDirForward() * m_CameraProperty.fZNear;
			Float3 f3FarCenter = f3Origin + GetDirForward() * m_CameraProperty.fZFar;
			Float3 f3NearCorner[4];
			Float3 f3FarCorner[4];

			Float1 fNearHalfWidth = tan(m_CameraProperty.fFovW * 0.5f) * m_CameraProperty.fZNear;
			Float1 fNearHalfHeight = tan(m_CameraProperty.fFovW * 0.5f) * m_CameraProperty.fZNear / m_CameraProperty.fAspectWH;

			Float1 fFarHalfWidth = tan(m_CameraProperty.fFovW * 0.5f) * m_CameraProperty.fZFar;
			Float1 fFarHalfHeight = tan(m_CameraProperty.fFovW * 0.5f) * m_CameraProperty.fZFar / m_CameraProperty.fAspectWH;

			f3NearCorner[0] = f3NearCenter - GetDirRight() * fNearHalfWidth + GetDirUp() * fNearHalfHeight;
			f3NearCorner[1] = f3NearCenter + GetDirRight() * fNearHalfWidth + GetDirUp() * fNearHalfHeight;
			f3NearCorner[2] = f3NearCenter + GetDirRight() * fNearHalfWidth - GetDirUp() * fNearHalfHeight;
			f3NearCorner[3] = f3NearCenter - GetDirRight() * fNearHalfWidth - GetDirUp() * fNearHalfHeight;

			f3FarCorner[0] = f3FarCenter - GetDirRight() * fFarHalfWidth + GetDirUp() * fFarHalfHeight;
			f3FarCorner[1] = f3FarCenter + GetDirRight() * fFarHalfWidth + GetDirUp() * fFarHalfHeight;
			f3FarCorner[2] = f3FarCenter + GetDirRight() * fFarHalfWidth - GetDirUp() * fFarHalfHeight;
			f3FarCorner[3] = f3FarCenter - GetDirRight() * fFarHalfWidth - GetDirUp() * fFarHalfHeight;

			for (UInt32 u = 0; u < 4; u++)
			{
				Graphics.ChangeColor(Float4(0, 1, 0, 1));
				Graphics.RenderLine(f3NearCorner[u], f3NearCorner[(u + 1) % 4], 1.0f);
				Graphics.RenderLine(f3Origin, f3FarCorner[u], 1.0f);
				Graphics.RenderLine(f3FarCorner[u], f3FarCorner[(u + 1) % 4], 1.0f);
			}
		}
		else
		{
			Float3 f3Origin = GetPosition(False);

			Float3 f3NearCenter = f3Origin + GetDirForward() * m_CameraProperty.fZNear;
			Float3 f3FarCenter = f3Origin + GetDirForward() * m_CameraProperty.fZFar;
			Float3 f3NearCorner[4];
			Float3 f3FarCorner[4];

			Float1 fHalfWidth = m_CameraProperty.fWidth * 0.5f;
			Float1 fHalfHeight = m_CameraProperty.fHeight * 0.5f;

			f3NearCorner[0] = f3NearCenter - GetDirRight() * fHalfWidth + GetDirUp() * fHalfHeight;
			f3NearCorner[1] = f3NearCenter + GetDirRight() * fHalfWidth + GetDirUp() * fHalfHeight;
			f3NearCorner[2] = f3NearCenter + GetDirRight() * fHalfWidth - GetDirUp() * fHalfHeight;
			f3NearCorner[3] = f3NearCenter - GetDirRight() * fHalfWidth - GetDirUp() * fHalfHeight;

			f3FarCorner[0] = f3FarCenter - GetDirRight() * fHalfWidth + GetDirUp() * fHalfHeight;
			f3FarCorner[1] = f3FarCenter + GetDirRight() * fHalfWidth + GetDirUp() * fHalfHeight;
			f3FarCorner[2] = f3FarCenter + GetDirRight() * fHalfWidth - GetDirUp() * fHalfHeight;
			f3FarCorner[3] = f3FarCenter - GetDirRight() * fHalfWidth - GetDirUp() * fHalfHeight;

			for (UInt32 u = 0; u < 4; u++)
			{
				Graphics.ChangeColor(Float4(0, 1, 0, 1));
				Graphics.RenderLine(f3NearCorner[u], f3NearCorner[(u + 1) % 4], 1.0f);
				Graphics.RenderLine(f3NearCorner[u], f3FarCorner[u], 1.0f);
				Graphics.RenderLine(f3FarCorner[u], f3FarCorner[(u + 1) % 4], 1.0f);
			}
		}
	}
	Void	CCamera::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyString("Name", GetName());
		Panel.AddPropertyFloat3("Position", GetPosition(MXVisual::True));
		MXVisual::Float3 f3 = MXVisual::Math::QuaternionToEular(GetRotation(MXVisual::True));
		f3 = MXVisual::Float3(MXVisual::Math::Radian2Degree(f3[0]), MXVisual::Math::Radian2Degree(f3[1]), MXVisual::Math::Radian2Degree(f3[2]));
		Panel.AddPropertyFloat3("Rotation", f3);

		Panel.AddPropertyTrigger("IsPerspective", m_CameraProperty.bPerspective);
		Panel.AddPropertyFloat1("NearPlane", m_CameraProperty.fZNear, 0.01f, 0.01f, 5000.0f);
		Panel.AddPropertyFloat1("FarPlane", m_CameraProperty.fZFar, 1.0f, 0.01f, 5000.0f);

		if (m_CameraProperty.bPerspective)
		{
			Panel.AddPropertyFloat1("HorizonFOV", Math::Radian2Degree(m_CameraProperty.fFovW), 0.01f, 1.0f, 180.0f);//Angle
			Panel.AddPropertyFloat1("WidthHeightAspect", m_CameraProperty.fAspectWH, 1.0f, 0.01f, 10.0f);
		}
		else
		{
			Panel.AddPropertyFloat2("WidthHeight",
				Float2(m_CameraProperty.fWidth, m_CameraProperty.fHeight),
				0.01f, 0.01f, 5000.0f);
		}
	}
	Void	CCamera::OnPanelInactived()
	{

	}
	Void	CCamera::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EString:
			SetName(((EditorPropertyString*)pProperty)->Value);
			break;
		case IEditorProperty::EFloat3:
			if (strcmp(pProperty->Name, "Position") == 0)
			{
				SetPosition(((EditorPropertyFloat3*)pProperty)->Value, MXVisual::True);
			}
			else if (strcmp(pProperty->Name, "Rotation") == 0)
			{
				MXVisual::Float3 r(Math::Degree2Radian(((EditorPropertyFloat3*)pProperty)->Value[0]),
					Math::Degree2Radian(((EditorPropertyFloat3*)pProperty)->Value[1]),
					Math::Degree2Radian(((EditorPropertyFloat3*)pProperty)->Value[2]));
				SetRotation(r, MXVisual::True);
			}
		case IEditorProperty::ETrigger:
			m_CameraProperty.bPerspective = ((EditorPropertyTrigger*)pProperty)->Value;
			if (m_CameraProperty.bPerspective)
				SetPerspective(Constant_PI * 0.5f, 16.0f / 9.0f, m_CameraProperty.fZNear, m_CameraProperty.fZFar);
			else
				SetOrthographic(160.f, 90.0f, m_CameraProperty.fZNear, m_CameraProperty.fZFar);
			break;
		case IEditorProperty::EFloat2:
		{
			Float2 Value = ((EditorPropertyFloat2*)pProperty)->Value;
			m_CameraProperty.fWidth = Value[0];
			m_CameraProperty.fHeight = Value[1];

			if (!m_CameraProperty.bPerspective)
			{
				SetOrthographic(m_CameraProperty.fWidth,
					m_CameraProperty.fHeight,
					m_CameraProperty.fZNear,
					m_CameraProperty.fZFar);
			}
			break;
		}
		case IEditorProperty::EFloat1:
		{
			Float1 Value = ((EditorPropertyFloat1*)pProperty)->Value;
			if (strcmp(pProperty->Name, "NearPlane") == 0)
			{
				m_CameraProperty.fZNear = Value;
			}
			else if (strcmp(pProperty->Name, "FarPlane") == 0)
			{
				m_CameraProperty.fZFar = Value;
			}
			else if (strcmp(pProperty->Name, "HorizonFOV") == 0)
			{
				m_CameraProperty.fFovW = Math::Degree2Radian(Value);
			}
			else if (strcmp(pProperty->Name, "WidthHeightAspect") == 0)
			{
				m_CameraProperty.fAspectWH = Value;
			}

			if (m_CameraProperty.bPerspective)
			{
				SetPerspective(m_CameraProperty.fFovW,
					m_CameraProperty.fAspectWH,
					m_CameraProperty.fZNear,
					m_CameraProperty.fZFar);
			}
			else
			{
				SetOrthographic(m_CameraProperty.fWidth,
					m_CameraProperty.fHeight,
					m_CameraProperty.fZNear,
					m_CameraProperty.fZFar);
			}
			break;
		}
		}
	}
	Boolean	CCamera::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("IsPerspective", m_CameraProperty.bPerspective);
		Serializer.Save("NearPlane", m_CameraProperty.fZNear);
		Serializer.Save("FarPlane", m_CameraProperty.fZFar);

		if (m_CameraProperty.bPerspective)
		{
			Serializer.Save("HorizonFOV", Math::Radian2Degree(m_CameraProperty.fFovW));
			Serializer.Save("WidthHeightAspect", m_CameraProperty.fAspectWH);
		}
		else
		{
			Serializer.Save("WidthHeight",
				Float2(m_CameraProperty.fWidth, m_CameraProperty.fHeight));
		}

		return ICObject::OnSerialize(Serializer);
	}
#endif
}