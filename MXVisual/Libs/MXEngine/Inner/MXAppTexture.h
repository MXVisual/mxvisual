#pragma once
#ifndef _MX_APP_TEXTURE_
#define _MX_APP_TEXTURE_
#include "MXAsset.h"
#include "MXGraphicsResource.h"

namespace MXVisual
{
	class AppTexture : public IAppTexture
	{
	public:
		AppTexture(const Char* szName);
		~AppTexture();
		const Char* GetName() const override { return m_strName.CString(); }

		Boolean Init(const Char* szAssetName, Format eFormat,
			UInt32 uWidth, UInt32 uHeight, UInt32 uDepth, UInt32 uMipLevel,
			Boolean bArrayTex, Boolean bCubeTex, Boolean bTargetable);

		UInt32 GetWidth() const override { return m_pTexture->GetWidth(); }
		UInt32 GetHeight() const override { return m_pTexture->GetHeight(); }
		UInt32 GetDepth() const override { return m_pTexture->GetDepth(); }
		UInt32 GetMipLevel() const override { return m_pTexture->GetMipmapCount(); }
		Format GetFormat() const override;
		Void   GetPixelData(UInt32 uMipmap, UInt32 uDepth, Array<UInt8>& arrResult) const override;

		Void   SetPixelData(UInt32 uMipmap, UInt32 uDepth, const UInt8* pPixelDatas) override;

		IGTexture* GetGraphicsResource() const { return m_pTexture; }
	protected:
		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override;
#if MX_BUILD_EDITOR
		Boolean	OnSerialize(ISerializer& Serializer) override;
#endif

	protected:
		String m_strName;
		IGTexture* m_pTexture;

		Array<UInt8*> m_arrUncompressMipPixelDatas;
	};
}
#endif