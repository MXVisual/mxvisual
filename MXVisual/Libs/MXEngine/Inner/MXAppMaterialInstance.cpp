#include "MXAppMaterialInstance.h"
#include "MXCSerializer.h"
#include "MXGraphicsResource.h"

#include "MXAppMaterial.h"
#include "MXAppTexture.h"

namespace MXVisual
{
	AppMaterialInstance::AppMaterialInstance(const Char* szName)
		: m_strName(szName)
		, m_pParameterBlock(nullptr)
	{
	}
	AppMaterialInstance::~AppMaterialInstance()
	{
		MX_RELEASE_INTERFACE(m_pRefMaterial);
		MX_RELEASE_INTERFACE(m_pParameterBlock);
		for (auto it = m_mapTexture.begin(); it != m_mapTexture.end(); it++)
		{
			MX_RELEASE_INTERFACE(it->second);
		}
		m_mapTexture.clear();
	}

	Boolean AppMaterialInstance::Init(const Char* szMaterial)
	{
		AppMaterial* pMtl = (AppMaterial*)AppAssetManager::GetAsset(szMaterial);
		if (pMtl == nullptr)return False;

		m_pRefMaterial = pMtl;
		if (pMtl->GetGraphicsResource() == nullptr)return False;

		return UpdateParameterBuffer();
	}
	IGMaterialParameterBuffer* AppMaterialInstance::GetGraphicsResource()
	{
		UpdateParameterBuffer();

		return m_pParameterBlock;
	}

	Boolean AppMaterialInstance::UpdateParameterBuffer()
	{
		if (m_pRealMaterial != m_pRefMaterial->GetGraphicsResource())
		{
			IGMaterialParameterBuffer* pOldParameterBlock = m_pParameterBlock;
			m_pRealMaterial = m_pRefMaterial->GetGraphicsResource();

			m_pParameterBlock = GraphicsResource::CreateMaterialParameterBuffer(m_pRefMaterial->GetGraphicsResource());
			if (m_pParameterBlock == nullptr)return False;

			for (UInt32 u = 0; u < m_pRefMaterial->GetParameterCount(); u++)
			{
				IAppMaterial::ParameterDescription desc = m_pRefMaterial->GetParameterDescription(u);
				IGMaterialParam* pOldParameter = nullptr;
				if (pOldParameterBlock != nullptr)
				{
					pOldParameter = pOldParameterBlock->GetParamByName(desc.strName.CString());

					IGMaterialParam::MaterialParamType eType;
					switch (desc.eType)
					{
					case IAppMaterial::EPT_Float1: eType = IGMaterialParam::EMPT_Float1; break;
					case IAppMaterial::EPT_Float2: eType = IGMaterialParam::EMPT_Float2; break;
					case IAppMaterial::EPT_Float3: eType = IGMaterialParam::EMPT_Float3; break;
					case IAppMaterial::EPT_Float4: eType = IGMaterialParam::EMPT_Float4; break;
					case IAppMaterial::EPT_Texture1D: eType = IGMaterialParam::EMPT_Texture1D; break;
					case IAppMaterial::EPT_Texture2D: eType = IGMaterialParam::EMPT_Texture2D; break;
					case IAppMaterial::EPT_Texture3D: eType = IGMaterialParam::EMPT_Texture3D; break;
					case IAppMaterial::EPT_TextureCube: eType = IGMaterialParam::EMPT_TextureCube; break;
					}
					if (pOldParameter->GetType() != eType)
						pOldParameter = nullptr;
				}

				switch (desc.eType)
				{
				case IAppMaterial::EPT_Float1:
					SetFloat1(desc.strName.CString(),
						pOldParameter != nullptr ? pOldParameter->GetFloat1() : desc.f1Default);
					break;
				case IAppMaterial::EPT_Float2:
					SetFloat2(desc.strName.CString(),
						pOldParameter != nullptr ? pOldParameter->GetFloat2() : desc.f2Default);
					break;
				case IAppMaterial::EPT_Float3:
					SetFloat3(desc.strName.CString(),
						pOldParameter != nullptr ? pOldParameter->GetFloat3() : desc.f3Default);
					break;
				case IAppMaterial::EPT_Float4:
					SetFloat4(desc.strName.CString(),
						pOldParameter != nullptr ? pOldParameter->GetFloat4() : desc.f4Default);
					break;
				case IAppMaterial::EPT_Texture1D:
					if (pOldParameter != nullptr)
						m_pParameterBlock->GetParamByName(desc.strName.CString())->SetTexture1D(pOldParameter->GetTexture1D());
					else
						SetTexture1D(desc.strName.CString(), desc.strDefault.CString());
					break;
				case IAppMaterial::EPT_Texture2D:
					if (pOldParameter != nullptr)
						m_pParameterBlock->GetParamByName(desc.strName.CString())->SetTexture2D(pOldParameter->GetTexture2D());
					else
						SetTexture2D(desc.strName.CString(), desc.strDefault.CString());
					break;
				case IAppMaterial::EPT_Texture3D:
					if (pOldParameter != nullptr)
						m_pParameterBlock->GetParamByName(desc.strName.CString())->SetTexture3D(pOldParameter->GetTexture3D());
					else
						SetTexture3D(desc.strName.CString(), desc.strDefault.CString());
					break;
				case IAppMaterial::EPT_TextureCube:
					if (pOldParameter != nullptr)
						m_pParameterBlock->GetParamByName(desc.strName.CString())->SetTextureCube(pOldParameter->GetTextureCube());
					else
						SetTextureCube(desc.strName.CString(), desc.strDefault.CString());
					break;
				}
			}

			MX_RELEASE_INTERFACE(pOldParameterBlock);
		}
		return True;
	}

	const Char* AppMaterialInstance::GetRefMaterialName() const
	{
		return GetRefMaterial()->GetName();
	}

	Float1 AppMaterialInstance::GetFloat1(const Char* szName) const
	{
		IGMaterialParam* pParam = m_pParameterBlock->GetParamByName(szName);
		if (pParam->GetType() == IGMaterialParam::EMPT_Float1)
			return pParam->GetFloat1();
		return 0.0f;
	}
	Float2 AppMaterialInstance::GetFloat2(const Char* szName) const
	{
		IGMaterialParam* pParam = m_pParameterBlock->GetParamByName(szName);
		if (pParam->GetType() == IGMaterialParam::EMPT_Float2)
			return pParam->GetFloat2();
		return Float2();
	}
	Float3 AppMaterialInstance::GetFloat3(const Char* szName) const
	{
		IGMaterialParam* pParam = m_pParameterBlock->GetParamByName(szName);
		if (pParam->GetType() == IGMaterialParam::EMPT_Float3)
			return pParam->GetFloat3();
		return Float3();
	}
	Float4 AppMaterialInstance::GetFloat4(const Char* szName) const
	{
		IGMaterialParam* pParam = m_pParameterBlock->GetParamByName(szName);
		if (pParam->GetType() == IGMaterialParam::EMPT_Float4)
			return pParam->GetFloat4();
		return Float4();
	}

	const Char* AppMaterialInstance::GetTexture1D(const Char* szName) const
	{
		auto it = m_mapTexture.find(szName);
		return it != m_mapTexture.end() ? it->second->GetName() : "";
	}
	const Char* AppMaterialInstance::GetTexture2D(const Char* szName) const
	{
		auto it = m_mapTexture.find(szName);
		return it != m_mapTexture.end() ? it->second->GetName() : "";
	}
	const Char* AppMaterialInstance::GetTexture3D(const Char* szName) const
	{
		auto it = m_mapTexture.find(szName);
		return it != m_mapTexture.end() ? it->second->GetName() : "";
	}
	const Char* AppMaterialInstance::GetTextureCube(const Char* szName) const
	{
		auto it = m_mapTexture.find(szName);
		return it != m_mapTexture.end() ? it->second->GetName() : "";
	}

	Void AppMaterialInstance::SetFloat1(const Char* szName, Float1 f1Value)
	{
		m_pParameterBlock->GetParamByName(szName)->SetFloat1(f1Value);
	}
	Void AppMaterialInstance::SetFloat2(const Char* szName, const Float2& f2Value)
	{
		m_pParameterBlock->GetParamByName(szName)->SetFloat2(f2Value);
	}
	Void AppMaterialInstance::SetFloat3(const Char* szName, const Float3& f3Value)
	{
		m_pParameterBlock->GetParamByName(szName)->SetFloat3(f3Value);
	}
	Void AppMaterialInstance::SetFloat4(const Char* szName, const Float4& f4Value)
	{
		m_pParameterBlock->GetParamByName(szName)->SetFloat4(f4Value);
	}

	Void AppMaterialInstance::SetTexture1D(const Char* szName, const Char* szTextureName)
	{
		AppTexture* pTex = (AppTexture*)AppAssetManager::GetAsset(szTextureName);
		if (pTex == nullptr)
			pTex = (AppTexture*)AppAssetManager::GetAsset(DefaultResource::MOSAIC_TEXTURE);
		if (m_pParameterBlock->GetParamByName(szName)->SetTexture1D(pTex->GetGraphicsResource()))
		{
			MX_RELEASE_INTERFACE(m_mapTexture[szName]);
			m_mapTexture[szName] = pTex;
		}
	}
	Void AppMaterialInstance::SetTexture2D(const Char* szName, const Char* szTextureName)
	{
		AppTexture* pTex = (AppTexture*)AppAssetManager::GetAsset(szTextureName);
		if (pTex == nullptr)
			pTex = (AppTexture*)AppAssetManager::GetAsset(DefaultResource::MOSAIC_TEXTURE);
		if (m_pParameterBlock->GetParamByName(szName)->SetTexture2D(pTex->GetGraphicsResource()))
		{
			MX_RELEASE_INTERFACE(m_mapTexture[szName]);
			m_mapTexture[szName] = pTex;
		}
	}
	Void AppMaterialInstance::SetTexture3D(const Char* szName, const Char* szTextureName)
	{
		AppTexture* pTex = (AppTexture*)AppAssetManager::GetAsset(szTextureName);
		if (pTex == nullptr)
			pTex = (AppTexture*)AppAssetManager::GetAsset(DefaultResource::MOSAIC_TEXTURE);
		if (m_pParameterBlock->GetParamByName(szName)->SetTexture3D(pTex->GetGraphicsResource()))
		{
			MX_RELEASE_INTERFACE(m_mapTexture[szName]);
			m_mapTexture[szName] = pTex;
		}
	}
	Void AppMaterialInstance::SetTextureCube(const Char* szName, const Char* szTextureName)
	{
		AppTexture* pTex = (AppTexture*)AppAssetManager::GetAsset(szTextureName);
		if (pTex == nullptr)
			pTex = (AppTexture*)AppAssetManager::GetAsset(DefaultResource::MOSAIC_TEXTURE);
		if (m_pParameterBlock->GetParamByName(szName)->SetTextureCube(pTex->GetGraphicsResource()))
		{
			MX_RELEASE_INTERFACE(m_mapTexture[szName]);
			m_mapTexture[szName] = pTex;
		}
	}

	Boolean	AppMaterialInstance::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		if (strcmp(DeserializerBlock.GetBlockName(), "AppMaterialInstance") != 0)
			return False;

		String strMaterial;
		DeserializerBlock.Load("Material", strMaterial);
		if (!Init(strMaterial.CString()))
			return False;

		m_pRefMaterial->GetGraphicsResource();
		MXVisual::UInt32 uIndex = 0;

		IGMaterialParam* pParam = m_pParameterBlock->GetParamByIndex(uIndex++);
		while (pParam->GetType() != MXVisual::IGMaterialParam::EMPT_Invalid)
		{
			switch (pParam->GetType())
			{
			case MXVisual::IGMaterialParam::EMPT_Float1:
			{
				Float1 value;
				DeserializerBlock.Load(pParam->GetName(), value);
				pParam->SetFloat1(value);
				break;
			}
			case MXVisual::IGMaterialParam::EMPT_Float2:
			{
				Float2 value;
				DeserializerBlock.Load(pParam->GetName(), value);
				pParam->SetFloat2(value);
				break;
			}
			case MXVisual::IGMaterialParam::EMPT_Float3:
			{
				Float3 value;
				DeserializerBlock.Load(pParam->GetName(), value);
				pParam->SetFloat3(value);
				break;
			}
			case MXVisual::IGMaterialParam::EMPT_Float4:
			{
				Float4 value;
				DeserializerBlock.Load(pParam->GetName(), value);
				pParam->SetFloat4(value);
				break;
			}
			case MXVisual::IGMaterialParam::EMPT_Texture1D:
			{
				String value;
				DeserializerBlock.Load(pParam->GetName(), value);
				SetTexture1D(pParam->GetName(), value.CString());
				break;
			}
			case MXVisual::IGMaterialParam::EMPT_Texture2D:
			{
				String value;
				DeserializerBlock.Load(pParam->GetName(), value);
				SetTexture2D(pParam->GetName(), value.CString());
				break;
			}
			case MXVisual::IGMaterialParam::EMPT_Texture3D:
			{
				String value;
				DeserializerBlock.Load(pParam->GetName(), value);
				SetTexture3D(pParam->GetName(), value.CString());
				break;
			}
			case MXVisual::IGMaterialParam::EMPT_TextureCube:
			{
				String value;
				DeserializerBlock.Load(pParam->GetName(), value);
				SetTextureCube(pParam->GetName(), value.CString());
				break;
			}
			}
			pParam = m_pParameterBlock->GetParamByIndex(uIndex++);
		}

		return True;
	}

#if MX_BUILD_EDITOR
	Boolean	AppMaterialInstance::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("AppMaterialInstance");
		Serializer.Save("Material", GetRefMaterialName());

		MXVisual::UInt32 uIndex = 0;
		IGMaterialParam* pParam = m_pParameterBlock->GetParamByIndex(uIndex++);
		while (pParam->GetType() != MXVisual::IGMaterialParam::EMPT_Invalid)
		{
			switch (pParam->GetType())
			{
			case MXVisual::IGMaterialParam::EMPT_Float1:
				Serializer.Save(pParam->GetName(), pParam->GetFloat1());
				break;
			case MXVisual::IGMaterialParam::EMPT_Float2:
				Serializer.Save(pParam->GetName(), pParam->GetFloat2());
				break;
			case MXVisual::IGMaterialParam::EMPT_Float3:
				Serializer.Save(pParam->GetName(), pParam->GetFloat3());
				break;
			case MXVisual::IGMaterialParam::EMPT_Float4:
				Serializer.Save(pParam->GetName(), pParam->GetFloat4());
				break;
			case MXVisual::IGMaterialParam::EMPT_Texture1D:
			case MXVisual::IGMaterialParam::EMPT_Texture2D:
			case MXVisual::IGMaterialParam::EMPT_Texture3D:
			case MXVisual::IGMaterialParam::EMPT_TextureCube:
				if (m_mapTexture.find(pParam->GetName()) != m_mapTexture.end())
				{
					Serializer.Save(pParam->GetName(), m_mapTexture[pParam->GetName()]->GetName());
				}
				break;
			}
			pParam = m_pParameterBlock->GetParamByIndex(uIndex++);
		}

		return True;
	}
#endif

}