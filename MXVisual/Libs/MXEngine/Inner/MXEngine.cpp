#include "MXEngine.h"
#include "MXAsset.h"
#include "MXAppMesh.h"
#include "MXAppMeshInstance.h"
#include "MXAppMaterial.h"
#include "MXGraphicsRenderer.h"
#include "MXMediaPlayer.h"
#include "MXAppAudio.h"

namespace MXVisual
{
	Boolean	EngineInit()
	{

		return False;
	}
	Void	EngineRelease()
	{

	}


	Void EngineRenderMesh(class IAppMesh* pMesh, UInt32 uSubMesh,
		class IAppMaterialInstance* pMtlIns)
	{
		AppMesh* pRealMesh = ((AppMesh*)pMesh);

		GraphicsRenderer::RegisterPrimitive(
			pRealMesh->GetGraphicsResource(),
			pRealMesh->GetVertexOffset(uSubMesh),
			pRealMesh->GetVertexCount(uSubMesh),
			((AppMaterialInstance*)pMtlIns)->GetRefMaterial()->GetGraphicsResource(),
			((AppMaterialInstance*)pMtlIns)->GetGraphicsResource());
	}

	Void EngineRenderMesh(class IAppMeshInstance* pMeshInstance)
	{
		ASSERT_IF_FAILED(pMeshInstance);
		AppMeshInstance* pReal = (AppMeshInstance*)pMeshInstance;
		AppMesh* pRealMesh = (AppMesh*)pReal->GetMesh();

		for (UInt32 uSubMesh = 0; uSubMesh < pRealMesh->GetSubMeshCount(); uSubMesh++)
		{
			AppMaterialInstance* pMtlIns = pReal->GetMaterialInstance(uSubMesh);
			if (pMtlIns == nullptr)continue;

			GraphicsRenderer::RegisterPrimitive(
				pRealMesh->GetGraphicsResource(),
				pRealMesh->GetVertexOffset(uSubMesh),
				pRealMesh->GetVertexCount(uSubMesh),
				pMtlIns->GetRefMaterial()->GetGraphicsResource(),
				pMtlIns->GetGraphicsResource(),
				pReal->GetGraphicsResource());
		}
	}

	Void EnginePlayAudio(class IAppAudio* pAudio)
	{
		if (pAudio != nullptr)
		{
			AppAudio* pRealAudio = (AppAudio*)pAudio;
			MediaPlayer::RegisterResource(pRealAudio->GetMediaResource());
		}
	}
}
