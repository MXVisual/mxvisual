#include "MXAppAnimationClip.h"
#include "MXMath.h"
#include "MXFile.h"

#include "MXAppMesh.h"
#include "MXAppSkeleton.h"

#include "stdio.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	AppAnimationClip::AppAnimationClip(const Char* szName)
		: m_strName(szName)
		, m_fTotalTime(0.0f)
		, m_fCurrentTime(0.0f)
		, m_fSampleRate(0.0f)
	{

	}
	AppAnimationClip::~AppAnimationClip()
	{

	}

	Float1	AppAnimationClip::GetTotalTime() const
	{
		return m_fTotalTime;
	}
	Void	AppAnimationClip::SetSampleRate(Float1 fRate)
	{
		m_fSampleRate = fRate;
		//Change Data
		for (UInt32 u = 0; u < m_arrAnimStack.ElementCount(); u++)
		{
		}
	}

	Void	AppAnimationClip::SetSamplingTime(Float1 fTime)
	{
		m_fCurrentTime = Math::Clamp(fTime, 0.0f, m_fTotalTime);
	}

	Void AppAnimationClip::Sampling(const IAppMesh* pMesh, const IAppSkeleton* pSkeleton, Array<Matrix4>& arrSampleResult) const
	{
		ASSERT_IF_FAILED(pMesh != nullptr && pSkeleton != nullptr);
		if (pMesh == nullptr || pSkeleton == nullptr)return;

		const AppMesh* pRealMesh = (const AppMesh*)pMesh;
		//FIXME!! 此处去除了const，不够规范
		AppSkeleton* pRealSkeleton = (AppSkeleton*)pSkeleton;

		auto FindTrack = [this](const Char* szNode) -> CAnimationTrack*
		{
			CAnimationTrack* pStack = nullptr;
			for (UInt32 u = 0; u < m_arrAnimStack.ElementCount(); u++)
			{
				if (m_arrAnimStack[u]->strNodeName == szNode)
				{
					pStack = m_arrAnimStack[u];
					break;
				}
			}
			return pStack;
		};

		auto GetTrackTransformData = [this](CAnimationTrack* pTrack)
		{
			if (pTrack != nullptr && pTrack->arrTransform.ElementCount() > 0)
			{
				UInt32 uMaxIndex = pTrack->arrTransform.ElementCount() - 1;
				Float1 fRate = m_fCurrentTime / m_fTotalTime * uMaxIndex;
				UInt32 uIndex0 = Math::Floor(fRate);
				UInt32 uIndex1 = Math::Ceil(fRate);

				Matrix4 mat[2] = {
					pTrack->arrTransform[uIndex0],
					pTrack->arrTransform[uIndex1]
				};

				Float3	s[2] = {
					Math::MatrixGetScale(mat[0]),
					Math::MatrixGetScale(mat[1]),
				};

				Quaternion	q[2] = {
					Math::MatrixGetQuaternion(mat[0]),
					Math::MatrixGetQuaternion(mat[1]),
				};

				Float3	t[2] = {
					Math::MatrixGetTranslation(mat[0]),
					Math::MatrixGetTranslation(mat[1]),
				};

				Float3 sL = Math::Lerp(s[0], s[1], fRate - uIndex0);
				Quaternion qL = Math::SLerp(q[0], q[1], fRate - uIndex0);
				Float3 tL = Math::Lerp(t[0], t[1], fRate - uIndex0);

				return Math::MatrixScale(sL) * Math::MatrixQuaternion(qL) *
					Math::MatrixTranslation(tL);
			}

			return Matrix4::Identity;
		};

		Array<Matrix4> FullSkeletonTransforms;
		for (UInt32 u = 0; u < pRealSkeleton->GetBoneCount(); u++)
		{
			CAnimationTrack* pTrack = FindTrack(pRealSkeleton->GetBoneName(u));
			FullSkeletonTransforms.Add(GetTrackTransformData(pTrack));
		}

		Array<Matrix4> FullSkeletonAbsTransforms;
		pRealSkeleton->CalculateBoneToModels(FullSkeletonTransforms, FullSkeletonAbsTransforms);

		for (UInt32 uBindingBone = 0; uBindingBone < pRealMesh->GetBindingBoneCount(); uBindingBone++)
		{
			UInt32 uBoneIdx = pRealSkeleton->GetBoneIndex(pRealMesh->GetBindingBoneName(uBindingBone));
			arrSampleResult.Add(
				pRealMesh->GetBindingBoneModelToBone(uBindingBone) * FullSkeletonAbsTransforms[uBoneIdx]);
		}
	}

	Void	AppAnimationClip::AddSample(const Char* szNodeName, Float1 fSampleTime, Matrix4 matTransform)
	{
		CAnimationTrack* pStack = nullptr;
		for (UInt32 u = 0; u < m_arrAnimStack.ElementCount(); u++)
		{
			if (m_arrAnimStack[u]->strNodeName == szNodeName)
			{
				pStack = m_arrAnimStack[u];
				break;
			}
		}
		if (pStack == nullptr)
		{
			pStack = new CAnimationTrack;
			m_arrAnimStack.Add(pStack);

			pStack->strNodeName = szNodeName;
		}

		pStack->arrTransform.Add(matTransform);
		m_fTotalTime = Math::Max(fSampleTime, m_fTotalTime);
	}

	Boolean	AppAnimationClip::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		if (strcmp(DeserializerBlock.GetBlockName(), "AnimationClip") != 0)
			return False;


		DeserializerBlock.Load("TotalTime", m_fTotalTime);
		DeserializerBlock.Load("SampleRate", m_fSampleRate);

		for (UInt32 u = 0; u < DeserializerBlock.GetChildBlocksCount(); u++)
		{
			const IDeserializerBlock* pChild = DeserializerBlock.GetChildBlock(u);

			CAnimationTrack* pStack = new CAnimationTrack;
			pChild->Load("Name", pStack->strNodeName);
			Float1 fRes;
			pChild->Load("SampleCount", fRes);

			for (UInt32 uSample = 0; uSample < (UInt32)fRes; uSample++)
			{
				Matrix4 mat;
				pChild->Load(String::Format("Transform%d", uSample).CString(), mat);
				pStack->arrTransform.Add(mat);
			}

			m_arrAnimStack.Add(pStack);
		}

		return True;
	}
#if MX_BUILD_EDITOR
	Boolean	AppAnimationClip::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("AnimationClip");

		Serializer.Save("TotalTime", m_fTotalTime);
		Serializer.Save("SampleRate", m_fSampleRate);

		for (UInt32 u = 0; u < m_arrAnimStack.ElementCount(); u++)
		{
			Serializer.Save("{");
			Serializer.Save("Stack");
			Serializer.Save("Name", m_arrAnimStack[u]->strNodeName.CString());

			Serializer.Save("SampleCount", m_arrAnimStack[u]->arrTransform.ElementCount());
			for (UInt32 uSample = 0; uSample < m_arrAnimStack[u]->arrTransform.ElementCount(); uSample++)
			{
				Serializer.Save(
					String::Format("Transform%d", uSample).CString(),
					m_arrAnimStack[u]->arrTransform[uSample]);
			}

			Serializer.Save("}");
		}
		return True;
	}
#endif
}