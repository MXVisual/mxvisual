#include "MXTextureLoader.h"
#include "DirectXTex.h"
#include "MXString.h"

namespace MXVisual
{
	Boolean DDSLoader(const Char* szFileName, TextureFileInfo& result)
	{
		Array<UInt8> wc = String::Transcode_GB2312_To_UTF16(szFileName);

		DirectX::ScratchImage images;
		if (FAILED(DirectX::LoadFromDDSFile((const wchar_t*)wc.GetRawData(), DirectX::DDS_FLAGS_NONE, nullptr, images)))
		{
			return False;
		}

		switch (images.GetMetadata().format)
		{
		case DXGI_FORMAT_R8_UNORM:result.eFormat = IAppTexture::Format::ETF_R8; break;
		case DXGI_FORMAT_R8G8_UNORM:result.eFormat = IAppTexture::Format::ETF_RG8; break;
		case DXGI_FORMAT_R8G8B8A8_UNORM:result.eFormat = IAppTexture::Format::ETF_RGBA8; break;

		case DXGI_FORMAT_R16_FLOAT:result.eFormat = IAppTexture::Format::ETF_R16; break;
		case DXGI_FORMAT_R16G16_FLOAT:result.eFormat = IAppTexture::Format::ETF_RG16; break;
		case DXGI_FORMAT_R16G16B16A16_FLOAT:result.eFormat = IAppTexture::Format::ETF_RGBA16; break;

		case DXGI_FORMAT_R32_FLOAT:result.eFormat = IAppTexture::Format::ETF_R32; break;
		case DXGI_FORMAT_R32G32_FLOAT:result.eFormat = IAppTexture::Format::ETF_RG32; break;
		case DXGI_FORMAT_R32G32B32A32_FLOAT:result.eFormat = IAppTexture::Format::ETF_RGBA32; break;

		case DXGI_FORMAT_BC1_UNORM:result.eFormat = IAppTexture::Format::ETF_BC1; break;
		case DXGI_FORMAT_BC3_UNORM:result.eFormat = IAppTexture::Format::ETF_BC3; break;
		case DXGI_FORMAT_BC4_UNORM:result.eFormat = IAppTexture::Format::ETF_BC4; break;
		case DXGI_FORMAT_BC5_UNORM:result.eFormat = IAppTexture::Format::ETF_BC5; break;
		case DXGI_FORMAT_BC6H_UF16:result.eFormat = IAppTexture::Format::ETF_BC6H; break;
		case DXGI_FORMAT_BC7_UNORM:result.eFormat = IAppTexture::Format::ETF_BC7; break;

		default:	return False;
		}
		result.uWidth = images.GetMetadata().width;
		result.uHeight = images.GetMetadata().height;
		result.uDepth = images.GetMetadata().depth * images.GetMetadata().arraySize;
		result.uMipLevel = images.GetMetadata().mipLevels;
		result.bCubeMap = images.GetMetadata().IsCubemap() ||
			(images.GetMetadata().arraySize == 6 && images.GetMetadata().depth == 1 && images.GetMetadata().dimension == DirectX::TEX_DIMENSION_TEXTURE2D);
		result.bArrayTex = !result.bCubeMap && images.GetMetadata().arraySize > 1;
		
		for (UInt32 uMip = 0; uMip < result.uMipLevel; uMip++)
		{
			for (UInt32 uPlane = 0; uPlane < result.uDepth; uPlane++)
			{
				const DirectX::Image* pImg = nullptr;
				if (result.bCubeMap)
				{
					pImg = images.GetImage(uMip, uPlane, 0);
				}
				else
				{
					pImg = images.GetImage(uMip, 0, uPlane);
				}
				UInt32 PixelDataSize = pImg->slicePitch;
				UInt8* pData = new UInt8[PixelDataSize];
				memcpy(pData, pImg->pixels, PixelDataSize);
				result.arrMipPixelData.Add(pData);
			}
		}

		return True;
	}
}
