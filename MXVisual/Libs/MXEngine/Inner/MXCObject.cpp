#include "MXCObject.h"
#include "MXCComponent.h"
#include "MXCComponent2D.h"
#include "MXMath.h"
#include "MXCSerializer.h"
#include "MXAsset.h"

#if MX_BUILD_EDITOR
#include "MXCEditorGraphics.h"
#include "MXCEditorPanel.h"
#endif

namespace MXVisual
{
	namespace
	{
		UInt32 g_uObjectCount = 0;

		Matrix4 TransformMatrix(const Float3& p, const Quaternion& q, const Float3& s)
		{
			Matrix4 matS = Math::MatrixScale(s);
			Matrix4 matR = Math::MatrixQuaternion(q);
			Matrix4 matT = Math::MatrixTranslation(p);

			return matS * matR * matT;
		}
		Matrix4 TransformMatrix2D(const Float2& prepNormalized, const Float2& p, Float1 q, const Float2& s)
		{
			Matrix4 matPreT = Math::MatrixTranslation(Float3(prepNormalized[0], prepNormalized[1], 0.0f));

			Matrix4 matS = Math::MatrixScale(Float3(s[0], s[1], 0.0f));
			Matrix4 matR = Math::MatrixAxisRotation(Math::AXIS_Z, q);
			Matrix4 matT = Math::MatrixTranslation(Float3(p[0], p[1], 0.0f));

			return matPreT * matS * matR * matT;
		}
	}

#pragma region ICOBJECT
	ICObject::ICObject(const Char* szName)
		: m_strName(szName)
		, m_uUniqueID(g_uObjectCount)
		, m_matLocal(Matrix4::Identity)
		, m_bEnable(True)
	{
		g_uObjectCount++;
	}
	ICObject::~ICObject()
	{
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			delete m_arrComponent[u];
		}
		m_arrComponent.Clear();
	}

	Void ICObject::SetEnable(Boolean bEnable)
	{
		if (m_bEnable != bEnable)
		{
			m_bEnable = bEnable;
			if (m_bEnable)
			{
				//OnEnable();
			}
			else
			{
				//OnDisable();
			}
		}
	}

	Void ICObject::SetTransform(const Matrix4& mat, Boolean bLocal)
	{
		if (bLocal)
		{
			m_matLocal = mat;
		}
		else
		{
			if (m_pParent != nullptr)
			{
				Matrix4 matParentInv = Math::MatrixInverse(m_pParent->GetTransform(False));
				m_matLocal = mat * matParentInv;
			}
			else
			{
				m_matLocal = mat;
			}
		}

		m_matGlobal = m_matLocal * (m_pParent == nullptr ? Matrix4::Identity : m_pParent->GetTransform(False));
		for (UInt32 u = 0; u < m_arrChilden.size(); u++)
		{
			m_arrChilden[u]->SetTransform(m_arrChilden[u]->GetTransform(True), True);
		}
	}
	Matrix4	ICObject::GetTransform(Boolean bLocal) const
	{
		return (bLocal || m_pParent == nullptr) ? m_matLocal : m_matGlobal;
	}

	Void ICObject::OnInit()
	{
		Matrix4 matParent = Matrix4::Identity;
		if (m_pParent != nullptr)
		{
			matParent = m_pParent->GetTransform(False);
		}
		m_matGlobal = m_matLocal * matParent;

		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			if (!m_arrComponent[u]->GetEnable())continue;
			m_arrComponent[u]->OnInit();
		}
	}
	Void ICObject::OnUpdate()
	{
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			if (!m_arrComponent[u]->GetEnable())continue;
			m_arrComponent[u]->OnUpdate();
		}
	}
	Void ICObject::OnUpdateLate()
	{
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			if (!m_arrComponent[u]->GetEnable())continue;
			m_arrComponent[u]->OnUpdateLate();
		}
	}
	Void ICObject::OnUpdateFixedDeltaTime()
	{
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			if (!m_arrComponent[u]->GetEnable())continue;
			m_arrComponent[u]->OnUpdateFixedDeltaTime();
		}
	}
	Void ICObject::OnRender()
	{
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			if (!m_arrComponent[u]->GetEnable())continue;
			m_arrComponent[u]->OnRender();
		}
	}
	Void ICObject::OnRelease()
	{
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			if (!m_arrComponent[u]->GetEnable())continue;
			m_arrComponent[u]->OnRelease();
		}
	}

	Void ICObject::AddChild(ICObject* pChild, Boolean bKeepTransform)
	{
		if (pChild == this || pChild == nullptr)return;

		m_arrChilden.push_back(pChild);
		Matrix4 matGlobal = pChild->GetTransform(False);

		pChild->m_pParent = this;

		if (bKeepTransform)
		{
			pChild->SetTransform(matGlobal, False);
		}

		pChild->OnInit();
	}
	ICObject* ICObject::GetChild(UInt32 uIndex) const
	{
		if (uIndex < m_arrChilden.size())
			return m_arrChilden[uIndex];
		return nullptr;
	}
	ICObject* ICObject::RemoveChild(UInt32 uIndex)
	{
		ICObject* pRes = nullptr;
		if (uIndex < m_arrChilden.size())
		{
			pRes = m_arrChilden[uIndex];
			pRes->m_pParent = nullptr;

			m_arrChilden.erase(m_arrChilden.begin() + uIndex);
		}
		return pRes;
	}
	ICObject* ICObject::GetChild(const Char* szChildName, Boolean bRecursion) const
	{
		ICObject* pRes = nullptr;
		for (UInt32 u = 0; u < m_arrChilden.size(); u++)
		{
			if (strcmp(m_arrChilden[u]->GetName(), szChildName) == 0)
			{
				return m_arrChilden[u];
			}
			if (bRecursion)
			{
				pRes = m_arrChilden[u]->GetChild(szChildName, bRecursion);
				if (pRes)break;
			}
		}
		return pRes;
	}
	Void ICObject::AddComponent(ICComponent* pComponent)
	{
		pComponent->m_pRefObj = this;
		pComponent->OnInit();
		m_arrComponent.Add(pComponent);
	}
	ICComponent* ICObject::GetComponent(UInt32 uIndex) const
	{
		if (uIndex < m_arrComponent.ElementCount())
			return m_arrComponent[uIndex];
		return nullptr;
	}
	ICComponent* ICObject::GetComponent(const Char* szComponentClass) const
	{
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			if (strcmp(m_arrComponent[u]->GetClassName(), szComponentClass) == 0)
			{
				return m_arrComponent[u];
			}
		}
		return nullptr;
	}
	ICComponent* ICObject::RemoveComponent(UInt32 uIndex)
	{
		ICComponent* pComp = nullptr;
		if (uIndex < m_arrComponent.ElementCount())
		{
			pComp = m_arrComponent[uIndex];;
			m_arrComponent.Erase(uIndex, 1);
		}
		return pComp;
	}
	ICComponent* ICObject::RemoveComponent(const Char* szComponentClass)
	{
		ICComponent* pComp = nullptr;
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			if (strcmp(m_arrComponent[u]->GetClassName(), szComponentClass) == 0)
			{
				pComp = m_arrComponent[u];
				m_arrComponent.Erase(u, 1);
				break;
			}
		}
		return pComp;
	}

	Boolean ICObject::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		String strValue;
		DeserializerBlock.Load("Name", strValue);
		SetName(strValue.CString());

		Matrix4 mat;
		DeserializerBlock.Load("Local", mat);
		SetTransform(mat, True);

		for (UInt32 u = 0; u < DeserializerBlock.GetChildBlocksCount(); u++)
		{
			const IDeserializerBlock* pComponentBlock = DeserializerBlock.GetChildBlock(u);
			if (strcmp(pComponentBlock->GetBlockName(), "Component") == 0)
			{
				pComponentBlock->Load("Name", strValue);
				ICComponent* pComp = ICComponent::Create(strValue.CString());
				ICSerializable* pSupported = dynamic_cast<ICSerializable*>(pComp);
				if (pSupported != nullptr)
				{
					pSupported->OnDeserialize(*pComponentBlock);
				}
				AddComponent(pComp);
			}
		}
		return True;
	}

#if MX_BUILD_EDITOR
	Boolean ICObject::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("Name", m_strName.CString());
		Serializer.Save("Local", m_matLocal);
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			Serializer.Save("{");
			Serializer.Save("Component");
			Serializer.Save("Name", m_arrComponent[u]->GetClassName());

			ICSerializable* pSupported = dynamic_cast<ICSerializable*>(m_arrComponent[u]);
			if (pSupported != nullptr)
				pSupported->OnSerialize(Serializer);
			Serializer.Save("}");
		}
		return True;
	}
#endif
#pragma endregion

#pragma region COBJECT
	CObject::CObject(const Char* szName)
		: ICObject(szName)
		, m_f3Scale(1.0f, 1.0f, 1.0f)
		, m_f3DirRight(Math::AXIS_X)
		, m_f3DirUp(Math::AXIS_Y)
		, m_f3DirForward(Math::AXIS_Z)
	{
	}

	Void CObject::SetPosition(const Float3& f3Position, Boolean bLocal)
	{
		if (!bLocal && m_pParent != nullptr)
		{
		}
		else
		{
			m_f3Position = f3Position;
		}
		SetTransform(TransformMatrix(m_f3Position, m_qRotation, m_f3Scale), True);
	}
	Void CObject::SetRotation(const Float3& f3Rotation, Boolean bLocal)
	{
		if (!bLocal && m_pParent != nullptr)
		{
		}
		else
		{
			m_qRotation = Math::EularToQuaternion(f3Rotation);
		}
		SetTransform(TransformMatrix(m_f3Position, m_qRotation, m_f3Scale), True);
	}
	Void CObject::SetScale(const Float3& f3Scale, Boolean bLocal)
	{
		if (!bLocal && m_pParent != nullptr)
		{
		}
		else
		{
			m_f3Scale = f3Scale;
		}
		SetTransform(TransformMatrix(m_f3Position, m_qRotation, m_f3Scale), True);
	}

	Void CObject::SetTransform(const Matrix4& mat, Boolean bLocal)
	{
		ICObject::SetTransform(mat, bLocal);

		m_f3Position = Math::MatrixGetTranslation(m_matLocal);
		m_f3Scale = Math::MatrixGetScale(m_matLocal);
		m_qRotation = Math::MatrixGetQuaternion(m_matLocal);

		Matrix3 matRot = Math::Split(Math::MatrixQuaternion(m_qRotation));
		m_f3DirRight = Math::MatrixTransform(Math::AXIS_X, matRot);
		m_f3DirUp = Math::MatrixTransform(Math::AXIS_Y, matRot);
		m_f3DirForward = Math::MatrixTransform(Math::AXIS_Z, matRot);
	}

	const Float3 CObject::GetPosition(Boolean bLocal) const
	{
		if (!bLocal && m_pParent != nullptr)
		{
			Float4 res = Math::MatrixTransform(Math::Combine(m_f3Position, 1.0f),
				m_pParent->GetTransform(False));
			return Math::Split(res);
		}
		return m_f3Position;
	}
	const Quaternion CObject::GetRotation(Boolean bLocal) const
	{
		if (!bLocal && m_pParent != nullptr)
		{
			//	m_f3Scale * m_pParent->GetTransform(False);
		}
		return m_qRotation;
	}
	const Float3 CObject::GetScale(Boolean bLocal) const
	{
		if (!bLocal && m_pParent != nullptr)
		{
			//	m_f3Scale * m_pParent->GetTransform(False);
		}
		return m_f3Scale;
	}

	Void CObject::TransMove(const Float3 & f3Direction, Float1 fDistance)
	{
		m_f3Position += f3Direction * fDistance;
		m_matLocal = TransformMatrix(m_f3Position, m_qRotation, m_f3Scale);
	}
	Void CObject::TransRotate(const Float3 & f3Axis, Float1 fRadius)
	{
		Matrix4 mat = Math::MatrixAxisRotation(f3Axis, fRadius);
		m_qRotation *= Math::MatrixGetQuaternion(mat);

		Float4 vec = Math::Combine(m_f3DirForward, 1);
		vec = Math::MatrixTransform(vec, mat);
		m_f3DirForward = Math::Split(vec);

		vec = Math::Combine(m_f3DirUp, 1);
		vec = Math::MatrixTransform(vec, mat);
		m_f3DirUp = Math::Split(vec);

		vec = Math::Combine(m_f3DirRight, 1);
		vec = Math::MatrixTransform(vec, mat);
		m_f3DirRight = Math::Split(vec);
		m_matLocal = TransformMatrix(m_f3Position, m_qRotation, m_f3Scale);
	}
	Void CObject::TransScale(const Float3& f3Direction, Float1 fRate)
	{
		m_f3Scale += f3Direction * fRate;
		m_matLocal = TransformMatrix(m_f3Position, m_qRotation, m_f3Scale);
	}

	//Editor
#if MX_BUILD_EDITOR
	Void CObject::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyString("Name", GetName());
		Panel.AddPropertyFloat3("Position", GetPosition(MXVisual::True), 0.01f,
			-9999.0f, 9999.0f);
		MXVisual::Float3 f3 = MXVisual::Math::QuaternionToEular(GetRotation(MXVisual::True));
		f3 = MXVisual::Float3(MXVisual::Math::Radian2Degree(f3[0]), MXVisual::Math::Radian2Degree(f3[1]), MXVisual::Math::Radian2Degree(f3[2]));
		Panel.AddPropertyFloat3("Rotation", f3, 0.01f,
			-360.0f, 360.0f);
		Panel.AddPropertyFloat3("Scale", GetScale(MXVisual::True), 0.01f,
			-9999.0f, 9999.0f);
	}
	Void CObject::OnPanelInactived()
	{

	}
	Void CObject::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EString:
			SetName(((EditorPropertyString*)pProperty)->Value);
			break;
		case IEditorProperty::EFloat3:
			if (strcmp(pProperty->Name, "Position") == 0)
			{
				SetPosition(((EditorPropertyFloat3*)pProperty)->Value, MXVisual::True);
			}
			else if (strcmp(pProperty->Name, "Rotation") == 0)
			{
				MXVisual::Float3 r(Math::Degree2Radian(((EditorPropertyFloat3*)pProperty)->Value[0]),
					Math::Degree2Radian(((EditorPropertyFloat3*)pProperty)->Value[1]),
					Math::Degree2Radian(((EditorPropertyFloat3*)pProperty)->Value[2]));
				SetRotation(r, MXVisual::True);
			}
			else if (strcmp(pProperty->Name, "Scale") == 0)
			{
				SetScale(((EditorPropertyFloat3*)pProperty)->Value, MXVisual::True);
			}
		default:
			break;
		}
	}
#endif
#pragma endregion

#pragma region COBJECT2D
	CObject2D::CObject2D(const Char* szName)
		: ICObject(szName)
		, m_f2AnchorPosition(0.0f, 0.0f)
		, m_f2Scale(1.0f, 1.0f)
		, m_f2DirRight(1.0f, 0.0f)
		, m_f2DirUp(0.0f, 1.0f)
		, m_uLayer(0)
	{
	}

	Void CObject2D::SetPosition(const Float2& f2Position, Boolean bLocal)
	{
		if (!bLocal && m_pParent != nullptr)
		{
		}
		else
		{
			m_f2Position = f2Position;
			m_matLocal = TransformMatrix2D(m_f2AnchorPosition * -1.0f, m_f2Position, m_fRotation, m_f2Scale);
		}
	}
	Void CObject2D::SetAnchorPosition(const Float2 & f2Anchor)
	{
		m_f2AnchorPosition = Math::Clamp(f2Anchor, Float2(0, 0), Float2(1, 1));
		m_matLocal = TransformMatrix2D(m_f2AnchorPosition * -1.0f, m_f2Position, m_fRotation, m_f2Scale);
	}
	Void CObject2D::SetRotation(Float1 fRotation, Boolean bLocal)
	{
		if (!bLocal && m_pParent != nullptr)
		{
		}
		else
		{
			m_fRotation = fRotation;

			Matrix3 matR = Math::Split(Math::MatrixAxisRotation(Math::AXIS_Z, m_fRotation));

			Float3 vec = Math::MatrixTransform(Math::AXIS_X, matR);
			m_f2DirRight = Float2(vec[0], vec[1]);

			vec = Math::MatrixTransform(Math::AXIS_Y, matR);
			m_f2DirUp = Float2(vec[0], vec[1]);

			m_matLocal = TransformMatrix2D(m_f2AnchorPosition * -1.0f, m_f2Position, m_fRotation, m_f2Scale);
		}
	}
	Void CObject2D::SetScale(const Float2& f2Scale, Boolean bLocal)
	{
		if (!bLocal && m_pParent != nullptr)
		{
		}
		else
		{
			m_f2Scale = f2Scale;
			m_matLocal = TransformMatrix2D(m_f2AnchorPosition * -1.0f, m_f2Position, m_fRotation, m_f2Scale);
		}
	}
	Void CObject2D::SetTransform(const Matrix4& mat, Boolean bLocal)
	{
		ICObject::SetTransform(mat, bLocal);

		Float3 fValue = Math::MatrixGetTranslation(m_matLocal);
		m_f2Position = Float2(fValue[0], fValue[1]);

		fValue = Math::MatrixGetScale(m_matLocal);
		m_f2Scale = Float2(fValue[0], fValue[1]);

		fValue = Math::MatrixGetRotation(m_matLocal);
		m_fRotation = fValue[2];

		Matrix3 matR = Math::Split(Math::MatrixAxisRotation(Math::AXIS_Z, m_fRotation));
		Float3 vec = Math::MatrixTransform(Math::AXIS_X, matR);
		m_f2DirRight = Float2(vec[0], vec[1]);
		vec = Math::MatrixTransform(Math::AXIS_Y, matR);
		m_f2DirUp = Float2(vec[0], vec[1]);
	}

	Float2 CObject2D::GetPosition(Boolean bLocal)
	{
		if (!bLocal && m_pParent != nullptr)
		{
			Float4 p = Math::MatrixTransform(Float4(m_f2Position[0], m_f2Position[1], 0, 1.0f),
				m_pParent->GetTransform(False));
			return Float2(p[0], p[1]);
		}
		return m_f2Position;
	}
	Float1 CObject2D::GetRotation(Boolean bLocal)
	{
		if (!bLocal && m_pParent != nullptr)
		{
			Float3 f3ParentRotate = Math::MatrixGetRotation(m_pParent->GetTransform(False));
			return m_fRotation * f3ParentRotate[2];
		}
		return m_fRotation;
	}
	Float2 CObject2D::GetScale(Boolean bLocal)
	{
		if (!bLocal && m_pParent != nullptr)
		{
			Float3 f3ParentScale = Math::MatrixGetScale(m_pParent->GetTransform(False));
			return m_f2Scale * Float2(f3ParentScale[0], f3ParentScale[1]);
		}
		return m_f2Scale;
	}

	Void CObject2D::TransMove(const Float2& f2Direction, Float1 fDistance)
	{
		m_f2Position += f2Direction * fDistance;
		m_matLocal = TransformMatrix2D(m_f2AnchorPosition * -1.0f, m_f2Position, m_fRotation, m_f2Scale);
	}
	Void CObject2D::TransRotate(Float1 fRadius)
	{
		Matrix4 matRAdd = Math::MatrixAxisRotation(Math::AXIS_Z, fRadius);
		m_fRotation += fRadius;

		Float4 vec = Float4(m_f2DirRight[0], m_f2DirRight[1], 0.0f, 0.0f);
		vec = Math::MatrixTransform(vec, matRAdd);
		m_f2DirRight = Float2(vec[0], vec[1]);

		vec = Float4(m_f2DirUp[0], m_f2DirUp[1], 0.0f, 0.0f);
		vec = Math::MatrixTransform(vec, matRAdd);
		m_f2DirUp = Float2(vec[0], vec[1]);
		m_matLocal = TransformMatrix2D(m_f2AnchorPosition * -1.0f, m_f2Position, m_fRotation, m_f2Scale);
	}
	Void CObject2D::TransScale(const Float2& f2Direction, Float1 fRate)
	{
		m_f2Scale += f2Direction * fRate;
		m_matLocal = TransformMatrix2D(m_f2AnchorPosition * -1.0f, m_f2Position, m_fRotation, m_f2Scale);
	}

	//Editor
#if MX_BUILD_EDITOR
	Void CObject2D::OnPanelActived(IEditorPropertyPanel& Panel)
	{
		Panel.AddPropertyString("Name", GetName());
		Panel.AddPropertyFloat2("Anchor", m_f2AnchorPosition,
			0.01f, 0.0f, 1.0f);

		Panel.AddPropertyFloat2("Position", GetPosition(MXVisual::True), 0.01f,
			-9999.0f, 9999.0f);
		Panel.AddPropertyFloat1("Rotation", MXVisual::Math::Radian2Degree(GetRotation(MXVisual::True)),
			0.01f, -9999.0f, 9999.0f);
		Panel.AddPropertyFloat2("Scale", GetScale(MXVisual::True), 0.01f,
			-9999.0f, 9999.0f);
		Panel.AddPropertyFloat1("Layer", (Float1)m_uLayer,
			1.0f, 0.0f, 8.0f);
	}
	Void CObject2D::OnPanelInactived()
	{

	}
	Void CObject2D::OnPanelChanged(const IEditorProperty* pProperty)
	{
		switch (pProperty->PropertyType)
		{
		case IEditorProperty::EString:
			SetName(((EditorPropertyString*)pProperty)->Value);
			break;
		case IEditorProperty::EFloat2:
			if (strcmp(pProperty->Name, "Position") == 0)
			{
				SetPosition(((EditorPropertyFloat2*)pProperty)->Value, MXVisual::True);
			}
			else if (strcmp(pProperty->Name, "Scale") == 0)
			{
				SetScale(((EditorPropertyFloat2*)pProperty)->Value, MXVisual::True);
			}
			else if (strcmp(pProperty->Name, "Anchor") == 0)
			{
				SetAnchorPosition(((EditorPropertyFloat2*)pProperty)->Value);
			}
			break;
		case IEditorProperty::EFloat1:
		{
			if (strcmp(pProperty->Name, "Position") == 0)
			{
				SetRotation(Math::Degree2Radian(((EditorPropertyFloat1*)pProperty)->Value), MXVisual::True);
			}
			else if (strcmp(pProperty->Name, "Layer") == 0)
			{
				m_uLayer = (UInt32)((EditorPropertyFloat1*)pProperty)->Value;
			}
			break;
		}
		default:
			break;
		}
	}

	Void CObject2D::OnEditorRender(IEditorGraphics& Graphics)
	{
		const Float1 lineWidth = 4.0f;
		Float4 ltrb(
			m_f2Scale[0] * -m_f2AnchorPosition[0] + m_f2Position[0],
			m_f2Scale[1] * -m_f2AnchorPosition[1] + m_f2Position[1],
			m_f2Scale[0] * (1.0f - m_f2AnchorPosition[0]) + m_f2Position[0],
			m_f2Scale[1] * (1.0f - m_f2AnchorPosition[1]) + m_f2Position[1]
		);

		Graphics.ChangeColor(Float4(0, 1, 0, 1));
		Graphics.RenderLine2D(Float2(ltrb[0], ltrb[1]), Float2(ltrb[0], ltrb[3]), lineWidth);
		Graphics.RenderLine2D(Float2(ltrb[0], ltrb[1]), Float2(ltrb[2], ltrb[1]), lineWidth);
		Graphics.RenderLine2D(Float2(ltrb[0], ltrb[3]), Float2(ltrb[2], ltrb[3]), lineWidth);
		Graphics.RenderLine2D(Float2(ltrb[2], ltrb[1]), Float2(ltrb[2], ltrb[3]), lineWidth);
	}
#endif

	Void CObject2D::OnClick(Input::Key eTriggerKey)
	{
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			CComponent2D* pComp = dynamic_cast<CComponent2D*>(m_arrComponent[u]);
			if (pComp != nullptr)
			{
				pComp->OnClick(eTriggerKey);
			}
		}
	}
	Void CObject2D::OnFocused()
	{
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			CComponent2D* pComp = dynamic_cast<CComponent2D*>(m_arrComponent[u]);
			if (pComp != nullptr)
			{
				pComp->OnFocused();
			}
		}
	}
	Void CObject2D::OnLostFocus()
	{
		for (UInt32 u = 0; u < m_arrComponent.ElementCount(); u++)
		{
			CComponent2D* pComp = dynamic_cast<CComponent2D*>(m_arrComponent[u]);
			if (pComp != nullptr)
			{
				pComp->OnLostFocus();
			}
		}
	}

#pragma endregion
}
