#pragma once
#ifndef _MX_APP_FONT_
#define _MX_APP_FONT_
#include "MXAsset.h"
#include "MXString.h"
#include "MXArray.h"

#include "ft2build.h"
#include FT_FREETYPE_H

namespace MXVisual
{
	class IGTexture;

	class AppFont : public IAppFont
	{
	public:
		AppFont(const Char* szName);
		~AppFont();

		const Char* GetName() const override { return m_strName.CString(); }

		Boolean Init(const Char* szFileName, FT_Library ftLibrary);

		UInt32 GetCharacterSize() const override { return m_uCharacterSize; }
		Boolean GetBold() const override { return m_bBold; }
		Boolean GetItalic() const override { return m_bItalic; }

		Void SetCharacterSize(UInt32 uSize) override;
		Void SetBold(Boolean bBold) override;
		Void SetItalic(Boolean bItalic) override;

		Float4 GetCharacterRect(UInt16 uCharacter);

	protected:
		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override { return True; }
#if MX_BUILD_EDITOR
		Boolean	OnSerialize(ISerializer& Serializer) override { return True; }
#endif

	protected:
		Void RenderFont();

		String m_strName;
		UInt32 m_uUniqueID;

		FT_Face m_FTFace;

		UInt32 m_uCharacterSize;
		Boolean m_bBold;
		Boolean m_bItalic;
		IGTexture* m_pFontTexture;
		Array<Float4> m_mapCharacterUV;
	};
}
#endif