#pragma once
#ifndef _MX_APP_MESH_INSTANCE_
#define _MX_APP_MESH_INSTANCE_
#include "MXAsset.h"
#include "MXAppMesh.h"
#include "MXAppMaterialInstance.h"

namespace MXVisual
{
	class IGPrimitiveTransformBuffer;
	class AppMeshInstance : public IAppMeshInstance
	{
	public:
		AppMeshInstance(const Char* szName);
		~AppMeshInstance();
		const Char* GetName() const override { return m_strName.CString(); }

		Void SetTransforms(UInt32 uTransformCount, const Matrix4* pTransforms) override;
		Void SetMaterialInstance(UInt32 uSubMesh, const Char* szMaterialInstanceAssetName) override;
		Void SetMesh(const Char* szMeshAssetName) override;

		const IAppMesh* GetMesh() const override { return m_pAppMesh; }
		const Char* GetMaterialInstanceName(UInt32 uSubMesh) const override
		{
			return m_arrMaterialIns[uSubMesh] != nullptr ? m_arrMaterialIns[uSubMesh]->GetName() : nullptr;
		}

		AppMesh* GetMesh() { return m_pAppMesh; }
		AppMaterialInstance* GetMaterialInstance(UInt32 uSubMesh) { return m_arrMaterialIns[uSubMesh]; }

		IGPrimitiveTransformBuffer* GetGraphicsResource() const { return m_pTransformBlock; }
	protected:
		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override;
#if MX_BUILD_EDITOR
		Boolean	OnSerialize(ISerializer& Serializer) override;
#endif

	protected:
		String m_strName;

		AppMesh* m_pAppMesh;
		Array<Matrix4> m_arrTransforms;
		Array<AppMaterialInstance*> m_arrMaterialIns;
		IGPrimitiveTransformBuffer* m_pTransformBlock;
	};
}
#endif