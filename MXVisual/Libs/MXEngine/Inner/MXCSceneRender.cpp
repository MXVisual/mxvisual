#include "MXCSceneRender.h"
#define USE_DLL 0
#include "MXCScene.h"
#include "MXCObject.h"
#include "MXCCamera.h"
#include "MXCComponent.h"
#include "MXCComponent2D.h"
#include "MXGraphicsRenderer.h"
#include "MXPhysicsSimulator.h"

#include "MXFile.h"
#include "MXLog.h"
#include <functional>

#include "MXAsset.h"
#include "MXAppMesh.h"
#include "MXAppMeshInstance.h"

namespace MXVisual
{
	Array<CComponent2D*> arrComponent2D;

	Void TraverDepthFirst(ICObject* pObj, std::function<Void(ICObject*)>& FCallback)
	{
		if (pObj != nullptr)
		{
			FCallback(pObj);

			for (UInt32 u = 0; u < pObj->GetChildCount(); u++)
			{
				TraverDepthFirst(pObj->GetChild(u), FCallback);
			}
		}
	}

	Void CSceneRender::Render(CScene* pScene, CCamera* pViewCamera)
	{
		if (pViewCamera == nullptr || pViewCamera->GetViewFrustum() == nullptr)return;

		GVarAmbientColor.f3Value = pScene->m_envProperty.f3AmbientColor;
		GVarAmbientScale.fValue = pScene->m_envProperty.fAmbientScale;
		GVarEnableSSAO.bValue = pScene->m_envProperty.bSSAO;
		GVarSSAORange.fValue = pScene->m_envProperty.fSSAORange;
		GVarEnableSSR.bValue = pScene->m_envProperty.bSSR;
		GVarEnableBloom.bValue = pScene->m_envProperty.bBloom;
		GVarBloomExporedLuminance.fValue = pScene->m_envProperty.fBloomLuminance;
		GVarHeightFogEnable.bValue = pScene->m_envProperty.bHeightFog;
		GVarHeightFogColor.f3Value = pScene->m_envProperty.f3HeightFogColor;
		GVarHeightFogDensity.fValue = pScene->m_envProperty.fHeightFogDensity;
		GVarHeightFogStartDistance.fValue = pScene->m_envProperty.fHeightFogStartDistance;
		GVarHeightFogDistanceFalloff.fValue = pScene->m_envProperty.fHeightFogDistanceFalloff;
		GVarHeightFogHeight.fValue = pScene->m_envProperty.fHeightFogHeight;
		GVarHeightFogHeightFalloff.fValue = pScene->m_envProperty.fHeightFogHeightFalloff;

		arrComponent2D.Clear();
		std::function<Void(ICObject*)> CallRender = [](ICObject* pObj)
		{
			if (!pObj->GetEnable())return;

			ICComponent* pComp = nullptr;
			CComponent2D* pComp2D = nullptr;
			for (UInt32 u = 0; u < pObj->GetComponentCount(); u++)
			{
				pComp = pObj->GetComponent(u);
				pComp2D = dynamic_cast<CComponent2D*>(pComp);
				if (!pComp->GetEnable())continue;
				if (pComp2D != nullptr)
				{
					arrComponent2D.Add(pComp2D);
				}
				else
				{
					pComp->OnRender();
				}
			}
		};

		for (UInt32 u = 0; u < pScene->GetRootObjectCount(); u++)
		{
			TraverDepthFirst(pScene->GetRootObject(u), CallRender);
		}

		//Sort UI Component
		//TODO!!

		//Render UI Component
		for (UInt32 u = 0; u < arrComponent2D.ElementCount(); u++)
		{
			arrComponent2D[u]->OnRender();
		}

		GRendererConfigure configure;
		GraphicsRenderer::Render(pViewCamera->GetViewFrustum(), configure);
	}
}
