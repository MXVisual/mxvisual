#include "MXAppMesh.h"
#include "MXMath.h"
#include "MXFile.h"
#include "MXCSerializer.h"
#include <functional>

namespace MXVisual
{
	Boolean IAppMesh::VertexRigid::operator==(const IAppMesh::VertexRigid& other) const
	{
		return f4Position == other.f4Position &&
			f4Normal == other.f4Normal &&
			f4Tangent == other.f4Tangent &&
			f4Color == other.f4Color &&
			f2TexCoord0 == other.f2TexCoord0;
	}
	Boolean IAppMesh::VertexSkin::operator==(const  IAppMesh::VertexSkin& other) const
	{
		if (f4Position == other.f4Position &&
			f4Normal == other.f4Normal &&
			f4Tangent == other.f4Tangent &&
			f4Color == other.f4Color &&
			f2TexCoord0 == other.f2TexCoord0)
		{
			for (UInt32 u = 0; u < 4; u++)
			{
				if (uRefBoneIndex[u] != other.uRefBoneIndex[u] ||
					Math::Abs(fRefBoneWeight[u] - other.fRefBoneWeight[u]) > Constant_MIN_FLOAT)
				{
					return False;
				}
			}
			return True;
		}
		return False;
	}

	AppMesh::AppMesh(const Char* szName)
		: m_strName(szName)
		, m_pPrimitive(nullptr)
		, m_pVertexBuffer(nullptr)
		, m_pIndexBuffer(nullptr)
	{

	}
	AppMesh::~AppMesh()
	{
		MX_RELEASE_INTERFACE(m_pPrimitive);

		if (m_pVertexBuffer != nullptr)
		{
			switch (m_eMeshType)
			{
			case EAPPMT_Rigid2D:
			{
				VertexRigid2D* pVertex = (VertexRigid2D*)m_pVertexBuffer;
				delete[] pVertex;
			}
			break;
			case EAPPMT_Skin2D:
			{
				VertexSkin2D* pVertex = (VertexSkin2D*)m_pVertexBuffer;
				delete[] pVertex;
			}
			break;
			case EAPPMT_Rigid:
			case EAPPMT_Line:
			{
				VertexRigid* pVertex = (VertexRigid*)m_pVertexBuffer;
				delete[] pVertex;
			}
			break;
			case EAPPMT_Skin:
			{
				VertexSkin* pVertex = (VertexSkin*)m_pVertexBuffer;
				delete[] pVertex;
			}
			break;
			}
			m_pVertexBuffer = nullptr;
		}
		MX_DELETE_ARRAY(m_pIndexBuffer);
	}

	Boolean AppMesh::Init(MeshType eType, Boolean b32BitIndex, Boolean bDynamic,
		UInt32 uVertexCapacity, UInt32 uIndexCapacity, UInt32 uSubMeshCount)
	{
		MX_DELETE_ARRAY(m_pVertexBuffer);
		MX_DELETE_ARRAY(m_pIndexBuffer);

		m_eMeshType = eType;
		IGPrimitive::Description primDesc;
		switch (eType)
		{
		case EAPPMT_Rigid2D:
			primDesc.eType = IGPrimitive::Type::EPrimT_Rigid2D;
			m_pVertexBuffer = new VertexSkin[uVertexCapacity];
			break;
		case EAPPMT_Skin2D:
			m_pVertexBuffer = new VertexSkin2D[uVertexCapacity];
			break;
		case EAPPMT_Rigid:
			primDesc.eType = IGPrimitive::Type::EPrimT_Rigid;
			m_pVertexBuffer = new VertexRigid[uVertexCapacity];
			break;
		case EAPPMT_Skin:
			primDesc.eType = IGPrimitive::Type::EPrimT_Skin;
			m_pVertexBuffer = new VertexSkin[uVertexCapacity];
			break;
		case EAPPMT_Line:
			primDesc.eType = IGPrimitive::Type::EPrimT_Line;
			m_pVertexBuffer = new VertexRigid[uVertexCapacity];
			break;
		}

		ASSERT_IF_FAILED(b32BitIndex || !"16 bit Index Not Supported Now");
		m_pIndexBuffer = new UInt8[uIndexCapacity * (b32BitIndex ? 4 : 2)];

		m_uSubMeshCount = uSubMeshCount;
		m_arrSubMeshOffset.SetElementCount(uSubMeshCount);
		m_arrSubMeshSize.SetElementCount(uSubMeshCount);
		for (UInt32 u = 0; u < m_uSubMeshCount; u++)
		{
			m_arrSubMeshOffset[u] = 0;
			m_arrSubMeshSize[u] = 0;
		}

		m_uVertexCount = 0;
		m_uIndexCount = 0;

		primDesc.uVertexCount = uVertexCapacity;
		primDesc.uIndexCount = uIndexCapacity;
		primDesc.bDynamic = bDynamic;
		m_pPrimitive = GraphicsResource::CreatePrimitive(primDesc);

		return m_pVertexBuffer != nullptr &&
			m_pIndexBuffer != nullptr &&
			m_pPrimitive != nullptr;
	}

	Void AppMesh::SetVertices(UInt32 uVertexCount, const IVertex* pVertices)
	{
		m_uVertexCount = Math::Min<SInt32>(GetVertexCapacity(), uVertexCount);

		switch (m_eMeshType)
		{
		case MXVisual::IAppMesh::EAPPMT_Rigid2D:
			m_pPrimitive->UpdateVertexBuffer(
				(const GVertexRigid2D*)pVertices, m_uVertexCount);
			memcpy(m_pVertexBuffer, pVertices, m_uVertexCount * sizeof(VertexRigid));
			break;
		case MXVisual::IAppMesh::EAPPMT_Skin2D:
			break;
		case MXVisual::IAppMesh::EAPPMT_Line:
		case MXVisual::IAppMesh::EAPPMT_Rigid:
			m_pPrimitive->UpdateVertexBuffer(
				(const GVertexRigid*)pVertices, m_uVertexCount);
			memcpy(m_pVertexBuffer, pVertices, m_uVertexCount * sizeof(VertexRigid));
			break;
		case MXVisual::IAppMesh::EAPPMT_Skin:
			m_pPrimitive->UpdateVertexBuffer(
				(const GVertexSkin*)pVertices, m_uVertexCount);
			memcpy(m_pVertexBuffer, pVertices, m_uVertexCount * sizeof(VertexSkin));
			break;
		default:
			break;
		}
	}
	Void AppMesh::SetIndices(UInt32 uIndexCount, const UInt8* pIndices)
	{
		m_uIndexCount = Math::Min<SInt32>(GetIndexCapacity(), uIndexCount);
		m_pPrimitive->UpdateIndexBuffer((UInt32*)pIndices, m_uIndexCount);
		memcpy(m_pIndexBuffer, pIndices, sizeof(UInt32) * uIndexCount);
	}
	Void AppMesh::SetSubMesh(UInt32 uSubMesh, UInt32 uOffset, UInt32 uSize)
	{
		ASSERT_IF_FAILED(uSubMesh < m_uSubMeshCount);
		m_arrSubMeshOffset[uSubMesh] = uOffset;
		m_arrSubMeshSize[uSubMesh] = uSize;
	}

	Boolean AppMesh::AddBindingBone(const Char* szBoneName, const Matrix4& matLocalToBone)
	{
		m_arrBindingBoneName.Add(szBoneName);
		m_arrModelToBindingBone.Add(matLocalToBone);
		return True;
	}
	UInt32	AppMesh::GetBindingBoneCount() const
	{
		return Math::Max(1u, m_arrBindingBoneName.ElementCount());
	}
	const Char*	AppMesh::GetBindingBoneName(UInt32 uBoneIndex) const
	{
		if (uBoneIndex < m_arrBindingBoneName.ElementCount())
		{
			return m_arrBindingBoneName[uBoneIndex].CString();
		}
		return nullptr;
	}
	Matrix4	AppMesh::GetBindingBoneModelToBone(UInt32 uBoneIndex) const
	{
		if (uBoneIndex < m_arrModelToBindingBone.ElementCount())
		{
			return m_arrModelToBindingBone[uBoneIndex];
		}
		return Matrix4::Identity;
	}

	Boolean	AppMesh::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		UInt32 uVertexSize = 0;
		std::function<Void(const Char*, IVertex*)> FLoadVertex;
		if (strcmp(DeserializerBlock.GetBlockName(), "RigidMesh") == 0)
		{
			m_eMeshType = IAppMesh::EAPPMT_Rigid;
			uVertexSize = sizeof(VertexRigid);
			FLoadVertex = [](const Char* szContent, IVertex* pRawVertex) {
				VertexRigid* pVertex = (VertexRigid*)pRawVertex;
				sscanf(szContent, "%f,%f,%f;%f,%f,%f;%f,%f,%f,%f;%f,%f;%f,%f,%f,%f",
					&pVertex->f4Position[0], &pVertex->f4Position[1], &pVertex->f4Position[2],
					&pVertex->f4Normal[0], &pVertex->f4Normal[1], &pVertex->f4Normal[2],
					&pVertex->f4Tangent[0], &pVertex->f4Tangent[1], &pVertex->f4Tangent[2], &pVertex->f4Tangent[3],
					&pVertex->f2TexCoord0[0], &pVertex->f2TexCoord0[1],
					&pVertex->f4Color[0], &pVertex->f4Color[1], &pVertex->f4Color[2], &pVertex->f4Color[3]
				);
			};
		}
		else if (strcmp(DeserializerBlock.GetBlockName(), "SkinMesh") == 0)
		{
			m_eMeshType = IAppMesh::EAPPMT_Skin;
			uVertexSize = sizeof(VertexSkin);
			FLoadVertex = [](const Char* szContent, IVertex* pRawVertex) {
				VertexSkin* pVertex = (VertexSkin*)pRawVertex;
				sscanf(szContent, "%f,%f,%f;%f,%f,%f;%f,%f,%f,%f;%f,%f;%f,%f,%f,%f;%f,%f,%f,%f;%d,%d,%d,%d",
					&pVertex->f4Position[0], &pVertex->f4Position[1], &pVertex->f4Position[2],
					&pVertex->f4Normal[0], &pVertex->f4Normal[1], &pVertex->f4Normal[2],
					&pVertex->f4Tangent[0], &pVertex->f4Tangent[1], &pVertex->f4Tangent[2], &pVertex->f4Tangent[3],
					&pVertex->f2TexCoord0[0], &pVertex->f2TexCoord0[1],
					&pVertex->f4Color[0], &pVertex->f4Color[1], &pVertex->f4Color[2], &pVertex->f4Color[3],
					&pVertex->fRefBoneWeight[0], &pVertex->fRefBoneWeight[1], &pVertex->fRefBoneWeight[2], &pVertex->fRefBoneWeight[3],
					&pVertex->uRefBoneIndex[0], &pVertex->uRefBoneIndex[1], &pVertex->uRefBoneIndex[2], &pVertex->uRefBoneIndex[3]
				);
			};
		}
		else
		{
			return False;
		}

		std::function<Void(const Char*, UInt8*)> FLoadTriangle;
		FLoadTriangle = [](const Char* szContent, UInt8* pRawIndex) {
			UInt32* pIndex = (UInt32*)pRawIndex;
			sscanf(szContent, "%d,%d,%d", &pIndex[0], &pIndex[1], &pIndex[2]);
		};

		UInt32 uVertexCount, uTriangleCount, uSubMeshCount;
		DeserializerBlock.Load("Vertex", uVertexCount);
		DeserializerBlock.Load("Triangle", uTriangleCount);
		DeserializerBlock.Load("SubMesh", uSubMeshCount);
		uSubMeshCount = Math::Max<SInt32>(1, uSubMeshCount);
		if (!Init(m_eMeshType, True, False, uVertexCount, uTriangleCount * 3, uSubMeshCount))
		{
			return False;
		}

		m_uVertexCount = uVertexCount;
		m_uIndexCount = uTriangleCount * 3;
		m_uSubMeshCount = uSubMeshCount;

		UInt32 uSubMeshDataIndex = 0;
		for (UInt32 u = 0; u < DeserializerBlock.GetChildBlocksCount(); u++)
		{
			const IDeserializerBlock* pChild = DeserializerBlock.GetChildBlock(u);

			if (strcmp(pChild->GetBlockName(), "Vertex") == 0)
			{
				String Buf;
				for (UInt32 uV = 0; uV < uVertexCount; uV++)
				{
					pChild->Load(String::Format("v%d", uV).CString(), Buf);
					FLoadVertex(Buf.CString(), m_pVertexBuffer + uVertexSize * uV);
				}
			}
			else if (strcmp(pChild->GetBlockName(), "Triangle") == 0)
			{
				String Buf;
				for (UInt32 uTri = 0; uTri < uTriangleCount; uTri++)
				{
					pChild->Load(String::Format("tri%d", uTri).CString(), Buf);
					FLoadTriangle(Buf.CString(), m_pIndexBuffer + sizeof(UInt32) * uTri * 3);
				}
			}
			else if (strcmp(pChild->GetBlockName(), "SubMesh") == 0)
			{
				ASSERT_IF_FAILED(uSubMeshDataIndex < uSubMeshCount);
				pChild->Load("Offset", m_arrSubMeshOffset[uSubMeshDataIndex]);
				pChild->Load("Size", m_arrSubMeshSize[uSubMeshDataIndex]);

				uSubMeshDataIndex++;
			}
			else if (strcmp(pChild->GetBlockName(), "BindingBone") == 0)
			{
				UInt32 uBoneCount = 0;
				pChild->Load("Count", uBoneCount);
				String strRes;
				Char szBuf[Constant_MAX_PATH] = { 0 };
				for (UInt32 u = 0; u < uBoneCount; u++)
				{
					pChild->Load(String::Format("b%d", u).CString(), strRes);

					Float1 value[4][4];

					sscanf(strRes.CString(), "%[^,],%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",
						szBuf,
						&value[0][0], &value[0][1], &value[0][2], &value[0][3],
						&value[1][0], &value[1][1], &value[1][2], &value[1][3],
						&value[2][0], &value[2][1], &value[2][2], &value[2][3],
						&value[3][0], &value[3][1], &value[3][2], &value[3][3]);
					AddBindingBone(szBuf,
						Matrix4(value[0][0], value[0][1], value[0][2], value[0][3],
							value[1][0], value[1][1], value[1][2], value[1][3],
							value[2][0], value[2][1], value[2][2], value[2][3],
							value[3][0], value[3][1], value[3][2], value[3][3]));
				}
			}
		}

		switch (m_eMeshType)
		{
		case IAppMesh::EAPPMT_Rigid:
			m_pPrimitive->UpdateVertexBuffer((const GVertexRigid*)m_pVertexBuffer, m_uVertexCount);
			break;
		case IAppMesh::EAPPMT_Skin:
			m_pPrimitive->UpdateVertexBuffer((const GVertexSkin*)m_pVertexBuffer, m_uVertexCount);
			break;
		}
		m_pPrimitive->UpdateIndexBuffer((const UInt32*)m_pIndexBuffer, m_uIndexCount);

		return True;
	}
#if MX_BUILD_EDITOR
	typedef Void(*FSaveVertex_Ptr)(String&, IAppMesh::IVertex*);

	Void FSaveVertex_Rigid(String& strContent, IAppMesh::IVertex* pRawVertex)
	{
		IAppMesh::VertexRigid* pVertex = (IAppMesh::VertexRigid*)pRawVertex;
		strContent += Math::Split(pVertex->f4Position);
		strContent += ";";
		strContent += Math::Split(pVertex->f4Normal);
		strContent += ";";
		strContent += pVertex->f4Tangent;
		strContent += ";";
		strContent += pVertex->f2TexCoord0;
		strContent += ";";
		strContent += pVertex->f4Color;
	};
	Void FSaveVertex_Skin(String& strContent, IAppMesh::IVertex* pRawVertex)
	{
		IAppMesh::VertexSkin* pVertex = (IAppMesh::VertexSkin*)pRawVertex;
		strContent += Math::Split(pVertex->f4Position);
		strContent += ";";
		strContent += Math::Split(pVertex->f4Normal);
		strContent += ";";
		strContent += pVertex->f4Tangent;
		strContent += ";";
		strContent += pVertex->f2TexCoord0;
		strContent += ";";
		strContent += pVertex->f4Color;
		strContent += ";";
		strContent += String::Format("%f,%f,%f,%f",
			pVertex->fRefBoneWeight[0],
			pVertex->fRefBoneWeight[1],
			pVertex->fRefBoneWeight[2],
			pVertex->fRefBoneWeight[3]);
		strContent += ";";
		strContent += String::Format("%d,%d,%d,%d",
			pVertex->uRefBoneIndex[0],
			pVertex->uRefBoneIndex[1],
			pVertex->uRefBoneIndex[2],
			pVertex->uRefBoneIndex[3]);
	};

	Boolean	AppMesh::OnSerialize(ISerializer& Serializer)
	{
		UInt32 uVertexSize = 0;

		FSaveVertex_Ptr FSaveVertex = nullptr;
		switch (m_eMeshType)
		{
		case MXVisual::IAppMesh::EAPPMT_Rigid2D:
		case MXVisual::IAppMesh::EAPPMT_Skin2D:
			ASSERT_IF_FAILED(!"Invalid Mesh Type For Saving");
			return False;
			break;
		case MXVisual::IAppMesh::EAPPMT_Rigid:
			Serializer.Save("RigidMesh");
			uVertexSize = sizeof(VertexRigid);
			FSaveVertex = FSaveVertex_Rigid;
			break;
		case MXVisual::IAppMesh::EAPPMT_Skin:
			Serializer.Save("SkinMesh");
			uVertexSize = sizeof(VertexSkin);
			FSaveVertex = FSaveVertex_Skin;
			break;
		}

		std::function<Void(String&, UInt8*)> FSaveTriangle;
		FSaveTriangle = [](String& strContent, UInt8* pRawIndex) {
			UInt32* pIndex = (UInt32*)pRawIndex;
			strContent = String::Format("%d,%d,%d", pIndex[0], pIndex[1], pIndex[2]);
		};

		Serializer.Save("Vertex", m_uVertexCount);
		Serializer.Save("Triangle", m_uIndexCount);
		Serializer.Save("SubMesh", GetSubMeshCount());

		Serializer.Save("{");
		Serializer.Save("Triangle");
		String strLineData;
		for (UInt32 u = 0; u < m_uIndexCount; u += 3)
		{
			FSaveTriangle(strLineData, m_pIndexBuffer + sizeof(UInt32) * u);
			Serializer.Save(
				String::Format("tri%d", u / 3).CString(),
				strLineData.CString());
		}
		Serializer.Save("}");

		Serializer.Save("{");
		Serializer.Save("Vertex");
		for (UInt32 u = 0; u < m_uVertexCount; u++)
		{
			strLineData.Clear();
			FSaveVertex(strLineData, m_pVertexBuffer + u * uVertexSize);
			Serializer.Save(
				String::Format("v%d", u).CString(),
				strLineData.CString());
		}
		Serializer.Save("}");

		if (m_eMeshType == IAppMesh::EAPPMT_Skin)
		{
			Serializer.Save("{");
			Serializer.Save("BindingBone");
			Serializer.Save("Count", m_arrBindingBoneName.ElementCount());

			for (UInt32 u = 0; u < m_arrBindingBoneName.ElementCount(); u++)
			{
				Serializer.Save(String::Format("b%d", u).CString(),
					String::Format("%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",
						m_arrBindingBoneName[u].CString(),
						m_arrModelToBindingBone[u].Get(0, 0), m_arrModelToBindingBone[u].Get(0, 1), m_arrModelToBindingBone[u].Get(0, 2), m_arrModelToBindingBone[u].Get(0, 3),
						m_arrModelToBindingBone[u].Get(1, 0), m_arrModelToBindingBone[u].Get(1, 1), m_arrModelToBindingBone[u].Get(1, 2), m_arrModelToBindingBone[u].Get(1, 3),
						m_arrModelToBindingBone[u].Get(2, 0), m_arrModelToBindingBone[u].Get(2, 1), m_arrModelToBindingBone[u].Get(2, 2), m_arrModelToBindingBone[u].Get(2, 3),
						m_arrModelToBindingBone[u].Get(3, 0), m_arrModelToBindingBone[u].Get(3, 1), m_arrModelToBindingBone[u].Get(3, 2), m_arrModelToBindingBone[u].Get(3, 3)).CString());
			}
			Serializer.Save("}");
		}

		for (UInt32 u = 0; u < GetSubMeshCount(); u++)
		{
			Serializer.Save("{");
			Serializer.Save("SubMesh");
			Serializer.Save("Offset", String::Format("%d", m_arrSubMeshOffset[u]).CString());
			Serializer.Save("Size", String::Format("%d", m_arrSubMeshSize[u]).CString());
			Serializer.Save("}");
		}
		return True;
	}
#endif
}