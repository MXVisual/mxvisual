#include "MXAppAudio.h"
#include "MXMath.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	AppAudio::AppAudio(const Char* szName)
		: m_strName(szName)
		, m_pAudio(nullptr)
	{

	}
	AppAudio::~AppAudio()
	{
		MX_RELEASE_INTERFACE(m_pAudio);
	}

	Boolean AppAudio::Init(UInt32 SampleRate, UInt32 Channels, UInt32  uPCMDataSize, UInt16* pPCMData)
	{
		if (uPCMDataSize == 0 || pPCMData == nullptr)
			return False;

		IMAudio::Description desc;
		desc.uFrequency = SampleRate;
		if (Channels == 1)
		{
			desc.eChannelType = MAudioChannelType::EMACT_Mono;
			m_eChannelType = IAppAudio::ChannelType::EMono;
		}
		else
		{
			desc.eChannelType = MAudioChannelType::EMACT_Stero;
			m_eChannelType = IAppAudio::ChannelType::EStero;
		}
		desc.pPCMData = pPCMData;
		desc.uPCMDataSize = uPCMDataSize;
		m_pAudio = MediaResource::CreateAudio(desc);

		m_arrPCMData.SetElementCount(uPCMDataSize);
		memcpy(&m_arrPCMData[0], pPCMData, uPCMDataSize);

		return m_pAudio != nullptr;
	}
	Void	AppAudio::SamplePCF(Float1 fSecond, Array<Float1>& arrPCFData, UInt32 uSampleCount) const
	{
		UInt32 uIndex = fSecond / m_pAudio->GetTotalSecond();

		UInt32 RealSampleCount = uSampleCount * (m_eChannelType == IAppAudio::ChannelType::EMono ? 1.0f : 2.0f);

		SInt32 MaxIndex = m_arrPCMData.ElementCount() / 2;
		Float1 fIndex = MaxIndex * fSecond / m_pAudio->GetTotalSecond() - RealSampleCount / 2.0f;
		fIndex = Math::Clamp<Float1>(fIndex, 0.0f, MaxIndex);
		SInt32 nIndex = fIndex;

		UInt32 count = 0;
		for (; count < RealSampleCount && nIndex < MaxIndex; count++, nIndex++)
		{
			arrPCFData.Add(m_arrPCMData[nIndex]);
		}
	}

	Boolean	AppAudio::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		return True;
	}
#if MX_BUILD_EDITOR
	Boolean	AppAudio::OnSerialize(ISerializer& Serializer)
	{
		return True;
	}
#endif
}