#pragma once
#ifndef _MX_APP_AUDIO_
#define _MX_APP_AUDIO_
#include "MXAsset.h"
#include "MXMediaResource.h"

namespace MXVisual
{
	class AppAudio : public IAppAudio
	{
	public:
		AppAudio(const Char* szName);
		~AppAudio();
		const Char* GetName() const override { return m_strName.CString(); }

		Boolean Init(UInt32 SampleRate, UInt32 Channels, UInt32 uPCMDataSize, UInt16* pPCMData);
		Void	SamplePCF(Float1 fSecond, Array<Float1>& arrPCFData, UInt32 uSampleCount) const;

		Float1	GetCurrentSecond() const { return m_pAudio->GetCurrentSecond(); }
		ChannelType GetChannelType() const { return m_eChannelType; }
		UInt32	GetSampleRate() const { return m_pAudio->GetSampleRate(); }
		IMAudio* GetMediaResource() const { return m_pAudio; }
	protected:
		Boolean	OnDeserialize(const IDeserializerBlock& DeserializerBlock) override;
#if MX_BUILD_EDITOR
		Boolean	OnSerialize(ISerializer& Serializer) override;
#endif

	protected:
		String m_strName;
		ChannelType m_eChannelType;

		IMAudio* m_pAudio;
		Array<UInt16> m_arrPCMData;
	};
}
#endif