#include "MXAppMaterial.h"
#include "MXCSerializer.h"

namespace MXVisual
{
	IAppMaterial::ParameterDescription::ParameterDescription()
		: strDefault("Invalid")
		, eType(EPT_Invalid)
	{
	}
	IAppMaterial::ParameterDescription::ParameterDescription(
		const Char* szName, ParameterType ePType, const Float4& fDefault, Float1 f1Step)
	{
		strName = szName;
		eType = ePType;
		switch (ePType)
		{
		case MXVisual::IAppMaterial::EPT_Float1:
			f1Default = fDefault[0];
			break;
		case MXVisual::IAppMaterial::EPT_Float2:
			f2Default = Float2(fDefault[0], fDefault[1]);
			break;
		case MXVisual::IAppMaterial::EPT_Float3:
			f3Default = Float3(fDefault[0], fDefault[1], fDefault[2]);
			break;
		case MXVisual::IAppMaterial::EPT_Float4:
			f4Default = fDefault;
			break;
		}
		fStep = f1Step;
	}
	IAppMaterial::ParameterDescription::ParameterDescription(
		const Char* szName, ParameterType ePType, const Char* szDefault, const SamplerState& ss)
	{
		strName = szName;
		eType = ePType;
		strDefault = szDefault;
		sampler = ss;
	}

	AppMaterial::AppMaterial(const Char* szName)
		: m_strName(szName)
		, m_pMaterial(nullptr)
	{
		m_descMaterial.strUniqueName = szName;
	}
	AppMaterial::~AppMaterial()
	{
		MX_RELEASE_INTERFACE(m_pMaterial);
	}

	Boolean AppMaterial::Init(Boolean bCanCache)
	{
		m_pMaterial = nullptr;

		m_descMaterial.bCanCache = bCanCache;

		return True;
	}

#if MX_BUILD_EDITOR
	IAppMaterial::ParameterDescription AppMaterial::GetParameterDescription(UInt32 uParameter) const
	{
		switch (m_descMaterial.arrMaterialParams[uParameter].eType)
		{
		case IGMaterialParam::EMPT_Float1:
			return ParameterDescription(
				m_descMaterial.arrMaterialParams[uParameter].strName.CString(), ParameterType::EPT_Float1, m_descMaterial.arrMaterialParams[uParameter].fDefault);
		case IGMaterialParam::EMPT_Float2:
			return ParameterDescription(
				m_descMaterial.arrMaterialParams[uParameter].strName.CString(), ParameterType::EPT_Float2, m_descMaterial.arrMaterialParams[uParameter].fDefault);
		case IGMaterialParam::EMPT_Float3:
			return ParameterDescription(
				m_descMaterial.arrMaterialParams[uParameter].strName.CString(), ParameterType::EPT_Float3, m_descMaterial.arrMaterialParams[uParameter].fDefault);
		case IGMaterialParam::EMPT_Float4:
			return ParameterDescription(
				m_descMaterial.arrMaterialParams[uParameter].strName.CString(), ParameterType::EPT_Float4, m_descMaterial.arrMaterialParams[uParameter].fDefault);
		case IGMaterialParam::EMPT_Texture1D:
			return ParameterDescription(
				m_descMaterial.arrMaterialParams[uParameter].strName.CString(), ParameterType::EPT_Texture1D, m_descMaterial.arrMaterialParams[uParameter].strDefault.CString());
		case IGMaterialParam::EMPT_Texture2D:
			return ParameterDescription(
				m_descMaterial.arrMaterialParams[uParameter].strName.CString(), ParameterType::EPT_Texture2D, m_descMaterial.arrMaterialParams[uParameter].strDefault.CString());
		case IGMaterialParam::EMPT_Texture3D:
			return ParameterDescription(
				m_descMaterial.arrMaterialParams[uParameter].strName.CString(), ParameterType::EPT_Texture3D, m_descMaterial.arrMaterialParams[uParameter].strDefault.CString());
		case IGMaterialParam::EMPT_TextureCube:
			return ParameterDescription(
				m_descMaterial.arrMaterialParams[uParameter].strName.CString(), ParameterType::EPT_TextureCube, m_descMaterial.arrMaterialParams[uParameter].strDefault.CString());
		}
		return ParameterDescription("Invalid", IAppMaterial::EPT_Invalid, Float4());
	}
	IAppMaterial::ParameterDescription AppMaterial::GetParameterDescription(const Char* szParameter) const
	{
		for (UInt32 u = 0; u < m_descMaterial.arrMaterialParams.ElementCount(); u++)
		{
			if (m_descMaterial.arrMaterialParams[u].strName == szParameter)
			{
				switch (m_descMaterial.arrMaterialParams[u].eType)
				{
				case IGMaterialParam::EMPT_Float1:
					return ParameterDescription(szParameter, ParameterType::EPT_Float1, m_descMaterial.arrMaterialParams[u].fDefault);
				case IGMaterialParam::EMPT_Float2:
					return ParameterDescription(szParameter, ParameterType::EPT_Float2, m_descMaterial.arrMaterialParams[u].fDefault);
				case IGMaterialParam::EMPT_Float3:
					return ParameterDescription(szParameter, ParameterType::EPT_Float3, m_descMaterial.arrMaterialParams[u].fDefault);
				case IGMaterialParam::EMPT_Float4:
					return ParameterDescription(szParameter, ParameterType::EPT_Float4, m_descMaterial.arrMaterialParams[u].fDefault);
				case IGMaterialParam::EMPT_Texture1D:
					return ParameterDescription(szParameter, ParameterType::EPT_Texture1D, m_descMaterial.arrMaterialParams[u].strDefault.CString());
				case IGMaterialParam::EMPT_Texture2D:
					return ParameterDescription(szParameter, ParameterType::EPT_Texture2D, m_descMaterial.arrMaterialParams[u].strDefault.CString());
				case IGMaterialParam::EMPT_Texture3D:
					return ParameterDescription(szParameter, ParameterType::EPT_Texture3D, m_descMaterial.arrMaterialParams[u].strDefault.CString());
				case IGMaterialParam::EMPT_TextureCube:
					return ParameterDescription(szParameter, ParameterType::EPT_TextureCube, m_descMaterial.arrMaterialParams[u].strDefault.CString());
				}
				break;
			}
		}
		return ParameterDescription("Invalid", ParameterType::EPT_Invalid, Float4());
	}

	Void AppMaterial::SetDomain(Domain eType)
	{
		m_descMaterial.eDomain = (MaterialDomain)eType;
	}
	Void AppMaterial::SetShadingModel(ShadingModel eType)
	{
		m_descMaterial.eShadingModel = (MaterialShadingModel)eType;
	}
	Void AppMaterial::SetFaceType(FaceType eType)
	{
		m_descMaterial.eFaceType = (MaterialFaceType)eType;
	}
	Void AppMaterial::SetBlendMode(BlendMode eType)
	{
		m_descMaterial.eBlendMode = (MaterialBlendMode)eType;
	}
	Void AppMaterial::SetMaterialFunc(MaterialFunction eType, const String& strCode)
	{
		m_descMaterial.arrMaterialFunction[eType] = strCode;
	}
	const Char* AppMaterial::GetMaterialFunc(MaterialFunction eType) const
	{
		return m_descMaterial.arrMaterialFunction[eType].CString();
	}

	Void AppMaterial::RemoveParameter(const Char* szName)
	{
		for (UInt32 u = 0; u < m_descMaterial.arrMaterialParams.ElementCount(); u++)
		{
			if (m_descMaterial.arrMaterialParams[u].strName == szName)
			{
				m_descMaterial.arrMaterialParams.Erase(u, 1);
				break;
			}
		}
	}
	Void AppMaterial::SetParameter(const ParameterDescription& desc)
	{
		for (UInt32 u = 0; u < m_descMaterial.arrMaterialParams.ElementCount(); u++)
		{
			if (desc.strName == m_descMaterial.arrMaterialParams[u].strName)
			{
				m_descMaterial.arrMaterialParams[u].strName = desc.strName;
				ASSERT_IF_FAILED(!"Parameter Already Exist");
				return;
			}
		}

		switch (desc.eType)
		{
		case IAppMaterial::EPT_Float1:
			m_descMaterial.arrMaterialParams.Add(
				IGMaterialParam::Description(IGMaterialParam::EMPT_Float1,
					desc.strName.CString(),
					Float4(desc.f1Default, 0, 0, 0)));
			break;
		case IAppMaterial::EPT_Float2:
			m_descMaterial.arrMaterialParams.Add(
				IGMaterialParam::Description(IGMaterialParam::EMPT_Float2,
					desc.strName.CString(),
					Float4(desc.f2Default[0], desc.f2Default[1], 0, 0)));
			break;
		case IAppMaterial::EPT_Float3:
			m_descMaterial.arrMaterialParams.Add(
				IGMaterialParam::Description(IGMaterialParam::EMPT_Float3,
					desc.strName.CString(),
					Float4(desc.f3Default[0], desc.f3Default[1], desc.f3Default[2], 0)));
			break;
		case IAppMaterial::EPT_Float4:
			m_descMaterial.arrMaterialParams.Add(
				IGMaterialParam::Description(IGMaterialParam::EMPT_Float4,
					desc.strName.CString(),
					desc.f4Default));
			break;
		case IAppMaterial::EPT_Texture1D:
		case IAppMaterial::EPT_Texture2D:
		case IAppMaterial::EPT_Texture3D:
		case IAppMaterial::EPT_TextureCube:
		{
			IGMaterialParam::MaterialParamType type;
			if (desc.eType == IAppMaterial::EPT_Texture1D)
				type = IGMaterialParam::EMPT_Texture1D;
			else if (desc.eType == IAppMaterial::EPT_Texture2D)
				type = IGMaterialParam::EMPT_Texture2D;
			else if (desc.eType == IAppMaterial::EPT_Texture3D)
				type = IGMaterialParam::EMPT_Texture3D;
			else if (desc.eType == IAppMaterial::EPT_TextureCube)
				type = IGMaterialParam::EMPT_TextureCube;

			TextureAddressMode address;
			if (desc.sampler.eAddress == IAppMaterial::SamplerState::ESS_Address_Repeat)
				address = ETexAM_Repeat;
			else if (desc.sampler.eAddress == IAppMaterial::SamplerState::ESS_Address_Clamp)
				address = ETexAM_Clamp;
			else if (desc.sampler.eAddress == IAppMaterial::SamplerState::ESS_Address_Mirror)
				address = ETexAM_Mirror;

			TextureFilterMode filter;
			if (desc.sampler.eFilter == IAppMaterial::SamplerState::ESS_Filter_Point)
				filter = ETexFM_Point;
			else if (desc.sampler.eFilter == IAppMaterial::SamplerState::ESS_Filter_Bilinear)
				filter = ETexFM_Bilinear;
			else if (desc.sampler.eFilter == IAppMaterial::SamplerState::ESS_Filter_Anisotropic)
				filter = ETexFM_Anisotropic;

			m_descMaterial.arrMaterialParams.Add(
				IGMaterialParam::Description(type,
					desc.strName.CString(), desc.strDefault.CString(), address, filter));
			break;
		}
		}
	}

	Boolean AppMaterial::Recompile()
	{
		MX_RELEASE_INTERFACE(m_pMaterial);
		m_pMaterial = GraphicsResource::CreateMaterial(m_descMaterial, True);

		return m_pMaterial != nullptr;
	}
#endif
	Boolean	AppMaterial::OnDeserialize(const IDeserializerBlock& DeserializerBlock)
	{
		if (strcmp(DeserializerBlock.GetBlockName(), "AppMaterial") != 0)
			return False;

		String strValue;
		DeserializerBlock.Load("Domain", strValue);
		if (strValue == "WorldSpace")
		{
			m_descMaterial.eDomain = MaterialDomain::EWorldSpace;
		}
		else if (strValue == "Decal")
		{
			m_descMaterial.eDomain = MaterialDomain::EDecal;
		}
		else if (strValue == "ScreenSpace")
		{
			m_descMaterial.eDomain = MaterialDomain::EScreenSpace;
		}
		else if (strValue == "PostProcess")
		{
			m_descMaterial.eDomain = MaterialDomain::EPostProcess;
		}

		DeserializerBlock.Load("ShadingModel", strValue);
		if (strValue == "Nolighting")
		{
			m_descMaterial.eShadingModel = MaterialShadingModel::ENolighting;
		}
		else if (strValue == "Default")
		{
			m_descMaterial.eShadingModel = MaterialShadingModel::EDefault;
		}
		else if (strValue == "PostprocessFur")
		{
			m_descMaterial.eShadingModel = MaterialShadingModel::EPostprocessFur;
		}

		DeserializerBlock.Load("FaceType", strValue);
		if (strValue == "Frontface")
		{
			m_descMaterial.eFaceType = MaterialFaceType::EFrontface;
		}
		else if (strValue == "Backface")
		{
			m_descMaterial.eFaceType = MaterialFaceType::EBackface;
		}
		else if (strValue == "TwoSide")
		{
			m_descMaterial.eFaceType = MaterialFaceType::ETwoSide;
		}
		else if (strValue == "Wireframe")
		{
			m_descMaterial.eFaceType = MaterialFaceType::EWireframe;
		}

		DeserializerBlock.Load("BlendMode", strValue);
		if (strValue == "Opaque")
		{
			m_descMaterial.eBlendMode = MaterialBlendMode::EOpaque;
		}
		else if (strValue == "Masked")
		{
			m_descMaterial.eBlendMode = MaterialBlendMode::EMasked;
		}
		else if (strValue == "Translucency")
		{
			m_descMaterial.eBlendMode = MaterialBlendMode::ETranslucency;
		}
		else if (strValue == "Additive")
		{
			m_descMaterial.eBlendMode = MaterialBlendMode::EAdditive;
		}
		else if (strValue == "MaskTranslucency")
		{
			m_descMaterial.eBlendMode = MaterialBlendMode::EMaskTranslucency;
		}

		for (UInt32 u = 0; u < DeserializerBlock.GetChildBlocksCount(); u++)
		{
			const IDeserializerBlock* pBlock = DeserializerBlock.GetChildBlock(u);
			if (strcmp(pBlock->GetBlockName(), "MaterialParam") == 0)
			{
				String strValue;
				pBlock->Load("Type", strValue);
				Float4 f4Value;
				IGMaterialParam::MaterialParamType eType = IGMaterialParam::EMPT_Invalid;
				ParameterType eAppParameterType = IAppMaterial::EPT_Invalid;
				if (strValue == "Float1")
				{
					eAppParameterType = EPT_Float1;
					eType = IGMaterialParam::EMPT_Float1;
					pBlock->Load("Default", f4Value[0]);
				}
				else if (strValue == "Float2")
				{
					eAppParameterType = EPT_Float2;
					eType = IGMaterialParam::EMPT_Float2;
					Float2 v;
					pBlock->Load("Default", v);
					f4Value[0] = v[0];
					f4Value[1] = v[1];
				}
				else if (strValue == "Float3")
				{
					eAppParameterType = EPT_Float3;
					eType = IGMaterialParam::EMPT_Float3;
					Float3 v;
					pBlock->Load("Default", v);
					f4Value[0] = v[0];
					f4Value[1] = v[1];
					f4Value[2] = v[2];
				}
				else if (strValue == "Float4")
				{
					eAppParameterType = EPT_Float4;
					eType = IGMaterialParam::EMPT_Float4;
					pBlock->Load("Default", f4Value);
				}
				else if (strValue == "Texture1D")
				{
					eAppParameterType = EPT_Texture1D;
					eType = IGMaterialParam::EMPT_Texture1D;
				}
				else if (strValue == "Texture2D")
				{
					eAppParameterType = EPT_Texture2D;
					eType = IGMaterialParam::EMPT_Texture2D;
				}
				else if (strValue == "Texture3D")
				{
					eAppParameterType = EPT_Texture3D;
					eType = IGMaterialParam::EMPT_Texture3D;
				}
				else if (strValue == "TextureCube")
				{
					eAppParameterType = EPT_TextureCube;
					eType = IGMaterialParam::EMPT_TextureCube;
				}

				pBlock->Load("Name", strValue);
				String strDefault;
				pBlock->Load("Default", strDefault);

				if (eType == IGMaterialParam::EMPT_Float1 ||
					eType == IGMaterialParam::EMPT_Float2 ||
					eType == IGMaterialParam::EMPT_Float3 ||
					eType == IGMaterialParam::EMPT_Float4)
				{
					Float1 step;
					pBlock->Load("Step", step);
					m_arrParameterDesc.Add(
						ParameterDescription(strValue.CString(), eAppParameterType, f4Value, step));
					m_descMaterial.arrMaterialParams.Add(
						IGMaterialParam::Description(eType, strValue.CString(), f4Value));
				}
				else if (eType == IGMaterialParam::EMPT_Texture1D ||
					eType == IGMaterialParam::EMPT_Texture2D ||
					eType == IGMaterialParam::EMPT_Texture3D ||
					eType == IGMaterialParam::EMPT_TextureCube)
				{
					m_descMaterial.arrMaterialParams.Add(
						IGMaterialParam::Description(eType, strValue.CString(),
							strDefault.CString()));
				}
			}
			else if (strcmp(pBlock->GetBlockName(), "Function") == 0)
			{
				String strValue;
				pBlock->Load("Type", strValue);

				MaterialFunctionType eType = MaterialFunctionType::EInvalid;
				if (strValue == "Custom")
				{
					eType = MaterialFunctionType::ECustom;
				}
				else if (strValue == "Mesh")
				{
					eType = MaterialFunctionType::EMeshParams;
				}
				else if (strValue == "Surface")
				{
					eType = MaterialFunctionType::ESurfaceParams;
				}

				pBlock->Load("Code", m_descMaterial.arrMaterialFunction[(const UInt32)eType]);
			}
		}

		m_descMaterial.bCanCache = True;

		m_pMaterial = GraphicsResource::CreateMaterial(m_descMaterial);
		return True;
	}

#if MX_BUILD_EDITOR
	Boolean	AppMaterial::OnSerialize(ISerializer& Serializer)
	{
		Serializer.Save("AppMaterial");

		switch (m_descMaterial.eDomain)
		{
		case MaterialDomain::EWorldSpace:
			Serializer.Save("Domain", "WorldSpace");
			break;
		case MaterialDomain::EDecal:
			Serializer.Save("Domain", "Decal");
			break;
		case MaterialDomain::EScreenSpace:
			Serializer.Save("Domain", "ScreenSpace");
			break;
		case MaterialDomain::EPostProcess:
			Serializer.Save("Domain", "PostProcess");
			break;
		}

		switch (m_descMaterial.eShadingModel)
		{
		case MaterialShadingModel::ENolighting:
			Serializer.Save("ShadingModel", "Nolighting");
			break;
		case MaterialShadingModel::EDefault:
			Serializer.Save("ShadingModel", "Default");
			break;
		case MaterialShadingModel::EPostprocessFur:
			Serializer.Save("ShadingModel", "PostprocessFur");
			break;
		}

		switch (m_descMaterial.eFaceType)
		{
		case MaterialFaceType::EFrontface:
			Serializer.Save("FaceType", "Frontface");
			break;
		case MaterialFaceType::EBackface:
			Serializer.Save("FaceType", "Backface");
			break;
		case MaterialFaceType::ETwoSide:
			Serializer.Save("FaceType", "TwoSide");
			break;
		case MaterialFaceType::EWireframe:
			Serializer.Save("FaceType", "Wireframe");
			break;
		}

		switch (m_descMaterial.eBlendMode)
		{
		case MaterialBlendMode::EOpaque:
			Serializer.Save("BlendMode", "Opaque");
			break;
		case MaterialBlendMode::EMasked:
			Serializer.Save("BlendMode", "Masked");
			break;
		case MaterialBlendMode::ETranslucency:
			Serializer.Save("BlendMode", "Translucency");
			break;
		case MaterialBlendMode::EAdditive:
			Serializer.Save("BlendMode", "Additive");
			break;
		case MaterialBlendMode::EMaskTranslucency:
			Serializer.Save("BlendMode", "MaskTranslucency");
			break;
		}

		for (UInt32 u = 0; u < m_arrParameterDesc.ElementCount(); u++)
		{
			const ParameterDescription& param = m_arrParameterDesc[u];
			Serializer.Save("{");
			Serializer.Save("MaterialParam");
			switch (param.eType)
			{
			case ParameterType::EPT_Float1:
				Serializer.Save("Name", param.strName.CString());
				Serializer.Save("Type", "Float1");
				Serializer.Save("Default", param.f1Default);
				Serializer.Save("Step", param.fStep);
				break;
			case ParameterType::EPT_Float2:
				Serializer.Save("Name", param.strName.CString());
				Serializer.Save("Type", "Float2");
				Serializer.Save("Default", param.f2Default);
				Serializer.Save("Step", param.fStep);
				break;

			case ParameterType::EPT_Float3:
				Serializer.Save("Name", param.strName.CString());
				Serializer.Save("Type", "Float3");
				Serializer.Save("Default", param.f3Default);
				Serializer.Save("Step", param.fStep);
				break;
			case ParameterType::EPT_Float4:
				Serializer.Save("Name", param.strName.CString());
				Serializer.Save("Type", "Float4");
				Serializer.Save("Default", param.f4Default);
				Serializer.Save("Step", param.fStep);
				break;
			case ParameterType::EPT_Texture1D:
				Serializer.Save("Name", param.strName.CString());
				Serializer.Save("Type", "Texture1D");
				Serializer.Save("Default", param.strDefault.CString());
				Serializer.Save("AddressMode", "");
				Serializer.Save("FilterMode", "");
				break;
			case ParameterType::EPT_Texture2D:
				Serializer.Save("Name", param.strName.CString());
				Serializer.Save("Type", "Texture2D");
				Serializer.Save("Default", param.strDefault.CString());
				Serializer.Save("AddressMode", "");
				Serializer.Save("FilterMode", "");
				break;
			case ParameterType::EPT_Texture3D:
				Serializer.Save("Name", param.strName.CString());
				Serializer.Save("Type", "Texture3D");
				Serializer.Save("Default", param.strDefault.CString());
				Serializer.Save("AddressMode", "");
				Serializer.Save("FilterMode", "");
				break;
			case ParameterType::EPT_TextureCube:
				Serializer.Save("Name", param.strName.CString());
				Serializer.Save("Type", "TextureCube");
				Serializer.Save("Default", param.strDefault.CString());
				Serializer.Save("AddressMode", "");
				Serializer.Save("FilterMode", "");
				break;
			}
			Serializer.Save("}");
		}

		for (UInt32 u = 0; u < (const UInt32)MaterialFunctionType::ECount; u++)
		{
			if (m_descMaterial.arrMaterialFunction[u].IsEmpty())continue;

			Serializer.Save("{");
			Serializer.Save("Function");
			switch ((MaterialFunctionType)u)
			{
			case MaterialFunctionType::ECustom:
				Serializer.Save("Type", "Custom");
				break;
			case MaterialFunctionType::EMeshParams:
				Serializer.Save("Type", "Mesh");
				break;
			case MaterialFunctionType::ESurfaceParams:
				Serializer.Save("Type", "Surface");
				break;
			}
			Serializer.Save("Code", m_descMaterial.arrMaterialFunction[u].CString());
			Serializer.Save("}");
		}

		return True;
	}
#endif

}