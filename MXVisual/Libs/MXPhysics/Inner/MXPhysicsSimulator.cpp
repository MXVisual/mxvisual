#include "MXPhysicsSimulator.h"
#include "MXLog.h"
#include "btBulletDynamicsCommon.h"
#include "PPhysicBody.h"

#include "MXMath.h"
#include "MXGraphicsResource.h"
#include "MXGraphicsRenderer.h"

namespace MXVisual
{
	GlobalVariable	GVarPhysicsDebug("PhysicsDebug", GlobalVariable::eReadWrite, False);

#if defined(DEBUG)
	struct PhysicsDebugDrawer : public btIDebugDraw
	{
		PhysicsSimulator::DebugCallbacks callbacks;
		int m_DebugMode = 0;

		void reportErrorWarning(const char* warningString)
		{
			Log::Output("PhysicsSimulator", warningString);
		}

		void drawLine(const btVector3& from, const btVector3& to, const btVector3& color) override
		{
			if (callbacks.drawLine != nullptr)
			{
				callbacks.drawLine(
					Float3(from.getX(), from.getY(), from.getZ()),
					Float3(to.getX(), to.getY(), to.getZ()),
					Float4(color.getX(), color.getY(), color.getZ(), 1.0f)
				);
			}
		}
		void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) override
		{
			//TODO!!
		}
		void draw3dText(const btVector3& location, const char* textString) override
		{
			if (callbacks.drawText3D != nullptr)
			{
				callbacks.drawText3D(
					Float3(location.getX(), location.getY(), location.getZ()),
					textString
				);
			}
		}

		void setDebugMode(int debugMode) override { m_DebugMode = debugMode; }
		int getDebugMode() const override { return m_DebugMode; }
	};
#endif

	struct SimulatorContent
	{
#if defined(DEBUG)
		PhysicsDebugDrawer			DebugDrawer;
#endif

		btDiscreteDynamicsWorld*	pWorld = nullptr;
		btBroadphaseInterface*		pBroadphase = nullptr;
		btCollisionConfiguration*	pCollisionConfiguration = nullptr;
		btCollisionDispatcher*		pDispatcher = nullptr;
		btConstraintSolver*			pSolver = nullptr;

		Array<PPhysicsBody*>		arrObjPerformContactCallback;
	};
	SimulatorContent* pSimulatorContent;

	Boolean PhysicsSimulator::Init(PhysicsSimulator::InitParam& param)
	{
		pSimulatorContent = new SimulatorContent;

		pSimulatorContent->pCollisionConfiguration = new btDefaultCollisionConfiguration();
		pSimulatorContent->pDispatcher = new btCollisionDispatcher(pSimulatorContent->pCollisionConfiguration);

		btVector3 WorldMin(-param.fWorldSise / 2, -param.fWorldSise / 2, -param.fWorldSise / 2);
		btVector3 WorldMax(param.fWorldSise / 2, param.fWorldSise / 2, param.fWorldSise / 2);
		pSimulatorContent->pBroadphase = new btAxisSweep3(WorldMin, WorldMax);

		pSimulatorContent->pSolver = new btSequentialImpulseConstraintSolver;

		pSimulatorContent->pWorld = new btDiscreteDynamicsWorld(
			pSimulatorContent->pDispatcher,
			pSimulatorContent->pBroadphase,
			pSimulatorContent->pSolver,
			pSimulatorContent->pCollisionConfiguration);
		pSimulatorContent->pWorld->setGravity(btVector3(0, -9.8f, 0));

#if defined(DEBUG)
		pSimulatorContent->DebugDrawer.setDebugMode(
			btIDebugDraw::DebugDrawModes::DBG_DrawWireframe
			| btIDebugDraw::DebugDrawModes::DBG_DrawConstraints
			| btIDebugDraw::DebugDrawModes::DBG_DrawConstraintLimits
		);
		pSimulatorContent->pWorld->setDebugDrawer(&pSimulatorContent->DebugDrawer);
#endif
		return True;
	}
	Void	PhysicsSimulator::Release()
	{
		//TODO!!
		if (pSimulatorContent->pWorld != nullptr)
		{
			delete pSimulatorContent->pWorld;
			pSimulatorContent->pWorld = nullptr;
		}
		delete pSimulatorContent;
	}

	Void	PhysicsSimulator::SetDebugCallbacks(DebugCallbacks& callbacks)
	{
#if defined(DEBUG)
		pSimulatorContent->DebugDrawer.callbacks = callbacks;
#endif
	}

	Void	PhysicsSimulator::Register(IPhysicsBody* pPhysicsBody)
	{
		PPhysicsBody* pRealPhysicsBody = dynamic_cast<PPhysicsBody*>(pPhysicsBody);
		if (pRealPhysicsBody != nullptr)
		{
			pSimulatorContent->pWorld->addRigidBody(pRealPhysicsBody->GetReal());
			pSimulatorContent->pWorld->addConstraint(pRealPhysicsBody->GetSelfConstraint());

			pSimulatorContent->arrObjPerformContactCallback.Add(pRealPhysicsBody);
		}
	}
	Void	PhysicsSimulator::Unregister(IPhysicsBody* pPhysicsBody)
	{
		PPhysicsBody* pRealPhysicsBody = dynamic_cast<PPhysicsBody*>(pPhysicsBody);
		if (pRealPhysicsBody != nullptr)
		{
			pSimulatorContent->pWorld->removeRigidBody(pRealPhysicsBody->GetReal());
			pSimulatorContent->pWorld->removeConstraint(pRealPhysicsBody->GetSelfConstraint());

			for (UInt32 u = 0; u < pSimulatorContent->arrObjPerformContactCallback.ElementCount(); u++)
			{
				if (pSimulatorContent->arrObjPerformContactCallback[u] == pRealPhysicsBody)
				{
					pSimulatorContent->arrObjPerformContactCallback.Erase(u, 1);
					break;
				}
			}
		}
	}
	Void	PhysicsSimulator::Simulate(Float1 fDeltaTime)
	{
		if (pSimulatorContent->pWorld != nullptr)
		{
			pSimulatorContent->pWorld->stepSimulation(fDeltaTime);

			PhysicsBodyContactCallBack callback;
			for (UInt32 u = 0; u < pSimulatorContent->arrObjPerformContactCallback.ElementCount(); u++)
			{
				pSimulatorContent->pWorld->contactTest(
					pSimulatorContent->arrObjPerformContactCallback[u]->GetReal(),
					callback);
			}
		}
	}
	Void	PhysicsSimulator::Render()
	{
#if defined(DEBUG)
		if (GVarPhysicsDebug.bValue)
		{
			pSimulatorContent->pWorld->debugDrawWorld();
		}
#endif
	}
}