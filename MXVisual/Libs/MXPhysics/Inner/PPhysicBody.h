#pragma once
#ifndef _P_PHYSICBODY_
#define _P_PHYSICBODY_
#include "MXPhysicsResource.h"
#include "BulletDynamics/ConstraintSolver/btGeneric6DofConstraint.h"
#include "BulletCollision/CollisionDispatch/btCollisionWorld.h"

class btRigidBody;
class btMotionState;

namespace MXVisual
{
	struct PhysicsBodyContactCallBack : public btCollisionWorld::ContactResultCallback
	{
	public:
		btScalar addSingleResult(
			btManifoldPoint & cp,
			const btCollisionObjectWrapper * colObj0Wrap,
			int partId0,
			int index0,
			const btCollisionObjectWrapper * colObj1Wrap,
			int partId1,
			int index1)
		{
			btVector3 posA = cp.getPositionWorldOnA();
			btVector3 posB = cp.getPositionWorldOnB();

			return btScalar(0.f);
		};
	};

	class PPhysicsBody : public IPhysicsBody
	{
	public:
		PPhysicsBody(UInt32 uID);
		~PPhysicsBody();

		UInt32 GetUniqueID() { return m_uUniqueID; }

		Void	SetStatic(Boolean bStatic);
		Boolean	GetStatic();

		Void	SetMass(Float1 fMass);
		Float1	GetMass();

		Void	SetFriction(Float1 fFriction);
		Float1	GetFriction();

		Void	SetRestitution(Float1 fRestitution);
		Float1	GetRestitution();

		Void	GetForceLimitations(Float3& f3Min, Float3& f3Max) override;
		Void	GetTorqueLimitations(Float3& f3Min, Float3& f3Max) override;

		Void	SetForceLimitations(const Float3& f3Min, const Float3& f3Max) override;
		Void	SetTorqueLimitations(const Float3& f3Min, const Float3& f3Max) override;

		Void	SetTransform(const Quaternion& f3GlobalQuater, const Float3& f3GlobalPos) override;
		Matrix4	GetTransform() override;

		Void	AddForce(const Float3& force) override;
		
		Float3	GetVelocity() const override;

		Void	SetContactCallback() override;
		Void	SetCollision(ICollisionShape* pShape) override;

		Void	SetDebugShow(Boolean bShow) override;

		btRigidBody*		GetReal() { return m_pRigidBody; }
		btTypedConstraint*	GetSelfConstraint() { return m_pSelfConstraint; }
		
	protected:
		Void UpdateFlags();

	protected:
		UInt32 m_uUniqueID;
		Boolean m_bStatic;
		btRigidBody*		m_pRigidBody;
		btMotionState*		m_pMotionState;
		btGeneric6DofConstraint* m_pSelfConstraint;
	};
}

#endif