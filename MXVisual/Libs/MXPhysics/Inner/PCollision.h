#pragma once
#ifndef _P_COLLISION_
#define _P_COLLISION_
#include "MXPhysicsResource.h"
#include "btBulletCollisionCommon.h"
#include "BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"

namespace MXVisual
{
	class PCollision
	{
	public:
		virtual btCollisionShape*		GetReal() = 0;
	};

	class PCollisionSphere : public PCollision, public ICollisionSphere
	{
	public:
		PCollisionSphere(UInt32 uID);
		~PCollisionSphere();

		UInt32 GetUniqueID() { return m_uUniqueID; }

		Void SetRadius(Float1 fRadius);
		Float1 GetRadius();

		CollisionShapeType GetShapeType() override { return ICollisionShape::ESphere; }

		btCollisionShape*		GetReal() { return m_pShape; }

	protected:
		UInt32 m_uUniqueID;
		btSphereShape*		m_pShape;
	};

	class PCollisionBox : public PCollision, public ICollisionBox
	{
	public:
		PCollisionBox(UInt32 uID);
		~PCollisionBox();

		UInt32 GetUniqueID() { return m_uUniqueID; }

		Void SetScale(const Float3& f3Scale);
		Float3 GetScale();

		CollisionShapeType GetShapeType() override { return ICollisionShape::EBox; }

		btCollisionShape*		GetReal() { return m_pShape; }

	protected:
		UInt32 m_uUniqueID;
		btBoxShape*		m_pShape;
	};

	class PCollisionMesh : public PCollision, public ICollisionMesh
	{
	public:
		PCollisionMesh(UInt32 uID);
		~PCollisionMesh();

		UInt32	GetUniqueID() { return m_uUniqueID; }

		Void	SetTriangleData(Array<Float3>& position);

		Void	GetTriangleDataOptimized(Array<Float3>& position);

		CollisionShapeType GetShapeType() override { return ICollisionShape::EMesh; }

		btCollisionShape*		GetReal() { return m_pShape; }

	protected:
		UInt32 m_uUniqueID;
		btConvexHullShape*		m_pShape;
	};

	class PCollisionCapsule : public PCollision, public ICollisionCapsule
	{
	public:
		PCollisionCapsule(UInt32 uID, Float1 fHeight, Float1 fRadius);
		~PCollisionCapsule();

		Float1 GetHeight() override;
		Float1 GetRadius() override;

		UInt32	GetUniqueID() { return m_uUniqueID; }

		CollisionShapeType GetShapeType() override { return ICollisionShape::ECapsule; }

		btCollisionShape*		GetReal() { return m_pShape; }

	protected:
		UInt32 m_uUniqueID;
		btCapsuleShape* m_pShape;
	};

	class PCollisionTerrain : public PCollision, public ICollisionTerrain
	{
	public:
		PCollisionTerrain(UInt32 uID);
		~PCollisionTerrain();

		UInt32	GetUniqueID() { return m_uUniqueID; }

		Void SetScale(const Float3& f3Scale) override;
		Float3 GetScale() override;
		Void SetTerrainHeightData(UInt32 uWidth, UInt32 uHeight, Float1 fHeightScale, const Float1* pData) override;

		CollisionShapeType GetShapeType() override { return ICollisionShape::ETerrain; }

		btCollisionShape*		GetReal() { return m_pShape; }

	protected:
		UInt32 m_uUniqueID;
		btHeightfieldTerrainShape*	m_pShape;
		Array<Float1>				m_aHeightData;
	};
}

#endif