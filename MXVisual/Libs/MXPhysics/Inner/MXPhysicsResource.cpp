#include "MXPhysicsSimulator.h"
#include "MXLog.h"
#include <vector>
#include "btBulletDynamicsCommon.h"

#include "PPhysicBody.h"
#include "PCollision.h"

namespace MXVisual
{
	UInt32 g_uResourceID = 0;

	std::vector<IPhysicsResource*> arrPhysicsResources;
	Void	DeleteResource(IPhysicsResource* pResource)
	{
		for (auto it = arrPhysicsResources.begin(); it != arrPhysicsResources.end(); it++)
		{
			if (*it == pResource)
			{
				arrPhysicsResources.erase(it);
			}
		}
		delete pResource;
	}

	Void IPhysicsResource::Release()
	{
		m_nRefCount--;
		if (m_nRefCount <= 0)
		{
			DeleteResource(this);
		}
	}

	IPhysicsBody*	PhysicsResourceManager::CreatePhysicsBody()
	{
		IPhysicsBody* pRes = new PPhysicsBody(g_uResourceID);

		if (pRes != nullptr)
		{
			g_uResourceID++;
		}

		return pRes;
	}
	ICollisionShape* PhysicsResourceManager::CreateCollisionShape(ICollisionShape::Description& desc)
	{
		ICollisionShape* pRes = nullptr;

		switch (desc.eType)
		{
		case ICollisionShape::EBox:
			pRes = new PCollisionBox(g_uResourceID);
			break;
		case ICollisionShape::ESphere:
			pRes = new PCollisionSphere(g_uResourceID);
			break;
		case ICollisionShape::EMesh:
			pRes = new PCollisionMesh(g_uResourceID);
			break;
		case ICollisionShape::ETerrain:
			pRes = new PCollisionTerrain(g_uResourceID);
			break;
		case ICollisionShape::ECapsule:
			pRes = new PCollisionCapsule(g_uResourceID, desc.fCapsuleHeight, desc.fCapsuleRadius);
			break;
		default:break;
		}

		if (pRes != nullptr)
		{
			g_uResourceID++;
		}

		return pRes;
	}
}