#include "PCollision.h"

namespace MXVisual
{
	PCollisionSphere::PCollisionSphere(UInt32 uID)
		: m_pShape(nullptr)
		, m_uUniqueID(uID)
	{
		m_pShape = new btSphereShape(1);
	}
	PCollisionSphere::~PCollisionSphere()
	{
		delete m_pShape;
	}

	Void PCollisionSphere::SetRadius(Float1 fRadius)
	{
		m_pShape->setUnscaledRadius(fRadius);
	}
	Float1 PCollisionSphere::GetRadius()
	{
		return m_pShape->getRadius();
	}

	PCollisionBox::PCollisionBox(UInt32 uID)
		: m_pShape(nullptr)
		, m_uUniqueID(uID)
	{
		m_pShape = new btBoxShape(btVector3(1, 1, 1));
	}
	PCollisionBox::~PCollisionBox()
	{
		delete m_pShape;
	}
	Void PCollisionBox::SetScale(const Float3& f3Scale)
	{
		m_pShape->setLocalScaling(btVector3(f3Scale[0], f3Scale[1], f3Scale[2]));
	}
	Float3 PCollisionBox::GetScale()
	{
		btVector3 v = m_pShape->getLocalScaling();
		return Float3(v.x(), v.y(), v.z());
	}

	PCollisionMesh::PCollisionMesh(UInt32 uID)
		: m_pShape(nullptr)
		, m_uUniqueID(uID)
	{
		m_pShape = new btConvexHullShape();
	}
	PCollisionMesh::~PCollisionMesh()
	{
		delete m_pShape;
	}

	Void PCollisionMesh::SetTriangleData(Array<Float3>& position)
	{
		delete m_pShape;
		m_pShape = new btConvexHullShape(
			(const btScalar*)position.GetRawData(),
			position.ElementCount(),
			sizeof(Float3));
		m_pShape->initializePolyhedralFeatures();
		m_pShape->optimizeConvexHull();
	}

	Void PCollisionMesh::GetTriangleDataOptimized(Array<Float3>& position)
	{
		for (UInt32 u = 0;
			u < m_pShape->getNumVertices(); u++)
		{
			btVector3 a;
			m_pShape->getVertex(u, a);
			position.Add(Float3(a.x(), a.y(), a.z()));
		}
	}

	PCollisionTerrain::PCollisionTerrain(UInt32 uID)
		: m_pShape(nullptr)
		, m_uUniqueID(uID)
	{
	}
	PCollisionTerrain::~PCollisionTerrain()
	{
		delete m_pShape;
	}

	Void PCollisionTerrain::SetScale(const Float3& f3Scale)
	{
		if (m_pShape != nullptr)
		{
			m_pShape->setLocalScaling(btVector3(f3Scale[0], f3Scale[1], f3Scale[2]));
		}
	}
	Float3 PCollisionTerrain::GetScale()
	{
		if (m_pShape != nullptr)
		{
			btVector3 v = m_pShape->getLocalScaling();
			return Float3(v.x(), v.y(), v.z());
		}

		return Float3();
	}
	Void PCollisionTerrain::SetTerrainHeightData(UInt32 uWidth, UInt32 uHeight, Float1 fHeightScale, const Float1* pData)
	{
		if (m_pShape != nullptr)
			delete m_pShape;

		m_aHeightData.Clear();
		for (SInt32 u = 0; u < uWidth * uHeight; u++)
		{
			m_aHeightData.Add(pData[u]);
		}

		m_pShape = new btHeightfieldTerrainShape(uWidth, uHeight, m_aHeightData.GetRawData(),
			fHeightScale, -1.0f, 1.0f, 1, PHY_FLOAT, false);
		m_pShape->setUseDiamondSubdivision();
		m_pShape->buildAccelerator();
	}

	PCollisionCapsule::PCollisionCapsule(UInt32 uID, Float1 fHeight, Float1 fRadius)
		: m_pShape(nullptr)
		, m_uUniqueID(uID)
	{
		m_pShape = new btCapsuleShape(fRadius, fHeight);
	}
	PCollisionCapsule::~PCollisionCapsule()
	{
		delete m_pShape;
	}
	Float1 PCollisionCapsule::GetHeight()
	{
		return m_pShape->getHalfHeight() * 2.0f;
	}
	Float1 PCollisionCapsule::GetRadius()
	{
		return m_pShape->getRadius();
	}

}
