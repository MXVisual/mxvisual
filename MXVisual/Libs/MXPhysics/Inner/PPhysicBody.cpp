#include "PPhysicBody.h"
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"
#include "PCollision.h"
#include "MXMath.h"
#include "MXPhysicsSimulator.h"

namespace MXVisual
{
	PPhysicsBody::PPhysicsBody(UInt32 uID)
		: m_uUniqueID(uID)
		, m_pMotionState(nullptr)
		, m_pRigidBody(nullptr)
		, m_bStatic(True)
	{
		m_pMotionState = new btDefaultMotionState;
		m_pRigidBody = new btRigidBody(0, m_pMotionState, nullptr);
		m_pSelfConstraint = new btGeneric6DofConstraint(*m_pRigidBody, btTransform::getIdentity(), true);
		m_pSelfConstraint->setDbgDrawSize(4.0f);

		//Ĭ�ϲ�����
		m_pSelfConstraint->setLinearLowerLimit(btVector3(1, 1, 1));
		m_pSelfConstraint->setLinearUpperLimit(btVector3(-1, -1, -1));

		m_pSelfConstraint->setAngularLowerLimit(btVector3(1, 1, 1));
		m_pSelfConstraint->setAngularUpperLimit(btVector3(-1, -1, -1));

		SetDebugShow(False);
	}
	PPhysicsBody::~PPhysicsBody()
	{
		PhysicsSimulator::Unregister(this);

		delete m_pRigidBody;
		delete m_pMotionState;
	}

	Void PPhysicsBody::UpdateFlags()
	{
		int collisionflags = m_pRigidBody->getCollisionFlags();

		collisionflags &= ~(btCollisionObject::CF_KINEMATIC_OBJECT | btCollisionObject::CF_STATIC_OBJECT);
		if (m_bStatic)//staic object
		{
			collisionflags |= btCollisionObject::CF_STATIC_OBJECT;

			m_pRigidBody->setCollisionFlags(collisionflags);
			m_pRigidBody->setActivationState(WANTS_DEACTIVATION);
		}
		else if (m_pRigidBody->getMass() > Constant_MIN_FLOAT)//dynamic object
		{
			m_pRigidBody->setCollisionFlags(collisionflags);
			m_pRigidBody->setActivationState(DISABLE_DEACTIVATION);
		}
		else//kinematic object
		{
			collisionflags |= btCollisionObject::CF_KINEMATIC_OBJECT;

			m_pRigidBody->setCollisionFlags(btCollisionObject::CF_KINEMATIC_OBJECT);
			m_pRigidBody->setActivationState(DISABLE_DEACTIVATION);
		}
	}

	Void	PPhysicsBody::SetStatic(Boolean bStatic)
	{
		m_bStatic = bStatic;
		UpdateFlags();
	}
	Boolean	PPhysicsBody::GetStatic()
	{
		return m_bStatic;
	}

	Void	PPhysicsBody::SetMass(Float1 fMass)
	{
		btVector3 inertia(0, 0, 0);
		if (m_pRigidBody->getCollisionShape() != nullptr)
		{
			m_pRigidBody->getCollisionShape()->calculateLocalInertia(fMass, inertia);
		}
		m_pRigidBody->setMassProps(fMass, inertia);

		UpdateFlags();
	}
	Float1	PPhysicsBody::GetMass()
	{
		return m_pRigidBody->getMass();
	}

	Void	PPhysicsBody::SetFriction(Float1 fFriction)
	{
		m_pRigidBody->setFriction(fFriction);
	}
	Float1	PPhysicsBody::GetFriction()
	{
		return m_pRigidBody->getFriction();
	}

	Void	PPhysicsBody::SetRestitution(Float1 fRestitution)
	{
		m_pRigidBody->setRestitution(fRestitution);
	}
	Float1	PPhysicsBody::GetRestitution()
	{
		return m_pRigidBody->getRestitution();
	}

	Void	PPhysicsBody::GetForceLimitations(Float3& f3Min, Float3& f3Max)
	{
		btVector3 v;
		m_pSelfConstraint->getLinearLowerLimit(v);
		f3Min = Float3(v.x(), v.y(), v.z());

		m_pSelfConstraint->getLinearUpperLimit(v);
		f3Max = Float3(v.x(), v.y(), v.z());
	}
	Void	PPhysicsBody::GetTorqueLimitations(Float3& f3Min, Float3& f3Max)
	{
		btVector3 v;
		m_pSelfConstraint->getAngularLowerLimit(v);
		f3Min = Float3(v.x(), v.y(), v.z());

		m_pSelfConstraint->getAngularUpperLimit(v);
		f3Max = Float3(v.x(), v.y(), v.z());
	}

	Void	PPhysicsBody::SetForceLimitations(const Float3& f3Min, const Float3& f3Max)
	{
		m_pSelfConstraint->setLinearLowerLimit(btVector3(f3Min[0], f3Min[1], f3Min[2]));
		m_pSelfConstraint->setLinearUpperLimit(btVector3(f3Max[0], f3Max[1], f3Max[2]));
	}

	Void	PPhysicsBody::SetTorqueLimitations(const Float3& f3Min, const Float3& f3Max)
	{
		m_pSelfConstraint->setAngularLowerLimit(btVector3(f3Min[0], f3Min[1], f3Min[2]));
		m_pSelfConstraint->setAngularUpperLimit(btVector3(f3Max[0], f3Max[1], f3Max[2]));
	}

	Void	PPhysicsBody::SetTransform(const Quaternion& f3GlobalQuater, const Float3& f3GlobalPos)
	{
		btTransform t = btTransform(btQuaternion(f3GlobalQuater[0], f3GlobalQuater[1], f3GlobalQuater[2], f3GlobalQuater[3]),
			btVector3(f3GlobalPos[0], f3GlobalPos[1], f3GlobalPos[2]));
		m_pRigidBody->setWorldTransform(t);
	}
	Matrix4	PPhysicsBody::GetTransform()
	{
		btTransform transform = m_pRigidBody->getWorldTransform();

		btVector3 t = transform.getOrigin();
		btQuaternion q = transform.getRotation();

		Matrix4 mt = Math::MatrixTranslation(Float3(t.x(), t.y(), t.z()));
		Matrix4 mq = Math::MatrixQuaternion(Quaternion(q.x(), q.y(), q.z(), q.w()));

		return mq * mt;
	}

	Void	PPhysicsBody::AddForce(const Float3& force)
	{
		m_pRigidBody->applyCentralForce(btVector3(force[0], force[1], force[2]));
	}

	Float3	PPhysicsBody::GetVelocity() const
	{
		btVector3 v = m_pRigidBody->getLinearVelocity();
		return Float3(v.x(), v.y(), v.z());
	}

	Void	PPhysicsBody::SetDebugShow(Boolean bShow)
	{
		int collisionflags = m_pRigidBody->getCollisionFlags();

		if (bShow)
			collisionflags &= ~btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT;
		else
			collisionflags |= btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT;

		m_pRigidBody->setCollisionFlags(collisionflags);
	}

	Void	PPhysicsBody::SetContactCallback()
	{

	}

	Void 	PPhysicsBody::SetCollision(ICollisionShape* pShape)
	{
		PCollision* pReal = dynamic_cast<PCollision*>(pShape);
		if (pReal != nullptr)
		{
			m_pRigidBody->setCollisionShape(pReal->GetReal());
		}
	}
}