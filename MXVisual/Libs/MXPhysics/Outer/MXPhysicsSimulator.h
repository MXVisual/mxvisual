#pragma once
#ifndef _MX_PHYSICS_
#define _MX_PHYSICS_
#include "MXCommon.h"
#include "MXPhysicsResource.h"
#include "MXGlobalVariable.h"

namespace MXVisual
{
	extern  GlobalVariable	GVarPhysicsDebug;

	struct PhysicsSimulator
	{
		struct InitParam
		{
			Float1 fWorldSise = 10000.0f;
		};

		static Boolean Init(InitParam& param);
		static Void	Release();

		static Void	Register(IPhysicsBody* pPhysicsBody);
		static Void	Unregister(IPhysicsBody* pPhysicsBody);
		static Void	Simulate(Float1 fDeltaTime);

		//For Debug Visualized
		static Void Render();

		typedef Void(*FDrawLine)(const Float3&, const Float3&, const Float4& color);
		typedef Void(*FDrawText3D)(const Float3&, const Char*);

		struct DebugCallbacks
		{
			FDrawLine drawLine = nullptr;
			FDrawText3D drawText3D = nullptr;
		};
		static Void SetDebugCallbacks(DebugCallbacks& callbacks);
	};
}
#endif