#pragma once
#ifndef _MX_P_RESOURCE_
#define _MX_P_RESOURCE_
#include "MXPlatform.h"
#include "MXInterface.h"
#include "MXVector.h"
#include "MXArray.h"
#include "MXMatrix.h"
#include "MXQuaternion.h"

namespace MXVisual
{
	class IPhysicsResource : public IInterface
	{
	public:
		virtual ~IPhysicsResource() {};

		Void Release();

		virtual UInt32 GetUniqueID() = 0;
	};

	class ICollisionShape : public IPhysicsResource
	{
	public:
		enum CollisionShapeType
		{
			EBox,
			ESphere,
			EMesh,
			ECapsule,
			ETerrain,
			ECollisionShapeTypeCount,
			ECollisionShapeTypeInvalid = ECollisionShapeTypeCount
		};

		struct Description
		{
			CollisionShapeType eType = ECollisionShapeTypeInvalid;
			union
			{
				struct
				{
					Float1 fCapsuleRadius;
					Float1 fCapsuleHeight;
				};
			};
		};

		virtual CollisionShapeType GetShapeType() = 0;
	};

	class IPhysicsBody : public IPhysicsResource
	{
	public:
		virtual Void	SetStatic(Boolean bStatic) = 0;
		virtual Boolean	GetStatic() = 0;

		virtual Void	SetMass(Float1 fMass) = 0;
		virtual Float1	GetMass() = 0;

		virtual	Void	SetFriction(Float1 fFriction) = 0;
		virtual	Float1	GetFriction() = 0;

		virtual	Void	SetRestitution(Float1 fRestitution) = 0;
		virtual	Float1	GetRestitution() = 0;

		virtual	Void	GetForceLimitations(Float3& f3Min, Float3& f3Max) = 0;
		virtual Void	GetTorqueLimitations(Float3& f3Min, Float3& f3Max) = 0;

		virtual	Void	SetForceLimitations(const Float3& f3Min, const Float3& f3Max) = 0;
		virtual Void	SetTorqueLimitations(const Float3& f3Min, const Float3& f3Max) = 0;

		virtual Void	SetTransform(const Quaternion& f3GlobalQuater, const Float3& f3GlobalPos) = 0;
		virtual Matrix4	GetTransform() = 0;
		
		virtual Void	AddForce(const Float3& force) = 0;
		virtual Float3	GetVelocity() const = 0;

		virtual Void	SetContactCallback() = 0;
		virtual Void	SetCollision(ICollisionShape* pShape) = 0;

		virtual Void	SetDebugShow(Boolean bShow) = 0;
	};

	class ICollisionSphere : public ICollisionShape
	{
	public:
		virtual Void SetRadius(Float1 fRadius) = 0;
		virtual Float1 GetRadius() = 0;
	};

	class ICollisionBox : public ICollisionShape
	{
	public:
		virtual Void SetScale(const Float3& f3Scale) = 0;
		virtual Float3 GetScale() = 0;
	};

	class ICollisionCapsule : public ICollisionShape
	{
	public:
		virtual Float1 GetHeight() = 0;
		virtual Float1 GetRadius() = 0;
	};

	class ICollisionMesh : public ICollisionShape
	{
	public:
		virtual Void SetTriangleData(Array<Float3>& position) = 0;
		virtual	Void GetTriangleDataOptimized(Array<Float3>& position) = 0;
	};

	class ICollisionTerrain : public ICollisionShape
	{
	public:
		virtual Void SetScale(const Float3& f3Scale) = 0;
		virtual Float3 GetScale() = 0;
		virtual Void SetTerrainHeightData(UInt32 uWidth, UInt32 uHeight, Float1 fHeightScale, const Float1* pData) = 0;
	};

	struct PhysicsResourceManager
	{
		static IPhysicsBody*	CreatePhysicsBody();
		static ICollisionShape* CreateCollisionShape(ICollisionShape::Description& desc);
	};
}
#endif