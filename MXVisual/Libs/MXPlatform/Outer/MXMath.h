#pragma once
#ifndef _MX_MATH_
#define _MX_MATH_
#include "MXPlatform.h"
#include "MXConstant.h"
#include "MXMatrix.h"
#include "MXVector.h"
#include "MXQuaternion.h"

namespace MXVisual
{
	namespace Math
	{
		extern const Float3 AXIS_X;
		extern const Float3 AXIS_Y;
		extern const Float3 AXIS_Z;

		extern const Float3 AXIS_X_Inverse;
		extern const Float3 AXIS_Y_Inverse;
		extern const Float3 AXIS_Z_Inverse;

		inline Float1 Abs(Float1 fValue)
		{
			return fValue > 0 ? fValue : -fValue;
		}
		inline SInt32 Ceil(Float1 fValue)
		{
			SInt32 nRes = (SInt32)fValue;
			nRes = (fValue - nRes) > 0 ? (nRes + 1) : nRes;
			return nRes;
		}
		inline SInt32 Floor(Float1 fValue)
		{
			SInt32 nRes = (SInt32)fValue;
			return nRes;
		}

		Float1 Power(Float1 fValue, Float1 fRate);

		UInt32 AlignmentedSize(UInt32 uOrgSize, UInt32 uAlignSize);

		template<typename T>
		T Min(const T& fA, const T& fB)
		{
			return fA > fB ? fB : fA;
		}
		template<typename T>
		T Max(const T& fA, const T& fB)
		{
			return fA > fB ? fA : fB;
		}
		template<typename T>
		T Clamp(const T& fA, const T& fMin, const T& fMax)
		{
			return Max(Min(fA, fMax), fMin);
		}
		template<typename T>
		T Lerp(const T& fA, const T& fB, Float1 fT)
		{
			return fA * (1 - fT) + fB * fT;
		}

		template<>	SInt32 Min(const SInt32& nA, const SInt32& nB);
		template<>	SInt32 Max(const SInt32& nA, const SInt32& nB);
		template<>	SInt32 Clamp(const SInt32& nA, const SInt32& nMin, const SInt32& nMax);

		template<>	Float1 Min(const Float1& fA, const Float1& fB);
		template<>	Float1 Max(const Float1& fA, const Float1& fB);
		template<>	Float1 Clamp(const Float1& fA, const Float1& fMin, const Float1& fMax);
		template<>	Float1 Lerp(const Float1& fA, const Float1& fB, Float1 fT);

		template<>	Float2 Min(const Float2& f2A, const Float2& f2B);
		template<>	Float2 Max(const Float2& f2A, const Float2& f2B);
		template<>	Float2 Clamp(const Float2& f2A, const Float2& f2Min, const Float2& f2Max);
		template<>	Float2 Lerp(const Float2& f2A, const Float2& f2B, Float1 fT);

		template<>	Float3 Min(const Float3& f2A, const Float3& f2B);
		template<>	Float3 Max(const Float3& f2A, const Float3& f2B);
		template<>	Float3 Clamp(const Float3& f2A, const Float3& f2Min, const Float3& f2Max);
		template<>	Float3 Lerp(const Float3& f2A, const Float3& f2B, Float1 fT);

		template<>	Float4 Min(const Float4& f2A, const Float4& f2B);
		template<>	Float4 Max(const Float4& f2A, const Float4& f2B);
		template<>	Float4 Clamp(const Float4& f2A, const Float4& f2Min, const Float4& f2Max);
		template<>	Float4 Lerp(const Float4& f2A, const Float4& f2B, Float1 fT);

		Quaternion	SLerp(const Quaternion& qA, const Quaternion& qB, Float1 fT);

		inline Float1 Degree2Radian(Float1 fDegree) { return fDegree / 180.0f * Constant_PI; }
		inline Float1 Radian2Degree(Float1 fDadian) { return fDadian / Constant_PI * 180.0f; }

		inline Float3 Split(const Float4& f4Value)
		{
			return Float3(f4Value[0], f4Value[1], f4Value[2]);
		}
		inline Float4 Combine(const Float3& f3A, const Float1& fB)
		{
			return Float4(f3A[0], f3A[1], f3A[2], fB);
		}

		inline Matrix3 Split(const Matrix4& mat)
		{
			return Matrix3(
				mat.Get(0, 0), mat.Get(0, 1), mat.Get(0, 2),
				mat.Get(1, 0), mat.Get(1, 1), mat.Get(1, 2),
				mat.Get(2, 0), mat.Get(2, 1), mat.Get(2, 2)
			);
		}

		Float1 Gaussian(Float1 fPos, Float1 fMax, Float1 fCenter, Float1 fStandardDeviation);
		Float1 Gaussian(const Float2& f2Pos, Float1 fMax, const Float2& f2Center, const Float2& f2StandardDeviation);

		Float1 Random01();

		Matrix4 MatrixTranspose(const Matrix4& mat);
		Matrix4 MatrixInverse(const Matrix4& mat);

		Matrix4 MatrixClipToViewport(
			const Float2& offset,
			const Float2& size,
			const Float2& depthRange);

		Matrix4 MatrixTranslation(const Float3& f3Offset);
		Matrix4 MatrixScale(const Float3& f3Scale);
		Matrix4 MatrixAxisRotation(const Float3& f3Axis, Float1 fRadius);
		Matrix4 MatrixQuaternion(const Quaternion& quaternion);

		Float3  QuaternionToEular(const Quaternion& quaternion);
		Quaternion  EularToQuaternion(const Float3& f3Eular);
		Quaternion	VectorToVectorQuaternion(const Float3& f3Vec0, const Float3& f3Vec1);

		Float1 VectorToVectorAngle(const Float3& f3Vec0, const Float3& f3Vec1);

		//Reverse Z fNear �� fFar��������
		Matrix4 MatrixPerspectiveProjection(Float1 fFovW, Float1 fAspectWH, Float1 fNear, Float1 fFar);
		Matrix4 MatrixOrthographicProjection(Float1 fWidth, Float1 fHeight, Float1 fNear, Float1 fFar);

		Matrix4 MatrixView(const Float3& f3Pos, const Float3& f3Forward, const Float3& f3Up);
		Float3  MatrixGetCameraPosition(const Matrix4& mat);

		Float3 MatrixTransform(const Float3& f3A, const Matrix3& mat3B);
		Float4 MatrixTransform(const Float4& f4A, const Matrix4& mat4B);

		Float3 MatrixGetRotation(const Matrix3& mat);
		Float3 MatrixGetScale(const Matrix3& mat);

		Float3 MatrixGetTranslation(const Matrix4& mat);
		Float3 MatrixGetRotation(const Matrix4& mat);
		Quaternion MatrixGetQuaternion(const Matrix4& mat);
		Float3 MatrixGetScale(const Matrix4& mat);

		Float3 CrossProduct(const Float3& f3A, const Float3& f3B);
		Float1 DotProduct(const Float3& f3A, const Float3& f3B);
		
		Float1 DotProduct(const Float2& f2A, const Float2& f2B);

		Float1 Length(const Float2& f2Value);
		Float1 Length(const Float3& f3Value);
		Float1 Length(const Float4& f4Value);

		Float2 Normalize(const Float2& f2Value);
		Float3 Normalize(const Float3& f3Value);
		Float4 Normalize(const Float4& f4Value);

		Float3 ComputeNormal(const Float3& v0, const Float3& v1, const Float3& v2);
		Float4 ComputeTangent(const Float3& v0, const Float3& v1, const Float3& v2,
			const Float2&  uv0, const Float2& uv1, const Float2& uv2);

		Void	FFT_Transform(const Float1 dataTimeDomain[], Float1 dataFrequencyDomain[], UInt32 dataCount);

		struct MD5
		{
			UInt32 ID[4];
			MD5()
			{
				ID[0] = 0;
				ID[1] = 0;
				ID[2] = 0;
				ID[3] = 0;
			}
			Boolean operator==(const MD5& other) const
			{
				return ID[0] == other.ID[0] &&
					ID[1] == other.ID[1] &&
					ID[2] == other.ID[2] &&
					ID[3] == other.ID[3];
			}
		};
		MD5	CalculateMD5_128(const Char* szString);
	}
}
#endif