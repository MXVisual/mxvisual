#pragma once
#ifndef _MX_INTERFACE_COMMON_
#define _MX_INTERFACE_COMMON_
#include "MXPlatform.h"

namespace MXVisual
{
	class IInterface
	{
	public:
		IInterface() { m_nRefCount = 1; }
		virtual ~IInterface() {};

		virtual Void	Release() = 0;

		void AddRef() { m_nRefCount++; }
		SInt32 RefCount() { return m_nRefCount; }

	protected:
		SInt32 m_nRefCount;
	};

#define MX_RELEASE_INTERFACE(object) \
		if(object != nullptr)\
		{\
			(object)->Release();\
			(object) = nullptr;\
		}
}
#endif
