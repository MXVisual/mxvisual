#pragma once
#ifndef _MX_PLATFORM_
#define _MX_PLATFORM_

#define PLATFORM_WINDOWS 0

#define PLATFORM_UNKNOWN 1

#if defined(WINDOWS) 
#define PLATFORM PLATFORM_WINDOWS
#else
#define PLATFORM PLATFORM_UNKNOWN
#endif

#if PLATFORM == PLATFORM_UNKNOWN
#error "Unsupported Platform"
#endif

#if defined(DEBUG)
#include "assert.h"
#define ASSERT_IF_FAILED(hr) assert(hr)
#define ASSERT_IF_FAILED_STATIC(hr, msg) static_assert(hr, msg)
#else
#define ASSERT_IF_FAILED(hr)
#define ASSERT_IF_FAILED_STATIC(hr) 
#endif

namespace MXVisual
{
	typedef float				Float1;

	typedef double				Double1;

	typedef void				Void;
	typedef char				Char;

	typedef	bool				Boolean;

	typedef char				SInt8;
	typedef short				SInt16;
	typedef int					SInt32;
	typedef long long			SInt64;

	typedef unsigned char		UInt8;
	typedef unsigned short		UInt16;
	typedef unsigned int		UInt32;
	typedef unsigned long long	UInt64;

	struct Region
	{
		UInt32 uLeft;
		UInt32 uTop;
		UInt32 uRight;
		UInt32 uBottom;

		Region();
		Boolean operator==(const Region& other);
	};

	const Boolean False = false;
	const Boolean True = true;

#define DLL_IMPORT extern "C" __declspec(dllimport)
#define DLL_EXPORT extern "C" __declspec(dllexport)

#define MX_RELEASE_INTERFACE(object) \
	if(object) { (object)->Release(); (object) = nullptr; }

#define MX_DELETE(object) \
	if (object) { delete object; object = nullptr; }

#define MX_DELETE_ARRAY(object) \
	if (object) { delete[] object; object = nullptr; }


	typedef Boolean(*FBooleanCallBack)();
	typedef Void(*FVoidCallBack)();
	typedef Void(*FIMECallBack)(UInt8 uCharSize, const Char* c);

	//MAX 256
	Void GetApplicationPath(Char* szBuffer);
	Void GetWorkingPath(Char* szBuffer);

	namespace Application
	{
		extern	FBooleanCallBack	FOnInit;
		extern	FVoidCallBack		FOnRelease;
		extern	FVoidCallBack		FOnLoop;
		extern	FIMECallBack		FOnIME;

		UInt32	Run();
	};
}

#endif