#pragma once
#ifndef _MX_LOG_
#define _MX_LOG_
#include "MXPlatform.h"

namespace MXVisual
{
	namespace Log
	{
		class IOutputCallback
		{
		public:
			virtual Void Print(const Char* szLogMessage) = 0;
		};

		extern IOutputCallback* g_pDefaultOutputCallback;

		Void SetOutputCallback(IOutputCallback* pCallback);

		Void Output(const Char* szSpeaker, const Char* szMessage);
	}
}

#endif