#pragma once
#ifndef _MX_WINDOW_
#define _MX_WINDOW_
#include "MXPlatform.h"
#include "MXVector.h"
#include "MXString.h"

namespace MXVisual
{
	class Window
	{
	public:
		Window(const Char* szName, Window* pParent = nullptr);
		~Window();

		Window* GetParent() const { return m_pParent; }
		const Char* GetName() const { return m_strName.CString(); }

		Float2 ScreenToWindow(const Float2& ScreenPos) const;

		const UInt32 GetWidth() const { return m_uWidth; }
		const UInt32 GetHeight()const { return m_uHeight; }
		Boolean	IsActived() const { return m_bActived; }

		Void SetPosition(UInt32 uX, UInt32 uY);
		Void Resize(UInt32 uWidth, UInt32 uHeight);

		Void Show(Boolean bShow);
		Void ShowFramework(Boolean bShow);

	protected:
		String m_strName;
		Window* m_pParent;
		UInt64 m_hWnd;
		Boolean m_bActived;
		UInt32 m_uWidth;
		UInt32 m_uHeight;

	protected:
		Window() {};

		Window(const Window&) = delete;
		Window& operator=(const Window&) = delete;
		Window(Window&&) = delete;
		Window& operator=(Window&&) = delete;

		static UInt64 CreateRealWindow(const Char * szTitle,
			UInt32 uX, UInt32 uY, UInt32 uWidth, UInt32 uHeight,
			Window* pWnd, Window* pParent);
	};

}
#endif