#pragma once
#ifndef _MX_INPUT_
#define _MX_INPUT_
#include "MXPlatform.h"
#include "MXVector.h"

namespace MXVisual
{
	namespace Input
	{
		enum Key
		{
			eLButton,
			eMButton,
			eRButton,

			eLCtrl,
			eRCtrl,
			eLShift,
			eRShift,
			eLAlt,
			eRAlt,

			eTab,
			eSpace,
			eBackSpace,
			eEnter,
			eEscape,
			eDelete,
			eNumLock,
			eCapsLock,

			eUp,
			eDown,
			eLeft,
			eRight,

			ePageUp,
			ePageDown,
			ePageHome,
			ePageEnd,

			eF1,
			eF2,
			eF3,
			eF4,
			eF5,
			eF6,
			eF7,
			eF8,
			eF9,
			eF10,
			eF11,
			eF12,

			eQ,
			eW,
			eE,
			eR,
			eT,
			eY,
			eU,
			eI,
			eO,
			eP,
			eA,
			eS,
			eD,
			eF,
			eG,
			eH,
			eJ,
			eK,
			eL,
			eZ,
			eX,
			eC,
			eV,
			eB,
			eN,
			eM,

			e1,
			e2,
			e3,
			e4,
			e5,
			e6,
			e7,
			e8,
			e9,
			e0,

			eNum0,
			eNum1,
			eNum2,
			eNum3,
			eNum4,
			eNum5,
			eNum6,
			eNum7,
			eNum8,
			eNum9,
		};

		enum JoystickKey
		{
			eJoyA,
			eJoyB,
			eJoyX,
			eJoyY,

			eJoyLB,
			eJoyRB,

			eJoyBack,
			eJoyMenu,

			eJoyUp,
			eJoyDown,
			eJoyLeft,
			eJoyRight,

			eJoyStickL,
			eJoyStickR,
		};

		Boolean	GetKeyDown(Key eKey);
		Boolean	GetKeyUp(Key eKey);
		Boolean	GetKeyClick(Key eKey);
		Boolean	GetKeyDbClick(Key eKey);

		Boolean	GetKeyDown(JoystickKey eKey);
		Boolean	GetKeyUp(JoystickKey eKey);
		Boolean	GetKeyClick(JoystickKey eKey);
		Boolean	GetKeyDbClick(JoystickKey eKey);

		Float2	GetJoyStickRelativePosition(JoystickKey eJoystickKey);

		Float2	GetMouseScreenPosition();

		//Mouse On Actived Window Position
		Float2	GetMousePosition();
		Float2	GetMouseRelativePosition();

		Float1	GetMouseWheelRelativePosition();

		Boolean	Init();
		Void	Update();
		Void	Release();
	};
}
#endif