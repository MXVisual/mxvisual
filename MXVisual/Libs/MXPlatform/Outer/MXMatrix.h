#pragma once
#ifndef _MX_MATRIX_
#define _MX_MATRIX_
#include "MXPlatform.h"

namespace MXVisual
{
	class Matrix3
	{
	public:
		static Matrix3 Identity;

		Matrix3();
		Matrix3(Float1 m11, Float1 m12, Float1 m13,
			Float1 m21, Float1 m22, Float1 m23,
			Float1 m31, Float1 m32, Float1 m33);

		Matrix3(const Matrix3& Other);

		Float1* operator[](UInt32 uIndex);
		const Float1* operator[](UInt32 uIndex) const;
		
		const Matrix3& operator=(const Matrix3& Other);

		const Matrix3& operator*=(const Matrix3& Other);

		Matrix3 operator*(const Matrix3& Other);

		const Float1 Get(UInt32 uRow, UInt32 uCol) const;

		Void Set(UInt32 uRow, UInt32 uCol, Float1 fValue);

	protected:
		Float1 m_data[3][3];
	};

	class Matrix4
	{
	public:
		static Matrix4 Identity;

		Matrix4();
		Matrix4(Float1 m11, Float1 m12, Float1 m13, Float1 m14,
			Float1 m21, Float1 m22, Float1 m23, Float1 m24,
			Float1 m31, Float1 m32, Float1 m33, Float1 m34,
			Float1 m41, Float1 m42, Float1 m43, Float1 m44);

		Matrix4(const Matrix4& Other);

		const Matrix4& operator=(const Matrix4& Other);

		const Matrix4& operator*=(const Matrix4& Other);

		Matrix4 operator*(const Matrix4& Other) const;

		const Float1 Get(UInt32 uRow, UInt32 uCol) const;

		Void Set(UInt32 uRow, UInt32 uCol, Float1 fValue);

	protected:
		Float1 m_data[4][4];
	};
}
#endif