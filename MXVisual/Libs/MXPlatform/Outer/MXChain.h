#pragma once
#ifndef _MX_CHAIN_
#define _MX_CHAIN_
#include "MXPlatform.h"

namespace MXVisual
{
	template<typename T>
	struct ChainNode : public MemoryBlock
	{
		T Data;
		ChainNode* Prev = nullptr;
		ChainNode* Next = nullptr;

		~ChainNode()
		{
			Detach();
		}

		Void Detach()
		{
			if (Prev != nullptr)
			{
				Prev->Next = Next;
			}

			if (Next != nullptr)
			{
				Next->Prev = Prev;
			}

			Prev = nullptr;
			Next = nullptr;
		}
		Void InsertAfter(ChainNode* Node)
		{
			ASSERT_IF_FAILED(Node != nullptr);
			ASSERT_IF_FAILED(Next == nullptr && Prev == nullptr);

			Prev = Node;
			Next = Node->Next;
			if (Next != nullptr)
			{
				Next->Prev = this;
			}
			Node->Next = this;
		}
		Void InsertBefore(ChainNode* Node)
		{
			ASSERT_IF_FAILED(Node != nullptr);
			ASSERT_IF_FAILED(Next == nullptr && Prev == nullptr);

			Prev = Node->Prev;
			Next = Node;
			if (Prev != nullptr)
			{
				Prev->Next = this;
			}
			Node->Prev = this;
		}
	};

	template<typename T>
	class Chain
	{
	public:
		Chain()
		{
			m_ChainTail.InsertAfter(&m_ChainHead);
		}
		~Chain()
		{

		}

		ChainNode<T>* GetNode(UInt32 NodeCount) const
		{
			UInt32 uCount = 0;
			ChainNode<T>* t = m_ChainHead.Next;
			while (t != &m_ChainTail && uCount < NodeCount)
			{
				uCount++;
				t = t->Next;
			}

			return t == &m_ChainTail ? nullptr : t;
		}

		ChainNode<T>* Pop(UInt32 NodeIndex)
		{
			ChainNode<T>* PopNode = GetNode(NodeIndex);

			if (PopNode != nullptr)
			{
				PopNode->Detach();
			}

			return PopNode;
		}
		Void Insert(T& Data)
		{
			ChainNode<T>* Node = new ChainNode<T>;
			Node->Data = Data;

			Node->InsertAfter(m_ChainHead);
		}
		Void PushBack(T& Data)
		{
			ChainNode<T>* Node = new ChainNode<T>;
			Node->Data = Data;
			PushBack(Node);
		}
		Void PushBack(ChainNode<T>* Node)
		{
			Node->InsertBefore(&m_ChainTail);
		}

	private:
		ChainNode<T> m_ChainHead;
		ChainNode<T> m_ChainTail;
	};
}
#endif