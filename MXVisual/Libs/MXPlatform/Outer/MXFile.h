#pragma once
#ifndef _MX_FILE_
#define _MX_FILE_
#include "MXPlatform.h"
#include "MXMemory.h"
#include "MXString.h"

namespace MXVisual
{
	class IFile : public MemoryBlock
	{
	public:
		enum FileDataType
		{
			EFDT_Binary,
			EFDT_Text
		};

		static IFile* Open(const Char* szFileName, FileDataType eType);
		static IFile* New(const Char* szFileName, FileDataType eType);

	public:
		virtual Void Close() = 0;

		virtual const Char* GetFileName() const = 0;
		virtual UInt32 GetFileSize() const = 0;

		virtual UInt32 ReadLine(Void* pBuf, UInt32 uMaxSize) = 0;
		virtual UInt32 Read(Void* pBuf, UInt32 uMaxSize, Boolean bOffset = False, UInt32 uOffset = 0) = 0;

		virtual UInt32 Write(const Void* pBuf, UInt32 uBufSize) = 0;

	protected:
		virtual ~IFile() {};
	};

	Void	CalFileName(const Char* szFilePath, Char* szResult, UInt32 uMaxSize);
	Void	CalAbsolutePath(const Char* szPath, const Char* szPathToCal, Char* szResult, UInt32 uMaxSize);
	Void	CalRelativePath(const Char* szPath, const Char* szPathToCal, Char* szResult, UInt32 uMaxSize);
	Void	NormalizePath(const Char* szPathToCal, Char* szResult, UInt32 uMaxSize);

	Boolean CheckFile(const Char* szFileName);
	Boolean CheckFileSuffix(const Char* szFileName, const Char* szSuffix);
}
#endif