#pragma once
#ifndef _MX_TIMER_
#define _MX_TIMER_
#include "MXPlatform.h"

namespace MXVisual
{
	namespace Timer
	{
		Void Reset();
		Void Update(Float1 fTargetDeltaTime = 0);//Default TargetDeltatTime = 0, No Waitting
		Void SetTimeScale(Float1 fScale);

		Double1	GetTime();
		Double1	GetTimeNoScale();
		Double1	GetDeltaTime();
		Double1	GetDeltaTimeNoScale();
	};

}
#endif