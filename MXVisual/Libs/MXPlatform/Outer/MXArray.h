#pragma once
#ifndef _MX_ARRAY_
#define _MX_ARRAY_
#include "MXPlatform.h"
#include <assert.h>
#include <string.h>

namespace MXVisual
{
	template<typename T>
	class Array
	{
	public:
		Array()
			: m_pData(nullptr)
			, m_uElementCount(0)
			, m_uBufferSize(InitSize)
		{
			m_pData = new T[InitSize];
		}
		Array(UInt32 uElementCount)
		{
			m_uBufferSize = uElementCount;
			m_pData = new T[uElementCount];
			m_uElementCount = uElementCount;
		}
		~Array()
		{
			MX_DELETE_ARRAY(m_pData);
		}

		Array(const Array& Other)
			: m_pData(nullptr)
			, m_uElementCount(0)
		{
			m_uBufferSize = Other.m_uBufferSize;
			if (m_uBufferSize != 0)
			{
				m_pData = new T[m_uBufferSize];
				memcpy(m_pData, Other.m_pData, m_uBufferSize * sizeof(T));
			}
			m_uElementCount = Other.m_uElementCount;
		}
		const Array& operator=(const Array& Other)
		{
			m_uBufferSize = Other.m_uBufferSize;
			if (m_uBufferSize != 0)
			{
				delete[] m_pData;
				m_pData = new T[m_uBufferSize];
				for (UInt32 u = 0; u < Other.m_uElementCount; u++)
				{
					m_pData[u] = Other.m_pData[u];
				}
			}
			m_uElementCount = Other.m_uElementCount;
			return *this;
		}

		T& operator[](UInt32 uIndex)
		{
			assert(uIndex < m_uBufferSize && "Access Out of Bounds ");
			return m_pData[uIndex];
		}
		const T& operator[](UInt32 uIndex) const
		{
			assert(uIndex < m_uBufferSize && "Access Out of Bounds ");
			return m_pData[uIndex];
		}

		Void SetElementCount(UInt32 uCount)
		{
			if (m_uBufferSize < uCount)
			{
				Resize(uCount);
			}
			m_uElementCount = uCount;
		}

		Void Resize(UInt32 uSize)
		{
			assert((uSize > m_uElementCount) && "New Size Cannot Hold All Element");

			T* pTemp = m_pData;
			m_pData = new T[uSize];
			UInt32 uCopySize = uSize > m_uElementCount ? m_uElementCount : uSize;;
			memcpy(m_pData, pTemp, uCopySize * sizeof(T));
			m_uElementCount = uCopySize;
			m_uBufferSize = uSize;
			delete[] pTemp;
		}

		Void Add(const T& element)
		{
			if (m_uElementCount + 1 > m_uBufferSize)
			{
				Resize(m_uBufferSize + Increment);
			}

			m_pData[m_uElementCount] = element;
			m_uElementCount++;
		}

		const UInt32 ElementCount() const
		{
			return m_uElementCount;
		}

		Void Erase(UInt32 uIndex, UInt32 uCount)
		{
			UInt32 uMoveDataSize = (m_uElementCount - uCount - uIndex) * sizeof(T);
			UInt8* pTemp = new UInt8[uMoveDataSize];

			T* pSrc = m_pData + (uIndex + uCount);
			T* pDest = m_pData + uIndex;

			memcpy(pTemp, pSrc, uMoveDataSize);
			memcpy(pDest, pTemp, uMoveDataSize);

			delete[] pTemp;
			m_uElementCount -= uCount;
		}

		//数组最后的数据直接拷贝到前面
		Void EraseOutOfOrder(UInt32 uIndex, UInt32 uCount)
		{
			uCount = Math::Min(m_uElementCount - uIndex, uCount);
			UInt32 uDestIndex = uIndex;
			UInt32 uSrcIndex = Math::Max(m_uElementCount - uCount, uDestIndex + uCount);

			if (uDestIndex < uSrcIndex)
			{
				memcpy(&m_pData[uDestIndex], &m_pData[uSrcIndex], (m_uElementCount - uSrcIndex) * sizeof(T));
			}
			m_uElementCount -= uCount;
		}

		Void Clear()
		{
			m_uElementCount = 0;
		}

		SInt32 Find(const T& element, UInt32 uOffset = 0) const
		{
			for (UInt32 u = uOffset; u < m_uElementCount; u++)
			{
				if (m_pData[u] == element)
				{
					return u;
				}
			}
			return -1;
		}

		const T* GetRawData() const { return m_pData; }

	protected:
		T* m_pData;
		UInt32 m_uElementCount;
		UInt32 m_uBufferSize;

		const static UInt32 InitSize = 256;
		const static UInt32 Increment = 256;
	};
}
#endif