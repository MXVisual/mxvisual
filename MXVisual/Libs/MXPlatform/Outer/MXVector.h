#pragma once
#ifndef _MX_VECTOR_
#define _MX_VECTOR_
#include "MXPlatform.h"
#include "MXConstant.h"
#include "string.h"

namespace MXVisual
{
	template<typename BaseType>
	class Vector2
	{
	public:
		Vector2() 
		{
			m_data[0] = 0;
			m_data[1] = 0;
		};
		Vector2(const BaseType& t0, const BaseType& t1)
		{
			m_data[0] = t0;
			m_data[1] = t1;
		}
		~Vector2() {}

		const Vector2& operator=(const Vector2& vOther)
		{
			m_data[0] = vOther.m_data[0];
			m_data[1] = vOther.m_data[1];
			return *this;
		}

		Boolean operator!=(const Vector2& vOther) const
		{
			return Math::Abs(m_data[0] - vOther.m_data[0]) > Constant_MIN_FLOAT ||
				Math::Abs(m_data[1] - vOther.m_data[1]) > Constant_MIN_FLOAT;
		}
		Boolean	operator==(const Vector2& vOther) const
		{
			return !operator!=(vOther);
		}
		Boolean	operator>(const Vector2& vOther) const
		{
			return (m_data[0] - vOther.m_data[0]) > 0.0f &&
				(m_data[1] - vOther.m_data[1]) > 0.0f;
		}
		Boolean	operator<(const Vector2& vOther) const
		{
			return (m_data[0] - vOther.m_data[0]) < 0.0f &&
				(m_data[1] - vOther.m_data[1]) < 0.0f;
		}
		Boolean	operator>=(const Vector2& vOther) const
		{
			return !operator<(vOther);
		}
		Boolean	operator<=(const Vector2& vOther) const
		{
			return !operator>(vOther);
		}

		const Vector2& operator+=(const Vector2& vOther)
		{
			m_data[0] += vOther.m_data[0];
			m_data[1] += vOther.m_data[1];
			return *this;
		}
		const Vector2& operator-=(const Vector2& vOther)
		{
			m_data[0] -= vOther.m_data[0];
			m_data[1] -= vOther.m_data[1];
			return *this;
		}
		const Vector2& operator*=(const Vector2& vOther)
		{
			m_data[0] *= vOther.m_data[0];
			m_data[1] *= vOther.m_data[1];
			return *this;
		}
		const Vector2& operator/=(const Vector2& vOther)
		{
			m_data[0] /= vOther.m_data[0];
			m_data[1] /= vOther.m_data[1];
			return *this;
		}

		const Vector2& operator+=(const Float1& other)
		{
			m_data[0] += other;
			m_data[1] += other;
			return *this;
		}
		const Vector2& operator-=(const Float1& other)
		{
			m_data[0] -= other;
			m_data[1] -= other;
			return *this;
		}
		const Vector2& operator*=(const Float1& other)
		{
			m_data[0] *= other;
			m_data[1] *= other;
			return *this;
		}
		const Vector2& operator/=(const Float1& other)
		{
			m_data[0] /= other;
			m_data[1] /= other;
			return *this;
		}

		Vector2 operator+(const Vector2& vOther) const
		{
			Vector2 res = *this;
			res += vOther;
			return res;
		}
		Vector2 operator-(const Vector2& vOther) const
		{
			Vector2 res = *this;
			res -= vOther;
			return res;
		}
		Vector2 operator*(const Vector2& vOther) const
		{
			Vector2 res = *this;
			res *= vOther;
			return res;
		}
		Vector2 operator/(const Vector2& vOther) const
		{
			Vector2 res = *this;
			res /= vOther;
			return res;
		}

		Vector2 operator+(const Float1& other) const
		{
			Vector2 res = *this;
			res += other;
			return res;
		}
		Vector2 operator-(const Float1& other) const
		{
			Vector2 res = *this;
			res -= other;
			return res;
		}
		Vector2 operator*(const Float1& other) const
		{
			Vector2 res = *this;
			res *= other;
			return res;
		}
		Vector2 operator/(const Float1& other) const
		{
			Vector2 res = *this;
			res /= other;
			return res;
		}

		BaseType& operator[](UInt32 uDimension)
		{
			ASSERT_IF_FAILED(uDimension < 2);
			return m_data[uDimension];
		}
		const BaseType operator[](UInt32 uDimension) const
		{
			ASSERT_IF_FAILED(uDimension < 2);
			return m_data[uDimension];
		}

		BaseType Module() const
		{
			return m_data[0] * m_data[0] + m_data[1] * m_data[1];
		}

	protected:
		BaseType m_data[2];
	};
	template<typename BaseType>
	class Vector3
	{
	public:
		Vector3()
		{
			m_data[0] = 0;
			m_data[1] = 0;
			m_data[2] = 0;
		};
		Vector3(const BaseType& t0, const BaseType& t1, const BaseType& t2)
		{
			m_data[0] = t0;
			m_data[1] = t1;
			m_data[2] = t2;
		}
		~Vector3() {}

		const Vector3& operator=(const Vector3& vOther)
		{
			m_data[0] = vOther.m_data[0];
			m_data[1] = vOther.m_data[1];
			m_data[2] = vOther.m_data[2];
			return *this;
		}

		Boolean operator!=(const Vector3& vOther) const
		{
			return Math::Abs(m_data[0] - vOther.m_data[0]) > Constant_MIN_FLOAT ||
				Math::Abs(m_data[1] - vOther.m_data[1]) > Constant_MIN_FLOAT ||
				Math::Abs(m_data[2] - vOther.m_data[2]) > Constant_MIN_FLOAT;
		}
		Boolean	operator==(const Vector3& vOther) const
		{
			return !operator!=(vOther);
		}
		Boolean	operator>(const Vector3& vOther) const
		{
			return (m_data[0] - vOther.m_data[0]) > 0.0f &&
				(m_data[1] - vOther.m_data[1]) > 0.0f &&
				(m_data[2] - vOther.m_data[2]) > 0.0f;
		}
		Boolean	operator<(const Vector3& vOther) const
		{
			return (m_data[0] - vOther.m_data[0]) < 0.0f &&
				(m_data[1] - vOther.m_data[1]) < 0.0f &&
				(m_data[2] - vOther.m_data[2]) < 0.0f;
		}
		Boolean	operator>=(const Vector3& vOther) const
		{
			return !operator<(vOther);
		}
		Boolean	operator<=(const Vector3& vOther) const
		{
			return !operator>(vOther);
		}

		const Vector3& operator+=(const Vector3& vOther)
		{
			m_data[0] += vOther.m_data[0];
			m_data[1] += vOther.m_data[1];
			m_data[2] += vOther.m_data[2];
			return *this;
		}
		const Vector3& operator-=(const Vector3& vOther)
		{
			m_data[0] -= vOther.m_data[0];
			m_data[1] -= vOther.m_data[1];
			m_data[2] -= vOther.m_data[2];
			return *this;
		}
		const Vector3& operator*=(const Vector3& vOther)
		{
			m_data[0] *= vOther.m_data[0];
			m_data[1] *= vOther.m_data[1];
			m_data[2] *= vOther.m_data[2];
			return *this;
		}
		const Vector3& operator/=(const Vector3& vOther)
		{
			m_data[0] /= vOther.m_data[0];
			m_data[1] /= vOther.m_data[1];
			m_data[2] /= vOther.m_data[2];
			return *this;
		}

		const Vector3& operator+=(const Float1& other)
		{
			m_data[0] += other;
			m_data[1] += other;
			m_data[2] += other;
			return *this;
		}
		const Vector3& operator-=(const Float1& other)
		{
			m_data[0] -= other;
			m_data[1] -= other;
			m_data[2] -= other;
			return *this;
		}
		const Vector3& operator*=(const Float1& other)
		{
			m_data[0] *= other;
			m_data[1] *= other;
			m_data[2] *= other;
			return *this;
		}
		const Vector3& operator/=(const Float1& other)
		{
			m_data[0] /= other;
			m_data[1] /= other;
			m_data[2] /= other;
			return *this;
		}

		Vector3 operator+(const Vector3& vOther) const
		{
			Vector3 res = *this;
			res += vOther;
			return res;
		}
		Vector3 operator-(const Vector3& vOther) const
		{
			Vector3 res = *this;
			res -= vOther;
			return res;
		}
		Vector3 operator*(const Vector3& vOther) const
		{
			Vector3 res = *this;
			res *= vOther;
			return res;
		}
		Vector3 operator/(const Vector3& vOther) const
		{
			Vector3 res = *this;
			res /= vOther;
			return res;
		}

		Vector3 operator+(const Float1& other) const
		{
			Vector3 res = *this;
			res += other;
			return res;
		}
		Vector3 operator-(const Float1& other) const
		{
			Vector3 res = *this;
			res -= other;
			return res;
		}
		Vector3 operator*(const Float1& other) const
		{
			Vector3 res = *this;
			res *= other;
			return res;
		}
		Vector3 operator/(const Float1& other) const
		{
			Vector3 res = *this;
			res /= other;
			return res;
		}

		BaseType& operator[](UInt32 uDimension)
		{
			ASSERT_IF_FAILED(uDimension < 3);
			return m_data[uDimension];
		}
		const BaseType operator[](UInt32 uDimension) const
		{
			ASSERT_IF_FAILED(uDimension < 3);
			return m_data[uDimension];
		}

		BaseType Module() const
		{
			return m_data[0] * m_data[0] + m_data[1] * m_data[1] + m_data[2] * m_data[2];
		}
	protected:
		BaseType m_data[3];
	};
	template<typename BaseType>
	class Vector4
	{
	public:
		Vector4()
		{
			m_data[0] = 0;
			m_data[1] = 0;
			m_data[2] = 0;
			m_data[3] = 0;
		};
		Vector4(const BaseType& t0, const BaseType& t1, const BaseType& t2, const BaseType& t3)
		{
			m_data[0] = t0;
			m_data[1] = t1;
			m_data[2] = t2;
			m_data[3] = t3;
		}
		~Vector4() {}

		const Vector4& operator=(const Vector4& vOther)
		{
			m_data[0] = vOther.m_data[0];
			m_data[1] = vOther.m_data[1];
			m_data[2] = vOther.m_data[2];
			m_data[3] = vOther.m_data[3];
			return *this;
		}

		Boolean operator!=(const Vector4& vOther) const
		{
			return Math::Abs(m_data[0] - vOther.m_data[0]) > Constant_MIN_FLOAT ||
				Math::Abs(m_data[1] - vOther.m_data[1]) > Constant_MIN_FLOAT ||
				Math::Abs(m_data[2] - vOther.m_data[2]) > Constant_MIN_FLOAT ||
				Math::Abs(m_data[3] - vOther.m_data[3]) > Constant_MIN_FLOAT;
		}
		Boolean	operator==(const Vector4& vOther) const
		{
			return !operator!=(vOther);
		}
		Boolean	operator>(const Vector4& vOther) const
		{
			return (m_data[0] - vOther.m_data[0]) > 0.0f &&
				(m_data[1] - vOther.m_data[1]) > 0.0f &&
				(m_data[2] - vOther.m_data[2]) > 0.0f &&
				(m_data[3] - vOther.m_data[3]) > 0.0f;
		}
		Boolean	operator<(const Vector4& vOther) const
		{
			return (m_data[0] - vOther.m_data[0]) < 0.0f &&
				(m_data[1] - vOther.m_data[1]) < 0.0f &&
				(m_data[2] - vOther.m_data[2]) < 0.0f &&
				(m_data[3] - vOther.m_data[3]) < 0.0f;
		}
		Boolean	operator>=(const Vector4& vOther) const
		{
			return !operator<(vOther);
		}
		Boolean	operator<=(const Vector4& vOther) const
		{
			return !operator>(vOther);
		}

		const Vector4& operator+=(const Vector4& vOther)
		{
			m_data[0] += vOther.m_data[0];
			m_data[1] += vOther.m_data[1];
			m_data[2] += vOther.m_data[2];
			m_data[3] += vOther.m_data[3];
			return *this;
		}
		const Vector4& operator-=(const Vector4& vOther)
		{
			m_data[0] -= vOther.m_data[0];
			m_data[1] -= vOther.m_data[1];
			m_data[2] -= vOther.m_data[2];
			m_data[3] -= vOther.m_data[3];
			return *this;
		}
		const Vector4& operator*=(const Vector4& vOther)
		{
			m_data[0] *= vOther.m_data[0];
			m_data[1] *= vOther.m_data[1];
			m_data[2] *= vOther.m_data[2];
			m_data[3] *= vOther.m_data[3];
			return *this;
		}
		const Vector4& operator/=(const Vector4& vOther)
		{
			m_data[0] /= vOther.m_data[0];
			m_data[1] /= vOther.m_data[1];
			m_data[2] /= vOther.m_data[2];
			m_data[3] /= vOther.m_data[3];
			return *this;
		}

		const Vector4& operator+=(const Float1& other)
		{
			m_data[0] += other;
			m_data[1] += other;
			m_data[2] += other;
			m_data[3] += other;
			return *this;
		}
		const Vector4& operator-=(const Float1& other)
		{
			m_data[0] -= other;
			m_data[1] -= other;
			m_data[2] -= other;
			m_data[3] -= other;
			return *this;
		}
		const Vector4& operator*=(const Float1& other)
		{
			m_data[0] *= other;
			m_data[1] *= other;
			m_data[2] *= other;
			m_data[3] *= other;
			return *this;
		}
		const Vector4& operator/=(const Float1& other)
		{
			m_data[0] /= other;
			m_data[1] /= other;
			m_data[2] /= other;
			m_data[3] /= other;
			return *this;
		}

		Vector4 operator+(const Vector4& vOther) const
		{
			Vector4 res = *this;
			res += vOther;
			return res;
		}
		Vector4 operator-(const Vector4& vOther) const
		{
			Vector4 res = *this;
			res -= vOther;
			return res;
		}
		Vector4 operator*(const Vector4& vOther) const
		{
			Vector4 res = *this;
			res *= vOther;
			return res;
		}
		Vector4 operator/(const Vector4& vOther) const
		{
			Vector4 res = *this;
			res /= vOther;
			return res;
		}

		Vector4 operator+(const Float1& other) const
		{
			Vector4 res = *this;
			res += other;
			return res;
		}
		Vector4 operator-(const Float1& other) const
		{
			Vector4 res = *this;
			res -= other;
			return res;
		}
		Vector4 operator*(const Float1& other) const
		{
			Vector4 res = *this;
			res *= other;
			return res;
		}
		Vector4 operator/(const Float1& other) const
		{
			Vector4 res = *this;
			res /= other;
			return res;
		}

		BaseType& operator[](UInt32 uDimension)
		{
			ASSERT_IF_FAILED(uDimension < 4);
			return m_data[uDimension];
		}
		const BaseType operator[](UInt32 uDimension) const
		{
			ASSERT_IF_FAILED(uDimension < 4);
			return m_data[uDimension];
		}

		BaseType Module() const
		{
			return m_data[0] * m_data[0] + m_data[1] * m_data[1] + m_data[2] * m_data[2] + m_data[3] * m_data[3];
		}
	protected:
		BaseType m_data[4];
	};

	typedef Vector2<Float1> Float2;
	typedef Vector3<Float1> Float3;
	typedef Vector4<Float1> Float4;
}
#endif