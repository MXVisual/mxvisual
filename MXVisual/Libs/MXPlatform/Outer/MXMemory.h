#pragma once
#ifndef _MX_MEMORY_
#define _MX_MEMORY_
#include "MXPlatform.h"
#include <cstddef>

namespace MXVisual
{
	class MemoryBlock
	{
	public:
		Void* operator new(std::size_t);
		Void* operator new[](std::size_t);
		Void  operator delete(void*);
		Void  operator delete[](void*);

	};

	struct MemoryPool
	{
		static Void*	Allocate(UInt32 uSize);
		static Void	Deallocate(Void* pMemory);

		static UInt32	GetMemoryOccupied();
	};
}
#endif