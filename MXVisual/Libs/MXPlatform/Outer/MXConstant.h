#pragma once
#ifndef _MX_CONSTANT_
#define _MX_CONSTANT_
#include "MXPlatform.h"
#include "float.h"

namespace MXVisual
{
	const Float1 Constant_PI = 3.1415926f;

	const UInt32 Constant_MAX_PATH = 256;

	const Float1 Constant_MIN_FLOAT = FLT_EPSILON;

	const Float1 Constant_MAX_FLOAT = FLT_MAX;
}
#endif