#pragma once
#ifndef _MX_QUATERNION_
#define _MX_QUATERNION_
#include "MXPlatform.h"

namespace MXVisual
{
	class Quaternion
	{
	public:
		Quaternion();
		Quaternion(Float1 r0, Float1 r1, Float1 r2, Float1 v);
		Quaternion(const Quaternion& qOther);

		~Quaternion();

		Boolean operator!=(const Quaternion& qOther);
		const Quaternion& operator=(const Quaternion& qOther);

		const Quaternion& operator+=(const Quaternion& qOther);
		const Quaternion& operator-=(const Quaternion& qOther);
		const Quaternion& operator*=(const Quaternion& qOther);
		const Quaternion& operator*=(const Float1& fOther);

		Quaternion operator+(const Quaternion& qOther);
		Quaternion operator-(const Quaternion& qOther);
		Quaternion operator*(const Quaternion& qOther);
		Quaternion operator*(const Float1& fOther);

		Float1& operator[](const UInt32 uIndex);
		const Float1 operator[](const UInt32 uIndex) const;

		const Float1 Module() const;

		static Quaternion	Normalize(const Quaternion& q);
		static Float1		DotProduct(const Quaternion& qA, const Quaternion& qB);

	protected:
		Float1 m_data[4];
	};
}
#endif