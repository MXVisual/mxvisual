#pragma once
#ifndef _MX_SYSTEM_
#define _MX_SYSTEM_
#include "MXPlatform.h"

namespace MXVisual
{
	struct LocalTime
	{
		UInt32 uYear;
		UInt8 uMouth;
		UInt8 uDay;

		UInt8 uHour;
		UInt8 uMinute;
		UInt8 uSecond;
	};

	struct System
	{
		static LocalTime	GetLocalTime();

		static Double1		GetTime();//In Second

		static Void			CreateFolder(const Char* szFolderName);

		static Boolean		DlgSelectFolder(const Char* szDlgTitle, Char* szResult, UInt32 uMaxSize);
		static Boolean		DlgSelectFile(const Char* szDlgTitle, Char* szResult, UInt32 uMaxSize);
	};
}
#endif
