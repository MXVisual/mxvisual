#pragma once
#ifndef _MX_STRING_
#define _MX_STRING_
#include "MXPlatform.h"
#include "MXVector.h"
#include "MXMatrix.h"
#include "MXArray.h"

namespace MXVisual
{
	class String
	{
	public:
		static String Format(const Char *szString, ...);

		static Array<UInt8> Transcode_GB2312_To_UTF16(const String& str);
		static Array<UInt8> Transcode_UTF8_To_UTF16(const String& str);

		String();
		String(const Char *szString);
		String(const String& strString);

		const String& operator=(const Char *szString);
		const String& operator=(const String& strString);

		~String();

		const Char* CString() const;

		SInt32 Find(const Char *szString, UInt32 uOffset = 0) const;
		SInt32 FindLastOf(const Char *szString) const;

		Void Erase(UInt32 nIndex, SInt32 nCount = -1);//when -1, will erase all char from nindex
		Void Insert(const Char *szString, UInt32 uOffset);//if attach, should use this func

		Void Replace(const Char* szDest, const Char* szSrc, SInt32 nOffset = 0);

		UInt32 Length() const;
		Boolean IsEmpty() const;
		Void Clear();

		String operator+(const Char* szString);
		String operator+(const String& strString);

		const String& operator+=(const Char* szString);
		const String& operator+=(const String& strString);

		const String& operator+=(const Char Value);
		const String& operator+=(const Float1 Value);
		const String& operator+=(const Float2& Value);
		const String& operator+=(const Float3& Value);
		const String& operator+=(const Float4& Value);

		const String& operator+=(const Matrix3& Value);
		const String& operator+=(const Matrix4& Value);

		const Char operator[](UInt32 uIndex) const;

		Boolean operator<(const String& strString) const;
		Boolean operator>(const String& strString) const;
		Boolean operator!=(const String& strString) const;
		Boolean operator==(const String& strString) const;

		Boolean operator<(const Char* szString) const;
		Boolean operator>(const Char* szString) const;
		Boolean operator!=(const Char* szString) const;
		Boolean operator==(const Char* szString) const;

	protected:
		Char *m_szString;
		UInt32 m_uStringLength;
		UInt32 m_uBufferSize;
	};
}
#endif