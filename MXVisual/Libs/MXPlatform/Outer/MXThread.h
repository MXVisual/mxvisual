#pragma once
#ifndef _MX_THREAD_
#define _MX_THREAD_
#include "MXPlatform.h"
#include "MXString.h"
#include "MXChain.h"
#include <functional>

namespace MXVisual
{
	//Mutex和CriticalSection作用一样，只是Mutex可以进程间互斥
	//此处就不区分了
	class Mutex
	{
	public:
		Mutex();
		~Mutex();

		Void Lock();
		Void UnLock();

	protected:
		Void* m_pRealMutex;
	};

	class Semaphore
	{

	};

	//SInt16 SInt32 SInt64 Void*Pointer
	template<typename T>
	class Atomic
	{
	public:
		Atomic(T value);
		~Atomic();

		Atomic(const Atomic& value) = delete;
		Void operator=(const Atomic& value) = delete;

		Void	Increment();
		Void	Decrement();
		Void	Exchange(T value);
		Void	CompareExchange(T compare, T value);
		T		Value();

	protected:
		Void* m_pRealAtomic;
	};


#define MAIN_THREAD_NAME	"MainThread"
#define RENDER_THREAD_NAME	"RenderThread"
#define IO_THREAD_NAME		"IOThread"

#define RENDER_THREAD_ID	1
#define IO_THREAD_ID		2

	struct ThreadTaskStatistics
	{
		String Name;
		Float1 StartTime = 0.0f;
		Float1 EndTime = 0.0f;
	};

	struct ThreadManager
	{
		static Boolean	Init();
		static Void		Release();

		//High Priority Task Will Execute First
		static Void		AddThreadTask(
			const Char* ThreadName,
			std::function<Void(Void)> Task, const Char* TaskName = nullptr, UInt32 Priority = 0);

		static Void		GetThreadStatistics(
			const Char* ThreadName,
			Chain<ThreadTaskStatistics>& StatisticChain);

		static Void		SetStatisticsSwitch(Boolean bTurnOn);
		static Boolean	GetStatisticsSwitch();

		static Void Tick();
		static Void ShutDown();
	};
}
#endif