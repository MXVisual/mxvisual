#include "MXInput.h"
#include "MXInterface.h"
#if PLATFORM == PLATFORM_WINDOWS
#include "Windows.h"
#include "Windowsx.h"
#include "dinput.h"
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
#endif

namespace MXVisual
{
	namespace Input_Inner
	{
		struct CursorInfo
		{
			Float2 f2ScreenXY;
			Float1 fWheel;
			Boolean btnDown[3];
		};

		struct KeyInfo
		{
			UInt8 btnDown[256];
		};

		struct JoystickInfo
		{
			Boolean bReady;
			Boolean	*currentBtnState = nullptr;
			Boolean *previewBtnState = nullptr;

			Boolean btnDown[2][32];//abxy up down left down lb rb ljoy rjoy
			Float1 fTrigger[2];
			Float2 f2Joystick[2];
		};

		Boolean gInfoCurrentIndex = 0;
		Boolean gInfoLastIndex = 1;
		CursorInfo	gCursorInfo[2] = { Float2(0,0), 0, };
		KeyInfo		gKeyInfo[2] = { 0 };

		JoystickInfo Joystick = { False, };

#if PLATFORM == PLATFORM_WINDOWS
		IDirectInput8* pDInput = nullptr;
		IDirectInputDevice8* pDeviceMouse = nullptr;
		IDirectInputDevice8* pDeviceKeyboard = nullptr;
		IDirectInputDevice8* pDeviceJoystick = nullptr;

		BOOL CALLBACK EnumControllerCallback(const DIDEVICEINSTANCE* lpddi, VOID* pvRef)
		{
			pDInput->CreateDevice(lpddi->guidInstance, &pDeviceJoystick, NULL);
			pDeviceJoystick->SetDataFormat(&c_dfDIJoystick);

			DIPROPRANGE prop_range;
			prop_range.diph.dwSize = sizeof(DIPROPRANGE);
			prop_range.diph.dwHeaderSize = sizeof(DIPROPHEADER);
			prop_range.diph.dwObj = DIJOFS_X;
			prop_range.diph.dwHow = DIPH_BYOFFSET;  // offset into data format
			prop_range.lMin = -0x7fff;
			prop_range.lMax = 0x7fff;

			DIPROPDWORD prop_dword;
			prop_dword.diph.dwSize = sizeof(DIPROPDWORD);
			prop_dword.diph.dwHeaderSize = sizeof(DIPROPHEADER);
			prop_dword.diph.dwObj = DIJOFS_X;
			prop_dword.diph.dwHow = DIPH_BYOFFSET;
			prop_dword.dwData = 500;

			pDeviceJoystick->SetProperty(DIPROP_RANGE, &prop_range.diph);
			pDeviceJoystick->SetProperty(DIPROP_DEADZONE, &prop_dword.diph);

			prop_range.diph.dwObj = DIJOFS_Y;
			prop_dword.diph.dwObj = DIJOFS_Y;
			pDeviceJoystick->SetProperty(DIPROP_RANGE, &prop_range.diph);
			pDeviceJoystick->SetProperty(DIPROP_DEADZONE, &prop_dword.diph);

			prop_range.diph.dwObj = DIJOFS_RX;
			prop_dword.diph.dwObj = DIJOFS_RX;
			pDeviceJoystick->SetProperty(DIPROP_RANGE, &prop_range.diph);
			pDeviceJoystick->SetProperty(DIPROP_DEADZONE, &prop_dword.diph);

			prop_range.diph.dwObj = DIJOFS_RY;
			prop_dword.diph.dwObj = DIJOFS_RY;
			pDeviceJoystick->SetProperty(DIPROP_RANGE, &prop_range.diph);
			pDeviceJoystick->SetProperty(DIPROP_DEADZONE, &prop_dword.diph);

			return DIENUM_STOP;
		}

		Void Init()
		{
			HINSTANCE hInst = GetModuleHandle(0);
			if (SUCCEEDED(DirectInput8Create(hInst, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&pDInput, NULL)))
			{
				pDInput->CreateDevice(GUID_SysMouse, &pDeviceMouse, NULL);
				pDeviceMouse->SetDataFormat(&c_dfDIMouse);

				pDInput->CreateDevice(GUID_SysKeyboard, &pDeviceKeyboard, NULL);
				pDeviceKeyboard->SetDataFormat(&c_dfDIKeyboard);

				pDInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumControllerCallback, NULL, DIEDFL_ATTACHEDONLY);
			}
		}
		Void Release()
		{
			MX_RELEASE_INTERFACE(pDeviceJoystick);
			MX_RELEASE_INTERFACE(pDeviceKeyboard);
			MX_RELEASE_INTERFACE(pDeviceMouse);
			MX_RELEASE_INTERFACE(pDInput);
		}

		Void HandleMouseInput()
		{
			DIMOUSESTATE mouseState;
			if (pDeviceMouse != nullptr && SUCCEEDED(pDeviceMouse->Acquire()))
			{
				pDeviceMouse->Poll();
				pDeviceMouse->GetDeviceState(sizeof(DIMOUSESTATE), &mouseState);

				POINT pt;
				GetCursorPos(&pt);
				gCursorInfo[gInfoCurrentIndex].f2ScreenXY = Float2((Float1)pt.x, (Float1)pt.y);
				gCursorInfo[gInfoCurrentIndex].fWheel = mouseState.lZ / 120.0f;
				gCursorInfo[gInfoCurrentIndex].btnDown[0] = mouseState.rgbButtons[0] > 0;
				gCursorInfo[gInfoCurrentIndex].btnDown[1] = mouseState.rgbButtons[1] > 0;
				gCursorInfo[gInfoCurrentIndex].btnDown[2] = mouseState.rgbButtons[2] > 0;
			}
		}

		Void HandleKeyboardInput()
		{
			if (pDeviceKeyboard != nullptr && SUCCEEDED(pDeviceKeyboard->Acquire()))
			{
				pDeviceKeyboard->Poll();
				pDeviceKeyboard->GetDeviceState(sizeof(KeyInfo), &gKeyInfo[gInfoCurrentIndex]);
			}
		}

		Void HandleJoystickInput()
		{
			static DIJOYSTATE joyState;
			Joystick.bReady = False;
			if (pDeviceJoystick != nullptr && SUCCEEDED(pDeviceJoystick->Acquire()))
			{
				Joystick.currentBtnState = Joystick.currentBtnState == Joystick.btnDown[0] ?
					Joystick.btnDown[1] : Joystick.btnDown[0];
				Joystick.previewBtnState = Joystick.currentBtnState == Joystick.btnDown[1] ?
					Joystick.btnDown[0] : Joystick.btnDown[1];

				pDeviceJoystick->Poll();
				pDeviceJoystick->GetDeviceState(sizeof(DIJOYSTATE), &joyState);

				Joystick.bReady = True;
				Joystick.currentBtnState[Input::eJoyA] = joyState.rgbButtons[0] > 0;
				Joystick.currentBtnState[Input::eJoyB] = joyState.rgbButtons[1] > 0;
				Joystick.currentBtnState[Input::eJoyX] = joyState.rgbButtons[2] > 0;
				Joystick.currentBtnState[Input::eJoyY] = joyState.rgbButtons[3] > 0;

				Joystick.currentBtnState[Input::eJoyLB] = joyState.rgbButtons[4] > 0;
				Joystick.currentBtnState[Input::eJoyRB] = joyState.rgbButtons[5] > 0;
				Joystick.currentBtnState[Input::eJoyBack] = joyState.rgbButtons[6] > 0;
				Joystick.currentBtnState[Input::eJoyMenu] = joyState.rgbButtons[7] > 0;

				Joystick.currentBtnState[Input::eJoyStickL] = joyState.rgbButtons[8] > 0;
				Joystick.currentBtnState[Input::eJoyStickR] = joyState.rgbButtons[9] > 0;

				//-1 0 9000 18000 27000
				Joystick.currentBtnState[Input::eJoyUp] = joyState.rgdwPOV[0] == 0 ||
					joyState.rgdwPOV[0] == 4500 || joyState.rgdwPOV[0] == 31500;
				Joystick.currentBtnState[Input::eJoyRight] = joyState.rgdwPOV[0] == 4500 ||
					joyState.rgdwPOV[0] == 9000 || joyState.rgdwPOV[0] == 13500;
				Joystick.currentBtnState[Input::eJoyDown] = joyState.rgdwPOV[0] == 13500 ||
					joyState.rgdwPOV[0] == 18000 || joyState.rgdwPOV[0] == 22500;
				Joystick.currentBtnState[Input::eJoyLeft] = joyState.rgdwPOV[0] == 22500 ||
					joyState.rgdwPOV[0] == 27000 || joyState.rgdwPOV[0] == 31500;

				Joystick.fTrigger[0] = joyState.lZ > 0 ? joyState.lZ / (Float1)0x7fff : 0;
				Joystick.fTrigger[1] = joyState.lZ < 0 ? -joyState.lZ / (Float1)0x7fff : 0;
				Joystick.f2Joystick[0] = Float2(joyState.lX / (Float1)0x7fff,
					joyState.lY / (Float1)0x7fff);
				Joystick.f2Joystick[1] = Float2(joyState.lRx / (Float1)0x7fff,
					joyState.lRy / (Float1)0x7fff);
			}
		}

		Void HandleEmptyInput()
		{
			gCursorInfo[gInfoCurrentIndex].fWheel = 0.0f;
			gCursorInfo[gInfoCurrentIndex].btnDown[0] = False;
			gCursorInfo[gInfoCurrentIndex].btnDown[1] = False;
			gCursorInfo[gInfoCurrentIndex].btnDown[2] = False;

			memset(gKeyInfo[gInfoCurrentIndex].btnDown, 0, sizeof(gKeyInfo[gInfoCurrentIndex].btnDown));
			if (Joystick.bReady)
			{
				memset(Joystick.currentBtnState, 0, sizeof(Joystick.currentBtnState));

				Joystick.fTrigger[0] = 0.0f;
				Joystick.fTrigger[1] = 0.0f;
				Joystick.f2Joystick[0] = Float2();
				Joystick.f2Joystick[1] = Float2();
			}
		}

		Void SwapInputStateBuffer()
		{
			gInfoCurrentIndex = (gInfoCurrentIndex + 1) % 2;
			gInfoLastIndex = (gInfoCurrentIndex + 1) % 2;
		}
#endif
	}

	Boolean	Input::Init()
	{
		Input_Inner::Init();
		return True;
	}
	Void	Input::Update()
	{
		Input_Inner::SwapInputStateBuffer();

#if PLATFORM == PLATFORM_WINDOWS
		HWND hwnd = ::GetForegroundWindow();
		DWORD active_id = 0;
		::GetWindowThreadProcessId(hwnd, &active_id);
		if (active_id == ::GetCurrentProcessId())
#else
		if (1)
#endif
		{
			Input_Inner::HandleMouseInput();
			Input_Inner::HandleKeyboardInput();
			Input_Inner::HandleJoystickInput();
		}
		else
		{
			Input_Inner::HandleEmptyInput();
		}
	}
	Void	Input::Release()
	{
		Input_Inner::Release();
	}

	Boolean	Input::GetKeyDown(Key eKey)
	{
		switch (eKey)
		{
		case Input::eLButton:
			return Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].btnDown[0];
		case Input::eRButton:
			return Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].btnDown[1];
		case Input::eMButton:
			return Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].btnDown[2];

		case Input::eLCtrl:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_LCONTROL];
		case Input::eRCtrl:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_RCONTROL];
		case Input::eLShift:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_LSHIFT];
		case Input::eRShift:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_RSHIFT];
		case Input::eLAlt:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_LMENU];
		case Input::eRAlt:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_RMENU];

		case Input::eTab:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_TAB];
		case Input::eSpace:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_SPACE];
		case Input::eBackSpace:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_BACK];
		case Input::eEnter:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_RETURN];
		case Input::eEscape:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_ESCAPE];
		case Input::eDelete:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_DELETE];
		case Input::eNumLock:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMLOCK];
		case Input::eCapsLock:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_CAPITAL];

		case Input::eUp:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_UP];
		case Input::eDown:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_DOWN];
		case Input::eLeft:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_LEFT];
		case Input::eRight:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_RIGHT];
		case Input::ePageUp:
			return False;
			//return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[] ;
		case Input::ePageDown:
			return False;
			//return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_CAPITAL] ;
		case Input::ePageHome:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_HOME];
		case Input::ePageEnd:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_END];

		case Input::eF1:
		case Input::eF2:
		case Input::eF3:
		case Input::eF4:
		case Input::eF5:
		case Input::eF6:
		case Input::eF7:
		case Input::eF8:
		case Input::eF9:
		case Input::eF10:
		case Input::eF11:
		case Input::eF12:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_F1 + eKey - Input::eF1] > 0;

		case Input::eQ:
		case Input::eW:
		case Input::eE:
		case Input::eR:
		case Input::eT:
		case Input::eY:
		case Input::eU:
		case Input::eI:
		case Input::eO:
		case Input::eP:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_Q + eKey - Input::eQ] > 0;

		case Input::eA:
		case Input::eS:
		case Input::eD:
		case Input::eF:
		case Input::eG:
		case Input::eH:
		case Input::eJ:
		case Input::eK:
		case Input::eL:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_A + eKey - Input::eA] > 0;

		case Input::eZ:
		case Input::eX:
		case Input::eC:
		case Input::eV:
		case Input::eN:
		case Input::eM:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_Z + eKey - Input::eZ] > 0;

		case Input::e1:
		case Input::e2:
		case Input::e3:
		case Input::e4:
		case Input::e5:
		case Input::e6:
		case Input::e7:
		case Input::e8:
		case Input::e9:
		case Input::e0:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_1 + eKey - Input::e1] > 0;

		case Input::eNum0:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD0] > 0;
		case Input::eNum1:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD1] > 0;
		case Input::eNum2:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD2] > 0;
		case Input::eNum3:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD3] > 0;
		case Input::eNum4:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD4] > 0;
		case Input::eNum5:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD5] > 0;
		case Input::eNum6:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD6] > 0;
		case Input::eNum7:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD7] > 0;
		case Input::eNum8:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD8] > 0;
		case Input::eNum9:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD9] > 0;
		}
		return False;
	}
	Boolean	Input::GetKeyUp(Key eKey)
	{
		switch (eKey)
		{
		case Input::eLButton:
			return !Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].btnDown[0];
		case Input::eRButton:
			return !Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].btnDown[1];
		case Input::eMButton:
			return !Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].btnDown[2];

		case Input::eLCtrl:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_LCONTROL];
		case Input::eRCtrl:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_RCONTROL];
		case Input::eLShift:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_LSHIFT];
		case Input::eRShift:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_RSHIFT];
		case Input::eLAlt:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_LMENU];
		case Input::eRAlt:
			return!Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_RMENU];

		case Input::eTab:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_TAB];
		case Input::eSpace:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_SPACE];
		case Input::eBackSpace:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_BACK];
		case Input::eEnter:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_RETURN];
		case Input::eEscape:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_ESCAPE];
		case Input::eDelete:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_DELETE];
		case Input::eNumLock:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_NUMLOCK];
		case Input::eCapsLock:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_CAPITAL];

		case Input::eUp:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_UP];
		case Input::eDown:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_DOWN];
		case Input::eLeft:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_LEFT];
		case Input::eRight:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_RIGHT];
		case Input::ePageUp:
			return False;
			//return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnUp[] ;
		case Input::ePageDown:
			return False;
			//return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnUp[DIK_CAPITAL] ;
		case Input::ePageHome:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_HOME];
		case Input::ePageEnd:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_END];

		case Input::eF1:
		case Input::eF2:
		case Input::eF3:
		case Input::eF4:
		case Input::eF5:
		case Input::eF6:
		case Input::eF7:
		case Input::eF8:
		case Input::eF9:
		case Input::eF10:
		case Input::eF11:
		case Input::eF12:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_F1 + eKey - Input::eF1];

		case Input::eQ:
		case Input::eW:
		case Input::eE:
		case Input::eR:
		case Input::eT:
		case Input::eY:
		case Input::eU:
		case Input::eI:
		case Input::eO:
		case Input::eP:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_Q + eKey - Input::eQ] > 0;

		case Input::eA:
		case Input::eS:
		case Input::eD:
		case Input::eF:
		case Input::eG:
		case Input::eH:
		case Input::eJ:
		case Input::eK:
		case Input::eL:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_A + eKey - Input::eA] > 0;

		case Input::eZ:
		case Input::eX:
		case Input::eC:
		case Input::eV:
		case Input::eN:
		case Input::eM:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_Z + eKey - Input::eZ] > 0;

		case Input::e0:
		case Input::e1:
		case Input::e2:
		case Input::e3:
		case Input::e4:
		case Input::e5:
		case Input::e6:
		case Input::e7:
		case Input::e8:
		case Input::e9:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_1 + eKey - Input::e1] > 0;

		case Input::eNum0:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD0] > 0;
		case Input::eNum1:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD1] > 0;
		case Input::eNum2:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD2] > 0;
		case Input::eNum3:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD3] > 0;
		case Input::eNum4:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD4] > 0;
		case Input::eNum5:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD5] > 0;
		case Input::eNum6:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD6] > 0;
		case Input::eNum7:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD7] > 0;
		case Input::eNum8:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD8] > 0;
		case Input::eNum9:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD9] > 0;

		}
		return False;
	}
	Boolean	Input::GetKeyClick(Key eKey)
	{
		//上一帧按下，当前帧松开
		switch (eKey)
		{
		case Input::eLButton:
			return !Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].btnDown[0] &&
				Input_Inner::gCursorInfo[Input_Inner::gInfoLastIndex].btnDown[0];
		case Input::eRButton:
			return !Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].btnDown[1] &&
				Input_Inner::gCursorInfo[Input_Inner::gInfoLastIndex].btnDown[1];
		case Input::eMButton:
			return !Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].btnDown[2] &&
				Input_Inner::gCursorInfo[Input_Inner::gInfoLastIndex].btnDown[2];

		case Input::eLCtrl:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_LCONTROL] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_LCONTROL];
		case Input::eRCtrl:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_RCONTROL] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_RCONTROL];
		case Input::eLShift:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_LSHIFT] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_LSHIFT];
		case Input::eRShift:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_RSHIFT] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_RSHIFT];
		case Input::eLAlt:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_LMENU] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_LMENU];
		case Input::eRAlt:
			return!Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_RMENU] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_RMENU];

		case Input::eTab:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_TAB] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_TAB];
		case Input::eSpace:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_SPACE] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_SPACE];
		case Input::eBackSpace:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_BACK] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_BACK];
		case Input::eEnter:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_RETURN] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_RETURN];
		case Input::eEscape:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_ESCAPE] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_ESCAPE];
		case Input::eDelete:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_DELETE] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_DELETE];
		case Input::eNumLock:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_NUMLOCK] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_NUMLOCK];
		case Input::eCapsLock:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_CAPITAL] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_CAPITAL];

		case Input::eUp:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[VK_UP] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[VK_UP];
		case Input::eDown:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_DOWN] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_DOWN];
		case Input::eLeft:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_LEFT] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_LEFT];
		case Input::eRight:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_RIGHT] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_RIGHT];
		case Input::ePageUp:
			return False;
			//return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnUp[] ;
		case Input::ePageDown:
			return False;
			//return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnUp[VK_CAPITAL] ;
		case Input::ePageHome:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_HOME] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_HOME];
		case Input::ePageEnd:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_END] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_END];

		case Input::eF1:
		case Input::eF2:
		case Input::eF3:
		case Input::eF4:
		case Input::eF5:
		case Input::eF6:
		case Input::eF7:
		case Input::eF8:
		case Input::eF9:
		case Input::eF10:
		case Input::eF11:
		case Input::eF12:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_F1 + eKey - Input::eF1] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_F1 + eKey - Input::eF1] > 0;

		case Input::eQ:
		case Input::eW:
		case Input::eE:
		case Input::eR:
		case Input::eT:
		case Input::eY:
		case Input::eU:
		case Input::eI:
		case Input::eO:
		case Input::eP:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_Q + eKey - Input::eQ] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_Q + eKey - Input::eQ] > 0;

		case Input::eA:
		case Input::eS:
		case Input::eD:
		case Input::eF:
		case Input::eG:
		case Input::eH:
		case Input::eJ:
		case Input::eK:
		case Input::eL:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_A + eKey - Input::eA] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_A + eKey - Input::eA] > 0;

		case Input::eZ:
		case Input::eX:
		case Input::eC:
		case Input::eV:
		case Input::eN:
		case Input::eM:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_Z + eKey - Input::eZ] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_Z + eKey - Input::eZ] > 0;

		case Input::e0:
		case Input::e1:
		case Input::e2:
		case Input::e3:
		case Input::e4:
		case Input::e5:
		case Input::e6:
		case Input::e7:
		case Input::e8:
		case Input::e9:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_1 + eKey - Input::e1] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_1 + eKey - Input::e1] > 0;

		case Input::eNum0:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD0] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_NUMPAD0] > 0;
		case Input::eNum1:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD1] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_NUMPAD1] > 0;
		case Input::eNum2:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD2] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_NUMPAD2] > 0;
		case Input::eNum3:
			return Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD3] > 0;
		case Input::eNum4:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD4] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_NUMPAD4] > 0;
		case Input::eNum5:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD5] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_NUMPAD5] > 0;
		case Input::eNum6:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD6] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_NUMPAD6] > 0;
		case Input::eNum7:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD7] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_NUMPAD7] > 0;
		case Input::eNum8:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD8] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_NUMPAD8] > 0;
		case Input::eNum9:
			return !Input_Inner::gKeyInfo[Input_Inner::gInfoCurrentIndex].btnDown[DIK_NUMPAD9] &&
				Input_Inner::gKeyInfo[Input_Inner::gInfoLastIndex].btnDown[DIK_NUMPAD9] > 0;
		}
		return False;
	}
	Boolean	Input::GetKeyDbClick(Key eKey)
	{
		return False;
	}

	Boolean	Input::GetKeyDown(JoystickKey eKey)
	{
		return Input_Inner::Joystick.bReady ? Input_Inner::Joystick.currentBtnState[eKey] : False;
	}
	Boolean	Input::GetKeyUp(JoystickKey eKey)
	{
		return Input_Inner::Joystick.bReady ? !Input_Inner::Joystick.currentBtnState[eKey] : False;
	}
	Boolean	Input::GetKeyClick(JoystickKey eKey)
	{
		return Input_Inner::Joystick.bReady ?
			!Input_Inner::Joystick.currentBtnState[eKey] && Input_Inner::Joystick.previewBtnState[eKey] :
			False;
	}
	Boolean	Input::GetKeyDbClick(JoystickKey eKey)
	{
		return False;
	}

	Float2	Input::GetJoyStickRelativePosition(JoystickKey eJoystickKey)
	{
		if (eJoystickKey == Input::eJoyStickL)
			return Input_Inner::Joystick.f2Joystick[0];
		else if (eJoystickKey == Input::eJoyStickR)
			return Input_Inner::Joystick.f2Joystick[1];

		return Float2();
	}

	Float2	Input::GetMouseScreenPosition()
	{
		return Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].f2ScreenXY;
	}
	Float2	Input::GetMousePosition()
	{
		Float2 Pos = Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].f2ScreenXY;
#if PLATFORM == PLATFORM_WINDOWS
		HWND hwnd = GetActiveWindow();
		POINT pt = {
			(LONG)Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].f2ScreenXY[0],
			(LONG)Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].f2ScreenXY[1]
		};
		ScreenToClient(hwnd, &pt);
		Pos[0] = pt.x;
		Pos[1] = pt.y;
#endif
		return Pos;
	}
	Float2	Input::GetMouseRelativePosition()
	{
		return Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].f2ScreenXY -
			Input_Inner::gCursorInfo[Input_Inner::gInfoLastIndex].f2ScreenXY;
	}

	Float1	Input::GetMouseWheelRelativePosition()
	{
		return Input_Inner::gCursorInfo[Input_Inner::gInfoCurrentIndex].fWheel;
	}
}
