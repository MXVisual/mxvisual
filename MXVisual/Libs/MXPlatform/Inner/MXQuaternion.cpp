#include "MXQuaternion.h"

#include "assert.h"
#include "math.h"
#include "MXMath.h"
#include "MXConstant.h"

namespace MXVisual
{
	Quaternion::Quaternion()
	{
		m_data[0] = 0;
		m_data[1] = 0;
		m_data[2] = 0;
		m_data[3] = 1;
	}
	Quaternion::Quaternion(Float1 r0, Float1 r1, Float1 r2, Float1 v)
	{
		m_data[0] = r0;
		m_data[1] = r1;
		m_data[2] = r2;
		m_data[3] = v;
	}
	Quaternion::Quaternion(const Quaternion& qOther)
	{
		m_data[0] = qOther.m_data[0];
		m_data[1] = qOther.m_data[1];
		m_data[2] = qOther.m_data[2];
		m_data[3] = qOther.m_data[3];
	}

	Quaternion::~Quaternion() {}

	Boolean Quaternion::operator!=(const Quaternion& qOther)
	{
		if (Math::Abs(m_data[0] - qOther.m_data[0]) < Constant_MIN_FLOAT &&
			Math::Abs(m_data[1] - qOther.m_data[1]) < Constant_MIN_FLOAT &&
			Math::Abs(m_data[2] - qOther.m_data[2]) < Constant_MIN_FLOAT &&
			Math::Abs(m_data[3] - qOther.m_data[3]) < Constant_MIN_FLOAT)
			return True;
		return False;
	}
	const Quaternion& Quaternion::operator=(const Quaternion& qOther)
	{
		m_data[0] = qOther.m_data[0];
		m_data[1] = qOther.m_data[1];
		m_data[2] = qOther.m_data[2];
		m_data[3] = qOther.m_data[3];
		return *this;
	}

	const Quaternion& Quaternion::operator+=(const Quaternion& qOther)
	{
		m_data[0] += qOther.m_data[0];
		m_data[1] += qOther.m_data[1];
		m_data[2] += qOther.m_data[2];
		m_data[3] += qOther.m_data[3];
		return *this;
	}
	const Quaternion& Quaternion::operator-=(const Quaternion& qOther)
	{
		m_data[0] -= qOther.m_data[0];
		m_data[1] -= qOther.m_data[1];
		m_data[2] -= qOther.m_data[2];
		m_data[3] -= qOther.m_data[3];
		return *this;
	}
	const Quaternion& Quaternion::operator*=(const Quaternion& qOther)
	{
		*this = *this * qOther;
		return *this;
	}
	const Quaternion& Quaternion::operator*=(const Float1& fOther)
	{
		m_data[0] *= fOther;
		m_data[1] *= fOther;
		m_data[2] *= fOther;
		m_data[3] *= fOther;
		return *this;
	}

	Quaternion Quaternion::operator+(const Quaternion& qOther)
	{
		return Quaternion(m_data[0] + qOther.m_data[0],
			m_data[1] + qOther.m_data[1],
			m_data[2] + qOther.m_data[2],
			m_data[3] + qOther.m_data[3]);
	}
	Quaternion Quaternion::operator-(const Quaternion& qOther)
	{
		return Quaternion(m_data[0] - qOther.m_data[0],
			m_data[1] - qOther.m_data[1],
			m_data[2] - qOther.m_data[2],
			m_data[3] - qOther.m_data[3]);
	}
	Quaternion Quaternion::operator*(const Quaternion& qOther)
	{
		return Quaternion(
			m_data[3] * qOther.m_data[0] + m_data[0] * qOther.m_data[3] + m_data[2] * qOther.m_data[1] - m_data[1] * qOther.m_data[2],
			m_data[3] * qOther.m_data[1] + m_data[1] * qOther.m_data[3] + m_data[0] * qOther.m_data[2] - m_data[2] * qOther.m_data[0],
			m_data[3] * qOther.m_data[2] + m_data[2] * qOther.m_data[3] + m_data[1] * qOther.m_data[0] - m_data[0] * qOther.m_data[1],
			m_data[3] * qOther.m_data[3] - m_data[0] * qOther.m_data[0] - m_data[1] * qOther.m_data[1] - m_data[2] * qOther.m_data[2]);
	}
	Quaternion Quaternion::operator*(const Float1& fOther)
	{
		return Quaternion(m_data[0] * fOther, m_data[1] * fOther, m_data[2] * fOther, m_data[3] * fOther);
	}

	Float1& Quaternion::operator[](const UInt32 uIndex)
	{
		if (uIndex < 4)
			return m_data[uIndex];
		assert(0);
		return m_data[uIndex];
	}
	const Float1 Quaternion::operator[](const UInt32 uIndex) const
	{
		if (uIndex < 4)
			return m_data[uIndex];
		assert(0);
		return 0;
	}

	const Float1 Quaternion::Module() const
	{
		return (Float1)sqrt(m_data[3] * m_data[3] + m_data[0] * m_data[0] + m_data[1] * m_data[1] + m_data[2] * m_data[2]);
	}

	Quaternion  Quaternion::Normalize(const Quaternion& q)
	{
		Quaternion res = q;
		res *= 1.0f / res.Module();
		return res;
	}
	Float1		Quaternion::DotProduct(const Quaternion& qA, const Quaternion& qB)
	{
		return qA[3] * qB[3] + qA[0] * qB[0] + qA[1] * qB[1] + qA[2] * qB[2];
	}
}