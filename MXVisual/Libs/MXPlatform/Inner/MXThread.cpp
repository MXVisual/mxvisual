#include "MXThread.h"
#include "MXPlatform.h"
#include "MXSystem.h"
#include "MXString.h"
#include "MXArray.h"
#include "MXLog.h"
#if PLATFORM == PLATFORM_WINDOWS
#include "windows.h"
#endif
#include "MXMemory.h"

namespace MXVisual
{
#define STATISTICS_MAX				64

	struct ThreadTask
	{
		String	Name;
		UInt32	Priority = 0;
		std::function<Void(Void)> Execution = nullptr;
	};

	class PlatformThread
	{
	public:
		PlatformThread(const Char* ThreadName
#if PLATFORM == PLATFORM_WINDOWS
			, LPTHREAD_START_ROUTINE ThreadFunc
#endif
		)
			: m_ThreadHandle(INVALID_HANDLE_VALUE)
			, m_strName(ThreadName)
			, m_atomCloseState(0)
		{
#if PLATFORM == PLATFORM_WINDOWS
			m_ThreadHandle = CreateThread(NULL, 0, ThreadFunc, this,
				/*CREATE_SUSPENDED*/0,
				(DWORD*)&m_ThreadID);

			ASSERT_IF_FAILED(m_ThreadHandle != INVALID_HANDLE_VALUE || !"Thread Create Failed");
#endif

#if THREAD_STATISTICS_TURN_ON
			m_TaskStatisticsCount = 0;
#endif
		}
		~PlatformThread()
		{
			m_atomCloseState.Exchange(1);
			WaitForSingleObject(m_ThreadHandle, INFINITE);

			CloseHandle(m_ThreadHandle);

#if THREAD_STATISTICS_TURN_ON
			auto pNode = m_TaskStatistics.Pop(0);
			while (pNode != nullptr)
			{
				delete pNode;
				pNode = m_TaskStatistics.Pop(0);
			}
#endif
		}

		const Char* GetName() const { return m_strName.CString(); }
		Void	AddTask(std::function<Void(Void)> Execution, const Char* szName, UInt32 Priority)
		{
			ASSERT_IF_FAILED(Priority == 0 || !"Priority Not Support Now");
			//Must Call By Main Thread
			m_Mutex.Lock();
			m_Tasklist.PushBack(ThreadTask{ szName, Priority, Execution });
			m_Mutex.UnLock();
		}
		Void	Resume()
		{
#if PLATFORM == PLATFORM_WINDOWS
			ResumeThread(m_ThreadHandle);
#endif
		}
		Void	Pause()
		{
#if PLATFORM == PLATFORM_WINDOWS
			SuspendThread(m_ThreadHandle);
#endif
		}

		ChainNode<ThreadTask>*	PopTask()
		{
			ChainNode<ThreadTask>* Node = nullptr;
			m_Mutex.Lock();

			Node = m_Tasklist.Pop(0);

			m_Mutex.UnLock();

			return Node;
		}

		Void	PushTaskStatistics(ChainNode<ThreadTaskStatistics>* pNode)
		{
			m_Mutex.Lock();
			m_TaskStatistics.PushBack(pNode);
			if (m_TaskStatisticsCount > STATISTICS_MAX)
			{
				delete m_TaskStatistics.Pop(0);
			}
			else
			{
				m_TaskStatisticsCount++;
			}
			m_Mutex.UnLock();
		}
		ChainNode<ThreadTaskStatistics>*	PopTaskStatistics()
		{
			ChainNode<ThreadTaskStatistics>* pNode = nullptr;
			m_Mutex.Lock();
			pNode = m_TaskStatistics.Pop(0);
			if (pNode != nullptr)
				m_TaskStatisticsCount--;
			m_Mutex.UnLock();
			return pNode;
		}

		Boolean	IsTryClosing() { return m_atomCloseState.Value() > 0; }
	protected:
		Atomic<SInt16>				m_atomCloseState;
		Mutex						m_Mutex;
		String						m_strName;
		Chain<ThreadTask>			m_Tasklist;
		Chain<ThreadTaskStatistics> m_TaskStatistics;
		UInt32						m_TaskStatisticsCount;
		Boolean						m_bTryClose;


#if PLATFORM == PLATFORM_WINDOWS
		UInt32	m_ThreadID;
		HANDLE	m_ThreadHandle;
#endif
	};

	namespace
	{
#if PLATFORM == PLATFORM_WINDOWS
		UINT32	MainThreadID;
#endif
		Array<PlatformThread*>	Threads;
		Atomic<SInt16>			StatisticsSwitch(0);
	}


#if PLATFORM == PLATFORM_WINDOWS
	template<UInt32 ThreadID>
	DWORD WINAPI ThreadFunc(LPVOID lpParam)
	{
		PlatformThread* Thread = (PlatformThread*)lpParam;

		ChainNode<ThreadTask>* TaskNode = Thread->PopTask();
		while (1)
		{
			if (TaskNode != nullptr)
			{
				ChainNode<ThreadTaskStatistics>* StatNode = nullptr;
				Boolean bOn = StatisticsSwitch.Value() > 0;
				if (bOn)
				{
					StatNode = new ChainNode<ThreadTaskStatistics>;
					StatNode->Data.Name = TaskNode->Data.Name;
					StatNode->Data.StartTime = (Float1)System::GetTime();
				}
				TaskNode->Data.Execution();
				if (bOn)
				{
					StatNode->Data.EndTime = (Float1)System::GetTime();
					Thread->PushTaskStatistics(StatNode);
				}
				delete TaskNode;
			}
			else if (Thread->IsTryClosing())
			{
				break;
			}
			TaskNode = Thread->PopTask();
		}
		return 0;
	}
#endif

	Boolean ThreadManager::Init()
	{
#if PLATFORM == PLATFORM_WINDOWS
		SYSTEM_INFO si;
		GetSystemInfo(&si);
		si.dwNumberOfProcessors;

		MainThreadID = GetCurrentThreadId();


		Threads.Add(new PlatformThread(RENDER_THREAD_NAME, ThreadFunc<RENDER_THREAD_ID>));
		Threads.Add(new PlatformThread(IO_THREAD_NAME, ThreadFunc<IO_THREAD_ID>));
#endif
		return True;
	}
	Void	ThreadManager::Release()
	{
		for (UInt32 u = 0; u < Threads.ElementCount(); u++)
		{
			delete Threads[u];
		}
	}

	Void ThreadManager::AddThreadTask(
		const Char* ThreadName,
		std::function<Void(Void)> Task, const Char* TaskName, UInt32 Priority)
	{
		ASSERT_IF_FAILED(GetCurrentThreadId() == MainThreadID || !"AddThreadTask Must Call In Main Thread");

		if (strcmp(ThreadName, MAIN_THREAD_NAME) == 0)
		{
			Task();
			return;
		}
		else
		{
			for (UInt32 u = 0; u < Threads.ElementCount(); u++)
			{
				if (strcmp(ThreadName, Threads[u]->GetName()) == 0)
				{
					Threads[u]->AddTask(Task, TaskName, Priority);
					return;
				}
			}
		}

		ASSERT_IF_FAILED(!"Thread Not Found");
	}

	Void ThreadManager::GetThreadStatistics(const Char* ThreadName, Chain<ThreadTaskStatistics>& StatisticChain)
	{
		if (StatisticsSwitch.Value() == 0)return;

		for (UInt32 u = 0; u < Threads.ElementCount(); u++)
		{
			if (strcmp(ThreadName, Threads[u]->GetName()) == 0)
			{
				auto pNode = Threads[u]->PopTaskStatistics();
				while (pNode != nullptr)
				{
					StatisticChain.PushBack(pNode);

					pNode = Threads[u]->PopTaskStatistics();
				}
				return;
			}
		}
	}

	Void	ThreadManager::SetStatisticsSwitch(Boolean bTurnOn)
	{
		StatisticsSwitch.Exchange(bTurnOn ? 1 : 0);
	}
	Boolean	ThreadManager::GetStatisticsSwitch()
	{
		return StatisticsSwitch.Value() == 1;
	}

	Void ThreadManager::Tick()
	{
	}
	Void ThreadManager::ShutDown()
	{

	}
}
