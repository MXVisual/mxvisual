#include "MXLog.h"
#include "MXConstant.h"
#include "MXString.h"
#include "stdio.h"
#if defined(_WIN32)
#if defined(_MSC_VER)
#include "windows.h"
#endif
#endif
namespace MXVisual
{
	namespace Log
	{
		class DefaultOutputCallback : public IOutputCallback
		{
		public:
			Void Print(const Char* szLogMessage)
			{
#if defined(_WIN32)
#if defined(_MSC_VER)
				OutputDebugString(szLogMessage);
#endif
#endif
			}
		};

		DefaultOutputCallback defaultCallback;
		IOutputCallback* g_pDefaultOutputCallback = &defaultCallback;

		IOutputCallback* outputCallback = g_pDefaultOutputCallback;
		Void SetOutputCallback(IOutputCallback* pCallback)
		{
			outputCallback = pCallback;
		}

		Void Output(const Char* szSpeaker, const Char* szMessage)
		{
			String strOutput = String::Format("[%s]:%s\n", szSpeaker, szMessage);
			if (outputCallback != nullptr)
			{
				outputCallback->Print(strOutput.CString());
			}
		}
	}
}
