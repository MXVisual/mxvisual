#include "MXSystem.h"

#if PLATFORM == PLATFORM_WINDOWS
#include "windows.h"
#include "shlobj.h"
#endif
#include "time.h"

namespace MXVisual
{
	LocalTime System::GetLocalTime()
	{
		time_t rawtime;
		struct tm *ptminfo;
		time(&rawtime);
		ptminfo = localtime(&rawtime);

		LocalTime res;
		res.uYear = ptminfo->tm_year + 1900;
		res.uMouth = ptminfo->tm_mon + 1;
		res.uDay = ptminfo->tm_mday;
		res.uHour = ptminfo->tm_hour;
		res.uMinute = ptminfo->tm_min;
		res.uSecond = ptminfo->tm_sec;

		return res;
	}

	Double1 System::GetTime()
	{
		Double1 curTime = 0;
#if defined(WINDOWS)
		LARGE_INTEGER counter;
		LARGE_INTEGER freq;
		//单位是秒，精度是微秒
		QueryPerformanceFrequency(&freq);
		QueryPerformanceCounter(&counter);
		curTime = counter.QuadPart / (Double1)freq.QuadPart;
#endif
		return curTime;
	}

	Void	System::CreateFolder(const Char* szFolderName)
	{
#if defined(_WIN32)
		CreateDirectory(szFolderName, 0);
#endif
	}

	Boolean	System::DlgSelectFolder(const Char* szDlgTitle, Char* szResult, UInt32 uMaxSize)
	{
#if PLATFORM == PLATFORM_WINDOWS
		BROWSEINFO  bi = { 0 };
		bi.hwndOwner = NULL;
		bi.pidlRoot = CSIDL_DESKTOP;
		bi.pszDisplayName = NULL;
		bi.lpszTitle = "Select Project Folder";
		bi.ulFlags = BIF_USENEWUI | BIF_RETURNONLYFSDIRS;
		LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
		if (pidl != NULL)
		{
			SHGetPathFromIDList(pidl, szResult);
			return True;
		}
		return False;
#endif
	}

	Boolean	System::DlgSelectFile(const Char* szDlgTitle, Char* szResult, UInt32 uMaxSize)
	{
#if PLATFORM == PLATFORM_WINDOWS
		OPENFILENAME ofn = { sizeof(OPENFILENAME), 0 };
		ofn.lpstrTitle = szDlgTitle;
		ofn.nMaxFileTitle = (DWORD)strlen(szDlgTitle);
		ofn.lpstrFilter = "All\0*.*\0\0";
		ofn.lpstrFile = szResult;
		ofn.nMaxFile = uMaxSize;
		ofn.lpstrInitialDir = "./";
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

		if (GetOpenFileName(&ofn))
		{
			return True;
		}
		return False;
#endif
	}
}
