#include "MXThread.h"
#include "MXPlatform.h"
#if PLATFORM == PLATFORM_WINDOWS
#include "windows.h"
#endif

namespace MXVisual
{
	Mutex::Mutex()
	{
#if PLATFORM == PLATFORM_WINDOWS
		m_pRealMutex = new CRITICAL_SECTION;
		InitializeCriticalSection((CRITICAL_SECTION*)m_pRealMutex);
#endif
	}
	Mutex::~Mutex()
	{
#if PLATFORM == PLATFORM_WINDOWS
		DeleteCriticalSection((CRITICAL_SECTION*)m_pRealMutex);
		delete m_pRealMutex;
#endif
	}

	Void Mutex::Lock()
	{
#if PLATFORM == PLATFORM_WINDOWS
		EnterCriticalSection((CRITICAL_SECTION*)m_pRealMutex);
#endif
	}
	Void Mutex::UnLock()
	{
#if PLATFORM == PLATFORM_WINDOWS
		LeaveCriticalSection((CRITICAL_SECTION*)m_pRealMutex);
#endif
	}

	template<> Atomic<SInt16>::Atomic(SInt16 value)
	{
		m_pRealAtomic = new SInt16;
		Exchange(value);
	}
	template<> Atomic<SInt16>::~Atomic()
	{
		delete m_pRealAtomic;
	}
	template<> Void Atomic<SInt16>::Increment()
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedIncrement16((SInt16*)m_pRealAtomic);
#endif
	}
	template<> Void Atomic<SInt16>::Decrement()
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedDecrement16((SInt16*)m_pRealAtomic);
#endif
	}
	template<> Void Atomic<SInt16>::Exchange(SInt16 value)
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedExchange16((SInt16*)m_pRealAtomic, value);
#endif
	}
	template<> Void Atomic<SInt16>::CompareExchange(SInt16 compare, SInt16 value)
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedCompareExchange16((SInt16*)m_pRealAtomic, value, compare);
#endif
	}
	template<> SInt16 Atomic<SInt16>::Value()
	{
		return *(SInt16*)m_pRealAtomic;
	}

	template<> Atomic<SInt32>::Atomic(SInt32 value)
	{
		m_pRealAtomic = new SInt32;
		Exchange(value);
	}
	template<> Atomic<SInt32>::~Atomic()
	{
		delete m_pRealAtomic;
	}
	template<> Void Atomic<SInt32>::Increment()
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedIncrement((long*)m_pRealAtomic);
#endif
	}
	template<> Void Atomic<SInt32>::Decrement()
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedDecrement((long*)m_pRealAtomic);
#endif
	}
	template<> Void Atomic<SInt32>::Exchange(SInt32 value)
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedExchange((long*)m_pRealAtomic, (long)value);
#endif
	}
	template<> Void Atomic<SInt32>::CompareExchange(SInt32 compare, SInt32 value)
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedCompareExchange((long*)m_pRealAtomic, (long)value, (long)compare);
#endif
	}
	template<> SInt32 Atomic<SInt32>::Value()
	{
		return *(SInt32*)m_pRealAtomic;
	}

	template<> Atomic<SInt64>::Atomic(SInt64 value)
	{
		m_pRealAtomic = new SInt64;
		Exchange(value);
	}
	template<> Atomic<SInt64>::~Atomic()
	{
		delete m_pRealAtomic;
	}
	template<> Void Atomic<SInt64>::Increment()
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedIncrement64((SInt64*)m_pRealAtomic);
#endif
	}
	template<> Void Atomic<SInt64>::Decrement()
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedDecrement64((SInt64*)m_pRealAtomic);
#endif
	}
	template<> Void Atomic<SInt64>::Exchange(SInt64 value)
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedExchange64((SInt64*)m_pRealAtomic, value);
#endif
	}
	template<> Void Atomic<SInt64>::CompareExchange(SInt64 compare, SInt64 value)
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedCompareExchange64((SInt64*)m_pRealAtomic, value, compare);
#endif
	}
	template<> SInt64 Atomic<SInt64>::Value()
	{
		return *(SInt64*)m_pRealAtomic;
	}

	template<> Atomic<Void*>::Atomic(Void* value)
	{
		Exchange(value);
	}
	template<> Atomic<Void*>::~Atomic()
	{
	}
	template<> Void Atomic<Void*>::Increment()
	{
		ASSERT_IF_FAILED(!"Atomic Pointer Cannot Increment");
	}
	template<> Void Atomic<Void*>::Decrement()
	{
		ASSERT_IF_FAILED(!"Atomic Pointer Cannot Decrement");
	}
	template<> Void Atomic<Void*>::Exchange(Void* value)
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedExchangePointer(&m_pRealAtomic, value);
#endif
	}
	template<> Void Atomic<Void*>::CompareExchange(Void* compare, Void* value)
	{
#if PLATFORM == PLATFORM_WINDOWS
		InterlockedCompareExchangePointer(&m_pRealAtomic, value, compare);
#endif
	}
	template<> Void* Atomic<Void*>::Value()
	{
		return m_pRealAtomic;
	}
}
