#include "MXPlatform.h"
#include "MXMatrix.h"
#include <string.h>
#include <assert.h>

namespace MXVisual
{
	Matrix3 Matrix3::Identity(
		1, 0, 0,
		0, 1, 0,
		0, 0, 1);

	Matrix4 Matrix4::Identity(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);

#pragma region _MATRIX3_

	Matrix3::Matrix3()
	{
		m_data[0][0] = 0;	m_data[0][1] = 0;	m_data[0][2] = 0;
		m_data[1][0] = 0;	m_data[1][1] = 0;	m_data[1][2] = 0;
		m_data[2][0] = 0;	m_data[2][1] = 0;	m_data[2][2] = 0;
	}
	Matrix3::Matrix3(Float1 m11, Float1 m12, Float1 m13,
		Float1 m21, Float1 m22, Float1 m23,
		Float1 m31, Float1 m32, Float1 m33)
	{
		m_data[0][0] = m11;	m_data[0][1] = m12;	m_data[0][2] = m13;
		m_data[1][0] = m21;	m_data[1][1] = m22;	m_data[1][2] = m23;
		m_data[2][0] = m31;	m_data[2][1] = m32;	m_data[2][2] = m33;
	}

	Matrix3::Matrix3(const Matrix3& Other)
	{
		m_data[0][0] = Other.m_data[0][0];	m_data[0][1] = Other.m_data[0][1];	m_data[0][2] = Other.m_data[0][2];
		m_data[1][0] = Other.m_data[1][0];	m_data[1][1] = Other.m_data[1][1];	m_data[1][2] = Other.m_data[1][2];
		m_data[2][0] = Other.m_data[2][0];	m_data[2][1] = Other.m_data[2][1];	m_data[2][2] = Other.m_data[2][2];
	}

	const Matrix3& Matrix3::operator=(const Matrix3& Other)
	{
		m_data[0][0] = Other.m_data[0][0];	m_data[0][1] = Other.m_data[0][1];	m_data[0][2] = Other.m_data[0][2];
		m_data[1][0] = Other.m_data[1][0];	m_data[1][1] = Other.m_data[1][1];	m_data[1][2] = Other.m_data[1][2];
		m_data[2][0] = Other.m_data[2][0];	m_data[2][1] = Other.m_data[2][1];	m_data[2][2] = Other.m_data[2][2];
		return *this;
	}

	const Matrix3& Matrix3::operator*=(const Matrix3& Other)
	{
		Float1 Res[3][3] = { 0 };
		Res[0][0] = m_data[0][0] * Other.m_data[0][0] + m_data[0][1] * Other.m_data[1][0] + m_data[0][2] * Other.m_data[2][0];
		Res[0][1] = m_data[0][0] * Other.m_data[0][1] + m_data[0][1] * Other.m_data[1][1] + m_data[0][2] * Other.m_data[2][1];
		Res[0][2] = m_data[0][0] * Other.m_data[0][2] + m_data[0][1] * Other.m_data[1][2] + m_data[0][2] * Other.m_data[2][2];

		Res[1][0] = m_data[1][0] * Other.m_data[0][0] + m_data[1][1] * Other.m_data[1][0] + m_data[1][2] * Other.m_data[2][0];
		Res[1][1] = m_data[1][0] * Other.m_data[0][1] + m_data[1][1] * Other.m_data[1][1] + m_data[1][2] * Other.m_data[2][1];
		Res[1][2] = m_data[1][0] * Other.m_data[0][2] + m_data[1][1] * Other.m_data[1][2] + m_data[1][2] * Other.m_data[2][2];

		Res[2][0] = m_data[2][0] * Other.m_data[0][0] + m_data[2][1] * Other.m_data[1][0] + m_data[2][2] * Other.m_data[2][0];
		Res[2][1] = m_data[2][0] * Other.m_data[0][1] + m_data[2][1] * Other.m_data[1][1] + m_data[2][2] * Other.m_data[2][1];
		Res[2][2] = m_data[2][0] * Other.m_data[0][2] + m_data[2][1] * Other.m_data[1][2] + m_data[2][2] * Other.m_data[2][2];

		memcpy(m_data, Res, sizeof(m_data));
		return *this;
	}

	Matrix3 Matrix3::operator*(const Matrix3& Other)
	{
		return Matrix3(m_data[0][0] * Other.m_data[0][0] + m_data[0][1] * Other.m_data[1][0] + m_data[0][2] * Other.m_data[2][0],
			m_data[0][0] * Other.m_data[0][1] + m_data[0][1] * Other.m_data[1][1] + m_data[0][2] * Other.m_data[2][1],
			m_data[0][0] * Other.m_data[0][2] + m_data[0][1] * Other.m_data[1][2] + m_data[0][2] * Other.m_data[2][2],

			m_data[1][0] * Other.m_data[0][0] + m_data[1][1] * Other.m_data[1][0] + m_data[1][2] * Other.m_data[2][0],
			m_data[1][0] * Other.m_data[0][1] + m_data[1][1] * Other.m_data[1][1] + m_data[1][2] * Other.m_data[2][1],
			m_data[1][0] * Other.m_data[0][2] + m_data[1][1] * Other.m_data[1][2] + m_data[1][2] * Other.m_data[2][2],

			m_data[2][0] * Other.m_data[0][0] + m_data[2][1] * Other.m_data[1][0] + m_data[2][2] * Other.m_data[2][0],
			m_data[2][0] * Other.m_data[0][1] + m_data[2][1] * Other.m_data[1][1] + m_data[2][2] * Other.m_data[2][1],
			m_data[2][0] * Other.m_data[0][2] + m_data[2][1] * Other.m_data[1][2] + m_data[2][2] * Other.m_data[2][2]
		);
	}

	const Float1 Matrix3::Get(UInt32 uCol, UInt32 uRow) const
	{
		if (uRow < 3 && uCol < 3)
			return m_data[uCol][uRow];
		assert(0 && "Access Out of Bounds");
		return -1;
	}

	Void Matrix3::Set(UInt32 uRow, UInt32 uCol, Float1 fValue)
	{
		if (uRow < 3 && uCol < 3)
			m_data[uCol][uRow] = fValue;
		assert(0 && "Access Out of Bounds ");
	}
#pragma endregion

#pragma region _MATRIX4_
	Matrix4::Matrix4()
	{
		m_data[0][0] = 0; m_data[0][1] = 0; m_data[0][2] = 0; m_data[0][3] = 0;
		m_data[1][0] = 0; m_data[1][1] = 0; m_data[1][2] = 0; m_data[1][3] = 0;
		m_data[2][0] = 0; m_data[2][1] = 0; m_data[2][2] = 0; m_data[2][3] = 0;
		m_data[3][0] = 0; m_data[3][1] = 0; m_data[3][2] = 0; m_data[3][3] = 0;
	}
	Matrix4::Matrix4(Float1 m11, Float1 m12, Float1 m13, Float1 m14,
		Float1 m21, Float1 m22, Float1 m23, Float1 m24,
		Float1 m31, Float1 m32, Float1 m33, Float1 m34,
		Float1 m41, Float1 m42, Float1 m43, Float1 m44)
	{
		m_data[0][0] = m11;	m_data[0][1] = m12;	m_data[0][2] = m13; m_data[0][3] = m14;
		m_data[1][0] = m21;	m_data[1][1] = m22;	m_data[1][2] = m23; m_data[1][3] = m24;
		m_data[2][0] = m31;	m_data[2][1] = m32;	m_data[2][2] = m33; m_data[2][3] = m34;
		m_data[3][0] = m41;	m_data[3][1] = m42;	m_data[3][2] = m43; m_data[3][3] = m44;
	}

	Matrix4::Matrix4(const Matrix4& Other)
	{
		m_data[0][0] = Other.m_data[0][0]; m_data[0][1] = Other.m_data[0][1];
		m_data[0][2] = Other.m_data[0][2]; m_data[0][3] = Other.m_data[0][3];

		m_data[1][0] = Other.m_data[1][0]; m_data[1][1] = Other.m_data[1][1];
		m_data[1][2] = Other.m_data[1][2]; m_data[1][3] = Other.m_data[1][3];

		m_data[2][0] = Other.m_data[2][0]; m_data[2][1] = Other.m_data[2][1];
		m_data[2][2] = Other.m_data[2][2]; m_data[2][3] = Other.m_data[2][3];

		m_data[3][0] = Other.m_data[3][0]; m_data[3][1] = Other.m_data[3][1];
		m_data[3][2] = Other.m_data[3][2]; m_data[3][3] = Other.m_data[3][3];
	}

	const Matrix4& Matrix4::operator=(const Matrix4& Other)
	{
		m_data[0][0] = Other.m_data[0][0]; m_data[0][1] = Other.m_data[0][1];
		m_data[0][2] = Other.m_data[0][2]; m_data[0][3] = Other.m_data[0][3];

		m_data[1][0] = Other.m_data[1][0]; m_data[1][1] = Other.m_data[1][1];
		m_data[1][2] = Other.m_data[1][2]; m_data[1][3] = Other.m_data[1][3];

		m_data[2][0] = Other.m_data[2][0]; m_data[2][1] = Other.m_data[2][1];
		m_data[2][2] = Other.m_data[2][2]; m_data[2][3] = Other.m_data[2][3];

		m_data[3][0] = Other.m_data[3][0]; m_data[3][1] = Other.m_data[3][1];
		m_data[3][2] = Other.m_data[3][2]; m_data[3][3] = Other.m_data[3][3];
		return *this;
	}

	const Matrix4& Matrix4::operator*=(const Matrix4& Other)
	{
		Float1 Res[4][4] = { 0 };
		Res[0][0] = m_data[0][0] * Other.m_data[0][0] +
			m_data[0][1] * Other.m_data[1][0] +
			m_data[0][2] * Other.m_data[2][0] +
			m_data[0][3] * Other.m_data[3][0];

		Res[0][1] = m_data[0][0] * Other.m_data[0][1] +
			m_data[0][1] * Other.m_data[1][1] +
			m_data[0][2] * Other.m_data[2][1] +
			m_data[0][3] * Other.m_data[3][1];

		Res[0][2] = m_data[0][0] * Other.m_data[0][2] +
			m_data[0][1] * Other.m_data[1][2] +
			m_data[0][2] * Other.m_data[2][2] +
			m_data[0][3] * Other.m_data[3][2];

		Res[0][3] = m_data[0][0] * Other.m_data[0][3] +
			m_data[0][1] * Other.m_data[1][3] +
			m_data[0][2] * Other.m_data[2][3] +
			m_data[0][3] * Other.m_data[3][3];

		Res[1][0] = m_data[1][0] * Other.m_data[0][0] +
			m_data[1][1] * Other.m_data[1][0] +
			m_data[1][2] * Other.m_data[2][0] +
			m_data[1][3] * Other.m_data[3][0];

		Res[1][1] = m_data[1][0] * Other.m_data[0][1] +
			m_data[1][1] * Other.m_data[1][1] +
			m_data[1][2] * Other.m_data[2][1] +
			m_data[1][3] * Other.m_data[3][1];

		Res[1][2] = m_data[1][0] * Other.m_data[0][2] +
			m_data[1][1] * Other.m_data[1][2] +
			m_data[1][2] * Other.m_data[2][2] +
			m_data[1][3] * Other.m_data[3][2];

		Res[1][3] = m_data[1][0] * Other.m_data[0][3] +
			m_data[1][1] * Other.m_data[1][3] +
			m_data[1][2] * Other.m_data[2][3] +
			m_data[1][3] * Other.m_data[3][3];

		Res[2][0] = m_data[2][0] * Other.m_data[0][0] +
			m_data[2][1] * Other.m_data[1][0] +
			m_data[2][2] * Other.m_data[2][0] +
			m_data[2][3] * Other.m_data[3][0];

		Res[2][1] = m_data[2][0] * Other.m_data[0][1] +
			m_data[2][1] * Other.m_data[1][1] +
			m_data[2][2] * Other.m_data[2][1] +
			m_data[2][3] * Other.m_data[3][1];

		Res[2][2] = m_data[2][0] * Other.m_data[0][2] +
			m_data[2][1] * Other.m_data[1][2] +
			m_data[2][2] * Other.m_data[2][2] +
			m_data[2][3] * Other.m_data[3][2];

		Res[2][3] = m_data[2][0] * Other.m_data[0][3] +
			m_data[2][1] * Other.m_data[1][3] +
			m_data[2][2] * Other.m_data[2][3] +
			m_data[2][3] * Other.m_data[3][3];

		Res[3][0] = m_data[3][0] * Other.m_data[0][0] +
			m_data[3][1] * Other.m_data[1][0] +
			m_data[3][2] * Other.m_data[2][0] +
			m_data[3][3] * Other.m_data[3][0];

		Res[3][1] = m_data[3][0] * Other.m_data[0][1] +
			m_data[3][1] * Other.m_data[1][1] +
			m_data[3][2] * Other.m_data[2][1] +
			m_data[3][3] * Other.m_data[3][1];

		Res[3][2] = m_data[3][0] * Other.m_data[0][2] +
			m_data[3][1] * Other.m_data[1][2] +
			m_data[3][2] * Other.m_data[2][2] +
			m_data[3][3] * Other.m_data[3][2];

		Res[3][3] = m_data[3][0] * Other.m_data[0][3] +
			m_data[3][1] * Other.m_data[1][3] +
			m_data[3][2] * Other.m_data[2][3] +
			m_data[3][3] * Other.m_data[3][3];

		memcpy(m_data, Res, sizeof(m_data));
		return *this;
	}

	Matrix4 Matrix4::operator*(const Matrix4& Other) const
	{
		return Matrix4(
			m_data[0][0] * Other.m_data[0][0] + m_data[0][1] * Other.m_data[1][0] +
			m_data[0][2] * Other.m_data[2][0] + m_data[0][3] * Other.m_data[3][0],

			m_data[0][0] * Other.m_data[0][1] + m_data[0][1] * Other.m_data[1][1] +
			m_data[0][2] * Other.m_data[2][1] + m_data[0][3] * Other.m_data[3][1],

			m_data[0][0] * Other.m_data[0][2] + m_data[0][1] * Other.m_data[1][2] +
			m_data[0][2] * Other.m_data[2][2] + m_data[0][3] * Other.m_data[3][2],

			m_data[0][0] * Other.m_data[0][3] + m_data[0][1] * Other.m_data[1][3] +
			m_data[0][2] * Other.m_data[2][3] + m_data[0][3] * Other.m_data[3][3],

			m_data[1][0] * Other.m_data[0][0] + m_data[1][1] * Other.m_data[1][0] +
			m_data[1][2] * Other.m_data[2][0] + m_data[1][3] * Other.m_data[3][0],

			m_data[1][0] * Other.m_data[0][1] + m_data[1][1] * Other.m_data[1][1] +
			m_data[1][2] * Other.m_data[2][1] + m_data[1][3] * Other.m_data[3][1],

			m_data[1][0] * Other.m_data[0][2] + m_data[1][1] * Other.m_data[1][2] +
			m_data[1][2] * Other.m_data[2][2] + m_data[1][3] * Other.m_data[3][2],

			m_data[1][0] * Other.m_data[0][3] + m_data[1][1] * Other.m_data[1][3] +
			m_data[1][2] * Other.m_data[2][3] + m_data[1][3] * Other.m_data[3][3],

			m_data[2][0] * Other.m_data[0][0] + m_data[2][1] * Other.m_data[1][0] +
			m_data[2][2] * Other.m_data[2][0] + m_data[2][3] * Other.m_data[3][0],

			m_data[2][0] * Other.m_data[0][1] + m_data[2][1] * Other.m_data[1][1] +
			m_data[2][2] * Other.m_data[2][1] + m_data[2][3] * Other.m_data[3][1],

			m_data[2][0] * Other.m_data[0][2] + m_data[2][1] * Other.m_data[1][2] +
			m_data[2][2] * Other.m_data[2][2] + m_data[2][3] * Other.m_data[3][2],

			m_data[2][0] * Other.m_data[0][3] + m_data[2][1] * Other.m_data[1][3] +
			m_data[2][2] * Other.m_data[2][3] + m_data[2][3] * Other.m_data[3][3],

			m_data[3][0] * Other.m_data[0][0] + m_data[3][1] * Other.m_data[1][0] +
			m_data[3][2] * Other.m_data[2][0] + m_data[3][3] * Other.m_data[3][0],

			m_data[3][0] * Other.m_data[0][1] + m_data[3][1] * Other.m_data[1][1] +
			m_data[3][2] * Other.m_data[2][1] + m_data[3][3] * Other.m_data[3][1],

			m_data[3][0] * Other.m_data[0][2] + m_data[3][1] * Other.m_data[1][2] +
			m_data[3][2] * Other.m_data[2][2] + m_data[3][3] * Other.m_data[3][2],

			m_data[3][0] * Other.m_data[0][3] + m_data[3][1] * Other.m_data[1][3] +
			m_data[3][2] * Other.m_data[2][3] + m_data[3][3] * Other.m_data[3][3]
		);
	}

	const Float1 Matrix4::Get(UInt32 uRow, UInt32 uCol) const
	{
		ASSERT_IF_FAILED(uRow < 4 && uCol < 4 && "Access Out of Bounds ");

		if (uRow < 4 && uCol < 4)
			return m_data[uRow][uCol];

		return -1;
	}

	Void Matrix4::Set(UInt32 uRow, UInt32 uCol, Float1 fValue)
	{
		ASSERT_IF_FAILED(uRow < 4 && uCol < 4 && "Access Out of Bounds ");

		if (uRow < 4 && uCol < 4)
			m_data[uRow][uCol] = fValue;
	}

#pragma endregion
}