#include "MXTimer.h"
#include "MXSystem.h"
#if defined(WINDOWS)
#include "windows.h"
#endif

namespace MXVisual
{
	namespace Timer
	{
		Double1 m_dTime = 0.0f;
		Double1	m_dTimeNoScale = 0.0f;
		Double1 m_dDeltaTimeNoScale = 0.0f;
		Float1	m_fScale = 1.0f;

		Void Reset()
		{
			m_dTime = 0.0f;
			m_dTimeNoScale = 0.0f;
			m_dDeltaTimeNoScale = 0.0f;
			m_fScale = 1.0f;
		}

		Void Update(Float1 fTargetDeltaTime)
		{
			Double1 curTime = System::GetTime();

			m_dDeltaTimeNoScale = curTime - m_dTimeNoScale;
			m_dTimeNoScale = curTime;
			m_dTime += m_dDeltaTimeNoScale * m_fScale;

			if (fTargetDeltaTime > 0 && fTargetDeltaTime > m_dDeltaTimeNoScale)
			{
#if defined(WINDOWS)
				DWORD dwWaitTime = fTargetDeltaTime * 1e3f - (Float1)m_dDeltaTimeNoScale * 1e3f;
				Sleep(dwWaitTime);
#endif
			}
		}

		Void SetTimeScale(Float1 fScale)
		{
			m_fScale = fScale;
		}

		Double1 GetTime()
		{
			return m_dTime;
		}
		Double1 GetTimeNoScale()
		{
			return m_dTimeNoScale;
		}
		Double1 GetDeltaTime()
		{
			return m_dDeltaTimeNoScale * m_fScale;
		}
		Double1 GetDeltaTimeNoScale()
		{
			return m_dDeltaTimeNoScale;
		}
	}

}