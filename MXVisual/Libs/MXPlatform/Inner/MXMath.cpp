#include "MXMath.h"
#include "MXConstant.h"
#include "MXArray.h"

#include "assert.h"
#include "math.h"
#include "stdlib.h"

#include "string.h"

//逆矩阵 临时使用DirectX Math
#include <DirectXMath.h>

#include "FFTW/fftw3.h"

namespace MXVisual
{
	namespace Math
	{
		const Float3 AXIS_X(1, 0, 0);
		const Float3 AXIS_Y(0, 1, 0);
		const Float3 AXIS_Z(0, 0, 1);

		const Float3 AXIS_X_Inverse(-1, 0, 0);
		const Float3 AXIS_Y_Inverse(0, -1, 0);
		const Float3 AXIS_Z_Inverse(0, 0, -1);

		Float1 Power(Float1 fValue, Float1 fRate)
		{
			return (Float1)pow(fValue, fRate);
		}

		Float1 Random01()
		{
			const SInt32 nMax = 10240;
			Float1 f = 1.0f / nMax;
			SInt32 n = rand() % nMax;
			return n * f;
		}

		UInt32 AlignmentedSize(UInt32 uOrgSize, UInt32 uAlignSize)
		{
			return Math::Ceil(uOrgSize / (Float1)uAlignSize) * uAlignSize;
		}

		template<>
		SInt32 Min(const SInt32& nA, const SInt32& nB)
		{
			return 	nA > nB ? nB : nA;
		}
		template<>
		SInt32 Max(const SInt32& nA, const SInt32& nB)
		{
			return nA > nB ? nA : nB;
		}
		template<>
		SInt32 Clamp(const SInt32& nA, const SInt32& nMin, const SInt32& nMax)
		{
			return Max(Min(nA, nMax), nMin);
		}

		template<>
		Float1 Min(const Float1& fA, const Float1& fB)
		{
			return 	fA > fB ? fB : fA;
		}
		template<>
		Float1 Max(const Float1& fA, const Float1& fB)
		{
			return fA > fB ? fA : fB;
		}
		template<>
		Float1 Clamp(const Float1& fA, const Float1& fMin, const Float1& fMax)
		{
			return Max(Min(fA, fMax), fMin);
		}
		template<>
		Float1 Lerp(const Float1& fA, const Float1& fB, Float1 fT)
		{
			fT = Min(fT, 1.0f);
			Float1 a = fA;
			Float1 b = fB;
			return a * (1 - fT) + b * fT;
		}

		template<>
		Float2 Min(const Float2& f2A, const Float2& f2B)
		{
			return Float2(
				f2A[0] > f2B[0] ? f2B[0] : f2A[0],
				f2A[1] > f2B[1] ? f2B[1] : f2A[1]
			);
		}
		template<>
		Float2 Max(const Float2& f2A, const Float2& f2B)
		{
			return Float2(
				f2A[0] > f2B[0] ? f2A[0] : f2B[0],
				f2A[1] > f2B[1] ? f2A[1] : f2B[1]
			);
		}
		template<>
		Float2 Clamp(const Float2& f2A, const Float2& f2Min, const Float2& f2Max)
		{
			return Max(Min(f2A, f2Max), f2Min);
		}
		template<>
		Float2 Lerp(const Float2& f2A, const Float2& f2B, Float1 fT)
		{
			fT = Min(fT, 1.0f);
			Float2 a = f2A;
			Float2 b = f2B;
			return a * (1 - fT) + b * fT;
		}

		template<>
		Float3 Min(const Float3& f3A, const Float3& f3B)
		{
			return Float3(
				f3A[0] > f3B[0] ? f3B[0] : f3A[0],
				f3A[1] > f3B[1] ? f3B[1] : f3A[1],
				f3A[2] > f3B[2] ? f3B[2] : f3A[2]
			);
		}
		template<>
		Float3 Max(const Float3& f3A, const Float3& f3B)
		{
			return Float3(
				f3A[0] > f3B[0] ? f3A[0] : f3B[0],
				f3A[1] > f3B[1] ? f3A[1] : f3B[1],
				f3A[2] > f3B[2] ? f3A[2] : f3B[2]
			);
		}
		template<>
		Float3 Clamp(const Float3& f3A, const Float3& f3Min, const Float3& f3Max)
		{
			return Max(Min(f3A, f3Max), f3Min);
		}
		template<>
		Float3 Lerp(const Float3& f3A, const Float3& f3B, Float1 fT)
		{
			fT = Min(fT, 1.0f);
			Float3 a = f3A;
			Float3 b = f3B;
			return a * (1 - fT) + b * fT;
		}

		template<>
		Float4 Min(const Float4& f4A, const Float4& f4B)
		{
			return Float4(
				f4A[0] > f4B[0] ? f4B[0] : f4A[0],
				f4A[1] > f4B[1] ? f4B[1] : f4A[1],
				f4A[2] > f4B[2] ? f4B[2] : f4A[2],
				f4A[3] > f4B[3] ? f4B[3] : f4A[3]
			);
		}
		template<>
		Float4 Max(const Float4& f4A, const Float4& f4B)
		{
			return Float4(
				f4A[0] > f4B[0] ? f4A[0] : f4B[0],
				f4A[1] > f4B[1] ? f4A[1] : f4B[1],
				f4A[2] > f4B[2] ? f4A[2] : f4B[2],
				f4A[3] > f4B[3] ? f4A[3] : f4B[3]
			);
		}
		template<>
		Float4 Clamp(const Float4& f4A, const Float4& f4Min, const Float4& f4Max)
		{
			return Max(Min(f4A, f4Max), f4Min);
		}
		template<>
		Float4 Lerp(const Float4& f4A, const Float4& f4B, Float1 fT)
		{
			fT = Min(fT, 1.0f);
			Float4 a = f4A;
			Float4 b = f4B;
			return a * (1 - fT) + b * fT;
		}

		Quaternion	SLerp(const Quaternion& qA, const Quaternion& qB, Float1 fT)
		{
			//应该取最小角度
			if (fT < Constant_MIN_FLOAT)return qA;
			if (fT > (1.0f - Constant_MIN_FLOAT)) return qB;

			Quaternion Res;

			Float1 dotRes = Quaternion::DotProduct(qA, qB);

			Quaternion qLeft = Quaternion::Normalize(qA);
			Quaternion qRight = Quaternion::Normalize(qB);
			if (dotRes < 0.0f)
			{
				qRight *= -1;
				dotRes *= -1;
			}

			Float1 mod = qLeft.Module() * qRight.Module();
			if (mod < Constant_MIN_FLOAT) mod = 1.0f;

			Float1 r = dotRes / mod;

			Float1 theta = r > (1.0f - Constant_MIN_FLOAT) ? 0.0f : acosf(r);

			if (theta < Constant_MIN_FLOAT)return qLeft;

			Float1 sinRes = Math::Max(sinf(theta), 0.00001f);

			qLeft *= sinf((1 - fT) * theta) / sinRes;
			qRight *= sinf(fT * theta) / sinRes;

			Res = qLeft + qRight;
			return Res;

		}

		Float1 Gaussian(Float1 fPos, Float1 fMax, Float1 fCenter, Float1 fStandardDeviation)
		{
			Float1 e = -(fPos - fCenter) * (fPos - fCenter);
			e /= 2.0f * fStandardDeviation * fStandardDeviation;
			return fMax * exp(e) / sqrtf(2.0f * Constant_PI * fStandardDeviation);
		}
		Float1 Gaussian(const Float2& f2Pos, Float1 fMax, const Float2& f2Center, const Float2& f2StandardDeviation)
		{
			Float1 ex = -(f2Pos[0] - f2Center[0]) * (f2Pos[0] - f2Center[0]);
			ex /= 2.0f * f2StandardDeviation[0] * f2StandardDeviation[0];
			Float1 ey = -(f2Pos[1] - f2Center[1]) * (f2Pos[1] - f2Center[1]);
			ey /= 2.0f * f2StandardDeviation[1] * f2StandardDeviation[1];
			return fMax * exp(ex + ey) / (2.0f * Constant_PI * f2StandardDeviation[0] * f2StandardDeviation[1]);
		}

		Matrix4 MatrixTranspose(const Matrix4 & mat)
		{
			return Matrix4(
				mat.Get(0, 0), mat.Get(1, 0), mat.Get(2, 0), mat.Get(3, 0),
				mat.Get(0, 1), mat.Get(1, 1), mat.Get(2, 1), mat.Get(3, 1),
				mat.Get(0, 2), mat.Get(1, 2), mat.Get(2, 2), mat.Get(3, 2),
				mat.Get(0, 3), mat.Get(1, 3), mat.Get(2, 3), mat.Get(3, 3)
			);
		}
		Matrix4 MatrixInverse(const Matrix4& mat)
		{
			DirectX::XMMATRIX dxmat(
				mat.Get(0, 0), mat.Get(0, 1), mat.Get(0, 2), mat.Get(0, 3),
				mat.Get(1, 0), mat.Get(1, 1), mat.Get(1, 2), mat.Get(1, 3),
				mat.Get(2, 0), mat.Get(2, 1), mat.Get(2, 2), mat.Get(2, 3),
				mat.Get(3, 0), mat.Get(3, 1), mat.Get(3, 2), mat.Get(3, 3));

			DirectX::XMVECTOR deter = DirectX::XMMatrixDeterminant(dxmat);
			dxmat = DirectX::XMMatrixInverse(&deter, dxmat);

			DirectX::XMFLOAT4X4 data;
			DirectX::XMStoreFloat4x4(&data, dxmat);

			return Matrix4(
				data.m[0][0], data.m[0][1], data.m[0][2], data.m[0][3],
				data.m[1][0], data.m[1][1], data.m[1][2], data.m[1][3],
				data.m[2][0], data.m[2][1], data.m[2][2], data.m[2][3],
				data.m[3][0], data.m[3][1], data.m[3][2], data.m[3][3]);
		}

		Matrix4 MatrixClipToViewport(
			const Float2& offset,
			const Float2& size,
			const Float2& depthRange)
		{
			return Matrix4(
				size[0] * 0.5, 0, 0, 0,
				0, -size[1] * 0.5, 0, 0,
				0, 0, depthRange[1] - depthRange[0], 0,
				offset[0] + size[0] * 0.5, offset[1] + size[1] * 0.5, depthRange[0], 1
			);
		}

		Matrix4 MatrixTranslation(const Float3& f3Offset)
		{
			return Matrix4(
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				f3Offset[0], f3Offset[1], f3Offset[2], 1
			);
		}
		Matrix4 MatrixScale(const Float3& f3Scale)
		{
			return Matrix4(
				f3Scale[0], 0, 0, 0,
				0, f3Scale[1], 0, 0,
				0, 0, f3Scale[2], 0,
				0, 0, 0, 1
			);
		}
		Matrix4 MatrixAxisRotation(const Float3& f3Axis, Float1 fRadius)
		{
			Float1 fCosA = (Float1)cosf(fRadius);
			Float1 fSinA = (Float1)sinf(fRadius);

			Float1 nx2 = f3Axis[0] * f3Axis[0];
			Float1 nxny = f3Axis[0] * f3Axis[1];
			Float1 ny2 = f3Axis[1] * f3Axis[1];
			Float1 nynz = f3Axis[1] * f3Axis[2];
			Float1 nz2 = f3Axis[2] * f3Axis[2];
			Float1 nznx = f3Axis[2] * f3Axis[0];

			Float1 one_cos = 1 - fCosA;

			return Matrix4(
				nx2 * one_cos + fCosA, nxny * one_cos - f3Axis[2] * fSinA, nznx * one_cos + f3Axis[1] * fSinA, 0,
				nxny * one_cos + f3Axis[2] * fSinA, ny2 * one_cos + fCosA, nynz * one_cos - f3Axis[0] * fSinA, 0,
				nznx * one_cos - f3Axis[1] * fSinA, nynz * one_cos + f3Axis[0] * fSinA, nz2 * one_cos + fCosA, 0,
				0, 0, 0, 1
			);
		}
		Matrix4	MatrixQuaternion(const Quaternion& quaternion)
		{
			Matrix4 res;

			Float1 x2 = quaternion[0] * quaternion[0];
			Float1 y2 = quaternion[1] * quaternion[1];
			Float1 z2 = quaternion[2] * quaternion[2];
			Float1 w2 = quaternion[3] * quaternion[3];

			Float1 xy = quaternion[0] * quaternion[1];
			Float1 xz = quaternion[0] * quaternion[2];
			Float1 xw = quaternion[0] * quaternion[3];

			Float1 yz = quaternion[1] * quaternion[2];
			Float1 yw = quaternion[1] * quaternion[3];

			Float1 zw = quaternion[2] * quaternion[3];

			res = Matrix4(
				1.0f - 2.0f * y2 - 2.0f * z2, 2.0f * xy + 2.0f * zw, 2.0f * xz - 2.0f * yw, 0.0f,
				2.0f * xy - 2.0f * zw, 1.0f - 2.0f * x2 - 2.0f * z2, 2.0f * yz + 2.0f * xw, 0.0f,
				2.0f * xz + 2.0f * yw, 2.0f * yz - 2.0f * xw, 1.0f - 2.0f * x2 - 2.0f * y2, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			);

			return res;
		}

		Float3  QuaternionToEular(const Quaternion& quaternion)
		{
			Float3 Res;
			Res[0] = atan2(2.0f * (quaternion[3] * quaternion[0] + quaternion[1] * quaternion[2]),
				1.0f - 2.0f * (quaternion[0] * quaternion[0] + quaternion[1] * quaternion[1]));

			Res[1] = asin(2.0f * (quaternion[3] * quaternion[1] - quaternion[0] * quaternion[2]));

			Res[2] = atan2(2.0f * (quaternion[3] * quaternion[2] + quaternion[0] * quaternion[1]),
				1.0f - 2.0f * (quaternion[1] * quaternion[1] + quaternion[2] * quaternion[2]));

			return Res;
		}

		Quaternion  EularToQuaternion(const Float3& f3Eular)
		{
			Float1 X = f3Eular[0];
			Float1 Y = f3Eular[1];
			Float1 Z = f3Eular[2];
			Float1 x = cosf(Y / 2) * sinf(X / 2) * cosf(Z / 2) + sinf(Y / 2) * cosf(X / 2) * sinf(Z / 2);
			Float1 y = sinf(Y / 2) * cosf(X / 2) * cosf(Z / 2) - cosf(Y / 2) * sinf(X / 2) * sinf(Z / 2);
			Float1 z = cosf(Y / 2) * cosf(X / 2) * sinf(Z / 2) - sinf(Y / 2) * sinf(X / 2) * cosf(Z / 2);
			Float1 w = cosf(Y / 2) * cosf(X / 2) * cosf(Z / 2) + sinf(Y / 2) * sinf(X / 2) * sinf(Z / 2);
			return Quaternion(x, y, z, w);
		}
		Quaternion	VectorToVectorQuaternion(const Float3& f3Vec0, const Float3& f3Vec1)
		{
			Float3 v = Math::CrossProduct(f3Vec0, f3Vec1);
			Float1 c = Math::DotProduct(f3Vec0, f3Vec1);
			Float1 k = (Float1)sqrt(f3Vec0.Module() * f3Vec1.Module());
			if (Math::Abs(k) < Constant_MIN_FLOAT || Math::Abs(c / k + 1) < Constant_MIN_FLOAT)//k == 0 || c/k == -1
			{
				return Quaternion();
			}
			Float1 w = k + c;
			return Quaternion::Normalize(Quaternion(v[0], v[1], v[2], w));
		}
		Float1 VectorToVectorAngle(const Float3& f3Vec0, const Float3& f3Vec1)
		{
			Float1 cosRes = DotProduct(
				Math::Normalize(f3Vec0),
				Math::Normalize(f3Vec1));

			Float1 fCosAngle = (Float1)acos(cosRes);

			return fCosAngle;
		}

		Matrix4 MatrixPerspectiveProjection(Float1 fFovW, Float1 fAspectWH, Float1 fNear, Float1 fFar)
		{
			Float1 cotW = 1.0f / (Float1)tan(fFovW / 2);
			Float1 fFmN = fFar - fNear;
			/*Reverse Z
			return MiXMatrix4(
				cotW, 0, 0, 0,
				0, cotW / fAspectHW, 0, 0,
				0, 0, fNear / fNmF, 1,
				0, 0, -fFar * fNear / fNmF, 0
			);
			*/

			return Matrix4(
				cotW, 0, 0, 0,
				0, cotW * fAspectWH, 0, 0,
				0, 0, fFar / fFmN, 1,
				0, 0, -fFar * fNear / fFmN, 0
			);
		}
		Matrix4 MatrixOrthographicProjection(Float1 fWidth, Float1 fHeight, Float1 fNear, Float1 fFar)
		{
			return Matrix4(
				2.0f / fWidth, 0, 0, 0,
				0, 2.0f / fHeight, 0, 0,
				0, 0, 1 / (fFar - fNear), 0,
				0, 0, -fNear / (fFar - fNear), 1
			);
		}

		Matrix4 MatrixView(const Float3 & f3Pos, const Float3 & f3Forward, const Float3 & f3Up)
		{
			Matrix4 T(
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				-f3Pos[0], -f3Pos[1], -f3Pos[2], 1
			);

			Float3 right = CrossProduct(f3Up, f3Forward);

			Float3 forward = Normalize(f3Forward);
			Float3 up = Normalize(f3Up);
			right = Normalize(right);

			return T * Matrix4(
				right[0], up[0], forward[0], 0,
				right[1], up[1], forward[1], 0,
				right[2], up[2], forward[2], 0,
				0, 0, 0, 1
			);
		}

		Float3 MatrixGetCameraPosition(const Matrix4& mat)
		{
			Float3 res;
			res[0] = -mat.Get(3, 0) * mat.Get(0, 0) - mat.Get(3, 1) * mat.Get(0, 1) - mat.Get(3, 2) * mat.Get(0, 2);
			res[1] = -mat.Get(3, 0) * mat.Get(1, 0) - mat.Get(3, 1) * mat.Get(1, 1) - mat.Get(3, 2) * mat.Get(1, 2);
			res[2] = -mat.Get(3, 0) * mat.Get(2, 0) - mat.Get(3, 1) * mat.Get(2, 1) - mat.Get(3, 2) * mat.Get(2, 2);
			return res;
		}

		Float3 MatrixTransform(const Float3 & f3A, const Matrix3 & mat3B)
		{
			return Float3(
				f3A[0] * mat3B.Get(0, 0) + f3A[1] * mat3B.Get(1, 0) + f3A[2] * mat3B.Get(2, 0),
				f3A[0] * mat3B.Get(0, 1) + f3A[1] * mat3B.Get(1, 1) + f3A[2] * mat3B.Get(2, 1),
				f3A[0] * mat3B.Get(0, 2) + f3A[1] * mat3B.Get(1, 2) + f3A[2] * mat3B.Get(2, 2)
			);
		}

		Float4 MatrixTransform(const Float4 & f4A, const Matrix4 & mat4B)
		{
			return Float4(
				f4A[0] * mat4B.Get(0, 0) + f4A[1] * mat4B.Get(1, 0) + f4A[2] * mat4B.Get(2, 0) + f4A[3] * mat4B.Get(3, 0),
				f4A[0] * mat4B.Get(0, 1) + f4A[1] * mat4B.Get(1, 1) + f4A[2] * mat4B.Get(2, 1) + f4A[3] * mat4B.Get(3, 1),
				f4A[0] * mat4B.Get(0, 2) + f4A[1] * mat4B.Get(1, 2) + f4A[2] * mat4B.Get(2, 2) + f4A[3] * mat4B.Get(3, 2),
				f4A[0] * mat4B.Get(0, 3) + f4A[1] * mat4B.Get(1, 3) + f4A[2] * mat4B.Get(2, 3) + f4A[3] * mat4B.Get(3, 3)
			);
		}

		Float1 Length(const Float2& f2Value)
		{
			return (Float1)sqrt(f2Value[0] * f2Value[0]
				+ f2Value[1] * f2Value[1]);
		}
		Float1 Length(const Float3& f3Value)
		{
			return (Float1)sqrt(f3Value[0] * f3Value[0]
				+ f3Value[1] * f3Value[1]
				+ f3Value[2] * f3Value[2]);
		}
		Float1 Length(const Float4& f4Value)
		{
			return (Float1)sqrt(f4Value[0] * f4Value[0]
				+ f4Value[1] * f4Value[1]
				+ f4Value[2] * f4Value[2]
				+ f4Value[3] * f4Value[3]);
		}

		Float2 Normalize(const Float2& f2Value)
		{
			Float2 ret = f2Value;
			Float1 l = Length(f2Value);
			l = Max(Constant_MIN_FLOAT, l);
			return ret / l;
		}
		Float3 Normalize(const Float3& f3Value)
		{
			Float3 ret = f3Value;
			Float1 l = Length(f3Value);
			l = Max(Constant_MIN_FLOAT, l);
			return ret / l;
		}
		Float4 Normalize(const Float4& f4Value)
		{
			Float4 ret = f4Value;
			Float1 l = Length(f4Value);
			l = Max(Constant_MIN_FLOAT, l);
			return ret / l;
		}

		Float3 MatrixGetRotation(const Matrix3 & mat)
		{
			Float3 scale = MatrixGetScale(mat);

			//from https://www.geometrictools.com/Documentation/EulerAngles.pdf
			//Rx Ry Rz

			Float1 AngleY = 0;
			Float1 AngleX = 0;
			Float1 AngleZ = 0;

			Float1 r02 = mat.Get(0, 2) / scale[2];
			if (r02 < 1.0f)
			{
				if (r02 > -1.0f)
				{
					Float1 r12 = mat.Get(1, 2) / scale[2];
					Float1 r22 = mat.Get(2, 2) / scale[2];
					Float1 r01 = mat.Get(0, 1) / scale[1];
					Float1 r00 = mat.Get(0, 0) / scale[0];

					AngleY = (Float1)asin(r02);
					AngleX = (Float1)atan2(-r12, r22);
					AngleZ = (Float1)atan2(-r01, r00);
				}
				else
				{
					Float1 r10 = mat.Get(1, 0) / scale[0];
					Float1 r11 = mat.Get(1, 1) / scale[1];

					AngleY = -Constant_PI / 2.0f;
					AngleX = (Float1)-atan2(r10, r11);
					AngleZ = 0;
				}
			}
			else
			{
				Float1 r10 = mat.Get(1, 0) / scale[0];
				Float1 r11 = mat.Get(1, 1) / scale[1];

				AngleY = Constant_PI / 2.0f;
				AngleX = (Float1)atan2(r10, r11);
				AngleZ = 0;
			}

			return Float3(-AngleX, -AngleY, -AngleZ);
		}

		Quaternion MatrixGetQuaternion(const Matrix4& mat)
		{
			Float3 scale = MatrixGetScale(mat);

			Matrix4 matNoScale(
				mat.Get(0, 0) / scale[0], mat.Get(0, 1) / scale[0], mat.Get(0, 2) / scale[0], mat.Get(0, 3),
				mat.Get(1, 0) / scale[1], mat.Get(1, 1) / scale[1], mat.Get(1, 2) / scale[1], mat.Get(1, 3),
				mat.Get(2, 0) / scale[2], mat.Get(2, 1) / scale[2], mat.Get(2, 2) / scale[2], mat.Get(2, 3),
				mat.Get(3, 0), mat.Get(3, 1), mat.Get(3, 2), mat.Get(3, 3)
			);

			Quaternion res;

			//探测四元数中最大的项 
			Float1 fourWSquaredMinusl = matNoScale.Get(0, 0) + matNoScale.Get(1, 1) + matNoScale.Get(2, 2);
			Float1 fourXSquaredMinusl = matNoScale.Get(0, 0) - matNoScale.Get(1, 1) - matNoScale.Get(2, 2);
			Float1 fourYSquaredMinusl = matNoScale.Get(1, 1) - matNoScale.Get(0, 0) - matNoScale.Get(2, 2);
			Float1 fourZSquaredMinusl = matNoScale.Get(2, 2) - matNoScale.Get(0, 0) - matNoScale.Get(1, 1);

			int biggestIndex = 0;
			Float1 fourBiggestSqureMinus1 = fourWSquaredMinusl;
			if (fourXSquaredMinusl > fourBiggestSqureMinus1) {
				fourBiggestSqureMinus1 = fourXSquaredMinusl;
				biggestIndex = 1;
			}
			if (fourYSquaredMinusl > fourBiggestSqureMinus1) {
				fourBiggestSqureMinus1 = fourYSquaredMinusl;
				biggestIndex = 2;
			}
			if (fourZSquaredMinusl > fourBiggestSqureMinus1) {
				fourBiggestSqureMinus1 = fourZSquaredMinusl;
				biggestIndex = 3;
			}

			//计算平方根和除法 
			Float1 biggestVal = (Float1)sqrt(fourBiggestSqureMinus1 + 1.0f) * 0.5f;
			Float1 mult = 0.25f / biggestVal;

			//计算四元数的值
			switch (biggestIndex) {
			case 0:
				res[3] = biggestVal;
				res[0] = (matNoScale.Get(1, 2) - matNoScale.Get(2, 1))*mult;
				res[1] = (matNoScale.Get(2, 0) - matNoScale.Get(0, 2))*mult;
				res[2] = (matNoScale.Get(0, 1) - matNoScale.Get(1, 0))*mult;
				break;
			case 1:
				res[0] = biggestVal;
				res[3] = (matNoScale.Get(1, 2) - matNoScale.Get(2, 1))*mult;
				res[1] = (matNoScale.Get(0, 1) + matNoScale.Get(1, 0))*mult;
				res[2] = (matNoScale.Get(2, 0) + matNoScale.Get(0, 2))*mult;
				break;
			case 2:
				res[1] = biggestVal;
				res[3] = (matNoScale.Get(2, 0) - matNoScale.Get(0, 2))*mult;
				res[0] = (matNoScale.Get(0, 1) + matNoScale.Get(1, 0))*mult;
				res[2] = (matNoScale.Get(1, 2) + matNoScale.Get(2, 1))*mult;
				break;
			case 3:
				res[2] = biggestVal;
				res[3] = (matNoScale.Get(0, 1) - matNoScale.Get(1, 0))*mult;
				res[0] = (matNoScale.Get(2, 0) + matNoScale.Get(0, 2))*mult;
				res[1] = (matNoScale.Get(1, 2) + matNoScale.Get(2, 1))*mult;
				break;
			}

			return res;
		}

		Float3 MatrixGetScale(const Matrix3 & mat)
		{
			Float1 Sx = (Float1)sqrt(mat.Get(0, 0) * mat.Get(0, 0) + mat.Get(1, 0) * mat.Get(1, 0) + mat.Get(2, 0) * mat.Get(2, 0));
			Float1 Sy = (Float1)sqrt(mat.Get(0, 1) * mat.Get(0, 1) + mat.Get(1, 1) * mat.Get(1, 1) + mat.Get(2, 1) * mat.Get(2, 1));
			Float1 Sz = (Float1)sqrt(mat.Get(0, 2) * mat.Get(0, 2) + mat.Get(1, 2) * mat.Get(1, 2) + mat.Get(2, 2) * mat.Get(2, 2));

			return Float3(Sx, Sy, Sz);
		}

		Float3 MatrixGetTranslation(const Matrix4 & mat)
		{
			return Float3(mat.Get(3, 0), mat.Get(3, 1), mat.Get(3, 2));
		}

		Float3 MatrixGetRotation(const Matrix4 & mat)
		{
			Float3 scale = MatrixGetScale(mat);

			//from https://www.geometrictools.com/Documentation/EulerAngles.pdf
			//Rx Ry Rz

			Float1 AngleY = 0;
			Float1 AngleX = 0;
			Float1 AngleZ = 0;

			Float1 r02 = mat.Get(0, 2) / scale[2];
			if (r02 < 1.0f)
			{
				if (r02 > -1.0f)
				{
					Float1 r12 = mat.Get(1, 2) / scale[2];
					Float1 r22 = mat.Get(2, 2) / scale[2];
					Float1 r01 = mat.Get(0, 1) / scale[1];
					Float1 r00 = mat.Get(0, 0) / scale[0];

					AngleY = (Float1)asin(r02);
					AngleX = (Float1)atan2(-r12, r22);
					AngleZ = (Float1)atan2(-r01, r00);
				}
				else
				{
					Float1 r10 = mat.Get(1, 0) / scale[0];
					Float1 r11 = mat.Get(1, 1) / scale[1];

					AngleY = -Constant_PI / 2.0f;
					AngleX = (Float1)-atan2(r10, r11);
					AngleZ = 0;
				}
			}
			else
			{
				Float1 r10 = mat.Get(1, 0) / scale[0];
				Float1 r11 = mat.Get(1, 1) / scale[1];

				AngleY = Constant_PI / 2.0f;
				AngleX = (Float1)atan2(r10, r11);
				AngleZ = 0;
			}

			return Float3(-AngleX, -AngleY, -AngleZ);
		}

		Float3 MatrixGetScale(const Matrix4 & mat)
		{
			Float1 Sx = (Float1)sqrt(mat.Get(0, 0) * mat.Get(0, 0) + mat.Get(0, 1) * mat.Get(0, 1) + mat.Get(0, 2) * mat.Get(0, 2));
			Float1 Sy = (Float1)sqrt(mat.Get(1, 0) * mat.Get(1, 0) + mat.Get(1, 1) * mat.Get(1, 1) + mat.Get(1, 2) * mat.Get(1, 2));
			Float1 Sz = (Float1)sqrt(mat.Get(2, 0) * mat.Get(2, 0) + mat.Get(2, 1) * mat.Get(2, 1) + mat.Get(2, 2) * mat.Get(2, 2));

			return Float3(Sx, Sy, Sz);
		}

		Float3 CrossProduct(const Float3& f3A, const Float3& f3B)
		{
			return Float3(
				f3A[1] * f3B[2] - f3A[2] * f3B[1],
				f3A[2] * f3B[0] - f3A[0] * f3B[2],
				f3A[0] * f3B[1] - f3A[1] * f3B[0]
			);
		}
		Float1 DotProduct(const Float3& f3A, const Float3& f3B)
		{
			return f3A[0] * f3B[0] + f3A[1] * f3B[1] + f3A[2] * f3B[2];
		}

		Float1 DotProduct(const Float2& f2A, const Float2& f2B)
		{
			return f2A[0] * f2B[0] + f2A[1] * f2B[1];
		}

		Float3 ComputeNormal(const Float3& v0, const Float3& v1, const Float3& v2)
		{
			Float3 v01 = v0; v01 -= v1;
			v01 = Math::Normalize(v01);
			Float3 v02 = v0; v02 -= v2;
			v02 = Math::Normalize(v02);

			Float3 n = Math::CrossProduct(v01, v02);

			return n;
		}

		Float4 ComputeTangent(const Float3& v0, const Float3& v1, const Float3& v2,
			const Float2&  uv0, const Float2& uv1, const Float2& uv2)
		{
			Float3 v01 = v0 - v1;
			Float3 v02 = v0 - v2;

			Float3 n = Math::CrossProduct(v01, v02);

			Float2 uvDelta0 = uv0 - uv1;
			Float2 uvDelta1 = uv0 - uv2;

			//(uvDelta0.u * tangent + uvDelta0.v * bitangent) = vec0
			//(uvDelta1.u * tangent + uvDelta1.v * bitangent) = vec1
			//(uvDelta0.u + uvDelta1.u)
			//[vec0 vec1] =  [T B][u0 u1]		[T B] = [u0 u1]-1  [vec0]
			//				      [v0 v1]				[v0 v1]    [vec1]
			//**-1 = 	1.0f				[ v1 -v0]
			//			u0*v1 - u1*v0		[-u0  u1]

			Float1 fDenominator = 1.0f / Max(uvDelta0[0] * uvDelta1[1] - uvDelta1[0] * uvDelta0[1], Constant_MIN_FLOAT);
			Float3 tangent(
				uvDelta1[1] * v01[0] - uvDelta0[1] * v02[0],
				uvDelta1[1] * v01[1] - uvDelta0[1] * v02[1],
				uvDelta1[1] * v01[2] - uvDelta0[1] * v02[2]
			);
			tangent = tangent * fDenominator;

			Float3 bitangent(
				uvDelta1[0] * v01[0] - uvDelta0[0] * v02[0],
				uvDelta1[0] * v01[1] - uvDelta0[0] * v02[1],
				uvDelta1[0] * v01[2] - uvDelta0[0] * v02[2]
			);
			bitangent = bitangent * fDenominator;

			Float3 tbNormal = Math::CrossProduct(tangent, bitangent);
			Float1 dotRes = Math::DotProduct(tbNormal, n);

			tangent = Math::Normalize(tangent);

			return Float4(tangent[0], tangent[1], tangent[2], dotRes > 0 ? 1.0f : -1.0f);
		}

		Void   FFT_Transform(const Float1 dataTimeDomain[], Float1 dataFrequencyDomain[], UInt32 dataCount)
		{
			ASSERT_IF_FAILED(dataFrequencyDomain != nullptr && "Out Buffer Must Be Allocated");
			if (dataCount == 0 || dataTimeDomain == nullptr || dataFrequencyDomain == nullptr)
				return;

			Float1 *in = new Float1[dataCount];
			fftwf_complex *out = new fftwf_complex[dataCount];
			fftwf_plan p;

			memcpy(in, dataTimeDomain, dataCount * sizeof(Float1));

			// 傅里叶变换
			p = fftwf_plan_dft_r2c_1d(dataCount, in, out, FFTW_ESTIMATE);
			fftwf_execute(p);

			// 输出幅度谱
			for (int i = 0; i < dataCount / 2; i++) {
				dataFrequencyDomain[i] = sqrt(out[i][0] * out[i][0] + out[i][1] * out[i][1]);
			}

			// 释放资源
			fftwf_destroy_plan(p);
			delete[] in;
			delete[] out;
		}

		/*4组计算函数*/
		UInt32 F(UInt32 X, UInt32 Y, UInt32 Z)
		{
			return (X & Y) | ((~X) & Z);
		}
		UInt32 G(UInt32 X, UInt32 Y, UInt32 Z)
		{
			return (X & Z) | (Y & (~Z));
		}
		UInt32 H(UInt32 X, UInt32 Y, UInt32 Z)
		{
			return X ^ Y ^ Z;
		}
		UInt32 I(UInt32 X, UInt32 Y, UInt32 Z)
		{
			return Y ^ (X | (~Z));
		}
		Void ROL(UInt32 &s, UInt32 cx)
		{
			if (cx > 32)cx %= 32;
			s = (s << cx) | (s >> (32 - cx));
			return;
		}
		Void ltob(UInt32 &i)
		{
			UInt32 org = i;
			UInt8 *psrc = (UInt8*)&org;
			UInt8 *pdes = (UInt8*)&i;
			pdes += 3;

			pdes[0] = psrc[3];
			pdes[1] = psrc[2];
			pdes[2] = psrc[1];
			pdes[3] = psrc[0];
			return;
		}
		void AccLoop(UInt8 label, UInt32 *lGroup, Void *M)
		{
			UInt32 *i1, *i2, *i3, *i4, TAcc, tmpi = 0;
			const UInt32 rolarray[4][4] = {
				{ 7, 12, 17, 22 },
				{ 5, 9, 14, 20 },
				{ 4, 11, 16, 23 },
				{ 6, 10, 15, 21 }
			};
			const UInt8 mN[4][16] = {
				{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
				{ 1, 6, 11, 0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12 },
				{ 5, 8, 11, 14, 1, 4, 7, 10, 13, 0, 3, 6, 9, 12, 15, 2 },
				{ 0, 7, 14, 5, 12, 3, 10, 1, 8, 15, 6, 13, 4, 11, 2, 9 }
			};//数据坐标表
			const UInt32 *pM = (UInt32*)M;
			TAcc = ((label - 1) * 16) + 1;

			typedef UInt32(*clac)(UInt32 X, UInt32 Y, UInt32 Z);
			clac clacArr[4] = { F, G, H, I };

			for (SInt8 i = 0; i < 16; ++i)
			{
				i1 = lGroup + ((0 + i) % 4);
				i2 = lGroup + ((3 + i) % 4);
				i3 = lGroup + ((2 + i) % 4);
				i4 = lGroup + ((1 + i) % 4);

				Float1 f = sinf(TAcc + i);
				f *= f > 0 ? 1.0f : -1.0f;
				tmpi = (*i1 + clacArr[label - 1](*i2, *i3, *i4) + pM[(mN[label - 1][i])] + 
					(UInt32)(0x100000000UL * f));
				ROL(tmpi, rolarray[label - 1][i % 4]);
				*i1 = *i2 + tmpi;//第三步:相加并赋值到种子
			}
			return;
		}
		MD5	CalculateMD5_128(const Char* szString)
		{
			UInt32 uLength = strlen(szString);
			if (uLength == 0) return MD5();

			UInt32 FillSize = 448 - ((uLength * 8) % 512);
			UInt32 FSbyte = FillSize / 8;
			UInt32 BuffLen = uLength + 8 + FSbyte;
			Array<Char> MD5Buffer;
			MD5Buffer.SetElementCount(BuffLen);
			memcpy(&MD5Buffer[0], szString, uLength);

			MD5Buffer[uLength] = 0x80;
			memset(&MD5Buffer[uLength + 1], 0, FSbyte - 1);
			UInt64 lenBit = uLength * 8;
			memcpy(&MD5Buffer[uLength + FSbyte], &lenBit, 8);

			UInt32 A = 0x67452301, B = 0x0EFCDAB89, C = 0x98BADCFE, D = 0x10325476;

			UInt32 LoopNumber = BuffLen / 64;
			MD5 md5;
			md5.ID[0] = A;
			md5.ID[1] = D;
			md5.ID[2] = C;
			md5.ID[3] = B;
			for (unsigned int Bcount = 0; Bcount < LoopNumber; ++Bcount)
			{
				/*进入4次计算的小循环*/
				for (UInt8 Lcount = 1; Lcount < 5; Lcount++)
				{
					AccLoop(Lcount, md5.ID, &MD5Buffer[Bcount * 64]);
				}
				A = (md5.ID[0] += A);
				D = (md5.ID[1] += D);
				C = (md5.ID[2] += C);
				B = (md5.ID[3] += B);
			}
			for (UInt32 u = 0; u < 4; u++)
			{
				UInt32 org = md5.ID[u];
				UInt8 *psrc = (UInt8*)&org;
				UInt8 *pdes = (UInt8*)&md5.ID[u];

				pdes[0] = psrc[3];
				pdes[1] = psrc[2];
				pdes[2] = psrc[1];
				pdes[3] = psrc[0];
			}
			return md5;
		}
	}
}