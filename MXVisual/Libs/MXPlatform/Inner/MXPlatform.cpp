#include "MXPlatform.h"
#include "MXInput.h"
#include "MXTimer.h"
#include "MXInterface.h"
#include "MXConstant.h"
#if PLATFORM == PLATFORM_WINDOWS
#include "Windows.h"
#endif

namespace MXVisual
{
	Region::Region()
		: uLeft(0)
		, uTop(0)
		, uRight(0)
		, uBottom(0)
	{}
	Boolean Region::operator==(const Region& other)
	{
		return uLeft == other.uLeft &&
			uTop == other.uTop &&
			uRight == other.uRight &&
			uBottom == other.uBottom;
	}

	Void GetApplicationPath(Char* szBuffer)
	{
#if PLATFORM == PLATFORM_WINDOWS
		GetModuleFileName(NULL, szBuffer, Constant_MAX_PATH);
		SInt32 n = (SInt32)strlen(szBuffer);
		while (szBuffer[n] != '\\')	n--;
		szBuffer[n] = '\0';
#endif
	}

	Void GetWorkingPath(Char* szBuffer)
	{
#if PLATFORM == PLATFORM_WINDOWS
		GetCurrentDirectory(Constant_MAX_PATH, szBuffer);
#endif
	}

	namespace Application
	{
		FBooleanCallBack	FOnInit = nullptr;
		FVoidCallBack		FOnRelease = nullptr;
		FVoidCallBack		FOnLoop = nullptr;
		FIMECallBack		FOnIME = nullptr;

		UInt32 Run()
		{
			Input::Init();

			if (FOnInit != nullptr)
			{
				if (!FOnInit()) return -1;
			}

			Timer::Reset();
			Timer::Update();

#if PLATFORM == PLATFORM_WINDOWS
			MSG msg;
			if (FOnLoop == nullptr)
			{
				while (GetMessage(&msg, NULL, 0, 0))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);

					if (FOnIME != nullptr && msg.message == WM_CHAR)
					{
						FOnIME(1, (const Char*)&msg.wParam);
					}
				}
			}
			else
			{
				while (1)
				{
					if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
						if (msg.message == WM_QUIT)
							break;

						if (FOnIME != nullptr && msg.message == WM_CHAR)
						{
							FOnIME(1, (const Char*)&msg.wParam);
						}
					}
					else
					{
						Input::Update();

						Timer::Update();

						FOnLoop();
					}
				}
			}
#endif

			if (FOnRelease != nullptr)
			{
				FOnRelease();
			}

			Input::Release();
			return 0;
		}
	};
}
