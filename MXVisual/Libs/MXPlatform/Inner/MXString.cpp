#include "MXString.h"
#include "MXConstant.h"

#include <string.h>
#include <stdarg.h>
#include <stdio.h>//vsprintf
#include <assert.h>
//Temp
#include <string>
#include "windows.h"

namespace
{
	const MXVisual::UInt32 Init_Size = 256;
	const MXVisual::UInt32 Extra_Size = 128;
}

namespace MXVisual
{
	String::String(const Char * str)
	{
		m_uStringLength = (UInt32)strlen(str);
		m_uBufferSize = m_uStringLength + Extra_Size;
		m_szString = new Char[m_uBufferSize]{ 0 };
		strcpy(m_szString, str);
	}

	String::String(const String & strString)
	{
		m_uBufferSize = strString.m_uStringLength + Extra_Size;
		m_szString = new Char[m_uBufferSize]{ 0 };
		strcpy(m_szString, strString.m_szString);
		m_uStringLength = strString.m_uStringLength;
	}

	String::String()
	{
		m_szString = new Char[Init_Size]{ 0 };
		m_uBufferSize = Init_Size;
		m_uStringLength = 0;
	}

	String::~String()
	{
		if (m_szString != nullptr)
		{
			delete[] m_szString;
			m_szString = nullptr;
		}
	}

	SInt32 String::Find(const Char * szString, UInt32 uOffset) const
	{
		UInt32 uStringLength = (SInt32)strlen(szString);
		for (UInt32 nCharIndex = uOffset; nCharIndex < m_uStringLength; nCharIndex++)
		{
			UInt32 uCharOffset = 0;
			for (; uCharOffset < uStringLength; uCharOffset++)
			{
				if (m_szString[nCharIndex + uCharOffset] != szString[uCharOffset])
					break;
			}
			if (uCharOffset == uStringLength)
				return (SInt32)nCharIndex;
		}

		return -1;
	}

	SInt32 String::FindLastOf(const Char * szString) const
	{
		SInt32 nStringLength = (SInt32)strlen(szString);
		for (SInt32 nCharIndex = m_uStringLength - nStringLength; nCharIndex > -1; nCharIndex--)
		{
			SInt32 nCharOffset = 0;
			for (; nCharOffset < nStringLength; nCharOffset++)
			{
				if (m_szString[nCharIndex + nCharOffset] != szString[nCharOffset])
					break;
			}
			if (nCharOffset == nStringLength)
				return nCharIndex;
		}

		return -1;
	}

	String String::Format(const Char * szString, ...)
	{
		const UInt32 uInitSize = 1024;
		Char* pBuf = new Char[uInitSize]{ 0 };
		va_list args;
		va_start(args, szString);
		UInt32 uSize = uInitSize;
		while (_vsnprintf(pBuf, uSize, szString, args) < 0)
		{
			uSize <<= 1;
			delete[] pBuf;
			pBuf = new Char[uSize]{ 0 };
		}
		va_end(args);
		String res = pBuf;
		delete[] pBuf;
		return res;
	}

	Array<UInt8> String::Transcode_GB2312_To_UTF16(const String& str)
	{
		Array<UInt8> result;
		String convertedString;
		UInt32 requiredSize = MultiByteToWideChar(CP_ACP, 0, str.CString(), -1, 0, 0);
		if (requiredSize > 0)
		{
			result.SetElementCount(requiredSize * sizeof(wchar_t));
			MultiByteToWideChar(CP_ACP, 0, str.CString(), -1, (wchar_t*)&result[0], requiredSize);
		}

		return result;
	}
	Array<UInt8> String::Transcode_UTF8_To_UTF16(const String& str)
	{
		Array<UInt8> result;
		String convertedString;
		UInt32 requiredSize = MultiByteToWideChar(CP_UTF8, 0, str.CString(), -1, 0, 0);
		if (requiredSize > 0)
		{
			result.SetElementCount(requiredSize * sizeof(wchar_t));
			MultiByteToWideChar(CP_UTF8, 0, str.CString(), -1, (wchar_t*)&result[0], requiredSize);
		}

		return result;
	}

	String String::operator+(const Char *szString)
	{
		SInt32 nStringLength = m_uStringLength + (SInt32)strlen(szString);
		Char *newBuffer = new Char[nStringLength + Extra_Size]{ 0 };
		strcpy(newBuffer, m_szString);
		strcat(newBuffer, szString);

		String result = newBuffer;
		return result;
	}
	String String::operator+(const String& strString)
	{
		String result = *this;
		result += strString.CString();
		return result;
	}

	const String& String::operator+=(const Char *szString)
	{
		if (szString == nullptr) return *this;

		UInt32 uStringLength = m_uStringLength + (UInt32)strlen(szString);
		if (uStringLength + 1 > m_uBufferSize)
		{
			while (m_uBufferSize <= uStringLength) m_uBufferSize <<= 1;

			Char *newBuffer = m_szString;
			m_szString = new Char[m_uBufferSize];
			m_szString[0] = 0;
			strcpy(m_szString, newBuffer);
			strcat(m_szString, szString);
			delete[] newBuffer;
		}
		else
		{
			strcat(m_szString, szString);
		}
		m_uStringLength = uStringLength;
		return *this;
	}
	const String& String::operator+=(const String& strString)
	{
		UInt32 uStringLength = m_uStringLength + strString.m_uStringLength;
		if (uStringLength + 1 > m_uBufferSize)
		{
			m_uBufferSize = uStringLength + Extra_Size;
			Char *newBuffer = m_szString;
			m_szString = new Char[m_uBufferSize];
			m_szString[0] = 0;
			strcpy(m_szString, newBuffer);
			strcat(m_szString, strString.m_szString);
			delete[] newBuffer;
		}
		else
		{
			strcat(m_szString, strString.m_szString);
		}
		m_uStringLength = uStringLength;
		return *this;
	}

	const String& String::operator+=(const Char Value)
	{
		Char buf[2];
		sprintf(buf, "%c", Value);
		return operator+=(buf);
	}
	const String& String::operator+=(const Float1 Value)
	{
		Char buf[Constant_MAX_PATH];
		sprintf(buf, "%f", Value);
		return operator+=(buf);
	}
	const String& String::operator+=(const Float2& Value)
	{
		Char buf[Constant_MAX_PATH];
		sprintf(buf, "%f,%f", Value[0], Value[1]);
		return operator+=(buf);
	}
	const String& String::operator+=(const Float3& Value)
	{
		Char buf[Constant_MAX_PATH];
		sprintf(buf, "%f,%f,%f", Value[0], Value[1], Value[2]);
		return operator+=(buf);
	}
	const String& String::operator+=(const Float4& Value)
	{
		Char buf[Constant_MAX_PATH];
		sprintf(buf, "%f,%f,%f,%f", Value[0], Value[1], Value[2], Value[3]);
		return operator+=(buf);
	}

	const String& String::operator+=(const Matrix3& Value)
	{
		Char buf[Constant_MAX_PATH];
		sprintf(buf, "%f,%f,%f\n%f,%f,%f\n%f,%f,%f",
			Value.Get(0, 0), Value.Get(0, 1), Value.Get(0, 2),
			Value.Get(1, 0), Value.Get(1, 1), Value.Get(1, 2),
			Value.Get(2, 0), Value.Get(2, 1), Value.Get(2, 2));
		return operator+=(buf);
	}
	const String& String::operator+=(const Matrix4& Value)
	{
		Char buf[Constant_MAX_PATH];
		sprintf(buf, "%f,%f,%f,%f\n%f,%f,%f,%f\n%f,%f,%f,%f\n%f,%f,%f,%f",
			Value.Get(0, 0), Value.Get(0, 1), Value.Get(0, 2), Value.Get(0, 3),
			Value.Get(1, 0), Value.Get(1, 1), Value.Get(1, 2), Value.Get(1, 3),
			Value.Get(2, 0), Value.Get(2, 1), Value.Get(2, 2), Value.Get(2, 3),
			Value.Get(3, 0), Value.Get(3, 1), Value.Get(3, 2), Value.Get(3, 3));
		return operator+=(buf);
	}

	const String & String::operator=(const Char * szString)
	{
		UInt32 uStringLength = szString ? (UInt32)strlen(szString) : 0;
		if (m_uBufferSize < uStringLength + 1)
		{
			delete[] m_szString;
			m_uBufferSize = uStringLength + Extra_Size;
			m_szString = new Char[m_uBufferSize]{ 0 };
		}

		if (szString)
			strcpy(m_szString, szString);
		else
			m_szString[0] = 0;
		m_uStringLength = uStringLength;

		return *this;
	}

	const String & String::operator=(const String & strString)
	{
		if (m_uBufferSize < strString.m_uStringLength + 1)
		{
			delete[] m_szString;
			m_uBufferSize = strString.m_uStringLength + Extra_Size;
			m_szString = new Char[m_uBufferSize]{ 0 };
			strcpy(m_szString, strString.m_szString);
			m_uStringLength = strString.m_uStringLength;
		}
		else
		{
			if (strString.m_szString != nullptr)
			{
				strcpy(m_szString, strString.m_szString);
			}
			else
			{
				m_szString[0] = '\0';
			}
			m_uStringLength = strString.m_uStringLength;
		}
		return *this;
	}

	const Char String::operator[](UInt32 uIndex) const
	{
		if (uIndex < m_uStringLength)
			return m_szString[uIndex];
		return 0;
	}

	Boolean String::operator<(const String & strString) const
	{
		SInt32 nResult = strcmp(m_szString, strString.m_szString);
		return nResult < 0;
	}

	Boolean String::operator>(const String &strString) const
	{
		SInt32 nResult = strcmp(m_szString, strString.m_szString);
		return nResult > 0;
	}

	Boolean String::operator!=(const String & strString) const
	{
		SInt32 nResult = strcmp(m_szString, strString.m_szString);
		return nResult != 0;
	}

	Boolean String::operator==(const String & strString) const
	{
		SInt32 nResult = strcmp(m_szString, strString.m_szString);
		return nResult == 0;
	}

	Boolean String::operator<(const Char* szString) const
	{
		SInt32 nResult = strcmp(m_szString, szString);
		return nResult < 0;
	}
	Boolean String::operator>(const Char* szString) const
	{
		SInt32 nResult = strcmp(m_szString, szString);
		return nResult > 0;
	}
	Boolean String::operator!=(const Char* szString) const
	{
		if (szString == nullptr)return m_uStringLength != 0;

		SInt32 nResult = strcmp(m_szString, szString);
		return nResult != 0;
	}
	Boolean String::operator==(const Char* szString) const
	{
		if (szString == nullptr)return m_uStringLength == 0;

		SInt32 nResult = strcmp(m_szString, szString);
		return nResult == 0;
	}

	Void String::Erase(UInt32 nIndex, SInt32 nCount)
	{
		if (nCount == -1)
		{
			m_uStringLength = nIndex;
			m_szString[nIndex] = 0;
		}
		else
		{
			m_uStringLength -= nCount;
			strcpy(m_szString + nIndex, m_szString + nIndex + nCount);
		}
	}

	Void String::Insert(const Char *szString, UInt32 uOffset)
	{
		SInt32 nStringLength = (SInt32)strlen(szString);
		if (m_uStringLength + nStringLength > (m_uBufferSize - 1))
		{
			Char *pTemp = m_szString;
			m_szString = new Char[m_uStringLength + nStringLength + Extra_Size]{ 0 };
			strncpy(m_szString, pTemp, uOffset);
			strcat(m_szString, szString);
			strcat(m_szString, pTemp + uOffset);
			delete[] pTemp;
			m_uBufferSize = m_uStringLength + nStringLength + Extra_Size;
		}
		else
		{
			SInt32 nAppendOffset = m_uStringLength - uOffset;
			Char *pTemp = new Char[nAppendOffset + 1]{ 0 };
			strcpy(pTemp, m_szString + uOffset);
			m_szString[uOffset] = 0;
			strcat(m_szString, szString);
			strcat(m_szString, pTemp);
			delete[] pTemp;
		}
		m_uStringLength += nStringLength;
	}

	UInt32 String::Length() const
	{
		return m_uStringLength;
	}

	Void String::Replace(const Char * szToReplace, const Char * szSrc, SInt32 nOffset)
	{
		std::string str = m_szString;
		str.replace(str.begin() + nOffset, str.end(), szSrc);
		if (str.length() > m_uBufferSize)
		{
			delete[] m_szString;
			m_szString = new Char[str.length() + Extra_Size];
			m_szString[0] = '\0';
			m_uBufferSize = str.length() + Extra_Size;
		}
		strcpy(m_szString, str.c_str());
		m_uStringLength = str.length();
	}

	Boolean String::IsEmpty() const
	{
		return m_uStringLength == 0;
	}

	Void String::Clear()
	{
		m_uStringLength = 0;
		m_uBufferSize = m_uStringLength + Extra_Size;
		m_szString = new Char[m_uBufferSize]{ 0 };
	}

	const Char * String::CString() const
	{
		return m_szString;
	}

}