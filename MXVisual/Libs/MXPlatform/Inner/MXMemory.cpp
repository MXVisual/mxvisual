#define LOG 0

#include "MXMemory.h"
#if LOG
#include "MXLog.h"
#endif
#include <vector>

namespace MXVisual
{

	namespace _MemoryPool
	{
		std::vector<void*> arrMemoryBlockList;
		std::vector<UInt32>	arrMemoryBlockSize;
		UInt32 uMemoryOccupied;

		Void Init()
		{
			uMemoryOccupied = 0;
		}
		Void Release()
		{
#if LOG
			Log::Output("MemoryPool", "Begin Memory Leak Detection");
#endif
			for (UInt32 u = 0; u < arrMemoryBlockList.size(); u++)
			{
#if LOG
				String str = String::Format("Memory Leak: %u bytes", arrMemoryBlockSize[u]);
				Log::Output("MemoryPool", str.CString());
#endif
				free(arrMemoryBlockList[u]);
			}
			arrMemoryBlockList.clear();
			arrMemoryBlockSize.clear();
#if LOG
			Log::Output("MemoryPool", "End Memory Leak Detection");
#endif
		}

		struct SelfManager
		{
			SelfManager()
			{
				Init();
			}
			~SelfManager()
			{
				Release();
			}

		}SelfManagerItem;
	}


	Void*	MemoryPool::Allocate(UInt32 uSize)
	{
#if LOG
		String str = String::Format("Allocate Memory: %u bytes", uSize);
		Log::Output("MemoryPool", str.CString());
#endif
		void* pRes = (void*)malloc(uSize);
		_MemoryPool::uMemoryOccupied += uSize;

		_MemoryPool::arrMemoryBlockList.push_back(pRes);
		_MemoryPool::arrMemoryBlockSize.push_back(uSize);
		return pRes;
	}

	Void	MemoryPool::Deallocate(Void* pMemory)
	{
		for (UInt32 u = 0; u < _MemoryPool::arrMemoryBlockList.size(); u++)
		{
			if (_MemoryPool::arrMemoryBlockList[u] == pMemory)
			{
#if LOG
				String str = String::Format("Deallocate Memory: %u bytes", _MemoryPool::arrMemoryBlockSize[u]);
				Log::Output("MemoryPool", str.CString());
#endif
				free(_MemoryPool::arrMemoryBlockList[u]);
				_MemoryPool::uMemoryOccupied -= _MemoryPool::arrMemoryBlockSize[u];

				_MemoryPool::arrMemoryBlockList.erase(_MemoryPool::arrMemoryBlockList.begin() + u);
				_MemoryPool::arrMemoryBlockSize.erase(_MemoryPool::arrMemoryBlockSize.begin() + u);
			}
		}
	}
	UInt32	MemoryPool::GetMemoryOccupied()
	{
		return _MemoryPool::uMemoryOccupied;
	}

	struct _GlobalSelfManageParam
	{
		_GlobalSelfManageParam()
		{

		}

		~_GlobalSelfManageParam()
		{

		}

		static _GlobalSelfManageParam selfManageItem;
	};

	Void* MemoryBlock::operator new(std::size_t s)
	{
		return MemoryPool::Allocate((UInt32)s);
	}

	Void* MemoryBlock::operator new[](std::size_t s)
	{
		return MemoryPool::Allocate((UInt32)s);
	}

	Void  MemoryBlock::operator delete(void* p)
	{
		MemoryPool::Deallocate(p);
	}

	Void  MemoryBlock::operator delete[](void* p)
	{
		MemoryPool::Deallocate(p);
	}
}
