#include "MXFile.h"
#include "MXString.h"
#include "stdio.h"
#include "io.h"
#include "MXMath.h"
#include <stack>

#if PLATFORM == PLATFORM_WINDOWS
#include "windows.h"
#endif

namespace MXVisual
{
	Void	CalFileName(const Char* szFilePath, Char* szResult, UInt32 uMaxSize)
	{
		NormalizePath(szFilePath, szResult, uMaxSize);
		String strNormalizedPath = szResult;

		SInt32 nSeperator = strNormalizedPath.FindLastOf("/");
		if (nSeperator >= 0)
		{
			strcpy(szResult, strNormalizedPath.CString() + nSeperator + 1);
		}
	}
	Void	CalAbsolutePath(const Char* szPath, const Char* szPathToCal, Char* szResult, UInt32 uMaxSize)
	{
		strcpy(szResult, szPathToCal);

		String strTemp = szPath;
		SInt32 nFileExt = strTemp.FindLastOf(".");
		if (nFileExt > 0)
		{
			nFileExt = strTemp.FindLastOf("\\");
			strTemp.Replace("\\", "\0", nFileExt);
		}

		UInt32 uPathToCal = (UInt32)strlen(szPathToCal);

		//szPathToCal要相对路径
		if (uPathToCal < 2 || szPathToCal[1] == ':')
		{
			return;
		}
		szResult[0] = '\0';

		strcat(szResult, strTemp.CString());
		if (szResult[strTemp.Length() - 1] != '\\')
			strcat(szResult, "\\");
		strcat(szResult, szPathToCal);

		std::stack<std::string> FolderStack;
		UInt32 uLength = strlen(szResult);

		//Merge "..\" and ".\"
		UInt32 uFolder = 0;
		for (UInt32 u = 0; u <= uLength; u++)
		{
			if (szResult[u] == '\\' || szResult[u] == '\0')
			{
				if (!FolderStack.empty() &&
					FolderStack.top().compare(".") != 0 &&
					FolderStack.top().compare("..") != 0 &&
					strncmp(&szResult[u - uFolder], "..", uFolder) == 0)
				{
					FolderStack.pop();
				}
				else
				{
					szResult[u] = '\0';
					FolderStack.push(&szResult[u - uFolder]);
				}
				uFolder = 0;
			}
			else
			{
				uFolder++;
			}
		}

		String strRes;
		while (!FolderStack.empty())
		{
			strRes.Insert("\\", 0);
			strRes.Insert(FolderStack.top().c_str(), 0);
			FolderStack.pop();
		}
		strRes.Erase(strRes.Length() - 1);//删除最后的分隔符
		memcpy(szResult, strRes.CString(), min(uMaxSize, strRes.Length()));
	}
	Void	CalRelativePath(const Char* szPath, const Char* szPathToCal, Char* szResult, UInt32 uMaxSize)
	{
		Char temp[Constant_MAX_PATH] = { 0 };
		strcat(temp, szPath);

		UInt32 uPath = strlen(temp);
		if (szPath[uPath - 1] != '\\' || szPath[uPath - 1] != '/')
		{
			strcat(temp, "\\");
			uPath++;
		}

		UInt32 uPathToCal = strlen(szPathToCal);
		UInt32 uPathMin = Math::Min(uPathToCal, uPath);

		//非绝对路径
		if (uPath < 2 || uPathToCal < 2 ||
			szPath[1] != ':' || szPathToCal[1] != ':')
		{
			return;
		}
		//找到两个绝对路径第一个不相等的字符
		UInt32 uDiff = 0;
		UInt32 uFolder = 0;
		for (; uDiff < uPath; uDiff++)
		{
			if (temp[uDiff] != szPathToCal[uDiff])
			{
				break;
			}
			uFolder++;
			if (temp[uDiff] == '\\' || temp[uDiff] == '/')
				uFolder = 0;
		}
		uDiff -= uFolder;

		szResult[0] = '\0';
		UInt32 uIndex = uDiff;
		while (uIndex < uPath)
		{
			if (temp[uIndex] == '\\' || temp[uIndex] == '/')
			{
				strcat(szResult, "..\\");
			}
			uIndex++;
		}
		strcat(szResult, &szPathToCal[uDiff]);
	}
	Void	NormalizePath(const Char* szPathToCal, Char* szResult, UInt32 uMaxSize)
	{
		strcpy(szResult, szPathToCal);
		if (strstr(szResult, "\\") == nullptr)return;

		std::stack<std::string> FolderStack;
		UInt32 uLength = strlen(szResult);

		//Merge "..\" and ".\"
		UInt32 uFolder = 0;
		for (UInt32 u = 0; u <= uLength; u++)
		{
			if (szResult[u] == '\\' || szResult[u] == '\0')
			{
				if (!FolderStack.empty() &&
					FolderStack.top().compare(".") != 0 &&
					FolderStack.top().compare("..") != 0 &&
					strncmp(&szPathToCal[u - uFolder], "..", uFolder) == 0)
				{
					FolderStack.pop();
				}
				else
				{
					szResult[u] = '\0';
					FolderStack.push(&szResult[u - uFolder]);
				}
				uFolder = 0;
			}
			else
			{
				uFolder++;
			}
		}

		String strRes;
		while (!FolderStack.empty())
		{
			strRes.Insert("/", 0);
			strRes.Insert(FolderStack.top().c_str(), 0);
			FolderStack.pop();
		}
		strRes.Erase(strRes.Length() - 1);//删除最后的分隔符
		memcpy(szResult, strRes.CString(), min(uMaxSize, strRes.Length()));
	}

	Boolean CheckFile(const Char* szFileName)
	{
		/*
		00 Existence only
		02 Write permission
		04 Read permission
		*/
		SInt32 nLength = (SInt32)strlen(szFileName);
		if (nLength <= 0)
			return False;
		String strFileName;
		if (nLength > 0 && szFileName[1] != ':')//非相对对路径
		{
			Char exeFullPath[Constant_MAX_PATH]; // Full path
			GetCurrentDirectory(Constant_MAX_PATH, exeFullPath);
			strFileName = exeFullPath;
			strFileName += "\\";
		}
		strFileName += szFileName;
		return _access(strFileName.CString(), 0) != -1;
	}

	Boolean CheckFileSuffix(const Char* szFileName, const Char* szSuffix)
	{
		ASSERT_IF_FAILED(szFileName != nullptr && szSuffix != nullptr);
		if (szSuffix[0] == '*')return True;

		SInt32 nLength0 = (SInt32)strlen(szFileName);
		SInt32 nLength1 = (SInt32)strlen(szSuffix);
		if (nLength0 <= nLength1 || nLength1 <= 0 /*|| nLength0 <= 0*/)
			return False;

		SInt32 nOffset = nLength0 - nLength1;
		if (_strnicmp(szFileName + nOffset, szSuffix, nLength1) == 0)
			return True;

		return False;
	}

	class TextFile : public IFile
	{
		friend IFile;
	public:
		Void Close();

		const Char* GetFileName() const override;
		UInt32 GetFileSize() const override;

		UInt32 ReadLine(Void* pBuf, UInt32 uMaxSize) override;
		UInt32 Read(Void* pBuf, UInt32 uMaxSize, Boolean bOffset = False, UInt32 uOffset = 0) override;

		UInt32 Write(const Void* pBuf, UInt32 uBufSize) override;

	protected:
		TextFile()
			: m_pCFile(nullptr)
		{

		}
		~TextFile()
		{
			if (m_pCFile != nullptr)
			{
				fclose(m_pCFile);
				m_pCFile = nullptr;
			}
		}

		FILE*	m_pCFile;
		UInt32	m_uFileSize;
		String	m_strFileName;
	};

	Void TextFile::Close()
	{
		delete this;
	}

	const Char* TextFile::GetFileName() const
	{
		return m_strFileName.CString();
	}
	UInt32 TextFile::GetFileSize() const
	{
		return m_uFileSize;
	}

	UInt32 TextFile::ReadLine(Void* pBuf, UInt32 uMaxSize)
	{
		if (pBuf == nullptr)return 0;

		UInt32 uOffset = 0;
		Char c = fgetc(m_pCFile);
		while (uOffset < uMaxSize && c != '\n')
		{
			((UInt8*)pBuf)[uOffset] = c;
			c = fgetc(m_pCFile);
			uOffset++;
		}
		return uOffset;
	}
	UInt32 TextFile::Read(Void* pBuf, UInt32 uMaxSize, Boolean bOffset, UInt32 uOffset)
	{
		if (pBuf == nullptr)return 0;

		if (bOffset)
		{
			fseek(m_pCFile, uOffset, SEEK_SET);
		}
		return fread(pBuf, 1, uMaxSize, m_pCFile);
	}
	UInt32 TextFile::Write(const Void* pBuf, UInt32 uBufSize)
	{
		if (pBuf == nullptr || uBufSize == 0)return 0;

		return (UInt32)fwrite(pBuf, 1, uBufSize, m_pCFile);
	}

	class BinaryFile : public IFile
	{
		friend IFile;
	public:
		Void Close();

		const Char* GetFileName() const override;
		UInt32 GetFileSize() const override;

		UInt32 ReadLine(Void* pBuf, UInt32 uMaxSize) override;
		UInt32 Read(Void* pBuf, UInt32 uMaxSize, Boolean bOffset = False, UInt32 uOffset = 0) override;
		UInt32 Write(const Void* pBuf, UInt32 uBufSize) override;

	protected:
		BinaryFile()
			: m_pCFile(nullptr)
		{
		}
		~BinaryFile()
		{
			fclose(m_pCFile);
			m_pCFile = nullptr;
		}

		FILE*	m_pCFile;
		UInt32	m_uFileSize;
		String	m_strFileName;
	};

	Void BinaryFile::Close()
	{
		delete this;
	}

	const Char* BinaryFile::GetFileName() const
	{
		return m_strFileName.CString();
	}
	UInt32 BinaryFile::GetFileSize() const
	{
		return m_uFileSize;
	}

	UInt32 BinaryFile::ReadLine(Void* pBuf, UInt32 uMaxSize)
	{
		return Read(pBuf, uMaxSize);
	}
	UInt32 BinaryFile::Read(Void* pBuf, UInt32 uMaxSize, Boolean bOffset, UInt32 uOffset)
	{
		if (pBuf == nullptr)return 0;

		if (bOffset)
		{
			fseek(m_pCFile, uOffset, SEEK_SET);
		}
		return fread(pBuf, 1, uMaxSize, m_pCFile);
	}
	UInt32 BinaryFile::Write(const Void* pBuf, UInt32 uBufSize)
	{
		if (pBuf == nullptr || uBufSize == 0)return 0;

		return (UInt32)fwrite(pBuf, 1, uBufSize, m_pCFile);
	}

	IFile * IFile::Open(const Char* szFileName, FileDataType eType)
	{
		if (szFileName == nullptr)return nullptr;

		if (eType == EFDT_Binary)
		{
			FILE *pFile = fopen(szFileName, "rb+");
			if (pFile != nullptr)
			{
				BinaryFile *pNewFile = new BinaryFile;
				pNewFile->m_strFileName = szFileName;
				pNewFile->m_pCFile = pFile;

				fseek(pFile, 0, SEEK_END);
				pNewFile->m_uFileSize = ftell(pFile);
				fseek(pFile, 0, SEEK_SET);

				return pNewFile;
			}
		}
		else if (eType == EFDT_Text)
		{
			FILE *pFile = fopen(szFileName, "r+");
			if (pFile != nullptr)
			{
				TextFile *pNewFile = new TextFile;
				pNewFile->m_strFileName = szFileName;
				pNewFile->m_pCFile = pFile;

				fseek(pFile, 0, SEEK_END);
				pNewFile->m_uFileSize = ftell(pFile);
				fseek(pFile, 0, SEEK_SET);

				return pNewFile;
			}
		}
		return nullptr;
	}
	IFile * IFile::New(const Char* szFileName, FileDataType eType)
	{
		if (szFileName == nullptr)return nullptr;

		if (eType == EFDT_Binary)
		{
			FILE *pFile = fopen(szFileName, "wb+");
			if (pFile != nullptr)
			{
				BinaryFile *pNewFile = new BinaryFile;
				pNewFile->m_strFileName = szFileName;
				pNewFile->m_pCFile = pFile;
				return pNewFile;
			}
		}
		else if (eType == EFDT_Text)
		{
			FILE *pFile = fopen(szFileName, "w+");
			if (pFile != nullptr)
			{
				TextFile *pNewFile = new TextFile;
				pNewFile->m_strFileName = szFileName;
				pNewFile->m_pCFile = pFile;
				return pNewFile;
			}
		}
		return nullptr;
	}
}