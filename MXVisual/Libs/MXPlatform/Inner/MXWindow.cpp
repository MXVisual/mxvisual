#include "MXWindow.h"
#include <assert.h>
#include <unordered_map>

#if PLATFORM == PLATFORM_WINDOWS
#include "windows.h"
#include "windowsx.h"

#pragma comment(linker,"/manifestdependency:\"\
	type='win32' name='Microsoft.Windows.Common-Controls' \
	version='6.0.0.0' processorArchitecture='amd64'\
	publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

namespace MXVisual
{
#if PLATFORM == PLATFORM_WINDOWS
	std::unordered_map<UInt64, Window*> gMapWindows;

	LRESULT CALLBACK gWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		static auto GetMsgWindow = [](HWND hwnd)
		{
			Window* pWnd = nullptr;
			auto pRenderWindow = gMapWindows.find((UInt64)hwnd);
			if (pRenderWindow != gMapWindows.end())
			{
				pWnd = pRenderWindow->second;
			}
			return pWnd;
		};

		Window* pCurrentWnd = nullptr;
		switch (msg)
		{
		case WM_CREATE:
		{
			CREATESTRUCT *cs = (CREATESTRUCT*)lParam;
			pCurrentWnd = (Window*)cs->lpCreateParams;
			if (pCurrentWnd != nullptr)
			{
				//pCurrentWnd->OnCreate();
			}
			break;
		}
		case WM_SIZE:
		{
			pCurrentWnd = GetMsgWindow(hwnd);
			if (pCurrentWnd != nullptr)
			{
				//pCurrentWnd->OnResized();
			}
			break;
		}
		case WM_ACTIVATE:
		{
			pCurrentWnd = GetMsgWindow(hwnd);
			if (pCurrentWnd != nullptr)
			{
				//pCurrentWnd->m_bActived = LOWORD(wParam) == WA_INACTIVE ? False : True;
			}
			break;
		}
		case WM_SETFOCUS:
		{
			pCurrentWnd = GetMsgWindow(hwnd);
			if (pCurrentWnd != nullptr)
			{
				//pCurrentWnd->m_bActived = True;
			}
			break;
		}
		case WM_KILLFOCUS:
		{
			pCurrentWnd = GetMsgWindow(hwnd);
			if (pCurrentWnd != nullptr)
			{
				//pCurrentWnd->m_bActived = False;
			}
			break;
		}
		case WM_PAINT:
		{
			pCurrentWnd = GetMsgWindow(hwnd);
			if (pCurrentWnd != nullptr)
			{
				//pCurrentWnd->OnPaint();
			}
			break;
		}
		case WM_DESTROY:
		{
			//pCurrentWnd->OnDestroy();
			gMapWindows.erase((UInt64)hwnd);
			PostQuitMessage(0);
			return 0;
		}
		default:
			break;
		}
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
#endif

	UInt64 Window::CreateRealWindow(const Char * szTitle,
		UInt32 uX, UInt32 uY, UInt32 uWidth, UInt32 uHeight,
		Window* pWnd, Window* pParent)
	{
		UInt64 hRes = -1;
#if PLATFORM == PLATFORM_WINDOWS
		WNDCLASS wndc;
		wndc.style = CS_HREDRAW | CS_VREDRAW;
		wndc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		wndc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wndc.cbClsExtra = 0;
		wndc.cbWndExtra = 0;
		wndc.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
		wndc.lpszMenuName = NULL;
		wndc.lpszClassName = szTitle;
		wndc.lpfnWndProc = gWndProc;
		wndc.hInstance = NULL;

		if (FAILED(RegisterClass(&wndc)))
		{
			return false;
		}

		hRes = (UInt64)CreateWindow(szTitle, szTitle,
			(pParent == nullptr) ? WS_OVERLAPPEDWINDOW : WS_CHILDWINDOW,
			CW_DEFAULT, CW_DEFAULT,
			CW_DEFAULT, CW_DEFAULT, (HWND)pParent->m_hWnd, NULL, NULL, pWnd);
#endif
		return hRes;
	}

	Window::Window(const Char* szName, Window* pParent)
	{
		m_hWnd = CreateRealWindow(szName, CW_DEFAULT, CW_DEFAULT, CW_DEFAULT, CW_DEFAULT,
			this, pParent);

		gMapWindows.insert(std::pair<UInt64, Window*>(m_hWnd, this));
	}

	Window::~Window()
	{
		DestroyWindow((HWND)m_hWnd);
	}

	Float2 Window::ScreenToWindow(const Float2& ScreenPos) const
	{
		Float2 Res;
#if PLATFORM == PLATFORM_WINDOWS
		POINT pt = { ScreenPos[0], ScreenPos[1] };
		ScreenToClient((HWND)m_hWnd, &pt);
		Res = Float2(pt.x, pt.y);
#endif	
		return Res;
	}

	Void Window::SetPosition(UInt32 uX, UInt32 uY)
	{
#if PLATFORM == PLATFORM_WINDOWS
		MoveWindow((HWND)m_hWnd, uX, uY, 0, 0, False);
#endif
	}

	Void Window::Resize(UInt32 uWidth, UInt32 uHeight)
	{
#if PLATFORM == PLATFORM_WINDOWS
		MoveWindow((HWND)m_hWnd, 0, 0, uWidth, uHeight, true);
#endif
	}

	Void Window::Show(Boolean bShow)
	{
#if PLATFORM == PLATFORM_WINDOWS
		ShowWindow((HWND)m_hWnd, bShow ? SW_SHOW : SW_HIDE);
		UpdateWindow((HWND)m_hWnd);
#endif
	}

	Void Window::ShowFramework(Boolean bShow)
	{
		if (bShow)
		{
			//SetWindowLong((HWND)m_hWnd, )
		}
		else
		{

		}
	}
}
