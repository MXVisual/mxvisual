#pragma once
#ifndef _MX_GLOBAL_VARIABLE_
#define _MX_GLOBAL_VARIABLE_
#include "MXPlatform.h"
#include "MXString.h"

namespace MXVisual
{
	class GlobalVariable
	{
	public:
		enum Property
		{
			eReadOnly,
			eReadWrite,
		};

		GlobalVariable(const Char* szName, Property eProp, Float1 DefaultValue);
		GlobalVariable(const Char* szName, Property eProp, const Float3& DefaultValue);
		GlobalVariable(const Char* szName, Property eProp, SInt32 DefaultValue);
		GlobalVariable(const Char* szName, Property eProp, Boolean DefaultValue);
		~GlobalVariable() {}

		const Char* GetName();
		Void	 Reset();

		union
		{
			Float1 fValue;
			Float3 f3Value;
			SInt32 nValue;
			Boolean bValue;
		};

	private:
		Property	 m_property;
		String		 m_strName;
		union
		{
			Float1	fDefaultValue;
			Float3  f3DefaultValue;
			SInt32	nDefaultValue;
			Boolean bDefaultValue;
		};
	};
}

#endif