#pragma once
#ifndef _MX_COMMON_
#define _MX_COMMON_
#include "MXPlatform.h"
#include "MXString.h"

namespace MXVisual
{
#define ARRAY_ELEMENT_COUNT(t) sizeof(t)/sizeof(t[0])

	class IRuntimeTypeIndentification
	{
	public:
		virtual const Char* GetClassName() const = 0;
		virtual UInt32		GetClassID() const = 0;
	};

	//TODO!! ClassID
#define DECLARE_RTTI_INFO(tn)\
	public:\
		virtual const Char* GetClassName() const override { return #tn; }\
		virtual UInt32		GetClassID() const override { return (UInt32)strlen(#tn);}
}

#endif