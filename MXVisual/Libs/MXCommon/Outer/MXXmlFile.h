#pragma once
#ifndef _MX_XML_
#define _MX_XML_
#include "MXPlatform.h"
#include "MXConstant.h"
#include "MXInterface.h"
#include "MXArray.h"
#include "MXString.h"

namespace MXVisual
{
	class XmlTag
	{
	public:
		XmlTag(const Char* szName);
		~XmlTag();

		const Char* GetTagName();

		void AddAttribute(const Char* szName, const Char* szValue);

		UInt32 GetAttributeCount();
		const Char* GetAttributeNameByIndex(UInt32 uIndex);
		const Char* GetAttributeValueByIndex(UInt32 uIndex);
		const Char* GetAttributeValueByName(const Char* szAttributeName);

		void AddChildTag(XmlTag* pTag);
		UInt32 GetChildTagCount();
		XmlTag* GetChildTag(UInt32 uIndex);

	protected:
		char m_szName[Constant_MAX_PATH];
		Array<String> m_aAttributeName;
		Array<String> m_aAttributeValue;
		Array<XmlTag*> m_aChildTag;
	};

	class IXmlFile : public IInterface
	{
	public:
		static IXmlFile *Open(const Char *szFileName, String* pStrError = nullptr);

		virtual ~IXmlFile() {};
		virtual XmlTag* GetRootTag() = 0;
		virtual const Char* GetFileData() = 0;
		//virtual void Close() = 0;
	};
}
#endif