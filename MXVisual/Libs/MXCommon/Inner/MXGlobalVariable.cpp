#include "MXGlobalVariable.h"
#include "MXConstant.h"
#include "MXVector.h"

namespace MXVisual
{
	GlobalVariable::GlobalVariable(const Char* szName, Property eProp, Float1 DefaultValue)
		: m_strName(szName)
		, m_property(eProp)
		, fValue(DefaultValue)
		, fDefaultValue(DefaultValue)
	{
	}
	GlobalVariable::GlobalVariable(const Char* szName, Property eProp, const Float3& DefaultValue)
		: m_strName(szName)
		, m_property(eProp)
		, f3Value(DefaultValue)
		, f3DefaultValue(DefaultValue)
	{
	}
	GlobalVariable::GlobalVariable(const Char* szName, Property eProp, SInt32 DefaultValue)
		: m_strName(szName)
		, m_property(eProp)
		, nValue(DefaultValue)
		, nDefaultValue(DefaultValue)
	{
	}
	GlobalVariable::GlobalVariable(const Char* szName, Property eProp, Boolean DefaultValue)
		: m_strName(szName)
		, m_property(eProp)
		, bValue(DefaultValue)
		, bDefaultValue(DefaultValue)
	{
	}

	const Char* GlobalVariable::GetName() { return m_strName.CString(); }
	Void	 GlobalVariable::Reset()
	{
		fValue = fDefaultValue;
		/*
		uValue = uDefaultValue;
		nValue = nDefaultValue;
		bValue = bDefaultValue;
		*/
	}
}
