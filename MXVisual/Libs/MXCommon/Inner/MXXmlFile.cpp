#include "MXXmlFile.h"
#include "tinyxml.h"
#include "tinystr.h"

namespace MXVisual
{
	XmlTag::XmlTag(const Char* szName)
	{
		strcpy(m_szName, szName);
	}
	XmlTag::~XmlTag()
	{
		for (UInt32 u = 0; u < m_aChildTag.ElementCount(); u++)
		{
			delete m_aChildTag[u];
		}
		m_aChildTag.Clear();
	}

	const Char* XmlTag::GetTagName()
	{
		return m_szName;
	}

	void XmlTag::AddAttribute(const Char* szName, const Char* szValue)
	{
		m_aAttributeName.Add(String(szName));
		m_aAttributeValue.Add(String(szValue));
	}

	UInt32 XmlTag::GetAttributeCount()
	{
#ifdef DEBUG
		assert(m_aAttributeName.ElementCount() == m_aAttributeValue.ElementCount());
#endif
		return m_aAttributeName.ElementCount();
	}
	const Char* XmlTag::GetAttributeNameByIndex(UInt32 uIndex)
	{
#ifdef DEBUG
		assert(m_aAttributeName.ElementCount() == m_aAttributeValue.ElementCount());
#endif

		if (uIndex < m_aAttributeName.ElementCount())
			return m_aAttributeName[uIndex].CString();
		return nullptr;
	}
	const Char* XmlTag::GetAttributeValueByIndex(UInt32 uIndex)
	{
#ifdef DEBUG
		assert(m_aAttributeName.ElementCount() == m_aAttributeValue.ElementCount());
#endif

		if (uIndex < m_aAttributeValue.ElementCount())
			return m_aAttributeValue[uIndex].CString();
		return nullptr;
	}

	const Char * XmlTag::GetAttributeValueByName(const Char * szAttributeName)
	{
		for (UInt32 u = 0; u < m_aAttributeName.ElementCount(); u++)
		{
			if (_stricmp(m_aAttributeName[u].CString(), szAttributeName) == 0)
			{
				return m_aAttributeValue[u].CString();
			}
		}
		return "";
	}

	void XmlTag::AddChildTag(XmlTag * pTag)
	{
		m_aChildTag.Add(pTag);
	}
	UInt32 XmlTag::GetChildTagCount()
	{
		return m_aChildTag.ElementCount();
	}
	XmlTag* XmlTag::GetChildTag(UInt32 uIndex)
	{
		if (uIndex < m_aChildTag.ElementCount())
			return m_aChildTag[uIndex];
		return nullptr;
	}


	class XmlFile : public IXmlFile
	{
	public:
		~XmlFile();

		Boolean LoadFile(const Char *szFileName, String* pStrError);

		XmlTag* GetRootTag();
		const Char* GetFileData();

		Void Release()
		{
			m_nRefCount--;
		}

	private:
		XmlTag* LoadTag(TiXmlNode *pNode);
		void LoadAttribute(TiXmlElement* pElement, XmlTag *pTag);

		XmlTag *m_pMainTag;
		String m_strFileContent;
	};

	Boolean XmlFile::LoadFile(const Char * szFileName, String* pStrError)
	{
		TiXmlDocument *pDoc = new TiXmlDocument();
		if (pDoc->LoadFile(szFileName))
		{
			FILE *pFile = fopen((Char*)szFileName, "r");
			Char buf[Constant_MAX_PATH];
			while (fgets(buf, Constant_MAX_PATH, pFile) > 0)
			{
				m_strFileContent += buf;
			}
			fclose(pFile);

			if (!pDoc->FirstChild())
			{
				delete pDoc;
				if (pStrError)
					*pStrError += "XML File has No Root Tag";
				return False;
			}
			m_pMainTag = LoadTag(pDoc->FirstChild());
			pDoc->Clear();
			delete pDoc;
			return True;
		}
		if (pStrError)
			*pStrError += pDoc->ErrorDesc();
		return False;
	}

	XmlTag* XmlFile::GetRootTag()
	{
		return m_pMainTag;
	}

	const Char * XmlFile::GetFileData()
	{
		//TODO 返回xml文件内容
		return nullptr;
	}

	XmlTag* XmlFile::LoadTag(TiXmlNode * pNode)
	{
		//解析pNode，然后返回这个Tag
		XmlTag *pTag = new XmlTag(pNode->Value());
		LoadAttribute(pNode->ToElement(), pTag);

		TiXmlNode *pChildNode = pNode->FirstChild();
		while (pChildNode)
		{
			pTag->AddChildTag(LoadTag(pChildNode));
			pChildNode = pChildNode->NextSibling();
		}
		return pTag;
	}

	void XmlFile::LoadAttribute(TiXmlElement* pElement, XmlTag * pTag)
	{
		if (!pElement)
			return;

		TiXmlAttribute* pAttrib = pElement->FirstAttribute();
		while (pAttrib)
		{
			pTag->AddAttribute(pAttrib->Name(), pAttrib->Value());
			pAttrib = pAttrib->Next();
		}
	}

	XmlFile::~XmlFile()
	{
		delete m_pMainTag;
	}

	IXmlFile * IXmlFile::Open(const Char *szFileName, String* pStrError)
	{
		XmlFile *pResult = new XmlFile();
		if (pResult->LoadFile(szFileName, pStrError))
		{
			return pResult;
		}
		else
		{
			delete pResult;
			return nullptr;
		}
	}

}