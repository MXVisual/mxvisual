# MXVisual
Target: Base Game Engine

Request: VS2017 c++

 ![Image text](./SampleImages/Editor.jpg)
 ![Image text](./SampleImages/Demo.jpg)

### [编译]
1. 安装VS2017(17763 Win10 SDK)
2. 运行根目录下的.sln工程文件
3. 编译Editor、BaseScene(暂时只能用Debug、Release版本)

### [Demo运行]
1. 打开编辑器，打开工程根目录下的SampleProject.mscn场景
2. 正常退出编辑器
3. 将编辑器根目录(Binary/Tools/Editor)的.Caches文件夹拷贝到Demo运行目录下(Binary/Demos/BaseScene)
4. 编译Demo，正常运行

### [进度]
2021.1.24	Cascade Shadow Map

### [下一步]
1. 视频播放
2. TAA
3. 后期材质系统（计算着色器支持）
4. 地形、水面网格细分，LOD切换
5. Editor模式下，所有Lib改用dll，Editor从此不需要再重新编译，
	Script库可以动态修改（比如Component逻辑修改）
6. 物理碰撞测试回调
7. 骨骼动画混合、动画混合管理器
8. 多线程加载、渲染等
9. 利用7z资源打包
...